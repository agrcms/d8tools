<?php

use Drupal\node\Entity\Node;

global $connection, $dbname;
global $site_dir; 

 //hijacking this cron hook to fix article bodies to all be basic_html format
//$node = Node::load('4795');
 // \Drupal::logger('formatter_script')->notice("node title:".$node->getTitle());
 
 //echo "node title:".$node->getTitle();
  
  //start with node 4795
 /* 
  $node = Node::load('4795');
  \Drupal::logger('formatter_script')->notice("node title:".$node->getTitle());
  $body = $node->get('body')->value;
  $format = $node->get('body')->format;
  echo $format;
  //$node->body->setValue(['value' => $body, 'format' => 'basic_html']);
  //$node->save();
  
  */

	$nids = \Drupal::entityQuery('node')->condition('type','mandate_letter')->execute(); //minister, parliamentary_secretaries, mandate_letter
	//$nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);
	$numnodes = sizeof($nids);
	echo "count:".$numnodes."\n";
	
try {
	$i=0;
	$changed = 0;
	foreach($nids as $nid){
		$node = Node::load($nid);
		
		//$nid = $node->id();
		/*
		if ($nid == '4795'){
			//\Drupal::logger('formatter_script')->notice("nid:".$nid);
			$format = $node->get('body')->format;
			echo "nid:".$nid.", format=".$format."\n";
			$frenchFormat = $node->getTranslation('fr')->get('body')->format;
			echo "nid:".$nid.", FRformat=".$frenchFormat."\n";
		}
		*/
		$i++;
		
		if ($i % 100 == 0){
			echo $i.", nid:".$nid."\n";
		}
		
		//$node = Node::load($nid);
		$format = $node->get('body')->format;
		$frenchFormat = $node->getTranslation('fr')->get('body')->format;
		
		if ($format != 'basic_html' || $frenchFormat != 'basic_html'){
			$body = $node->get('body')->value;
			$node->body->setValue(['value' => $body, 'format' => 'basic_html']);
			
			
			$bodyFR = $node->getTranslation('fr')->get('body')->value;
			$node->getTranslation('fr')->body->setValue(['value' => $bodyFR, 'format' => 'basic_html']);
			
			$node->save();	
			$changed++;
		}
  		
	}
	echo "\ndone! ".$changed." nodes should be basic_html now. dont forget to drush cr before checking.";
}
catch (Exception $e){
	echo 'exception:'.$e->getMessage()."\n";
}
	
	//\Drupal::logger('formatter_script')->notice("nodes:".$i);
	
	