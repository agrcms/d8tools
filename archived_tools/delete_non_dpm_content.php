<?php

use Drupal\node\Entity\Node;

global $connection, $dbname;
global $site_dir;

$site_dir = 'dpm';
//$site_dir = 'default';
$connection = \Drupal::database();
$opt = $connection->getConnectionOptions();
$dbname = $opt['database'];

$type = 'videos';
$query = $connection->query("SELECT count(*) as cnt FROM {node}");
$result = $query->fetchAll();
if ($row = $result[0]) {
  $nnodes = $row->cnt;
}

echo "Found $nnodes nodes...\n";

$query = $connection->query("SELECT nid,type FROM {node} order by nid");
$result = $query->fetchAll();

$cnt = 0;
$first_banner = false;
foreach ($result as $row) {
  // Just don't delete the first banner for now
  if ($row->type == 'banner') {
    continue;
  }
  if ($row->type == 'page') {
    continue;
  }
  if ($row->type == 'webform') {
    continue;
  }
  if ($row->type == 'mandate_letter') {
    continue;
  }
  if ($row->type == 'minister') {
    continue;
  }
  if ($row->type == 'shareable_news_image') {
    continue;
  }
  if ($row->type == 'canada_logo') {
    continue;
  }

  if ($row->type == 'article') {
    $article_nids = '8978,8966,8957,8840,8801,8747,8735,8717,8663,8609,8615,8537,8531,8528,8498,8489,8465,8456,8444,8348,8342,8279,8240,8231,8144,8123,8108,8003,7979,7835,7826,7760,7673,7652,7610,7598,7583,7574';
    $article_nids = explode(",", $article_nids);// Make it an array.
    foreach ($article_nids as $article_nid) {
      if ($row->nid == $article_nid) {
        echo "Skipping nid " . $row->nid . "\n";
        continue;
      }
    }
  }
  $cnt++;
  if (($cnt % 100) == 0) {
    echo "$cnt\n";
  }
  $node = Node::load($row->nid);
  $node->delete();
}

// Re-enable when we start deleting banners again
//$connection->query("ALTER TABLE {node} AUTO_INCREMENT=1");

$cnt = 0;
$keeps = 0;
if (1) {
  echo "Deleting media files...\n";
  
  $files_dir = DRUPAL_ROOT."/sites/$site_dir/files";
  
  // Get the current usage for each fid
  // Commented this out because we need to include blocks too, which is module=editor and type=block_content
  //$query = $connection->query("SELECT fid,id FROM {file_usage} where module='file' and type='node'");
  $query = $connection->query("select fid from file_managed where uri like '%minister%' or uri like '%secretary' or uri like '%article%' or uri like '%override%' or uri like '%news%' or uri like '%twitt%' or uri like '%logo%' or fid in (select fid from file_usage)");
  $result = $query->fetchAll();
  $usage = array();
  foreach ($result as $row) {
    $usage[$row->fid][] = $row->id;
  }
  
  // Now get all the managed files
  $query = $connection->query("SELECT fid,uri FROM {file_managed}");
  $result = $query->fetchAll();
  foreach ($result as $row) {
    if (empty($usage[$row->fid])) {
      $cnt++;
      if (($cnt % 100) == 0) {
        echo "$cnt\n";
      }
      $entity = \Drupal::entityManager()->getStorage('file')->load($row->fid);
      $entity->delete();
    }
    else {
      $keeps++;
      if (($keeps % 25) == 0) {
        echo "keeping: $keeps files\n";
      }
    }
  }
}

echo "Reset auto_increment counters...\n";

//reset_ai('id', 'block_content');
//reset_ai('revision_id', 'block_content_revision');
//reset_ai('cid', 'comment');
//reset_ai('fid', 'file_managed');
//reset_ai('fid', 'flood');
//reset_ai('lid', 'locales_location');
//reset_ai('lid', 'locales_source');
//reset_ai('id', 'menu_link_content');
//reset_ai('mlid', 'menu_tree');
//reset_ai('nid', 'node');
//reset_ai('vid', 'node_revision');
//reset_ai('item_id', 'queue');
//reset_ai('value', 'sequences');
//reset_ai('id', 'shortcut');
//reset_ai('tid', 'taxonomy_term_data');
//reset_ai('pid', 'url_alias');
reset_ai('wid', 'watchdog');

function reset_ai($fld, $table)
{
  global $connection, $dbname;
  
  $query = $connection->query("SELECT $fld FROM {$table} ORDER BY $fld DESC");
  $result = $query->fetchAll();
  if ($row = $result[0]) {
    $set_ai = $row->$fld + 1;
  } else {
    $set_ai = 1;
  }
  
  $query = $connection->query("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='$dbname' AND TABLE_NAME='$table'");
  $result = $query->fetchAll();
  if ($row = $result[0]) {
    $cur_ai = $row->auto_increment;
  } else {
    $cur_ai = null;
  }
  
  if ($cur_ai != $set_ai) {
    $sql = "ALTER TABLE $table AUTO_INCREMENT=$set_ai";
    echo "$sql\n";
    $connection->query($sql);
  }
}
