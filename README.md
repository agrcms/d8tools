Several scripts for various levels/steps of export/import processing.

Initial export from teamsite starts with a table of values exported via sql to an excel spreadsheet
```
    select
        "METADATA_DCR_ID",
        "METADATA_DCR_LANG_CODE",
        "DC_TITLE_NAME",
/*       "DC_DESC",
        "AAFC_KEYWORD_NAME", */
        "DCTERMS_MODIFIED_DATE",
        "DCTERMS_ISSUED_DATE",
--        "DCR_NAME",
        "DCR_TYPE_NAME",
        "NODE_ID",
        "DCR_LAYOUT_NAME",
        "CREATED_DATE",
        "LAST_UPDATE_DATE" 
    from
        "AMAP"."AMAP_METADATA_DCR"
    where
                METADATA_DCR_LANG_CODE='eng' OR METADATA_DCR_LANG_CODE='fra'
```

From Here, export the excel to csv tab seperated with " identifiers the header row is adjusted removing most of the prefixes (as follows):
"dcr_id"        "lang"  "title" "modified"      "issued"        "type_name"     "node_id"       "layout_name" "created"        "updated"

Then another script is run to convert these csv rows into json files:

`d8tools/csv_to_json_export.php  path/to/d8tools/Intranet_Database_dumpEN_FR_tab_adjusted.csv \t`

the above script writes to the new_export folder

now that those are converted to json records, we run the updatejsons script on it as follows:


how to run the `update_json.php` script (must be inside the AAFC network)

`cd /some/folder`

`git clone https://gitlab.com/agrcms/d8tools.git`

`cd d8tools`

`php update_json.php /some/folder/new_export teamsite`

teamsite content json entities are written into json files in the new_export folder

Now for the sitemap

The source sitemap is in xml
I have created a script to convert it to json: 

`cd agrisource/xml2json`

`php teamsite_sitemap_xml2json.php`


and then from json to flatten it into json entities with this script:

`php agrisource/process_sitemap.php`

sitemap json entities will be written into the `export` folder

`php update_json.php /some/folder/export sitemap`

sitemap json entities will be updated into the export folder with meta data and body content.


Troubleshooting: locate files with less than 800 bytes, have the update_json.php script scripts re-run the export (page reader)

mkdir run_json_update_again;

find new_export -type f -size -800c -exec echo cp\ {}\ run_json_update_again/. \;


How to update missing information of News and Employment Opportunity after migrating scripts were executed.

Drupal Bug:
When you run any import or update script, you receive the error:
**Drupal\Componet\Plugin\Exception\PlugninNOtFoundException: The "EntityHasField" plugin does not exist.
Valid plugin IDs for Drupal\Core\ValidationConstraintManger are: Callback...
In SqlContentEntiyStorage.php line 846:
The "EntityHasField" plugin does not exist. **


Workaround:
Go to /en/admin/content, add any fake node. After that, run the script agian.

How to run updating News information:
1. Convert a csv file as Json files
php csv_to_json_exportfornewscategory.php /fullpath/update_newscategory.csv \t;
2. Run the update script to update the category of the news
drush scr /fullpath/update.php /fullpath/update_newscategory/news-nouv news;
3. Run the update script to update the approval and submitter information of News
php csv_to_json_exportfornewsinfo.php update_newsapprovalsubmitter.csv \t;
drush scr /fullpath/update.php /fullpath/update_newsapprovalsubmitter/news-nouv news;
4. Run the update script to update News expiry date
drush scr /d8tools/d8tools/update.php /d8tools/d8tools/update_newsexpirydate/news-nouv news;

How to run updating Employment Opportunity information
1. php csv_to_json_exportforEOinfo.php  /fullpath/update_emplopportunity.csv \t;
2. drush scr /fullpath/update.php /fullpath/update_newsapprovalsubmitter/empl-empl empl;



---------------- news's  category & expirydate  ---------------------------------

/**  generate json files into update_newscategory/news-nouv/ folder    ***/


php csv_to_json_exportfornewscategory.php news_category_expireddate_softlaunch.csv \t

 

/**********  update news category and expirydate  **********/

drush scr /d8tools/update.php /d8tools/update_newscategory/news-nouv news;

 
----------------- news's submitter and approver --------------------------------- 

/**  generate json files into update_newsapprovalsubmitter/news-nouv/ folder    ***/


php csv_to_json_exportfornewsinfo.php update_newsapprovalsubmitter_softlaunch.csv \t;

 

/**********  update news approver and submitter  **********/

drush scr /d8tools/update.php /d8tools/update_newsapprovalsubmitter/news-nouv news;

 
--------------- EO submitter and approver ----------------------------------

/**  generate json files into update_emplopportunity/empl-empl/ folder   ***/


php csv_to_json_exportforEOinfo.php update_emplopportunity_softlaunch.csv  \t;

 

/**********  update EO approver and submitter  **********/

drush scr  /d8tools/update.php  /d8tools/update_emplopportunity/empl-empl empl;


 HARDLAUNCH PREPED IMPORT STEPS:
cd /d8;
cp d8tools/convertTeamsiteIdToDrupal/exportcontent_hardlaunch/resources/prod/news/images/*.* ags/html/sites/default/files/legacy/resources/prod/news/images/;
cd /d8/ags;
drush scr ../d8tools/drupal_delete/delete.php /d8/d8tools/new_export_10-28/news-nouv news; #DELETE
drush scr ../d8tools/drupal_export_import/import.php /d8/d8tools/export news
drush scr ../d8tools/drupal_delete/delete.php /d8/d8tools/new_export_10-28/empl-empl empl;#DELETE
drush scr ../d8tools/drupal_export_import/import.php /d8/d8tools/export empl
drush scr ../d8tools/update.php /d8/d8tools/update_newscategory_hardlaunch/news-nouv news;
drush scr ../d8tools/update.php /d8/d8tools/update_newsapprovalsubmitter_hardlaunch/news-nouv news;
drush scr ../d8tools/update.php /d8/d8tools/update_newscategory_hardlaunch/news-nouv news;
drush scr ../d8tools/update.php /d8/d8tools/update_newsexpirydate_hardlaunch/news-nouv news;
drush scr ../d8tools/update.php /d8/d8tools/update_emplopportunity_hardlaunch/empl-empl empl;

drush scr ../d8tools/convertTeamsiteIdToDrupal/convert_teamsiteId_to_drupal.php HARD_LAUNCH;
drush scr ../d8tools/convertTeamsiteIdToDrupal/updateContentURL.php /d8/d8tools/convertTeamsiteIdToDrupal/export/

