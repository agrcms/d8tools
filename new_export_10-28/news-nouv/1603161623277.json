{
    "dcr_id": "1603161623277",
    "lang": "en",
    "title": {
        "en": "This Halloween, Become Part of the Movement of Inclusivity and Treat Accessibly!",
        "fr": "Cette ann\u00e9e, \u00e0 l'Halloween, contribuez \u00e0 la cr\u00e9ation d'un monde inclusif en participant au mouvement Treat Accessibly!"
    },
    "modified": "2020-10-22 00:00:00.0",
    "issued": "2020-10-22 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2020-10-22",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2020-10-22",
            "fr": "2020-10-22"
        },
        "modified": {
            "en": "2020-10-22",
            "fr": "2020-10-22"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "This Halloween, Become Part of the Movement of Inclusivity and Treat Accessibly!",
            "fr": "Cette ann\u00e9e, \u00e0 l'Halloween, contribuez \u00e0 la cr\u00e9ation d'un monde inclusif en participant au mouvement Treat Accessibly!"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "The importance of disability inclusion is becoming recognized in many ways.",
            "fr": "L'importance de l'inclusion des personnes handicap\u00e9es commence \u00e0 jouir d'une reconnaissance \u00e0 bien des niveaux."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>The importance of disability inclusion is becoming recognized in many ways. Most recently, the Canadian government introduced the new <i>Accessibility Act.</i> The <i>Act </i>is how the government will react; but what about us as a society? Often exclusion is the result of innocent actions because we lack the lived experience or exposure needed for awareness.</p>\n<p>A few years ago, while getting ready for Halloween, one Toronto man was enlightened while watching a neighbour\u2019s child with a disability. When he looked around, he realized that this child could not trick or treat at his place or almost any of the houses on his street. The child was literally being left at the curb! He decided to do something about it. First, he started with his own home by moving the candy to his garage, where it would be accessible; but he did not end there. He needed to spread awareness -\u00a0and from then on, the Treat Accessibly Movement was founded.</p>\n<p>It\u2019s tough to imagine, but it is estimated that almost 400,000 Canadian kids have some sort of accessibility needs. This Halloween, the Persons with Disabilities Network would like to ask you to take some time and explore the tips that have been posted on the <a href=\"https://www.treataccessibly.com/\">Treat Accessibly Website</a> and participate in inclusivity. Not only does it contain tips on accessibility, it also incorporates COVID safety tips!</p>\n<p>All of these tips are simple things which make a big difference. Things like moving your car to the side of the driveway, removing obstacles, relocating your trick or treating to your driveway or garage, and using chalk to create safety space markings. Now that your space is accessible, you can pick up or print a sign (details on the website) to create awareness and show others how to be part of the solution!</p>\n<p>In addition, the Persons with Disabilities Netowrk (PwDN) reminds you that not all disabilities are mobility issues. Some disabilities, like those on the autistic spectrum or some developmental disabilities, may result in a child who is nonverbal or has anxiety or learning issues. For them it may be a big accomplishment just to have the confidence to come to your door in a costume. So please encourage them by offering a big smile, even if they can\u2019t squeak out the lovely \u201cTrick or Treat!\u201d\u00a0Tell them how much you love seeing them dressed up coming to see you. This October 31, you may just make a difference for that child and, more importantly, play your part in creating a more inclusive Canada!</p>\n<p>To learn more about disabilities, tips on disability, or about the PwDN, visit the\u00a0<a href=\"https://collab.agr.gc.ca/co/pdn-rph/SitePages/Treat%20Accessibility.aspx\">PwDN Knowledge Workspace page</a>\u00a0or email: <a href=\"mailto:aafc.pwdn-rph.aac@canada.ca\">aafc.pwdn-rph.aac@canada.ca</a>.</p>\n<p>The PwDN is a confidential employee network that welcomes all employees (including managers) who wish to share in the goal of making our department accessible and inclusive.</p>\n<p>Learn more about AAFC\u2019s <a href=\"https://collab.agr.gc.ca/co/eedi-emedi/SitePages/Home.aspx\">Diversity Networks</a> and their upcoming events.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/TreatAccessibly-EN.png\" class=\"center-block\"></p>\n<p>\u00a0</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>L\u2019importance de l\u2019inclusion des personnes handicap\u00e9es commence \u00e0 jouir d\u2019une reconnaissance \u00e0 bien des niveaux. Plus r\u00e9cemment, le gouvernement du Canada a d\u00e9pos\u00e9 la <em>Loi canadienne sur l\u2019accessibilit\u00e9</em>. Cette loi pr\u00e9cise comment le gouvernement doit agir, mais qu\u2019en est-il de la fa\u00e7on dont nous, comme soci\u00e9t\u00e9, sommes cens\u00e9s agir? Souvent, l\u2019exclusion r\u00e9sulte de choses que l\u2019on fait involontairement parce qu\u2019on n\u2019a pas v\u00e9cu soi-m\u00eame une exp\u00e9rience donn\u00e9e ou qu\u2019on n\u2019y est pas sensibilis\u00e9.</p>\n<p>Il y a quelques ann\u00e9es, un Torontois qui se pr\u00e9parait pour l\u2019Halloween a eu une id\u00e9e en observant l\u2019enfant handicap\u00e9 d\u2019un de ses voisins. En regardant autour de lui, l\u2019homme s\u2019est rendu compte que l\u2019enfant en question ne pourrait pas venir chercher de friandises chez lui ni chez plusieurs autres de ses voisins. L\u2019enfant devait litt\u00e9ralement \u00eatre laiss\u00e9 derri\u00e8re. L\u2019homme a donc d\u00e9cid\u00e9 d\u2019agir. Il a commenc\u00e9 par se mettre \u00e0 distribuer ses friandises \u00e0 partir de son garage, o\u00f9 elles seraient accessibles. Il ne s\u2019est toutefois pas arr\u00eat\u00e9 l\u00e0! Il devait sensibiliser d\u2019autres gens \u00e0 cette r\u00e9alit\u00e9, et c\u2019est ainsi qu\u2019est n\u00e9 le mouvement Treat Accessibly.</p>\n<p>On ne s\u2019imagine pas qu\u2019au Canada, il est estim\u00e9 que pr\u00e8s de 400\u2009000 enfants ont des besoins en mati\u00e8re d\u2019accessibilit\u00e9. Pour l\u2019Halloween cette ann\u00e9e, le R\u00e9seau des personnes handicap\u00e9es (RPH) vous invite \u00e0 prendre du temps pour examiner les trucs affich\u00e9s sur le site Web du mouvement <a href=\"https://www.treataccessibly.com/\">Treat Accessibly</a> [en anglais seulement] et jouer un r\u00f4le dans la cr\u00e9ation d\u2019un monde inclusif. Le site renferme des trucs sur l\u2019accessibilit\u00e9, de m\u00eame que des conseils sur la s\u00e9curit\u00e9 en temps de COVID!</p>\n<p>M\u00eame s\u2019ils sont simples, tous les trucs peuvent produire de grands effets. Il est notamment sugg\u00e9r\u00e9 de garer votre voiture sur le c\u00f4t\u00e9 de votre entr\u00e9e de cour, de distribuer vos friandises dans votre entr\u00e9e de cour ou \u00e0 partir de votre garage et d\u2019utiliser de la craie pour cr\u00e9er des espaces de s\u00e9curit\u00e9. Une fois votre espace accessible, vous pourrez aller chercher ou imprimer un panneau (d\u00e9tails sur le site Web) pour sensibiliser d\u2019autres gens et leur montrer comment faire partie de la solution.</p>\n<p>En outre, le RPH vous rappelle qu\u2019un handicap n\u2019est pas n\u00e9cessairement un probl\u00e8me de mobilit\u00e9. Certains enfants, comme ceux sur le spectre de l\u2019autisme ou pr\u00e9sentant certains troubles du d\u00e9veloppement, peuvent ne pas \u00eatre en mesure de s\u2019exprimer de vive voix, faire de l\u2019anxi\u00e9t\u00e9 ou avoir des troubles de l\u2019apprentissage. Pour eux, le seul fait de se pr\u00e9senter \u00e0 votre porte dans un costume est un exploit. Encouragez\u2011les en leur souriant, m\u00eame s\u2019ils n\u2019arrivent pas \u00e0 vous dire \u00ab Joyeuse Halloween \u00bb \u2013 dites-leur \u00e0 quel point vous \u00eates heureux de les voir dans leur costume. Le 31 octobre, vous pourriez laisser une marque dans la vie d\u2019un enfant et, surtout, contribuer \u00e0 faire du Canada un pays plus inclusif!</p>\n<p>Pour en apprendre plus sur les handicaps, obtenir des trucs sur les handicaps ou en savoir plus sur le RPH, visitez la <a href=\"https://collab.agr.gc.ca/co/pdn-rph/SitePages/Treat%20Accessibility.aspx\">page du R\u00e9seau dans l'Espace de travail du savoir</a> ou \u00e9crivez \u00e0 <a href=\"mailto:aafc.pwdn-rph.aac@canada.ca\">aafc.pwdn-rph.aac@canada.ca</a>.</p>\n<p>Le RPH est un r\u00e9seau confidentiel qui accueille tous les employ\u00e9s (y compris les gestionnaires) souhaitant contribuer \u00e0 l\u2019objectif de rendre notre minist\u00e8re plus accessible et inclusif.</p>\n<p>Apprenez-en plus sur les <a href=\"https://collab.agr.gc.ca/co/eedi-emedi/SitePages/Home.aspx\">r\u00e9seaux de la diversit\u00e9</a> d\u2019AAC et leurs prochaines activit\u00e9s.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/TreatAccessibly-FR.png\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "This Halloween, the Person\u2019s with Disabilities Network invites you to become part of the movement of inclusivity and treat accessibly!",
        "fr": "Cette ann\u00e9e, \u00e0 l'Halloween, contribuez \u00e0 la cr\u00e9ation d'un monde inclusif en participant au mouvement"
    },
    "news": {
        "date_posted": {
            "en": "2020-10-22",
            "fr": "2020-10-22"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Persons with Disabilities Network",
            "fr": "R\u00e9seau des personnes handicap\u00e9es"
        }
    }
}