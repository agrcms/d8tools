<?php
//require '/var/www/clients/client1/web38/web/d8tools/vendor/autoload.php';
//require '../vendor/autoload.php';
//require '../../vendor/autoload.php';
require 'C:\Users\OlstadJ\d8tools\vendor\autoload.php';
use \Goutte\Client;
use \Symfony\Component\HttpClient\HttpClient;

class Sitemap extends UpdateExport
{
  public $success = FALSE;
  //public $could_not_load = FALSE; // inherited.
  function update($jfile)
  {
    $this->loadData($jfile);
    if ($this->data->has_dcr_id && is_numeric($this->data->dcr_id)) {
      $this->add_or_update_body_field();
    }

    $this->save();
    $this->success = TRUE;
    return $this->success;
  }

  public function add_or_update_body_field() {
    global $section_count_en;
    $section_count_en = 0;
    global $section_count_fr;
    $section_count_fr = 0;
    global $clean_this_content_array;
    $clean_this_content_array = array();
    global $section_array;
    $section_array = array();

    $client = new Client(HttpClient::create(['timeout' => 10]));

    $lang = 'en';
    $language = $lang;
    $otherLang = 'fr';
    $crawler = $client->request('GET', $this->path_to_teamsite_content($lang));
    // Go to the intranet.agr.gc.ca website in english
    echo $this->path_to_teamsite_content('en') . "\n";
    $pageNotFound = FALSE; // Default to false.
    $crawler->filter('head title')->each(function ($node) {
      $title = $node->text();
      $test404string = 'Error 404';
      if (stripos($title, $test404string) > 0) {
        $this->data->pageNotFound = TRUE;
        $this->data->has_dcr_id = FALSE;
      }
    });
    if (isset($this->data->pageNotFound) && $this->data->pageNotFound) {
      $test404string = 'Error 404';
      $this->data->pageNotFound = TRUE;
      if (isset($body) && is_object(@$this->data->body) != $test404string) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        @$this->data->body->{$lang} = $test404string;
      } else if (isset($body)) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        $this->data->body[$lang] = $test404string;
      }
      if (isset($body) && is_object(@$this->data->body) != $test404string) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        @$this->data->body->{$lang} = $test404string;
      } else if (isset($body)) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        $this->data->body[$lang] = $test404string;
      }
      if (isset($this->data->body->{$otherLang}) && is_object(@$this->data->body) != $test404string) {
        @$this->data->body->{$lang} = $test404string;
      } else if (!isset($this->data->body->fr)) {
        $this->data->body[$otherLang] = $test404string;
      }
      $section_array = array();
      $section_count_en = 0;
      $section_count_fr = 0;
      $this->success = TRUE;
      $this->pageNotFound = TRUE;
      return FALSE;
    }
    //$crawler->filter('main section')->each(function ($node) {
    $this->data->meta = array();
    $crawler->filter('meta')->each(function ($node) {
      $lang = 'en';
      if (!empty($node->attr('property'))) {
        $attr = $node->attr('property');
        $attr = str_replace('aafc:', '', $attr); // Remove aafc: from property name, eg: aafc:subject = subject.
        $attr = str_replace('dcterms:', '', $attr); // Remove dcterms: from property name, eg: dcterms:description = text.
        $attr = str_replace('dcterms.', '', $attr); // Remove dcterms. from property name, eg: dcterms.description = text.
        $attr = str_replace('aafc.', '', $attr); // Remove aafc: from property name, eg: aafc:subject = subject.
        $attr = str_replace('news.category', 'news-category', $attr); // Remove aafc: from property name, eg: aafc.news.category = news.category
        $value = $node->attr('content');
        if (empty($value)) {
          $value = $node->attr('title');
        }
        if (!empty($attr)) {
          $this->data->meta[$attr][$lang] = $value;
        }
        echo $attr . ' ' . $value . "\n";
      } else {
        echo ' empty property english? ' . "\n";
        echo $node->html();
        echo "\n";
      }
    });
    $crawler
      ->filter('body main.container section:first-child h1:first-child')
      ->each(function (Symfony\Component\DomCrawler\Crawler $crawler) {
          foreach ($crawler as $node) {
            $node->parentNode->removeChild($node);
          }
        }
      );
    $crawler->filter('body main.container section:first-child')->each(function ($node) {
      $lang = 'en';
      $this->data->body = array();
      $this->data->body['en'] = $node->html();
    });
    $crawler->filter('body main.container section')->each(function ($node) {
      $lang = 'en';
      global $section_count_en;
      global $section_array;
      $section_count_en++;
      $section_array[$lang][] = $node->html();
    });
    $crawler
      ->filter('body main.container')
      ->each(function ($node) {
      global $clean_this_content_array;
      $clean_this_content_array['en'][] = $this->clean_up_content($node->html());
    });
    if ($section_count_en > 1) {
      echo $this->data->dcr_id . " debug1 section workaround en\n";
      if (is_object(@$this->data->body)) {
        @$this->data->body->{$lang} = implode($clean_this_content_array[$lang], "\n\n");
      } else {
        $this->data->body[$lang] = implode($clean_this_content_array[$lang], "\n\n");
      }
    }
    $crawler
      ->filter('ol.breadcrumb li:last-child')
      ->each(function ($node) {
      if (is_object(@$this->data->breadcrumb)) {
        if (!$this->data->breadcrumb->{'en'} == $node->text()) {
          $this->data->breadcrumb->{'en'} = $node->text();
        }
      }
      else {
        if (!$this->data->breadcrumb->{'en'} == $node->text()) {
          $this->data->breadcrumb['en'] = $node->text();
        }
      }
    });
//    sleep(1);
    //$client = new Client(HttpClient::create(['timeout' => 5]));

    //$client = NULL;
    //$client = new Client(HttpClient::create(['timeout' => 60]));
    $crawler = $client->request('GET', $this->path_to_teamsite_content($otherLang) . "&test=" . time());
    $crawler->filter('meta')->each(function ($node) {
      $otherLang = 'fr';
      if (!empty($node->attr('property'))) {
        $attr = $node->attr('property');
        $attr = str_replace('aafc:', '', $attr); // Remove aafc: from property name, eg: aafc:subject = subject.
        $attr = str_replace('dcterms:', '', $attr); // Remove dcterms: from property name, eg: dcterms:description = text.
        $attr = str_replace('dcterms.', '', $attr); // Remove dcterms. from property name, eg: dcterms.description = text.
        $attr = str_replace('aafc.', '', $attr); // Remove aafc: from property name, eg: aafc.news.category = news.category
        $attr = str_replace('news.category', 'news-category', $attr); // Remove aafc: from property name, eg: aafc.news.category = news.category
        $value = $node->attr('content');
        if (empty($value)) {
          $value = $node->attr('title');
        }
        if (!empty($attr)) {
          $this->data->meta[$attr][$otherLang] = $value;
        }
      } else {
        echo ' empty property Français? ' . "\n";
        echo $node->outerHtml();
        echo "\n";
      }
    });
    $crawler
      ->filter('body main.container section:first-child h1:first-child')
      ->each(function (Symfony\Component\DomCrawler\Crawler $crawler) {
          foreach ($crawler as $node) {
            $node->parentNode->removeChild($node);
          }
        }
      );
    $crawler
      ->filter('body main.container section:first-child')
      ->each(function ($node) {
      $otherLang = 'fr';
      if (is_object(@$this->data->body)) {
        @$this->data->body->{$otherLang} = $node->html();
      }
      else {
        $this->data->body[$otherLang] = $node->html();
      }
    });
    $crawler
      ->filter('body main.container section')
      ->each(function ($node) {
      global $section_count_fr;
      global $section_array;
      $section_count_fr++;
      $section_array['fr'][] = $node->html();
    });
    $crawler
      ->filter('body main.container')
      ->each(function ($node) {
      global $clean_this_content_array;
      $clean_this_content_array['fr'][] = $this->clean_up_content($node->html());
    });
    if ($section_count_fr > 1/* && strlen($this->data->body['fr']) < 500*/) {
      $otherLang = 'fr';
      echo $this->data->dcr_id . " debug1 section workaround $otherLang\n";
      if (is_object(@$this->data->body)) {
        //@$this->data->body->{$otherLang} = implode($section_array[$otherLang], "\n\n");
        @$this->data->body->{$otherLang} = implode($clean_this_content_array[$otherLang], "\n\n");
      } else {
        //$this->data->body[$otherLang] = implode($section_array[$otherLang], "\n\n");
        $this->data->body[$otherLang] = implode($clean_this_content_array[$otherLang], "\n\n");
        if (!isset($this->data->body[$lang])) {
          //$this->data->body[$lang] = implode($section_array[$lang], "\n\n");
          $this->data->body[$lang] = implode($clean_this_content_array[$lang], "\n\n");
        }
      }
    }
    $crawler
      ->filter('ol.breadcrumb li:last-child')
      ->each(function ($node) {
      if (is_object(@$this->data->breadcrumb)) {
        if (!$this->data->breadcrumb->{'fr'} == $node->text()) {
          $this->data->breadcrumb->{'fr'} = $node->text();
        }
      }
      else {
        if (!$this->data->breadcrumb->{'fr'} == $node->text()) {
          $this->data->breadcrumb['fr'] = $node->text();
        }
      }
    });

    $client = NULL;
    $node = NULL;
    $crawler = NULL;
    $section_array = array();
    $clean_this_content_array = array();
    $section_count_en = 0;
    $section_count_fr = 0;
//    sleep(1);
  }

  public function path_to_teamsite_content($langcode) {
    if (!isset($this->data->dcr_id) || empty($this->data->dcr_id)) {
      return ' empty dcr_id ?';
    }
    if ($langcode == 'en') {
      //return 'https://agriculture-qa.9pro.ca/en/branches-and-offices/corporate-management-branch';
      return 'https://intranet.agr.gc.ca/agrisource/eng?id=' . $this->data->dcr_id;
    } else {
      //return 'https://agriculture-qa.9pro.ca/fr/directions-generales-et-bureaux/direction-generale-de-la-gestion-integree';
      return 'https://intranet.agr.gc.ca/agrisource/fra?id=' . $this->data->dcr_id;
    }
  }


}

