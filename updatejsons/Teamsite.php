<?php
//require '/var/www/clients/client1/web38/web/d8tools/vendor/autoload.php';
//require '../vendor/autoload.php';
require 'C:\Users\OlstadJ\d8tools\vendor\autoload.php';
use \Goutte\Client;
use \Symfony\Component\HttpClient\HttpClient;

class Teamsite extends UpdateExport
{
  public $success = FALSE;
  public $meta_count = FALSE;
  //public $could_not_load = FALSE; // inherited.
  function update($jfile)
  {
    $this->loadData($jfile);
    if (!empty($this->data->dcr_id) && is_numeric($this->data->dcr_id)) {
      $this->success = $this->add_or_update_body_field();
      if ($this->success) {
        $this->save();
        return TRUE;
      }
      echo $this->data->dcr_id . " ?? \n";
      return FALSE;
    } else {
      echo "Error, dcr_id is not numeric, this is likely an external menu link.";
      $this->success = FALSE;
      return FALSE;
    }

  }

  public function changeNewsKeyLabel($key) {
    switch ($key) {
      case "Date Posted":
      case "Date d’affichage":
        return "date_posted";
        break;
      case "To":
      case "À":
        return 'to';
        break;
      case "Sent by":
      case "De":
        return 'by';
        break;
      default:
        return 'other';
        break;
    }
  }

  public function changeEmploymentKeyLabel($key) {
    switch ($key) {
      case "Employment Opportunity Type(s)":
      case "Type de possibilité d'emploi":
        return 'type';
        break;
      case "Classification(s)":
        return 'classification';
        break;
      case "Direction générale":
      case "Branch":
        return 'branch';
        break;
      case "Location(s)":
      case "Lieu":
      case "Lieux":
      case "Lieu(x)":
        return "locations";
        break;
      case "Closing date and time":
      case "Date de clôture":
        return 'date_closing';
        break;
      case "Open to":
      case "Ouvert à":
        return 'open_to';
        break;
      case "Name":
      case "Nom":
        return 'name';
        break;
      case "Email":
      case "Courriel":
        return 'email';
        break;
      default:
        return 'other';
        break;
    } 
  }

  public function processFieldsFromContent($language) {
    if ($this->data->type_name == 'intra-intra/news-nouv' ||
      $this->data->type_name == 'intra-intra/empl-empl') {
      // Happy.
      echo '';
    }
    else {
      // No need to process if not news or empl.
      $this->success = TRUE;
      return TRUE;
    }
    //echo $this->data->type_name . " debug4 " . $language . "\n";
    $body_text = '';
    if (!isset($this->data->body)) {
      $this->success = FALSE;
      return FALSE; // Stop processing.
    }
    if (is_object($this->data->body)) {
      if (is_object($this->data->body->{$language})) {
        $body_text = $this->data->body->{$language};
      } else {
        $body_text = $this->data->body->{$language};
        if (empty($body_text) || is_null($body_text)) {
          return FALSE; // Stop processing.
        }
      }
    } else if (isset($this->data->body[$language])) {
      $body_text = $this->data->body[$language];
    }
    if ($this->data->type_name == 'intra-intra/news-nouv') {
      $catch = 'Sent by:';
      if ($language == 'fr') {
        $catch = 'De :';
      }
      //$offset = 50;
      //$news_meta = substr($body_text, 0, (strpos($body_text, $catch) + $offset));
      $pos_catch_this = strpos($body_text, $catch);
      $next_tag_offset = strlen($catch) + 3;
      $pos_catch_next_tag_after = strpos($body_text, '<', $pos_catch_this + $next_tag_offset);
      $news_meta = substr($body_text, 0, ($pos_catch_next_tag_after-1));//(strpos($body_text, $catch)));
      $body = substr($body_text, $pos_catch_next_tag_after);//(strpos($body_text, $catch)));
      if (isset($body) && is_object(@$this->data->body->{$language})) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        @$this->data->body->{$language} = $body;
      } else if (isset($body)) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        $this->data->body[$language] = $body;
      }
      $news_array = explode("\n", $news_meta);
      $newKeyedCategoryArray = array();
      foreach ($news_array as $news_category_line) {
        if (strpos($news_category_line, ':') > 0) {
          // Begin processing, we have a hit.
          $no_html_tags = strip_tags($news_category_line);
          $clean = str_replace("\n", '', $no_html_tags);
          $cleaner = str_replace("\t", '', $clean);
          $indexOfColon = strpos($cleaner, ':'); 
          $cleanestKey = trim(substr($cleaner, 0, $indexOfColon));
          $cleanestValue = trim(substr($cleaner, $indexOfColon+1, strlen($cleaner)));
          $newKeyedCategoryArray[$cleanestKey] = $cleanestValue;
          if ($cleanestKey == 'Sent by' && $language == 'en') {
            break;
          }
          if ($cleanestKey == 'De' && $language == 'fr') {
            break;
          }
        }
      }
      foreach ($newKeyedCategoryArray as $key => $value) {
        $key = $this->changeNewsKeyLabel($key);
        if (isset($this->data)) {
          if (isset($this->data->news->{$key}->{$language})
          && getType($this->data->news->{$key}->{$language}) == 'object') {
            $this->data->news->{$key}->{$language} = $value; 
          }
	  else {
            // The @ symbol is for ignoring warnings/notices.
            @$this->data->news->{$key}->{$language} = $value;
          }
        }
      }
    }
    else if ($this->data->type_name == 'intra-intra/empl-empl') {
      // Deal with privacy notice.
      if ($language == 'en') {
        $privacy_needle = '<p><strong>Privacy Notice:</strong></p>';
      }
      else {
        $privacy_needle = '<p><strong>Avis de confidentialit';
        $pos_privacy_needle = strpos($body_text, $privacy_needle);
      }
      $pos_privacy_needle = strpos($body_text, $privacy_needle);
      if ($pos_privacy_needle > 50) {
        // Strips off the privacy notice / avis de confidentialité.
        $body_text = substr($body_text, 0, $pos_privacy_needle);
      }
      $catch = '<p><strong>Description</strong></p>';
      $offset = 0;
      if (strpos($body_text, $catch) <= 0) {
        $pos_test = strpos($body_text, 'Email<');
        if ($language == 'fr') {
          $pos_test = strpos($body_text, 'Courriel :');
        }
        $pos_test2 = strpos($body_text, '<h2>Description');
        if ($pos_test < $pos_test2 && $pos_test2 > 0) {
          $catch = '<h2>Description';
          $empl_meta = substr($body_text, 0, ($pos_test2 + $offset));
          $pos_test2_offset = strlen($catch) + strlen('</h2>');
          $body = substr($body_text, $pos_test2 + $pos_test2_offset);
        } else if ($pos_test > 0) {
          $catch = 'Email<';
          if ($language == 'fr') {
            $catch = 'Courriel :';
          }
          //$offset = 50;
          $pos_catch_this = strpos($body_text, $catch);
          $length_of_catch = strlen($catch);
          $next_tag_offset = $length_of_catch + 1;
          $pos_catch_next_tag_after = strpos($body_text, '>', $pos_catch_this + $next_tag_offset);
          $empl_meta = substr($body_text, 0, ($pos_catch_next_tag_after-1));//(strpos($body_text, $catch)));
          $body = substr($body_text, $pos_catch_next_tag_after);//(strpos($body_text, $catch)));
        }
      }
      else {
        $pos_catch = strpos($body_text, $catch);
        $empl_meta = substr($body_text, 0, $pos_catch + $offset);
        $body = substr($body_text, $pos_catch);
      }

      if (isset($body) && is_object(@$this->data->body->{$language})) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        @$this->data->body->{$language} = $body;
      } else if (isset($body)) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        $this->data->body[$language] = $body;
      }
      $empl_array = explode("\n", $empl_meta);
      $newKeyedCategoryArray = array();
      foreach ($empl_array as $empl_category_line) {
        if (strpos($empl_category_line, ':') > 0) {
          // Begin processing, we have a hit.
          $no_html_tags = strip_tags($empl_category_line);
          $clean = str_replace("\n", '', $no_html_tags);
          $cleaner = str_replace("\t", '', $clean);
          $indexOfColon = strpos($cleaner, ':'); 
          $cleanestKey = trim(substr($cleaner, 0, $indexOfColon));
          $cleanestValue = trim(substr($cleaner, $indexOfColon+1, strlen($cleaner)));
          $newKeyedCategoryArray[$cleanestKey] = $cleanestValue;
          if ($cleanestKey == 'Email' && $language == 'en') {
            break;
          }
          if ($cleanestKey == 'Courriel' && $language == 'fr') {
            break;
          }
        }
      }
      foreach ($newKeyedCategoryArray as $key => $value) {
        $key = $this->changeEmploymentKeyLabel($key);
        if ($key == 'date_closing') {
          $sante = ', 23:59';
          $bonbon = '';
          if (strpos($value, $sante) > 0) {
            $value = substr($value, 0, strpos($value, $sante));
          }
        }
        if (isset($this->data)) {
          if (isset($this->data->empl->{$key}->{$language})
          && getType($this->data->empl->{$key}->{$language}) == 'object') {
            $this->data->empl->{$key}->{$language} = $value; 
          }
	  else {
            // The @ symbol is for ignoring warnings/notices.
            @$this->data->empl->{$key}->{$language} = $value;
          }
        }
      }
    }
    return TRUE;
  }

  public function add_or_update_body_field() {
    $client = new Client(HttpClient::create(['timeout' => 30]));
    global $section_count_en;
    $section_count_en = 0;
    global $section_count_fr;
    $section_count_fr = 0;
    global $clean_this_content_array;
    $clean_this_content_array = array();
    global $section_array;
    $section_array = array();

    $lang = 'en';
    $otherLang = 'fr';
    $crawler = $client->request('GET', $this->path_to_teamsite_content($lang));
    // Go to the intranet.agr.gc.ca website in english
    //echo $this->path_to_teamsite_content('en') . "\n"; // Helps for debugging but I no longer want to see this message.
    $pageNotFound = FALSE; // Default to false.
    $crawler->filter('head title')->each(function ($node) {
      $title = $node->text();
      $test404string = 'Error 404';
      if (stripos($title, $test404string) > 0) {
        $this->data->pageNotFound = TRUE;
      }
    });
    if (isset($this->data->pageNotFound) && $this->data->pageNotFound) {
      $test404string = 'Error 404';
      $this->data->pageNotFound = TRUE;
      if (isset($this->data->body->en) && $this->data->body->en != $test404string) {
        $this->data->body['en'] = $test404string;
      } else if (!isset($this->data->body->en)) {
        $this->data->body['en'] = $test404string;
      }
      if (isset($this->data->body->fr) && $this->data->body->fr != $test404string) {
        $this->data->body['fr'] = $test404string;
      } else if (!isset($this->data->body->fr)) {
        $this->data->body['fr'] = $test404string;
      }
      $section_array = array();
      $section_count_en = 0;
      $section_count_fr = 0;
      $this->success = TRUE;
      $this->pageNotFound = TRUE;
      return TRUE;
    }
    $this->data->meta = array();
    $this->meta_count = 0;
    $crawler->filter('meta')->each(function ($node) {
      $lang = 'en';
      if (!empty($node->attr('property'))) {
        if (!isset($this->meta_count)) {
          $this->meta_count = 0;
        }
        $this->meta_count++;
        $attr = $node->attr('property');
                   // Remove aafc: from property name, eg: aafc:subject = subject.
        $attr = str_replace('aafc:', '', $attr);
       	           // Remove dcterms: from property name, eg: dcterms:description = text.
        $attr = str_replace('dcterms:', '', $attr);
                   // Remove dcterms. from property name, eg: dcterms.description = text.
        $attr = str_replace('dcterms.', '', $attr);
       	           // Remove aafc. from property name, eg: aafc:subject = subject.
        $attr = str_replace('aafc.', '', $attr);
       	           // Remove aafc: from property name, eg: aafc.news.category = news.category
        $attr = str_replace('news.category', 'news-category', $attr);
        $value = $node->attr('content');
        if (empty($value)) {
          $value = $node->attr('title');
        }
        if (!empty($attr)) {
          $this->data->meta[$attr][$lang] = $value;
        }
      }
      /*else {
        echo ' empty property english? ' . "\n";
        echo $node->outerHtml();
        echo "\n";
      }*/
    });
    echo "meta_count =" . $this->meta_count . "\n";
    $this->meta_count = NULL;

    $crawler
      ->filter('body main.container section:first-child h1:first-child')
      ->each(function (Symfony\Component\DomCrawler\Crawler $crawler) {
          foreach ($crawler as $node) {
            $node->parentNode->removeChild($node);
          }
        }
      );
    $crawler->filter('body main.container section:first-child')->each(function ($node) {
      $lang = 'en';
      $this->data->body = array();
      $this->data->body['en'] = $node->html();
    });
    $crawler
      ->filter('body main.container section')
      ->each(function ($node) {
      global $section_count_en;
      global $section_array;
      $section_count_en++;
      $section_array['en'][] = $node->outerHtml();
    });
    $crawler
      ->filter('body main.container')
      ->each(function ($node) {
      global $clean_this_content_array;
      $clean_this_content_array['en'][] = $this->clean_up_content($node->html());
    });
    if ($section_count_en > 1) {
      echo $this->data->dcr_id . " debug1 section workaround en\n";
      if (is_object(@$this->data->body)) {
        @$this->data->body->{$lang} = implode($clean_this_content_array[$lang], "\n\n");
      } else {
        $this->data->body[$lang] = implode($clean_this_content_array[$lang], "\n\n");
      }
    }
    $crawler
      ->filter('ol.breadcrumb li:last-child')
      ->each(function ($node) {
      if (is_object(@$this->data->breadcrumb)) {
        $this->data->breadcrumb->{'en'} = $node->text();
      }
      else {
        $this->data->breadcrumb['en'] = $node->text();
      }
    });
    //$client = new Client(HttpClient::create(['timeout' => 5]));

    $crawler = $client->request('GET', $this->path_to_teamsite_content($otherLang)/* . "&test=" . time()*/);
    // Go to the intranet.agr.gc.ca website in French
    $crawler->filter('meta')->each(function ($node) {
      $otherLang = 'fr';
      if (!empty($node->attr('property'))) {
        $attr = $node->attr('property');
        $attr = str_replace('aafc:', '', $attr); // Remove aafc: from property name.
        $attr = str_replace('dcterms:', '', $attr); // Remove dcterms: from property name.
        $attr = str_replace('dcterms.', '', $attr); // Remove dcterms. from property name.
        $attr = str_replace('aafc.', '', $attr); // Remove aafc: from property name.
        $attr = str_replace('news.category', 'news-category', $attr); // Remove aafc: from property name.
        $value = $node->attr('content');
        if (empty($value)) {
          $value = $node->attr('title');
        }
        if (!empty($attr)) {
          $this->data->meta[$attr][$otherLang] = $value;
        }
      }/*
      else {
        echo ' empty property Français? ' . "\n";
        echo $node->outerHtml();
        echo "\n";
      }*/
    });
    $crawler
      ->filter('body main.container section:first-child h1:first-child')
      ->each(function (Symfony\Component\DomCrawler\Crawler $crawler) {
          foreach ($crawler as $node) {
            $node->parentNode->removeChild($node);
          }
        }
      );
    $crawler
      ->filter('body main.container section')
      ->each(function ($node) {
      global $section_count_fr;
      global $section_array;
      $section_count_fr++;
      $section_array['fr'][] = $node->outerHtml();
    });
    $crawler
      ->filter('body main.container section:first-child')
      ->each(function ($node) {
      $otherLang = 'fr';
      if (is_object(@$this->data->body->{$otherLang})) {
        @$this->data->body->{$otherLang} = $node->html();
      }
      else {
        $this->data->body[$otherLang] = $node->html();
      }
    });
    $crawler
      ->filter('body main.container')
      ->each(function ($node) {
      global $clean_this_content_array;
      $clean_this_content_array['fr'][] = $this->clean_up_content($node->html());
    });
    if ($section_count_fr > 1) {
      $otherLang = 'fr';
      //echo $this->data->dcr_id . " debug1 section workaround $otherLang\n";
      if (is_object(@$this->data->body)) {
        @$this->data->body->{$otherLang} = implode($clean_this_content_array[$otherLang], "\n\n");
      } else {
        $this->data->body[$otherLang] = implode($clean_this_content_array[$otherLang], "\n\n");
	if (!isset($this->data->body[$lang])) {
          $this->data->body[$lang] = implode($clean_this_content_array[$lang], "\n\n");
        }
      }
    }
    $crawler
      ->filter('ol.breadcrumb li:last-child')
      ->each(function ($node) {
      if (is_object(@$this->data->breadcrumb)) {
        $this->data->breadcrumb->{'fr'} = $node->text();
      }
      else {
        $this->data->breadcrumb['fr'] = $node->text();
      }
    });
    if (!property_exists($this->data, 'breadcrumb')) {
      $this->data->breadcrumb['en'] = '';
      $this->data->breadcrumb['fr'] = '';
    }

    //echo print_r($this->data, TRUE) . "\n";
    $success_en = $this->processFieldsFromContent('en');
    $success_fr = $this->processFieldsFromContent('fr');
    $client = NULL;
    $node = NULL;
    $crawler = NULL;
    $section_array = array();
    $clean_this_content_array = array();
    $section_count_en = 0;
    $section_count_fr = 0;
    if ($success_en && $success_fr) {
      $this->success = TRUE;
      return TRUE;
    }
    else {
      $this->success = FALSE;
      return FALSE;
    }
//    sleep(1);
  }

  public function path_to_teamsite_content($langcode) {
    if (!isset($this->data->dcr_id) || empty($this->data->dcr_id)) {
      return ' empty dcr_id ?';
    }
    if ($langcode == 'en') {
      //return 'https://agriculture-qa.9pro.ca/en/branches-and-offices/corporate-management-branch';
      return 'https://intranet.agr.gc.ca/agrisource/eng?id=' . $this->data->dcr_id;
    } else {
      //return 'https://agriculture-qa.9pro.ca/fr/directions-generales-et-bureaux/direction-generale-de-la-gestion-integree';
      return 'https://intranet.agr.gc.ca/agrisource/fra?id=' . $this->data->dcr_id;
    }
  }


}

