<?php

class UpdateExport
{
  public $could_not_load = FALSE;
  public $pageNotFound = FALSE;
  public $success = FALSE;
  protected $node;
  protected $trnode;
  public $data;
  protected $preserveNid = false;
  protected $connection = NULL;

  function loadData($jsonfile)
  {
    $this->data = json_decode(file_get_contents($jsonfile));
    if (empty($this->data)) {
      $this->could_not_load = TRUE;
    }
  }

  function nid()
  {
    // not yet implemented.
    return null;
  }

  function id()
  {
    return $this->data->node_id;
  }

  function dcr_id()
  {
    //drush_print('debug d7nid ' . (int) $this->data->nid);
    return (int) $this->data->dcr_id;
  }

  function loadPageById() {
  }

  /**
   * Import files (images) from the Drupal 7 site
   * @param int $d7fid
   */
  function importFile($d7fid, &$d8fid=null)
  {
  }

  function updateField($field, $d8_field=null)
  {
    global $nid_old_to_new;
    global $nid_new_to_old;

    if (!$d8_field) {
      $d8_field = $field;
    }

    if($this->data->language == "en") {
      $origLang = "en";
      $oppLang = "fr";
    }
    else {
      $origLang = "fr";
      $oppLang = "en";
    }

    switch ($field) {
      case 'field_something':
        break;
      default:
	drush_print('Switch default, no action taken.');
        break;
    } //End switch

  } // End updateField function.

  function save()
  {
    $this->write_to_json();
  }

  function write_to_json() {
    if (file_exists($this->id().'.json')) {
      if ($fp = fopen($this->id().'.json', 'w')) {
        echo "Write " . $this->id() .  ".json\n";
        fwrite($fp, json_encode($this->data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        //echo "\n";
        //echo json_last_error_msg();
        //echo "\n";

        fclose($fp);
      }
    } else if (file_exists($this->dcr_id().'.json')) {
      if ($fp = fopen($this->dcr_id().'.json', 'w')) {
        echo "Write " . $this->dcr_id() .  ".json\n";
        fwrite($fp, json_encode($this->data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        //echo "\n";
        //echo json_last_error_msg();
        //echo "\n";
        fclose($fp);
      }
    } else {
      echo "file does not exist? where is it writing to? what folder is this?.\n";
    }
  }

  public function clean_up_content($html) {
    $begin_text = '<!-- Begin Content Area -->';
    $pos_begin = stripos($html, $begin_text) + strlen($begin_text);
    $pos_end = stripos($html, '<!-- End Content');
    $html_length = strlen($html);
    $html = substr($html, $pos_begin, ($pos_end-$pos_begin));
    $html = $this->auto_clean_broken_html($html);
    return $html;
  }

  /**
   * Make sure our html is not broken.
   */
  public function auto_clean_broken_html($content) {

    // Detect the string encoding
    $encoding = mb_detect_encoding($content);

    // pass it to the DOMDocument constructor
    $doc = new DOMDocument('', $encoding);

    // Must include the content-type/charset meta tag with $encoding
    // Bad HTML will trigger warnings, suppress those
    @$doc->loadHTML('<html><head>'
      . '<meta http-equiv="content-type" content="text/html; charset='
      . $encoding . '"></head><body>' . trim($content) . '</body></html>');

    // extract the components we want
    $nodes = $doc->getElementsByTagName('body')->item(0)->childNodes;
    $html = '';
    $len = $nodes->length;
    for ($i = 0; $i < $len; $i++) {
      $html .= $doc->saveHTML($nodes->item($i));
    }
    return $html;
  }

}


