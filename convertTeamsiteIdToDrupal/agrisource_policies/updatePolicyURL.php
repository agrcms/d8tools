<?php

use Drush\Drush;
use Drupal\node\Entity\Node;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

require 'db_operations.php';


global $import_override;
$import_override = TRUE;

global $policyJsonFileName;

$db = new DBOperations();

$count = 0;
$countURL = 0;
$countContent = 0;
global $countAgriDoc;
$countAgriDoc = 0;
$fileCount = 0;
$successCount = 0;
$failCount = 0;
$countOthers = 0;

global $failed_array;
$failed_array = array();

$files = glob(dirname(__FILE__) . "/export/*.json");

print_r($files);

foreach ($files as $file) {


  $json = json_decode(file_get_contents($file));
  $policyJsonFileName = dirname(__FILE__) . "/" .basename($file);
  echo  " "."\n";
  echo $policyJsonFileName . "\n";
  echo  " "."\n";
  $policyFileStr = file_get_contents($policyJsonFileName);

  foreach ($json as $data){
    $count ++;
    if($data->type == "internalContent"){
      // echo $data->url . "\n";
      $countContent ++;
      $policyFileStr = updateInternalContentURL($data, $policyFileStr);

    }elseif($data->type == "internalURL"){
      $countURL ++;
      $policyFileStr = updateInternalURL($data->url, $policyFileStr);
    }else{
      $countOthers ++;
    }

  }

  $policyFileStr = "{\"data\":" . $policyFileStr . "}";

  if ($fp = fopen($policyJsonFileName . ".bak", 'w')) {
    fwrite($fp, $policyFileStr);
    fclose($fp);
  }



}

echo "Total URL : " . $count . "\n";
echo "Total Internal URL : " . $countURL . "\n";
echo "Total Internal Content : " . $countContent . "\n";
echo "Total AgriDoc  : " . $countAgriDoc . "\n";
echo "Total Others  : " . $countOthers . "\n";


print_r($failed_array);


function updateInternalURL($url, $policyFileStr){
  global $failed_array;
  global $countAgriDoc;
  global $policyJsonFileName;

  $val = $url;
  echo $val . "\n" ;

  //skip if the link to AgriDoc
  if (stripos($val, "AgriDoc.do?") !== false) {
    echo "Agridoc ignor: " . $val . "\n" ;
    $countAgriDoc ++;
    return $policyFileStr;
  }

  if(preg_match('/id=([0-9]+)/', $val, $matchedIDs)){

      // $matchedString = $matchedIDs[0][0];
      // $position = strpos($val, "?id=", 0);
    $matchedId = $matchedIDs[1];
    echo $matchedId . "\n" ;

    //get anchor value;
    $matchedAnchor = '';
    if(preg_match('/#(.*?)\"/', $val, $match)){
      $matchedAnchor = "#" . $match[1];
    }

    $node = findDrupalNodeIdByTeamsiteId($matchedId);
    //print_r($node);

    if (!empty($node)) {
      $val = "a href=\\\"" . $val . "\\\"";
      echo "val = " . $val ."\n";
      if (stripos($policyJsonFileName, "v13_FR") !== false) {

        echo                      "a data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/fr/node/" . $node[0]->nid . $matchedAnchor . "\"" . "\n";
        $policyFileStr = str_replace($val, "a data-entity-substitution=\\\"canonical\\\" data-entity-type=\\\"node\\\" data-entity-uuid=\\\"" . $node[0]->uuid . "\\\" href=\\\"/fr/node/" . $node[0]->nid . $matchedAnchor . "\\\"", $policyFileStr);
      }else{
        echo                      "a data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\"" . "\n";
        $policyFileStr = str_replace($val, "a data-entity-substitution=\\\"canonical\\\" data-entity-type=\\\"node\\\" data-entity-uuid=\\\"" . $node[0]->uuid . "\\\" href=\\\"/node/" . $node[0]->nid . $matchedAnchor . "\\\"", $policyFileStr);
      }
    } else {
      global $nodeId;
      $failed_array[] = " dcr_id not exiting in node table = " . $matchedId ."\n";
      echo " node_id = " . $nodeId;
      echo " dcr_id not exiting in node table = " . $matchedId ."\n";
    }
  }
  return $policyFileStr;
}

function updateInternalContentURL($data, $policyFileStr){
  $oldurl = $data->url;
  if (startsWith($oldurl,"/"))
  {
    $newurl = "/sites/default/files/legacy/" . $oldurl;
  } else if (startsWith($oldurl, 'https://i')) {
    $newurl = str_replace("https://intranet.agr.gc.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'http://i')) {
    $newurl = str_replace("http://intranet.agr.gc.ca/", "/sites/default/files/legacy/", $oldurl);
  }
  $policyFileStr = str_replace($oldurl, $newurl, $policyFileStr);
  return $policyFileStr;
}

function startsWith ($string, $startString)
{
  $len = strlen($startString);
  return (substr($string, 0, $len) === $startString);
}

function findDrupalNodeIdByTeamsiteId($param) {
    $db = new DBOperations();
    $node = $db->getNodeBydcrId($param);
    //TODO
    return $node;
}

?>
