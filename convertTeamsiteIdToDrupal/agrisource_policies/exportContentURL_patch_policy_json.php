<?php
include ('../vendor/simplehtmldom/simple_html_dom.php');

//extract all content url in legacy agrisource and put into ./export folder.
//put policy json file in same folder of this php file.


$files = glob(dirname(__FILE__) . "/*.json");


foreach ($files as $file) {
  // print_r($file);
  $str = file_get_contents($file);
  // print_r($str);
  $json = json_decode($str);
  $objArray[] = (object) array();
  unset($objArray);
  // var_dump($json);
  foreach ($json as $data){
    // var_dump($data);

    // echo $data->Policy . "\n";
    $ampersandFreeString = str_replace("&", "&amp;", $data->Policy);

    $a = new SimpleXMLElement($ampersandFreeString);
    // will echo www.something.com
    $url = str_replace("&amp;","&",  $a['href']);
    echo  $a['href'] . "\n";;
    echo $url . "\n";;
    $i=0;


    $obj = new class{};

    if(startsWith($url,"http://intranet.agr.gc.ca") || startsWith($url,"https://intranet.agr.gc.ca")){
      if(startsWith($url,"http://intranet.agr.gc.ca/agrisource") || startsWith($url,"https://intranet.agr.gc.ca/agrisource")){
        //internal url
        $obj->type = "internalURL";
        $obj->url = $url;
      } elseif(startsWith($url,"http://intranet.agr.gc.ca/resources") || startsWith($url,"https://intranet.agr.gc.ca/resources")){
        $obj->type = "internalContent";
        $obj->url = $url;
      }else{
        $obj->type = "internalOther";
        $obj->url = $url;
      }


    }else{
      $obj->type = "externalOther";
      $obj->url = $url;
    }
    $objArray[] = $obj;
    // $xml = simplexml_load_string($data->Policy);
    // print_r($xml);
}
  if(! empty($objArray) && sizeof($objArray)>0){
    if ($fp = fopen(dirname(__FILE__) . "/export/" . basename($file), 'w')) {
      fwrite($fp, json_encode($objArray, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE));
      fclose($fp);
      // $jsonfileCount++;
    }
  }

}

function startsWith ($string, $startString)
{
  $len = strlen($startString);
  return (substr($string, 0, $len) === $startString);
}
// echo "Total Content links = " . $fileCount . "\n";
// echo "Total content json file created  = " . $jsonfileCount . "\n";
// echo "Total json file scaned = " . $count . "\n";

?>
