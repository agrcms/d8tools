<?php


//download all content url in legacy agrisource and put into ./exportContent folder.
//put policy json file in same folder of this php file.

$count = 0;
$fileCount = 0;
$successCount = 0;
$failCount = 0;
$countOthers = 0;

global $failed_array;
$failed_array = array();

$files = glob(dirname(__FILE__) . "/export/*.json");

print_r($files);

foreach ($files as $jfile) {

        $json = json_decode(file_get_contents($jfile));
        foreach ($json as $data){
          if($data->type == "internalContent"){
            echo $data->url . "\n";
            $count ++;
            retrieveAndSaveContent($data->url, dirname(__FILE__)."/");
          }else{
            $countOthers ++;
          }
        }

    }

  echo "Total files : " . $count . "\n";
  echo "Successfully downloaded files : " . $successCount . "\n";
  echo "Failed downloaded files : " . $failCount . "\n";
  echo "Other Type of URL : " . $countOthers . "\n";
  print_r($failed_array);


  // $myfile = file_put_contents('download_log.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);


  function retrieveAndSaveContent($url, $export_root){
    global $failed_array;
    if(startsWith($url,"/")){
      $url = "http://intranet.agr.gc.ca" . $url;
      echo $url . "\n";
    };
    if(!startsWith( $url, "http" ) )
      return;
    global $successCount,$failCount;
    $parsedURL = parse_url ($url);
    echo $parsedURL['path'] . "\n";
    $path = str_replace("//", "/", $parsedURL['path']);
    $path = $export_root . "content" . $path;
    $path = urldecode($path);
    echo $path . "\n";
    $dir_to_save = dirname($path);
    echo $dir_to_save . "\n";
    if (!is_dir($dir_to_save)) {
      mkdir($dir_to_save, 0777, true);
    }
    if(file_put_contents( $path,file_get_contents($url))) {
      // echo "File downloaded successfully";
      $successCount++;
    }
    else {
        // echo "File downloading failed.";
        $failed_array[] = $url;
        $failCount++;
    }
    //file_get_contents($url);
  }

  function startsWith ($string, $startString)
{
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}

?>
