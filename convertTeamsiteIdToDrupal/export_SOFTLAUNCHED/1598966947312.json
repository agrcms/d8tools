{
    "dcr_id": "1598966947312",
    "lang": "en",
    "title": {
        "en": "We Are Listening And We Are Learning: a Conversation About Racism",
        "fr": "\u00c0 l'\u00e9coute et pr\u00eats \u00e0 apprendre : un dialogue sur le racisme"
    },
    "modified": "2020-09-03 00:00:00.0",
    "issued": "2020-09-03 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2020-09-01",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2020-09-03",
            "fr": "2020-09-03"
        },
        "modified": {
            "en": "2020-09-03",
            "fr": "2020-09-03"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "We Are Listening And We Are Learning: a Conversation About Racism",
            "fr": "\u00c0 l'\u00e9coute et pr\u00eats \u00e0 apprendre : un dialogue sur le racisme"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Recent events in Canada and around the world have served as a reminder that racism is not a new issue.",
            "fr": "Les \u00e9v\u00e9nements r\u00e9cents qui se sont d\u00e9roul\u00e9s au Canada et partout dans le monde nous ont rappel\u00e9 que le racisme, ce n\u2019est rien de nouveau."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>Recent events in Canada and around the world have served as a reminder that racism is not a new issue. As Co-Champions of Diversity and Inclusion at AAFC, we are fortunate that we can use our voice to continue these sometimes difficult and emotional conversations, while ensuring AAFC continues to grow as an inclusive, respectful and diverse workplace. We are listening and we are learning.</p>\n<p>We know that we are not exempt from racism here at AAFC. Some employees continue to be subject to racism and discrimination. This is not acceptable and we all have a role to play in ending discrimination of any kind: Whether it\u2019s to better understand and overcome our unconscious biases, to take concrete action to promote a respectful and inclusive workplace, or simply to help support a friend or loved one through difficult times.</p>\n<h2><strong>Be part of the solution: show your commitment by becoming involved </strong></h2>\n<ul>\n<li>Participate in AAFC\u2019s diversity initiatives and/or join one of the five <a href=\"https://collab.agr.gc.ca/co/eedi-emedi/SitePages/Diversity%20Networks.aspx\">AAFC Diversity Networks</a>. Contact Diversity and Inclusiveness (aafc.inclusive-inclusif.aac@canada.ca) to get more information.</li>\n<li>We created a new diversity and inclusion look in support of our commitment to stop racism and discrimination. Use <a href=\"https://intranet.agr.gc.ca/resources/prod/news/images/VirtualBackgrounder-diversity.jpg\">this look</a> (JPG) as your virtual background for your MS Team calls to show that you take diversity and inclusion to heart.</li>\n<li>Join us on social media, make your voices heard and show solidarity! Let\u2019s support each other!\u00a0#Diversity <img src=\"https://statics.teams.cdn.office.net/evergreen-assets/skype/v2/heart/20.png?v=4\" alt=\"https://statics.teams.cdn.office.net/evergreen-assets/skype/v2/heart/20.png?v=4\" style=\"height: 20px; width: 20px;\">AAFC #GCAggie</li>\n</ul>\n<h2>We also hope you take the time to consult some of the resources available to you</h2>\n<ul>\n<li>The <a href=\"https://www.workhealthlife.com/?lang=en-CA\">Employee and Family Assistance Program</a>, for confidential counselling, expert advice, support, and resource material to those experiencing personal or work-related events.</li>\n<li>AAFC\u2019s Workplace Well-Being Ombudsperson (<a href=\"mailto:AAFC.Ombuds.AAC@canada.ca\">AAFC.Ombuds.AAC@canada.ca</a>), for a safe environment where employees at all levels can raise, discuss and address work-related issues.</li>\n<li>The Harassment and Violence in the Workplace Prevention Program (<a href=\"mailto:aafc.harassment-violence-harcelement.aac@canada.ca\">aafc.harassment-violence-harcelement.aac@canada.ca</a>), for information and guidance on harassment prevention</li>\n<li>The <a href=\"https://ccdi.ca/\">Canadian Centre for Diversity and Inclusion (CCDI)</a> for access to various anti-racism resources including live and previously recorded webinars, podcasts, books and much more.</li>\n<li>The Canada School of Public Service for diversity and inclusion online training, including:\n\t\n<ul>\n<li>\n\t<a href=\"https://www.csps-efpc.gc.ca/Catalogue/courses-eng.aspx?code=W005\">Understanding Unconscious Bias (W005)</a>\u00a0\u00a0</li>\n<li><a href=\"https://www.csps-efpc.gc.ca/Catalogue/courses-eng.aspx?code=W006\">Overcoming Your Own Unconscious Biases (W006)</a>\u00a0</li>\n<li><a href=\"https://www.csps-efpc.gc.ca/Catalogue/courses-eng.aspx?code=K099\">Cultural Self-Reflection: What I Know and What I Don\u2019t know (K099)</a>\u00a0</li>\n<li><a href=\"https://www.youtube.com/watch?v=wq3rwoWT85w\">Understanding Anti-Black Racism and How to Be an Ally Workshop</a> (webcast available on CSPS YouTube)</li>\n</ul>\n</li>\n<li>Canadian Heritage published <a href=\"https://www.canada.ca/en/canadian-heritage/campaigns/anti-racism-engagement.html\">Canada\u2019s Anti-Racism Strategy</a> which includes immediate steps taken to combat racism and discrimination. It puts forth the value of diversity and encourages inclusion so every Canadian feels safe to be who they are. These same values should be echoed within the public service: everyone has the right to work in an environment free of racism and discrimination. Many <a href=\"https://intranet.canada.ca/hr-rh/ve/dee-deme/index-eng.asp\">resources on inclusion, diversity and employment equity</a> are available and we encourage you to read and use them.\t</li>\n<li>The <a href=\"https://fsl-bsf.scitech.gc.ca/eng/intranet/home/\">Canadian Agriculture Library</a>, where you can search for resources (e.g., ebooks, ejournals, reports) related to diversity, including those on <a href=\"https://collab.agr.gc.ca/co/kis-sis/SitePages/Indigenous-related%20Information.aspx\">Indigenous culture and history</a>.</li>\n</ul>\n<h2><strong>Lots of work remains </strong></h2>\n<p>In working towards a culture that fully embraces inclusiveness and is representative of the Canadian public that it serves, the Deputy Minister and Associate Deputy Minister recently met with the Co-Chairs of AAFC\u2019s Diversity and Inclusion Networks to start a dialogue on discrimination in the workplace. As next steps, the Department will also:</p>\n<ul>\n<li>Hold engagement sessions with each Diversity and Inclusion Network and engage all branches to identify further specific actions to take</li>\n<li>Implement targeted recruitment initiatives to increase representation of minority groups at AAFC</li>\n<li>Develop tools to minimize unconscious bias in staffing processes</li>\n<li>Expand the suite of diversity and inclusion learning to include offerings on anti-discrimination.</li>\n</ul>\n<p>We are convinced that by working together and learning from one another, we can make great strides in creating a discrimination-free workplace. Let\u2019s listen to each other, open our minds and, above all, accept everyone just as they are.</p>\n<p><strong>\u00a0</strong></p>\n<p><strong>Christine Walker and Michel Lessard</strong><br>\nAAFC Diversity and Inclusion Co-Champions</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Les \u00e9v\u00e9nements r\u00e9cents qui se sont d\u00e9roul\u00e9s au Canada et partout dans le monde nous ont rappel\u00e9 que le racisme, ce n\u2019est rien de nouveau. \u00c0 titre de cochampions de la diversit\u00e9 et de l\u2019inclusion \u00e0 AAC, nous avons la chance de pouvoir utiliser nos voix pour poursuivre ces discussions parfois difficiles et \u00e9motives, tout en nous assurant qu\u2019AAC continue de cro\u00eetre en tant que milieu de travail inclusif, respectueux et diversifi\u00e9. Nous sommes \u00e0 l\u2019\u00e9coute et nous sommes pr\u00eats \u00e0 apprendre.</p>\n<p>Nous savons que nous ne sommes pas exempts de tout racisme, ici \u00e0 AAC. Certains employ\u00e9s continuent de faire l\u2019objet de racisme et de discrimination, ce qui est inacceptable. Nous avons tous un r\u00f4le \u00e0 jouer pour mettre fin \u00e0 la discrimination de toute sorte. Que ce soit pour mieux comprendre et surmonter nos partis pris inconscients, pour prendre action en vue de promouvoir un milieu de travail respectueux et inclusif, ou simplement pour appuyer un ami ou un \u00eatre cher qui traverse une p\u00e9riode difficile.</p>\n<h2><strong>Faites partie de la solution : montrez votre engagement en participant\u00a0</strong><strong> </strong></h2>\n<ul>\n<li>Joignez-vous aux initiatives de diversit\u00e9 d\u2019AAC ou \u00e0 l\u2019un des cinq <a href=\"https://collab.agr.gc.ca/co/eedi-emedi/SitePages/Diversity%20Networks.aspx\">r\u00e9seaux de la diversit\u00e9 d\u2019AAC</a>. Pour obtenir de plus amples renseignements, communiquez avec Diversit\u00e9 et Inclusivit\u00e9 (<a href=\"mailto:aafc.inclusive-inclusif.aac@canada.ca\">aafc.inclusive-inclusif.aac@canada.ca</a>).</li>\n<li>Nous avons cr\u00e9\u00e9 une nouvelle image pour la diversit\u00e9 et l\u2019inclusivit\u00e9 afin d\u2019appuyer notre engagement \u00e0 freiner le racisme et la discrimination. Utilisez <a href=\"https://intranet.agr.gc.ca/resources/prod/news/images/VirtualBackgrounder-diversity.jpg\">cette image</a> (JPG) comme arri\u00e8re-plan virtuel lors de vos appels MS Teams pour montrer que vous tenez \u00e0 la diversit\u00e9 et \u00e0 l\u2019inclusion.</li>\n<li>Joignez-vous \u00e0 nous dans les r\u00e9seaux sociaux, faites-vous entendre et faites preuve de solidarit\u00e9! Soutenons-nous mutuellement! <em>#Diversit\u00e9 <img src=\"https://statics.teams.cdn.office.net/evergreen-assets/skype/v2/heart/20.png?v=4\" alt=\"https://statics.teams.cdn.office.net/evergreen-assets/skype/v2/heart/20.png?v=4\" style=\"height: 20px; width: 20px;\">AAC #AgromaneGC</em></li>\n</ul>\n<h2><strong>Nous esp\u00e9rons \u00e9galement que vous prendrez le temps de consulter quelques\u2011unes des nombreuses ressources qui sont mises \u00e0 votre disposition </strong></h2>\n<ul>\n<li>Le <a href=\"https://www.workhealthlife.com/?lang=fr-CA\">Programme aux employ\u00e9s et \u00e0 la famille</a>, pour obtenir des conseils confidentiels, des conseils d\u2019experts, du soutien et des ressources offerts aux employ\u00e9s et aux membres de leur famille qui \u00e9prouvent des probl\u00e8mes de nature personnelle ou professionnelle.</li>\n<li>L\u2019Ombudsman (AAFC.Ombuds.AAC@canada.ca), du mieux-\u00eatre en milieu de travail \u00e0 AAC, pour un environnement s\u00fbr, o\u00f9 les employ\u00e9s de tous les niveaux peuvent soulever, discuter et r\u00e9soudre les probl\u00e8mes li\u00e9s au travail.</li>\n<li>Le Programme de la pr\u00e9vention du harc\u00e8lement et de la violence en milieu de travail (aafc.harassment-violence-harcelement.aac@canada.ca), pour des renseignements et de l\u2019orientation sur la pr\u00e9vention du harc\u00e8lement.</li>\n<li>Le Centre canadien pour la diversit\u00e9 et l\u2019inclusion (CCDI)(<a href=\"https://ccdi.ca/\">https://ccdi.ca</a>), pour acc\u00e9der \u00e0 diverses ressources anti-racisme, y compris des webinaires en direct et pr\u00e9enregistr\u00e9s, des balados, des livres et plus encore.</li>\n<li>L\u2019\u00c9cole de la fonction publique du Canada, pour des formations sur la diversit\u00e9 et l\u2019inclusion, y compris les suivantes :\n<ul>\n<li>\u00a0<a href=\"https://www.csps-efpc.gc.ca/Catalogue/courses-fra.aspx?code=W005\">Compr\u00e9hension des pr\u00e9jug\u00e9s inconscients (W005)</a></li>\n<li><a href=\"https://www.csps-efpc.gc.ca/Catalogue/courses-fra.aspx?code=W006\">Surmonter vos propres pr\u00e9jug\u00e9s inconscients (W006)</a>\u00a0</li>\n<li><a href=\"https://www.csps-efpc.gc.ca/Catalogue/courses-fra.aspx?code=K099\">Introspection culturelle : ce que je sais et ce que j\u2019ignore (K099)</a></li>\n<li><a href=\"https://www.youtube.com/watch?v=wq3rwoWT85w\">Comprendre le racisme envers les Noirs, et comment devenir un alli\u00e9</a> (webdiffusion offerte sur la page YouTube de l\u2019EFPC)</li>\n</ul>\n</li>\n<li>\n\t\n\tLe minist\u00e8re du Patrimoine canadien a publi\u00e9 <a href=\"https://www.canada.ca/fr/patrimoine-canadien/campagnes/mobilisation-contre-racisme.html\">la strat\u00e9gie canadienne de lutte contre le racisme</a>, qui comprend des \u00e9tapes \u00e0 suivre pour lutter contre le racisme et la discrimination. On y met l\u2019accent sur la valeur de la diversit\u00e9 et on encourage l\u2019inclusion, de sorte que tous les Canadiens et les Canadiennes se sentent en s\u00e9curit\u00e9 et libres d\u2019\u00eatre ce qu\u2019ils sont. Ces m\u00eames valeurs doivent trouver \u00e9cho au sein de la fonction publique : tout le monde a le droit de travailler dans un milieu exempt de racisme et de discrimination. De nombreuses <a href=\"https://intranet.canada.ca/hr-rh/ve/dee-deme/index-fra.asp\">ressources sur l\u2019inclusion, la diversit\u00e9 et l\u2019\u00e9quit\u00e9 en mati\u00e8re d\u2019emploi</a> sont disponibles, et nous vous encourageons \u00e0 les consulter et \u00e0 les utiliser.</li>\n<li>La <a href=\"https://fsl-bsf.scitech.gc.ca/fra/intranet/home/\">Biblioth\u00e8que canadienne de l\u2019agriculture</a>, o\u00f9 vous pouvez trouver des ressources (p. ex. des livres, des revues \u00e9lectroniques, des rapports) en lien avec la diversit\u00e9, y compris celles qui touchent \u00e0 l\u2019<a href=\"https://collab.agr.gc.ca/co/kis-sis/SitePages/Indigenous-related%20Information.aspx\">histoire et la culture autochtones</a>.</li>\n</ul>\n<h2><strong>Il reste beaucoup \u00e0 faire </strong></h2>\n<p>Dans le but de cr\u00e9er une culture qui favorise l\u2019inclusion et qui est repr\u00e9sentative de la population canadienne, le sous-ministre et la sous-ministre d\u00e9l\u00e9gu\u00e9e ont r\u00e9cemment rencontr\u00e9 les copr\u00e9sidents des r\u00e9seaux de la diversit\u00e9 et de l\u2019inclusivit\u00e9 d\u2019AAC pour entamer un dialogue sur la discrimination au travail. Le Minist\u00e8re prendra \u00e9galement les mesures suivantes :</p>\n<ul>\n<li>Organiser des s\u00e9ances de mobilisation avec chaque r\u00e9seau de la diversit\u00e9 et de l\u2019inclusion et inciter toutes les directions g\u00e9n\u00e9rales \u00e0 d\u00e9finir d\u2019autres mesures particuli\u00e8res \u00e0 prendre</li>\n<li>Mettre en \u0153uvre des initiatives de recrutement cibl\u00e9 pour accro\u00eetre la repr\u00e9sentation des groupes minoritaires \u00e0 AAC</li>\n<li>Cr\u00e9er des outils pour r\u00e9duire les partis pris inconscients dans les processus de recrutement</li>\n<li>\u00c9largir la gamme d\u2019apprentissages en mati\u00e8re de diversit\u00e9 et d\u2019inclusion pour y inclure des formations sur la lutte contre la discrimination</li>\n</ul>\n<p>Nous sommes persuad\u00e9s qu\u2019en travaillant ensemble et en apprenant aupr\u00e8s des uns et des autres, nous pouvons faire de grands progr\u00e8s pour cr\u00e9er un milieu de travail exempt de discrimination. Soyons \u00e0 l\u2019\u00e9coute les uns des autres, ouvrons nos oreilles et nos esprits et, par-dessus tout, acceptons tout le monde tel qu\u2019il est.</p>\n<p>\u00a0</p>\n<p><strong>Christine Walker et Michel Lessard</strong><br>\nCochampions de la diversit\u00e9 et de l\u2019inclusion d\u2019AAC</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "We Are Listening And We Are Learning: a Conversation About Racism",
        "fr": "\u00c0 l'\u00e9coute et pr\u00eats \u00e0 apprendre : un dialogue sur le racisme"
    },
    "news": {
        "date_posted": {
            "en": "2020-09-03",
            "fr": "2020-09-03"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "AAFC Diversity and Inclusion Co-Champions Christine Walker and Michel Lessard",
            "fr": "Cochampions de la diversit\u00e9 et de l\u2019inclusion d\u2019AAC Christine Walker et Michel Lessard"
        }
    }
}