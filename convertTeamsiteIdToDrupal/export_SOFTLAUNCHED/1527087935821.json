{
    "dcr_id": "1527087935821",
    "lang": "en",
    "title": {
        "en": "Special Canadian Space Agency Presentation - Space: From Exploration to Agriculture Applications",
        "fr": "Pr\u00e9sentation sp\u00e9ciale de l'Agence spatiale canadienne - L'espace : De l'exploration aux applications en agriculture"
    },
    "modified": "2018-05-24 00:00:00.0",
    "issued": "2018-05-23 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2018-05-23",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-05-23",
            "fr": "2018-05-23"
        },
        "modified": {
            "en": "2018-05-24",
            "fr": "2018-05-23"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Special Canadian Space Agency Presentation - Space: From Exploration to Agriculture Applications",
            "fr": "Pr\u00e9sentation sp\u00e9ciale de l'Agence spatiale canadienne - L'espace : De l'exploration aux applications en agriculture"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Join Colonel Jeremy Hansen of the Canadian Space Agency as we examine his journey as an astronaut and the future of space exploration from an agricultural perspective.",
            "fr": "Nous vous invitons \u00e0 assister \u00e0 la pr\u00e9sentation du colonel Jeremy Hansen de l'Agence spatiale canadienne pour conna\u00eetre son parcours comme astronaute et examiner l'avenir de l'exploration spatiale du point de vue de l'agriculture."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>Our planet and the environment surrounding us benefit from space research and development. Be it to improve soil use, reduce pollution or guarantee better access to drinking water, space technologies and satellite data play an essential role in the daily lives of Canadians.</p>\n<p>Join Colonel Jeremy Hansen of the Canadian Space Agency as we examine his journey as an astronaut and the future of space exploration from an agricultural perspective.</p>\n<p>After his presentation, there will be a 30-minute moderated question and answer period. To ensure that as many questions are answered as possible, videoconference participants are asked to submit their queries via email to <a href=\"mailto:vincent.beaulieu@agr.gc.ca\">vincent.beaulieu@agr.gc.ca</a>. Please ensure you gather with colleagues who have access to email during the presentation.</p>\n<p>We encourage you to block off the time in your Outlook calendar.</p>\n<p>We look forward to your participation at this AAFC event.</p>\n<h2><strong>Event information</strong></h2>\n<p><strong>Date and Time:</strong> May 28, 2018 | 1:00 to 2:00 p.m. (ET)<br><strong>Language:</strong> Bilingual<br><strong>Location:</strong> Ottawa - NHCAP T6 Cafeteria | Available across Canada via videoconference<br><strong>Videoconference:</strong> 6610919365</p>\n<p>To maximize AAFC\u2019s videoconferencing capacity, we ask that individuals refrain from connecting via their own PC. Staff are encouraged to gather with colleagues where possible to make use of boardrooms with videoconferencing facilities. <strong>Centres and teams are asked to please coordinate room bookings.</strong></p>\n<p>If you experience difficulties in connecting, please contact the IT Centre at 1-855-545-4411<a href=\"#\" style=\"margin: 0px; border: currentColor; left: 0px; top: 0px; width: 16px; height: 16px; right: 0px; bottom: 0px; overflow: hidden; vertical-align: middle; float: none; display: inline; white-space: nowrap; position: static !important;\" title=\"Call: 1-855-545-4411\"></a> or <a href=\"mailto:service@itcentreti.collaboration.gc.ca\">service@itcentreti.collaboration.gc.ca</a>.</p>\n<p>Please note that to avoid accidental noise, all lines will be muted.</p>\n<h2><strong>Biography: Colonel Jeremy R. Hansen</strong></h2>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/CSAJeremy-Hansen.png\" class=\"pull-left mrgn-rght-md\" alt=\"Col. Jeremy Hansen\">Colonel Jeremy Hansen was one of two recruits selected by the Canadian Space Agency (<abbr title=\"Canadian Space Agency\">CSA</abbr>) in May 2009 through the third Canadian Astronaut Recruitment Campaign. He is one of 14 members of the 20<sup>th</sup> National Aeronautics and Space Administration (<abbr title=\"National Aeronautics and Space Administration\">NASA</abbr>) astronaut class. In 2011, Colonel Hansen graduated from Astronaut Candidate Training, which included scientific and technical briefings, intensive instruction in International Space Station (<abbr title=\"International Space Station\">ISS</abbr>) systems, Extravehicular Activities (<abbr title=\"Extravehicular Activities\">EVAs</abbr>, or spacewalks), robotics, physiological training, T-38 flight training, Russian language courses, and sea and wilderness survival training.</p>\n<p>While waiting for a flight assignment, Colonel Hansen represents <abbr title=\"Canadian Space Agency\">CSA</abbr> at <abbr title=\"National Aeronautics and Space Administration\">NASA</abbr>, and works at the Mission Control Center as Capcom\u2014the voice between the ground and the <abbr title=\"International Space Station\">ISS</abbr>.</p>\n<p>Learn more about <a rel=\"external\" href=\"http://asc-csa.gc.ca/eng/astronauts/canadian/active/bio-jeremy-hansen.asp\">Colonel Jeremy Hansen</a>.</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Notre plan\u00e8te et l'environnement qui nous entoure profitent des travaux de recherche et d\u00e9veloppement men\u00e9s dans l\u2019espace. Que ce soit dans le but d'am\u00e9liorer l'exploitation des sols, de r\u00e9duire la pollution ou encore de garantir un meilleur acc\u00e8s \u00e0 l'eau potable; les technologies spatiales et les donn\u00e9es satellitaires jouent un r\u00f4le essentiel dans le quotidien des Canadiens.</p>\n<p>Nous vous invitons \u00e0 assister \u00e0 la pr\u00e9sentation du colonel Jeremy Hansen de l\u2019Agence spatiale canadienne pour conna\u00eetre son parcours comme astronaute et examiner l\u2019avenir de l\u2019exploration spatiale du point de vue de l\u2019agriculture.</p>\n<p>Apr\u00e8s la pr\u00e9sentation, il y aura une p\u00e9riode de questions de 30 minutes anim\u00e9e par un mod\u00e9rateur. Pour que le colonel Hansen puisse r\u00e9pondre au plus grand nombre de questions possible, nous demandons aux participants par vid\u00e9oconf\u00e9rence de soumettre leurs questions par courriel \u00e0 l\u2019adresse <a href=\"mailto:vincent.beaulieu@agr.gc.ca\">vincent.beaulieu@agr.gc.ca</a>. Assurez-vous donc d\u2019avoir dans votre groupe des coll\u00e8gues qui ont acc\u00e8s au courriel.</p>\n<p>Nous vous encourageons \u00e0 r\u00e9server la p\u00e9riode vis\u00e9e dans votre calendrier Outlook.</p>\n<p>Nous esp\u00e9rons vous compter parmi les participants \u00e0 cette activit\u00e9 d\u2019AAC.</p>\n<h2><strong>Renseignements sur l\u2019\u00e9v\u00e9nement</strong></h2>\n<p><strong>Date et heure :</strong> Le 28 mai 2018 | De 13 h \u00e0 14 h (HE)<br><strong>Langues :</strong> Bilingue<br><strong>Lieu :</strong> Ottawa - CACPA - Caf\u00e9teria de la tour 6 | Offert partout au Canada par vid\u00e9oconf\u00e9rence<br><strong>Vid\u00e9oconf\u00e9rence :</strong> 6610919365</p>\n<p>Afin de maximiser les services de vid\u00e9oconf\u00e9rence, nous vous demandons d\u2019\u00e9viter de vous connecter par l\u2019interm\u00e9diaire de votre ordinateur. Nous encourageons les employ\u00e9s \u00e0 se regrouper, si possible, dans des salles munies d\u2019installations pour vid\u00e9oconf\u00e9rence. <strong>Les \u00e9quipes et les centres sont pri\u00e9s de coordonner la r\u00e9servation des salles.</strong></p>\n<p>Si vous \u00e9prouvez des difficult\u00e9s, veuillez communiquer avec le Centre de TI par t\u00e9l\u00e9phone (1\u2011855\u2011545\u20114411) ou par courriel (<a href=\"mailto:service@itcentreti.collaboration.gc.ca\">service@itcentreti.collaboration.gc.ca</a>).</p>\n<p>Veuillez noter que pour \u00e9viter les bruits accidentels, toutes les lignes seront mises en sourdine.</p>\n<h2><strong>Biographie du colonel Jeremy R. Hansen</strong></h2>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/CSAJeremy-Hansen.png\" class=\"pull-left mrgn-rght-md\" alt=\"Le colonel Jeremy Hansen\">Le colonel Hansen est l'une des deux recrues s\u00e9lectionn\u00e9es en mai 2009 par l'Agence spatiale canadienne (<abbr title=\"Agence spatiale canadienne\">ASC</abbr>) \u00e0 l'issue de la troisi\u00e8me campagne de recrutement d'astronautes canadiens. Il est l'un des 14 membres de la 20<sup>e</sup> classe d'astronautes de la National Aeronautics and Space Administration (<abbr title=\"National Aeronautics and Space Administration\">NASA</abbr>). En 2011, il a termin\u00e9 la formation de candidat astronaute qui comprend des s\u00e9ances d'information scientifique et technique, de la formation intensive sur les syst\u00e8mes de la Station spatiale internationale, des activit\u00e9s extrav\u00e9hiculaires (sorties dans l'espace), de la robotique, de l'entra\u00eenement physiologique, des vols en T-38, l'apprentissage du russe, et des entra\u00eenements de survie en mer et en milieu sauvage.</p>\n<p>En attendant d'\u00eatre affect\u00e9 \u00e0 un vol, le colonel Hansen repr\u00e9sente l'<abbr title=\"Agence spatiale canadienne\">ASC</abbr> \u00e0 la <abbr title=\"National Aeronautics and Space Administration\">NASA</abbr> et travaille comme capcom au centre de contr\u00f4le de mission, c'est-\u00e0-dire celui qui parle avec l'\u00e9quipage de la Station spatiale internationale.</p>\n<p>Pour en savoir plus sur le <a rel=\"external\" href=\"http://asc-csa.gc.ca/fra/astronautes/canadiens/actifs/bio-jeremy-hansen.asp\">colonel Jeremy Hansen</a>.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Special Canadian Space Agency Presentation - Space: From Exploration to Agriculture Applications",
        "fr": "Pr\u00e9sentation sp\u00e9ciale de l'Agence spatiale canadienne - L'espace : De l'exploration aux applications"
    },
    "news": {
        "date_posted": {
            "en": "2018-05-23",
            "fr": "2018-05-23"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Brian Gray, Assistant Deputy Minister, Science and Technology, and Steve Jurgutis, A/Assistant Deputy Minister, Public Affairs",
            "fr": "Brian Gray, Sous-ministre adjoint, Science et technologie, et Steve Jurgutis, Sous-ministre adjoint par int\u00e9rim, Affaires publiques"
        }
    }
}