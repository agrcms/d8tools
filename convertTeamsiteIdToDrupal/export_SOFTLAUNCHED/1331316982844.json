{
    "dcr_id": "1331316982844",
    "lang": "en",
    "title": {
        "en": "Abolish (Delete) a Position",
        "fr": "Abolir (supprimer) un poste"
    },
    "modified": "2019-12-04 00:00:00.0",
    "issued": "2012-03-20 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1403608870102",
    "layout_name": "1 column",
    "dc_date_created": "2012-03-09",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2012-03-20",
            "fr": "2012-03-20"
        },
        "modified": {
            "en": "2019-12-04",
            "fr": "2019-12-04"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Abolish (Delete) a Position",
            "fr": "Abolir (supprimer) un poste"
        },
        "subject": {
            "en": "staffing",
            "fr": "dotation en personnel"
        },
        "description": {
            "en": "Abolish a position when it is no longer up-to-date or required.",
            "fr": "Abolir un poste lorsqu'il n'est plus \u00e0 jour ou n'est plus n\u00e9cessaire."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<!--\n<div class=\"alert alert-warning\">\n   <p>Please note that the HR service request forms will not be available on Wednesday, December&nbsp;4<sup>th</sup> from 6:00&nbsp;a.m. to 9:00&nbsp;a.m&nbsp;EST due to server updates.</p>\n</div>-->\n\n<p>Managers are responsible for abolishing positions which are no longer required and vacant to provide a clear and current picture of the organization.</p>\n\n<p>If a position has been vacant for more than 5 years, it must be abolished as per the Treasury Board of Canada Secretariat <a href=\"http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=28700\">Directive on Classification</a>, Appendix\u00a0B. Organizations should strive for a position vacancy rate between 10 and 15 per cent. Agriculture and Agri-Food Canada will conduct annual exercises to ensure positions will be abolished if they are no longer required. \n</p>\n\n<p>A position against which there is an active grievance cannot be abolished until the issues have been resolved.  The Classification team will monitor the status of these positions and abolish them when possible and, in the meantime, these positions should not be staffed.</p>\n\n<p><b>Notes:</b></p>\n\n<ul>\n <li>A temporarily vacant position due to a leave of absence cannot be abolished until the employee is removed from the position. The incumbent continues to occupy the position until they return to work, accept employment elsewhere or leave the Department.</li> \n<li>A position which has subordinates cannot be abolished until the reporting structure is changed. See <a href=\"display-afficher.do?id=1331318118197&amp;lang=eng\">Change Reporting Relationship and/or Location</a>.</li>\n</ul> \n\n<h2>Process</h2>\n\n<h3>Step 1: Gather the information you need</h3>\n\n<p>To process a deletion, please ensure that you have the following information ready:</p>\n\n<ol class=\"lst-lwr-alph\">\n <li>Position number</li> \n <li>Position's classification</li> \n <li>Branch</li> \n <li>Division</li> \n <li>Effective date when the position is to be deleted</li> \n <li>Supervisor's name</li> \n <li>Supervisor's position number</li>\n</ol> \n\n<h2>Step 2: Submit your request</h2>\n\n<p><a href=\"http://hri-irh.agr.gc.ca:8080/elcf-fcve/F100-1.aspx?lang=eng\">Submit your request using the Delete a Position Online Form</a></p> \n\n<p>You will receive an e-mail once your request has been submitted.</p>\n \n<h2>Service standard</h2>\n\n<p>10-20 working days</p>\n\n<p><strong>Note:</strong> Specific timelines can be provided after initial discussions with your <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=eng\" target=\"_blank\">Classification Advisor (Word)</a>.</p>\n\n<h2>Contact us</h2>\n\n<p>Questions? We can help. Contact any member of the Classification team (see <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=eng\" target=\"_blank\">Classification Advisor (Word)</a>)  or email <a href=\"mailto:aafc.classification.aac@canada.ca\">aafc.classification.aac@canada.ca</a>.</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<!--\n<div class=\"alert alert-warning\">\n   <p>Veuillez noter que les formulaires de service RH ne seront pas disponible entre 6h00 et 9h00&nbsp;HNE le mercredi&nbsp;4&nbsp;d&eacute;cembre&nbsp;2019 afin de proc&eacute;der a une mise-&agrave;-jour du serveur.</p>\n</div>-->\n\n<p>Il incombe aux gestionnaires d'abolir les postes qui ne sont plus requis et vacants afin d'\u00e9tablir un portrait clair et actuel de l'organisation.</p>\n\n<p>Les postes vacants depuis plus de cinq ans doivent \u00eatre abolis, conform\u00e9ment \u00e0 l'annexe\u00a0B de la <a href=\"http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=28700\">Directive sur la classification</a>  du Secr\u00e9tariat du Conseil du Tr\u00e9sor. Les organisations devraient faire en sorte que le taux de postes vacants corresponde \u00e0 entre 10 et 15\u00a0% de leur effectif. Agriculture et Agroalimentaire Canada effectuera des exercices annuels en vue de s'assurer que les postes qui ne sont plus requis sont abolis. </p>\n\n<p>Un poste qui fait l'objet d'un grief actif ne peut pas \u00eatre aboli tant que les probl\u00e8mes n\u2019ont  pas \u00e9t\u00e9 r\u00e9solus. L'\u00e9quipe charg\u00e9e de la classification surveillera l'\u00e9tat de ces postes et les abolira d\u00e8s que possible. Entre-temps, ces postes ne devraient pas \u00eatre dot\u00e9s.</p>\n\n<p><b>Remarques :</b></p>\n\n<ul>\n<li>Un poste  temporairement vacant en raison d'une absence autoris\u00e9e ne peut pas \u00eatre aboli tant que l'employ\u00e9 n'est pas retir\u00e9 de ce poste. Le titulaire continue d'occuper le poste jusqu'\u00e0 ce qu'il retourne au travail, accepte un emploi ailleurs ou quitte le Minist\u00e8re.</li>\n<li>Un poste duquel rel\u00e8vent des subordonn\u00e9s ne peut pas \u00eatre aboli tant que la structure des liens hi\u00e9rarchiques n\u2019est pas modifi\u00e9e. Voir Modifier le rapport hi\u00e9rarchique ou l'emplacement. Voir \n <a href=\"display-afficher.do?id=1331318118197&amp;lang=fra\">Modifier le rapport hi\u00e9rarchique ou l'emplacement</a>.</li>\n</ul> \n\n<h2>Processus</h2>\n\n<h3>\u00c9tape\u00a01\u00a0: Recueillir les renseignements dont vous avez besoin</h3>\n\n<p>Afin de traiter une suppression, veuillez vous assurer que vous disposez des renseignements suivants\u00a0:</p>\n\n<ol class=\"lst-lwr-alph\">\n <li>Num\u00e9ro du poste</li> \n <li>Classification du poste</li> \n <li>Direction g\u00e9n\u00e9rale</li> \n <li>Division</li> \n <li>Date d'entr\u00e9e en vigueur de la suppression du poste</li> \n <li>Nom du superviseur</li> \n <li>Num\u00e9ro de poste du superviseur</li>\n</ol>\n\n<h2>\u00c9tape\u00a02\u00a0: Soumettre la demande</h2>\n\n<p><a href=\"http://hri-irh.agr.gc.ca:8080/elcf-fcve/F100-1.aspx?lang=fra\">Soumettez votre demande \u00e0 l'aide du formulaire en ligne de suppression d'un poste</a>.</p> \n\n<p>Vous recevrez un courriel une fois que votre demande aura \u00e9t\u00e9 soumise.</p>\n \n<h2>Norme de service</h2>\n\n<p>De dix \u00e0 vingt jours ouvrables</p>\n\n<p><strong>Remarque\u00a0:</strong> Des calendriers pr\u00e9cis peuvent \u00eatre \u00e9tablis apr\u00e8s les discussions initiales avec votre <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=fra\" target=\"_blank\">conseiller en classification (Word)</a>.</p>\n\n<h2>Contactez-nous</h2>\n\n<p>Des questions? Nous pouvons vous aider. Contactez un membre de l'\u00e9quipe de classification  (voir la liste <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=fra\" target=\"_blank\">conseiller en classification (Word)</a>) ou envoyez un courriel \u00e0 <a href=\"mailto:aafc.classification.aac@canada.ca\">aafc.classification.aac@canada.ca</a>.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Abolish (Delete) a Position",
        "fr": "Abolir (supprimer) un poste"
    }
}