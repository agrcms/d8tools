{
    "dcr_id": "1560353159759",
    "lang": "en",
    "title": {
        "en": "Retirement",
        "fr": "D\u00e9part \u00e0 la retraite"
    },
    "modified": "2019-07-16 00:00:00.0",
    "issued": "2019-07-16 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279035583756",
    "layout_name": "1 column",
    "dc_date_created": "2019-06-12",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-07-16",
            "fr": "2019-07-16"
        },
        "modified": {
            "en": "2019-07-16",
            "fr": "2019-07-16"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Retirement",
            "fr": "D\u00e9part \u00e0 la retraite"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<h2>What to do and when to do it</h2>\n\n<p>Congratulations on your upcoming retirement! Here are the steps you will need to follow before you leave.</p>\n\n<h3>Employee</h3>\n\n<ol class=\"lst-spcd\">\n\t<li>Inform the <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-pension-services/pension/cn-cu-eng.html\">Pension Centre</a> of your retirement date so they can provide you with pension and benefits information and begin the process on your behalf. They will provide you with information on documents you need to complete and what will need to come from the Pay Centre. The Pension Centre will advise you on when your completed forms need to be returned to them for processing.\n<p><strong>Do this at least 3 months before your departure.</strong></p></li>\n\t<li>Inform your delegated manager <strong>in writing</strong> about your upcoming retirement and the date of your departure. Your manager will need to accept your resignation in writing and submit your resignation and the acceptance along with a <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/demande-employee-request-eng.html#a1\">Pay Action Request</a> to the AAFC HR Trusted Source for processing by the <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/cn-cu-eng.html\">Pay Centre</a>. Send a copy of your resignation and acceptance letter/email with completed forms to the <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-pension-services/pension/cn-cu-eng.html\">Pension Centre</a>. <p><strong>Do this at least 3 months before your departure.</strong></p></li>\n\t<li>Ensure your leave records are up-to-date in PeopleSoft and all overtime and leave without pay in Phoenix have been approved so that the Pay Centre is able to begin the closure of your files, such as payment of any outstanding leave or overtime. The Pay Centre will work with the Pension Centre to finalize your pay and benefit files. If you are using leave, such as vacation and/or compensatory time, ensure your request has been entered into PeopleSoft and approved by your manager as soon as possible.\n<p><strong>Do this as soon as possible.</strong></p></li>\n\t<li>Complete the necessary departmental departure forms and return any assets you may have, such as a Blackberry, cellphone or laptop. While not pay related, this is an important step in your departure process.\n<p><strong>Do this the week of your departure.</strong></p></li>\n\t<li>Ensure both your home and mailing addresses are accurate in both PeopleSoft and Phoenix. If corrections are required, you will need to correct the information in PeopleSoft and verify in Phoenix that the changes have been made correctly.</li>\n</ol>\n\n<h3>Delegated manager</h3>\n\n<ol class=\"lst-spcd\">\n\t<li>Once you receive the resignation notice, approve in writing your employee's resignation due to retirement. Provide the employee's resignation, your acceptance, and a <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/demande-employee-request-eng.html#a1\">Pay Action Request</a> to the AAFC HR Trusted Source by email at <a href=\"mailto:aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca\">aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca</a> for processing by the <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/cn-cu-eng.html\">Pay Centre</a>.</li>\n\t<li>Ensure all of their leave has been approved in PeopleSoft, including overtime and any leave without pay in Phoenix. If they are taking leave as part of their departure, such as vacation before their actual retirement date, they need to submit this and have it approved in advance of their departure so the Pay Centre can accurately process their remaining pay.</li>\n\t<li>Complete the final performance management documents, noting the departure date. If the departing employee has direct reports, ensure their employee's performance agreements are updated in the system and released as needed so their replacement can claim them.</li>\n\t<li>Ensure your employee completes the departure form and returns all assets, such as a Blackberry, cellphone or laptop, to the appropriate areas. While not pay related, this is an important step in the departure process.\n<p><strong>Do this the week of your employee\u2019s departure.</strong></p></li>\n</ol>\n\n<p>Need additional information? Visit <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/index-eng.html\">Pay Centre resources</a>.</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<h2>Quoi faire et \u00e0 quel moment</h2>\n\n<p>F\u00e9licitations pour votre retraite prochaine! Voici les \u00e9tapes \u00e0 suivre avant votre d\u00e9part.</p>\n\n<h3>Employ\u00e9</h3>\n\n<ol class=\"lst-spcd\">\n\t<li>Informez le <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-pension-services/pension/cn-cu-fra.html\">Centre des pensions</a> de la date de votre d\u00e9part \u00e0 la retraite pour qu\u2019il puisse vous fournir des renseignements relatifs \u00e0 la pension et aux avantages sociaux et ainsi entamer le processus en votre nom. Ils vous fourniront de renseignements sur les documents \u00e0 remplir et sur ce que le Centre des services de paye devra fournir. Le Centre des pensions vous indiquera \u00e0 quel moment vous devez lui retourner vos formulaires remplis afin de proc\u00e9der au traitement. <p><strong>Faites-le au moins trois mois avant votre d\u00e9part.</strong></p></li>\n\t<li>Informez votre gestionnaire d\u00e9l\u00e9gataire <strong>par \u00e9crit</strong> de votre d\u00e9part \u00e0 la retraite et de la date \u00e0 laquelle vous quitterez votre poste. Votre gestionnaire devra accepter votre d\u00e9mission fournie par \u00e9crit, puis pr\u00e9senter celle-ci accompagn\u00e9e de son acceptation et d\u2019une <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/demande-employee-request-fra.html\">demande d\u2019intervention de paye</a> \u00e0 la source fiable des RH d'AAC par courrier \u00e9lectronique \u00e0 <a href=\"mailto:aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca\">aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca</a> \u00e0 des fins de traitement par le <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/cn-cu-fra.html\">Centre des services de paye</a>. Envoyez une copie de votre lettre/courriel de d\u00e9mission et d\u2019acceptation avec les formulaires d\u00fbment remplis au <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-pension-services/pension/cn-cu-fra.html\">Centre des pensions</a>. <strong>\u00c0 faire au moins trois mois avant la date de votre d\u00e9part.</strong></li>\n\t<li>Assurez-vous que vos relev\u00e9s de cong\u00e9 sont \u00e0 jour dans PeopleSoft et que les heures suppl\u00e9mentaires et les cong\u00e9s sans solde dans Ph\u00e9nix ont \u00e9t\u00e9 approuv\u00e9s afin que le Centre des services de paye puisse proc\u00e9der \u00e0 la fermeture de vos dossiers (par exemple, le paiement de tout cong\u00e9 ou toute heure suppl\u00e9mentaire en attente). Le Centre des services de paye collaborera avec le Centre des pensions en vue de finaliser vos dossiers de r\u00e9mun\u00e9ration et d\u2019avantages sociaux. Si vous utilisez des cong\u00e9s (par exemple, vacances ou cong\u00e9s compensatoires) avant votre d\u00e9part, assurez-vous que votre demande a \u00e9t\u00e9 entr\u00e9e dans PeopleSoft et qu\u2019elle a \u00e9t\u00e9 approuv\u00e9e par votre gestionnaire. <p><strong>\u00c0 faire le plus t\u00f4t possible </strong></p></li>\n\t<li>Remplissez les formulaires de d\u00e9part du Minist\u00e8re n\u00e9cessaires et rendez tout bien en votre possession (par exemple, BlackBerry, t\u00e9l\u00e9phone cellulaire, ordinateur portable). Bien que cela n\u2019ait pas de lien avec la paye, il s\u2019agit d\u2019une \u00e9tape importante dans le processus de d\u00e9part. <p><strong>Faites-le la semaine de votre d\u00e9part.</strong></p></li>\n\t<li>Assurez-vous que votre adresse domiciliaire et votre adresse postale sont exactes dans PeopleSoft et Ph\u00e9nix. Si des corrections sont requises, vous devrez les apporter dans PeopleSoft et v\u00e9rifier que les changements ont \u00e9t\u00e9 correctement effectu\u00e9s dans Ph\u00e9nix.</li>\n</ol>\n\n<h3>Gestionnaire d\u00e9l\u00e9gu\u00e9</h3>\n\n<ol class=\"lst-spcd\">\n\t<li>Lorsque vous recevez l\u2019avis de d\u00e9mission de l\u2019employ\u00e9, approuvez par \u00e9crit la d\u00e9mission de votre employ\u00e9 due \u00e0 un d\u00e9part \u00e0 la retraite. Faites parvenir la d\u00e9mission de votre employ\u00e9, votre approbation et une <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/demande-employee-request-fra.html\">demande d\u2019intervention de paye</a> \u00e0 la source fiable des RH d\u2019AAC fiable par courrier \u00e9lectronique \u00e0 <a href=\"mailto:aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca\">aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca</a> \u00e0 des fins de traitement par le <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/cn-cu-fra.html\">Centre des services de paye</a>.</li>\n\t<li>Assurez-vous que tous les cong\u00e9s de l\u2019employ\u00e9 ont \u00e9t\u00e9 approuv\u00e9s dans PeopleSoft, y compris les heures suppl\u00e9mentaires et les cong\u00e9s sans solde dans Ph\u00e9nix. Si l\u2019employ\u00e9 prend cong\u00e9 dans le cadre de son processus de d\u00e9part (par exemple, des vacances avant la date de sa retraite), il doit soumettre son cong\u00e9 et le faire approuver avant de quitter ses fonctions afin que le Centre des services de paye puisse traiter correctement la paye restante.</li>\n\t<li>Remplissez les derniers documents de gestion du rendement en prenant en note la date de d\u00e9part. Si l\u2019employ\u00e9 sortant a des subordonn\u00e9s directs, assurez-vous que leurs ententes de rendement sont mises \u00e0 jour dans le syst\u00e8me et diffus\u00e9es au besoin pour que son rempla\u00e7ant puisse les r\u00e9cup\u00e9rer.</li>\n\t<li>Assurez-vous que l\u2019employ\u00e9 a rempli les formulaires de d\u00e9part et qu\u2019il a remis tous ses biens (BlackBerry, t\u00e9l\u00e9phone cellulaire, ordinateur portable, etc.) aux endroits appropri\u00e9s. Bien que cela n\u2019ait pas de lien avec la paye, il s\u2019agit d\u2019une \u00e9tape importante dans le processus de d\u00e9part. <p><strong>Faites-le la semaine du d\u00e9part de l\u2019employ\u00e9.</strong></p></li>\n</ol>\n\n<p>Besoin de renseignements suppl\u00e9mentaires? Visitez le site Web des <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/index-fra.html\">ressources du Centre des services de paye</a>.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Retirement",
        "fr": "D\u00e9part \u00e0 la retraite"
    }
}