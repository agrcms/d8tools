{
    "dcr_id": "1325703455461",
    "lang": "en",
    "title": {
        "en": "Access Control",
        "fr": "Contr\u00f4le de l'acc\u00e8s aux lieux"
    },
    "modified": "2018-02-21 00:00:00.0",
    "issued": "2012-01-05 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1485266766613",
    "layout_name": "1 column",
    "dc_date_created": "2012-01-04",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2012-01-05",
            "fr": "2012-01-05"
        },
        "modified": {
            "en": "2018-02-21",
            "fr": "2018-02-21"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Access Control",
            "fr": "Contr\u00f4le de l'acc\u00e8s aux lieux"
        },
        "subject": {
            "en": "security",
            "fr": "s\u00e9curit\u00e9"
        },
        "description": {
            "en": "Access control is another crucial step in meeting the Policy on Government Security (PGS) objective to safeguard employees, assets and the assurance of continued delivery of services. Government Departments must restrict access to classified and protected information. To accomplish this it is necessary to establish an operations zone which is an area where access is limited to authorized personnel and escorted visitors. This is the Department's typical working environment.",
            "fr": "Le contr\u00f4le de l'acc\u00e8s aux lieux est une autre \u00e9tape d\u00e9terminante de la r\u00e9alisation de l'objectif de la Politique sur la s\u00e9curit\u00e9 du gouvernement (PSG), visant \u00e0 prot\u00e9ger les employ\u00e9s et les biens ainsi qu'\u00e0 assurer la continuit\u00e9 des services. Les minist\u00e8res f\u00e9d\u00e9raux doivent restreindre l'acc\u00e8s aux renseignements classifi\u00e9s et prot\u00e9g\u00e9s. Pour cela, il faut \u00e9tablir une zone de travail, soit une zone dont l'acc\u00e8s est limit\u00e9 au personnel autoris\u00e9 et aux visiteurs accompagn\u00e9s. C'est l'environnement de travail typique du minist\u00e8re."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "safety measures",
            "fr": "mesure de s\u00e9curit\u00e9"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Access control is another crucial step in meeting the <a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=16578\">Policy on Government Security</a> (PGS) objective to safeguard employees, assets and the assurance of continued delivery of services. Government Departments must restrict access to classified and protected information. To accomplish this it is necessary to establish an operations zone which is an area where access is limited to authorized personnel and escorted visitors. This is the Department's typical working environment.</p>\n\n<p>A reception zone at the entry to the facility needs to be established. It is in this area that staff will greet, service, or direct clients as appropriate. For employees and other authorized personnel it is in this area where their access privileges are verified prior to entering the operations zone. The effectiveness of our access control is enhanced when employees can be readily distinguished from other persons who might be on the premises. To accomplish this, a procedure must be in place to permit quick and effective verification that employees, tradespersons and visitors have been processed through the access control area. This is normally accomplished by one of the following methods:</p>\n\n<ul>\n <li>Personal recognition (in the case of smaller facilities);</li>\n <li>Identification cards and /or access badges;</li>\n <li>Administrative procedures;</li>\n <li>Escorts; and,</li>\n <li>Mechanical systems</li>\n</ul>\n \n<p>For any of these methods to be effective employees must be diligent in following their workplace's prescribed procedures. Many of <abbr title=\"Agriculture and Agri-Food Canada\">AAFC</abbr>'s facilities across Canada utilize mechanical methods involving electronic access control, where the system permits access through the use of specially designed access badges that when read by the system equipment determine the individuals access privileges. In some sites a combination identification and access card is used. These badges must be displayed by employees at all times when they are in the workplace. Additionally to ensure the effectiveness of the control of access all employees should follow these best practices:</p>\n\n<ul>\n <li>Challenge individuals in your workplace that are not displaying the appropriate identification;</li>\n <li>Remove the opportunity for piggybacking (allowing a person to enter a restricted area by following an employee through a door they just opened); and,</li>\n <li>Ensure access badges and/or identification cards are returned when an individual has terminated employment with the Department.</li>\n</ul>\n\n<p>For these security measures to be effective we need to ensure that all personnel have been appropriately screened before unescorted access is granted beyond the reception zone.</p>\n\n<p>If you have any questions regarding this or any other security topic please contact Rob King at 613-773-0250.</p>\n\t\t",
        "fr": "\n\t\t\t  \n \n<p>Le contr\u00f4le de l'acc\u00e8s aux lieux est une autre \u00e9tape d\u00e9terminante de la r\u00e9alisation de l'objectif de la <a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=16578\">Politique sur la s\u00e9curit\u00e9 du gouvernement</a> (PSG), visant \u00e0 prot\u00e9ger les employ\u00e9s et les biens ainsi qu'\u00e0 assurer la continuit\u00e9 des services. Les minist\u00e8res f\u00e9d\u00e9raux doivent restreindre l'acc\u00e8s aux renseignements classifi\u00e9s et prot\u00e9g\u00e9s. Pour cela, il faut \u00e9tablir une zone de travail, soit une zone dont l'acc\u00e8s est limit\u00e9 au personnel autoris\u00e9 et aux visiteurs accompagn\u00e9s. C'est l'environnement de travail typique du minist\u00e8re.</p>\n\n<p>Une zone d'accueil \u00e0 l'entr\u00e9e de l'installation doit \u00eatre am\u00e9nag\u00e9e. C'est dans cette zone que les employ\u00e9s salueront, serviront ou orienteront les clients, selon le cas. Pour les employ\u00e9s et d'autres personnes autoris\u00e9es, c'est l\u00e0 que leurs titres d'acc\u00e8s seront v\u00e9rifi\u00e9s avant qu'ils ne p\u00e9n\u00e8trent dans la zone de travail. L'efficacit\u00e9 de notre contr\u00f4le de l'acc\u00e8s augmente lorsque les employ\u00e9s se distinguent facilement d'autres personnes qui pourraient se trouver sur les lieux. Pour cela, il faut mettre en place une m\u00e9thode qui permette de v\u00e9rifier rapidement et efficacement que les employ\u00e9s, les personnes de m\u00e9tier et les visiteurs sont pass\u00e9s par la zone de contr\u00f4le de l'acc\u00e8s. Normalement, on y parvient en appliquant l'une des m\u00e9thodes suivantes\u00a0:</p>\n\n<ul>\n <li>reconnaissance de la personne (dans le cas des installations plus petites);</li>\n <li>cartes d'identit\u00e9 et/ou laissez passer;</li>\n <li>formalit\u00e9s administratives;</li>\n <li>accompagnateurs;</li>\n <li>dispositifs m\u00e9caniques.</li>\n</ul>\n \n<p>Pour que ces m\u00e9thodes soient efficaces, les employ\u00e9s doivent appliquer avec diligence les consignes prescrites par leur lieu de travail. Bon nombre d'installations d'AAC dans l'ensemble du Canada recourent aux moyens m\u00e9caniques qui supposent le contr\u00f4le \u00e9lectronique de l'acc\u00e8s, o\u00f9 le dispositif autorise l'acc\u00e8s en utilisant des laissez passer sp\u00e9cialement con\u00e7us qui, lorsqu'ils sont lus par lui, d\u00e9terminent les titres d'acc\u00e8s des particuliers. En certains lieux, on utilise une combinaison de cartes d'identit\u00e9 et d'acc\u00e8s. Ces laissez passer doivent \u00eatre pr\u00e9sent\u00e9s par les employ\u00e9s \u00e0 tout moment lorsqu'ils se trouvent sur les lieux de travail. De plus, pour garantir l'efficacit\u00e9 du contr\u00f4le de l'acc\u00e8s, tous les employ\u00e9s devraient suivre ces meilleures pratiques\u00a0:</p>\n\n<ul>\n <li>Interpeler les personnes qui se trouvent sur les lieux de travail et qui ne portent pas l'identification appropri\u00e9e;</li>\n <li>\u00e9liminer les possibilit\u00e9s de resquille (permettre \u00e0 une personne d'entrer dans une zone restreinte en suivant un employ\u00e9 par une porte qui vient juste de s'ouvrir);</li>\n <li>Veiller \u00e0 ce que les laissez passer et/ou les cartes d'identit\u00e9 soient retourn\u00e9s lorsque la p\u00e9riode d'emploi du titulaire a pris fin au minist\u00e8re.</li>\n</ul>\n\n<p>Pour que ces mesures de s\u00e9curit\u00e9 soient efficaces, nous devons veiller \u00e0 ce que tous les employ\u00e9s aient \u00e9t\u00e9 bien tri\u00e9s avant qu'ils ne puissent se rendre, non accompagn\u00e9s, au del\u00e0 de la zone d'accueil.</p>\n\n<p>Pour toute question concernant cette m\u00e9thode de s\u00e9curit\u00e9 ou d'autres, communiquez avec Rob King au num\u00e9ro 613-773-0250.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Access Control",
        "fr": "Contr\u00f4le de l'acc\u00e8s aux lieux"
    }
}