{
    "dcr_id": "1415296553079",
    "lang": "en",
    "title": {
        "en": "Submit your travel request",
        "fr": "Soumettez votre demande de voyage"
    },
    "modified": "2019-04-11 00:00:00.0",
    "issued": "2014-12-04 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1484940874128",
    "layout_name": "1 column",
    "dc_date_created": "2014-11-06",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2014-12-04",
            "fr": "2014-12-04"
        },
        "modified": {
            "en": "2019-04-11",
            "fr": "2019-04-11"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Submit your travel request",
            "fr": "Soumettez votre demande de voyage"
        },
        "subject": {
            "en": "communications;travel",
            "fr": "communications;voyage"
        },
        "description": {
            "en": "The following page will help you submit your travel request.",
            "fr": "Cette page vous aidera \u00e0 soumettre votre demande de voyage."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Travel Request (TR), Online Booking Tool (OBT), Expenditure Management Tool (EMT)",
            "fr": "Outil de r\u00e9servation en ligne (OREL), Cr\u00e9er la demande de voyage, Outil de gestion des d\u00e9penses (OGD)"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>The following page will help you submit your travel request.</p>\n\n<h2>Using the Shared Travel Service Portal</h2>\n\n<p>To create your Travel Request (TR), login to the <a href=\"https://isuite6.hrgworldwide.com/gcportal/en-ca/sts.aspx\">Shared Travel Service Portal</a>.</p>\n\n<p>Use the Online Booking Tool (OBT) to select your travel arrangements and  accommodation requirements (that is, air travel, rail, hotel, car rental) and complete your trip details. Then click on \"Create Travel Request\" and you will  be automatically redirected to the Expenditure Management Tool (EMT) where you  can add additional travel request expenses (for example, meals, incidentals, mileage).</p>\n<p>If no OBT bookings are required for travel arrangements or  accommodations, enter your trip details and click on \"Create Travel Request\" to go straight to EMT to complete the TR.</p>\n\n<p><b>Please  note:</b></p>\n\n<ul>\n  <li>If you have forgotten your login information, please contact <a href=\"aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca\">aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca</a> and a member  of the team will assist you.</li>\n\n  <li>There are many user guides available in the STS Portal that will help you use the travel solution. In the STS Portal, click on \u201cSupport\u201d and then \u201cTraining\u201d to access the user guides. The <b>\"Creating a Travel  Request (Full Solution) - Travellers and Arrangers\" </b>guide will help you  create and submit your travel request.</li>\n\n  <li>Please ensure that you are requesting the <a href=\"display-afficher.do?id=1415290508568&amp;lang=eng#wwnayt\">correct level of delegated approval</a> when  submitting the TR for approval or recommendation based on your branch process.</li>\n  <li>If you have <b>selected \"auto-book\"</b> in the OBT, the system will try to automatically book everything  once your TR has been approved. If the system was not able to book something,  you will be notified as to which portions were not booked.</li>\n  <li>If you <b>did  not select \"auto-book\"</b> in the OBT, you will need to book your travel  requirements using the OBT or by calling the travel call centre (1-866-857-3578)  once your TR has been approved.</li>\n\n  <li>In exceptional cases, where a Travel Request  cannot be submitted using the Shared Travel Service Portal, a <a href=\"https://forms-formulaires.agr.gc.ca/pdf/A2672-E.pdf\"> Travel  Authority and Advance form (AAFC/AAC 2672) (PDF)</a> may be used to submit your travel request. Submit your completed form to your <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100248505&amp;lang=eng\" target=\"_blank\">Travel Authority Number holder (Excel)</a> to obtain an offline Travel Authority Number (TAN), for booking through the Hogg Robinson Group (HRG) call centre.</li>\n</ul>\n\n<h2>How-to videos</h2>\n\n<p>These short videos produced by the Travel Helpdesk provide step-by-step instructions on how to complete certain tasks within this step of the travel process.</p>\n\n<ul class=\"list-unstyled\">\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Adding%20the%20allowances%20for%20private%20motor%20vehicle%20to%20a%20travel%20request%20compressed.mp4\">Adding an allowance for a private motor vehicle (MP4)</a>\n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415296553079_1-eng.docx\">Adding an allowance for a private motor vehicle - Transcript (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Adding%20an%20expense%20to%20a%20travel%20request%20compressed.mp4\">Adding an expense to a travel request (MP4)</a> \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415296553079_2-eng.docx\">Adding an expense to a travel request - Transcript (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/English%20To%20add%20a%20Recommender%20-%20Remastered.mp4\">Adding a Recommender (MP4)</a> \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415296553079_3-eng.docx\">Adding a Recommender - Transcript (Word)</a></li></ul></li>\n</ul>\n\n<h2>Contact us</h2>\n\n<p>Have questions? View our list of key contacts at <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100477471&amp;lang=eng\" target=\"_blank\">Agriculture and Agri-Food Canada for travel (Word)</a>.</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Cette page vous aidera \u00e0 soumettre votre demande de voyage.</p>\n\n<h2>Portail des Services de voyage partag\u00e9s</h2>\n\n<p>Pour cr\u00e9er votre demande de voyage, ouvrez une session dans le <a href=\"https://isuite6.hrgworldwide.com/gcportal/fr-ca/sts.aspx\">Portail des Services de voyage partag\u00e9s</a>.</p>\n\n<p>Utilisez l'Outil de r\u00e9servation en ligne (OREL)  pour s\u00e9lectionner vos besoins en mati\u00e8re de pr\u00e9paratifs de voyage et d'h\u00e9bergement  (par exemple, transport a\u00e9rien ou ferroviaire, h\u00f4tel, location d'un v\u00e9hicule) et  indiquez les d\u00e9tails de votre voyage. Cliquez ensuite sur \u00ab\u00a0Cr\u00e9er la  demande de voyage\u00a0\u00bb, et vous serez automatiquement redirig\u00e9 vers l'Outil  de gestion des d\u00e9penses (OGD), o\u00f9 vous pourrez ajouter des frais de voyage suppl\u00e9mentaires (c'est-\u00e0-dire repas, faux frais, kilom\u00e9trage).</p>\n<p>Si vous n'avez aucune r\u00e9servation \u00e0 faire au moyen de l'OREL pour des pr\u00e9paratifs de voyage ou un logement, indiquez les d\u00e9tails de votre voyage et cliquez sur \u00ab\u00a0Cr\u00e9er la demande de voyage\u00a0\u00bb pour passer directement \u00e0 l'OGD et terminer votre demande.</p>\n\n<p><b>Remarques\u00a0:</b></p>\n<ul>\n  <li>Si vous avez oubli\u00e9 votre information pour  l'ouverture de session, communiquez avec le <a href=\"mailto:aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca\">aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca</a>, et un membre de l'\u00e9quipe vous aidera.</li>\n\n  <li>Le Portail des SVP vous donne acc\u00e8s \u00e0 de nombreux guides de l'utilisateur qui vous aideront \u00e0 vous servir de la solution de voyage. Dans le Portail des SVP, cliquez sur \u00ab\u00a0Soutien\u00a0\u00bb et ensuite \u00ab\u00a0Formation\u00a0\u00bb pour acc\u00e9der aux guides de l'utilisateur. Le guide intitul\u00e9 \u00ab\u00a0<b>Cr\u00e9er une demande de voyage (solution compl\u00e8te) \u2013 Voyageurs et organisateurs</b>\u00a0\u00bb vous aidera \u00e0 cr\u00e9er et \u00e0  soumettre votre demande de voyage.</li>\n\n  <li>Assurez-vous d'obtenir le <a href=\"display-afficher.do?id=1415290508568&amp;lang=fra#qdavv\">bon niveau d'approbation</a>. Selon le  processus de votre direction g\u00e9n\u00e9rale lorsque vous soumettez votre demande de voyage pour approbation ou recommandation.</li>\n  <li>Si vous <b>avez  s\u00e9lectionn\u00e9 \u00ab\u00a0R\u00e9servation automatique\u00a0\u00bb</b> dans l'OREL, le syst\u00e8me essaiera de tout r\u00e9server automatiquement une fois votre demande de voyage  approuv\u00e9e. S'il n'y arrive pas, vous serez inform\u00e9 de ce qui n'a pas pu \u00eatre  r\u00e9serv\u00e9.</li>\n  <li>Si vous <b>n'avez  pas s\u00e9lectionn\u00e9 \u00ab\u00a0R\u00e9servation automatique\u00a0\u00bb</b> dans l'OREL, vous devez faire vos r\u00e9servations au moyen de l'OREL ou en communiquant avec le  centre d'appels pour les voyages (1-866-857-3578) une fois votre demande de  voyage approuv\u00e9e.</li>\n  <li>Dans des cas exceptionnels, lorsqu'une demande de voyage ne peut pas \u00eatre envoy\u00e9e par le Portail des Services de voyage partag\u00e9s, le formulaire <a href=\"https://forms-formulaires.agr.gc.ca/pdf/A2672-F.pdf\">Autorisation de voyage et avance (AAFC/AAC 2672) (PDF)</a> peut \u00eatre utilis\u00e9 pour envoyer votre demande de voyage. Soumettez votre formulaire d\u00fbment rempli \u00e0 votre <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100248505&amp;lang=fra\" target=\"_blank\">d\u00e9tenteur de num\u00e9ro d'autorisation de voyager (Excel)</a> pour obtenir un num\u00e9ro d'autorisation de voyager (NAV) hors ligne, afin de faire une r\u00e9servation via le centre d\u2019appels HRG. </li>\n</ul>\n\n<h2>Vid\u00e9os d\u00e9mo</h2>\n\n<p>Ces courtes vid\u00e9os produites par le bureau d\u2019aide de voyage fournissent des instructions \u00e9tape par \u00e9tape sur la fa\u00e7on d'effectuer certaines t\u00e2ches au cours de cette \u00e9tape du processus de voyage.</p>\n\n<ul class=\"list-unstyled\">\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Remastered%20Ajouter%20une%20d%C3%A9pense%20dans%20la%20demande%20de%20voyage%20(compressed).mp4\">Ajouter une d\u00e9pense \u00e0 une demande de voyage (MP4)</a>\n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415296553079_1-fra.docx\">Ajouter une d\u00e9pense \u00e0 une demande de voyage - Transcription (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Remasted%20-%20Ajout%C3%A9%20une%20d%C3%A9pense%20pour%20un%20v%C3%A9hicule%20priv%C3%A9%20-%20compressed.mp4\">Ajouter une d\u00e9pense pour un v\u00e9hicule priv\u00e9 \u00e0 une demande de voyage (MP4)</a> \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415296553079_2-fra.docx\">Ajouter une d\u00e9pense pour un v\u00e9hicule priv\u00e9 \u00e0 une demande de voyage - Transcription (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/RemasteredAjouter-supprimer%20un%20recommandeur%20-%20compressed.mp4\">Ajouter un recommandeur (MP4)</a> \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415296553079_3-fra.docx\">Ajouter un recommandeur - Transcription (Word)</a></li></ul></li>\n</ul>\n\n<h2>Contactez-nous</h2>\n\n<p>Vous avez des questions? Consultez la liste de  personnes-ressources cl\u00e9s d'<a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100477477&amp;lang=fra\" target=\"_blank\">Agriculture et Agroalimentaire Canada pour les voyages (Word)</a> afin de savoir \u00e0 qui vous adresser.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Submit your travel request",
        "fr": "Soumettez votre demande de voyage"
    }
}