{
    "dcr_id": "1592393215900",
    "lang": "en",
    "title": {
        "en": "Leave with income averaging (AAFC fact sheet)",
        "fr": "Cong\u00e9 avec \u00e9talement du revenu (fiche d'information d'AAC)"
    },
    "modified": "2020-06-17 00:00:00.0",
    "issued": "2020-06-17 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279035583756",
    "layout_name": "1 column",
    "dc_date_created": "2020-06-17",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2020-06-17",
            "fr": "2020-06-17"
        },
        "modified": {
            "en": "2020-06-17",
            "fr": "2020-06-17"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Leave with income averaging (AAFC fact sheet)",
            "fr": "Cong\u00e9 avec \u00e9talement du revenu (fiche d'information d'AAC)"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<h2>What it is and how to get it</h2>\n\n<p>Leave with income averaging is a special working arrangement that allows an employee to take leave without pay for a minimum of 5 weeks (25 working days) and a maximum of 3 months within a 12-month period. While an employee\u2019s pay would be reduced and averaged out over a 12-month period, pension and benefits coverage and the corresponding premiums and contributions would continue at pre-arrangement levels.</p>\n\n<h2>What you need to know</h2>\n\n<p>The leave without pay portion of the working arrangement may be taken in one or two parts within a 12-month period. Each leave period must be a minimum of 5 weeks (25 working days), and together may not exceed 3 months.</p>\n\n<p>Employees on leave with income averaging maintain their employment status, and continue to fall under the provisions of the relevant collective agreement or terms and conditions of employment.</p>\n\n<p>Though income continues throughout the 12-month period, employees are deemed to be on leave without pay during the non-work period of the arrangement; adjustments to some entitlements, such as bilingual bonus, may result.</p>\n\n<p>Before approving the leave, Section 34 managers must confirm that:</p>\n\n<ul>\n\t<li>the employee meets the terms and conditions of leave, and</li>\n\t<li>the work arrangement is operationally feasible for the 12-month period\u2014in other words, the quality of service or costs associated with service delivery would not be adversely affected.</li>\n</ul>\n\t\n<p>Once the application has been signed by the employee and the Section 34 manager, any changes to the arrangement may only be made in rare and unforeseen circumstances.</p>\n\n<h2>Conditions of leave</h2>\n\n<p>To be eligible, employees must:</p>\n\n<ul>\n\t<li>Be appointed to the core public administration with indeterminate employment status</li>\n\t<li>Not be surplus at the start of the arrangement</li>\n\t<li>Agree not to work for the federal public service while on leave without pay</li>\n\t<li>Agree to respect the <cite><a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=25049\">Values and Ethics Code for the Public Sector</a></cite> while on leave without pay</li>\n</ul>\n\n<p>Refer to <a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=15774#appD\">Appendix D of the Directive on Leave and Special Working Arrangements</a> for more information on terms and conditions of leave with income averaging.</p>\n\n<h2>Applying for leave with income averaging: employees</h2>\n\n<ol>\n\t<li>Consult the Directive to verify that you meet all the required terms and condition, and fully understand the impacts on your pay and benefits</li>\n\t<li>After speaking with your manager, submit an <a href=\"http://www.tbs-sct.gc.ca/tbsf-fsct/325-10-eng.asp\">application for leave with income averaging</a>.</li>\n</ol>\n\n<h2>Responsibilities for Section 34 managers</h2>\n\n<ol>\n\t<li>Verify that the employee meets all of the eligibility criteria</li>\n\t<li>Assess the operational impact of the special working arrangement</li>\n\t<li>If leave is approved, send the signed application and a <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/demande-employee-request-eng.html#a1\">Pay Action Request</a> to the AAFC HR Trusted Source by email at <a href=\"mailto:aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca\">aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca</a> for processing by the <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/cn-cu-eng.html\">Pay Centre</a>.</li>\n</ol>\n\n<h2>Resources</h2>\n\t\n<ul>\n\t<li><a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-information-pay/vie-life/vie-conge-life-leave/cer-lia-eng.html\">Leave with Income Averaging (PSPC)</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<h2>En quoi consiste ce cong\u00e9 et comment l'obtient-on?</h2>\n\n<p>Un cong\u00e9 avec \u00e9talement du revenu est une entente de travail sp\u00e9ciale qui permet \u00e0 un employ\u00e9 de prendre un cong\u00e9 sans solde d'au moins 5 semaines (25 jours) et d'au plus 3 mois sur une p\u00e9riode de 12 mois. La paye de l'employ\u00e9 qui prend un tel cong\u00e9 est r\u00e9duite et r\u00e9partie sur une p\u00e9riode de 12 mois, mais les protections en mati\u00e8re de pension, les avantages sociaux et les prestations connexes restent les m\u00eames.</p>\n\n<h2>Ce qu'il faut savoir</h2>\n\n<p>Le volet \u00ab\u00a0cong\u00e9 sans solde\u00a0\u00bb de l'entente peut \u00eatre pris en une ou deux p\u00e9riodes pendant les 12 mois. Si le cong\u00e9 est \u00e9tal\u00e9 sur deux p\u00e9riodes, celles-ci doivent durer au moins 5 semaines (25 jours ouvrables), mais ne doivent pas d\u00e9passer 3 mois au total.</p>\n\n<p>Un employ\u00e9 en cong\u00e9 avec \u00e9talement du revenu conserve son statut d'emploi et reste assujetti aux dispositions de sa convention collective et \u00e0 ses conditions d'emploi.</p>\n\n<p>M\u00eame si sa r\u00e9mun\u00e9ration lui est vers\u00e9e pendant toute la p\u00e9riode de 12 mois, l'employ\u00e9 est r\u00e9put\u00e9 \u00eatre en cong\u00e9 sans solde pendant la p\u00e9riode de l'entente de travail au cours de laquelle il ne travaille pas; certaines de ses prestations peuvent \u00eatre rajust\u00e9es en cons\u00e9quence, comme sa prime au bilinguisme.</p>\n\n<p>Avant d'approuver le cong\u00e9, le gestionnaire d\u00e9tenant les pouvoirs d\u00e9l\u00e9gu\u00e9s vis\u00e9s par l'article 34 doit confirmer ce qui suit\u00a0:</p>\n\n<ul>\n\t<li>l'employ\u00e9 satisfait aux conditions requises pour avoir droit au cong\u00e9;</li>\n\t<li>les modalit\u00e9s de travail sp\u00e9ciales sont r\u00e9alisables sur le plan op\u00e9rationnel pendant la p\u00e9riode de 12 mois\u00a0\u2014 autrement dit, elles n'influeront pas sur la qualit\u00e9 ou le co\u00fbt des services offerts.</li>\n</ul>\n\t\n<p>Une fois la demande de cong\u00e9 sign\u00e9e par l'employ\u00e9 et le gestionnaire d\u00e9tenant les pouvoirs d\u00e9l\u00e9gu\u00e9s vis\u00e9s par l'article 34, les modalit\u00e9s de travail ne peuvent \u00eatre modifi\u00e9es qu'en cas de circonstances rares et impr\u00e9vues.</p>\n\n<h2>Conditions \u00e0 remplir</h2>\n\n<p>Pour avoir droit au cong\u00e9 avec \u00e9talement du revenu, l'employ\u00e9 doit\u00a0:</p>\n\n<ul>\n\t<li>\u00catre nomm\u00e9 pour une p\u00e9riode ind\u00e9termin\u00e9e dans un poste \u00e0 l'administration publique centrale;</li>\n\t<li>Ne pas \u00eatre un employ\u00e9 exc\u00e9dentaire au d\u00e9but de l'entente;</li>\n\t<li>Accepter de ne pas travailler ailleurs \u00e0 la fonction publique f\u00e9d\u00e9rale pendant son cong\u00e9 sans solde;</li>\n\t<li>Accepter de se conformer au <cite><a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=25049\">Code de valeurs et d'\u00e9thique de la fonction publique</a></cite> pendant son cong\u00e9 sans solde.</li>\n</ul>\n\n<p>L'<a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=15774#appD\">annexe D de la Directive sur les cong\u00e9s et les modalit\u00e9s de travail sp\u00e9ciales</a> renferme plus de renseignements sur les modalit\u00e9s du cong\u00e9 avec \u00e9talement du revenu.</p>\n\n<h2>Demander le cong\u00e9 avec \u00e9talement du revenu</h2>\n\n<ol>\n\t<li>L'employ\u00e9 doit consulter la directive pour confirmer qu'il remplit toutes les conditions et qu'il comprend bien les r\u00e9percussions du cong\u00e9 sur sa r\u00e9mun\u00e9ration et ses avantages sociaux.</li>\n\t<li>Apr\u00e8s avoir discut\u00e9 avec son gestionnaire, l'employ\u00e9 pr\u00e9sente sa <a href=\"https://www.tbs-sct.gc.ca/tbsf-fsct/325-10-fra.asp\">demande de cong\u00e9 avec \u00e9talement du revenu</a>.</li>\n</ol>\n\n<h2>Responsabilit\u00e9s du gestionnaire d\u00e9tenant les pouvoirs d\u00e9l\u00e9gu\u00e9s vis\u00e9s par l'article 34</h2>\n\n<ol>\n\t<li>Le gestionnaire confirme que l'employ\u00e9 remplit tous les crit\u00e8res d'admissibilit\u00e9.</li>\n\t<li>Il \u00e9value l'incidence op\u00e9rationnelle des modalit\u00e9s de travail sp\u00e9ciales.</li>\n\t<li>Si le cong\u00e9 est approuv\u00e9, il envoie par courriel le formulaire de demande sign\u00e9 et une <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/demande-employee-request-fra.html#a1\">demande d'intervention de paye</a> \u00e0 la source fiable des RH d'AAC (<a href=\"mailto:aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca\">aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca</a>), qui transmet le tout au <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/cn-cu-fra.html\">Centre des services de paye</a> pour traitement.</li>\n</ol>\n\n<h2>Ressources</h2>\n\t\n<ul>\n\t<li><a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-information-pay/vie-life/vie-conge-life-leave/cer-lia-fra.html\">Cong\u00e9 avec \u00e9talement du revenu (SPAC)</a></li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Leave with income averaging (AAFC fact sheet)",
        "fr": "Cong\u00e9 avec \u00e9talement du revenu (fiche d'information d'AAC)"
    }
}