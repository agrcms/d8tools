{
    "dcr_id": "1547218785356",
    "lang": "en",
    "title": {
        "en": "Boardroom Equipment: How to Minimize Meeting Disruptions ",
        "fr": "\u00c9quipement des salles de conf\u00e9rence : r\u00e9duire le risque de pannes lors des r\u00e9unions"
    },
    "modified": "2019-01-17 00:00:00.0",
    "issued": "2019-01-17 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-01-11",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-01-17",
            "fr": "2019-01-17"
        },
        "modified": {
            "en": "2019-01-17",
            "fr": "2019-01-17"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Boardroom Equipment: How to Minimize Meeting Disruptions ",
            "fr": "\u00c9quipement des salles de conf\u00e9rence : r\u00e9duire le risque de pannes lors des r\u00e9unions"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "It's a good bet that, at this very moment, you're preparing for a meeting. Or maybe you just left one.",
            "fr": "Vous vous pr\u00e9parez peut-\u00eatre en ce moment en vue d'une r\u00e9union. Ou peut-\u00eatre que vous revenez justement d'une r\u00e9union."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>It\u2019s a good bet that, at this very moment, you\u2019re preparing for a meeting. Or maybe you just left one. Business meetings take a lot of our time, but they\u2019re not always universally productive.</p>\n<p>Sometimes, malfunctioning boardroom equipment can be to blame. Bad audio and video will make remote participants difficult to see and hear. Equipment will refuse to connect to your computer. These, of course, can be caused by connection or server issues which are completely out of your control. But sometimes, they can be caused by meeting participants unplugging or re-wiring computer and videoconferencing equipment, leaving subsequent boardroom users without a proper connection.</p>\n<p>This results in urgent requests to IT Centre TI and takes IT staff away from other calls, sometimes with no guarantee that the issue will be resolved in time for the meeting. Boardroom equipment failures are as frustrating as they are costly. Here are some ways you can minimize them.</p>\n<ol>\n<li>\n<p><strong>Leave the configuration of boardroom equipment, including cameras and cables, to IT staff.</strong></p>\n<p>Even if you are a secret tech wizard at heart, call IT Centre TI and ask for help in changing the way the equipment is connected. If you find the equipment disconnected, do the same. Moving it may cause damage, resulting in an expensive replacement.</p>\n</li>\n<li>\n<p><strong>Arrive early and check the equipment.</strong></p>\n<p>Preparation never hurts, especially if you\u2019re organizing a big meeting. Depending on your technical needs, book the boardroom 15 or 30 minutes earlier to allow yourself time to test it. This will give you enough time to troubleshoot potential issues with IT Centre TI.</p>\n</li>\n<li>\n<p><strong>Identify additional equipment needed for the meeting and request it.</strong></p>\n<p>Projectors, speakers and televisions are not part of the standard setup of a boardroom. If you need that equipment, <a href=\"http://assystweb.agr.gc.ca:8080/assystnet/application.jsp#serviceOfferings/1287\">submit</a> a request to IT Centre ahead of the meeting.</p>\n</li>\n</ol>\n<p>The Information Systems Branch regularly reviews employees\u2019 boardroom needs and frequent technical issues encountered to make improvements and enable everyone to collaborate with minimal disruption. If you have questions about boardroom equipment or needs that the current boardroom set-up is not meeting, please contact IT Centre TI.</p>\n<p>\u00a0</p>\n<p><strong>IT Centre TI</strong><br>\nTelephone: 1-855-545-4411<br>\n<a href=\"http://assystweb:8080/assystnet\">My IT Centre TI</a></p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/ITCentreTI2.png\" class=\"center-block\"></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Vous vous pr\u00e9parez peut-\u00eatre en ce moment en vue d\u2019une r\u00e9union. Ou peut-\u00eatre que vous revenez justement d\u2019une r\u00e9union. Les r\u00e9unions d\u2019affaires prennent beaucoup de temps, mais elles ne sont pas toujours productives.</p>\n<p>Parfois, le mauvais fonctionnement de l\u2019\u00e9quipement dans la salle de conf\u00e9rence entre en jeu. Les participants \u00e0 distance peuvent avoir de la difficult\u00e9 \u00e0 voir et \u00e0 entendre en raison d\u2019une mauvaise connexion audio et vid\u00e9o. Ou le mat\u00e9riel refuse de se connecter \u00e0 votre ordinateur. Il peut s\u2019agir alors de probl\u00e8mes de connexion ou de serveur qui \u00e9chappent enti\u00e8rement \u00e0 votre contr\u00f4le. Mais il arrive parfois que des participants \u00e0 une r\u00e9union d\u00e9branchent ou reconfigurent le mat\u00e9riel informatique et de vid\u00e9oconf\u00e9rence, de sorte que les utilisateurs suivants de la salle de r\u00e9union se retrouvent sans connexion ad\u00e9quate.</p>\n<p>De telles situations donnent lieu \u00e0 des demandes urgentes aupr\u00e8s du IT Centre TI, lesquelles emp\u00eachent le personnel de TI de se consacrer \u00e0 d\u2019autres appels. En outre, il n\u2019est pas toujours garanti que le probl\u00e8me sera r\u00e9gl\u00e9 \u00e0 temps pour la r\u00e9union. Les pannes d\u2019\u00e9quipement des salles de conf\u00e9rence sont non seulement frustrantes, mais elles entra\u00eenent des co\u00fbts. Voici des fa\u00e7ons de r\u00e9duire le risque de probl\u00e8mes techniques.</p>\n<ol>\n<li>\n<p><strong>Laissez le personnel de la TI se charger de la configuration de l\u2019\u00e9quipement de la salle de conf\u00e9rence, y compris les cam\u00e9ras et les c\u00e2bles.</strong></p>\n<p>M\u00eame si vous avez des talents cach\u00e9s d\u2019expert en informatique, appelez le IT Centre TI et demandez de l\u2019aide pour modifier la connexion du mat\u00e9riel. M\u00eame chose si vous trouvez le mat\u00e9riel d\u00e9branch\u00e9. Le d\u00e9placer peut causer des dommages et entra\u00eener un remplacement co\u00fbteux.</p>\n</li>\n<li>\n<p><strong>Arrivez t\u00f4t et v\u00e9rifiez le mat\u00e9riel.</strong></p>\nUne bonne pr\u00e9paration est toujours conseill\u00e9e, surtout si vous organisez une r\u00e9union importante. Selon vos besoins techniques, r\u00e9servez la salle 15 \u00e0 30\u00a0 minutes avant la r\u00e9union pour avoir le temps de faire un essai. Vous aurez ainsi le temps de r\u00e9gler les probl\u00e8mes potentiels avec le IT Centre TI.</li>\n<li>\n<p><strong>Demandez \u00e0 l\u2019avance tout mat\u00e9riel suppl\u00e9mentaire n\u00e9cessaire \u00e0 la r\u00e9union.</strong></p>\n<p>Les projecteurs, les haut-parleurs et les t\u00e9l\u00e9viseurs ne font pas partie de l\u2019\u00e9quipement standard d\u2019une salle de conf\u00e9rence. Si vous avez besoin de ces appareils, <a href=\"http://assystweb.agr.gc.ca:8080/assystnet/application.jsp#serviceOfferings/1287\">soumettez</a> une demande au IT Centre TI avant la r\u00e9union.</p>\n</li>\n</ol>\n<p>La Direction g\u00e9n\u00e9rale des syst\u00e8mes d\u2019information examine r\u00e9guli\u00e8rement les besoins li\u00e9s aux salles de conf\u00e9rence et les probl\u00e8mes techniques les plus fr\u00e9quents afin d\u2019apporter des am\u00e9liorations et de permettre \u00e0 tous les employ\u00e9s de collaborer avec le moins de perturbation possible. Si vous avez des questions sur l\u2019\u00e9quipement des salles de conf\u00e9rence ou si vous avez des besoins auxquels la configuration actuelle d\u2019une salle de conf\u00e9rence ne r\u00e9pond pas, veuillez communiquer avec le IT Centre TI.</p>\n<p>\u00a0</p>\n<p><strong>IT Centre TI</strong><br>\nT\u00e9l\u00e9phone : 1-855-545-4411<br>\n<a href=\"http://assystweb:8080/assystnet\">Mon IT Centre TI</a></p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/ITCentreTI2.png\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Boardroom Equipment: How to Minimize Meeting Disruptions",
        "fr": "\u00c9quipement des salles de conf\u00e9rence : r\u00e9duire le risque de pannes lors des r\u00e9unions"
    },
    "news": {
        "date_posted": {
            "en": "2019-01-17",
            "fr": "2019-01-17"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "IT Centre TI",
            "fr": "IT Centre TI"
        }
    }
}