{
    "dcr_id": "1588020354759",
    "lang": "en",
    "title": {
        "en": "A Message From AAFC's Ombudsperson",
        "fr": "Un message de l'ombudsman d'AAC "
    },
    "modified": "2020-04-27 00:00:00.0",
    "issued": "2020-04-27 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2020-04-27",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2020-04-27",
            "fr": "2020-04-27"
        },
        "modified": {
            "en": "2020-04-27",
            "fr": "2020-04-27"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "A Message From AAFC's Ombudsperson",
            "fr": "Un message de l'ombudsman d'AAC "
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "In these trying times, I want to ensure employees are aware that Ombuds services remain available.",
            "fr": "En ces temps difficiles, je veux m'assurer que les employ\u00e9s savent que les services de l\u2019ombudsman restent disponibles."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>In these trying times, I want to ensure employees are aware that Ombuds services remain available. Workplace well-being remains a priority for AAFC, and, in addition to services and resources offered by Human Resources, employees can contact the Ombuds office regarding workplace issues such as harassment, civility and wellness.</p>\n<p>There is a poem circulating on social media that reflects on the current coronavirus measures and that says \u201cwe are not in the same boat, but we are in the same storm\u201d. This resonated with me, both in my personal and work life. While everyone is impacted, people are going through this time very differently. With different needs, abilities, comforts, discomforts, pressures and opportunities. I am talking to employees who have never been so busy, and others who are desperate to get back to work. Some employees are completely alone, while others are juggling multigenerational families. The poem goes on to say that the key to weathering the storm is not to judge, and for people to navigate their route with respect, empathy and responsibility.</p>\n<p>The storm analogy works for me on other fronts too. Teams that weather a storm together, often come out stronger. Storms like the current one we\u2019re facing remind us the value and importance of rowing in the same direction, and put skills to the test. They can be exhausting, they encourage innovation and eventually they do end. After a big storm, things do not return to normal quickly. There is rebuilding and change, and areas that need special attention and time to heal. I expect this, too, will be the reality.</p>\n<p>As I talk to employees, there are some common elements that emerge: people miss their colleagues; they are tired of electronic communication; they are concerned about their work, about students and about the months ahead. It\u2019s a good reminder that while we are all experiencing this differently, we are in this together.</p>\n<p>Like so many of you, the Ombuds office has been adjusting and is working to continue to support employees. The office is fully functional remotely, and I remain available to discuss well-being and harassment, and can offer support to navigate existing resources. When you contact the Ombuds office we listen, talk through the issue, and brainstorm possible next steps and solutions. At all times, the Ombuds office operates under four principles: independent; confidential, informal and impartial.</p>\n<p>A reminder too that regular departmental messages, as well as tools and resources to support you are posted on the <a href=\"http://multimedia.agr.gc.ca/COVID-19/index-eng.html\">Agriculture and Agri-Food Canada</a> website.</p>\n<p>I know that there will be smoother sailing ahead. And while I have no doubt we will be working towards building a new normal, we will do this best by supporting one another.</p>\n<p>\u00a0</p>\n<p><strong>Jane Taylor<br></strong>Workplace Well-Being Ombudsperson</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>En ces temps difficiles, je veux m\u2019assurer que les employ\u00e9s savent que les services de l\u2019ombudsman restent disponibles. Le bien-\u00eatre au travail demeure une priorit\u00e9 pour AAC et, outre les services et les ressources offerts par les ressources humaines, les employ\u00e9s peuvent communiquer avec le Bureau de l\u2019ombudsman pour des questions li\u00e9es au milieu de travail telles que le harc\u00e8lement, la civilit\u00e9 et le bien-\u00eatre.</p>\n<p>Un po\u00e8me, qui circule pr\u00e9sentement dans les m\u00e9dias sociaux, porte une r\u00e9flexion sur les mesures actuelles contre le coronavirus et dit que \u00ab nous ne sommes pas tous dans le m\u00eame bateau, mais nous affrontons tous la m\u00eame temp\u00eate \u00bb. Ces mots font \u00e9cho tant dans ma vie personnelle que professionnelle. Bien que tout le monde soit touch\u00e9, les gens traversent cette p\u00e9riode de fa\u00e7ons tr\u00e8s diff\u00e9rentes : leurs besoins, leurs capacit\u00e9s, leurs conforts, leurs inconforts, les pressions qu\u2019ils subissent et leurs opportunit\u00e9s ne sont pas tous les m\u00eames. Je parle \u00e0 des employ\u00e9s qui n\u2019ont jamais \u00e9t\u00e9 aussi occup\u00e9s et \u00e0 d\u2019autres qui sont impatients de retourner au travail. Certains employ\u00e9s sont compl\u00e8tement seuls, tandis que d\u2019autres jonglent avec des familles multig\u00e9n\u00e9rationnelles. Le po\u00e8me poursuit en disant que pour survivre \u00e0 la temp\u00eate, l\u2019essentiel n\u2019est pas de juger, mais plut\u00f4t de naviguer sa route avec respect, empathie et responsabilit\u00e9.</p>\n<p>L\u2019analogie avec la temp\u00eate me semble appropri\u00e9e \u00e0 d\u2019autres \u00e9gards aussi. Les \u00e9quipes qui traversent ensemble une temp\u00eate en sortent souvent plus fortes. Les temp\u00eates comme celle que nous vivons en ce moment mettent nos comp\u00e9tences \u00e0 l\u2019\u00e9preuve et nous rappellent l\u2019importance que nous ramions tous dans la m\u00eame direction. Elles peuvent \u00eatre \u00e9puisantes, elles favorisent l\u2019innovation, puis elles finissent par s\u2019apaiser. Apr\u00e8s une grosse temp\u00eate, les choses ne reviennent pas rapidement \u00e0 la normale. Il y a de la reconstruction et des changements \u00e0 faire, des secteurs qui n\u00e9cessitent une attention particuli\u00e8re et un temps de gu\u00e9rison. Je pense que ce sera aussi le cas avec cette pand\u00e9mie.</p>\n<p>Lorsque je parle aux employ\u00e9s, certains \u00e9l\u00e9ments communs se d\u00e9gagent : les gens s\u2019ennuient de leurs coll\u00e8gues, ils sont fatigu\u00e9s des communications \u00e9lectroniques, ils se pr\u00e9occupent de leur travail, des \u00e9tudiants et des mois \u00e0 venir. Il est bon de se rappeler que m\u00eame si nous vivons tous cette situation diff\u00e9remment, elle nous affecte tous.</p>\n<p>Comme beaucoup d\u2019entre vous, le Bureau de l\u2019ombudsman s\u2019est adapt\u00e9 et continue de soutenir les employ\u00e9s. Le Bureau est enti\u00e8rement fonctionnel \u00e0 distance. Je reste disponible pour discuter de bien-\u00eatre ou de harc\u00e8lement et pour aider les employ\u00e9s \u00e0 naviguer les ressources existantes. Lorsque vous contactez le Bureau de l\u2019ombudsman, nous vous \u00e9coutons, discutons du probl\u00e8me et r\u00e9fl\u00e9chissons avec vous aux prochaines \u00e9tapes et aux solutions possibles. Le Bureau de l\u2019ombudsman fonctionne toujours selon quatre principes : ind\u00e9pendance, confidentialit\u00e9, caract\u00e8re informel et impartialit\u00e9.</p>\n<p>Nous vous rappelons \u00e9galement que les messages r\u00e9guliers du Minist\u00e8re ainsi que les outils et les ressources pour vous appuyer sont affich\u00e9s sur le site Web d\u2019<a href=\"http://multimedia.agr.gc.ca/COVID-19/index-fra.html\">Agriculture et Agroalimentaire Canada</a>.</p>\n<p>J\u2019ai confiance que la temp\u00eate finira par se calmer. Et si je ne doute pas que nous travaillerons \u00e0 la construction d\u2019une nouvelle normalit\u00e9, je suis convaincue que c\u2019est en nous soutenant les uns les autres que nous y parviendrons le mieux.</p>\n<p>\u00a0</p>\n<p><strong>Jane Taylor<br></strong>Ombudsman du mieux-\u00eatre en milieu de travail</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "A Message From AAFC's Ombudsperson",
        "fr": "Un message de l'ombudsman d'AAC"
    },
    "news": {
        "date_posted": {
            "en": "2020-04-27",
            "fr": "2020-04-27"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Jane Taylor, Workplace Well-Being Ombudsperson",
            "fr": "Jane Taylor, Ombudsman du mieux-\u00eatre en milieu de travail"
        }
    }
}