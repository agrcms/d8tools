{
    "dcr_id": "1554814593900",
    "lang": "en",
    "title": {
        "en": "Reminder - Legislative Measures to Previous Year Overpayments",
        "fr": "Rappel - Mesures l\u00e9gislatives concernant les trop-pay\u00e9s d\u2019ann\u00e9es ant\u00e9rieures"
    },
    "modified": "2019-04-25 00:00:00.0",
    "issued": "2019-04-11 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-04-09",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-04-11",
            "fr": "2019-04-11"
        },
        "modified": {
            "en": "2019-04-25",
            "fr": "2019-04-17"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Reminder - Legislative Measures to Previous Year Overpayments",
            "fr": "Rappel - Mesures l\u00e9gislatives concernant les trop-pay\u00e9s d\u2019ann\u00e9es ant\u00e9rieures"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Employees affected by previous year overpayments due to clerical, administrative, or system errors should note that new legislative changes to the Income Tax and Employment Act have been implemented.",
            "fr": "Les employ\u00e9s ayant re\u00e7u un trop-pay\u00e9 au cours d\u2019une ann\u00e9e ant\u00e9rieure \u00e0 la suite d\u2019une erreur administrative, de syst\u00e8me ou d\u2019\u00e9criture devraient noter que les modifications l\u00e9gislatives visant la Loi de l\u2019imp\u00f4t sur le revenu et la Loi sur l\u2019assurance\u2011emploi ont \u00e9t\u00e9 mises en \u0153uvre."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>Employees affected by previous year overpayments due to clerical, administrative, or system errors should note that new legislative changes to the <i>Income Tax and Employment Act</i> have been implemented. The <a href=\"http://intranet.agr.gc.ca/agrisource/eng/newswork/draft-legislative-proposals-overpayments?id=1547647542891&amp;sendfrom=email\">January 17 news@work article</a>\u00a0referenced these draft legislative proposals which would allow an employee to repay only the net amount of the overpayment received in a previous year, rather than the gross amount.</p>\n<p><strong>These changes will apply if all of the following conditions are met:</strong></p>\n<ul>\n<li>The overpayment was made after 2015 because of an administrative, clerical, or system error.</li>\n<li>A T4 was not previously issued with the employee\u2019s correct earnings (that is, with the overpayment removed).</li>\n<li>The overpayment has been repaid by the employee, or an arrangement to repay the net amount has been made.</li>\n</ul>\n<p>These new legislative measures will apply to any overpayment issued/paid within three (3) calendar years preceding the year the overpayment is recorded in Phoenix. Prior to this change, a previous year salary overpayment that was reported after tax slip production was considered a gross overpayment. This meant that statutory deductions were included in the overpayment amount that would have been paid by the employee, and in the refund processed by the Canada Revenue Agency or Revenu Qu\u00e9bec once they reassessed the employee\u2019s tax return.</p>\n<p>Phoenix will now adjust the Federal Income Tax, Qu\u00e9bec Provincial Income Tax, Employment Insurance, and Qu\u00e9bec Parental Insurance Plan deductions at the source, which will reduce the amount of the overpayment owed by the employee. <strong>Note exceptions</strong>: Canada Pension Plan and Qu\u00e9bec Pension Plan contributions cannot be adjusted to reduce the overpayment at this time. These will be reassessed by CRA or Revenu Qu\u00e9bec.</p>\n<p>Please refer to the <a href=\"http://www.gcpedia.gc.ca/wiki/Phoenixhome?setlang=en&amp;uselang=en\">Tax slips amendments schedule for employees</a> for the dates that amended tax slips will be available to you in Phoenix or the <a href=\"http://gcintranet.tpsgc-pwgsc.gc.ca/gc/rem/awr-cwa-eng.html\">Compensation Web Applications</a>.</p>\n<p>Please note that the amounts that were deducted from your payment may not be the exact same amounts that will be reflected on your overpayment as the following will be taken into consideration when calculating the adjustments: </p>\n<ul>\n<li>Year to date tax deductions</li>\n<li>Year to date earnings</li>\n<li>Filing status (for example, province of work and residence)</li>\n<li>Adjustments (for example, pension adjustment)</li>\n<li>Exemptions</li>\n<li>Tax credits\u00a0</li>\n</ul>\n<p>In some cases, if your adjusted annual earnings are above the <a href=\"https://www.canada.ca/en/revenue-agency/services/tax/businesses/topics/payroll/payroll-deductions-contributions/employment-insurance-ei/ei-premium-rates-maximums.html\">maximum EI insurable earnings</a> of that year, the EI premiums withheld on your original payment may not be refunded.</p>\n<h2><strong>Additional resources</strong></h2>\n<p>For more information, please refer to the following resources:</p>\n<ul>\n<li><a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/systeme-paye-employes-pay-system-employees/feuillet-impot-tax-slip-eng.html#multipletaxslips\">Accessing and managing your tax slips</a>: Explains how the pay system issues tax slips to current and former public service employees. Find out what to do if you receive multiple or amended tax slips.</li>\n<li><a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/systeme-paye-employes-pay-system-employees/trop-payes-overpayments-eng.html\">Overpayments and options to repay them</a>: Provides information about overpayments and the new flexible repayment options available to you.</li>\n<li><a href=\"https://www.canada.ca/en/revenue-agency/campaigns/about-canada-revenue-agency-cra/frequently-asked-questions-tax-implications-phoenix-payroll-issues.html\">Frequently Asked Questions \u2013 2018 Tax Implications of Phoenix payroll issues</a>: Provides information, questions and answers about the tax implications of the Phoenix payroll issues.</li>\n<li><a href=\"https://www.canada.ca/en/revenue-agency/programs/about-canada-revenue-agency-cra/federal-government-budgets/budget2019.html\">Salary overpayments made in error</a>: Questions and answers regarding the January 15, 2019, Government released proposed legislative changes to the Income Tax Act, Canada Pension Plan Act, and Employment Insurance Act.</li>\n</ul>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Les employ\u00e9s ayant re\u00e7u un trop-pay\u00e9 au cours d\u2019une ann\u00e9e ant\u00e9rieure \u00e0 la suite d\u2019une erreur administrative, de syst\u00e8me ou d\u2019\u00e9criture devraient noter que les modifications l\u00e9gislatives visant la <em>Loi de l\u2019imp\u00f4t sur le revenu</em> et la <em>Loi sur l\u2019assurance\u2011emploi</em> ont \u00e9t\u00e9 mises en \u0153uvre. L\u2019article publi\u00e9 dans le bulletin <a href=\"http://intranet.agr.gc.ca/agrisource/fra/nouvelleslouvrage/dispositions-legislatives-proposees-trop-payes?id=1547647542891\">nouvelles@l\u2019ouvrage du 17 janvier 2019</a> d\u00e9crivait ces propositions l\u00e9gislatives qui permettraient \u00e0 un employ\u00e9 de rembourser seulement le montant net du paiement en trop re\u00e7u au cours d\u2019une ann\u00e9e ant\u00e9rieure, plut\u00f4t que le montant brut.</p>\n<p><strong>Ces modifications s\u2019appliquent si toutes les conditions suivantes sont r\u00e9unies :</strong></p>\n<ul>\n<li>Le trop-pay\u00e9 a \u00e9t\u00e9 effectu\u00e9 apr\u00e8s 2015 \u00e0 la suite d\u2019une erreur administrative, d\u2019\u00e9criture ou de syst\u00e8me.</li>\n<li>Un feuillet T4 indiquant le bon montant de r\u00e9mun\u00e9ration de l\u2019employ\u00e9 n\u2019a pas \u00e9t\u00e9 produit pr\u00e9c\u00e9demment (c\u2019est-\u00e0-dire, sans le paiement en trop).</li>\n<li>Le trop-pay\u00e9 a \u00e9t\u00e9 rembours\u00e9 par l\u2019employ\u00e9 ou une entente a \u00e9t\u00e9 conclue pour rembourser le montant net.</li>\n</ul>\n<p>Les nouvelles mesures l\u00e9gislatives s\u2019appliqueront \u00e0 tout trop-pay\u00e9 vers\u00e9 dans les trois (3) ann\u00e9es civiles pr\u00e9c\u00e9dant l\u2019ann\u00e9e o\u00f9 le trop-pay\u00e9 a \u00e9t\u00e9 enregistr\u00e9 dans Ph\u00e9nix<strong>. </strong>Avant cette modification, un trop-pay\u00e9 salarial d\u2019une ann\u00e9e ant\u00e9rieure d\u00e9clar\u00e9 apr\u00e8s la production des feuillets d\u2019imp\u00f4t \u00e9tait consid\u00e9r\u00e9 comme un trop-pay\u00e9 brut. Cela signifie que les retenues obligatoires \u00e9taient incluses dans le montant du trop-pay\u00e9 qui aurait \u00e9t\u00e9 pay\u00e9 par l\u2019employ\u00e9 et dans le remboursement trait\u00e9 par l\u2019Agence du revenu du Canada ou Revenu Qu\u00e9bec apr\u00e8s le r\u00e9examen de la d\u00e9claration de revenus de l\u2019employ\u00e9.</p>\n<p>Ph\u00e9nix rajustera dor\u00e9navant \u00e0 la source les retenues de l\u2019imp\u00f4t f\u00e9d\u00e9ral sur le revenu, de l\u2019imp\u00f4t sur le revenu du Qu\u00e9bec, de l\u2019assurance-emploi et du R\u00e9gime qu\u00e9b\u00e9cois d\u2019assurance parentale, ce qui r\u00e9duira le montant du paiement en trop que doit rembourser l\u2019employ\u00e9. <strong>Notez les exceptions </strong>: Les cotisations au R\u00e9gime de pensions du Canada et au R\u00e9gime de rentes du Qu\u00e9bec ne peuvent \u00eatre rajust\u00e9es pour r\u00e9duire le trop-pay\u00e9 \u00e0 ce moment-ci. Celles-ci seront r\u00e9\u00e9valu\u00e9es par l\u2019ARC ou Revenu Qu\u00e9bec.</p>\n<p>Veuillez consulter le <a href=\"http://www.gcpedia.gc.ca/wiki/PhoenixhomeFRA?setlang=fr&amp;uselang=fr\">Calendrier des modifications aux feuillets de renseignements fiscaux pour les employ\u00e9s</a> pour d\u00e9terminer \u00e0 quelle date les feuillets modifi\u00e9s seront accessibles dans Ph\u00e9nix ou les <a href=\"http://gcintranet.tpsgc-pwgsc.gc.ca/gc/rem/awr-cwa-fra.html\">Applications Web de la r\u00e9mun\u00e9ration</a>.</p>\n<p>Notez bien que les montants d\u00e9duits de votre paiement peuvent ne pas \u00eatre exactement les m\u00eames que ceux qui figurent sur votre paiement en trop, car les \u00e9l\u00e9ments suivants sont pris en compte dans le calcul des rajustements :</p>\n<ul>\n<li>les retenues d\u2019imp\u00f4t depuis le d\u00e9but de l\u2019ann\u00e9e</li>\n<li>les gains cumulatifs de l\u2019ann\u00e9e</li>\n<li>la situation de d\u00e9claration (par exemple, la province de travail et de r\u00e9sidence)</li>\n<li>les rajustements (par exemple, le facteur d\u2019\u00e9quivalence)</li>\n<li>les exemptions</li>\n<li>les cr\u00e9dits d\u2019imp\u00f4t\u00a0</li>\n</ul>\n<p>Dans certains cas, si vos gains annuels rajust\u00e9s sont sup\u00e9rieurs \u00e0 la <a href=\"https://www.canada.ca/fr/agence-revenu/services/impot/entreprises/sujets/retenues-paie/retenues-paie-cotisations/assurance-emploi-ae/taux-cotisation-a-ae-maximums.html\">r\u00e9mun\u00e9ration assurable maximale</a> au titre de l\u2019assurance\u2011emploi de cette ann\u00e9e-l\u00e0, les cotisations d\u2019assurance\u2011emploi retenues sur votre paiement initial ne peuvent \u00eatre rembours\u00e9es.</p>\n<h2><strong>Ressources suppl\u00e9mentaires</strong></h2>\n<p>Pour de plus amples renseignements, veuillez consulter les ressources suivantes :</p>\n<ul>\n<li><a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/systeme-paye-employes-pay-system-employees/feuillet-impot-tax-slip-fra.html\">Consulter et g\u00e9rer vos feuillets de renseignements fiscaux </a>: Explique la fa\u00e7on dont le syst\u00e8me de paye produit les feuillets de renseignements fiscaux des employ\u00e9s actuels et des anciens employ\u00e9s de la fonction publique. D\u00e9couvrez ce qu\u2019il faut faire si vous recevez plusieurs feuillets de renseignements fiscaux ou des feuillets de renseignements fiscaux modifi\u00e9s.</li>\n<li><a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/systeme-paye-employes-pay-system-employees/trop-payes-overpayments-fra.html\">Trop-pay\u00e9s et options de remboursement</a> : Pr\u00e9sente des renseignements sur les trop\u2011pay\u00e9s et les options de remboursement flexibles qui vous sont offertes.</li>\n<li><a href=\"https://www.canada.ca/fr/agence-revenu/campagnes/a-propos-agence-revenu-canada-arc/foire-questions-repercussions-fiscales-liees-problemes-systeme-paye-phenix.html\">Foire aux questions \u2013 R\u00e9percussions fiscales li\u00e9es aux probl\u00e8mes du syst\u00e8me de paye Ph\u00e9nix pour 2018</a> : Fournit des renseignements et des questions et r\u00e9ponses sur les r\u00e9percussions fiscales li\u00e9es aux probl\u00e8mes du syst\u00e8me de paye Ph\u00e9nix.</li>\n<li><a href=\"https://www.canada.ca/fr/agence-revenu/programmes/a-propos-agence-revenu-canada-arc/budgets-gouvernement-federal/budget2019.html\">Salaires vers\u00e9s en trop par erreur</a> : Questions et r\u00e9ponses concernant les modifications \u00e0 la <em>Loi de l\u2019imp\u00f4t sur le revenu</em>, \u00e0 la <em>Loi sur l\u2019assurance-emploi </em>et \u00e0 la <em>Loi sur le r\u00e9gime de pensions du Canada</em> propos\u00e9es par le gouvernement le 15 janvier 2019.</li>\n</ul>\n<p>\u00a0</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Reminder - Legislative Measures to Previous Year Overpayments",
        "fr": "Rappel - Mesures l\u00e9gislatives concernant les trop-pay\u00e9s d\u2019ann\u00e9es ant\u00e9rieures"
    },
    "news": {
        "date_posted": {
            "en": "2019-04-11",
            "fr": "2019-04-11"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Public Services and Procurement Canada",
            "fr": "Services publics et Approvisionnement Canada"
        }
    }
}