{
    "dcr_id": "1569344088721",
    "lang": "en",
    "title": {
        "en": "Reminder - Mid-Year Review Due by October 25: Tips for Successful Discussions",
        "fr": "Rappel - Examen de mi-exercice - \u00e9ch\u00e9ance le 25 octobre : Conseils pour la r\u00e9ussite des discussions "
    },
    "modified": "2019-10-02 00:00:00.0",
    "issued": "2019-09-26 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-09-24",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-09-26",
            "fr": "2019-09-26"
        },
        "modified": {
            "en": "2019-10-02",
            "fr": "2019-10-16"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Reminder - Mid-Year Review Due by October 25: Tips for Successful Discussions",
            "fr": "Rappel - Examen de mi-exercice - \u00e9ch\u00e9ance le 25 octobre : Conseils pour la r\u00e9ussite des discussions "
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Mid-year is another period in the performance management cycle to check in, reflect on your progress, and discuss if your performance agreement objectives need to be realigned based on shifting priorities.",
            "fr": "L'examen de mi-exercice est une autre phase qui fait partie du cycle de la gestion du rendement permettant de faire le point, de r\u00e9fl\u00e9chir sur les progr\u00e8s accomplis et d'entamer une discussion afin de d\u00e9terminer si les objectifs de rendement doivent \u00eatre r\u00e9orient\u00e9 en fonctions des priorit\u00e9s changeantes."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>Mid-year is another period in the performance management cycle to check in, reflect on your progress, and discuss if your performance agreement objectives need to be realigned based on shifting priorities.</p>\n<p><b>All </b>2019-2020 mid-year performance review exercises must be completed by <strong>Friday,</strong> <strong>October 25.</strong></p>\n<p><strong>Employee performance assessments</strong></p>\n<ul>\n<li>Complete in the <a href=\"https://portal-portail.tbs-sct.gc.ca/home-eng.aspx\">Public Service Performance Management Application</a></li>\n<li>Questions? Send an email to <a href=\"mailto:aafc.performance-rendement.aac@canada.ca\">aafc.performance-rendement.aac@canada.ca</a>.</li>\n</ul>\n<p><strong>Executive performance assessments</strong></p>\n<ul>\n<li>Complete in the <a href=\"https://talent.tbs-sct.gc.ca/etms-sgtcs/login-eng.aspx\">Executive Talent Management System</a></li>\n<li>Questions? Send an email to <a href=\"mailto:aafc.egs-sgd.aac@canada.ca\">aafc.egs-sgd.aac@canada.ca</a>.</li>\n</ul>\n<h2><strong>Tips to help you have targeted mid-year discussions</strong></h2>\n<p><strong>Managers</strong></p>\n<ol>\n<li><strong><strong>Consider adjusting work objectives</strong>:<strong> </strong></strong>Reflect any changes that could impact the employee\u2019s objectives and discuss how those changes will affect the year moving forward.</li>\n<li><strong><strong>Record progress made against set work objectives: </strong></strong>Is the employee on track to meet their objectives? What support might they require over the coming months? What challenges are they facing in achieving their objectives?</li>\n<li><strong><strong>Provide candid, constructive criticism and ask for feedback</strong>: </strong>Talk about what is working well and what areas need improvement. Be open to receiving feedback from your employees to help keep lines of communication open.</li>\n</ol>\n<p><strong>Employees</strong></p>\n<ol>\n<li><strong><strong>Reflect on your performance to date: </strong></strong>Are you on track to meet your objectives? What support do you require from your manager to help you meet your objectives?</li>\n<li><strong>Review and update individual learning activities:</strong> Are mandatory training items reflected? Use the <a href=\"http://intranet.agr.gc.ca/agrisource/eng/human-resources/performance-management/learning-and-development-plan-guide?id=1287603193878#step3\">70-20-10 rule</a> when considering new learning activities and talk to your manager about opportunities for development.</li>\n<li><strong>Be fully engaged during the discussion: </strong>This is an opportunity to review your achievements, areas for improvement and career goals. Remain open to feedback, as this will help your effectiveness at work and your career development.</li>\n</ol>\n<h2><strong>Learn more</strong></h2>\n<ul>\n<li><a href=\"http://intranet.agr.gc.ca/agrisource/eng/human-resources/performance-management/mid-year-review-ongoing-discussions-and-feedback?id=1562618716702\">Mid-year review: ongoing discussions and feedback</a></li>\n<li><a href=\"https://intranet.canada.ca/hr-rh/ptm-grt/index-eng.asp\">Performance and talent management</a></li>\n<li><a href=\"https://intranet.canada.ca/hr-rh/ptm-grt/pm-gr/pmc-dgr/ldp-pap-eng.asp\">Learning and development plan</a></li>\n<li><a href=\"https://intranet.canada.ca/hr-rh/ptm-grt/pm-gr/pmc-dgr/support-soutien-eng.asp\">Supporting employees</a></li>\n<li><a href=\"https://collab.agr.gc.ca/co/ex-comm-ex/SitePages/Home.aspx\">EX Community</a></li>\n</ul>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/PerformanceManagement2019-EN.png\" class=\"center-block\"></p>\n<ul>\n</ul>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>L\u2019examen de mi-exercice est une autre phase du cycle de gestion du rendement. Il permet de faire le point, de r\u00e9fl\u00e9chir aux progr\u00e8s accomplis et d\u2019entamer une discussion afin de d\u00e9terminer si les objectifs de rendement doivent \u00eatre r\u00e9orient\u00e9s en fonction des priorit\u00e9s changeantes.</p>\n<p><strong>Tous</strong> les examens de mi-exercice de 2019-2020 doivent \u00eatre compl\u00e9t\u00e9s d\u2019ici le <strong>vendredi 25 octobre</strong>.</p>\n<p><strong>\u00c9valuation du rendement des employ\u00e9s</strong></p>\n<ul>\n<li>\u00c0 compl\u00e9ter dans l\u2019<a href=\"https://portal-portail.tbs-sct.gc.ca/home-fra.aspx\">application de Gestion du rendement de la fonction publique</a></li>\n<li>Des questions? Envoyez un courriel \u00e0 <a href=\"mailto:aafc.performance-rendement.aac@canada.ca\">aafc.performance-rendement.aac@canada.ca</a></li>\n</ul>\n<p><strong>\u00c9valuation du rendement des cadres sup\u00e9rieurs</strong></p>\n<ul>\n<li>\u00c0 compl\u00e9ter dans le <a href=\"https://talent.tbs-sct.gc.ca/etms-sgtcs/Connexion-fra.aspx\">Syst\u00e8me de la gestion des talents des cadres sup\u00e9rieurs</a></li>\n<li>Des questions? Envoyez un courriel \u00e0 <a href=\"mailto:aafc.egs-sgd.aac@canada.ca\">aafc.egs-sgd.aac@canada.ca</a></li>\n</ul>\n<h2><strong>Conseils pour vous aider \u00e0 entreprendre des discussions de mi-exercice cibl\u00e9es </strong></h2>\n<p><strong>Gestionnaires</strong></p>\n<ol>\n<li><strong><strong>Modifiez les objectifs de travail en fonction des priorit\u00e9s changeantes (au besoin) : </strong></strong>\n\tComme les priorit\u00e9s \u00e9voluent au cours de l\u2019ann\u00e9e, il est important de tenir compte de tout changement qui pourrait avoir une incidence sur les objectifs de l\u2019employ\u00e9 et de discuter des r\u00e9percussions futures de ces changements.</li>\n<li><strong><strong>Faites le bilan des progr\u00e8s r\u00e9alis\u00e9s par rapport aux objectifs de travail : </strong></strong>L\u2019employ\u00e9 est-il en voie d\u2019atteindre ses objectifs? De quel soutien aura-t-il besoin au cours des prochains mois? Quels sont les d\u00e9fis auxquels il est confront\u00e9 pour atteindre ses objectifs?</li>\n<li><strong><strong>Offrez de la critique constructive franche, et demandez \u00e0 avoir de la r\u00e9troaction : </strong></strong>Qu\u2019est-ce qui fonctionne bien? Quels sont les points \u00e0 am\u00e9liorer et quel soutien est requis? Soyez r\u00e9ceptif aux commentaires de vos employ\u00e9s; cela vous aidera \u00e0 garder les voies de communication ouvertes.</li>\n</ol>\n<p><strong>Employ\u00e9s</strong></p>\n<ol>\n<li><strong><strong>R\u00e9fl\u00e9chissez sur votre rendement \u00e0 ce jour : </strong></strong>\u00cates-vous sur la bonne voie pour atteindre vos objectifs? De quel soutien avez-vous besoin de la part de votre gestionnaire pour vous aider \u00e0 atteindre vos objectifs ou \u00e0 maintenir le cap?</li>\n<li><strong><strong>Passez en revue et mettez \u00e0 jour les activit\u00e9s d\u2019apprentissage : </strong></strong>Les formations obligatoires sont-elles prises en compte? Consultez la R\u00e8gle de <a href=\"http://intranet.agr.gc.ca/agrisource/fra/ressources-humaines/gestion-du-rendement/guide-de-lelaboration-dun-plan-dapprentissage-et-de-perfectionnement?id=1287603193878#three%20\">70-20-10</a> lorsque vous envisagez de nouvelles activit\u00e9s d\u2019apprentissage. Travaillez en collaboration avec votre gestionnaire lorsqu\u2019il s\u2019agit de votre perfectionnement.</li>\n<li><strong><strong>Participez pleinement \u00e0 la discussion : </strong></strong>Accueillez la discussion comme une occasion de passer en revue vos r\u00e9alisations, vos points \u00e0 am\u00e9liorer et vos objectifs de carri\u00e8re. Soyez ouvert \u00e0 la r\u00e9troaction, en reconnaissant qu\u2019elle est essentielle \u00e0 votre efficacit\u00e9 au travail et \u00e0 votre avancement professionnel.</li>\n</ol>\n<h2><strong>Pour en savoir plus</strong></h2>\n<ul>\n<li><a href=\"http://intranet.agr.gc.ca/agrisource/fra/ressources-humaines/gestion-du-rendement/examen-de-mi-exercice-discussions-et-retroaction-continues?id=1562618716702\">Examen de mi-exercice : discussions et r\u00e9troaction continues</a></li>\n<li><a href=\"https://intranet.canada.ca/hr-rh/ptm-grt/index-fra.asp\">Gestion du rendement et des talents</a></li>\n<li><a href=\"https://intranet.canada.ca/hr-rh/ptm-grt/pm-gr/pmc-dgr/ldp-pap-fra.asp\">Plan d\u2019apprentissage et de perfectionnement</a></li>\n<li><a href=\"https://intranet.canada.ca/hr-rh/ptm-grt/pm-gr/pmc-dgr/support-soutien-fra.asp\">Soutenir les employ\u00e9s</a></li>\n<li><a href=\"https://collab.agr.gc.ca/co/ex-comm-ex/SitePages/Home.aspx\">Communaut\u00e9 EX</a></li>\n</ul>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/PerformanceManagement2019-FR.png\" class=\"center-block\"></p>\n<ul>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Reminder - Mid-Year Review Due by October 25: Tips for Successful Discussions",
        "fr": "Rappel - Examen de mi-exercice - \u00e9ch\u00e9ance le 25 octobre : Conseils pour la r\u00e9ussite des discussions"
    },
    "news": {
        "date_posted": {
            "en": "2019-09-26",
            "fr": "2019-09-26"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Human Resources, Corporate Management Branch",
            "fr": "Ressources humaines, Direction g\u00e9n\u00e9rale de la gestion int\u00e9gr\u00e9e"
        }
    }
}