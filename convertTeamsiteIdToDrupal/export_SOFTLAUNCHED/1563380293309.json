{
    "dcr_id": "1563380293309",
    "lang": "en",
    "title": {
        "en": "Director, Disaster Assessment and Analysis",
        "fr": "Directeur, \u00c9valuation et analyse des catastrophes"
    },
    "modified": "2019-07-31 00:00:00.0",
    "issued": "2019-07-17 00:00:00.0",
    "type_name": "intra-intra/empl-empl",
    "node_id": "1279029955397",
    "layout_name": null,
    "dc_date_created": "2019-07-17",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-07-17",
            "fr": "2019-07-17"
        },
        "modified": {
            "en": "2019-07-31",
            "fr": "2019-07-31"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Director, Disaster Assessment and Analysis",
            "fr": "Directeur, \u00c9valuation et analyse des catastrophes"
        },
        "subject": {
            "en": "staffing",
            "fr": "dotation en personnel"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "notice",
            "fr": "avis"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n<p>\u00a0</p>\n<p>Employees of Agriculture and Agri-Food Canada (AAFC) at the EC-08 (or equivalents) or EX minus 1 level are invited to express their interest in an assignment at level or acting opportunity to lead the Disaster Assessment and Analysis Division, in the Business Risk Management (BRM) Directorate of Programs Branch.</p>\n<p>The Disaster Assessment and Analysis Division is responsible for administering the AgriRecovery Program through the research and analysis of the impacts of real or potential disasters affecting agricultural producers across Canada. This opportunity is a chance for you to be exposed to a wide range of issues dealt with by the Disaster Assessment and Analysis Division and gain a thorough understanding of provincial and federal agriculture programs and how they respond to disaster events. The selected candidate will also be involved in the development of programs, including their implementation and monitoring.</p>\n<p>\u00a0</p>\n<p><strong>Duties</strong></p>\n<p>Responsibilities will include: preparing briefings for senior management and the Minister; managing the team responsible for policy analysis on current and emerging issues impacting one or more sectors or regions; and developing potential programs to respond to these issues in partnership with provincial governments.</p>\n<p>\u00a0</p>\n<p><strong>Language Requirement: </strong>Various language requirements</p>\n<p>\u00a0</p>\n<p><strong>Education</strong></p>\n<p>If you are at a lower level seeking an acting opportunity, you are required to meet the minimum education of graduation with a <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#deg\">degree</a> from a <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#rpsi\">recognized post-secondary institution</a> with <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#acce\">acceptable</a> <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#spec\">specialization</a> in economics, sociology or statistics.</p>\n<p>If you are at an equivalent level seeking an assignment opportunity, you are required to meet the minimum education requirement of graduation with a <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#deg\">degree</a> from a <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#rpsi\">recognized post-secondary institution</a>.</p>\n<p>\u00a0</p>\n<p><strong>Experience</strong></p>\n<p>Extensive experience* in policy development and/or program delivery.</p>\n<p>Extensive experience* in formulating recommendation on policy options and strategies.</p>\n<p>Experience in managing human resources and financial management.</p>\n<p>Experience in establishing collaborative relationships with government officials (international, federal, provincial or municipal), businesses, or external stakeholders.</p>\n<p>Experience in conducting economic analysis and providing strategic information and advice to senior managers.<br>\nExtensive experience* in preparing and coordinating comprehensive briefing material (such as reports, briefing notes, presentations, program or policy documents) for senior officials (Director General and above).</p>\n<p><em>* Extensive experience is experience gained over a minimum of 3 years.</em></p>\n<p>\u00a0</p>\n<p><strong>Knowledge </strong></p>\n<p>Knowledge of production and supply management in the Agricultural sector.</p>\n<p>\u00a0</p>\n<p><strong>Abilities</strong></p>\n<p>Ability to communicate effectively orally.<br>\nAbility to communicate effectively in writing.</p>\n<p><strong>Competencies</strong></p>\n<p>Judgement<br>\nLeadership</p>\n<p>Strategic Thinking<br>\nRelationship building</p>\n<p><strong><br>Asset experience and knowledge</strong></p>\n<p>Experience negotiating with stakeholders.<br>\nKnowledge of business risk management theories and practices.</p>\n<p><strong><br>Operational Requirements</strong></p>\n<p>Willingness and ability to work overtime.</p>\n<p>Willingness and ability to travel.</p>\n<p><strong><br>Conditions of Employment</strong></p>\n<p>Secret Security Clearance</p>\n<p><strong><br>Applications</strong></p>\n<p>If you are an indeterminate EX equivalent employee and are interested in this assignment, please provide:</p>\n<ul>\n<li>a short letter (2 pages maximum) expressing why you are interested in the position and the skills, experience and knowledge you would bring to the position of <strong>Director, Disaster Assessment and Analysis, BRM ; </strong>and</li>\n<li>your resume.</li>\n</ul>\n<p><br>\nIf you are not at an EX equivalent level and are interested in an acting opportunity, please provide:</p>\n<ul style=\"list-style-type: square;\">\n<li>a cover letter explaining clearly how you meet the education and experiences related to this opportunity<strong>; </strong>and</li>\n</ul>\n<ul style=\"list-style-type: square;\">\n<li>your resume.</li>\n</ul>\n<p>\u00a0</p>\n<p>Send all documents to Julie Desroches at julie.desroches@canada.ca by Wednesday August 7th, 2019.</p>\n<p><br>\nResumes must clearly identify your substantive group and level.</p>\n<p>If you have any questions or would like more information, please contact Chris Levac at chris.levac@canada.ca or 613-773-2042.</p>\n<p><strong><br>Notes: </strong></p>\n<ul>\n<li>Employees at an indeterminate EX equivalent level will be considered first for an assignment opportunity. Should there be no qualified at level candidates, employees at an EX minus one level will be considered for an acting opportunity.</li>\n<li>The opportunity is for a 1 year acting or assignment with the possibility of an extension.</li>\n<li>Communication for this process will be sent via email.</li>\n<li>Candidates must have their manager\u2019s approval prior to applying for this opportunity.</li>\n<li>A pool of qualified candidates may be established and may be used to staff similar positions.</li>\n<li>Candidates may be assessed through an interview.</li>\n<li>A written exam may be administered.</li>\n<li>Reference checks may be sought.</li>\n</ul>\n<p>\u00a0</p>\n",
        "fr": "\n<p>\u00a0</p>\n<p>La Direction des programmes de gestion des risques de l'entreprise (GRE) au sein de la Direction g\u00e9n\u00e9rale des programmes (DGP) est \u00e0 la recherche d\u2019une personne int\u00e9ress\u00e9e \u00e0 relever de nouveaux d\u00e9fis dans un milieu de travail dynamique dans le cadre d\u2019une affectation d\u2019un an, avec possibilit\u00e9 de prolongation.</p>\n<p>L\u2019\u00e9quipe de l\u2019\u00e9valuation et analyse des catastrophes est responsable de l\u2019administration du programme Agri-relance en faisant de la recherche et de l\u2019analyse sur les impacts de catastrophes r\u00e9elles ou potentielles affectant les producteurs agricoles \u00e0 travers le pays. Cette opportunit\u00e9 vous permettra de vous familiariser avec les multiples enjeux g\u00e9r\u00e9s par l\u2019\u00e9quipe de l\u2019\u00e9valuation et analyse des catastrophes et d\u2019approfondir votre compr\u00e9hension des programmes agricoles provinciaux et f\u00e9d\u00e9raux et comment ceux-ci sont utilis\u00e9s pour r\u00e9pondre \u00e0 des catastrophes. Le candidat s\u00e9lectionn\u00e9 sera aussi impliqu\u00e9 dans le d\u00e9veloppement de programmes, incluant la mise en \u0153uvre et la surveillance.</p>\n<p><strong><br>Responsabilit\u00e9s</strong></p>\n<p>Les responsabilit\u00e9s li\u00e9es \u00e0 ce poste inclues : la pr\u00e9paration de note d\u2019information pour la haute gestion et pour la Ministre ; la gestion de l\u2019\u00e9quipe responsable de l\u2019analyse de politiques portant sur les enjeux existant et \u00e9mergeant aillant un impact sur un ou plusieurs secteurs ou r\u00e9gions ; et le d\u00e9veloppement, en partenariat avec les gouvernements provinciaux, de programmes pouvant r\u00e9pondre \u00e0 ces enjeux.</p>\n<p><strong><br>\u00c9ducation</strong></p>\n<p>Si vous \u00eates un employ\u00e9 qui n\u2019est pas \u00e0 niveau \u00e0 la recherche d\u2019une nomination int\u00e9rimaire, vous devez rencontrer le niveau d\u2019\u00e9ducation minimum suivant: un <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/dotation/normes-qualification/centrale.html#Dipl\">grade</a> d\u2019un <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/dotation/normes-qualification/centrale.html#eepr\">\u00e9tablissement d\u2019enseignement postsecondaire reconnu</a> avec <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/dotation/normes-qualification/centrale.html#spec\">sp\u00e9cialisation</a> <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/dotation/normes-qualification/centrale.html#acc\">acceptable</a> en \u00e9conomie, en sociologie ou en statistique.</p>\n<p>Si vous \u00eates un employ\u00e9 \u00e0 niveau \u00e0 la recherche d\u2019une affectation, vous devez rencontrer le niveau d\u2019\u00e9ducation minimum suivant : un <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/dotation/normes-qualification/centrale.html#Dipl\">grade</a> d\u2019un <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/dotation/normes-qualification/centrale.html#eepr\">\u00e9tablissement d\u2019enseignement postsecondaire reconnu</a>.</p>\n<p><strong><br>Profil linguistique exig\u00e9 :</strong> Profils vari\u00e9s</p>\n<p><strong><br>Exp\u00e9rience</strong></p>\n<p>Exp\u00e9rience vaste* dans la conception et/ou la prestation de programmes ou de services.</p>\n<ol>\n<li>vaste* dans la formulation de recommandations sur des questions de politiques et de strat\u00e9gies.</li>\n</ol>\n<p>Exp\u00e9rience en gestion des ressources humaines et des finances.</p>\n<p>Exp\u00e9rience de l\u2019\u00e9tablissement de relations de collaborations avec des employ\u00e9es du gouvernement (international, f\u00e9d\u00e9ral, provincial ou municipal), des entreprises ou des intervenants externes.</p>\n<p>Exp\u00e9rience de l\u2019analyse \u00e9conomique et de la prestation de renseignements et de conseils strat\u00e9giques aux cadres sup\u00e9rieurs.</p>\n<p>Exp\u00e9rience vaste* de la pr\u00e9paration de mat\u00e9riel d\u2019information (p. ex., rapports, notes d\u2019information, pr\u00e9sentations, document de programme ou de politique) \u00e0 l\u2019intention de cadres sup\u00e9rieurs (directeurs g\u00e9n\u00e9raux et postes de niveau sup\u00e9rieur).</p>\n<p><em>* Exp\u00e9rience vaste est de l\u2019exp\u00e9rience acquise sur un minimum de 3 ans.</em></p>\n<p><strong><br>Connaissance</strong></p>\n<p>Connaissance de la production et de la gestion de l\u2019offre dans le secteur agricole.</p>\n<p><strong><br>Capacit\u00e9s</strong></p>\n<p>Capacit\u00e9 de communiquer efficacement \u00e0 l\u2019oral</p>\n<p>Capacit\u00e9 de communiquer efficacement par \u00e9crit</p>\n<p><strong><br>Comp\u00e9tences</strong></p>\n<p>Jugement</p>\n<p>Leadership</p>\n<p>Pens\u00e9e strat\u00e9gique</p>\n<p>\u00c9tablissement de relations</p>\n<p><strong><br>Exp\u00e9rience et connaissances constituant un atout</strong></p>\n<p>Exp\u00e9rience dans la n\u00e9gociation avec des intervenants.<br>\nConnaissance des th\u00e9ories et pratiques dans le domaine de gestion des risques de l'entreprise.</p>\n<p><strong><br>Exigences op\u00e9rationnelles </strong></p>\n<p>Consentir \u00e0 faire des heures suppl\u00e9mentaires et \u00eatre en mesure de le faire</p>\n<p>Consentir \u00e0 voyager et \u00eatre en mesure de le faire\u00a0</p>\n<p><strong><br>Condition d\u2019emploi: Cote de s\u00e9curit\u00e9 </strong>Secr\u00e8te</p>\n<p><strong><br>Applications</strong></p>\n<p>Si vous \u00eates un employ\u00e9 ind\u00e9termin\u00e9 \u00e0 un niveau EX \u00e9quivalent et que cette possibilit\u00e9 d\u2019affectation vous int\u00e9resse, veuillez fournir :</p>\n<ul>\n<li>\n<p>une courte lettre (2 pages maximum) expliquant pourquoi ce poste vous int\u00e9resse et les habilet\u00e9s, les exp\u00e9riences et les connaissances que vous apporterez au poste de Directeur, \u00c9valuation et analyse des catastrophes, Direction des programmes de gestion des risques de l'entreprise ; et</p>\n</li>\n</ul>\n<ul>\n<li>\n<p>votre curriculum vitae.</p>\n</li>\n</ul>\n<p>Si vous n\u2019\u00eates pas \u00e0 un niveau EX \u00e9quivalent et que cette possibilit\u00e9 d\u2019int\u00e9rimaire vous int\u00e9resse, veuillez fournir :</p>\n<ul>\n<li>\n<p>une lettre expliquant clairement comment vous rencontrez l\u2019\u00e9ducation et les exp\u00e9riences li\u00e9es \u00e0 cette opportunit\u00e9<strong> </strong>; et</p>\n</li>\n</ul>\n<ul>\n<li>\n<p>votre curriculum vitae.</p>\n</li>\n</ul>\n<p><br>\nVeuillez envoyer tous les documents \u00e0 Julie Desroches \u00e0 julie.desroches@canada.ca d\u2019ici mercredi le 7 ao\u00fbt, 2019.</p>\n<p>Veuillez indiquer clairement votre groupe et niveau de titularisation dans votre curriculum vitae.</p>\n<p>\u00a0</p>\n<p>Si vous avez des questions ou si vous souhaitez obtenir de plus amples renseignements, n\u2019h\u00e9sitez pas \u00e0 communiquer avec Chris Levac par courriel \u00e0 l\u2019adresse chris.levac@canada.ca ou par t\u00e9l\u00e9phone au 613-773-2042.</p>\n<p>\u00a0</p>\n<p><strong>Notes: </strong></p>\n<ul>\n<li>Les employ\u00e9s ind\u00e9termin\u00e9s \u00e0 un niveau EX \u00e9quivalent seront les premiers \u00e0 \u00eatre consid\u00e9r\u00e9s pour une opportunit\u00e9 d\u2019affection. S\u2019il n\u2019y a pas de candidats qualifi\u00e9s \u00e0 niveau, les employ\u00e9es \u00e0 un niveau EX moins 1 seront consid\u00e9r\u00e9s pour une opportunit\u00e9 int\u00e9rimaire.</li>\n<li>Il s\u2019agit d\u2019une affection ou d\u2019une nomination int\u00e9rimaire d\u2019un an avec possibilit\u00e9 de prolongation.</li>\n<li>Toute communication relative \u00e0 ce processus sera faite par courriel.</li>\n<li>Les candidates doivent obtenir l\u2019approbation de leur gestionnaire avant de postuler pour cette opportunit\u00e9.</li>\n<li>Un bassin de candidats qualifi\u00e9s pourrait \u00eatre \u00e9tablie et utilis\u00e9 pour doter des postes semblables.</li>\n<li>Les candidats peuvent \u00eatre \u00e9valu\u00e9s lors d\u2019une entrevue.</li>\n<li>Un examen \u00e9crit pourrait \u00eatre administr\u00e9.</li>\n<li>Une v\u00e9rification des r\u00e9f\u00e9rences pourrait \u00eatre faite.</li>\n</ul>\n<p>\u00a0</p>\n\n"
    },
    "breadcrumb": {
        "en": "Director, Disaster Assessment and Analysis",
        "fr": "Directeur, \u00c9valuation et analyse des catastrophes"
    },
    "empl": {
        "type": {
            "en": "Assignment, Acting",
            "fr": "Affectation, Int\u00e9rimaire"
        },
        "classification": {
            "en": "EC-8 and equivalent",
            "fr": "EC-8 et \u00e9quivalent"
        },
        "branch": {
            "en": "Programs Branch",
            "fr": "Direction g\u00e9n\u00e9rale des programmes"
        },
        "locations": {
            "en": "Ottawa, Ontario",
            "fr": "Ottawa (Ontario)"
        },
        "date_closing": {
            "en": "2019-08-07",
            "fr": "2019-08-07"
        },
        "open_to": {
            "en": "All AAFC employees in the NCR in an EX equivalent or EX minus 1 level.",
            "fr": "Tous les employ\u00e9s d\u2019AAC situ\u00e9s dans la RCN \u00e0 un niveau EX \u00e9quivalent ou \u00e0 un niveau EX moins 1."
        },
        "name": {
            "en": "Julie Desroches",
            "fr": "Julie Desroches"
        },
        "email": {
            "en": "julie.desroches@canada.ca",
            "fr": "julie.desroches@canada.ca"
        }
    }
}