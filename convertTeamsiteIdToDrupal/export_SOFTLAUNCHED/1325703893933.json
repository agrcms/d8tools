{
    "dcr_id": "1325703893933",
    "lang": "en",
    "title": {
        "en": "Information Security Part 1: Identifying Sensitive Information",
        "fr": "S\u00e9curit\u00e9 de l'information, 1\u00e8re partie : la d\u00e9termination des renseignements sensibles"
    },
    "modified": "2020-08-31 00:00:00.0",
    "issued": "2012-01-05 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1485266766613",
    "layout_name": "1 column",
    "dc_date_created": "2012-01-04",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2012-01-05",
            "fr": "2012-01-05"
        },
        "modified": {
            "en": "2020-08-31",
            "fr": "2020-08-31"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Information Security Part 1: Identifying Sensitive Information",
            "fr": "S\u00e9curit\u00e9 de l'information, 1\u00e8re partie : la d\u00e9termination des renseignements sensibles"
        },
        "subject": {
            "en": "security",
            "fr": "s\u00e9curit\u00e9"
        },
        "description": {
            "en": "Most government information is adequately protected through good, basic information management and physical and materiel management procedures without classification or protection. The Security policy requires our department to identify the relatively limited amount of government information that is sensitive and therefore merits additional protection.",
            "fr": "La plus grande partie de l'information gouvernementale est ad\u00e9quatement prot\u00e9g\u00e9e au moyen de bonnes proc\u00e9dures \u00e9l\u00e9mentaires de gestion de l'information et de gestion physique et mat\u00e9rielle sans qu'il soit besoin de classification ou de protection. Toutefois, la politique en mati\u00e8re de s\u00e9curit\u00e9 exige que notre Minist\u00e8re identifie les renseignements gouvernementaux de nature sensible - ils sont relativement rares - et qui, de fait, m\u00e9ritent une protection suppl\u00e9mentaire."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Most government information is adequately protected through good, basic information management and physical and materiel management procedures without classification or protection. The Security policy requires our department to identify the relatively limited amount of government information that is sensitive and therefore merits additional protection. Identifying sensitive information is related directly to the exemption and exclusion criteria of the <cite>Access to Information Act</cite> and the <cite>Privacy Act</cite> which establish a legal authority over information to which departments may deny access to the public.</p>\n\n<p>Agriculture and Agri-Food Canada (AAFC) has a corporate guide that provides direction about the management of the information that we wish to safeguard. <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/information_management/guide_to_handling_sensitive_information-eng.doc\">The Guide to Classification and Handling of Sensitive Information (Word)</a> identifies:</p>\n\n<ul>\n\t<li>The types of information that are considered sensitive;</li>\n\t<li>How to determine the level of sensitivity, applying the injury test;</li>\n\t<li>How to mark information to show the minimum security standard to apply; and,</li>\n\t<li>How to declassify or downgrade information.</li>\n</ul>\n\n<p>The levels of sensitivity are as follows:</p>\n\n<p><strong>Classified Information</strong> - information related to the national interest that may qualify for an exemption or exclusion under the <cite>Access to Information Act</cite> or <cite>Privacy Act</cite>, and the compromise of which would reasonably be expected to cause injury to the national interest. Levels of classified information are:</p>\n\n<ul>\n\t<li>Confidential - level of injury if unauthorized disclosure occurs is minimal;\n  <ul>\t\n   <li>Federal-provincial affairs.</li>\n  </ul>\n </li>\n\t<li>Secret - level of injury if unauthorized disclosure occurs is serious;\n  <ul>\t\n   <li>AAFC input to the national budget prior to its official release.</li>\n  </ul>\n </li>\n\t<li>Top Secret - level of injury if unauthorized disclosure occurs is exceptionally grave;\n  <ul>\t\n   <li>Extremely sensitive information relating to international affairs.</li>\n  </ul>\n </li>\n</ul>\n\n<p><strong>Protected Information</strong> - information related to other than the national interest that may qualify for an exemption or exclusion under the <cite>Access to Information Act</cite> or <cite>Privacy Act</cite>, and the compromise of which would reasonably be expected to cause injury to a non-national interest (i.e. injury to an individual or corporation). Levels of protected information are:</p>\n\n<ul>\n\t<li>Protected A - level of injury if unauthorized disclosure occurs is minimal;\n  <ul>\t\n   <li>Social Insurance Number or Personnel Record Identifier.</li>\n  </ul>\n </li>\n\t<li>Protected B - level of injury if unauthorized disclosure occurs is serious;\n  <ul>\t\n   <li>Business- confidential records from producers, industry partners.</li>\n  </ul>\n </li>\n\t<li>Protected C - level of injury if unauthorized disclosure occurs is exceptionally grave.\n  <ul>\t\n   <li>Information that could cause loss of life or threaten safety of informants (note: this level is not commonly used in AAFC)</li>\n  </ul>\n </li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n \n<p>La plus grande partie de l'information gouvernementale est ad\u00e9quatement prot\u00e9g\u00e9e au moyen de bonnes proc\u00e9dures \u00e9l\u00e9mentaires de gestion de l'information et de gestion physique et mat\u00e9rielle sans qu'il soit besoin de classification ou de protection. Toutefois, la politique en mati\u00e8re de s\u00e9curit\u00e9 exige que notre Minist\u00e8re identifie les renseignements gouvernementaux de nature sensible - ils sont relativement rares - et qui, de fait, m\u00e9ritent une protection suppl\u00e9mentaire. L'identification de l'information sensible est en lien direct avec les crit\u00e8res d'exception et d'exclusion de la <cite>Loi sur l'acc\u00e8s \u00e0 l'information</cite> et de la <cite>Loi sur la protection des renseignements personnels</cite>, en vertu desquelles les minist\u00e8res sont habilit\u00e9s \u00e0 refuser l'acc\u00e8s \u00e0 certains renseignements au public.</p>\n\n<p>Agriculture et Agroalimentaire Canada (AAC) poss\u00e8de un guide g\u00e9n\u00e9ral dans lequel se trouvent des instructions \u00e0 propos de la gestion de l'information que nous d\u00e9sirons prot\u00e9ger. <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/information_management/guide_to_handling_sensitive_information-fra.docx\">Le Guide sur la classification et le traitement des renseignements de nature d\u00e9licate (Word)</a> pr\u00e9cise\u00a0:</p>\n\n<ul>\n\t<li>les types de renseignements qui sont consid\u00e9r\u00e9s comme sensibles;</li>\n\t<li>comment d\u00e9terminer le niveau de sensibilit\u00e9 en appliquant le crit\u00e8re subjectif;</li>\n\t<li>comment marquer les renseignements pour indiquer la norme de s\u00e9curit\u00e9 minimale \u00e0 appliquer;</li>\n\t<li>comment d\u00e9classifier ou d\u00e9classer de l'information.</li>\n</ul>\n\n<p>Voici les niveaux de sensibilit\u00e9 des renseignements\u00a0:</p>\n\n<p><strong>Les renseignements classifi\u00e9s</strong> renseignements li\u00e9s \u00e0 quelque chose d'autre que l'int\u00e9r\u00eat national, susceptibles d'\u00eatre vis\u00e9s par une exclusion ou une exception en vertu de la <cite>Loi sur l'acc\u00e8s \u00e0 l'information</cite> ou de la <cite>Loi sur la protection des renseignements personnels</cite>, et dont la divulgation sans autorisation risquerait vraisemblablement de porter pr\u00e9judice \u00e0 l'int\u00e9r\u00eat national. Il y a trois niveaux de classification des renseignements\u00a0:</p>\n\n<ul>\n\t<li>Confidentiel\u00a0: la divulgation non autoris\u00e9e peut causer un pr\u00e9judice minimal;\n  <ul>\t\n   <li>par exemple, affaires f\u00e9d\u00e9rales-provinciales.</li>\n  </ul>\n </li>\n\t<li>Secret\u00a0: la divulgation non autoris\u00e9e peut causer un pr\u00e9judice grave;\n  <ul>\t\n   <li>par exemple, renseignements fournis par AAC relativement au budget national avant sa publication officielle.</li>\n  </ul>\n </li>\n\t<li>Tr\u00e8s secret\u00a0: la divulgation non autoris\u00e9e peut causer un pr\u00e9judice exceptionnellement grave;\n  <ul>\t\n   <li>par exemple, renseignements extr\u00eamement sensibles relatifs aux affaires internationales.</li>\n  </ul>\n </li>\n</ul>\n\n<p><strong>Les renseignements prot\u00e9g\u00e9s</strong>\u00a0: renseignements autres que d'int\u00e9r\u00eat national susceptibles d'\u00eatre vis\u00e9s par une exclusion ou une exception en vertu de la <cite>Loi sur l'acc\u00e8s \u00e0 l'information</cite> ou de la <cite>Loi sur la protection des renseignements personnels</cite>, et dont la divulgation sans autorisation risquerait vraisemblablement de porter pr\u00e9judice \u00e0 des int\u00e9r\u00eats non reli\u00e9s \u00e0 l'int\u00e9r\u00eat national (c'est-\u00e0-dire pr\u00e9judice \u00e0 un particulier ou \u00e0 une soci\u00e9t\u00e9). Les niveaux de protection des renseignements sont\u00a0:</p>\n\n<ul>\n\t<li>Prot\u00e9g\u00e9 A\u00a0: la divulgation non autoris\u00e9e peut causer un pr\u00e9judice minimal;\n  <ul>\t\n   <li>par exemple, num\u00e9ro d'assurance sociale ou code d'identification de dossier personnel;</li>\n  </ul>\n </li>\n\t<li>Prot\u00e9g\u00e9 B\u00a0: la divulgation non autoris\u00e9e peut causer un pr\u00e9judice grave;\n  <ul>\t\n   <li>par exemple, dossiers commerciaux confidentiels des producteurs, des partenaires de l'industrie;</li>\n  </ul>\n </li>\n\t<li>Prot\u00e9g\u00e9 C\u00a0: la divulgation non autoris\u00e9e peut causer un pr\u00e9judice exceptionnellement grave;\n  <ul>\t\n   <li>par exemple, renseignements qui pourraient entra\u00eener une perte de vie ou menacer la s\u00e9curit\u00e9 des informateurs (Remarque\u00a0: ce niveau de sensibilit\u00e9 n'est pas utilis\u00e9 couramment par AAC).</li>\n  </ul>\n </li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Information Security Part 1: Identifying Sensitive Information",
        "fr": "S\u00e9curit\u00e9 de l'information, 1\u00e8re partie : la d\u00e9termination des renseignements sensibles"
    }
}