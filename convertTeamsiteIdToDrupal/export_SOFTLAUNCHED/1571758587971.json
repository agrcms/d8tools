{
    "dcr_id": "1571758587971",
    "lang": "en",
    "title": {
        "en": "2020 Gold Harvest Awards - Category Spotlight: Valuing People and Culture",
        "fr": "Prix Moisson d'or de 2020 - En vedette : Valoriser nos personnes et la culture"
    },
    "modified": "2019-10-24 00:00:00.0",
    "issued": "2019-10-24 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-10-22",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-10-24",
            "fr": "2019-10-24"
        },
        "modified": {
            "en": "2019-10-24",
            "fr": "2019-10-24"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "2020 Gold Harvest Awards - Category Spotlight: Valuing People and Culture",
            "fr": "Prix Moisson d'or de 2020 - En vedette : Valoriser nos personnes et la culture"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Do you know a colleague or team who contribute important foundational work at AAFC, often behind the scenes? It's time to recognize their work by nominating them for a Gold Harvest Award.",
            "fr": "Connaissez-vous un coll\u00e8gue ou une \u00e9quipe qui contribue souvent en coulisse au travail fondamental d'importance \u00e0 AAC? Il est temps de reconna\u00eetre leur travail en pr\u00e9sentant leur candidature pour le Prix Moisson d\u2019or."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>Do you know a colleague or team who contribute important foundational work at AAFC, often behind the scenes? It\u2019s time to recognize their work by nominating them for a <strong>Gold Harvest Award</strong>.</p>\n<p>The <strong>Valuing People and Culture category</strong> recognizes individuals and groups who are innovative leaders that promote a healthy, diverse, respectful and welcoming office culture. They support continuous learning and development, or enhance linguistic duality at work.</p>\n<p>Nominations are currently being accepted for the 2020 Gold Harvest Awards. As you consider nominating a colleague or team, the Awards and Recognition team is spotlighting a different category each week to help expand awareness and encourage participation.</p>\n<p>To see the criteria for the Valuing People and Culture Gold Harvest Award in action, consider examples of past recipients in the category:</p>\n<ul>\n<li>\n<p><strong><strong>Jacqueline (Jake) Freeman, STB (2018)</strong></strong></p>\n<ul style=\"list-style-type: circle;\">\n<li>\n<p>Co-chair of the Indigenous Network Circle, Jake works to increase cultural awareness at AAFC, promoting diversity and a positive, respectful work environment for Indigenous Peoples. Through the Indigenous Student Recruitment Initiative, she has been a dedicated mentor for AAFC's Indigenous students and encourages them to pursue an education and career in the sciences.</p>\n</li>\n</ul>\n</li>\n<li>\n<p><strong><strong>Kentville Research and Development Centre Workplace Wellness Group (2017)</strong></strong></p>\n<ul style=\"list-style-type: circle;\">\n<li>\n<p>Management and staff of the Kentville RDC formed the Workplace Wellness Group to provide active leadership in the promotion and support of a healthy and respectful workplace. They organized a succession of creative initiatives to promote diversity and inclusiveness, which have had a tangible effect on staff health and wellness, and inspired other Centres by their example.</p>\n</li>\n</ul>\n</li>\n</ul>\n<p>For detailed information regarding eligibility, selection criteria, nomination requirements and forms, please refer to the Gold Harvest Award Guidelines on the <a href=\"http://intranet.agr.gc.ca/agrisource/eng/human-resources/awards-and-recognition?id=1287491853497\">Awards and Recognition page</a> on AgriSource. If you have any questions regarding the nomination process, please send an email to <a href=\"mailto:aafc.goldharvest-prixmoissondor.aac@canada.ca\">aafc.goldharvest-prixmoissondor.aac@canada.ca</a>.</p>\n<p>Nominate your deserving colleagues today!</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/GoldHarvestAwards2019-EN.png\" class=\"center-block\"></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Connaissez-vous un coll\u00e8gue ou une \u00e9quipe qui contribue souvent en coulisse au travail fondamental d\u2019importance \u00e0 AAC? Il est temps de reconna\u00eetre leur travail en pr\u00e9sentant leur candidature pour le <strong>Prix Moisson d\u2019or</strong>.</p>\n<p>La <strong>cat\u00e9gorie Valoriser nos personnes et la culture</strong> reconna\u00eet les personnes et les groupes faisant partie des leaders innovateurs qui favorisent une culture saine, diversifi\u00e9e, respectueuse et accueillante au travail, qui pr\u00f4nent l\u2019apprentissage et le perfectionnement continus ou qui favorisent la dualit\u00e9 linguistique au travail.</p>\n<p>Nous acceptons actuellement les mises en candidature pour les Prix Moisson d\u2019or 2020. Alors que vous envisagez de mettre en candidature des \u00e9quipes ou des coll\u00e8gues, l\u2019\u00e9quipe des prix et de la reconnaissance mettra en valeur une cat\u00e9gorie diff\u00e9rente chaque semaine pour accro\u00eetre la sensibilisation et encourager la participation.</p>\n<p>Pour illustrer les crit\u00e8res du Prix Moisson d\u2019or dans la cat\u00e9gorie Valoriser nos personnes et la culture, voici des exemples d\u2019anciens laur\u00e9ats dans cette cat\u00e9gorie :</p>\n<ul>\n<li>\n<p><strong><strong>Jacqueline (Jake) Freeman, DGST (2018)</strong></strong></p>\n<ul style=\"list-style-type: circle;\">\n<li>\n<p>Copr\u00e9sidente du Cercle de r\u00e9seautage des employ\u00e9s autochtones, Jake vise \u00e0 accro\u00eetre la sensibilisation culturelle \u00e0 AAC en faisant la promotion de la diversit\u00e9 et d\u2019un milieu de travail positif et respectueux pour les Autochtones. Dans le cadre de l\u2019Initiative de recrutement d\u2019\u00e9tudiants autochtones, elle sert de mentor d\u00e9vou\u00e9e pour les \u00e9tudiants autochtones d\u2019AAC et les encourage \u00e0 poursuivre des \u00e9tudes et une carri\u00e8re en sciences.</p>\n</li>\n</ul>\n</li>\n<li>\n<p><strong><strong>Groupe de bien-\u00eatre au travail du Centre de recherche et de d\u00e9veloppement de Kentville (2017)</strong></strong></p>\n<ul style=\"list-style-type: circle;\">\n<li>\n<p>La direction et le personnel du CDR de Kentville ont form\u00e9 le Groupe de bien-\u00eatre au travail pour assurer un leadership actif dans la promotion et le soutien d\u2019un milieu de travail sain et respectueux. Ils ont organis\u00e9 une s\u00e9rie d\u2019initiatives cr\u00e9atives pour promouvoir la diversit\u00e9 et l\u2019inclusion, qui ont eu un effet tangible sur la sant\u00e9 et le mieux-\u00eatre du personnel, et inspir\u00e9 d\u2019autres centres par leur exemple.</p>\n</li>\n</ul>\n</li>\n</ul>\n<p>Pour obtenir des renseignements d\u00e9taill\u00e9s sur l\u2019admissibilit\u00e9, les crit\u00e8res de s\u00e9lection, les exigences de mise en candidature et les formulaires, veuillez consulter les Lignes directrices sur les Prix Moisson d\u2019or \u00e0 la <a href=\"http://intranet.agr.gc.ca/agrisource/fra/ressources-humaines/prix-et-reconnaissance?id=1287491853497\">page Prix et reconnaissance</a> sur AgriSource. Si vous avez des questions concernant le processus de mise en candidature, veuillez envoyer un courriel \u00e0 <a href=\"mailto:aafc.goldharvest-prixmoissondor.aac@canada.ca\">aafc.goldharvest-prixmoissondor.aac@canada.ca</a>.</p>\n<p>Soumettez la candidature de vos coll\u00e8gues m\u00e9ritants d\u00e8s aujourd\u2019hui!</p>\n<p>\n<img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/GoldHarvestAwards2019-FR.png\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "2020 Gold Harvest Awards - Category Spotlight: Valuing People and Culture",
        "fr": "Prix Moisson d'or de 2020 - En vedette : Valoriser nos personnes et la culture"
    },
    "news": {
        "date_posted": {
            "en": "2019-10-24",
            "fr": "2019-10-24"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Fr\u00e9d\u00e9ric Seppey, Recognition Champion",
            "fr": "Fr\u00e9d\u00e9ric Seppey, champion de la reconnaissance"
        }
    }
}