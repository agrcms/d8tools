{
    "dcr_id": "1534433658317",
    "lang": "en",
    "title": {
        "en": "Starting a Dialogue and Taking Action on Harassment in the Public Service",
        "fr": "D\u00e9buter un dialogue et prendre action pour lutter contre le harc\u00e8lement dans la fonction publique"
    },
    "modified": "2018-08-16 00:00:00.0",
    "issued": "2018-08-16 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2018-08-16",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-08-16",
            "fr": "2018-08-16"
        },
        "modified": {
            "en": "2018-08-16",
            "fr": "2018-08-16"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Starting a Dialogue and Taking Action on Harassment in the Public Service",
            "fr": "D\u00e9buter un dialogue et prendre action pour lutter contre le harc\u00e8lement dans la fonction publique"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Earlier this week, the Deputy Ministers' Task Team on Harassment released a report aimed at addressing harassment in the Public Service.",
            "fr": "L'\u00e9quipe de travail des sous-ministre sur le harc\u00e8lement a publi\u00e9, plus t\u00f4t cette semaine, un rapport sur le harc\u00e8lement dans la fonction publique."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>Earlier this week, the Deputy Ministers\u2019 Task Team on Harassment released a report aimed at addressing harassment in the Public Service.</p>\n<p>The report, <em>Safe Workspaces: Starting a Dialogue and Taking Action on Harassment in the Public Service</em>, identifies clear, immediate and ongoing actions to strengthen the approach of departments and agencies in preventing harassment and enable us to enhance support for employees when harassment occurs.</p>\n<p>This report, coupled with AAFC\u2019s 2017 Public Service Employee Survey (<abbr title=\"Public Service Employee Survey\">PSES</abbr>) results, will help us develop concrete actions as we continue to shape a harassment-free workplace. Consultations with unions, management and employees will be essential in helping advance the recommended actions, as well as specific department initiatives.</p>\n<p>It is our obligation as leaders and colleagues to ensure that harassment in the workplace is never tolerated. I encourage you to <a rel=\"external\" href=\"https://www.canada.ca/en/privy-council/corporate/clerk/publications/safe-workspaces.html\">read the report</a> and discuss the recommendations with your colleagues. In addition, employees can continue the dialogue with me directly by participating in a Conversation with the Deputy session, regularly promoted through news@work. I look forward to reporting back on our progress in the coming months.</p>\n<h2><strong>Resources for employees</strong></h2>\n<p>Information on the Government of Canada\u2019s legislations and policies related to harassment in the workplace can be found in Annex A of the report. The Annex also includes six recourse mechanisms available to employees:</p>\n<ul>\n<li>Harassment complaint under Treasury Board of Canada Secretariat <a rel=\"external\" href=\"http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=26041\">Policy on Harassment Prevention and Resolution</a></li>\n<li>Harassment/discrimination grievances under the applicable <a rel=\"external\" href=\"http://www.tbs-sct.gc.ca/agreements-conventions/index-eng.aspx\">collective agreement</a> (or terms and conditions of employment for unrepresented and excluded employees who do not form part of a bargaining unit)</li>\n<li>Complaint under the <em><a href=\"http://laws-lois.justice.gc.ca/eng/acts/H-6/\">Canadian Human Rights Act</a></em></li>\n<li>Workplace violence complaint under the <a rel=\"external\" href=\"http://laws-lois.justice.gc.ca/eng/regulations/SOR-86-304/\">Canada Occupational Health and Safety Regulations</a> (Part II of the Canada Labour Code)</li>\n<li>Work refusal (regarding danger) under the <a rel=\"external\" href=\"http://laws-lois.justice.gc.ca/eng/acts/L-2/page-53.html#h-46\">Canada Labour Code</a></li>\n<li><a href=\"http://intranet.agr.gc.ca/agrisource/eng/branches-and-offices/corporate-management-branch/services/departmental-security-services/office-for-internal-disclosure?id=1321351700255\">Disclosure of wrongdoing</a></li>\n</ul>\n<p>We all deserve to work in an environment where we are treated with respect, dignity and fairness. If you are experiencing issues in the workplace, speak with your manager and consult AgriSource for <a href=\"http://intranet.agr.gc.ca/agrisource/eng/human-resources/workplace-relations/options-and-solutions-for-issue-resolution?id=1299083757960\">options and solutions for issue resolution</a> or contact the <a href=\"http://intranet.agr.gc.ca/agrisource/eng/human-resources/workplace-relations/options-and-solutions-for-issue-resolution?id=1299083757960#deptharassment\">Departmental Harassment Coordinator</a>.</p>\n<p>\u00a0</p>\n<p><strong>Chris Forbes</strong><br>\nDeputy Minister</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>L\u2019\u00e9quipe de travail des sous-ministre sur le harc\u00e8lement a publi\u00e9, plus t\u00f4t cette semaine, un rapport sur le harc\u00e8lement dans la fonction publique.</p>\n<p>Le rapport, intitul\u00e9 <em>Milieux de travail sains : D\u00e9buter un dialogue et prendre action pour lutter contre le harc\u00e8lement dans la fonction publique</em>, propose des mesures claires, imm\u00e9diates et permanentes pour renforcer l\u2019approche adopt\u00e9e par les minist\u00e8res et organismes pour lutter contre le harc\u00e8lement. Il nous aide aussi \u00e0 am\u00e9liorer le soutien offert aux employ\u00e9s victimes de harc\u00e8lement.</p>\n<p>Ce rapport, combin\u00e9 aux r\u00e9sultats obtenus par AAC dans le Sondage aupr\u00e8s des fonctionnaires f\u00e9d\u00e9raux (<abbr title=\"Sondage aupr\u00e8s des fonctionnaires f\u00e9d\u00e9raux\">SAFF</abbr>) de 2017, nous aidera \u00e0 d\u00e9finir des mesures concr\u00e8tes qui contribueront \u00e0 cr\u00e9er un milieu de travail exempt de harc\u00e8lement. Il sera essentiel de consulter les syndicats, la direction et les employ\u00e9s pour promouvoir les mesures recommand\u00e9es et certaines initiatives lanc\u00e9es par le Minist\u00e8re.</p>\n<p>Nous devons, en tant que dirigeants et coll\u00e8gues, faire en sorte que le harc\u00e8lement en milieu de travail ne soit jamais tol\u00e9r\u00e9. Je vous invite donc \u00e0 <a rel=\"external\" href=\"https://www.canada.ca/fr/conseil-prive/organisation/greffier/publications/milieux-travail-sains.html\">lire le rapport</a> et \u00e0 discuter des recommandations avec vos coll\u00e8gues. Les employ\u00e9s peuvent aussi poursuivre la discussion avec moi en participant \u00e0 une s\u00e9ance \u00ab Conversation avec le sous-ministre \u00bb. Ces s\u00e9ances font r\u00e9guli\u00e8rement l\u2019objet d\u2019un article dans nouvelles@l\u2019ouvrage. J\u2019ai bien h\u00e2te de vous faire part de notre progr\u00e8s au cours des prochains mois.</p>\n<h2><strong>Ressources pour les employ\u00e9s</strong></h2>\n<p>Vous trouverez de plus amples renseignements sur les lois et politiques f\u00e9d\u00e9rales sur le harc\u00e8lement en milieu de travail \u00e0 l\u2019annexe A du rapport. Vous y trouverez \u00e9galement six m\u00e9canismes de recours pour les employ\u00e9s :</p>\n<ul>\n<li>Plainte de harc\u00e8lement d\u2019apr\u00e8s la <a rel=\"external\" href=\"http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=26041\">Politique sur la pr\u00e9vention et la r\u00e9solution du harc\u00e8lement</a> du Secr\u00e9tariat du Conseil du Tr\u00e9sor du Canada</li>\n<li>Griefs pour harc\u00e8lement ou discrimination selon <a rel=\"external\" href=\"http://www.tbs-sct.gc.ca/agreements-conventions/index-fra.aspx\">la convention collective</a> qui s\u2019applique (ou des conditions d\u2019emploi des employ\u00e9s qui ne sont pas repr\u00e9sent\u00e9s ou qui ont \u00e9t\u00e9 exclus parce qu\u2019ils ne font pas partie d\u2019une unit\u00e9 de n\u00e9gociation)</li>\n<li>Plainte en vertu de la <em><a rel=\"external\" href=\"http://laws-lois.justice.gc.ca/fra/lois/H-6/\">Loi canadienne sur les droits de la personne</a></em></li>\n<li>Plainte pour violence en milieu de travail conform\u00e9ment au <a rel=\"external\" href=\"http://laws-lois.justice.gc.ca/fra/reglements/DORS-86-304/\">R\u00e8glement canadien sur la sant\u00e9 et la s\u00e9curit\u00e9 au travail</a> (partie II du Code canadien du travail)</li>\n<li>Refus de travailler (en cas de danger) en vertu du <a rel=\"external\" href=\"http://laws-lois.justice.gc.ca/fra/lois/L-2/page-53.html\">Code canadien du travail</a></li>\n<li><a rel=\"external\" href=\"http://intranet.agr.gc.ca/agrisource/fra/directions-generales-et-bureaux/direction-generale-de-la-gestion-integree/services/services-de-securite-ministeriels/bureau-de-divulgation-interne?id=1321351700255\">Divulgation d\u2019un acte r\u00e9pr\u00e9hensible</a></li>\n</ul>\n<p>Nous avons tous le droit de travailler dans un milieu o\u00f9 nous sommes trait\u00e9s avec respect, dignit\u00e9 et \u00e9quit\u00e9. Si vous \u00e9prouvez des probl\u00e8mes au travail, n\u2019h\u00e9sitez pas \u00e0 en parler \u00e0 votre gestionnaire et \u00e0 consulter AgriSource pour d\u00e9couvrir vos <a href=\"http://intranet.agr.gc.ca/agrisource/fra/ressources-humaines/relations-en-milieu-de-travail/options-et-solutions-pour-la-resolution-des-problemes?id=1299083757960\">options et solutions pour la r\u00e9solution des probl\u00e8mes</a>. Vous pouvez aussi communiquer avec le <a href=\"http://intranet.agr.gc.ca/agrisource/fra/ressources-humaines/relations-en-milieu-de-travail/options-et-solutions-pour-la-resolution-des-problemes?id=1299083757960#coordministeriel\">coordinateur minist\u00e9riel en mati\u00e8re de harc\u00e8lement</a>.</p>\n<p>\u00a0</p>\n<p><strong>Chris Forbes</strong><br>\nSous-ministre</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Starting a Dialogue and Taking Action on Harassment in the Public Service",
        "fr": "D\u00e9buter un dialogue et prendre action pour lutter contre le harc\u00e8lement dans la fonction publique"
    },
    "news": {
        "date_posted": {
            "en": "2018-08-16",
            "fr": "2018-08-16"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Chris Forbes, Deputy Minister",
            "fr": "Chris Forbes, sous-ministre"
        }
    }
}