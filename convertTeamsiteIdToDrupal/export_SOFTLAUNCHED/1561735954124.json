{
    "dcr_id": "1561735954124",
    "lang": "en",
    "title": {
        "en": "Reminder - Review Your Tech List Before Going Abroad",
        "fr": "Rappel - Passez en revue votre liste de v\u00e9rification des technologies avant de partir \u00e0 l\u2019\u00e9tranger "
    },
    "modified": "2019-07-17 00:00:00.0",
    "issued": "2019-07-04 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-07-02",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-07-04",
            "fr": "2019-07-04"
        },
        "modified": {
            "en": "2019-07-17",
            "fr": "2019-07-17"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Reminder - Review Your Tech List Before Going Abroad",
            "fr": "Rappel - Passez en revue votre liste de v\u00e9rification des technologies avant de partir \u00e0 l\u2019\u00e9tranger "
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "All international travellers should be aware that carrying a portable device (laptop, mobile phone, tablet) while travelling brings with it a potential for unauthorized access to AAFC's network, placing our information at risk. Imagine this ...",
            "fr": "Tous les voyageurs internationaux doivent savoir que le fait de voyager avec un appareil portatif (ordinateur portatif, t\u00e9l\u00e9phone mobile, tablette) comporte un risque d\u2019acc\u00e8s non autoris\u00e9 au r\u00e9seau d'AAC, ce qui menace la s\u00e9curit\u00e9 des renseignements du Minist\u00e8re. Imaginez ..."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>In helping AAFC do its work, some employees travel outside of Canada. All international travellers should be aware that carrying a portable device (laptop, mobile phone, tablet) while travelling brings with it a potential for unauthorized access to AAFC\u2019s network, placing our information at risk. So does bringing a personal device to conduct AAFC business.</p>\n<h2><strong>Imagine this\u2026</strong></h2>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/InternationalTravel1.png\" class=\"pull-right mrgn-lft-md\" alt=\"\"></p>\n<p>You\u2019re getting ready for a business trip and realize at the last minute that you need to bring your work mobile device and laptop. You check with your colleague and they say that they have taken their work devices on business trips outside of Canada before. You pack up your portable devices and head to the airport. When you\u2019re on the plane, you realize you forgot the power cord for your computer \u2026 but wait, you have your personal tablet with you!</p>\n<p>On the first day of the trip, you use your laptop all morning and you know the battery won\u2019t last much longer. You quickly send some of the essential documents to your personal tablet, so you can still access them. Your laptop battery runs out, but your tablet is there to save the day. However, you realize you\u2019re missing one document, so you connect to the AAFC network. You find the document you need and now you\u2019re set for the remainder of your business trip.</p>\n<p>What\u2019s wrong with this scenario? <strong>Everything.</strong> Let us explain:</p>\n<ul>\n<li>If you\u2019re travelling outside of Canada for business, you cannot bring AAFC mobile phones or laptops abroad without departmental approval (via your Director General). You need to <a href=\"http://assystweb.agr.gc.ca:8080/assystnet/application.jsp#serviceOfferings/1319\">submit a request to IT Centre TI</a> at least 10 business days prior to travel and include all of your destinations, including layovers. They will determine your eligibility for a travel device.</li>\n<li>Only AAFC authorized IT devices may be used to access, process or store AAFC information. This means you shouldn\u2019t be sending and receiving work information on your personal portable devices.</li>\n<li>Only AAFC authorized devices may be connected to the Government of Canada network via remote access.</li>\n</ul>\n<p>Plan ahead and ensure you follow the steps to <a href=\"http://intranet.agr.gc.ca/agrisource/eng/branches-and-offices/corporate-management-branch/services/prepare-for-your-travel?id=1415297726311\">prepare for your travel</a> and you can help to keep AAFC\u2019s information safe. Bon voyage!</p>\n<h2><strong>Learn more</strong></h2>\n<ul>\n<li><a href=\"http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101055452&amp;lang=eng\">Information Technology Security Policy (Word)</a></li>\n<li><a href=\"https://www.cse-cst.gc.ca/en/cyberhygiene-pratiques-cybersecurite\">Practice good digital hygiene</a></li>\n<li><a href=\"https://cyber.gc.ca/en/guidance/mobile-devices-and-business-travellers-itsap00087\">Mobile Devices and Business Travellers</a></li>\n</ul>\n<p>\u00a0</p>\n<ul>\n</ul>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Pour aider AAC \u00e0 effectuer son travail, certains employ\u00e9s voyagent \u00e0 l\u2019ext\u00e9rieur du Canada. Tous les voyageurs internationaux doivent savoir que le fait de voyager avec un appareil portatif (ordinateur portatif, t\u00e9l\u00e9phone mobile, tablette) comporte un risque d\u2019acc\u00e8s non autoris\u00e9 au r\u00e9seau d\u2019AAC, ce qui menace la s\u00e9curit\u00e9 des renseignements du Minist\u00e8re. Il en est de m\u00eame de l\u2019utilisation d\u2019un appareil personnel pour effectuer des t\u00e2ches d\u2019AAC.</p>\n<h2><strong>Imaginez \u2026</strong></h2>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/InternationalTravel1.png\" class=\"pull-right mrgn-lft-md\">Vous vous pr\u00e9parez en vue d\u2019un voyage d\u2019affaires et vous r\u00e9alisez \u00e0 la derni\u00e8re minute que vous devez apporter votre appareil mobile de travail et votre ordinateur portable. Vous v\u00e9rifiez aupr\u00e8s de votre coll\u00e8gue qui vous dit qu\u2019il a d\u00e9j\u00e0 apport\u00e9 ses appareils de travail lors de voyages d\u2019affaires \u00e0 l\u2019ext\u00e9rieur du Canada. Vous mettez vos appareils portables dans vos bagages et vous vous rendez \u00e0 l\u2019a\u00e9roport. Dans l\u2019avion, vous r\u00e9alisez que vous avez oubli\u00e9 le c\u00e2ble d\u2019alimentation de votre ordinateur ... mais ce n\u2019est pas grave, vous avez votre tablette personnelle!</p>\n<p>Le premier jour du voyage, vous utilisez votre ordinateur portable toute la matin\u00e9e, m\u00eame si vous savez que la batterie ne durera pas longtemps. Vous transf\u00e9rez rapidement une partie des documents essentiels vers votre tablette personnelle afin de pouvoir y acc\u00e9der en tout temps. La batterie de votre ordinateur portable s\u2019\u00e9puise, mais votre tablette personnelle vous permet de sauver la mise. Toutefois, vous vous rendez compte qu\u2019il vous manque un document. Alors, vous vous connectez au r\u00e9seau d\u2019AAC. Vous trouvez le document dont vous avez besoin et vous \u00eates maintenant pr\u00eat pour le reste de votre voyage d\u2019affaires.</p>\n<p>Quel est le probl\u00e8me dans ce sc\u00e9nario? <strong>Tout.</strong> Voici pourquoi :</p>\n<ul>\n<li>Si vous voyagez \u00e0 l\u2019ext\u00e9rieur du Canada pour affaires, vous ne pouvez pas apporter les t\u00e9l\u00e9phones mobiles ni les ordinateurs portatifs d\u2019AAC \u00e0 l\u2019\u00e9tranger sans l\u2019approbation du Minist\u00e8re (par l\u2019entremise de votre directeur g\u00e9n\u00e9ral). Vous devez <a href=\"http://assystweb.agr.gc.ca:8080/assystnet/application.jsp#serviceOfferings/1319\">soumettre une demande au Centre de TI</a> au moins 10 jours ouvrables avant de partir en voyage et y indiquer toutes les destinations, y compris les escales. Le Centre d\u00e9terminera si vous \u00eates admissible \u00e0 recevoir un dispositif de voyage.</li>\n<li>Seuls les appareils de TI autoris\u00e9s d\u2019AAC peuvent \u00eatre utilis\u00e9s pour acc\u00e9der aux renseignements du Minist\u00e8re, les traiter ou les stocker. Cela signifie que vous ne devez ni envoyer ni recevoir de renseignements professionnels sur vos appareils portatifs personnels.</li>\n<li>Seuls les appareils autoris\u00e9s d\u2019AAC peuvent \u00eatre connect\u00e9s au r\u00e9seau du gouvernement du Canada au moyen d\u2019un acc\u00e8s \u00e0 distance.</li>\n</ul>\n<p>Soyez pr\u00e9voyant et suivez les \u00e9tapes suivantes pour <a href=\"http://intranet.agr.gc.ca/agrisource/fra/directions-generales-et-bureaux/direction-generale-de-la-gestion-integree/services/preparez-votre-voyage?id=1415297726311\">pr\u00e9parer votre voyage</a> afin d\u2019assurer la s\u00e9curit\u00e9 des renseignements d\u2019AAC. Bon voyage!</p>\n<h2><strong>Pour en savoir plus</strong></h2>\n<ul>\n<li><a href=\"http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101882750&amp;lang=fra\">Politique de s\u00e9curit\u00e9 des technologies de l\u2019information (document Word)</a></li>\n<li><a href=\"https://www.cse-cst.gc.ca/fr/cyberhygiene-pratiques-cybersecurite\">Pratiques exemplaires en cybers\u00e9curit\u00e9</a></li>\n<li><a href=\"https://cyber.gc.ca/fr/orientation/dispositifs-mobiles-et-voyages-daffaires-itsap00087\">Dispositifs mobiles et voyages d\u2019affaires</a></li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Reminder - Review Your Tech List Before Going Abroad",
        "fr": "Rappel - Passez en revue votre liste de v\u00e9rification des technologies avant de partir \u00e0 l\u2019\u00e9tranger"
    },
    "news": {
        "date_posted": {
            "en": "2019-07-04",
            "fr": "2019-07-02"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Information Systems Branch and Departmental Security Services",
            "fr": "Direction g\u00e9n\u00e9rale des syst\u00e8mes d\u2019information et Services de s\u00e9curit\u00e9 minist\u00e9riels"
        }
    }
}