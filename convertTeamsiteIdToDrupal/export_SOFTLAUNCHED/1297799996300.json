{
    "dcr_id": "1297799996300",
    "lang": "en",
    "title": {
        "en": "Exhibit Services",
        "fr": "Services d'exposition"
    },
    "modified": "2020-08-18 00:00:00.0",
    "issued": "2011-02-18 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1313612184140",
    "layout_name": "1 column",
    "dc_date_created": "2011-02-15",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2011-02-18",
            "fr": "2011-02-18"
        },
        "modified": {
            "en": "2020-08-18",
            "fr": "2020-08-18"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Exhibit Services",
            "fr": "Services d'exposition"
        },
        "subject": {
            "en": "communications;events management;marketing;media",
            "fr": "communications;gestion des activit\u00e9s;marketing;m\u00e9dias"
        },
        "description": {
            "en": "The Exhibit Services Unit manages the Agriculture and Agri-Food Canada (AAFC) corporate exhibits function. It also supports AAFC Program teams in their efforts to communicate directly and proactively with AAFC stakeholders and clients in their communities by providing them with communications advice and coordination services.",
            "fr": "Les Services d'exposition g\u00e8rent la fonction des expositions minist\u00e9rielles d'Agriculture et Agroalimentaire Canada (AAC). Ils aident \u00e9galement les \u00e9quipes des programmes d'AAC dans leurs efforts de communication directe et proactive avec les intervenants d'AAC et les clients dans leurs collectivit\u00e9s en leur assurant des conseils et des services de coordination en mati\u00e8re de communication."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Exhibit Services, Corporate Exhibits Function, Program-Led Exhibits Function, Exhibit Toolbox, Exhibit Best Practices Videos",
            "fr": "Services d'exposition, Fonction des expositions minist\u00e9rielles, Fonction des expositions dirig\u00e9es par les programmes, La bo\u00eete \u00e0 outils des Services d'expositions, Vid\u00e9os - Pratiques exemplaires en mati\u00e8re d'exposition, Pour nous joindre"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "organizational description",
            "fr": "description du programme ou du service"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>The Exhibit Services Unit manages the Agriculture and Agri-Food Canada (AAFC) corporate exhibits function. It also supports AAFC Program teams in their efforts to communicate directly and proactively with AAFC stakeholders and clients in their communities by providing them with communications advice and coordination services.</p>\n\n<p>To maximize an effective AAFC presence at exhibits across Canada, we are working in consultation with the Strategic Communications Advisors, Public Affairs Branch. Therefore, should you plan any exhibits, please contact your respective Strategic Communications Advisors to ensure your communication approach is coordinated.</p>\n\n<h2>Corporate exhibits function</h2>\n\n<p>The purpose of the corporate exhibits function is to promote AAFC's presence and visibility. Through face-to-face interactions, we provide the general public and stakeholders in Canadian agriculture with broad information about our programs, services and strategic priorities.</p>\n\n<p>We coordinate the planning and logistics for the AAFC corporate exhibits at large-scale events across Canada. This involves the following:</p>\n\n<ul>\n  <li>maintenance, storage and transport of display materials;</li>\n  <li>training for booth participation in exhibits; and</li>\n  <li>measurement of exhibit impact through standard evaluation methodologies and tools, such as the number of visitors to the booth and on-line visitor surveys.</li>\n</ul>\n\n<p>Typically, there are approximately 10 to 18\u00a0corporate events per year  as shown in our <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/calendar_2018-2019-eng.docx\">exhibits calendar (Word)</a>.</p>\n\n<h2>Program-led exhibits function</h2>\n\n<p>This function brings together individual AAFC programs participating in fairs and trade shows (decided upon by each program area) in which the Exhibits Services Unit, while not performing the corporate exhibits function, will provide coordination to ensure greater impact.</p>\n\n<p>Most program led exhibits are specific to the agricultural stakeholders, and many are located in smaller cities. Unlike corporate exhibits, most of the costs of program-led events are covered by participating programs.</p>\n\n<p>Typically, there are approximately 100\u00a0program-led events per year.</p>\n\n<h2>Exhibit toolbox</h2>\n\n<p>Does your program participate in trade shows, conferences or open houses? If yes, there is a toolbox to help you plan and deliver a solid presence at those events.</p>\n\n<p>The Exhibits Services Toolbox consists of</p>\n\n<ul>\n  <li>an <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/exhibitor_reference_guide-eng.doc\">Exhibitor reference guide (Word)</a></li>\n  <li>a <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/exhibit_planning_timeline-eng.doc\">Planning timeline (Word)</a></li>\n  <li>an <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/event_checklist-eng.doc\">Events checklist (Word)</a></li>\n  <li>an <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/event_post_mortem-eng.doc\">Event post mortem (Word)</a></li>\n  <li>and a <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/exhibit_survey-eng.doc\">Booth visitor survey (Word)</a></li>\n</ul>\n\n<p>These tools will assist you in maximizing your efforts to enhance the AAFC exhibit experience for visitors.</p>\n\n<h2>Contact us</h2>\n\n<ul>\n  <li>Employee Directory: <a href=\"http://directinfo.agr.gc.ca/directInfo/eng/index.php?fuseaction=agriInfo.orgUnit&amp;dn=OU=PCS-SPC,OU=O,OU=CS-SC,OU=CCB-DGCC,OU=AAFC-AAC,o=gc,c=ca\" title=\"DirectInfo\">Exhibit Services</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Les Services d'exposition g\u00e8rent la fonction des expositions minist\u00e9rielles d'Agriculture et Agroalimentaire Canada (AAC). Ils aident \u00e9galement les \u00e9quipes des programmes d'AAC dans leurs efforts de communication directe et proactive avec les intervenants d'AAC et les clients dans leurs collectivit\u00e9s en leur assurant des conseils et des services de coordination en mati\u00e8re de communication.</p>\n\n<p>Pour optimiser une pr\u00e9sence efficace d'AAC aux expositions ayant lieu dans l'ensemble du Canada, nous collaborons avec les conseillers en communications strat\u00e9giques de la Direction g\u00e9n\u00e9rale des affaires publiques . En cons\u00e9quence, si vous planifiez des expositions, veuillez communiquer avec vos conseillers respectifs en communications strat\u00e9giques pour vous assurer de la coordination de votre approche des communications.</p>\n\n<h2>Fonction des expositions minist\u00e9rielles</h2>\n\n<p>L'objet de la fonction des expositions minist\u00e9rielles est de promouvoir la pr\u00e9sence et la visibilit\u00e9 d'AAC. Par des interactions face \u00e0 face, nous offrons au grand public et aux intervenants de l'agriculture canadienne des informations sur nos programmes, services et priorit\u00e9s strat\u00e9giques.</p>\n\n<p>Nous coordonnons la planification et la logistique des expositions minist\u00e9rielles d'AAC \u00e0 des \u00e9v\u00e9nements \u00e0 grande \u00e9chelle se d\u00e9roulant au Canada. Cela suppose ce qui suit\u00a0:</p>\n\n<ul>\n  <li>l'entretien, l'entreposage et le transport du mat\u00e9riel de pr\u00e9sentation;</li>\n  <li>la formation sur l'installation de kiosques dans les expositions;</li>\n  <li>l'\u00e9valuation de l'effet de l'exposition par l'application de m\u00e9thodes et d'outils d'\u00e9valuation normalis\u00e9s comme le compte des visiteurs du kiosque et les sondages en direct aupr\u00e8s des visiteurs.</li>\n</ul>\n\n<p>Comme vous pourrez le constater en consultant <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/calendrier_2018-2019-fra.docx\">le calendrier des \u00e9v\u00e9nements (Word)</a>, il y a de 10 \u00e0 18\u00a0activit\u00e9s minist\u00e9rielles pr\u00e9vues chaque ann\u00e9e.</p>\n\n<h2>Fonction des expositions dirig\u00e9es par les programmes</h2>\n\n<p>Cette fonction r\u00e9unit les programmes particuliers d'AAC qui participent \u00e0 des foires et des salons professionnels (d\u00e9cid\u00e9s par chacun des secteurs de programme) o\u00f9 les Services d'exposition, bien qu'ils n'assument pas la fonction des expositions minist\u00e9rielles, assureront la coordination pour garantir des r\u00e9percussions maximales.</p>\n\n<p>La plupart des expositions dirig\u00e9es par les programmes s'adressent \u00e0 des intervenants agricoles pr\u00e9cis, et bon nombre ont lieu dans de plus petites villes. Contrairement aux expositions minist\u00e9rielles, la plupart des co\u00fbts des \u00e9v\u00e9nements dirig\u00e9s par les programmes sont assum\u00e9s par les programmes participants.</p>\n\n<p>En g\u00e9n\u00e9ral, environ 100\u00a0\u00e9v\u00e9nements dirig\u00e9s par les programmes ont lieu chaque ann\u00e9e.</p>\n\n<h2>La bo\u00eete \u00e0 outils des Services d'expositions</h2>\n\n<p>Votre programme participe-t-il \u00e0 des foires commerciales, des conf\u00e9rences ou des portes ouvertes? Dans l'affirmative, il existe une bo\u00eete \u00e0 outils pour vous aider \u00e0 planifier et \u00e0 assurer une solide pr\u00e9sence \u00e0 ces \u00e9v\u00e9nements.</p>\n\n<p>La bo\u00eete \u00e0 outils des Services d'exposition comprend\u00a0:</p>\n\n<ul>\n  <li>un <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/exhibitor_reference_guide-fra.doc\">guide de r\u00e9f\u00e9rence des exposants (Word)</a></li>\n  <li>un <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/exhibit_planning_timeline-fra.doc\">calendrier de planification des expositions (Word)</a></li>\n  <li>une <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/event_checklist-fra.doc\">liste de v\u00e9rification de l'\u00e9v\u00e8nement (Word)</a></li>\n  <li>un <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/event_post_mortem-fra.doc\">rapport r\u00e9trospectif de l'\u00e9v\u00e8nement (Word)</a></li>\n  <li>un <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/pab_dgap/exhibit_survey-fra.doc\">sondage aupr\u00e8s des visiteurs du kiosque (Word)</a></li>\n</ul>\n\n<p>Ces outils vous aideront \u00e0 maximiser vos efforts visant \u00e0 am\u00e9liorer les expositions \u00e0 AAC dans l'int\u00e9r\u00eat des visiteurs.</p>\n\n<h2>Pour nous joindre</h2>\n\n<ul>\n  <li>R\u00e9pertoire des employ\u00e9s\u00a0: <a href=\"http://directinfo.agr.gc.ca/directInfo/fra/index.php?fuseaction=agriInfo.orgUnit&amp;dn=OU=PCS-SPC,OU=O,OU=CS-SC,OU=CCB-DGCC,OU=AAFC-AAC,o=gc,c=ca\" title=\"DirectInfo\">Services d'exposition</a></li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Exhibit Services",
        "fr": "Services d'exposition"
    }
}