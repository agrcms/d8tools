{
    "dcr_id": "1565713704188",
    "lang": "en",
    "title": {
        "en": "Beefing Up Security: Suspicious Links, Zip Files and Email Quarantine",
        "fr": "Renforcement de la s\u00e9curit\u00e9 : liens suspects, fichiers compress\u00e9s et mise en quarantaine de courriels"
    },
    "modified": "2019-08-15 00:00:00.0",
    "issued": "2019-08-15 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-08-13",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-08-15",
            "fr": "2019-08-15"
        },
        "modified": {
            "en": "2019-08-15",
            "fr": "2019-08-15"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Beefing Up Security: Suspicious Links, Zip Files and Email Quarantine",
            "fr": "Renforcement de la s\u00e9curit\u00e9 : liens suspects, fichiers compress\u00e9s et mise en quarantaine de courriels"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Effective immediately, Shared Services Canada (SSC) is boosting security measures that may affect incoming emails you receive.",
            "fr": "\u00c0 compter de maintenant, Services partag\u00e9s Canada (SPC) accro\u00eet les mesures de s\u00e9curit\u00e9 qui peuvent toucher les courriels entrants que vous recevez."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>It seems we can\u2019t go a day without hearing about data breaches. Many of these start with phishing, a targeted attack in which cyber criminals send emails with attachments or hyperlinks containing malware. As the phishing tactics evolve, so do our methods to contain them.</p>\n<p>That is why, effective immediately, Shared Services Canada (SSC) is boosting security measures that may affect incoming emails you receive. While SSC believes the impact on departments/agencies government-wide will be minimal, here\u2019s the scoop on the changes and the steps you should take if they affect you.</p>\n<h2><strong>Suspicious links</strong></h2>\n<p>All incoming emails will be automatically scanned for dangerous hyperlinks. If the URL is safe, you will receive the email as it was sent. If the URL is deemed as potentially malicious, the email will be modified based on threat level.</p>\n<p>For example, some suspicious links will be blocked, but you will receive the email with the word \u201cblocked\u201d at the beginning and end of the link in question. If you judge the link to be valid, you can simply copy and paste the URL without the word \u201cblocked\u201d in your browser.</p>\n<p>Other suspicious links will result in the URL being completely removed. In that case, you will receive an email with instructions to follow:</p>\n<p class=\"text-center\">[THIS LINK HAS BEEN REMOVED. If the sender is known and this email is expected, please contact your service desk to request the original copy.]</p>\n<p>To request the original email, you can <a href=\"http://assystweb.agr.gc.ca:8080/assystnet/#services/235\">submit a ticket</a> to IT Centre TI.</p>\n<p>This dual approach based on threat level is being implemented to minimize unnecessary quarantine of emails with legitimate links and help remind you to think about security before you click on a link.</p>\n<h2><strong>Password-protected zip files</strong></h2>\n<p>All incoming emails with password-protected compressed zip files will be automatically quarantined. You will receive an email telling you to\u00a0<a href=\"http://assystweb.agr.gc.ca:8080/assystnet/#services/235\">submit a ticket</a> to IT Centre TI if you want the email released. This will include giving the security team the password so the zip files can be scanned.</p>\n<p>This change does not apply to emails that contain regular password-protected files (e.g., normal Excel, Word or Powerpoint files) or zip files that have no password protection.</p>\n<p>However, keep in mind that emails containing Protected B information should be <a href=\"https://collab.agr.gc.ca/co/smd_dgs/opr/IT%20Security/Encryption%20Instructions%20-%20BB%20%20and%20S8/Logging%20in%20to%20Entrust%20Client%20-%20File%20and%20Email%20Encryption%20Instructions%20-%20Mar%202018.docx\">encrypted (Word)</a> through the use of Entrust (i.e., MyKey). Large files should be shared via drop zones, such as ftp sites.</p>\n<p>These new security measures will help us reduce the risk of cyber attacks on our networks. For more information, contact IT Centre TI at 1-855-545-4411.</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Il semble qu\u2019on ne puisse pas passer une journ\u00e9e sans entendre parler d\u2019atteintes \u00e0 la protection des donn\u00e9es. Plusieurs de ces cas commencent par de l\u2019hame\u00e7onnage, une attaque cibl\u00e9e par laquelle les cybercriminels envoient des courriels avec des pi\u00e8ces jointes ou des hyperliens comportant un maliciel. \u00c0 mesure que les tactiques d\u2019hame\u00e7onnage \u00e9voluent, nos m\u00e9thodes pour les contrer \u00e9voluent \u00e9galement.</p>\n<p>C\u2019est pourquoi, \u00e0 compter de maintenant, Services partag\u00e9s Canada (SPC) accro\u00eet les mesures de s\u00e9curit\u00e9 qui peuvent toucher les courriels entrants que vous recevez. Bien que SPC croit que l\u2019incidence sur les minist\u00e8res et les organismes du gouvernement sera minime, voici des nouvelles concernant les changements et les \u00e9tapes que vous devriez suivre si vous \u00eates concern\u00e9s.</p>\n<h2><strong>Liens suspects</strong></h2>\n<p>Tous les courriels entrants seront automatiquement analys\u00e9s pour v\u00e9rifier s\u2019ils comportent des hyperliens dangereux. Si l\u2019adresse URL est s\u00e9curitaire, vous recevrez le courriel tel qu\u2019il a \u00e9t\u00e9 envoy\u00e9. Si l\u2019adresse URL est jug\u00e9e potentiellement malicieuse, le courriel sera modifi\u00e9 en fonction du niveau de la menace.</p>\n<p>Par exemple, certains liens suspects seront bloqu\u00e9s, mais vous recevrez le courriel avec le mot \u00ab bloqu\u00e9 \u00bb au d\u00e9but et \u00e0 la fin du lien en question. Si vous jugez que le lien est valide, vous pouvez simplement copier et coller l\u2019adresse URL sans le mot \u00ab bloqu\u00e9 \u00bb dans votre navigateur.</p>\n<p>Les autres liens suspects feront en sorte que l\u2019adresse URL sera enti\u00e8rement retir\u00e9e. Dans ce cas, vous recevrez un courriel avec les instructions \u00e0 suivre :</p>\n<p class=\"text-center\">[CE LIEN A \u00c9T\u00c9 ENLEV\u00c9. Si vous connaissez l\u2019exp\u00e9diteur et que vous attendiez ce courriel, <br>veuillez communiquer avec votre bureau de service pour obtenir sa version originale.]</p>\n<p>Pour demander le courriel original, vous pouvez <a href=\"http://assystweb.agr.gc.ca:8080/assystnet/#services/235\">soumettre un billet d\u2019assistance</a> \u00e0 IT Centre TI.</p>\n<p>Cette approche double, fond\u00e9e sur le niveau de menace, est mise en \u0153uvre afin de minimiser les mises en quarantaine inutiles de courriels comportant des liens l\u00e9gitimes et vise \u00e0 vous rappeler de penser \u00e0 la s\u00e9curit\u00e9 avant de cliquer sur un lien.</p>\n<h2><strong>Fichiers compress\u00e9s prot\u00e9g\u00e9s par un mot de passe</strong></h2>\n<p>Tous les courriels entrants comportant un fichier compress\u00e9 prot\u00e9g\u00e9 par un mot de passe seront automatiquement mis en quarantaine. Vous recevrez un courriel vous indiquant de\u00a0<a href=\"http://assystweb.agr.gc.ca:8080/assystnet/#services/235\">soumettre un billet d\u2019assistance </a>\u00e0 IT Centre TI si vous souhaitez recevoir le courriel. Cette \u00e9tape exigera que vous fournissiez \u00e0 l\u2019\u00e9quipe de s\u00e9curit\u00e9 le mot de passe afin que le fichier compress\u00e9 soit analys\u00e9.</p>\n<p>Ce changement ne s\u2019applique pas aux courriels qui comportent des fichiers ordinaires prot\u00e9g\u00e9s par un mot de passe (p. ex., fichiers ordinaires Excel, Word ou PowerPoint) ou des fichiers compress\u00e9s qui ne sont pas prot\u00e9g\u00e9s par un mot de passe.</p>\n<p>Toutefois, n\u2019oubliez pas que les courriels qui contiennent des renseignements d\u00e9sign\u00e9s Prot\u00e9g\u00e9 B doivent \u00eatre <a href=\"https://collab.agr.gc.ca/co/smd_dgs/opr/IT%20Security/Encryption%20Instructions%20-%20BB%20%20and%20S8/Connexion%20au%20client%20Entrust%20-%20File%20and%20Email%20Encryption%20Instructions%20-%20FR%20Mar%202018.DOCX\">chiffr\u00e9s (Word)</a>\u00a0au moyen d\u2019Entrust (p. ex., maCL\u00c9). Les fichiers volumineux doivent \u00eatre envoy\u00e9s au moyen de sites d\u2019envoi, comme les sites FTP.</p>\n<p>Ces nouvelles mesures de s\u00e9curit\u00e9 nous aideront \u00e0 r\u00e9duire le risque des cyberattaques sur nos r\u00e9seaux. Si vous d\u00e9sirez obtenir de plus amples renseignements, communiquez avec IT Centre TI au 1-855-545-4411.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Beefing Up Security: Suspicious Links, Zip Files and Email Quarantine",
        "fr": "Renforcement de la s\u00e9curit\u00e9 : liens suspects, fichiers compress\u00e9s et mise en quarantaine de courriels"
    },
    "news": {
        "date_posted": {
            "en": "2019-08-15",
            "fr": "2019-08-15"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Information Systems Branch",
            "fr": "Direction g\u00e9n\u00e9rale des syst\u00e8mes d\u2019information"
        }
    }
}