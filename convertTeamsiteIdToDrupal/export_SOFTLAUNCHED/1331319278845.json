{
    "dcr_id": "1331319278845",
    "lang": "en",
    "title": {
        "en": "Create a New Position",
        "fr": "Cr\u00e9er un nouveau poste"
    },
    "modified": "2019-12-04 00:00:00.0",
    "issued": "2012-03-20 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1403608870102",
    "layout_name": "1 column",
    "dc_date_created": "2012-03-09",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2012-03-20",
            "fr": "2012-03-20"
        },
        "modified": {
            "en": "2019-12-04",
            "fr": "2019-12-04"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Create a New Position",
            "fr": "Cr\u00e9er un nouveau poste"
        },
        "subject": {
            "en": "staffing",
            "fr": "dotation en personnel"
        },
        "description": {
            "en": "Create a new position when there are new work requirements.",
            "fr": "Cr\u00e9er un nouveau poste lorsqu'il y a de nouvelles exigences de travail."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<!--\n<div class=\"alert alert-warning\">\n   <p>Please note that the HR service request forms will not be available on Wednesday, December&nbsp;4<sup>th</sup> from 6:00&nbsp;a.m. to 9:00&nbsp;a.m&nbsp;EST due to server updates.</p>\n</div>-->\n\n<p>A new position may need to be created when new duties are required or when the volume of work increases sufficiently to warrant another position to perform similar or identical work as an existing position.</p>\n\n<p><b>Special Considerations when Creating a New Position</b></p>\n\n<p>\"Excluded Positions\" are certain positions, typically at the managerial level or within certain job groupings, that are excluded (also known as unrepresented) from some provisions in their respective collective agreements and where the <a href=\"http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?section=text&amp;id=15773\">Directive on Terms and Conditions of Employment for Certain Excluded/Unrepresented Employees</a> applies. For guidance on these types of positions please contact <a href=\"mailto:aafc.labourrelations-relationsdetravail.aac@canada.ca\">aafc.labourrelations-relationsdetravail.aac@canada.ca</a>.</p>\n\n<h2>Process</h2>\n\n<h3>Step 1: Gather the information you need</h3>\n\n<p>Ensure that you have the following information prior to submitting your request:</p>\n\n<ol class=\"lst-lwr-alph\">\n  <li>Location (city / building)</li>\n  <li>Branch</li>\n  <li>Region</li>\n  <li>Organization ID number, if known</li>\n  <li>Division</li>\n  <li>Position title</li>\n  <li>Supervisor's name</li>\n  <li>Supervisor's classification</li>\n  <li>Supervisor's position number</li>\n  <li>Proposed group and level</li>\n  <li>Effective date of position creation</li>\n  <li>Identical position(s), if applicable</li>\n  <li>Reason position is required - if this position is required to reclassify an incumbent in a developmental program (EC, FI), please indicate the position number it will be replacing so it can be abolished once the employee is promoted.</li>\n  <li>Language requirements of the position (see <a href=\"display-afficher.do?id=1403804515233&amp;lang=eng\">Determine the Language Profile for a Position</a>)</li>\n  <li>Communication requirements of the position</li>\n  <li>Security clearance of the position (see <a href=\"display-afficher.do?id=1403790099349&amp;lang=eng\">Determine the Security Level for a Position</a>)</li>\n  <li>Job description <strong>signed and dated by the delegated manager</strong> (see <a href=\"display-afficher.do?id=1288014650959&amp;lang=eng\">Job Descriptions</a>). Please attach the new or revised job description for the position. It is mandatory that the manager signs and dates the job description.</li>\n<li>Organizational Chart \u2013 please provide a signed and dated copy of your current organizational chart.  If you require a copy please contact your <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=eng\" target=\"_blank\">Classification Advisor (Word)</a>.</li>\n</ol>\n\n<p><strong>Note:</strong> by having the employee sign the job description, they are confirming that they have read the job description and have been given the opportunity to comment. Obtaining the employee\u2019s signature is optional.</p>\n\n<h3>Step 2: Submit your request</h3>\n\n<p><a href=\"http://hri-irh.agr.gc.ca:8080/elcf-fcve/F102-1.aspx?lang=eng\">Submit your request using the Create a New Position Online Form</a>.</p>\n\n<p>You will receive an email once your request has been submitted.</p>\n\n<h2>Service Standard</h2>\n\n<p>If you are using a generic, standardized job description or \"identical\" job description: 10 working days with all of the required documentation.</p>\n\n<p>If you are using a unique job description: 50 working days with all of the required documentation.</p>\n\n<p><strong>Note:</strong> Specific timelines can be provided after initial discussions with your <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=eng\" target=\"_blank\">Classification Advisor (Word)</a>.</p>\n\n<h2>Contact Us</h2>\n\n<p>Questions? We can help. Contact any member of the Classification team (see <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=eng\" target=\"_blank\">Classification Advisor (Word)</a>)  or email <a href=\"mailto:aafc.classification.aac@canada.ca\">aafc.classification.aac@canada.ca</a>.</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<!--\n<div class=\"alert alert-warning\">\n   <p>Veuillez noter que les formulaires de service RH ne seront pas disponible entre 6h00 et 9h00&nbsp;HNE le mercredi&nbsp;4&nbsp;d&eacute;cembre&nbsp;2019 afin de proc&eacute;der a une mise-&agrave;-jour du serveur.</p>\n</div>-->\n\n<p>Il est possible qu'un nouveau poste doive \u00eatre cr\u00e9\u00e9 lorsqu'il faut ajouter de nouvelles fonctions ou lorsque le volume de travail augmente suffisamment pour justifier la cr\u00e9ation d'un autre poste pour ex\u00e9cuter un travail similaire ou identique \u00e0 celui d'un poste existant.</p>\n\n<p><b>Consid\u00e9rations particuli\u00e8res au moment de cr\u00e9er un nouveau poste</b></p>\n\n<p>Les \u00ab\u00a0postes exclus\u00a0\u00bb consistent en certains postes, en r\u00e8gle g\u00e9n\u00e9rale au niveau de la gestion ou au sein de certains groupes d'emplois, qui sont exclus (aussi appel\u00e9s \u00ab\u00a0postes non repr\u00e9sent\u00e9s\u00a0\u00bb) de certaines dispositions de leurs conventions collectives respectives et lorsque la <a href=\"http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?section=text&amp;id=15773\">Directive sur les conditions d'emploi de certains employ\u00e9s exclus ou non repr\u00e9sent\u00e9s</a> s'applique. Pour de l'orientation sur ces types de postes, envoyez un courriel \u00e0\u00a0: <a href=\"mailto:aafc.labourrelations-relationsdetravail.aac@canada.ca\">aafc.labourrelations-relationsdetravail.aac@canada.ca</a>.</p>\n\n<h2>Processus</h2>\n\n<h3>\u00c9tape\u00a01\u00a0: Recueillir les renseignements dont vous avez besoin</h3>\n\n<p>Avant de soumettre la demande, assurez-vous de disposer des renseignements suivants\u00a0:</p>\n\n<ol class=\"lst-lwr-alph\">\n  <li>Emplacement (ville ou \u00e9difice) </li>\n  <li>Direction g\u00e9n\u00e9rale </li>\n  <li>R\u00e9gion</li>\n  <li>Num\u00e9ro d\u2019identification de l\u2019organisme, si vous le connaissez</li>\n  <li>Division</li>\n  <li>Titre du poste</li>\n  <li>Nom du superviseur</li>\n  <li>Classification du superviseur</li>\n  <li>Num\u00e9ro de poste du superviseur</li>\n  <li>Groupe et niveau pr\u00e9vus</li>\n  <li>Date d'entr\u00e9e en vigueur du poste cr\u00e9\u00e9</li>\n  <li>Poste(s) identique(s), s'il y a lieu</li>\n  <li>Justification de la cr\u00e9ation du poste  - si ce poste est requis afin de reclassifier un titulaire dans un programme de perfectionnement (EC, FI), veuillez indiquer le num\u00e9ro du poste qui sera remplac\u00e9 afin qu'il puisse \u00eatre aboli lorsque l'employ\u00e9 sera promu.</li>\n  <li>Exigences linguistiques du poste (voir <a href=\"display-afficher.do?id=1403804515233&amp;lang=fra\">D\u00e9terminer le profil linguistique d'un poste</a>)</li>\n  <li>Exigences du poste en mati\u00e8re de communication</li>\n  <li>Cote de s\u00e9curit\u00e9 du poste (voir <a href=\"display-afficher.do?id=1403790099349&amp;lang=fra\">D\u00e9terminer la cote de s\u00e9curit\u00e9 d'un poste</a>)</li>\n  <li>Description d'emploi <strong>sign\u00e9e et dat\u00e9e par le gestionnaire d\u00e9l\u00e9gataire</strong> (voir <a href=\"display-afficher.do?id=1288014650959&amp;lang=fra\">Descriptions d\u2019emploi</a>). Veuillez joindre la nouvelle description de poste du poste ou la description de poste r\u00e9vis\u00e9e du poste. Le gestionnaire doit obligatoirement signer et dater la description de poste.</li>\n<li>Organigramme - veuillez fournir une copie sign\u00e9e et dat\u00e9e de l'organigramme actuel de votre organisation. Si vous avez besoin d'une copie de l'organigramme, veuillez vous adresser \u00e0 votre <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=fra\" target=\"_blank\">conseiller en classification (Word)</a>.</li>\n</ol>\n\n<p><strong>Remarque :</strong> L'employ\u00e9 qui signe la description d\u2019emploi confirme qu'il l'a lue et qu'il a eu l'occasion de formuler des observations. La signature de l'employ\u00e9 est facultative.</p>\n\n<h3>\u00c9tape\u00a02\u00a0: Soumettre la demande</h3>\n\n<p><a href=\"http://hri-irh.agr.gc.ca:8080/elcf-fcve/F102-1.aspx?lang=fra\">Soumettez votre demande \u00e0 l'aide du formulaire en ligne de cr\u00e9ation d'un nouveau poste</a>.</p>\n\n<p>Vous recevrez un courriel des services une fois que votre demande aura \u00e9t\u00e9 soumise.</p>\n\n<h2>Norme de service</h2>\n\n<p>Si vous utilsez une description de emploi g\u00e9n\u00e9rique, normalis\u00e9e ou \u00ab\u00a0identique\u00a0\u00bb\u00a0: dix jours ouvrables avec tous les documents exig\u00e9s.</p>\n<p>Si vous utilisez \u00e0 une description de travail d\u2019emploi\u00a0: 50 jours ouvrables avec tous les documents exig\u00e9s.</p>\n\n<p><strong>Remarque\u00a0:</strong> Des calendriers pr\u00e9cis peuvent \u00eatre \u00e9tablis apr\u00e8s les discussions initiales avec votre <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=fra\" target=\"_blank\">conseiller en classification</a>.</p>\n\n<h2>Contactez-nous</h2>\n\n<p>Des questions? Nous pouvons vous aider. Contactez un membre de l'\u00e9quipe de classification  (voir la liste <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=fra\" target=\"_blank\">conseiller en classification (Word)</a>) ou envoyez un courriel \u00e0 <a href=\"mailto:aafc.classification.aac@canada.ca\">aafc.classification.aac@canada.ca</a>.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Create a New Position",
        "fr": "Cr\u00e9er un nouveau poste"
    }
}