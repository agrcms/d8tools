{
    "dcr_id": "1527019378435",
    "lang": "en",
    "title": {
        "en": "Working Remotely: Best Ways to Connect",
        "fr": "Travailler \u00e0 distance : Les meilleurs moyens de se brancher"
    },
    "modified": "2018-05-24 00:00:00.0",
    "issued": "2018-05-24 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2018-05-22",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-05-24",
            "fr": "2018-05-24"
        },
        "modified": {
            "en": "2018-05-24",
            "fr": "2018-05-24"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Working Remotely: Best Ways to Connect",
            "fr": "Travailler \u00e0 distance : Les meilleurs moyens de se brancher"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Do you travel for business within Canada? Conduct field work? Telework? Then you know all about AAFC's Secure Remote Access Service, which allows you to work away from the office while keeping you securely connected to your colleagues, applications and files/information housed on the departmental network.",
            "fr": "Voyagez-vous par affaires au Canada? Travaillez-vous sur le terrain? Vous faites du t\u00e9l\u00e9travail? Vous connaissez donc tous le Service d'acc\u00e8s \u00e0 distance s\u00e9curis\u00e9 d'AAC qui vous permet de travailler ailleurs qu'au bureau tout en demeurant branch\u00e9 en toute s\u00e9curit\u00e9 et travailler avec vos coll\u00e8gues, les applications, les dossiers ou les renseignements sur le r\u00e9seau du minist\u00e8re."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>Do you travel for business within Canada? Conduct field work? Telework? Then you know all about AAFC\u2019s <a href=\"http://intranet.agr.gc.ca/agrisource/eng/branches-and-offices/information-systems-branch/services/secure-remote-access-service?id=1330966926020\">Secure Remote Access Service</a>, which allows you to work away from the office while keeping you securely connected to your colleagues, applications and files/information housed on the departmental network.</p>\n<p>You already have your Director\u2019s permission and the security token, but now you face a tough decision: how to connect. Let\u2019s take a look at your options, and the pros and cons of each.</p>\n<h2><strong>Direct remote access on a departmental mobile device</strong></h2>\n<p>Use a departmental mobile device (e.g., laptop, tablet) and a wifi connection to sign into <a href=\"https://secure.agr.gc.ca/dana-na/auth/url_default/welcome.cgi\">Secure Remote Access Service</a>. Once signed in, minimize the pop-up AgriApps window, and simply use the device just like you do in the office <span style='font-family: \"Calibri\",\"sans-serif\"; font-size: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: \"Times New Roman\"; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-CA; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;'>\u2013</span> directly selecting desktop and Start menu shortcuts.</p>\n<p><strong>Pros:</strong> Quickest, most direct access to email, software and applications; fewer issues with connectivity and responsiveness.</p>\n<p><strong>Cons:</strong> Carrying the hardware and/or accessories.</p>\n<p><strong>Best for:</strong> anyone who works remotely on a regular basis, uses multiple software/applications, or values minimum-fuss access to the tools they need.</p>\n<h2><strong>Access through AgriApps on personal or travel device</strong></h2>\n<p>Use a personal computer or travel device and a wifi connection to sign into <a href=\"https://secure.agr.gc.ca/\">Secure Remote Access Service</a>. Once signed in, launch AgriApps and the application you want to access.</p>\n<p><strong>Pros:</strong> access to email, software and applications offered in Agriapps; ability to use devices at hand rather than carrying hardware.</p>\n<p><strong>Cons:</strong> Limited application availability (AgriApps applications only) and indirect access to them; higher likelihood of connectivity and responsiveness issues.</p>\n<p><strong>Best for:</strong> short-term (e.g., day) and unplanned work; anyone who wants to check email or update a file/document quickly.</p>\n<p>For more information or to get help, contact <a href=\"http://assystweb.agr.gc.ca:8080/assystnet/application.jsp#services\">My IT Centre TI</a>.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/ISBRemoteAccess.png\" class=\"center-block\"></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Voyagez-vous par affaires au Canada? Travaillez-vous sur le terrain? Vous faites du t\u00e9l\u00e9travail? Vous connaissez donc tous le <a href=\"http://intranet.agr.gc.ca/agrisource/fra/directions-generales-et-bureaux/direction-generale-des-systemes-dinformation/services/service-dacces-a-distance-securise?id=1330966926020\">Service d\u2019acc\u00e8s \u00e0 distance s\u00e9curis\u00e9</a> d\u2019AAC qui vous permet de travailler ailleurs qu\u2019au bureau tout en demeurant branch\u00e9 en toute s\u00e9curit\u00e9 et travailler avec vos coll\u00e8gues, les applications, les dossiers ou les renseignements sur le r\u00e9seau du minist\u00e8re.</p>\n<p>Vous avez d\u00e9j\u00e0 la permission de votre directeur et le jeton de s\u00e9curit\u00e9, mais vous \u00eates aux prises avec une d\u00e9cision difficile : comment se brancher? Explorons les possibilit\u00e9s qui s\u2019offrent \u00e0 vous, ainsi que les avantages et d\u00e9savantages de chacune.</p>\n<h2><strong>Acc\u00e8s \u00e0 distance direct \u00e0 partir d\u2019un appareil mobile minist\u00e9riel</strong></h2>\n<p>Utilisez un appareil mobile minist\u00e9riel (p. ex., ordinateur portable, tablette) et une connexion wifi pour ouvrir une session du <a href=\"https://secure.agr.gc.ca/dana-na/auth/url_default/welcome.cgi\">Service d\u2019acc\u00e8s \u00e0 distance s\u00e9curis\u00e9</a>. Une fois votre session ouverte, minimisez la fen\u00eatre contextuelle AgriApps et utilisez simplement l\u2019appareil comme vous le feriez au bureau <span style='font-family: \"Calibri\",\"sans-serif\"; font-size: 11pt; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: \"Times New Roman\"; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-CA; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;'>\u2013</span> vous pouvez utiliser les raccourcis du Bureau et du Menu D\u00e9marrer.</p>\n<p><strong>Avantages </strong>: Acc\u00e8s le plus rapide et le plus direct aux courriels, aux logiciels et aux applications; moins de probl\u00e8mes de connectivit\u00e9 et de r\u00e9ceptivit\u00e9.</p>\n<p><strong>D\u00e9savantages </strong>: Transporter le mat\u00e9riel et/ou les accessoires.</p>\n<p><strong>Id\u00e9al pour </strong>: Quiconque travaille \u00e0 distance sur une base r\u00e9guli\u00e8re, utilise une multitude de logiciels et d\u2019applications, ou appr\u00e9cie un acc\u00e8s sans complications aux outils dont il a besoin.</p>\n<h2><strong>Acc\u00e8s par l\u2019entremise d\u2019AgriApps sur un appareil personnel ou de voyage</strong></h2>\n<p>Utilisez un ordinateur personnel ou un appareil de voyage et une connexion wifi pour ouvrir une session du <a href=\"https://secure.agr.gc.ca/dana-na/auth/url_default/welcome.cgi\">Service d\u2019acc\u00e8s \u00e0 distance s\u00e9curis\u00e9</a>. Une fois votre session ouverte, lancez AgriApps et l\u2019application \u00e0 laquelle vous voulez avoir acc\u00e8s.</p>\n<p><strong>Avantages </strong>: Acc\u00e8s aux courriels, aux logiciels et aux applications dans Agriapps; capacit\u00e9 d\u2019utiliser les appareils dont vous disposez sans avoir \u00e0 transporter de mat\u00e9riel.</p>\n<p><strong>D\u00e9savantages </strong>: Disponibilit\u00e9 limit\u00e9e des applications (application d\u2019AgriApps seulement) et acc\u00e8s indirect seulement; risque plus \u00e9lev\u00e9 de probl\u00e8mes de connectivit\u00e9 ou de r\u00e9ceptivit\u00e9.</p>\n<p><strong>Id\u00e9al pour </strong>: Le travail \u00e0 distance \u00e0 court terme (p. ex., une journ\u00e9e) et impr\u00e9vu; quiconque veut v\u00e9rifier sa bo\u00eete de courriels ou mettre \u00e0 jour un dossier ou un document rapidement.</p>\n<p>Pour plus de renseignements ou pour obtenir de l\u2019aide, communiquez avec <a href=\"http://assystweb.agr.gc.ca:8080/assystnet/application.jsp#services\">Mon IT Centre TI</a>.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/ISBRemoteAccess.png\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Working Remotely: Best Ways to Connect",
        "fr": "Travailler \u00e0 distance : Les meilleurs moyens de se brancher"
    },
    "news": {
        "date_posted": {
            "en": "2018-05-24",
            "fr": "2018-05-24"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "IT Centre TI",
            "fr": "IT Centre TI"
        }
    }
}