{
    "dcr_id": "1522789054198",
    "lang": "en",
    "title": {
        "en": "Phoenix Flexible Repayment Options",
        "fr": "Ph\u00e9nix - Options flexibles de remboursement"
    },
    "modified": "2018-04-04 00:00:00.0",
    "issued": "2018-04-04 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2018-04-04",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-04-04",
            "fr": "2018-04-04"
        },
        "modified": {
            "en": "2018-04-04",
            "fr": "2018-04-05"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Phoenix Flexible Repayment Options",
            "fr": "Ph\u00e9nix - Options flexibles de remboursement"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "The government is continuing to do everything it can to resolve pay problems and provide as much flexibility as possible for employees experiencing issues with their pay.",
            "fr": "Le gouvernement continue de tout mettre en oeuvre pour r\u00e9gler les probl\u00e8mes de paye et de donner autant de flexibilit\u00e9 que possible aux employ\u00e9s \u00e9prouvant des probl\u00e8mes de paye."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>The government is continuing to do everything it can to resolve pay problems and provide as much flexibility as possible for employees experiencing issues with their pay.</p>\n<p>To help ensure that the recovery of overpayments related to a Phoenix issue minimizes financial hardship for employees, the government is introducing new measures:</p>\n<ul>\n<li>The recovery of overpayments, emergency salary advances and priority payments related to a Phoenix issue will be managed in the same way. Unless an employee chooses an earlier repayment, recoveries will <strong>only</strong> start when:\n\t\n<ul>\n<li>all of an employee\u2019s pay problems are addressed, and</li>\n<li>the employee has received correct pay for three consecutive pay periods.</li>\n</ul>\n</li>\n<li>Public Services and Procurement Canada will be responsible for managing recoveries for Agriculture and Agri-Food Canada.</li>\n<li>For employees on leave without pay, recoveries will not start until they return to work.</li>\n<li>Employees will be provided all reasonable flexibility, to decide how they wish to make repayments. This includes delaying repayments until an employee receives monies owed, such as income tax refunds or the receipt of retroactive payments from the implementation of collective agreements.</li>\n<li>If employees consider themselves to be in financial hardship, and have already agreed to a recovery plan before the current measures were put in place, they could ask for their plan to be modified in order to benefit from these new flexibilities.</li>\n<li>Once all of an employee\u2019s pay problems are fixed, the employee will be able to select from a number of options to repay the government, making changes when needed to better suit their situation.</li>\n</ul>\n<p>It is important to note that the new measures do not apply to recoveries associated with termination of employment or the end of term or casual contracts that are not extended or renewed.</p>\n<p>Details on how these new recovery processes will roll-out across departments and agencies will be finalized in the coming weeks. More information will be shared as it becomes available.</p>\n<p>All employees are reminded that information and resources to assist with the <a href=\"http://intranet.agr.gc.ca/agrisource/eng?id=1511367056518\">Phoenix pay system</a> are available on AgriSource. There are also <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/pay/phoenix.html\">claims processes</a> in place to compensate employees who have incurred expenses or financial losses because of the implementation of the Phoenix pay system.</p>\n<p>The government will continue to take a flexible approach toward addressing the needs of employees, and work in close collaboration with its union partners to minimize the financial impacts on employees due to Phoenix issues.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/pheonix1-eng.png\" class=\"center-block\"></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Le gouvernement continue de tout mettre en \u0153uvre pour r\u00e9gler les probl\u00e8mes de paye et de donner autant de flexibilit\u00e9 que possible aux employ\u00e9s \u00e9prouvant des probl\u00e8mes de paye.</p>\n<p>Pour veiller \u00e0 ce que le recouvrement des trop-pay\u00e9s, li\u00e9s \u00e0 un enjeu d\u00fb \u00e0 Ph\u00e9nix, occasionne le moins de difficult\u00e9s financi\u00e8res possible pour les employ\u00e9s, le gouvernement met en place de nouvelles mesures :</p>\n<ul>\n<li>Le recouvrement des trop-pay\u00e9s, des avances de salaire d\u2019urgence et des paiements prioritaires li\u00e9s \u00e0 un enjeu d\u00fb \u00e0 Ph\u00e9nix sera g\u00e9r\u00e9 de la m\u00eame mani\u00e8re. \u00c0 moins qu\u2019un employ\u00e9 ne choisisse un remboursement pr\u00e9coce, les recouvrements commenceront <strong>seulement</strong> une fois que :\n\t\n<ul>\n<li>les probl\u00e8mes de paye de l\u2019employ\u00e9 sont r\u00e9gl\u00e9s;</li>\n<li>l\u2019employ\u00e9 a \u00e9t\u00e9 pay\u00e9 correctement pour 3 p\u00e9riodes de paye cons\u00e9cutives.</li>\n</ul>\n</li>\n<li>Services publics et Approvisionnement Canada sera responsable de la gestion des recouvrements pour Agriculture et Agroalimentaire Canada.</li>\n<li>Pour un employ\u00e9 en cong\u00e9 non pay\u00e9, les recouvrements commenceront seulement une fois que l\u2019employ\u00e9 est revenu au travail.</li>\n<li>Les employ\u00e9s pourront disposer d\u2019une latitude raisonnable pour d\u00e9terminer la mani\u00e8re du remboursement, y compris la possibilit\u00e9 de reporter celui-ci jusqu\u2019\u00e0 ce que l\u2019employ\u00e9 re\u00e7oive de l\u2019argent qui lui est d\u00fb, par exemple, dans le cas d\u2019un remboursement d\u2019imp\u00f4t ou d\u2019un paiement r\u00e9troactif \u00e0 la suite de la mise en \u0153uvre d\u2019une entente collective.</li>\n<li>Si les employ\u00e9s estiment \u00e9prouver des difficult\u00e9s financi\u00e8res et ont d\u00e9j\u00e0 convenu d\u2019un plan de remboursement avant que les mesures actuelles aient \u00e9t\u00e9 mises en place, ils pourraient demander que leur arrangement actuel soit modifi\u00e9 pour tirer parti de ces souplesses additionnelles.</li>\n<li>Une fois que tous les probl\u00e8mes de paye d\u2019un employ\u00e9 sont r\u00e9gl\u00e9s, l\u2019employ\u00e9 aura le choix d\u2019un certain nombre d\u2019options pour rembourser le gouvernement et pourra effectuer des changements \u00e0 son plan, au besoin, pour que celui-ci convienne mieux \u00e0 sa situation.</li>\n</ul>\n<p>Il est important de noter que les nouvelles proc\u00e9dures ne s\u2019appliquent pas aux recouvrements associ\u00e9s \u00e0 la cessation d\u2019emploi, ni \u00e0 la fin d\u2019un emploi d\u2019une dur\u00e9e d\u00e9termin\u00e9e ou d\u2019un emploi occasionnel qui n\u2019est ni prolong\u00e9, ni renouvel\u00e9.</p>\n<p>Les d\u00e9tails sur la mani\u00e8re dont ces nouveaux processus de recouvrement seront d\u00e9ploy\u00e9s dans l\u2019ensemble des minist\u00e8res seront finalis\u00e9s au cours des prochaines semaines. D\u2019autres renseignements seront diffus\u00e9s \u00e0 mesure qu\u2019ils deviennent disponibles.</p>\n<p>On vous rappelle que vous pouvez acc\u00e9der \u00e0 des renseignements et \u00e0 des ressources pour vous aider avec le <a href=\"http://intranet.agr.gc.ca/agrisource/fra?id=1511367056518\">syst\u00e8me de paye Ph\u00e9nix</a> sur AgriSource. Un <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/remuneration/phenix.html\">processus de r\u00e9clamation</a> est \u00e9galement en place pour indemniser les employ\u00e9s qui ont engag\u00e9 des frais ou subi des pertes financi\u00e8res \u00e0 cause de la mise en \u0153uvre du syst\u00e8me de paye Ph\u00e9nix.</p>\n<p>Le gouvernement continue d\u2019adopter une approche flexible pour r\u00e9pondre aux besoins des employ\u00e9s et de travailler en collaboration avec ses partenaires syndicaux pour att\u00e9nuer les r\u00e9percussions financi\u00e8res sur les employ\u00e9s en raison des probl\u00e8mes de Ph\u00e9nix.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/pheonix1-fra.png\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Phoenix Flexible Repayment Options",
        "fr": "Ph\u00e9nix - Options flexibles de remboursement"
    },
    "news": {
        "date_posted": {
            "en": "2018-04-03",
            "fr": "2018-04-03"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Treasury Board Secretariat",
            "fr": "Secr\u00e9tariat du Conseil du Tr\u00e9sor"
        }
    }
}