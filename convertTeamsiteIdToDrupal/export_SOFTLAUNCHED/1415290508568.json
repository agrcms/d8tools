{
    "dcr_id": "1415290508568",
    "lang": "en",
    "title": {
        "en": "Get Your Travel Approved",
        "fr": "Faites approuver votre voyage"
    },
    "modified": "2018-12-11 00:00:00.0",
    "issued": "2014-12-04 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1484940874128",
    "layout_name": "1 column",
    "dc_date_created": "2014-11-06",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2014-12-04",
            "fr": "2014-12-04"
        },
        "modified": {
            "en": "2018-12-11",
            "fr": "2018-12-11"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Get Your Travel Approved",
            "fr": "Faites approuver votre voyage"
        },
        "subject": {
            "en": "communications;travel",
            "fr": "communications;voyage"
        },
        "description": {
            "en": "Depending on the type and cost of travel and your internal branch processes, your travel will be approved by the Minister, the Deputy Minister, your Assistant Deputy Minister, or another delegated individual within your division. To assist with consistent interpretations, an event approval decision tree has been prepared which outlines the approvals required for core and non-core events. More information can be found on the Delegation of Authority AgriSource page.",
            "fr": "Selon le type et les frais de voyage, ainsi que les processus internes de votre direction g\u00e9n\u00e9rale, votre voyage doit \u00eatre approuv\u00e9 par le ministre, le sous-ministre, votre sous-ministre adjoint ou une autre personne d\u00e9l\u00e9gu\u00e9e de votre secteur. \u00c0 des fins d'uniformit\u00e9, un arbre d\u00e9cisionnel pour l'approbation des \u00e9v\u00e9nements a \u00e9t\u00e9 \u00e9labor\u00e9 pour indiquer le niveau d'approbation requis pour les activit\u00e9s essentielles et non essentielles. Vous trouverez plus d'information \u00e0 ce sujet sur la page AgriSource consacr\u00e9e \u00e0 la d\u00e9l\u00e9gation du pouvoir de signer."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Approve Your Travel, Departmental Event Plan, Off-Cycle Request, Blanket Travel Authority",
            "fr": "approuver votre voyage, plan minist\u00e9riel des \u00e9v\u00e9nements, demande hors cycle (processus exceptionnel), autorisation g\u00e9n\u00e9rale de voyage"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>The following page will help you get your travel approved.</p>\n\n<h2 id=\"wwnayt\">Who Will Need to Approve Your Travel?</h2>\n\n<p>Depending on the type and cost of travel and your internal branch  processes, your travel will be approved by the Minister, the Deputy Minister,  your Assistant Deputy Minister, your Director General or another delegated individual within your  division. To assist with consistent interpretations, an <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=4502752&amp;lang=eng\" target=\"_blank\">approval decision tree - Travel, Hospitality, Conference and Events (Excel)</a> has been prepared which outlines the approvals required for operational activities, training and events. More information can be found on the <a href=\"display-afficher.do?id=1327610986849&amp;lang=eng\">Delegation  of Authority</a> AgriSource page. </p>\n\n<p><b>Note:</b> Your <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=2852232&amp;lang=eng\" target=\"_blank\">Branch Travel Coordinator (Word)</a> can  help you determine who will approve your travel as other travellers may be travelling to the same destination for  the same event, which would increase the total event cost, thus possibly requiring a higher level of approval.</p>\n\n<h2>Process to Get Your Travel Approved</h2>\n\n<h3>Travel Approved Via the Quarterly Travel, Hospitality, Conference and Event (THCE) Plan (Regular Process)</h3>\n\n<p>As travel may be part of an event, travel may be approved via the Quarterly THCE Plan.</p>\n\n<p>Contact your <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=2852232&amp;lang=eng\" target=\"_blank\">Branch Travel Coordinator (Word)</a> to determine what needs to be prepared and when the next call for submissions will take place.</p>\n\n<h3>Travel Approved Via an Off-Cycle Request (Exception Process)</h3>\n\n<p>If you cannot wait for the next call for submissions for the Quarterly THCE Plan, you may be able to request approval for your travel using an off-cycle request.</p>\n\n<p>To submit an off-cycle request, please contact your <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=2852232&amp;lang=eng\" target=\"_blank\">Branch Travel Coordinator (Word)</a> to determine the process to follow and what documents need to  be prepared and submitted for approval.</p>\n\n<h3>Travel Approved Via a Blanket Travel  Authority or Blanket Travel Request</h3>\n\n<p>A <b>Blanket Travel Authority (BTA)</b> is used for <b>continuous</b> or <b>repetitive</b> travel within Canada or the United States, for a specific purpose, in a specific geographical area, for a period of time where booking the trips with Shared Travel Services, such as air or rail, is required. A BTA provides an administratively efficient process to obtain prior approval for  these trips. It cannot be used when there are conference fees, hospitality, training or event costs.</p>\n\n  <p>Once a <a href=\"https://forms-formulaires.agr.gc.ca/pdf/A4340-E.pdf\">Blanket Travel Authority form (PDF)</a> (AAFC/AAC 4340), has been  completed and approved, please submit it to the <a href=\"mailto:aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca\">aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca</a> for processing.</p>\n  \n  <p>A <b>Blanket Travel Request (BTR)</b> can be used to pre-authorize <b>continuous</b> or <b>repetitive</b> travel for a specific purpose, in a specific geographical area, for a period of time, and <b>is restricted to</b>:</p>\n\n<ol>\n  <li>Local travel (no overnight stay) including: car  rental, fleet vehicle, mileage, and/or meals.</li>\n  <li>Travel within Canada with overnight stay but no  air or rail booking.</li>\n  <li>All bookings are to be done outside the Online Booking Tool/Travel Call Centre. Traveller must call and reserve vehicle rentals and/or accommodations directly with the approved supplier. See the <a href=\"http://rehelv-acrd.tpsgc-pwgsc.gc.ca/index-eng.aspx\">Accommodation and Car Rental Directory</a>.</li>\n</ol>\n\n<p>The BTR has to be created in the Shared Travel Service Portal by the traveller or travel arranger and approved by the appropriate delegated manager. A <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101402613&amp;lang=eng\" target=\"_blank\">Guide on the Creation of a Blanket Travel Request (PDF)</a> has been prepared to assist with their creation and processing.</p>\n\n  <p>Refer to the <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100491477&amp;lang=eng\" target=\"_blank\">Guide on Blanket Travel Authority and Blanket Travel Request (Word)</a> for more  information.</p>\n\n<h2>Contact Us</h2>\n\n<p>Have questions? View our <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100477471&amp;lang=eng\" target=\"_blank\">list of key contacts at Agriculture and Agri-Food Canada for travel (Word)</a>.</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Cette page vous aidera \u00e0 faire approuver votre voyage.</p>\n\n<h2 id=\"qdavv\">Qui doit approuver votre  voyage?</h2>\n\n<p>Selon le type et les frais de voyage, ainsi que les processus internes  de votre direction g\u00e9n\u00e9rale, votre voyage doit \u00eatre approuv\u00e9 par le ministre, le sous-ministre, votre sous-ministre adjoint, votre directeur g\u00e9n\u00e9ral ou une autre personne d\u00e9l\u00e9gu\u00e9e de votre secteur. \u00c0 des fins d'uniformit\u00e9, un <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=4502756&amp;lang=fra\" target=\"_blank\">arbre d\u00e9cisionnel pour l'approbation de voyages, d\u2019accueil, de conf\u00e9rences et d\u2019\u00e9v\u00e9nements (Excel)</a> a \u00e9t\u00e9 \u00e9labor\u00e9 pour indiquer le niveau d'approbation requis pour les activit\u00e9s op\u00e9rationnelles, la formation et les \u00e9v\u00e9nements. Vous trouverez plus d'information \u00e0 ce sujet sur la page AgriSource consacr\u00e9e \u00e0 la <a href=\"display-afficher.do?id=1327610986849&amp;lang=fra\">d\u00e9l\u00e9gation du pouvoir de signer</a>.</p>\n<p><b>Remarque\u00a0:</b> Le <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=2852234&amp;lang=fra\" target=\"_blank\">coordonnateur des voyages de votre direction g\u00e9n\u00e9rale (Word)</a> peut vous  aider \u00e0 d\u00e9terminer qui doit approuver votre voyage, car d'autres employ\u00e9s se d\u00e9placeront peut-\u00eatre aussi pour participer au m\u00eame \u00e9v\u00e9nement que vous, ce qui accro\u00eetra le co\u00fbt total de l'\u00e9v\u00e9nement, et requerra donc possiblement un niveau d'approbation plus \u00e9lev\u00e9.</p>\n\n<h2>Proc\u00e9dure \u00e0 suivre pour faire approuver votre voyage</h2>\n\n<h3>Approbation des voyages au moyen du plan trimestriel de voyages, d\u2019accueil, de conf\u00e9rences et d\u2019\u00e9v\u00e9nements (VAC\u00c9) (processus courant)</h3>\n\n<p>Comme les voyages peuvent \u00eatre consid\u00e9r\u00e9s comme des \u00e9v\u00e9nements, ils peuvent \u00eatre approuv\u00e9s par l'interm\u00e9diaire du plan trimestriel de VAC\u00c9.</p>\n\n<p>Communiquez avec le <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=2852234&amp;lang=fra\" target=\"_blank\">coordonnateur des voyages de votre direction g\u00e9n\u00e9rale (Word)</a> pour  d\u00e9terminer ce qu'il faut pr\u00e9parer et \u00e0 quel moment la prochaine invitation \u00e0  faire part des \u00e9v\u00e9nements pr\u00e9vus sera lanc\u00e9e.</p>\n\n<h3>Approbation des voyages au moyen d'une demande hors cycle (processus exceptionnel)</h3>\n\n<p>Si vous ne pouvez pas attendre la prochaine invitation g\u00e9n\u00e9rale pour la mise \u00e0 jour du plan trimestriel de VAC\u00c9, vous pouvez demander  l'approbation de votre voyage en faisant une demande hors cycle.</p>\n\n<p>Si vous souhaitez pr\u00e9senter une demande hors cycle, veuillez communiquer avec le <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=2852234&amp;lang=fra\" target=\"_blank\">coordonnateur des voyages de votre direction g\u00e9n\u00e9rale (Word)</a> pour d\u00e9terminer le processus \u00e0 suivre et quel documents doivent \u00eatre pr\u00e9par\u00e9s et  pr\u00e9sent\u00e9s pour approbation.</p>\n\n<h3>Approbation des voyages au moyen d'une autorisation g\u00e9n\u00e9rale de voyage ou d'une demande de voyage g\u00e9n\u00e9rale</h3>\n\n<p>L'<b>autorisation g\u00e9n\u00e9rale de voyage  (AGV)</b> est utilis\u00e9e pour les voyages de nature <b>continuelle</b> ou <b>r\u00e9p\u00e9titive</b> effectu\u00e9s  au Canada ou aux \u00c9tats-Unis pour une raison particuli\u00e8re, dans une r\u00e9gion g\u00e9ographique pr\u00e9cise et pendant une dur\u00e9e d\u00e9termin\u00e9e o\u00f9 des r\u00e9servations de voyage avec les Services de voyage partag\u00e9s pour billets d'avion ou de train sont n\u00e9cessaires. Une AGV est un processus administratif efficient qui permet d'obtenir une approbation  pr\u00e9alable pour de tels voyages. Elle ne peut pas \u00eatre utilis\u00e9e lorsqu'il y a  des frais de participation \u00e0 une conf\u00e9rence, des frais d'accueil, des frais de formation  ou des co\u00fbts li\u00e9s \u00e0 un \u00e9v\u00e9nement.</p>\n  \n<p>Une fois que votre <a href=\"https://forms-formulaires.agr.gc.ca/pdf/A4340-F.pdf\">Autorisation  g\u00e9n\u00e9rale de voyage (formulaire) (PDF)</a> (AAFC /AAC\u00a04340) d\u00fbment remplie  est approuv\u00e9e, envoyez-la pour traitement \u00e0 <a href=\"mailto:aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca\">aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca</a>.</p>\n  \n  <p>La <b>demande de voyage g\u00e9n\u00e9rale (DVG)</b> sert \u00e0  pr\u00e9autoriser les voyages de nature <b>continuelle</b> ou <b>r\u00e9p\u00e9titive</b> qui se font pour une raison particuli\u00e8re, dans une r\u00e9gion  g\u00e9ographique pr\u00e9cise et pendant une dur\u00e9e d\u00e9termin\u00e9e, et <b>elle se limite \u00e0 ce  qui suit\u00a0</b>:</p>\n<ol>\n  <li>D\u00e9placements locaux (sans nuit\u00e9e), y compris la location de v\u00e9hicules, v\u00e9hicule du parc automobile, le kilom\u00e9trage  et les repas;</li>\n  <li>Voyages  au Canada avec nuit\u00e9e, mais sans transport a\u00e9rien ou ferroviaire.</li>\n  <li>Les r\u00e9servations ne peuvent pas \u00eatre  effectu\u00e9es \u00e0 l'aide de l'Outil de r\u00e9servation en ligne (OREL)/centre d'appels  pour les voyages. Le voyageur doit faire ses r\u00e9servations de v\u00e9hicule ou de  logement directement aupr\u00e8s d'un fournisseur autoris\u00e9. Consultez le <a href=\"http://rehelv-acrd.tpsgc-pwgsc.gc.ca/index-fra.aspx\">R\u00e9pertoire des \u00e9tablissements d'h\u00e9bergement et des entreprises de location de v\u00e9hicules</a>.</li>\n</ol>\n\n<p>La DGV doit \u00eatre cr\u00e9\u00e9e dans le Portail des Services de voyage partag\u00e9s par le voyageur ou l'organisateur  et elle doit \u00eatre approuv\u00e9e par le gestionnaire d\u00e9l\u00e9gu\u00e9 comp\u00e9tent. Le <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101402621&amp;lang=fra\" target=\"_blank\">Guide sur la cr\u00e9ation d'une demande de voyage g\u00e9n\u00e9rale (PDF)</a> vise \u00e0  faciliter la cr\u00e9ation et le traitement des demandes.<br>\n  Pour obtenir plus d'information, consultez le <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100491499&amp;lang=fra\" target=\"_blank\">Guide sur l'autorisation g\u00e9n\u00e9rale de voyager et la demande de voyage g\u00e9n\u00e9rale (Word)</a>.</p>\n\n<h2>Contactez-nous</h2>\n<p>Vous avez des questions? Consultez la <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100477477&amp;lang=fra\" target=\"_blank\">liste de personnes-ressources cl\u00e9s d'Agriculture et Agroalimentaire Canada pour les voyages (Word)</a> afin de savoir \u00e0 qui vous adresser.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Get Your Travel Approved",
        "fr": "Faites approuver votre voyage"
    }
}