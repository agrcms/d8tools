{
    "dcr_id": "1580243185484",
    "lang": "en",
    "title": {
        "en": "Introducing the New Structure of AAFC Online Website",
        "fr": "Pr\u00e9sentation de la nouvelle structure du site Web d'AAC "
    },
    "modified": "2020-01-30 00:00:00.0",
    "issued": "2020-01-30 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2020-01-28",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2020-01-30",
            "fr": "2020-01-30"
        },
        "modified": {
            "en": "2020-01-30",
            "fr": "2020-01-30"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Introducing the New Structure of AAFC Online Website",
            "fr": "Pr\u00e9sentation de la nouvelle structure du site Web d'AAC "
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "The Public Affairs Branch, in collaboration with the Information Systems Branch, is planning some important changes to AAFC's website (www.agr.gc.ca) to align with the Canada.ca design, as per Treasury Board Secretariat directives.",
            "fr": "En collaboration avec la Direction g\u00e9n\u00e9rale des syst\u00e8mes d'information, la Direction g\u00e9n\u00e9rale des affaires publiques pr\u00e9voit apporter des changements importants au site Web d'AAC (www.agr.gc.ca) afin de l'harmoniser avec la conception de Canada.ca, conform\u00e9ment aux directives du Secr\u00e9tariat du Conseil du Tr\u00e9sor."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>The Public Affairs Branch, in collaboration with the Information Systems Branch, is planning some important changes to AAFC\u2019s website (<a href=\"http://www.agr.gc.ca\">www.agr.gc.ca</a>) to align with the Canada.ca design, as per Treasury Board Secretariat directives. Changes will include a new navigation structure, and\u00a0regrouping existing AAFC web content under new labels and headings that you will see online as of January 31. This is the first step in a series of changes that will eventually result in our entire web presence having a look and feel consistent with how Canada.ca is presented.</p>\n<p>Don\u2019t worry<strong>: </strong>this new structure will not change the content you are used to finding on the site!</p>\n<p>AAFC\u2019s implementation of the Canada.ca design aims to provide users with a website that is focused on top topics and tasks that are of interest to AAFC\u2019s audiences. The new structure aims to improve the findability and usability of our information. These changes are rather subtle and inline with continuous improvements we are making to the website based on regular usability research, analysis of performance analytics and optimization of our web content.</p>\n<h2><strong>Changes coming January 31st include:</strong></h2>\n<ul>\n<li>\n<p>Two navigation options:</p>\n</li>\n</ul>\n<ol>\n<li>The existing menus found at the top of the website will remain until March 2, 2020 (to navigate through the main activities: Programs; Industry, Markets and Trade; Science and Innovation)</li>\n<li>New section doormats (topics) will be introduced in the main content area of the home page linking to our main website\u2019s content areas:</li>\n</ol>\n\n<table cellspacing=\"0\" border=\"1\">\n<tbody>\n<tr>\n<td style=\"width: 137.2pt;\">\n<p>Programs and services</p>\n</td>\n<td style=\"border-color: windowtext windowtext windowtext #000000; width: 138.25pt;\">\n<p>Canadian agri-food sector</p>\n</td>\n<td style=\"border-color: windowtext windowtext windowtext #000000; width: 138.05pt;\">\n<p>Agriculture and climate</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 137.2pt;\">\n<p>International trade of agri-food products</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 138.25pt;\">\n<p>Scientific collaboration and research</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 138.05pt;\">\n<p>Indigenous Peoples and agriculture</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 137.2pt;\">\n<p>Horticulture</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 138.25pt;\">\n<p>Crops</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 138.05pt;\">\n<p>Food products</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 137.2pt;\">\n<p>Animal industry</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 138.25pt;\">\n<p>Youth in agriculture</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 138.05pt;\">\n<p>Food Policy for Canada</p>\n</td>\n</tr>\n</tbody>\n</table>\n<ul>\n<li>The \u201cPrograms and services\u201d page is being improved offering an ability to view programs grouped by categories (what the program is for), with a filterable table to improve findability.</li>\n<li>The \u201cScience and innovation\u201d<strong> </strong>page is renamed \u201cScientific collaboration and research.\u201d</li>\n<li>The \u201cAgricultural practices\u201d page is merged under a new topic page: \"Agriculture and climate.\"</li>\n<li>\u201cCanadian agri-food sector intelligence\u201d will be replaced by a \u201cCanadian agri-food sector\u201d topic page that will include sector overviews, publications and activities such as the Value Chain Roundtables.</li>\n<li>Four main sector topic pages (Food products, Animal industry, Crops, Horticulture) will now be available from AAFC\u2019s home page. They will offer content related to these specific sectors from various field of activities such as trade, science, industry, practices, etc.</li>\n</ul>\n<h2><strong>Next step</strong></h2>\n<p>The next step of the project will be to implement the Canada.ca design (look and feel) and meet TBS\u2019s deadline of complying with the Canada.ca design direction by the end of March. Current plans will see AAFC\u2019s website adopt the new Canada.ca look and feel on March 2, 2020. This will change the way our website looks, including new headers and footers; navigation exclusively using doormats (topics) and breadcrumbs; and new templates\u00a0using the\u00a0Canada.ca colours and overall presentation.</p>\n<p>Stay tuned to discover our new look on March 2 and get more updates in the coming months!</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>En collaboration avec la Direction g\u00e9n\u00e9rale des syst\u00e8mes d\u2019information, la Direction g\u00e9n\u00e9rale des affaires publiques pr\u00e9voit apporter des changements importants au site Web d\u2019AAC (<a href=\"http://www.agr.gc.ca\">www.agr.gc.ca</a>) afin de l\u2019harmoniser avec la conception de Canada.ca, conform\u00e9ment aux directives du Secr\u00e9tariat du Conseil du Tr\u00e9sor. Les changements comprendront une nouvelle structure de navigation et le regroupement du contenu actuel du site d\u2019AAC sous de nouveaux en-t\u00eates et rubriques que vous verrez en ligne \u00e0 partir du 31 janvier. Il s\u2019agit de la premi\u00e8re \u00e9tape d\u2019une s\u00e9rie de changements qui permettront \u00e0 terme d\u2019harmoniser l\u2019ensemble de notre pr\u00e9sence sur le Web avec la conception de Canada.ca.</p>\n<p>\nNe vous inqui\u00e9tez pas, cette nouvelle structure ne changera pas le contenu que vous trouvez habituellement sur le site!</p>\n<p>\nEn adoptant la conception de Canada.ca, AAC entend offrir aux utilisateurs un site Web ax\u00e9 sur les principaux sujets et t\u00e2ches qui int\u00e9ressent les publics d\u2019AAC. La nouvelle structure vise \u00e0 am\u00e9liorer la facilit\u00e9 de recherche et l\u2019utilit\u00e9 de nos renseignements. Ces changements sont plut\u00f4t mineurs et s\u2019inscrivent dans le cadre des am\u00e9liorations continues que nous apportons au site Web en fonction de recherches r\u00e9guli\u00e8res sur la convivialit\u00e9, de l\u2019analyse du rendement et de l\u2019optimisation de notre contenu Web.</p>\n<h2>\n<b>Changements en vigueur \u00e0 compter du 31 janvier :</b></h2>\n<ul>\n<li>\nDeux options de navigation\u00a0:</li>\n</ul>\n<ol>\n<li>Les menus existants situ\u00e9s dans le haut du site Web seront maintenus jusqu\u2019au 2 mars 2020 (pour naviguer dans les principales activit\u00e9s\u00a0 : Programmes; Industrie, march\u00e9s et commerce; Science et innovation)</li>\n<li>De nouveaux menus th\u00e9matiques (sujets) seront introduits dans la zone de contenu principale de la page d\u2019accueil, avec des liens vers les principales zones de contenu de notre site web : </li>\n</ol>\n<table cellpadding=\"0\" cellspacing=\"0\" border=\"1\">\n<tbody>\n<tr>\n<td valign=\"top\" width=\"183\">\n<p>Programmes\u00a0 et services</p>\n</td>\n<td valign=\"top\" width=\"184\">\n<p>Secteur agroalimentaire canadien</p>\n</td>\n<td valign=\"top\" width=\"184\">\n<p>Agriculture et climat </p>\n</td>\n</tr>\n<tr>\n<td valign=\"top\" width=\"183\">\n<p>Commerce\u00a0international de produits agroalimentaires </p>\n</td>\n<td valign=\"top\" width=\"184\">\n<p>Collaboration\u00a0scientifique et recherche</p>\n</td>\n<td valign=\"top\" width=\"184\">\n<p>Les Autochtones\u00a0et l\u2019agriculture </p>\n</td>\n</tr>\n<tr>\n<td valign=\"top\" width=\"183\">\n<p>Horticulture</p>\n</td>\n<td valign=\"top\" width=\"184\">Cultures\n</td>\n<td valign=\"top\" width=\"184\">\n<p>Produits alimentaires\u00a0 </p>\n</td>\n</tr>\n<tr>\n<td valign=\"top\" width=\"183\">\n<p>Productions animales </p>\n</td>\n<td valign=\"top\" width=\"184\">\n<p>Les jeunes en agriculture </p>\n</td>\n<td valign=\"top\" width=\"184\">\n<p>Politique alimentaire pour le Canada\u00a0 </p>\n</td>\n</tr>\n</tbody>\n</table>\n<ul>\n<li>La page \u00ab Programmes et services \u00bb est am\u00e9lior\u00e9e pour permettre de voir les programmes regroup\u00e9s en cat\u00e9gories (ce que vise le programme), dans un tableau muni de filtres pour faciliter la recherche.</li>\n<li>La page \u00ab Science et innovation \u00bb est rebaptis\u00e9e \u00ab Collaboration scientifique et recherche \u00bb.</li>\n<li>La page \u00ab Pratiques agricoles \u00bb est fusionn\u00e9e sous une nouvelle page th\u00e9matique : Agriculture et climat.</li>\n<li>La page \u00ab Renseignements sur les secteurs agroalimentaires canadiens \u00bb sera remplac\u00e9e par une page th\u00e9matique \u00ab Secteur agroalimentaire canadien \u00bb qui comprendra des aper\u00e7us sectoriels, des publications et des activit\u00e9s, comme les tables rondes sur la cha\u00eene de valeur.<b></b></li>\n<li>Quatre grandes pages th\u00e9matiques sectorielles (Produits alimentaires, Productions animales, Cultures, Horticulture) sont d\u00e9sormais disponibles sur la page d\u2019accueil d\u2019AAC. Elles pr\u00e9senteront un contenu sur ces diverses industries provenant de domaines d\u2019activit\u00e9s comme le commerce, la science, l\u2019industrie, les pratiques.<b></b></li>\n</ul>\n<h2>\n<b>Prochaine \u00e9tape</b></h2>\n<p>\nLa prochaine \u00e9tape du projet consistera \u00e0 mettre en \u0153uvre la conception de Canada.ca (aspect et convivialit\u00e9) et \u00e0 respecter l\u2019\u00e9ch\u00e9ance fix\u00e9e par le SCT pour se conformer \u00e0 l\u2019orientation de la conception de Canada.ca, soit d\u2019ici la fin mars. Selon les plans actuels, le site Web d\u2019AAC adoptera la nouvelle conception graphique de Canada.ca le 2 mars 2020. Ce changement modifiera l\u2019apparence d notre site Web, notamment : un nouvel en-t\u00eate et un nouveau pied de page, une navigation exclusivement ax\u00e9e sur des menus th\u00e9matiques (sujets) et les fils<br>d\u2019Ariane (pistes de navigation), ainsi que de nouveaux gabarits pour que notre site Web aient les couleurs et la pr\u00e9sentation g\u00e9n\u00e9rale de Canada.ca.</p>\n<p>\nRestez \u00e0 l\u2019aff\u00fbt et d\u00e9couvrez notre nouveau look le 2 mars. D\u2019autres mises \u00e0 jour seront communiqu\u00e9es dans les mois \u00e0 venir!</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Introducing the New Structure of AAFC Online Website",
        "fr": "Pr\u00e9sentation de la nouvelle structure du site Web d'AAC"
    },
    "news": {
        "date_posted": {
            "en": "2020-01-30",
            "fr": "2020-01-30"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Digital Communications",
            "fr": "Communications num\u00e9riques"
        }
    }
}