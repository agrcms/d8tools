{
    "dcr_id": "1594129892482",
    "lang": "en",
    "title": {
        "en": "Access to Information Obligations While Working Remotely",
        "fr": "Obligations des t\u00e9l\u00e9travailleurs en mati\u00e8re d\u2019acc\u00e8s \u00e0 l\u2019information"
    },
    "modified": "2020-07-08 00:00:00.0",
    "issued": "2020-07-08 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2020-07-07",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2020-07-08",
            "fr": "2020-07-08"
        },
        "modified": {
            "en": "2020-07-08",
            "fr": "2020-07-09"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Access to Information Obligations While Working Remotely",
            "fr": "Obligations des t\u00e9l\u00e9travailleurs en mati\u00e8re d\u2019acc\u00e8s \u00e0 l\u2019information"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Over the past few months, working remotely has presented many challenges, including the way we work and the tools we use.",
            "fr": "Au cours des derniers mois, le t\u00e9l\u00e9travail a entra\u00een\u00e9 de nombreux changements, notamment \u00e0 nos m\u00e9thodes de travail et \u00e0 nos outils."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>Over the past few months, working remotely has presented many challenges, including the way we work and the tools we use. The Access to Information and Privacy (ATIP) Office is no exception as we have had to adapt to a completely digital ATIP process to continue providing timely access to departmental records. We know it\u2019s been an adjustment for many, and we thank everyone for their support to date!</p>\n<p>Here are some quick tips that will help you navigate the ATIP process when working remotely:</p>\n<ul>\n<li>At this time, no one should be going into the office to retrieve records; only electronic documents should be provided. Please identify any records held on-site.</li>\n<li>Communicate early with your branch Office of Primary Interest <a href=\"https://collab.agr.gc.ca/co/aips-saiprp/Documents/AAFCAAC-4157314-v6-OPI_LIST.XLSX\">representative</a> if the request is unclear, more time is required, or if there are other impediments to providing information.</li>\n<li>Review information for relevancy and remove duplicates. Provide all records to the ATIP Office electronically.</li>\n<li>Identify sensitive information for possible exemption and explain why it is sensitive; give context if required.</li>\n</ul>\n<p>We\u2019ve received a lot of questions on GCcollaboration and whether the information contained within is subject to the <em>Access to Information Act</em> and the <em>Privacy Act</em>. The answer is yes. It may also be relevant to legal cases, so we encourage employees to be aware of and practice <a href=\"https://intranet.agr.gc.ca/agrisource/eng/newswork/managing-your-information-in-gccollaboration?id=1594142600868\">proper information management</a>.</p>\n<p>The new digital ATIP procedures are still a work in progress, and we look forward to providing more information and offering updated training sessions soon.</p>\n<p>If you have any questions, please contact <a href=\"mailto:aafc.atip-aiprp.aac@canada.ca\">aafc.atip-aiprp.aac@canada.ca</a></p>\n<h2>More Information</h2>\n<ul>\n<li><a href=\"https://collab.agr.gc.ca/co/aips-saiprp/SitePages/Home.aspx\">ATIP services</a></li>\n<li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/ATIP%20and%20IM%20-%20AIPRP%20et%20GI.aspx\">ATIP and information management best practices</a></li>\n<li><a target=\"_blank\" href=\"https://collab.agr.gc.ca/co/ima_sgi/_layouts/15/DocIdRedir.aspx?ID=AGR-8526043\">Information Management Requirements for GCcollaboration</a>\u00a0(Word)</li>\n</ul>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Au cours des derniers mois, le t\u00e9l\u00e9travail a entra\u00een\u00e9 de nombreux changements, notamment \u00e0 nos m\u00e9thodes de travail et \u00e0 nos outils. Le Bureau de l\u2019acc\u00e8s \u00e0 l\u2019information et de la protection des renseignements personnels (AIPRP) ne fait pas exception : il a d\u00fb s\u2019adapter \u00e0 un processus enti\u00e8rement num\u00e9rique afin de continuer \u00e0 fournir un acc\u00e8s aux documents minist\u00e9riels dans les d\u00e9lais voulus. Nous savons que ce changement a aussi oblig\u00e9 bon nombre d\u2019entre vous \u00e0 s\u2019adapter, et nous vous remercions de votre soutien \u00e0 ce jour.</p>\n<p>Voici quelques petits conseils pour vous aider \u00e0 appliquer le processus d\u2019AIPRP \u00e0 partir de la maison :</p>\n<ul>\n<li>\u00c0 l\u2019heure actuelle, personne ne doit se rendre au bureau pour r\u00e9cup\u00e9rer des documents; vous ne devez fournir que des documents \u00e9lectroniques. Dressez la liste des documents conserv\u00e9s sur place.</li>\n<li>Communiquez avec le <a href=\"https://collab.agr.gc.ca/co/aips-saiprp/Documents/ATIP%20LISTE%20BPR.XLSX\">repr\u00e9sentant du bureau de premi\u00e8re responsabilit\u00e9</a> de votre direction g\u00e9n\u00e9rale si la demande n\u2019est pas claire, si vous avez besoin de plus de temps ou si vous vous butez \u00e0 d\u2019autres obstacles \u00e0 la communication de l\u2019information.</li>\n<li>Passez l\u2019information en revue pour vous assurer de sa pertinence et \u00e9liminer les doubles. Transmettez tous les documents au Bureau de l\u2019AIPRP par voie \u00e9lectronique.</li>\n<li>Relevez l\u2019information de nature d\u00e9licate qui pourrait faire l\u2019objet d\u2019une exception. Expliquez en quoi l\u2019information est d\u00e9licate et faites des mises en contexte au besoin.</li>\n</ul>\n<p>Nous avons re\u00e7u de nombreuses questions \u00e0 savoir si l\u2019information contenue sur GCcollaboration est assujettie \u00e0 la <em>Loi sur l\u2019acc\u00e8s \u00e0 l\u2019information</em> et \u00e0 la <em>Loi sur la protection des renseignements personnels</em>. La r\u00e9ponse est oui. L\u2019information peut \u00e9galement servir en cas de recours judiciaire, alors nous encourageons les employ\u00e9s \u00e0 <a href=\"https://intranet.agr.gc.ca/agrisource/fra/nouvelleslouvrage/gestion-de-vos-renseignements-dans-gccollaboration?id=1594142600868\">pratiquer une saine gestion de l\u2019information</a>.</p>\n<p>Le nouveau processus num\u00e9rique en mati\u00e8re d\u2019AIPRP continue d\u2019\u00e9voluer et nous vous offrirons prochainement plus de renseignements et une formation \u00e0 jour.</p>\n<p>Si vous avez des questions, veuillez envoyer un courriel \u00e0 <a href=\"mailto:aafc.atip-aiprp.aac@canada.ca\">aafc.atip-aiprp.aac@canada.ca</a></p>\n<h2>Renseignements suppl\u00e9mentaires</h2>\n<ul>\n<li><a href=\"https://collab.agr.gc.ca/co/aips-saiprp/SitePages/Home.aspx\">Services \u00e0 l\u2019AIPRP</a></li>\n<li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/ATIP%20and%20IM%20-%20AIPRP%20et%20GI.aspx\">AIPRP et pratiques exemplaires en mati\u00e8re de gestion de l\u2019information</a></li>\n<li><a target=\"_blank\" href=\"https://collab.agr.gc.ca/co/ima_sgi/Documents/Gestion%20de%20l%E2%80%99information%20avec%20le%20syst%C3%A8me%20GCcollaboration%20dans%20le%20contexte%20de%20la%20COVID.docx\">Gestion de l\u2019information au moyen du syst\u00e8me GCcollaboration</a>\u00a0(Word)</li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Access to Information Obligations While Working Remotely",
        "fr": "Obligations des t\u00e9l\u00e9travailleurs en mati\u00e8re d'acc\u00e8s \u00e0 l'information"
    },
    "news": {
        "date_posted": {
            "en": "2020-07-09",
            "fr": "2020-07-09"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Public Affairs Branch",
            "fr": "Direction g\u00e9n\u00e9rale des affaires publiques"
        }
    }
}