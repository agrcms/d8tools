{
    "dcr_id": "1516910397405",
    "lang": "en",
    "title": {
        "en": "Assistant Director - Operations (AgriInsurance Program) ",
        "fr": "Directeur adjoint/directrice adjointe - Op\u00e9rations (programme Agri-protection) "
    },
    "modified": "2018-01-30 00:00:00.0",
    "issued": "2018-01-30 00:00:00.0",
    "type_name": "intra-intra/empl-empl",
    "node_id": "1279029955397",
    "layout_name": null,
    "dc_date_created": "2018-01-30",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-01-30",
            "fr": "2018-01-30"
        },
        "modified": {
            "en": "2018-01-30",
            "fr": "2018-01-30"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Assistant Director - Operations (AgriInsurance Program) ",
            "fr": "Directeur adjoint/directrice adjointe - Op\u00e9rations (programme Agri-protection) "
        },
        "subject": {
            "en": "staffing",
            "fr": "dotation en personnel"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "notice",
            "fr": "avis"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n<p><strong>Assignment Duration:</strong> Approximately 9 months (Feb. 12 \u2013 Oct. 12, 2018) with a possibility of extension.</p>\n<p>\u00a0</p>\n<p><strong>Background</strong></p>\n<p>AgriInsurance is the vehicle through which producers purchase insurance coverage to guarantee a certain level of production/quality for a specific agricultural commodity. The cost of the insurance premiums is partially shared by the federal and provincial governments to ensure the affordability of coverage for producers. By purchasing AgriInsurance coverage, producers minimize the effects of production losses caused by natural hazards and thereby increase their ability to manage production risks associated with weather and disease events.</p>\n<p>The objectives of AAFC\u2019s AgriInsurance Program is to minimize the financial impact of producers\u2019 production and productive asset losses caused by severe but uncontrollable natural hazards like drought, flood, wind, frost, excessive rain or heat, snow, as well as losses resulting from uncontrollable diseases and insect infestations. This is achieved by providing actuarially sound insurance coverage for agricultural products.</p>\n<p>Under the AgriInsurance program, there is also the possibility to offer a Wildlife Compensation program to producers. This program is not an insurance product. It is a compensation program for producers who experience agricultural losses (revenue loss) due to uncontrollable wildlife.</p>\n<p>Even if the provinces are responsible for the design, development, delivery and management of the insurance products they offer to their producers, the AgriInsurance Division manages the federal role in the provincially-delivered AgriInsurance Programs by:</p>\n<ul>\n<li>Providing oversight over the entire program across Canada;</li>\n<li>Contributing financially to the premium cost of provincially administered insurance products that conform with the Farm Income Protection Act (FIPA), The Canada Production Insurance Regulations and the Federal-Provincial-Territorial Multilateral Framework Agreement;</li>\n<li>Providing reinsurance protection to Provinces;</li>\n<li>Reviewing provincial premium rates;</li>\n<li>Facilitating the development, modification, and promotion of production insurance plans administered by provinces; and</li>\n<li>Providing funding for provincial administrative costs for the delivery of the AgriInsurance Program.</li>\n</ul>\n<p>\u00a0</p>\n<p><strong>Duties</strong></p>\n<p>The selected candidate will manage the National Operations Unit. The Unit acts as the federal liaison with the provincial delivery agents for AgriInsurance and is responsible for the overall management and co-ordination of the program ensuring Insurance products respect the criteria for federal funding.</p>\n<p>The Assistant Director - Operations is in charge of:</p>\n<ul>\n<li>\n<p>Negotiating federal/provincial contribution agreements;</p>\n</li>\n<li>\n<p>Negotiating insurance products changes;</p>\n</li>\n<li>\n<p>Monitoring program compliance audits;</p>\n</li>\n<li>\n<p>Managing the Production Insurance National Statistical System (PINSS).</p>\n</li>\n</ul>\n<p>The Assistant Director - Operations also supervises the Program Managers, Program Officers and the Statistical Operation. They support the Assistant Director \u2013 Operations in his/her duties.</p>\n<p>Furthermore, the Assistant Director leads, manages or contributes to the development of policy/program discussions for program parameter changes to assist provinces and federal senior managers in the program improvement and development.</p>\n<p><br>\n<strong>Essential Qualifications</strong></p>\n<p>Bilingual imperative CBC/CBC;</p>\n<p>Graduation with a degree from a recognized post-secondary institution in Business Administration, Economics, Finance, Agriculture or an acceptable combination of education and experience;</p>\n<p>Experience in managing the operations of a program to private sector clients or provinces;</p>\n<p>Experience in the design, management and delivery of a program to private sector clients or provinces;</p>\n<p>Experience in providing strategic program and operational advice to management.</p>\n<p>Analytical thinking</p>\n<p>Interactive Communication</p>\n<p>Writing Skills</p>\n<p>Teamwork</p>\n<p>Initiative</p>\n<p>\u00a0</p>\n<p><strong>Asset Qualifications</strong></p>\n<p>Experience in delivering legislated and/or regulated programming;</p>\n<p>Experience in managing the operations of an agricultural program to private sector clients or provinces;</p>\n<p>Experience in the design, management and delivery of an agricultural program to private sector clients or provinces;</p>\n<p>Experience in managing human and financial resources;</p>\n<p>Knowledge of farm business, financing and risk management.</p>\n<p>\u00a0</p>\n<p><strong>Conditions of Employment</strong></p>\n<p>Candidates must be able and willing to travel and work overtime when needed<strong> </strong></p>\n<p>\u00a0</p>\n<p><strong>Security</strong><br>\n<br>\nReliability <em>(must be maintained during employment)</em></p>\n<p>\u00a0</p>\n<p><strong>Organizational Needs</strong></p>\n<p>In support of achieving a diversified workforce, consideration may be given to candidates self-identifying as belonging to one of the following Employment Equity groups: Aboriginal peoples, Persons with a Disability; Visible Minorities, Women.</p>\n<p>Preference may be given to AAFC employees affected by workforce adjustment.</p>\n<p>\u00a0</p>\n<p><strong>Applications</strong><br>\n<br>\nInterested AAFC and CFIA employees are encouraged to forward their Cover Letter (2 pages maximum) and Resume to remi.villeneuve@agr.gc.ca</p>\n<p><em>Applicants must clearly demonstrate on their application that they meet the essential qualifications related to linguistic requirements, education and experience (and any asset qualifications if applicable) and are within the area of selection. Failure to do so may result in the rejection of your application.</em></p>\n<p><em>Candidates must clearly demonstrate IN THEIR COVER LETTER how they meet the education and experience factors listed as essential and asset qualifications by using concrete examples. Candidates must use each education/experience factor as a header and then write one or two paragraphs demonstrating how they meet each requirement by giving concrete examples. Resumes may be used as a secondary source to validate the experience described in the cover letter. FAILURE TO CLEARLY DEMONSTRATE HOW YOU MEET THE SCREENING CRITERIA MAY RESULT IN THE REJECTION OF YOUR APPLICATION. CANDIDATES WILL NOT BE SOLICITED FOR INCOMPLETE OR POSSIBLE MISSING INFORMATION.</em></p>\n<p>Resumes must clearly identify:</p>\n<ul>\n<li>Your tenure (e.g. term or indeterminate)</li>\n<li>Your substantive group and level</li>\n</ul>\n<p><strong>Notes:</strong></p>\n<ul>\n<li>Candidate must have manager\u2019s approval prior to applying</li>\n<li>Communication for this process will be sent via email.</li>\n<li>General inquiries can be directed to remi.villeneuve@agr.gc.ca</li>\n<li>Depending on the number of candidates who apply, assets may be used as a screening tool.</li>\n<li>Candidates may be assessed through an interview</li>\n<li>Reference checks may be sought</li>\n</ul>\n",
        "fr": "\n<p><strong>Dur\u00e9e de l\u2019affectation :</strong> Environ 9 mois (du 12 f\u00e9vrier au 12 octobre 2018) avec une possibilit\u00e9 de prolongation</p>\n<p>\u00a0</p>\n<p><strong>Contexte</strong></p>\n<p>Agri-protection est le moyen par lequel les producteurs souscrivent \u00e0 une assurance pour leur garantir un certain niveau de qualit\u00e9/production \u00e0 l\u2019\u00e9gard d\u2019un produit agricole sp\u00e9cifique. Les co\u00fbts li\u00e9s aux primes d\u2019assurance sont partiellement partag\u00e9s par les gouvernements f\u00e9d\u00e9ral et provinciaux afin d\u2019assurer le caract\u00e8re abordable de la couverture pour les producteurs. En souscrivant \u00e0 Agri-protection, les producteurs r\u00e9duisent les effets des pertes de production attribuables \u00e0 des al\u00e9as naturels et accroissent ainsi leur capacit\u00e9 de g\u00e9rer les risques de production associ\u00e9s \u00e0 la maladie et aux mauvaises conditions m\u00e9t\u00e9orologiques.</p>\n<p>Le programme Agri-protection d\u2019AAC a pour but de r\u00e9duire au minimum les r\u00e9percussions financi\u00e8res des pertes de production et d\u2019actifs li\u00e9es \u00e0 des catastrophes naturelles graves mais incontr\u00f4lables, par exemple les s\u00e9cheresses, les inondations, le vent, le gel, les pr\u00e9cipitations ou la chaleur excessives et la neige, de m\u00eame que les pertes caus\u00e9es par des maladies incontr\u00f4lables et des infestations d\u2019insectes. Cela se fait gr\u00e2ce \u00e0 une protection d\u2019assurance fiable sur le plan actuariel pour les produits agricoles.</p>\n<p>Le programme Agri-protection offre aussi aux producteurs la possibilit\u00e9 d\u2019acc\u00e8s au Programme d\u2019indemnisation pour les dommages caus\u00e9s par la faune. Ce programme n\u2019est pas un produit d\u2019assurance. Il s\u2019agit d\u2019un programme d\u2019indemnisation pour les producteurs qui \u00e9prouvent des pertes agricoles (perte de revenu) \u00e0 cause de la faune incontr\u00f4lable.</p>\n<p>Les provinces sont responsables de la conception, de l\u2019\u00e9laboration, de la prestation et de la gestion des produits d\u2019assurance qu\u2019elles offrent \u00e0 leurs producteurs, mais c\u2019est la Division de l\u2019assurance-production qui g\u00e8re le r\u00f4le f\u00e9d\u00e9ral dans les programmes d\u2019Agri-protection offerts par les provinces :</p>\n<ul>\n<li>La Division supervise l\u2019ensemble du programme aux quatre coins du Canada;</li>\n<li>La Division contribue financi\u00e8rement au co\u00fbt des primes des produits d\u2019assurance administr\u00e9s par les provinces qui sont conformes \u00e0 la <em>Loi sur la protection du revenu agricole</em>, au <em>R\u00e8glement canadien sur l\u2019assurance production</em> et \u00e0 l\u2019Accord-cadre multilat\u00e9ral f\u00e9d\u00e9ral-provincial-territorial;</li>\n<li>La Division offre une protection de r\u00e9assurance aux provinces;</li>\n<li>La Division examine les taux de primes provinciaux;</li>\n<li>La Division facilite l\u2019\u00e9laboration, la modification et la promotion des r\u00e9gimes d\u2019assurance-production administr\u00e9s par les provinces;</li>\n<li>La Division offre du financement pour couvrir les frais administratifs li\u00e9s \u00e0 la prestation du programme Agri-protection.</li>\n</ul>\n<p>\u00a0</p>\n<p><strong>Fonctions</strong></p>\n<p>Le candidat retenu ou la candidate retenue g\u00e9rera l\u2019Unit\u00e9 des op\u00e9rations nationales. Cette Unit\u00e9 sert de point de liaison f\u00e9d\u00e9ral avec les agents provinciaux de prestation d\u2019Agri-protection et est responsable de la gestion et de la coordination g\u00e9n\u00e9rales du programme, en veillant \u00e0 ce que les produits d\u2019assurance respectent les crit\u00e8res de financement du gouvernement f\u00e9d\u00e9ral.</p>\n<p>Le directeur adjoint - op\u00e9rations ou la directrice adjointe - op\u00e9rations est responsable de:</p>\n<ul>\n<li>\n<p>N\u00e9gocier les accords de contribution f\u00e9d\u00e9raux-provinciaux;</p>\n</li>\n<li>\n<p>N\u00e9gocier les changements aux produits d\u2019assurance;</p>\n</li>\n<li>\n<p>Surveiller les v\u00e9rifications de conformit\u00e9 de programme;</p>\n</li>\n<li>\n<p>G\u00e9rer le Syst\u00e8me de statistiques nationales pour l\u2019assurance production (SSNAP);</p>\n</li>\n</ul>\n<p>Le directeur adjoint - op\u00e9rations ou la directrice adjointe - op\u00e9rations supervise \u00e9galement les gestionnaires de programmes, les agents de programmes et l\u2019agent des op\u00e9rations statistiques. Ils supportent le directeur adjoint - op\u00e9rations ou la directrice adjointe - op\u00e9rations dans ses fonctions.</p>\n<p>De plus, le directeur adjoint ou la directrice adjointe dirige, g\u00e8re ou prend part aux discussions sur l\u2019\u00e9laboration des politiques/programmes en vue des changements aux param\u00e8tres du programme pour aider les provinces et les gestionnaires f\u00e9d\u00e9raux principaux dans le cadre de l\u2019am\u00e9lioration de celui-ci.</p>\n<p><br>\n<strong>Qualifications essentielles</strong></p>\n<p>Poste bilingue, CBC/CBC.</p>\n<p>Un grade d\u2019un \u00e9tablissement post-secondaire reconnu en administration des affaires, en \u00e9conomie, en finances, en agriculture ou combinaison acceptable d\u2019\u00e9tudes et d\u2019exp\u00e9rience.</p>\n<p>Exp\u00e9rience de la gestion des op\u00e9rations dans le cadre d\u2019un programme destin\u00e9 \u00e0 des clients du secteur priv\u00e9 ou \u00e0 des provinces.</p>\n<p>Exp\u00e9rience de la conception, de la gestion et de la prestation d\u2019un programme destin\u00e9 \u00e0 des clients du secteur priv\u00e9 ou \u00e0 des provinces.</p>\n<p>Exp\u00e9rience de la formulation de conseils strat\u00e9giques sur les programmes et les op\u00e9rations \u00e0 l\u2019intention des cadres sup\u00e9rieurs.</p>\n<p>R\u00e9flexion analytique</p>\n<p>Communication interactive</p>\n<p>Aptitudes en r\u00e9daction</p>\n<p>Travail d\u2019\u00e9quipe</p>\n<p>Initiative</p>\n<p>\u00a0</p>\n<p><strong>Qualifications constituant un atout</strong></p>\n<p>Exp\u00e9rience de la prestation de programmes r\u00e9gis par des lois ou des r\u00e8glements.</p>\n<p>Exp\u00e9rience de la gestion des op\u00e9rations dans le cadre d\u2019un programme agricole destin\u00e9 \u00e0 des clients du secteur priv\u00e9 ou \u00e0 des provinces.</p>\n<p>Exp\u00e9rience de la conception, de la gestion et de la prestation d\u2019un programme agricole destin\u00e9 \u00e0 des clients du secteur priv\u00e9 ou \u00e0 des provinces.</p>\n<p>Exp\u00e9rience de la gestion de ressources humaines et financi\u00e8res.</p>\n<p>Connaissances dans le secteur des entreprises agricoles, des finances et de la gestion des risques.</p>\n<p>\u00a0</p>\n<p><strong>Conditions d\u2019emploi</strong></p>\n<p>Les candidats doivent \u00eatre apte et dispos\u00e9s \u00e0 voyager et aptes \u00e0 faire du temps suppl\u00e9mentaire au besoin.<strong> </strong></p>\n<p>\u00a0</p>\n<p><strong>S\u00e9curit\u00e9 </strong></p>\n<p>Fiabilit\u00e9 (le titulaire doit satisfaire \u00e0 cette condition pendant toute la dur\u00e9e d\u2019emploi).</p>\n<p><strong>Besoins organisationnels</strong></p>\n<p>Pour les besoins de la diversification de l\u2019effectif, une attention particuli\u00e8re pourrait \u00eatre accord\u00e9e aux candidats qui auront indiqu\u00e9 leur appartenance \u00e0 l\u2019un des groupes suivants, vis\u00e9s par l\u2019\u00e9quit\u00e9 en mati\u00e8re d\u2019emploi : Autochtones, personnes handicap\u00e9es, membres d\u2019une minorit\u00e9 visible, femmes.</p>\n<p>La pr\u00e9f\u00e9rence pourrait \u00eatre accord\u00e9e aux employ\u00e9s d\u2019AAC touch\u00e9s par le r\u00e9am\u00e9nagement des effectifs.</p>\n<p>\u00a0</p>\n<p><strong>Pour poser sa candidature</strong><br>\n<br>\nOn encourage les employ\u00e9s d\u2019AAC et\u00a0l'ACIA\u00a0int\u00e9ress\u00e9s \u00e0 transmettre leur lettre de pr\u00e9sentation (2 pages maximum) et leur curriculum vitae \u00e0 remi.villeneuve@agr.gc.ca.</p>\n<p><em>Les candidats doivent clairement d\u00e9montrer dans leur demande qu\u2019ils poss\u00e8dent toutes les qualifications essentielles en mati\u00e8re d\u2019exigences linguistiques, d\u2019\u00e9tude et d\u2019exp\u00e9rience et qu\u2019ils se trouvent dans la zone de s\u00e9lection. Les candidats qui ne se conformeront pas \u00e0 ces consignes pourraient voir leur candidature rejet\u00e9e.</em></p>\n<p><em>Les candidats doivent d\u00e9montrer clairement, DANS LEUR LETTRE DE PR\u00c9SENTATION, qu\u2019ils satisfont aux crit\u00e8res en mati\u00e8re d\u2019\u00e9tudes et d\u2019exp\u00e9rience figurant concernant \u00e0 la rubrique \u00ab Qualifications essentielles \u00bb et \u00ab Qualifications constituant un aout \u00bb au moyen d\u2019exemples concrets. Les candidats doivent utiliser chaque crit\u00e8re relatif aux \u00e9tudes ou \u00e0 l\u2019exp\u00e9rience comme rubrique, puis r\u00e9diger un ou deux paragraphes qui d\u00e9montrent par des exemples concrets comment ils satisfont \u00e0 chaque crit\u00e8re. Les curriculum vit\u00e6 pourront \u00eatre utilis\u00e9s comme source d\u2019information secondaire pour valider l\u2019exp\u00e9rience d\u00e9crite dans la lettre de pr\u00e9sentation. LES CANDIDATS QUI N\u2019AURONT PAS CLAIREMENT D\u00c9MONTR\u00c9 EN QUOI ILS SATISFONT AUX CRIT\u00c8RES DE PR\u00c9S\u00c9LECTION POURRAIENT VOIR LEUR CANDIDATURE REJET\u00c9E. NOUS NE COMMUNIQUERONS PAS AVEC LES CANDIDATS POUR OBTENIR LES RENSEIGNEMENTS MANQUANTS OU POUVANT AVOIR \u00c9T\u00c9 OMIS.</em></p>\n<p>Dans leur curriculum vitae, les candidats doivent clairement indiquer :</p>\n<ul>\n<li>la dur\u00e9e de leur emploi (p. ex. d\u00e9termin\u00e9e ou ind\u00e9termin\u00e9e);</li>\n<li>leur groupe et niveau de titularisation.</li>\n</ul>\n<p><strong>Remarques :</strong></p>\n<ul>\n<li>Les employ\u00e9s doivent avoir l\u2019approbation de leur gestionnaire avant de pr\u00e9senter leur candidature.</li>\n<li>Les communications relatives \u00e0 ce processus se feront par courriel.</li>\n<li>Les demandes de renseignements g\u00e9n\u00e9raux peuvent \u00eatre adress\u00e9es \u00e0 remi.villeneuve@agr.gc.ca.</li>\n<li>Selon le nombre de candidats qui poseront leur candidature, les qualifications constituant un atout pourraient \u00eatre utilis\u00e9es comme outil de pr\u00e9s\u00e9lection.</li>\n<li>Les candidats pourront \u00eatre \u00e9valu\u00e9s au moyen d\u2019une entrevue.</li>\n<li>Les r\u00e9f\u00e9rences des candidats pourraient faire l\u2019objet d\u2019une v\u00e9rification.</li>\n</ul>\n\n"
    },
    "breadcrumb": {
        "en": "Assistant Director - Operations (AgriInsurance Program)",
        "fr": "Directeur adjoint/directrice adjointe - Op\u00e9rations (programme Agri-protection)"
    },
    "empl": {
        "type": {
            "en": "Assignment",
            "fr": "Affectation"
        },
        "classification": {
            "en": "CO-3",
            "fr": "CO-3"
        },
        "branch": {
            "en": "Programs Branch",
            "fr": "Direction g\u00e9n\u00e9rale des programmes"
        },
        "locations": {
            "en": "Ottawa, Ontario - NHCAP",
            "fr": "Ottawa (Ontario) - CACPA"
        },
        "date_closing": {
            "en": "2018-02-06",
            "fr": "2018-02-06"
        },
        "open_to": {
            "en": "AAFC and CFIA Employees located in the NCR, at the CO-03 group and level and equivalent",
            "fr": "Employ\u00e9s d\u2019AAC et de l\u2019ACIA de la RCN de niveau CO-3 ou l\u2019\u00e9quivalent"
        },
        "name": {
            "en": "R\u00e9mi Villeneuve",
            "fr": "R\u00e9mi Villeneuve"
        },
        "email": {
            "en": "Remi.Villeneuve@agr.gc.ca",
            "fr": "Remi.Villeneuve@agr.gc.ca"
        }
    }
}