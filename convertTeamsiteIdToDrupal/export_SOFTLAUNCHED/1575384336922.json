{
    "dcr_id": "1575384336922",
    "lang": "en",
    "title": {
        "en": "Reminder - Defining Disabilities in the Workplace - A Lot Broader Than You Think!",
        "fr": "Rappel - La d\u00e9finition d\u2019incapacit\u00e9 en milieu de travail est bien plus large que vous ne pourriez le penser! "
    },
    "modified": "2019-12-11 00:00:00.0",
    "issued": "2019-12-05 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-12-03",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-12-05",
            "fr": "2019-12-05"
        },
        "modified": {
            "en": "2019-12-11",
            "fr": "2019-12-11"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Reminder - Defining Disabilities in the Workplace - A Lot Broader Than You Think!",
            "fr": "Rappel - La d\u00e9finition d\u2019incapacit\u00e9 en milieu de travail est bien plus large que vous ne pourriez le penser! "
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "What constitutes a disability in the workplace is more complex than you might think.",
            "fr": "La d\u00e9finition de ce qui constitue une incapacit\u00e9 en milieu de travail est plus complexe que vous ne pourriez le penser."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>What constitutes a disability in the workplace is more complex than you might think. While some disabilities are easily seen, others are less obvious. Disabilities can be permanent, temporary, or episodic; they can be mild or severe.</p>\n<p>Some of the most common <strong>invisible disabilities<em> </em></strong>include:</p>\n<ul>\n<li>Addictions</li>\n<li>ADD/ADHD</li>\n<li>Allergies</li>\n<li>Anxiety disorders</li>\n<li>Asthma</li>\n<li>Autism</li>\n<li>Bipolar disorder</li>\n<li>Chronic pain</li>\n<li>Celiac disease</li>\n<li>Depression</li>\n<li>Diabetes</li>\n<li>Epilepsy</li>\n<li>Glaucoma</li>\n<li>Hearing disorders</li>\n<li>Heart disease</li>\n<li>Learning disabilities</li>\n<li>Migraines</li>\n<li>Obsessive-compulsive disorder</li>\n</ul>\n<h2><strong>Count yourself in \u2013 self identify!</strong></h2>\n<p>Your self-identification information is key to helping us understand and identify how to best support employees with both visible or invisible disabilities at AAFC. The more information you provide, the better we can look to enhance your workplace experience.</p>\n<p>If you have a disability that you feel affects or could affect you in the workplace (even if you have been accommodated), please self-identify. Self-identification is easy and 100% confidential.</p>\n<p>You can:</p>\n<ul>\n<li>Self-identify through <a href=\"https://psoft.agr.gc.ca/psp/peoplesoft8/?cmd=login&amp;languageCd=ENG\">PeopleSoft</a> under Self Service &gt; Personal Information &gt; Employment Equity; or</li>\n<li>Complete a <a href=\"http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3647742&amp;lang=eng\">self-identification form</a>\u00a0(PDF).</li>\n</ul>\n<p>If you have any additional questions or need help completing the self-identification form, please visit <a href=\"http://intranet.agr.gc.ca/agrisource/eng/human-resources/employment-equity-and-inclusiveness?id=1288034634018#b\">AgriSource</a> or contact the Corporate Diversity Team at <a href=\"mailto:aafc.inclusive-inclusif.aac@canada.ca\">aafc.inclusive-inclusif.aac@canada.ca</a>.</p>\n<p>To learn more about the Persons with Disabilities Network and its activities, visit the <a href=\"https://collab.agr.gc.ca/co/pdn-rph/SitePages/Home.aspx\">PwDN Knowledge Workspace site</a>.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/GrowDiversityInclusion-EN2.png\" class=\"center-block\"></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>La d\u00e9finition de ce qui constitue une incapacit\u00e9 en milieu de travail est plus complexe que vous ne pourriez le penser. Alors que certaines incapacit\u00e9s sont visibles, d\u2019autres sont moins \u00e9videntes. Une incapacit\u00e9 peut \u00eatre permanente, temporaire ou \u00e9pisodique. Elle peut \u00eatre mineure ou grave.</p>\n<p>Voici certaines des <strong>incapacit\u00e9s invisibles</strong> les plus courantes :</p>\n<ul>\n<li>D\u00e9pendances</li>\n<li>TDA/TDAH</li>\n<li>Allergies</li>\n<li>Troubles anxieux</li>\n<li>Asthme</li>\n<li>Autisme</li>\n<li>Trouble bipolaire</li>\n<li>Douleur chronique</li>\n<li>Maladie c\u0153liaque</li>\n<li>D\u00e9pression</li>\n<li>Diab\u00e8te</li>\n<li>\u00c9pilepsie</li>\n<li>Glaucome</li>\n<li>Troubles de l\u2019audition</li>\n<li>Maladies cardiovasculaires</li>\n<li>Troubles d\u2019apprentissage</li>\n<li>Migraines</li>\n<li>Trouble obsessionnel-compulsif</li>\n</ul>\n<h2><strong>Soyez du nombre \u2013 d\u00e9clarez votre incapacit\u00e9!</strong></h2>\n<p>Vos renseignements d\u2019auto-identification nous permettent de comprendre et de d\u00e9terminer la meilleure fa\u00e7on d\u2019aider les employ\u00e9s d\u2019AAC qui ont une incapacit\u00e9 visible ou invisible. Plus vous fournissez d\u2019information, plus nous sommes en mesure d\u2019am\u00e9liorer votre exp\u00e9rience en milieu de travail.</p>\n<p>Si vous avez l\u2019impression que vous \u00eates d\u00e9savantag\u00e9 au travail ou que vous pourriez l\u2019\u00eatre en raison d\u2019une incapacit\u00e9 (m\u00eame si des mesures d\u2019adaptation ont \u00e9t\u00e9 prises), n\u2019h\u00e9sitez pas \u00e0 d\u00e9clarer celle-ci. Vous pouvez le faire facilement et en toute confidentialit\u00e9 d\u2019une des deux fa\u00e7ons suivantes :</p>\n<ul>\n<li>En entrant l\u2019information dans <a href=\"https://psoft.agr.gc.ca/psp/peoplesoft8/?cmd=login&amp;languageCd=ENG\">PeopleSoft</a> sous Libre-service &gt; Donn\u00e9es personnelles &gt; \u00c9quit\u00e9 d\u2019emploi;</li>\n<li>En remplissant le <a href=\"https://forms-formulaires.agr.gc.ca/pdf/A6197-F.pdf\">formulaire d'auto-identification</a>\u00a0(PDF).</li>\n</ul>\n<p>Si vous avez des questions ou avez besoin d\u2019aide pour remplir le formulaire d\u2019auto-identification, visitez <a href=\"http://intranet.agr.gc.ca/agrisource/fra/ressources-humaines/equite-en-matiere-demploi-et-inclusivite?id=1288034634018#b\">AgriSource</a> ou communiquez avec l\u2019\u00c9quipe minist\u00e9rielle charg\u00e9e de la diversit\u00e9 \u00e0 <a href=\"mailto:aafc.inclusive-inclusif.aac@canada.ca\">aafc.inclusive-inclusif.aac@canada.ca</a>.</p>\n<p>Pour en savoir plus sur le R\u00e9seau des personnes handicap\u00e9es et ses activit\u00e9s, consultez le <a href=\"https://collab.agr.gc.ca/co/pdn-rph/SitePages/Home.aspx\">site du RPH sur l'Espace de travail du savoir.</a></p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/GrowDiversityInclusion-FR2.png\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Reminder - Defining Disabilities in the Workplace - A Lot Broader Than You Think!",
        "fr": "Rappel - La d\u00e9finition d\u2019incapacit\u00e9 en milieu de travail est bien plus large que vous ne pourriez..."
    },
    "news": {
        "date_posted": {
            "en": "2019-12-05",
            "fr": "2019-12-05"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Human Resources, Corporate Management Branch",
            "fr": "Ressources humaines, Direction g\u00e9n\u00e9rale de la gestion int\u00e9gr\u00e9e"
        }
    }
}