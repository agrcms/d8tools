{
    "dcr_id": "1574174407671",
    "lang": "en",
    "title": {
        "en": "Translate Data Into Business Value with AAFC's New Data Strategy and Workshops",
        "fr": "Transformer les donn\u00e9es en valeur op\u00e9rationnelle gr\u00e2ce \u00e0 la nouvelle strat\u00e9gie d'AAC en mati\u00e8re de donn\u00e9es et aux ateliers connexes"
    },
    "modified": "2019-11-19 00:00:00.0",
    "issued": "2019-11-19 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-11-19",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-11-19",
            "fr": "2019-11-19"
        },
        "modified": {
            "en": "2019-11-19",
            "fr": "2019-11-21"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Translate Data Into Business Value with AAFC's New Data Strategy and Workshops",
            "fr": "Transformer les donn\u00e9es en valeur op\u00e9rationnelle gr\u00e2ce \u00e0 la nouvelle strat\u00e9gie d'AAC en mati\u00e8re de donn\u00e9es et aux ateliers connexes"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "The volume of data that organizations and people produce is growing exponentially, spurred on by the proliferation of digital technology.",
            "fr": "Le volume de donn\u00e9es que les organisations et les gens produisent cro\u00eet de fa\u00e7on exponentielle, stimul\u00e9 par la prolif\u00e9ration de la technologie num\u00e9rique."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>The volume of data that organizations and people produce is growing exponentially, spurred on by the proliferation of digital technology. To keep up with the expectations of an increasingly digitally-oriented population, governments need to evolve.</p>\n<p>For the Government of Canada (GC), rising to this challenge includes actions like developing the new <a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=32603\">Policy on Service and Digital</a> and a <a href=\"https://collab.agr.gc.ca/co/ddph-pddp/Documents/GC%20Data%20Strategy%20Roadmap%20(EN).pdf\">Government of Canada Data Strategy Roadmap</a>\u00a0(PDF).</p>\n<p>For AAFC, this means translating the GC direction into a working reality. It means learning how to use data to build better programs and policies. Much like streaming service providers use your viewing habits to make content recommendations, we need to use data to make decisions that better support our clients.</p>\n<p>How do we do this? By building our capacity. We already collect, use and create immense amounts of data. And now we have the new AAFC <a href=\"https://collab.agr.gc.ca/co/ddph-pddp/Documents/AAFC%20Data%20Strategy.pptx\">Data Strategy</a> (PPT) in place. The strategy highlights the next steps we\u2019ll take:</p>\n<ul>\n<li>Make sure everyone understands data and how we should use it. We\u2019ll also be supporting data literacy for employees, so that everyone feels comfortable using data effectively.</li>\n<li>Secure the tools required to use and analyze data;</li>\n<li>Clarify what we can and can\u2019t do with data.</li>\n<li>Encourage a decision-making process that incorporates data from the beginning.</li>\n</ul>\n<p>To help you understand exactly how the Data Strategy can inform your work, we\u2019ve organized a series of workshops. We encourage you to attend these sessions, learn more about data analytics tools available to you, and get a glimpse into the diverse data assets the Department has at its fingertips.</p>\n<h2><strong>Upcoming sessions open to\u00a0all employees</strong></h2>\n<p>Please note that seating is limited and will be first come first served. Videoconferencing will also be available with details posted on <a href=\"https://collab.agr.gc.ca/co/ddph-pddp/SitePages/Data%20Week.aspx\">Knowledge Workspace</a>.</p>\n<table cellpadding=\"5\" border=\"1\" width=\"900\">\n<tbody>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p><strong>Date</strong></p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p><strong>Time</strong></p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p><strong>Location</strong></p>\n</td>\n<td style=\"width: 57pt;\">\n<p><strong>Language</strong></p>\n</td>\n<td style=\"width: 281.25pt;\">\n<p><strong>Presentation</strong></p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>Nov 25</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>1-3 p.m. (ET)</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>T6 cafeteria</p>\n</td>\n<td style=\"width: 57pt;\">\n<p>EN/FR</p>\n</td>\n<td style=\"width: 281.25pt;\">\n<p>Kara Beckles: Government Work in the Age of AI</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>Nov 26</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>9:30-11 a.m. (ET)</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>T5-4-211</p>\n</td>\n<td style=\"width: 57pt;\">\n<p>EN/FR</p>\n</td>\n<td style=\"width: 281.25pt;\">\n<p>Etienne Lord: Diving into Big Data and Deep Learning: What Should We Know?</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>Nov 27</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>11a.m.-12 p.m. (ET)</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>T5-4-211</p>\n</td>\n<td style=\"width: 57pt;\">\n<p>English</p>\n</td>\n<td style=\"width: 281.25pt;\">\n<p>Paul Guerra &amp; Joshua Tippins: Open Data/Open Government: What Can We Do with It?</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>Nov 27</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>1:30-3 p.m. (ET)</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>T5-3-251</p>\n</td>\n<td style=\"width: 57pt;\">\n<p>English</p>\n</td>\n<td style=\"width: 281.25pt;\">\n<p>James Ashton: Learn About AAFC\u2019s Wealth of Geospatial Data</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>Nov 28</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>10-11 a.m. (ET)</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>T5-4-211</p>\n</td>\n<td style=\"width: 57pt;\">\n<p>English</p>\n</td>\n<td style=\"width: 281.25pt;\">\n<p>Angela Chen: STATCAN\u2019s Modernization Efforts and Its Long-standing Relationship with AAFC</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>*Nov 28</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>12:30-1:30 p.m. (ET)</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>by registration*</p>\n</td>\n<td style=\"width: 57pt;\">\n<p>English</p>\n</td>\n<td style=\"width: 281.25pt;\">\n<p>Chris Thompson: Put the \u201cPower\u201d in Power BI by Doing It Yourself</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>Nov 28</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>1:30-2:30 p.m. (ET)</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>T7-6-211</p>\n</td>\n<td style=\"width: 57pt;\">\n<p>English</p>\n</td>\n<td style=\"width: 281.25pt;\">\n<p>Cameron MacDonald: Data and Digital Government: A View from CBSA</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>Nov 29</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>10-11 a.m. (ET)</p>\n</td>\n<td style=\"width: 79.5pt;\">\n<p>T5-4-211</p>\n</td>\n<td style=\"width: 57pt;\">\n<p>English</p>\n</td>\n<td style=\"width: 281.25pt;\">\n<p>Cathy Istead: AAFC\u2019s Data-Driven Animal Market Information Program</p>\n</td>\n</tr>\n</tbody>\n</table>\n<p>*To register, please contact Bonnie Wang at <a href=\"mailto:bonnie.wang@canada.ca\">bonnie.wang@canada.ca</a> by November 27.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/DataStrategyGraphic.png\" class=\"center-block\"></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Le volume de donn\u00e9es que les organisations et les gens produisent cro\u00eet de fa\u00e7on exponentielle, stimul\u00e9 par la prolif\u00e9ration de la technologie num\u00e9rique. Pour r\u00e9pondre aux attentes d\u2019une population de plus en plus tourn\u00e9e vers le num\u00e9rique, les gouvernements doivent \u00e9voluer.</p>\n<p>Pour le gouvernement du Canada (GC), relever ce d\u00e9fi comprend des mesures comme l\u2019\u00e9laboration de la nouvelle <a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=32603\">Politique sur les services et le num\u00e9rique</a> et d\u2019une <a href=\"https://www.canada.ca/fr/conseil-prive/organisation/greffier/publications/strategie-donnees.html\">feuille de route de la Strat\u00e9gie du gouvernement du Canada en mati\u00e8re de donn\u00e9es</a>.</p>\n<p>Pour AAC, cela signifie transformer l\u2019orientation du GC en une r\u00e9alit\u00e9 op\u00e9rationnelle. Cela signifie apprendre \u00e0 utiliser les donn\u00e9es pour \u00e9laborer de meilleurs programmes et politiques. Tout comme les fournisseurs de services de diffusion utilisent vos habitudes de visionnage pour faire des recommandations de contenu, nous devons utiliser les donn\u00e9es pour prendre des d\u00e9cisions qui nous permettent de mieux appuyer nos clients.</p>\n<p>Comment proc\u00e8de-t-on? En renfor\u00e7ant nos capacit\u00e9s. Nous recueillons, utilisons et produisons d\u00e9j\u00e0 d\u2019\u00e9normes quantit\u00e9s de donn\u00e9es. Et nous avons maintenant mis en place la nouvelle <a href=\"https://collab.agr.gc.ca/co/ddph-pddp/Documents/Strategie%20de%20donnees%20AAC.pptx\">strat\u00e9gie d\u2019AAC en mati\u00e8re de donn\u00e9es</a>\u00a0(PPT). La Strat\u00e9gie met en lumi\u00e8re les prochaines \u00e9tapes que nous prendrons :</p>\n<ul>\n<li>s\u2019assurer que tout le monde comprend les donn\u00e9es et la fa\u00e7on dont nous devrions les utiliser. Appuyer \u00e9galement la culture des donn\u00e9es pour les employ\u00e9s, afin que chacun se sente \u00e0 l\u2019aise d\u2019utiliser les donn\u00e9es efficacement;</li>\n<li>se munir des outils n\u00e9cessaires \u00e0 l\u2019utilisation et \u00e0 l\u2019analyse des donn\u00e9es;</li>\n<li>clarifier ce que nous pouvons et ne pouvons pas faire avec les donn\u00e9es;</li>\n<li>encourager un processus d\u00e9cisionnel qui int\u00e8gre les donn\u00e9es d\u00e8s le d\u00e9but.</li>\n</ul>\n<p>Pour vous aider \u00e0 comprendre exactement comment la Strat\u00e9gie sur les donn\u00e9es peut \u00e9clairer votre travail, nous avons pr\u00e9par\u00e9 une s\u00e9rie d\u2019ateliers. Nous vous encourageons \u00e0 assister \u00e0 ces s\u00e9ances, \u00e0 en apprendre davantage sur les outils d\u2019analyse de donn\u00e9es mis \u00e0 votre disposition et \u00e0 avoir un aper\u00e7u des divers actifs de donn\u00e9es que le Minist\u00e8re a \u00e0 port\u00e9e de main.</p>\n<h2><strong>Prochaines s\u00e9ances ouvertes \u00e0 tous les employ\u00e9s</strong></h2>\n<p>Veuillez noter que le nombre de places est limit\u00e9; le principe du premier arriv\u00e9, premier servi sera appliqu\u00e9. La vid\u00e9oconf\u00e9rence sera \u00e9galement offerte, et les d\u00e9tails seront affich\u00e9s sur l\u2019<a href=\"https://collab.agr.gc.ca/co/ddph-pddp/SitePages/Data%20Week.aspx\">Espace de travail du savoir</a>.</p>\n<table cellpadding=\"5\" border=\"1\" width=\"900\">\n<tbody>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p><strong>Date</strong></p>\n</td>\n<td style=\"width: 95.85pt;\">\n<p><strong>Temps</strong></p>\n</td>\n<td style=\"width: 69.35pt;\">\n<p><strong>Lieu</strong></p>\n</td>\n<td style=\"width: 55.2pt;\">\n<p><strong>Langue</strong></p>\n</td>\n<td style=\"width: 276.85pt;\">\n<p><strong>Pr\u00e9sentation</strong></p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>25 novembre</p>\n</td>\n<td style=\"width: 95.85pt;\">\n<p>13 h - 15 h (HE)</p>\n</td>\n<td style=\"width: 69.35pt;\">\n<p>Caf\u00e9t\u00e9ria T6</p>\n</td>\n<td style=\"width: 55.2pt;\">\n<p>Fran\u00e7ais/ anglais</p>\n</td>\n<td style=\"width: 276.85pt;\">\n<p>Kara Beckles : Le travail au gouvernement \u00e0 l\u2019\u00e8re de l\u2019IA</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>26 novembre</p>\n</td>\n<td style=\"width: 95.85pt;\">\n<p>9 h 30 - 11 h (HE)</p>\n</td>\n<td style=\"width: 69.35pt;\">\n<p>T5-4-211</p>\n</td>\n<td style=\"width: 55.2pt;\">\n<p>Fran\u00e7ais/ anglais</p>\n</td>\n<td style=\"width: 276.85pt;\">\n<p>Etienne Lord : Exploration des m\u00e9gadonn\u00e9es et de l\u2019apprentissage profound : que devons-nous savoir?</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>27 novembre</p>\n</td>\n<td style=\"width: 95.85pt;\">\n<p>11 h - 12 h (HE)</p>\n</td>\n<td style=\"width: 69.35pt;\">\n<p>T5-4-211</p>\n</td>\n<td style=\"width: 55.2pt;\">\n<p>Anglais</p>\n</td>\n<td style=\"width: 276.85pt;\">\n<p>Paul Guerra et Joshua Tippins : Donn\u00e9es ouvertes#gouvernement ouvert : Que pouvons-nous en faire?</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>27 novembre</p>\n</td>\n<td style=\"width: 95.85pt;\">\n<p>13 h 30 - 15 h (HE)</p>\n</td>\n<td style=\"width: 69.35pt;\">\n<p>T5-3-251</p>\n</td>\n<td style=\"width: 55.2pt;\">\n<p>Anglais</p>\n</td>\n<td style=\"width: 276.85pt;\">\n<p>James Ashton : D\u00e9couvrez la mine de donn\u00e9es g\u00e9ospatiales d\u2019AAC</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>28 novembre</p>\n</td>\n<td style=\"width: 95.85pt;\">\n<p>10 h - 11 h (HE)</p>\n</td>\n<td style=\"width: 69.35pt;\">\n<p>T5-4-211</p>\n</td>\n<td style=\"width: 55.2pt;\">\n<p>Anglais</p>\n</td>\n<td style=\"width: 276.85pt;\">\n<p>Angela Chen : Les efforts de modernisation de STATCAN et sa relation de longue date avec AAC</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>28 novembre*</p>\n</td>\n<td style=\"width: 95.85pt;\">\n<p>12 h 30 -13 h 30 (HE)</p>\n</td>\n<td style=\"width: 69.35pt;\">\n<p>Inscription requise*</p>\n</td>\n<td style=\"width: 55.2pt;\">\n<p>Anglais</p>\n</td>\n<td style=\"width: 276.85pt;\">\n<p>Chris Thompson : Mettez vous-m\u00eame le pouvoir dans Power BI</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>28 novembre</p>\n</td>\n<td style=\"width: 95.85pt;\">\n<p>13 h 30 -14 h 30 (HE)</p>\n</td>\n<td style=\"width: 69.35pt;\">\n<p>T7-6-211</p>\n</td>\n<td style=\"width: 55.2pt;\">\n<p>Anglais</p>\n</td>\n<td style=\"width: 276.85pt;\">\n<p>Cameron MacDonald : Donn\u00e9es et gouvernement num\u00e9rique: une vue de I\u2019ASFC</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 51.75pt;\">\n<p>29 novembre</p>\n</td>\n<td style=\"width: 95.85pt;\">\n<p>10 h - 11 h (HE)</p>\n</td>\n<td style=\"width: 69.35pt;\">\n<p>T5-4-211</p>\n</td>\n<td style=\"width: 55.2pt;\">\n<p>Anglais</p>\n</td>\n<td style=\"width: 276.85pt;\">\n<p>Cathy Istead : Programme d\u2019information sur le march\u00e9 des productions animals d\u2019AAC, ax\u00e9 sur les donn\u00e9es</p>\n</td>\n</tr>\n</tbody>\n</table>\n<p>*Pour vous inscrire, veuillez communiquer avec Bonnie Wang \u00e0 <a href=\"mailto:bonnie.wang@canada.ca\">bonnie.wang@canada.ca</a> d\u2019ici le 27 novembre.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/DataStrategyGraphic.png\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Translate Data Into Business Value with AAFC's New Data Strategy and Workshops",
        "fr": "Transformer les donn\u00e9es en valeur op\u00e9rationnelle gr\u00e2ce \u00e0 la nouvelle strat\u00e9gie dAAC en mati\u00e8re de..."
    },
    "news": {
        "date_posted": {
            "en": "2019-11-19",
            "fr": "2019-11-19"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Data Development and Partnership, Strategic Policy Branch",
            "fr": "D\u00e9veloppement et partenariats de donn\u00e9es, Direction g\u00e9n\u00e9rale des politiques strat\u00e9giques"
        }
    }
}