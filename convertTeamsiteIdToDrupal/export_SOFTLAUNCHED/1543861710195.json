{
    "dcr_id": "1543861710195",
    "lang": "en",
    "title": {
        "en": "Reminder - Securing Belongings During the Holidays",
        "fr": "Rappel - Rangement des effets personnels pendant la p\u00e9riode des F\u00eates"
    },
    "modified": "2018-12-19 00:00:00.0",
    "issued": "2018-12-13 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2018-12-05",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-12-13",
            "fr": "2018-12-13"
        },
        "modified": {
            "en": "2018-12-19",
            "fr": "2018-12-19"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Reminder - Securing Belongings During the Holidays",
            "fr": "Rappel - Rangement des effets personnels pendant la p\u00e9riode des F\u00eates"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "The holiday season is fast approaching, and while we busily prepare for this time of year, buying gifts for our loved ones and friends, planning family events and trips, or taking time away from work to enjoy the season, others may have less charitable intentions.",
            "fr": "Le temps des F\u00eates approche \u00e0 grands pas. Tandis que nous nous pr\u00e9parons f\u00e9brilement pour cette p\u00e9riode en achetant des cadeaux pour nos proches et amis, en planifiant des activit\u00e9s et des voyages en famille ou en prenant cong\u00e9 du travail, d'autres personnes peuvent avoir des intentions moins charitables."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>The holiday season is fast approaching, and while we busily prepare for this time of year, buying gifts for our loved ones and friends, planning family events and trips, or taking time away from work to enjoy the season, others may have less charitable intentions.\n</p>\n<p>While shopping, always secure your belongings in the trunk of your locked vehicle and park in well-lit areas. If your vehicle does not have an enclosed trunk, take a blanket, which can be used to cover valuables and is also good to have in the event of a roadside emergency.</p>\n<p>With the reduced daylight at this time of year, be vigilant when going to and from your car: be sure to locate your keys prior to going to your car, especially if you are carrying laptops, gifts or other items of value. Always keep your purse or wallet secure: whenever possible, keep cash and credit cards on your person.</p>\n<h2><strong>Important office security tips for the holiday season</strong></h2>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/SecuringBelongings2018.png\" class=\"pull-right mrgn-lft-md\" alt=\"\">Always leave your desk clean and tidy when out of the office. Be sure to properly secure all documents out of sight in approved cabinets and cupboards, along with attractive handheld assets such as smartphones and any other electronic devices.</p>\n<p>Even in the office environment, it is wise to keep all your personal items such as cell phones, purses, wallets and jackets safe, secure and out of plain sight where possible.</p>\n<p>Remember to lock your computer when you are away from your desk.</p>\n<p>Be sure to inform your colleagues if you are going to be on leave and ask them not to leave any secure documents or items of value on your desk while you are away. An <a target=\"_blank\" href=\"http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101150426&amp;lang=eng\">Away from the Office tent card</a> should be placed on your desk informing your colleagues of your absence.<br>\n<br>\nImmediately report any suspicious individual found wandering around your office space\u00a0who does not have an AAFC ID Card to <a href=\"http://directinfo.agr.gc.ca/directInfo/eng/index.php?fuseaction=agriInfo.orgUnit&amp;dn=OU=SS,OU=FMSSD-DGISS,OU=CMB-DGGI,OU=AAFC-AAC,o=gc,c=ca\">Departmental Security Services</a>.</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Le temps des F\u00eates approche \u00e0 grands pas. Tandis que nous nous pr\u00e9parons f\u00e9brilement pour cette p\u00e9riode en achetant des cadeaux pour nos proches et amis, en planifiant des activit\u00e9s et des voyages en famille ou en prenant cong\u00e9 du travail, d\u2019autres personnes peuvent avoir des intentions moins charitables.</p>\n<p>Lorsque vous magasinez, prenez l\u2019habitude de placer vos effets personnels dans le coffre de votre v\u00e9hicule verrouill\u00e9 et de stationner dans un endroit bien \u00e9clair\u00e9. Si le coffre de votre v\u00e9hicule n\u2019est pas cloisonn\u00e9, utilisez une couverture pour couvrir vos articles de valeur. Celle-ci pourrait aussi vous \u00eatre utile en cas d\u2019urgence sur la route.</p>\n<p>Compte tenu des heures r\u00e9duites de clart\u00e9 en cette p\u00e9riode de l\u2019ann\u00e9e, soyez vigilants lorsque vous vous rendez \u00e0 votre v\u00e9hicule ou lorsque vous le quittez. Assurez-vous d\u2019avoir vos cl\u00e9s avec vous avant de vous rendre \u00e0 votre v\u00e9hicule, surtout si vous transportez un ordinateur portable, des cadeaux ou d\u2019autres articles de valeur. Gardez toujours votre sac \u00e0 main ou votre porte-monnaie en lieu s\u00fbr et ayez votre argent et vos cartes de cr\u00e9dit sur vous dans la mesure du possible.</p>\n<h2><strong>Conseils importants pour la s\u00e9curit\u00e9 au bureau pendant la p\u00e9riode des F\u00eates</strong></h2>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/SecuringBelongings2018.png\" class=\"pull-right mrgn-lft-md\">Laissez toujours votre bureau propre et rang\u00e9 lorsque vous vous absentez. Mettez tous les documents en s\u00e9curit\u00e9 dans des meubles-classeurs et des armoires approuv\u00e9s avec les articles portatifs convoit\u00e9s comme les t\u00e9l\u00e9phones intelligents et les autres outils \u00e9lectroniques.</p>\n<p>M\u00eame au travail, il est pr\u00e9f\u00e9rable de mettre tous vos articles personnels comme les t\u00e9l\u00e9phones cellulaires, les sacs \u00e0 main, les porte-monnaie et les manteaux en lieu s\u00fbr et hors de la vue des passants dans la mesure du possible.</p>\n<p>N\u2019oubliez pas de verrouiller votre ordinateur lorsque vous quittez votre bureau.</p>\n<p>Assurez-vous d\u2019informer vos coll\u00e8gues si vous pr\u00e9voyez vous absenter du travail et demandez-leur de ne laisser aucun document prot\u00e9g\u00e9 ni aucun article de valeur sur votre bureau pendant votre absence. Il est recommand\u00e9 de placer une <a target=\"_blank\" href=\"http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101150426&amp;lang=fra\">carte-tente \u00ab titulaire absent \u00bb</a> sur votre bureau pour mettre vos coll\u00e8gues au courant de votre absence.</p>\n<p>Avisez imm\u00e9diatement les <a href=\"http://directinfo.agr.gc.ca/directInfo/fra/index.php?fuseaction=agriInfo.orgUnit&amp;dn=OU=SS,OU=FMSSD-DGISS,OU=CMB-DGGI,OU=AAFC-AAC,o=gc,c=ca\">Services de s\u00e9curit\u00e9 minist\u00e9riels</a> si vous voyez un individu suspect sans carte d\u2019identit\u00e9 d\u2019AAC dans les lieux de travail.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Reminder - Securing Belongings During the Holidays",
        "fr": "Rappel - Rangement des effets personnels pendant la p\u00e9riode des F\u00eates"
    },
    "news": {
        "date_posted": {
            "en": "2018-12-13",
            "fr": "2018-12-13"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Departmental Security Services, Corporate Management Branch",
            "fr": "Services de s\u00e9curit\u00e9 minist\u00e9riels, Direction g\u00e9n\u00e9rale de la gestion int\u00e9gr\u00e9e"
        }
    }
}