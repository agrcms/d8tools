{
    "dcr_id": "1536601024466",
    "lang": "en",
    "title": {
        "en": "Reminder - Presentation - Indigenous Agriculture and Settler Colonialism in the Prairies: Reflections on Lost Harvests - September 28",
        "fr": "Rappel - Pr\u00e9sentation - \u00ab Indigenous Agriculture and Settler Colonialism in the Prairies: Reflections on Lost Harvests \u00bb - 28 septembre"
    },
    "modified": "2018-09-26 00:00:00.0",
    "issued": "2018-09-13 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2018-09-10",
    "breadcrumb_label": "Reminder - Presentation - Indigenous Agriculture and Settler Colonialism in the Prairies...",
    "meta": {
        "issued": {
            "en": "2018-09-13",
            "fr": "2018-09-13"
        },
        "modified": {
            "en": "2018-09-26",
            "fr": "2018-09-19"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Reminder - Presentation - Indigenous Agriculture and Settler Colonialism in the Prairies: Reflections on Lost Harvests - September 28",
            "fr": "Rappel - Pr\u00e9sentation - \u00ab Indigenous Agriculture and Settler Colonialism in the Prairies: Reflections on Lost Harvests \u00bb - 28 septembre"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "The Indigenous Agriculture Working Group invites you to a presentation by Sarah Carter (University of Alberta) on Indigenous Agriculture and Settler Colonialism in the Prairies on September 28 at the Guelph Regional Office.",
            "fr": "Le Groupe de travail sur l'agriculture autochtone vous invite \u00e0 venir \u00e9couter Sarah Carter (Universit\u00e9 de l'Alberta) parler de l'agriculture autochtone et du colonialisme invasif dans les Prairies le 28 septembre prochain au bureau r\u00e9gional de Guelph."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>The Indigenous Agriculture Working Group invites you to a presentation by Sarah Carter (University of Alberta) on Indigenous Agriculture and Settler Colonialism in the Prairies on September 28 at the Guelph Regional Office. Her presentation will be a reflection on her book, <i>Lost Harvests</i>, which examined the effects of government policy on Indigenous agriculture in the prairies. She will also provide an overview of the research she has undertaken since publishing this book in 1990, bringing history into the present era of reconciliation. The presentation will be followed by a 30-minute moderated question and answer period. To ensure that as many questions are answered as possible, videoconference participants are asked to email Jill Hull (<a href=\"mailto:Jill.Hull@AGR.GC.CA\">Jill.Hull@AGR.GC.CA</a>).</p>\n<p>To learn more visit the\u00a0<a href=\"https://collab.agr.gc.ca/co/acn-crea/SitePages/Home.aspx\">Indigenous Network Circle Knowledge Workspace page</a>.</p>\n<h2><strong>Event information</strong></h2>\n<p><strong>Date and Time:</strong> September 28 |\u00a01:00\u00a0to 2:45 p.m. (ET)<br><strong>Language:</strong> English with a bilingual summary at the end<br><strong>Location:</strong> Boardroom A, 174 Stone Road W, Guelph / Ottawa - NHCAP T3-1-351, T5-6-247, and T3-1-118; Available across Canada via videoconference<br><strong>Videoconference:</strong> 6511001029</p>\n<p><strong>Available seating will be on a first come first serve basis.</strong></p>\n<p>To maximize AAFC\u2019s videoconferencing capacity, employees are encouraged to gather with colleagues where possible to make use of boardrooms with videoconferencing facilities. Centres and teams are asked to please coordinate room bookings.</p>\n<p>If you experience difficulties in connecting, please contact the IT Centre TI at 1-855-545-4411 or <a href=\"mailto:service@itcentreti.collaboration.gc.ca\">service@itcentreti.collaboration.gc.ca</a>. Please note that to avoid accidental noise, all lines will be muted.</p>\n<h2><strong>Speaker biography</strong></h2>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/SarahCarter2.png\" class=\"pull-left mrgn-rght-md\" alt=\"Sarah Carter\">Originally from Saskatoon, Sarah Carter is the author of <i>Lost Harvests: Prairie Indian Reserve Farmers and Government Policy</i> (1990), which focuses on Indigenous agriculture in the Prairies and government policy over the last century. She is also a professor and Henry Marshall Tory Chair in the Department of History and Classics, and a member of the Faculty of Native Studies of the University of Alberta. She specializes in the history of settler colonialism in Canada and in comparative colonial and borderlands perspectives. Her most recent book <i>Imperial Plots: Women, Land, and the Spadework of British Colonialism on the Canadian Prairies</i> (2016) has won several awards including the Governor General\u2019s History Award for Scholarly Research and the Sir John A. Macdonald Prize of the Canadian Historical Association (<abbr title=\"Canadian Historical Association\">CHA</abbr>).</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Le Groupe de travail sur l\u2019agriculture autochtone vous invite \u00e0 venir \u00e9couter Sarah Carter (Universit\u00e9 de l\u2019Alberta) parler de l\u2019agriculture autochtone et du colonialisme invasif dans les Prairies le 28 septembre prochain au bureau r\u00e9gional de Guelph. Sa pr\u00e9sentation portera sur son livre, <i>Lost Harvests</i>, dans lequel elle examine les r\u00e9percussions des politiques gouvernementales sur l\u2019agriculture autochtone dans les Prairies. Elle fera \u00e9galement un tour d\u2019horizon de ses travaux de recherche depuis la publication de son livre en 1990, faisant un rapprochement entre l\u2019histoire et le contexte actuel de r\u00e9conciliation. Une p\u00e9riode de questions de 30 minutes, anim\u00e9e par un mod\u00e9rateur, aura lieu apr\u00e8s la pr\u00e9sentation. Nous demandons aux participants par vid\u00e9oconf\u00e9rence d\u2019envoyer leurs questions par courriel \u00e0 Jill Hull (<a href=\"mailto:jill.hull@agr.gc.ca\">jill.hull@agr.gc.ca</a>). Nous voulons ainsi nous assurer de r\u00e9pondre au plus grand nombre de questions possible.</p>\n<p>Pour de plus amples renseignements, veuillez consulter la page du <a href=\"https://collab.agr.gc.ca/co/acn-crea/SitePages/Home.aspx\">Cercle de r\u00e9seautage des employ\u00e9s autochtones dans l\u2019Espace de travail du savoir</a>.</p>\n<h2><b>Renseignements sur l\u2019activit\u00e9</b></h2>\n<p><strong>Date et heure\u00a0:</strong> 28\u00a0septembre | 13\u00a0h\u00a0\u00e0 14\u00a0h\u00a045 (HE)<br><strong>Langue\u00a0:</strong> anglais, suivi d\u2019un r\u00e9sum\u00e9 bilingue<br><strong>Lieu\u00a0:</strong> salle de conf\u00e9rence\u00a0A, 174 Stone Road W, Guelph / Ottawa \u2013 CACPA, T3-1-351, T5-6-247 et T3\u20111\u2011118 ; Diffus\u00e9 partout au Canada par vid\u00e9oconf\u00e9rence<br><strong>Vid\u00e9oconf\u00e9rence\u00a0:</strong> 6511001029 </p>\n<p>\n<strong>Les places seront accord\u00e9es selon le principe du premier arriv\u00e9, premier servi.</strong> </p>\n<p>\nPour maximiser la capacit\u00e9 d\u2019AAC d\u2019offrir des vid\u00e9oconf\u00e9rences, nous invitons les employ\u00e9s \u00e0 se r\u00e9unir dans les salles de vid\u00e9oconf\u00e9rence pour assister \u00e0 la pr\u00e9sentation. Nous demandons aux centres de recherche et aux \u00e9quipes de coordonner la r\u00e9servation des salles.</p>\n<p>\nSi vous ne r\u00e9ussissez pas \u00e0 vous connecter, veuillez communiquer avec le IT\u00a0Centre TI au 1\u2011855\u2011545\u20114411 ou au <a href=\"mailto:service@itcentreti.collaboration.gc.ca\">service@itcentreti.collaboration.gc.ca</a>.</p>\n<p>\nVeuillez noter que toutes les lignes seront mises en sourdine pour \u00e9viter les bruits accidentels. </p>\n<h2><b>Biographie de la conf\u00e9renci\u00e8re</b></h2>\n<p>\n<img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/SarahCarter2.png\" class=\"pull-left mrgn-rght-md\" alt=\"Sarah Carter\">Originaire de Saskatoon, Sarah\u00a0Carter est l\u2019auteur de <i>Lost Harvests: Prairie Indian Reserve Farmers and Government Policy </i>(1990), un ouvrage sur l\u2019agriculture autochtone dans les Prairies et les politiques gouvernementales au cours du dernier si\u00e8cle. Elle est \u00e9galement professeur et titulaire de la chaire Henry\u00a0Marshall Tory au d\u00e9partement d\u2019histoire et de lettres classiques et membre de la facult\u00e9 des \u00e9tudes autochtones de l\u2019Universit\u00e9 de l\u2019Alberta. Elle est sp\u00e9cialiste de l\u2019histoire du colonialisme invasif au Canada et des perspectives comparatives du colonialisme et des fronti\u00e8res. Son dernier livre, <i>Imperial Plots: Women, Land, and the Spadework of British Colonialism on the Canadian Prairies</i> (2016), a remport\u00e9 plusieurs prix, dont le Prix d\u2019histoire du Gouverneur g\u00e9n\u00e9ral pour la recherche savante et le prix Sir-John-A.-Macdonald de la Soci\u00e9t\u00e9 historique du Canada (<abbr title=\"Soci\u00e9t\u00e9 historique du Canada\">SHC</abbr>). </p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Reminder - Presentation - Indigenous Agriculture and Settler Colonialism in the Prairies...",
        "fr": "Rappel - Pr\u00e9sentation - \u00ab Indigenous Agriculture and Settler Colonialism in the Prairies..."
    },
    "news": {
        "date_posted": {
            "en": "2018-09-13",
            "fr": "2018-09-13"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Indigenous Agriculture Working Group",
            "fr": "Groupe de travail sur l'agriculture autochtone"
        }
    }
}