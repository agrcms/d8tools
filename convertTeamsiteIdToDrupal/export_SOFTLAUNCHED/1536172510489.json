{
    "dcr_id": "1536172510489",
    "lang": "en",
    "title": {
        "en": "People Strategy",
        "fr": "Strat\u00e9gie de gestion des personnes"
    },
    "modified": "2019-09-03 00:00:00.0",
    "issued": "2018-09-06 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1536243068217",
    "layout_name": "1 column",
    "dc_date_created": "2018-09-05",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-09-06",
            "fr": "2018-09-06"
        },
        "modified": {
            "en": "2019-09-03",
            "fr": "2019-09-03"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "People Strategy",
            "fr": "Strat\u00e9gie de gestion des personnes"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Agriculture and Agri-Food Canada\u2019s <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/pses/pses_people_strategy_july_2018-eng.pdf\">People Strategy (PDF)</a> outlines our people management responsibilities for employees and managers alike. Working in tandem with employee feedback from a variety of sources, such as the Public Service Employee Survey, informal discussions, and team meetings, the People Strategy guides the people management actions to be undertaken by each branch. These actions help us build on progress we have made to date and inform future efforts to get where we want to go as an organization.</p>\n\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/images/pses/pses_people_strategy-eng.jpg\" class=\"img-responsive col-md-12\" alt=\"Diagram showing three themes arranged in a circle. They are: workforce, culture, and work environment. All three point to a central point which reads: AAFC as an employer of choice; Respect for people, Integrity, Excellence.\"></p>\n\n<h2>Workforce</h2>\n\n<table class=\"table table-bordered table-responsive\"><caption class=\"text-left\">Objective: Employees are given opportunities and support to thrive. They want to work here  and are proud of the Department and their contributions to it. </caption><thead>\n  <tr class=\"active\">\n <th scope=\"col\" class=\"col-md-2\">Area of Focus</th>\n<th scope=\"col\" class=\"col-md-6\">Goal</th>\n</tr></thead><tbody>\n  <tr>\n <td><b>Plan Ahead</b></td>\n <td>Employees understand current and future organizational needs</td>\n </tr>\n  <tr>\n <td><b>Recruit the Best</b></td>\n <td>Attract talent and specialized knowledge (both new recruits and mid-career employees)</td>\n </tr>\n  <tr>\n <td><b>Develop Talent</b></td>\n <td>Everyone has opportunities to grow professionally through continuous learning and career development</td>\n </tr>\n</tbody></table>\n\n<h2>Work Environment</h2>\n\n<table class=\"table table-bordered table-responsive\"><caption class=\"text-left\">Objective: AAFC is a safe, diverse and supportive place to work, where employees are  healthy, engaged and productive.</caption><thead>\n  <tr class=\"active\">\n <th scope=\"col\" class=\"col-md-2\">Area of Focus</th>\n<th scope=\"col\" class=\"col-md-6\">Goal</th>\n</tr></thead><tbody>\n  <tr>\n <td><b>Healthy and Safe</b></td>\n <td>Provide a safe physical environment and promote employee well-being</td>\n </tr>\n  <tr>\n <td><b>Inclusive</b></td>\n <td>All perspectives and ideas are encouraged, welcomed and valued</td>\n </tr>\n  <tr>\n <td><b>Enabling and Supportive</b></td>\n <td>Employees are equipped to do their jobs and are supported to balance life and work demands</td>\n </tr>\n</tbody></table>\n\n<h2>Culture</h2>\n\n<table class=\"table table-bordered table-responsive\"><caption class=\"text-left\">Objective: Employees are encouraged to innovate and empowered to make a difference.</caption><thead>\n  <tr class=\"active\">\n <th scope=\"col\" class=\"col-md-2\">Area of Focus</th>\n<th scope=\"col\" class=\"col-md-6\">Goal</th>\n</tr></thead><tbody>\n  <tr>\n <td><b>Innovative</b></td>\n <td>Smart risk-taking and room to innovate </td>\n </tr>\n  <tr>\n <td><b>Empowered</b></td>\n <td>Employees have opportunities to influence decision-making and contribute to results </td>\n </tr>\n  <tr>\n <td><b>Continuously Improving</b></td>\n <td>Processes are streamlined and improved to work and service clients efficiently </td>\n </tr>\n  <tr>\n <td><b>Rewarding</b></td>\n <td>Employee achievements, contributions and positive behaviours are recognized </td>\n </tr>\n</tbody></table>\n\n\n\n<h2>Related Information</h2>\n\n<ul>\n      <li><a href=\"?id=1407756839813\">Public Service Employee Surveys</a></li>\n      <li><a href=\"https://www.canada.ca/en/privy-council/corporate/clerk/publications/twenty-fifth-annual-report-prime-minister-public-service.html\">Clerk of the Privy Council\u2019s Annual Report to the Prime Minister on the Public Service</a></li>\n      <li><a href=\"https://www.canada.ca/en/privy-council/topics/blueprint-2020-public-service-renewal.html\">Blueprint 2020 and public service renewal</a></li>\n      <li><a href=\"?id=1510839111270\">Public Service Renewal results for Agriculture and Agri-Food Canada</a></li>\n      <li><a href=\"https://www.canada.ca/en/privy-council/corporate/clerk/publications/safe-workspaces.html\">Safe Workspaces: Starting a dialogue and taking action on harassment in the Public Service</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>La <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/pses/pses_people_strategy_july_2018_v2-fra.pdf\">Strat\u00e9gie de gestion des personnes (PDF)</a> d\u2019Agriculture et Agroalimentaire Canada d\u00e9crit les responsabilit\u00e9s des employ\u00e9s et des gestionnaires en ce qui a trait \u00e0 la gestion des personnes. De concert avec les commentaires des employ\u00e9s provenant de diverses sources, comme le Sondage aupr\u00e8s des fonctionnaires f\u00e9d\u00e9raux, les discussions informelles et les r\u00e9unions d\u2019\u00e9quipe, la Strat\u00e9gie de gestion des personnes oriente les mesures de gestion du personnel que doit prendre chaque direction g\u00e9n\u00e9rale. Ces mesures nous aident \u00e0 tirer parti des progr\u00e8s que nous avons r\u00e9alis\u00e9s jusqu\u2019\u00e0 pr\u00e9sent et \u00e0 \u00e9clairer les efforts futurs pour atteindre nos objectifs en tant qu\u2019organisation.</p> \n\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/images/pses/pses_people_strategy-fra.jpg\" class=\"img-responsive col-md-12\" alt=\"Diagramme montrant trois th\u00e8mes dispos\u00e9s en cercle. Ils sont : effectif, culture, et milieu de travail. Tous les trois indiquent un point central qui d\u00e9clare: AAC en tant qu\u2019employeur de choix; Respect des personnes, Int\u00e9grit\u00e9, Excellence.\"></p>\n\n<h2>Effectif</h2>\n\n<table class=\"table table-bordered table-responsive\"><caption class=\"text-left\">Objectif : Donner aux employ\u00e9s les possibilit\u00e9s et le soutien dont ils ont besoin pour  r\u00e9ussir. Les employ\u00e9s veulent travailler ici et sont fiers de leur Minist\u00e8re et  des contributions qu'ils y apportent.</caption><thead>\n  <tr class=\"active\">\n <th scope=\"col\" class=\"col-md-2\">Domaine d'int\u00e9r\u00eat particulier</th>\n<th scope=\"col\" class=\"col-md-6\">But</th>\n  </tr></thead><tbody>\n  <tr>\n <td><b>Pr\u00e9voir</b></td>\n <td>Comprendre les besoins organisationnels actuels et futurs des employ\u00e9s. </td>\n  </tr>\n  <tr>\n <td><b>Recruter les meilleurs</b></td>\n <td>Attirer des talents et des connaissances sp\u00e9cialis\u00e9es (nouvelles recrues et employ\u00e9s en milieu de carri\u00e8re). </td>\n  </tr>\n  <tr>\n <td><b>Former des talents</b></td>\n <td>Les employ\u00e9s ont la possibilit\u00e9 d'\u00e9voluer par des activit\u00e9s d'apprentissage et de perfectionnement continus. </td>\n  </tr>\n</tbody></table>\n<h2>Milieu de travail</h2>\n\n<table class=\"table table-bordered table-responsive\"><caption class=\"text-left\">Objectif : AAC  est un milieu de travail s\u00fbr, diversifi\u00e9 et positif, o\u00f9 les employ\u00e9s sont  sains, mobilis\u00e9s et productifs.</caption><thead>\n  <tr class=\"active\">\n <th scope=\"col\" class=\"col-md-2\">Domaine d'int\u00e9r\u00eat particulier</th>\n<th scope=\"col\" class=\"col-md-6\">But</th>\n  </tr></thead><tbody>\n  <tr>\n <td><b>Sain et s\u00fbr</b></td>\n <td>Offrir un milieu physique s\u00fbr et promouvoir le bien-\u00eatre des employ\u00e9s. </td>\n  </tr>\n  <tr>\n <td><b>Inclusif</b></td>\n <td>Tous les points de vue et id\u00e9es sont encourag\u00e9s, bienvenus et appr\u00e9ci\u00e9s. </td>\n  </tr>\n  <tr>\n <td><b>Favorable et positif</b></td>\n <td>Les employ\u00e9s ont les outils pour faire leur travail et re\u00e7oivent du soutien en mati\u00e8re d'\u00e9quilibre travail-vie personnelle. </td>\n  </tr>\n</tbody></table>\n<h2>Culture</h2>\n\n<table class=\"table table-bordered table-responsive\"><caption class=\"text-left\">Objectif : Les  employ\u00e9s sont encourag\u00e9s \u00e0 innover et ont les moyens n\u00e9cessaires pour apporter  des changements.</caption><thead>\n  <tr class=\"active\">\n <th scope=\"col\" class=\"col-md-2\">Domaine d'int\u00e9r\u00eat particulier</th>\n<th scope=\"col\" class=\"col-md-6\">But</th>\n  </tr></thead><tbody>\n  <tr>\n <td><b>Innovation</b></td>\n <td>Prise de risques r\u00e9fl\u00e9chie et latitude pour innover.</td>\n  </tr>\n  <tr>\n <td><b>Habilitation</b></td>\n <td>Les employ\u00e9s peuvent influer sur les d\u00e9cisions et contribuer aux r\u00e9sultats.</td>\n  </tr>\n  <tr>\n <td><b>Am\u00e9lioration continue</b></td>\n <td>Les processus sont simplifi\u00e9s et am\u00e9lior\u00e9s pour travailler et servir les clients efficacement.</td>\n  </tr>\n  <tr>\n <td><b>Reconnaisance</b></td>\n <td>Les r\u00e9alisations, les contributions et les comportements positifs des employ\u00e9s sont reconnus. </td>\n  </tr>\n</tbody></table>\n\n<h2>Renseignements connexes</h2>\n\n<ul>\n      <li><a href=\"?id=1407756839813\">Sondage aupr\u00e8s des fonctionnaires f\u00e9d\u00e9raux</a></li>\n      <li><a href=\"https://www.canada.ca/fr/conseil-prive/organisation/greffier/publications/vingt-cinquieme-rapport-annuel-premier-ministre-fonction-publique.html\">Rapport annuel du greffier du Conseil priv\u00e9 au premier ministre sur la fonction publique</a></li>\n      <li><a href=\"https://www.canada.ca/fr/conseil-prive/sujets/objectif-2020-renouvellement-fonction-publique.html\">Objectif 2020 et le renouvellement de la fonction publique</a></li>\n      <li><a href=\"?id=1510839111270\">R\u00e9sultats du renouvellement de la fonction publique d\u2019Agriculture et Agroalimentaire Canada</a></li>\n      <li><a href=\"https://www.canada.ca/fr/conseil-prive/organisation/greffier/publications/milieux-travail-sains.html\">Milieux de travail sains : Entamer un dialogue et prendre des mesures pour lutter contre le harc\u00e8lement dans la fonction publique</a></li>\n</ul>\n\n\t\t"
    },
    "breadcrumb": {
        "en": "People Strategy",
        "fr": "Strat\u00e9gie de gestion des personnes"
    }
}