{
    "dcr_id": "1571841555112",
    "lang": "en",
    "title": {
        "en": "Cyber Security Month 2019: Protect Yourself From Online Dangers",
        "fr": "Mois de la sensibilisation \u00e0 la cybers\u00e9curit\u00e9 2019 : Prot\u00e9gez-vous des dangers en ligne"
    },
    "modified": "2019-10-24 00:00:00.0",
    "issued": "2019-10-24 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-10-23",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-10-24",
            "fr": "2019-10-24"
        },
        "modified": {
            "en": "2019-10-24",
            "fr": "2019-10-24"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Cyber Security Month 2019: Protect Yourself From Online Dangers",
            "fr": "Mois de la sensibilisation \u00e0 la cybers\u00e9curit\u00e9 2019 : Prot\u00e9gez-vous des dangers en ligne"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "October is Cyber Security Awareness Month. This is a designated time of year for Canadians to become more aware of what they should be doing to stay safe online.",
            "fr": "Octobre est le Mois de la sensibilisation \u00e0 la cybers\u00e9curit\u00e9. On encourage les Canadiens \u00e0 en profiter pour se renseigner sur les fa\u00e7ons d\u2019assurer leur s\u00e9curit\u00e9 en ligne."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>October is Cyber Security Awareness Month. This is a designated time of year for Canadians to become more aware of what they should be doing to stay safe online.</p>\n<p>Here are some simple steps you can take to help make sure your online experience is a safe one.</p>\n<ol>\n<li><strong>Practice good password etiquette</strong>. Use unique passwords with a combination of upper and lower case letters and numbers/symbols. Do not share your passwords with anyone.</li>\n<li><strong>Accept updates to your mobile devices, computers, and applications</strong>. Whether on your desktop or mobile, your operating systems should be updated regularly to deal with vulnerabilities to new security threats. Restart your workstation regularly to ensure patching is performed.</li>\n<li><strong>Be on guard for phishing messages and extortion attempts</strong>. Never open emails or attachments from people you don\u2019t know, and never follow any links to Web sites included in these emails. They might infect your computer with a virus or spyware. Delete such emails immediately. If you have opened the email and clicked on the link, notify IT Centre TI. Report any email extortion attempts to security at <a href=\"mailto:aafc.securityincident-incidentsecurite.aac@canada.ca\">aafc.securityincident-incidentsecurite.aac@canada.ca</a>.</li>\n<li><a href=\"https://www.cyber.gc.ca/en/guidance/protect-how-you-connect\"><strong>Understand possible risks</strong></a><strong> before using social media platforms and applications</strong>. Instant messaging applications and social media platforms are not all created equal. In deciding what tools to use, you need to consider both the functionality of the service and how secure, and private your information and activity will be. Read the terms of service and privacy policies of any application you use.</li>\n<li><strong>For tips and tools on cyber security, </strong>bookmark the <a href=\"https://www.getcybersafe.gc.ca/index-en.aspx\">Get Cyber Safe</a> website and follow Get Cyber Safe content on <a href=\"https://twitter.com/GetCyberSafe\">Twitter</a>, <a href=\"https://www.facebook.com/GetCyberSafe\">Facebook</a>, <a href=\"https://www.instagram.com/getcybersafe/\">Instagram</a> and <a href=\"https://www.linkedin.com/showcase/get-cyber-safe/?viewAsMember=true\">LinkedIn</a><strong>. </strong></li>\n</ol>\n<p>Cyber defence is a team sport. It is essential that we all work together to strengthen our departmental cyber security.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/Cyber-Security.jpg\" class=\"center-block\"></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Octobre est le Mois de la sensibilisation \u00e0 la cybers\u00e9curit\u00e9. On encourage les Canadiens \u00e0 en profiter pour se renseigner sur les fa\u00e7ons d\u2019assurer leur s\u00e9curit\u00e9 en ligne.</p>\n<p>Voici de simples mesures que vous pouvez prendre pour que votre exp\u00e9rience en ligne soit s\u00e9curitaire.</p>\n<ol>\n<li><strong>Assurez la s\u00e9curit\u00e9 de vos mots de passe</strong>. Utilisez un mot de passe qui combine des lettres majuscules et minuscules, et des chiffres ou symboles. Ne communiquez pas vos mots de passe.</li>\n<li><strong>Acceptez les mises \u00e0 jour de vos appareils mobiles, ordinateurs et applications.</strong> Vous devriez mettre \u00e0 jour r\u00e9guli\u00e8rement le syst\u00e8me d\u2019exploitation de votre ordinateur ou de votre appareil mobile pour r\u00e9duire leur vuln\u00e9rabilit\u00e9 aux nouvelles menaces \u00e0 la s\u00e9curit\u00e9. Red\u00e9marrez votre poste de travail r\u00e9guli\u00e8rement pour permettre l\u2019application de correctifs.</li>\n<li><strong>Soyez \u00e0 l\u2019aff\u00fbt des messages d\u2019hame\u00e7onnage et des tentatives d\u2019extorsion.</strong> N\u2019ouvrez jamais les courriels ou les pi\u00e8ces jointes provenant d\u2019inconnus et ne cliquez jamais sur les liens menant \u00e0 des sites Web propos\u00e9s dans leurs courriels. Ils pourraient contaminer votre ordinateur au moyen d\u2019un virus ou d\u2019un logiciel espion. Supprimez ces courriels imm\u00e9diatement. Si vous avez ouvert le courriel et cliqu\u00e9 sur le lien, avisez le IT Centre TI. Signalez toute tentative d\u2019extorsion aux Services de s\u00e9curit\u00e9 \u00e0 <a href=\"mailto:aafc.securityincident-incidentsecurite.aac@canada.ca\">aafc.securityincident-incidentsecurite.aac@canada.ca</a>.</li>\n<li><strong><a href=\"https://www.cyber.gc.ca/fr/orientation/protegez-vous-en-ligne\">\u00c9valuez les risques</a> avant d\u2019utiliser les plateformes et applications de m\u00e9dias sociaux.</strong> Les applications de messagerie instantan\u00e9e et les plateformes de m\u00e9dias sociaux ne se valent pas toutes. Avant d\u2019en choisir une, tenez compte de la fonctionnalit\u00e9 du service et de la mesure dans laquelle ce service peut assurer la s\u00e9curit\u00e9 et la confidentialit\u00e9 de votre information et de vos activit\u00e9s. Lisez les conditions de service et les politiques de confidentialit\u00e9 des applications que vous utilisez.</li>\n<li><strong>Pour d\u00e9couvrir des conseils et outils sur la cybers\u00e9curit\u00e9, </strong>ajoutez le site Web de <a href=\"https://www.pensezcybersecurite.gc.ca/index-fr.aspx\">Pensez cybers\u00e9curit\u00e9 </a>\u00e0 vos favoris et suivez le contenu de Pensez cybers\u00e9curit\u00e9 sur <a href=\"https://twitter.com/cyber_securite\">Twitter</a>, <a href=\"https://www.facebook.com/Pensezcybersecurite/\">Facebook</a>, <a href=\"https://www.instagram.com/pensezcybersecurite/\">Instagram</a> et <a href=\"https://www.linkedin.com/showcase/pensez-cybers%C3%A9curit%C3%A9/?viewAsMember=true\">LinkedIn</a>.</li>\n</ol>\n<p>La cyberd\u00e9fense, c\u2019est un sport d\u2019\u00e9quipe. Il est essentiel que nous contribuions tous \u00e0 renforcer la cybers\u00e9curit\u00e9 du Minist\u00e8re.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/Cyber-Security.jpg\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Cyber Security Month 2019: Protect Yourself From Online Dangers",
        "fr": "Mois de la sensibilisation \u00e0 la cybers\u00e9curit\u00e9 2019 : Prot\u00e9gez-vous des dangers en ligne"
    },
    "news": {
        "date_posted": {
            "en": "2019-10-24",
            "fr": "2019-10-24"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Information Systems Branch and Departmental Security Services",
            "fr": "Direction g\u00e9n\u00e9rale des syst\u00e8mes d\u2019information et Services de s\u00e9curit\u00e9 minist\u00e9riels"
        }
    }
}