{
    "dcr_id": "1329400594854",
    "lang": "en",
    "title": {
        "en": "Professional Development and Apprenticeship Programs",
        "fr": "Programmes de perfectionnement professionnel et d'apprentissage"
    },
    "modified": "2018-11-21 00:00:00.0",
    "issued": "2012-02-21 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1474032522040",
    "layout_name": "1 column",
    "dc_date_created": "2012-02-16",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2012-02-21",
            "fr": "2012-02-21"
        },
        "modified": {
            "en": "2018-11-21",
            "fr": "2018-11-21"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Professional Development and Apprenticeship Programs",
            "fr": "Programmes de perfectionnement professionnel et d'apprentissage"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Professional Development and Apprenticeship Programs (PDAPs) provide a structured learning approach to employee development. They consist of formal training, coaching, and developmental assignments within a defined learning framework. PDAPs are an effective strategy to attract, retain, and develop employees.</p>\n\n<p>There are currently six development programs at Agriculture and Agri-Food Canada (AAFC):</p>\n\n<ul>\n<li>The <a href=\"display-afficher.do?id=1329403260606&amp;lang=eng\">Economics and Social Science Services Development Program</a> (ECDP) is aimed at developing participants from the EC-02, 03 and 04 positions up to the EC-05 level.</li>\n\n<li>The <a href=\"display-afficher.do?id=1331643881035&amp;lang=eng\">Human Resources Coordinator Development Program - Staffing</a> is designed for Human Resources (HR) Coordinators in Staffing in the Corporate Management Branch. The program is aimed at developing participants from an entry level CR-04 to a working level CR-05.</li>\n\n<li>The <a href=\"display-afficher.do?id=1331583605575&amp;lang=eng\">Human Resources Development Program</a> (HRDP) is designed for HR employees working in PE01-02 and AS02-03 positions to acquire experience and develop their HR competencies.</li>\n\n<li>As part of <a href=\"https://collab.agr.gc.ca/co/frms-dsfgr/SitePages/Financial%20Community%20Development.aspx\">Financial Community Development</a>, a Financial Officer Development Program (FIDP) is available to FI-01s in the Department.  This is a professional development and apprenticeship program that focuses on learning and development opportunities. It provides participants with the experience and knowledge necessary in order to attain the skills and competencies required to perform at the FI-02 level.</li>\n\n<li>The <a href=\"display-afficher.do?id=1509977974386&amp;lang=eng\">Administrative Recruitment and Development Program</a> (ARDP) is designed to bring structure and consistency to the training and development of administrative professionals.</li>\n\n<li>The <a href=\"display-afficher.do?id=1345121547628&amp;lang=eng\">Management and Leadership Development Program</a> (MLDP) is designed to support the development of managers at the Executive (EX) equivalent, EX minus 1 and EX minus 2 levels.</li>\n</ul>\n\n<h3>PDAP Considerations</h3>\n\n<p>Managers who are considering the development and implementation of a PDAP at AAFC are encouraged to review the <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=103999777&amp;lang=eng\" target=\"_blank\">PDAP Considerations document</a>.  It provides information and general guidance to managers, including an overview of PDAPs and an outline of the key foundational and operational PDAP considerations.</p>\n\n<p>For additional information on PDAP development, please contact <a href=\"mailto:aafc.learning-apprentissage.aac@canada.ca\">aafc.learning-apprentissage.aac@canada.ca</a>.</p>\n\n<h2>Contact Program Administrators</h2>\n\n<ul>\n<li>Economics and Social Science Services Development Program (ECDP): <a href=\"mailto:aafc.ecdpsecretariatppec.aac@canada.ca\">aafc.ecdpsecretariatppec.aac@canada.ca</a></li>\n<li>Human Resources Coordinator Development Program \u2013 Staffing: contact a Team Manager in Staffing Operations (refer to list of <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1548432&amp;lang=eng\" target=\"_blank\">Human Resources Advisors (Word)</a>)</li>\n<li>Human Resources Development Program (HRDP): <a href=\"mailto:aafc.hrdp-pprh.aac@canada.ca\">aafc.hrdp-pprh.aac@canada.ca</a></li>\n<li>Financial Officer Development Program (FIDP): <a href=\"mailto:aafc.fcd-dcf.aac@canada.ca\">aafc.fcd-dcf.aac@canada.ca</a></li>\n<li>Administrative Recruitment and Development Program (ARDP): <a href=\"mailto:aafc.ardp-prppa.aac@canada.ca\">aafc.ardp-prppa.aac@canada.ca</a></li>\n<li>Management and Leadership Development Program (MLDP): <a href=\"mailto:aafc.mldp-ppgl.aac@canada.ca\">aafc.mldp-ppgl.aac@canada.ca</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Les programmes de perfectionnement professionnel et d'apprentissage (PPPA) fournissent une approche d\u2019apprentissage structur\u00e9 au perfectionnement des employ\u00e9s. Ils consistent en une formation officielle, un encadrement et des affectations de perfectionnement dans un cadre d'apprentissage d\u00e9fini. Ces programmes repr\u00e9sentent une strat\u00e9gie efficace pour attirer, garder et perfectionner les employ\u00e9s.</p>\n\n<p>\u00c0 l'heure actuelle, Agriculture et Agroalimentaire Canada (AAC) offre six programmes de perfectionnement\u00a0:</p>\n\n<ul>\n<li>le <a href=\"display-afficher.do?id=1329403260606&amp;lang=fra\">Programme de perfectionnement du groupe \u00c9conomique et services de sciences sociales</a> (PPEC) vise \u00e0 d\u00e9velopper les participants de postes de niveau EC-02, 03 et 04 \u00e0 des postes de niveau EC-05;</li>\n\n<li>le <a href=\"display-afficher.do?id=1331643881035&amp;lang=fra\">Programme de perfectionnement des coordonnateurs des ressources humaines - dotation</a> est con\u00e7u pour les coordonnateurs des ressources humaines (RH) en mati\u00e8re de dotation au sein de la Direction g\u00e9n\u00e9rale de la gestion int\u00e9gr\u00e9e. Le programme vise \u00e0 d\u00e9velopper les participants de postes de niveau d'entr\u00e9e\u00a0CR-04 \u00e0 des postes de niveau CR-05;</li>\n\n<li>le <a href=\"display-afficher.do?id=1331583605575&amp;lang=fra\">Programme de perfectionnement des ressources humaines</a> (PPRH) est con\u00e7u pour les employ\u00e9s des RH aux niveaux PE01-02 et AS02-03 afin qu'ils acqui\u00e8rent de l'exp\u00e9riences et d\u00e9veloppent leurs comp\u00e9tences en mati\u00e8re de RH.</li>\n\n<li>Dans le cadre du <a href=\"https://collab.agr.gc.ca/co/frms-dsfgr/SitePages/Financial%20Community%20Development.aspx\">perfectionnement de la collectivit\u00e9 des finances</a>, le Minist\u00e8re offre aux FI-01 un Programme de perfectionnement des agents financiers (PPFI) qui met l'accent sur les possibilit\u00e9s d'apprentissage et de perfectionnement professionnels et qui fournit aux participants l'exp\u00e9rience et les connaissances n\u00e9cessaires \u00e0 l'acquisition des habilet\u00e9s et des comp\u00e9tences voulues pour exercer les fonctions de FI-02.</li>\n\n<li>le <a href=\"display-afficher.do?id=1509977974386&amp;lang=fra\">Programme de recrutement et de perfectionnement de professionnels en administration</a> (PRPPA) est con\u00e7u pour structurer et uniformiser les occasions de formation et de perfectionnement des professionnels en administration.</li>\n\n<li>le <a href=\"display-afficher.do?id=1345121547628&amp;lang=fra\">Programme de perfectionnement en gestion et en leadership</a> (PPGL) vise \u00e0 favoriser le perfectionnement des gestionnaires des niveaux Direction (EX) \u00e9quivalent, EX moins 1 et EX moins 2.</li>\n</ul>\n\n<h3>Consid\u00e9rations relatives au PPPA</h3>\n\n<p>Les gestionnaires qui envisagent de r\u00e9diger et de mettre en \u0153uvre un PPPA \u00e0 AAC sont invit\u00e9s \u00e0 passer en revue le document intitule\u00a0:  <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=104281378&amp;lang=fra\" target=\"_blank\">Consid\u00e9rations relatives au PPPA</a>. Ce document renferme des renseignements et des conseils g\u00e9n\u00e9raux \u00e0 l\u2019intention des gestionnaires, notamment une br\u00e8ve description d\u2019un PPPA, ainsi qu\u2019une description des consid\u00e9rations fondamentales et op\u00e9rationnelles relativement \u00e0 ce type de plan.</p>\n\n<p>Pour obtenir des renseignements additionnels sur le PPPA, veuillez \u00e9crire un courriel \u00e0\u00a0: <a href=\"mailto:aafc.learning-apprentissage.aac@canada.ca\">aafc.learning-apprentissage.aac@canada.ca</a>.</p>\n\n\n<h2>Contactez les administrateurs des programmes</h2>\n\n<ul>\n<li>Programme de perfectionnement du groupe \u00c9conomique  et services de sciences sociales (PPEC)\u00a0: <a href=\"mailto:aafc.ecdpsecretariatppec.aac@canada.ca\">aafc.ecdpsecretariatppec.aac@canada.ca</a></li>\n\n<li>Programme de perfectionnement des coordonnateurs des ressources humaines \u2013  Dotation\u00a0: communiquez avec un chef d'\u00e9quipe du groupe des Op\u00e9rations de dotation (consultez la liste des <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1548432&amp;lang=fra\" target=\"_blank\">conseillers en ressources humaines (Word)</a>)</li>\n\n<li>Programme de perfectionnement des ressources humaines (PPRH)\u00a0: <a href=\"mailto:aafc.hrdp-pprh.aac@canada.ca\">aafc.hrdp-pprh.aac@canada.ca</a></li>\n\n<li>Programme de perfectionnement des agents financiers (PPFI)\u00a0: <a href=\"mailto:aafc.fcd-dcf.aac@canada.ca\">aafc.fcd-dcf.aac@canada.ca</a></li>\n\n<li>Programme de recrutement et de perfectionnement de professionnels en administration (PRPPA)\u00a0: <a href=\"mailto:aafc.ardp-prppa.aac@canada.ca\">aafc.ardp-prppa.aac@canada.ca</a></li>\n\n<li>Programme de perfectionnement en gestion et en leadership (PPGL)\u00a0: <a href=\"mailto:aafc.mldp-ppgl.aac@canada.ca\">aafc.mldp-ppgl.aac@canada.ca</a></li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Professional Development and Apprenticeship Programs",
        "fr": "Programmes de perfectionnement professionnel et d'apprentissage"
    }
}