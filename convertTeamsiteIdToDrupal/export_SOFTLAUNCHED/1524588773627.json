{
    "dcr_id": "1524588773627",
    "lang": "en",
    "title": {
        "en": "Training for employees",
        "fr": "Formation pour les employ\u00e9s"
    },
    "modified": "2020-02-26 00:00:00.0",
    "issued": "2018-05-16 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1524660248728",
    "layout_name": "1 column",
    "dc_date_created": "2018-04-24",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-05-16",
            "fr": "2018-05-16"
        },
        "modified": {
            "en": "2020-02-26",
            "fr": "2020-02-26"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Training for employees",
            "fr": "Formation pour les employ\u00e9s"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<section>\n\t\t\t  \n\n<div class=\"row\">\n  <div class=\"col-md-3 pull-right\">\n\n    <section class=\"panel panel-primary\">\n      <header class=\"panel-heading\">\n        <h2 class=\"panel-title\">Contact Us</h2>\n      </header>\n      <div class=\"panel-body\"> \n\t\t  \n<p>For information about training and course registration: <a href=\"mailto:aafc.tlcregistrations-ceainscriptions.aac@canada.ca\">aafc.tlcregistrations-ceainscriptions.aac@canada.ca</a></p>\n\n\t\t</div>\n    </section>\n  </div>\n  <div class=\"col-md-9 pull-left\">\n\t  \n    <p>Find training for individuals who do not have managerial responsibilities. This category includes new Agriculture and Agri-Food Canada (AAFC) employees, casual workers, and employees on secondment and Interchange Canada assignments.</p>\n\t  \n<section class=\"alert alert-info\">\n<h2>To review your training summary</h2>\n\t\n<p><a href=\"https://psoft.agr.gc.ca/psp/peoplesoft8/?cmd=login&amp;languageCd=ENG&amp;\">Login to PeopleSoft</a> and go to Self Service \u203a Learning and Development \u203a Training Summary</p>\t\n\t\n</section>\t  \n\t  \n<h2>Recommended training</h2>\n\n<ul>\n<li><a href=\"?id=1366731814839\">AGO-102 \u2013 Occupational Health and Safety Roles and Responsibilities for Employees</a></li>\n<li><a href=\"?id=1537459996922\">AGR-213 \u2013 Access and Privacy 101 \u2013 The Basics</a></li>\n<li><a href=\"?id=1363787260050\">AGR-226 \u2013 Communication and Conflict Resolution in the Workplace</a></li>\n<li><a href=\"?id=1341950176975\">AGR-457 \u2013 Tracking and Reporting Executive Correspondence System (TRECS) Training</a></li>\n<li><a href=\"?id=1326124885316\">AGR-458 \u2013 AgriDoc Introduction</a></li>\n<li><a href=\"?id=1516828461782\">AGR-562 \u2013 Positive Space Champions</a></li>\n<li><a href=\"?id=1516827524772\">AGR-563 \u2013 Gender and Sexual Diversity in the Workplace</a></li>\n<li><a href=\"?id=1532614141817\">AGR-565 \u2013 The Working Mind \u2013 Mental Health Training for Employees</a></li>\n<li><a href=\"?id=1332250809743\">AGR-623 \u2013 Green Defensive Driving Course</a> (for employees who use government vehicles)</li>\n<li><a href=\"?id=1421091534123\">AGR-700 \u2013 Performance Management at Agriculture and Agri-Food Canada</a></li>\n<li><a href=\"?id=1523285476886\">C601 \u2013 Phoenix Self-Service for Employees</a> (GCcampus)</li>\n<li><a href=\"?id=1520885073130\">Career Transitions (00109028)</a></li>\n<li><a href=\"?id=1523285243215\">E236 \u2013 Navigating Through Change</a> (GCcampus)</li>\n<li><a href=\"https://collab.agr.gc.ca/help/Help%20Pages%20dAide/Training.aspx\">Knowledge Workspace Training</a></li>\n</ul> \n    \n  </div>\n</div>\n\t\t</section>",
        "fr": "<section>\n\t\t\t  \n\n<div class=\"row\">\n  <div class=\"col-md-3 pull-right\">\n\n    <section class=\"panel panel-primary\">\n      <header class=\"panel-heading\">\n        <h2 class=\"panel-title\">Contactez-nous</h2>\n      </header>\n      <div class=\"panel-body\"> \n\t\t  \n<p>Pour obtenir plus d\u2019informations au sujet de la formation et de l\u2019inscription aux cours\u00a0: <a href=\"mailto:aafc.tlcregistrations-ceainscriptions.aac@canada.ca\">aafc.tlcregistrations-ceainscriptions.aac@canada.ca</a></p>\n\t\t  \n\t\t</div>\n    </section>\n  </div>\n  <div class=\"col-md-9 pull-left\">\n\t  \n<p>Trouvez des cours pour les personnes qui n'ont pas de responsabilit\u00e9s de supervision ou de gestion. Cette cat\u00e9gorie englobe les nouveaux employ\u00e9s d'Agriculture et Agroalimentaire Canada (AAC), de m\u00eame que les employ\u00e9s occasionnels et les personnes qui sont en d\u00e9tachement ou en affectation dans le cadre d'\u00c9changes Canada.</p>\n\t  \n<section class=\"alert alert-info\">\n<h2>Pour consulter votre sommaire de formation</h2>\n\t\n<p><a href=\"https://psoft.agr.gc.ca/psp/peoplesoft8/?cmd=login&amp;languageCd=CFR\">Connectez-vous \u00e0 PeopleSoft</a>, puis s\u00e9lectionnez Libre-service \u203a Apprentissage et perfect. \u203a Sommaire formation</p>\t\n\t\n</section>\t  \n\n<h2>Cours recommand\u00e9s</h2>\n\n<ul>\n<li><a href=\"?id=1366731814839\">AGO-102 \u2013 R\u00f4les et responsabilit\u00e9s en mati\u00e8re de sant\u00e9 et de s\u00e9curit\u00e9 au travail pour les employ\u00e9s</a></li>\n<li><a href=\"?id=1537459996922\">AGR-213 \u2013 Formation sur l\u2019acc\u00e8s \u00e0 l\u2019information et sur la protection de la vie priv\u00e9e \u2013 Notions de base</a></li>\n<li><a href=\"?id=1363787260050\">AGR-226 \u2013 Communication et r\u00e9solution de conflits en milieu de travail</a></li>\n<li><a href=\"?id=1341950176975\">AGR-457 \u2013 Formation sur le syst\u00e8me de suivi et de rapports pour la correspondance de la haute direction (SSRCHD)</a></li>\n<li><a href=\"?id=1326124885316\">AGR-458 \u2013 AgriDoc</a></li>\n<li><a href=\"?id=1516828461782\">AGR-562 \u2013 Champions de l\u2019Espace positif</a></li>\n<li><a href=\"?id=1516827524772\">AGR-563 \u2013 La diversit\u00e9 des genres et de la sexualit\u00e9 en milieu de travail</a></li>\n<li><a href=\"?id=1532614141817\">AGR-565 - L\u2019esprit au travail \u2013 Formation sur la sant\u00e9 mentale pour les employ\u00e9s</a></li>\n<li><a href=\"?id=1332250809743\">AGR-623 \u2013 Cours d'\u00e9coconduite pr\u00e9ventive</a> (ce cours s\u2019applique aux employ\u00e9s qui utilisent des v\u00e9hicules du gouvernement)</li>\n<li><a href=\"?id=1421091534123\">AGR-700 \u2013 La gestion du rendement \u00e0 Agriculture et Agroalimentaire Canada</a></li>\n<li><a href=\"?id=1523285476886\">C601 \u2013 Libre-service de Ph\u00e9nix pour les employ\u00e9s</a> (GCcampus)</li>\n<li><a href=\"?id=1523285243215\">E236 \u2013 Composer avec le changement</a> (GCcampus)</li>\n<li><a href=\"https://collab.agr.gc.ca/help/Help%20Pages%20dAide/Training.aspx\">Formation sur l\u2019Espace de travail du savoir</a></li>\n<li><a href=\"?id=1520885073130\">Transitions de carri\u00e8re (00109028)</a></li>\n</ul>\t  \n    \n  </div>\n</div>\n\t\t</section>"
    },
    "breadcrumb": {
        "en": "Employees",
        "fr": "Employ\u00e9s"
    }
}