{
    "dcr_id": "1566486851076",
    "lang": "en",
    "title": {
        "en": "Message from AAFC's Ombudsperson",
        "fr": "Message d'ombudsman \u00e0 AAC"
    },
    "modified": "2019-08-22 00:00:00.0",
    "issued": "2019-08-22 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-08-22",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-08-22",
            "fr": "2019-08-22"
        },
        "modified": {
            "en": "2019-08-22",
            "fr": "2019-08-22"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Message from AAFC's Ombudsperson",
            "fr": "Message d'ombudsman \u00e0 AAC"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "It is an honour to have been appointed the Ombudsperson at AAFC.",
            "fr": "C'est un honneur d'avoir \u00e9t\u00e9 nomm\u00e9e au poste d'ombudsman \u00e0 AAC."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>It is an honour to have been appointed the Ombudsperson at AAFC. I have always been proud to be part of this Department and impressed with the work undertaken across the country to support the agricultural sector. As Ombuds, my work is with employees and my focus will be on the values we espouse internally, what kind of culture we create together as an organization, and what kind of legacy our leaders will pass along to their successors.</p>\n<p>The AAFC Code is clear on the values we are expected to uphold as public servants: respect for democracy; respect for people; integrity; stewardship; and excellence. And our People Strategy recognizes that people are our most important asset, with themes that clearly put employees at the centre of the strategy: workforce; work environment; and culture. All of these areas are important to me and my work. How are we doing at living these values? When the going gets tough, are we keeping them top of mind? How are we doing at creating and maintaining a healthy and productive workplace? Are we taking concrete steps to eliminate harassment and what more can we do? And, do employees have the support, resources and tools when harassment happens?</p>\n<p>While I remain part of the Department, I will work independently and report directly to the Deputy Minister. My initial priorities are: to establish an office; open lines of communications with employees (phone, video, in-person); and chart an outreach plan to begin in early fall. I will meet with individual employees upon their request and I will proactively be reaching out to committees and networks to describe my role and listen. I am planning an all-staff town hall in early fall. At all times, my work will be guided by four principles: confidentiality; informality; impartiality; and independence.</p>\n<p>My aim is to ensure the Ombuds office provides employees with a trusted safe space to discuss well-being and harassment without the fear of reprisal and helps navigate the existing systems. Overall my functions include:</p>\n<ul>\n<li>Assist all employees (regardless of location and level) to constructively takes issues forward by identifying and evaluating options for resolving problems, and referring to other channels of resolution as required;</li>\n<li>Support the resolution of individual concerns, and upon request bring issues forward to those with the authority to act;</li>\n<li>Be a resource and facilitates outreach to promote well-being and resources available to employees, including best practices;</li>\n<li>Serve as an upward feedback channel to management, and a catalyst for change by identifying trends and patterns and recommendations regarding systemic and organizational issues.</li>\n</ul>\n<p>For more information, check out <a href=\"http://intranet.agr.gc.ca/agrisource/eng?id=1566410288815&amp;utm_source=news_at_work&amp;utm_medium=news_at_work&amp;utm_campaign=aafc_ombudsperson\">my new webpage on AgriSource</a>. I will build on this information in the months ahead.</p>\n<p>No concern or question is too big or too small when it comes to prioritizing and supporting the well-being of AAFC employees. To send questions, feedback or suggestions or to schedule an appointment with the Ombudsperson please call 613-773-3114 or send an email to <a href=\"mailto:AAFC.Ombuds.AAC@canada.ca\">AAFC.Ombuds.AAC@canada.ca</a>.</p>\n<p>I look forward to meeting and working with you.<br>\n<br><br>\n<strong>Jane Taylor</strong><br>\nWorkplace Well-Being Ombudsperson</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/Ombudsperson-EN.png\" class=\"center-block\" alt=\"\"></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>C'est un honneur d\u2019avoir \u00e9t\u00e9 nomm\u00e9e au poste d\u2019ombudsman \u00e0 AAC. J\u2019ai toujours \u00e9t\u00e9 fi\u00e8re de faire partie de ce minist\u00e8re et impressionn\u00e9e par le travail accompli d\u2019un bout \u00e0 l\u2019autre du pays pour soutenir le secteur agricole. \u00c0 titre d\u2019ombudsman, je travaille avec les employ\u00e9s et mettrai l\u2019accent sur les valeurs que nous adoptons \u00e0 l\u2019interne, sur la culture que nous cr\u00e9ons ensemble comme organisation ainsi que sur l\u2019h\u00e9ritage que nos dirigeants l\u00e8gueront \u00e0 leurs successeurs.</p>\n<p>Le Code d\u2019AAC est clair au sujet des valeurs que nous sommes cens\u00e9s d\u00e9fendre en tant que fonctionnaires : le respect de la d\u00e9mocratie, le respect des personnes, l\u2019int\u00e9grit\u00e9, l\u2019intendance et l\u2019excellence. Notre Strat\u00e9gie de gestion des personnes reconnait que les gens repr\u00e9sentent notre atout le plus pr\u00e9cieux et repose sur des th\u00e8mes qui valorisent clairement les employ\u00e9s : effectif, environnement de travail et culture. Tous ces domaines sont importants \u00e0 mes yeux, mais aussi dans le cadre de mon travail. Dans quelle mesure adoptons-nous ces valeurs? Quand la situation se corse, est-ce que nous les gardons \u00e0 l\u2019esprit? \u00c0 quel point parvenons-nous \u00e0 cr\u00e9er et \u00e0 maintenir un milieu de travail sain et productif? Agissons-nous concr\u00e8tement pour \u00e9liminer le harc\u00e8lement? Que pouvons-nous faire de plus? Aussi, est-ce que les employ\u00e9s disposent du soutien, des ressources et des outils dont ils ont besoin en cas de harc\u00e8lement?</p>\n<p>Je continuerai de faire partie du minist\u00e8re, mais je travaillerai de fa\u00e7on ind\u00e9pendante et rel\u00e8verai directement du sous-ministre. Au d\u00e9part, mes priorit\u00e9s seront les suivantes : mettre sur pied un bureau; ouvrir les voies de communication avec les employ\u00e9s (par t\u00e9l\u00e9phone, par vid\u00e9o, en personne); entamer au d\u00e9but de l\u2019automne l\u2019\u00e9tablissement d\u2019un plan de sensibilisation. Je rencontrerai individuellement les employ\u00e9s qui en feront la demande et communiquerai proactivement avec les divers comit\u00e9s et r\u00e9seaux pour d\u00e9crire mon r\u00f4le et \u00e9couter ce que les gens ont \u00e0 dire. Je m\u2019affaire \u00e0 organiser une assembl\u00e9e g\u00e9n\u00e9rale qui aura lieu au d\u00e9but de l\u2019automne. En tout temps, je travaillerai selon quatre principes : confidentialit\u00e9, absence de formalit\u00e9, impartialit\u00e9 et ind\u00e9pendance.</p>\n<p>Mon but est de veiller \u00e0 ce que le bureau de l\u2019ombudsman offre aux employ\u00e9s un espace s\u00fbr leur permettant de discuter en toute confiance de bien-\u00eatre et de harc\u00e8lement sans crainte de repr\u00e9sailles et aide les employ\u00e9s \u00e0 utiliser les divers syst\u00e8mes en place. Essentiellement, mes fonctions seront les suivantes :</p>\n<ul>\n<li>Aider tous les employ\u00e9s (peu importe le lieu de travail ou le niveau de poste) \u00e0 soulever des questions de fa\u00e7on constructive en d\u00e9terminant et en \u00e9valuant les options permettant de r\u00e9soudre les probl\u00e8mes, et faire appel \u00e0 d\u2019autres moyens de r\u00e9solution au besoin;</li>\n<li>Faciliter la r\u00e9solution des pr\u00e9occupations de chacun et, sur demande, porter les questions \u00e0 l\u2019attention des autorit\u00e9s comp\u00e9tentes;</li>\n<li>\u00catre une personne-ressource qui contribue \u00e0 la promotion du bien-\u00eatre et des ressources offertes aux employ\u00e9s, y compris les pratiques exemplaires;</li>\n<li>Transmettre la r\u00e9troaction des employ\u00e9s \u00e0 la direction, et agir comme catalyseur de changement en cernant les tendances et habitudes, et en formulant des recommandations concernant des probl\u00e8mes syst\u00e9miques et organisationnels.</li>\n</ul>\n<p>Pour en savoir plus, consultez <a href=\"http://intranet.agr.gc.ca/agrisource/fra/directions-generales-et-bureaux/bureau-de-lombudsman?id=1566410288815&amp;utm_source=news_at_work&amp;utm_medium=news_at_work&amp;utm_campaign=afc_ombudsman\">ma nouvelle page Web sur AgriSource\u00a0</a>: j\u2019y ajouterai de l\u2019information au cours des mois \u00e0 venir.</p>\n<p>Aucune pr\u00e9occupation ou question n\u2019est trop ou pas assez importante lorsqu\u2019il s\u2019agit du bien-\u00eatre des employ\u00e9s d\u2019AAC : il faut y contribuer et en faire une priorit\u00e9. Pour faire part de vos questions, commentaires ou suggestions, ou pour fixer un rendez-vous avec l\u2019ombudsman, veuillez composer le 613-773-3114 ou envoyer un courriel \u00e0 l\u2019adresse <a href=\"mailto:AAFC.Ombuds.AAC@canada.ca\">AAFC.Ombuds.AAC@canada.ca</a>.</p>\n<p>Au plaisir de vous rencontrer et de travailler avec vous.<br>\n<br>\n<br><b>\nJane Taylor</b><br>\nL'ombudsman du mieux-\u00eatre en milieu de travail</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/Ombudsperson-FR.png\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Message from AAFC's Ombudsperson",
        "fr": "Message d'ombudsman \u00e0 AAC"
    },
    "news": {
        "date_posted": {
            "en": "2019-08-22",
            "fr": "2019-08-22"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Jane Taylor, Workplace Well-Being Ombudsperson",
            "fr": "Jane Taylor, L'ombudsman du mieux-\u00eatre en milieu de travail"
        }
    }
}