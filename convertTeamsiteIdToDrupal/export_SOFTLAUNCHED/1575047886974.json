{
    "dcr_id": "1575047886974",
    "lang": "en",
    "title": {
        "en": "Manager",
        "fr": "Gestionnaire"
    },
    "modified": "2019-12-03 00:00:00.0",
    "issued": "2019-12-02 00:00:00.0",
    "type_name": "intra-intra/empl-empl",
    "node_id": "1279029955397",
    "layout_name": null,
    "dc_date_created": "2019-11-29",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-12-02",
            "fr": "2019-12-02"
        },
        "modified": {
            "en": "2019-12-03",
            "fr": "2019-12-03"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Manager",
            "fr": "Gestionnaire"
        },
        "subject": {
            "en": "staffing",
            "fr": "dotation en personnel"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "notice",
            "fr": "avis"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n<p><strong>\u00a0</strong></p>\n<p><strong>Duties:</strong><em>\u00a0 </em></p>\n<p>The Manager of the Accounts Receivable Revenue Management Section (ARRMS) reports to the Director, Financial Services, Systems and Receivables and leads a dynamic unit with two teams: the first manages the administrative and monitoring aspects of revenues and receivables, and the second manages collection activities on several types of receivables: defaulted program receivables (Financial Guarantee Programs Division), program overpayment receivables (Financial Income Programs Division), Repayment Program receivables (Unconditional Repayable Contributions/Conditional Repayable Contributions), employee receivables and collaborator receivables.</p>\n<p>The main responsibilities of this position are:</p>\n<ul>\n<li>Managing two teams: Administrative and Monitoring Team (6 employees - FIs, CRs, Students), and Collections Team (13 employees - PMs, ASs);</li>\n<li>Managing a budget and exercising financial delegation authority (S32, S34);</li>\n<li>Managing the department\u2019s quarterly debt deletion process;</li>\n<li>Overseeing the accuracy of financial information in the collections management system, Sales and Distribution Accounts Receivables (SD) module of SAP, and Tax Information Slip System (TISS);</li>\n<li>Undertaking/Supporting\u00a0 projects to achieve the Departmental and/or Recovery Unit's overall goals (Government-wide pilot projects, Paper reduction, electronic approvals, etc.); and</li>\n<li>Various ad hoc activities related to:\u00a0 year-end, resolution of systems issues, report preparation, maintaining client relationships, etc.</li>\n</ul>\n<p><strong><br>Statement of Merit Criteria &amp; Conditions of Employment</strong></p>\n<p><strong><br>Essential Qualifications</strong></p>\n<p>Education:</p>\n<p>A degree from a recognized post-secondary institution with specialization in accounting, finance, business administration, commerce or economics</p>\n<p>AND</p>\n<p>Experience related to positions in the Financial Management Group;</p>\n<p>OR</p>\n<p>Possession of a recognized professional accounting designation (i.e. CPA, CA, CMA or CGA).</p>\n<p><br>Experience:\u00a0</p>\n<ul>\n<li>Significant and recent experience* in one (1) or more of the following areas of financial management:\n\t\n<ul style=\"list-style-type: circle;\">\n<li>Financial Planning</li>\n<li>External Reporting</li>\n<li>Internal Resource Management</li>\n<li>Financial Management Advisory Services</li>\n<li>Accounting Operations</li>\n<li>Financial Systems</li>\n<li>Financial Policy</li>\n<li>Costing</li>\n<li>Attestation</li>\n<li>Internal Controls</li>\n<li>Corporate Reporting</li>\n<li>Accounts Receivable/Collections</li>\n</ul>\n</li>\n<li>Experience in conducting strategic analysis and providing recommendations to senior management** on financial management issues.</li>\n<li>Experience in managing partnerships with clients and key stakeholders in the Federal Government.</li>\n<li>Experience in supporting the implementation of Federal Government initiatives.\u00a0</li>\n<li>Experience in managing human resources..\u00a0\u00a0</li>\n</ul>\n<p>*Significant and recent experience is defined as the depth and breadth of experience acquired in the Federal Government of Canada for a minimum period of five (5) years at the FI-03 level or above (or equivalent group and level) within the last seven (7) years, where the experience was part of the work duties of the employee.</p>\n<p>**Senior management is defined as Assistant Deputy Ministers (ADM), Director Generals and/or Directors.</p>\n<p><br>Knowledge:</p>\n<ul>\n<li>Knowledge of the Federal Government\u2019s priorities and their impact on the department\u2019s mandate, policies and programs.</li>\n<li>Knowledge of current challenges impacting the financial management community in the Federal Government.</li>\n<li>Knowledge of the Treasury Board of Canada Secretariat\u2019s financial management policies and directives.</li>\n<li>Knowledge of the Federal Government\u2019s budgetary planning cycle.</li>\n</ul>\n<p><br>Abilities:</p>\n<ul>\n<li>Ability to adjust to changes in priorities and to meet deadlines.</li>\n<li>Ability to communicate effectively orally.</li>\n<li>Ability to communicate effectively in writing.</li>\n<li>Ability to plan and organize own work and the work of a team.</li>\n</ul>\n<p><br>Competencies:</p>\n<ul>\n<li>Create Vision and Strategy</li>\n<li>Mobilize People</li>\n<li>Uphold Integrity and Respect</li>\n<li>Collaborate with Partners and Stakeholders</li>\n<li>Promote Innovation and Guide Change</li>\n<li>Achieve Results</li>\n</ul>\n<p><br>Language Requirement:</p>\n<ul>\n<li>Bilingual Imperative (CBC/CBC).</li>\n</ul>\n<h3><br>Asset Qualifications:</h3>\n<ul>\n<li>Possession of a recognized professional accounting designation (i.e CPA, CA, CMA, or CGA).</li>\n</ul>\n<h3><br>Organizational Needs:</h3>\n<p>In support of achieving a diversified workforce, consideration may be given to candidates self-identifying as belonging to one of the following Employment Equity groups: Aboriginal peoples; Persons with a Disability; Visible Minorities; and Women.</p>\n<p><strong><br>Conditions of Employment:</strong></p>\n<p>Security Requirement(s): reliability</p>\n<p><strong><br>Operational Requirements:</strong></p>\n<ul>\n<li>Ability and willingness to work overtime on short notice.</li>\n</ul>\n<p><strong><br>Applications:</strong></p>\n<p>Interested AAFC employees in the NCR are encouraged to forward their r\u00e9sum\u00e9 and cover letter to Tania Gardner at <a href=\"mailto:aafc.fcd-dcf.aac@canada.ca\">aafc.fcd-dcf.aac@canada.ca</a> by December 18, 2019.</p>\n<p>R\u00e9sum\u00e9s must clearly identify:</p>\n<ul>\n<li>Your tenure (e.g. term or indeterminate)</li>\n<li>Your substantive group and level</li>\n</ul>\n<p><strong>Notes:</strong></p>\n<ul>\n<li>The opportunity is for an approximately 12-month assignment at level or acting opportunity, starting in February 2020.</li>\n<li>The Public Service of Canada is committed to developing inclusive, barrier-free selection and appointment processes and work environments. If contacted in relation to this process, please advise the organization's representative of your need for accommodation measures which must be taken to enable you to be assessed in a fair and equitable manner.</li>\n<li>Communication for this process will be sent via email.</li>\n<li>A pool of qualified candidates may be established and may be used to staff similar positions on an acting, assignment or deployment basis.</li>\n<li>Assessment of applicants will be based on the review of their r\u00e9sum\u00e9, an interview and reference checks.</li>\n</ul>\n<p>\u00a0</p>\n",
        "fr": "\n<p><strong><br>Fonctions :</strong></p>\n<p>Le gestionnaire de la Section des comptes recevables et de la gestion des revenus (SCRGR) rel\u00e8ve du directeur des Services financiers, syst\u00e8mes et comptes recevables et dirige une unit\u00e9 dynamique compos\u00e9e de deux \u00e9quipes. La premi\u00e8re \u00e9quipe g\u00e8re les fonctions d\u2019administration et de surveillance des revenus et des comptes recevables tandis que la deuxi\u00e8me g\u00e8re les activit\u00e9s de recouvrement li\u00e9es \u00e0 plusieurs types de comptes recevables : comptes clients en d\u00e9faut (Division des programmes de garanties financi\u00e8res), cr\u00e9ances li\u00e9es aux trop-pay\u00e9s des programmes (Division des programmes du revenu agricole), cr\u00e9ances li\u00e9es aux remboursements (contributions remboursables inconditionnelles et contributions remboursables conditionnelles), comptes d\u00e9biteurs des employ\u00e9s et comptes d\u00e9biteurs des collaborateurs.</p>\n<p>Responsabilit\u00e9s principales du poste :</p>\n<ul>\n<li>G\u00e9rer deux \u00e9quipes : \u00e9quipe de l\u2019administration et de la surveillance (6 employ\u00e9s \u2013 FI, CR, \u00e9tudiants) et \u00e9quipe des recouvrements (13 employ\u00e9s \u2013 PM, AS).</li>\n<li>G\u00e9rer un budget et exercer des pouvoirs financiers d\u00e9l\u00e9gu\u00e9s (articles 32 et 34).</li>\n<li>G\u00e9rer le processus trimestriel de radiation de dettes du Minist\u00e8re.</li>\n<li>Surveiller l\u2019exactitude des renseignements financiers dans le syst\u00e8me de gestion des recouvrements, le module Ventes et distribution / comptes d\u00e9biteurs (VD) du SAP et le Syst\u00e8me de feuillets de renseignements fiscaux (SFRF).</li>\n<li>Entreprendre ou appuyer des projets pour r\u00e9aliser les objectifs globaux du Minist\u00e8re ou de l\u2019Unit\u00e9 des recouvrements (projets pilotes pangouvernementaux, r\u00e9duction de la paperasse, approbations \u00e9lectroniques, etc.).</li>\n<li>Mener diverses activit\u00e9s ponctuelles li\u00e9es \u00e0 la fin de l\u2019exercice, \u00e0 la r\u00e9solution des probl\u00e8mes de syst\u00e8mes, \u00e0 la pr\u00e9paration de rapports, au maintien de relations avec la client\u00e8le, etc.</li>\n</ul>\n<p><strong><br>\u00c9noncer des crit\u00e8res de m\u00e9rite et conditions d\u2019emploi : </strong></p>\n<p><strong><br>Qualifications essentielles </strong></p>\n<p>\u00c9tudes:</p>\n<p>Un grade d\u2019un \u00e9tablissement d\u2019enseignement postsecondaire reconnu avec sp\u00e9cialisation en comptabilit\u00e9, en finances, en administration des affaires, en commerce ou en \u00e9conomie</p>\n<p>ET</p>\n<p>Exp\u00e9rience li\u00e9e aux postes du groupe Gestion financi\u00e8re;</p>\n<p>OU</p>\n<p>Poss\u00e9der un titre comptable professionnel reconnu (c.-\u00e0-d. CPA, CA, CMA ou CGA).</p>\n<p><br>Exp\u00e9rience :</p>\n<ul>\n<li>Exp\u00e9rience appr\u00e9ciable et r\u00e9cente* dans au moins un (1) des domaines de gestion financi\u00e8re suivants :\n\t\n<ul style=\"list-style-type: circle;\">\n<li>Planification financi\u00e8re</li>\n<li>Production de rapports externes</li>\n<li>Gestion des ressources internes</li>\n<li>Services consultatifs de gestion financi\u00e8re</li>\n<li>Op\u00e9rations comptables</li>\n<li>Syst\u00e8mes financiers</li>\n<li>Politique financi\u00e8re</li>\n<li>\u00c9tablissement des co\u00fbts</li>\n<li>Attestation</li>\n<li>Contr\u00f4les internes</li>\n<li>Rapports int\u00e9gr\u00e9s</li>\n<li>Comptes clients et recouvrements</li>\n</ul>\n</li>\n<li>Exp\u00e9rience de la r\u00e9alisation d\u2019analyses strat\u00e9giques et de la pr\u00e9sentation de recommandations \u00e0 des cadres sup\u00e9rieurs** concernant des questions de gestion financi\u00e8re.</li>\n<li>Exp\u00e9rience de la gestion de partenariats avec des clients et des intervenants importants du gouvernement f\u00e9d\u00e9ral.</li>\n<li>Exp\u00e9rience de l'appui \u00e0 la mise en \u0153uvre d'initiatives du gouvernement f\u00e9d\u00e9ral.</li>\n<li>Exp\u00e9rience de la gestion des ressources humaines.</li>\n</ul>\n<p>*On entend par exp\u00e9rience appr\u00e9ciable et r\u00e9cente\u00a0 une exp\u00e9rience dont l\u2019\u00e9tendue et la richesse sont associ\u00e9es \u00e0 des fonctions de FI-03 ou niveau sup\u00e9rieur (ou groupe et\u00a0 niveau \u00e9quivalents) au gouvernement f\u00e9d\u00e9ral du Canada pendant une p\u00e9riode d\u2019au moins cinq (5) ans au cours des sept (7) derni\u00e8res ann\u00e9es, dans le cadre des t\u00e2ches exerc\u00e9es par l\u2019employ\u00e9.\u00a0</p>\n<p>**On entend par cadres sup\u00e9rieurs des sous-ministres adjoints (SMA), des directeurs g\u00e9n\u00e9raux et(ou) des directeurs.</p>\n<p><br>Connaissances :</p>\n<ul>\n<li>Connaissance des priorit\u00e9s du gouvernement f\u00e9d\u00e9ral et de leur incidence sur le mandat, les politiques et les programmes du Minist\u00e8re.</li>\n<li>Connaissance des d\u00e9fis ayant une incidence sur le personnel des finances du gouvernement f\u00e9d\u00e9ral.</li>\n<li>Connaissance des politiques et des directives de gestion financi\u00e8re du Secr\u00e9tariat du Conseil du Tr\u00e9sor du Canada.</li>\n<li>Connaissance de cycle de planification budg\u00e9taire du gouvernement f\u00e9d\u00e9ral.</li>\n</ul>\n<p><br>Capacit\u00e9s :</p>\n<ul>\n<li>Capacit\u00e9 de s\u2019ajuster aux changements de priorit\u00e9s et de respecter les \u00e9ch\u00e9ances.</li>\n<li>Capacit\u00e9 de communiquer efficacement oralement.</li>\n<li>Capacit\u00e9 de communiquer efficacement\u00a0 par \u00e9crit.</li>\n<li>Capacit\u00e9 de planifier et d\u2019organiser son travail et le travail d\u2019une \u00e9quipe.</li>\n</ul>\n<p><br>Comp\u00e9tences :</p>\n<ul>\n<li>Cr\u00e9er une vision et une strat\u00e9gie</li>\n<li>Mobiliser les personnes</li>\n<li>Pr\u00e9server l\u2019int\u00e9grit\u00e9 et le respect</li>\n<li>Collaborer avec les partenaires et les intervenants</li>\n<li>Promouvoir l\u2019innovation et orienter le changement</li>\n<li>Obtenir des r\u00e9sultats</li>\n</ul>\n<p><br>Exigence linguistique :</p>\n<ul>\n<li>Bilingue imp\u00e9ratif (CBC/CBC).</li>\n</ul>\n<p><strong><br>Qualifications constituant un atout :</strong></p>\n<ul>\n<li>Poss\u00e9der un titre comptable professionnel reconnu (c.\u00e0-d. CPA, CA, CMA ou CGA).</li>\n</ul>\n<p><strong><br>Besoins de l'organisation :</strong></p>\n<p>Pour r\u00e9pondre aux besoins de diversification de l\u2019effectif, la pr\u00e9f\u00e9rence pourrait \u00eatre accord\u00e9e aux personnes qui auront indiqu\u00e9 leur appartenance \u00e0 l\u2019un des groupes suivants, vis\u00e9s par l\u2019\u00e9quit\u00e9 en mati\u00e8re d\u2019emploi : Autochtones, personnes handicap\u00e9es, membres d\u2019une minorit\u00e9 visible, femmes.</p>\n<p><strong><br>Conditions d'emploi :</strong></p>\n<p>Exigences de s\u00e9curit\u00e9(s) : Cote de fiabilit\u00e9</p>\n<p><strong><br>Exigences op\u00e9rationnelles :</strong></p>\n<ul>\n<li>\u00catre apte et dispos\u00e9 \u00e0 faire des heures suppl\u00e9mentaires \u00e0 bref pr\u00e9avis.</li>\n</ul>\n<p><strong><br>Candidatures</strong></p>\n<p>Les employ\u00e9s d\u2019AAC de la RCN int\u00e9ress\u00e9s sont invit\u00e9s \u00e0 envoyer leur curriculum vitae et une lettre d\u2019accompagnement \u00e0 Tania Gardner \u00e0 <a href=\"mailto:aafc.fcd-dcf.aac@canada.ca\">aafc.fcd-dcf.aac@canada.ca</a> d\u2019ici le 18 d\u00e9cembre 2019.</p>\n<p>Les curriculum vitae doivent nettement pr\u00e9ciser :</p>\n<ul>\n<li>votre type de poste (p. ex., \u00e0 dur\u00e9e d\u00e9termin\u00e9e ou ind\u00e9termin\u00e9e);</li>\n<li>la classification de votre poste d\u2019attache.</li>\n</ul>\n<p><strong><br>Remarques</strong></p>\n<ul>\n<li>Il s\u2019agit d\u2019une affectation d\u2019environ 12 mois au niveau ou d\u2019une nomination int\u00e9rimaire, \u00e0 compter de f\u00e9vrier 2020.</li>\n<li>La fonction publique du Canada s'est aussi engag\u00e9e \u00e0 instaurer des processus de s\u00e9lection et un milieu de travail inclusifs et exempts d'obstacles. Si l'on communique avec vous au sujet d'une possibilit\u00e9 d'emploi ou pour des examens, veuillez faire part au repr\u00e9sentant du minist\u00e8re, en temps opportun, de vos besoins pour lesquels des mesures d'adaptation doivent \u00eatre prises pour vous permettre une \u00e9valuation juste et \u00e9quitable.</li>\n<li>Les communications relatives au\u00a0 processus de s\u00e9lection se feront par courriel.</li>\n<li>Un bassin de candidats qualifi\u00e9s pourra \u00eatre cr\u00e9\u00e9 et pourra servir \u00e0 doter des postes semblables sur une base int\u00e9rimaire, d\u2019affectation ou de mutation.<em> </em></li>\n<li>L'\u00e9valuation des candidats sera bas\u00e9e sur l'examen de leur r\u00e9sum\u00e9, une entrevue et de la v\u00e9rification des r\u00e9f\u00e9rences.</li>\n</ul>\n<p>\u00a0</p>\n\n"
    },
    "breadcrumb": {
        "en": "Manager",
        "fr": "Gestionnaire"
    },
    "empl": {
        "type": {
            "en": "Assignment, Acting",
            "fr": "Affectation, Int\u00e9rimaire"
        },
        "classification": {
            "en": "FI-4",
            "fr": "FI-4"
        },
        "branch": {
            "en": "Corporate Management Branch",
            "fr": "Gestion int\u00e9gr\u00e9e"
        },
        "locations": {
            "en": "Ottawa, Ontario",
            "fr": "Ottawa, Ontario"
        },
        "date_closing": {
            "en": "2019-12-18",
            "fr": "2019-12-18"
        },
        "open_to": {
            "en": "All AAFC Employees in the National Capital Region (NCR)",
            "fr": "Tous les employ\u00e9s d\u2019AAC dans la r\u00e9gion de la capitale nationale (RCN)"
        },
        "name": {
            "en": "Tania Gardner",
            "fr": "Tania Gardner"
        },
        "email": {
            "en": "aafc.fcd-dcf.aac@canada.ca",
            "fr": "aafc.fcd-dcf.aac@canada.ca"
        }
    }
}