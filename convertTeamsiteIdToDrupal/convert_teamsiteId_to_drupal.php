<?php

global $argv;

$myargs = array_slice($argv, 3);
use Drush\Drush;
use Drupal\node\Entity\Node;
include "db_operations.php";
global $import_override;
$import_override = TRUE;


if (empty($myargs)) {
  echo "Syntax: drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL\n";
  echo "OR      drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php HARD_LAUNCH\n";
  echo "The FULL_MEAL_DEAL flag processes all available nodes.\n";
  echo "The HARD_LAUNCH flag currently hard coded for doing nodes newer than Oct-11-2020. Also skips blocks which we have already done by this point.\n";
  exit;
}

$import_phase = array_shift($myargs);
echo "Import phase chosen=$import_phase\n";
sleep(2);
if ($import_phase != 'FULL_MEAL_DEAL' && $import_phase != 'HARD_LAUNCH') {
  echo "Syntax: drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL\n";
  echo "OR      drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php HARD_LAUNCH\n";
  echo "The FULL_MEAL_DEAL flag processes all available nodes.\n";
  echo "The HARD_LAUNCH flag currently hard coded for doing nodes newer than Oct-11-2020. Also skips blocks which we have already done by this point.\n";
  exit;
}

$nodeId;


$db = new DBOperations();
$countNode = 0;

$count = 0;

//process menu_link URLs in Footer
$storage = \Drupal::entityManager()->getStorage('menu_link_content');
$menu_links = $storage->loadByProperties(['menu_name' => "Footer"]);
processMenuLink($menu_links);

$menu_links_fr = $storage->loadByProperties(['menu_name' => "footer-fr"]);

processMenuLink($menu_links_fr);

//finished process footer


//$uuids = ['8c43040c-9e83-4f13-b774-f4771b2a5a8b', '49baecda-63c8-4e16-811b-be2ff38cea33'];

$uuids = ['2ae71642-ca4f-4e8a-9f43-588ae3e110f9',
          '30e4c971-edb5-48f2-9f12-86589e46b849',
          '41c877fc-4f51-422b-821c-1f23452edb93',
          '49baecda-63c8-4e16-811b-be2ff38cea33',
          '78aca85d-3afc-4257-902b-144ae0d874e1',
          '878c1db8-a3da-452d-8479-8a1c1f740d6e',
          '8c43040c-9e83-4f13-b774-f4771b2a5a8b',
          '9cc4d184-ead7-4997-9da1-fc10b12ab2af',
          'a499786a-3b20-4157-89a6-2d3a0a0edf06',
          'b1130f0a-f3ae-4dab-b765-4ffc5467d499',
          'b290c5dc-03ed-4f0b-bcf3-fa489258543c',
          'b382751d-0591-4714-8dc8-506d8d8aa343',
          'be4d3824-c613-4aef-8992-42ba10131caf'];

//$uuids = ['49baecda-63c8-4e16-811b-be2ff38cea33']; // This one doesn't seem to work.
foreach ($uuids as $uuid) {
  $block_content = \Drupal::service('entity.repository')->loadEntityByUuid('block_content', $uuid);
  if ($block_content) {
    $body = $block_content->get('body')->value;
    $body = updateInternalReference($body);
    // $body = html_entity_decode($body);
    Drush::output()->writeln($body);
    $block_content->body->value = $body;
    $block_content->setSyncing(TRUE);
    if ($import_phase == 'FULL_MEAL_DEAL') {
      $block_content->save();
    }
    if ($block_content->hasTranslation('fr')) {
      $block_content_fr = $block_content->getTranslation('fr');
      $body_fr = $block_content_fr->get('body')->value;
      $body_fr = updateInternalReference($body_fr);
      // $body_fr = html_entity_decode($body_fr);
      $block_content_fr->body->value = $body_fr;
      $block_content_fr->setSyncing(TRUE);
      if ($import_phase == 'FULL_MEAL_DEAL') {
        $block_content_fr->save();
      }
    }
  }
  $block_content = NULL;
  Drush::output()->writeln('Process block body field for uuid=' . $uuid);
}
// Done with blocks, now start NODES.


if ($import_phase == 'FULL_MEAL_DEAL') {
  $result = $db->getAllNodeIdFromNodeTable();
}
elseif ($import_phase == 'HARD_LAUNCH') {
  $result = $db->getHardlaunchNewsEmplFromTables();// HARD LAUNCH.
}
foreach ($result as $row) {
  global $nodeId;
  $nodeId = $row->nid;
  //echo $nodeId . "\n";

  if(empty($nodeId))
    continue;
  //echo $nodeId . "\n";
  $node = Node::load($nodeId);

  $nodeValueEn = $node->body->value;
  $node->body->value = updateInternalReference($nodeValueEn);

/*
// DEBUG SECTION.
  $str = <<<'JOSEPH'
; Safety</a></li>
<li><a href="http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=102720306&amp;lang=eng" target="_blank">Manager's Guide on Mental Health in the Workplace (PDF)</a></li>
<li><a href="http://www.mentalhealthcommission.ca/English">Mental Health Commission of Canada</a></li>
<li><a href="http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=103780595&amp;lang=eng" target="_blank">Mental Health Handbook (PDF)</a> - AAFC</li>
<li><a href="http://mentalhealthworks.ca/">Mental Health Works</a></li>
<li>Mental Wellness Champion - <a href="?id=1580246566002">  "blahs" One in five (Video)</a></li>
<li><a href="http://www.conferenceboard.ca/e-Library/abstract.aspx?did=1433">What You Need to Know About Mental Health: A Tool for Managers - Conference Board of Canada</a></li>\n<li><a href="http://wfmh.com/">World Federation for Mental Health</a></li>\n<li><a href="http://www.who.int/topics/mental_health/en/">World Health Organization\u00a0- Mental Health</a></li>\n<li><a href="https://www.canada.ca/en/treasury-board-secretariat/topics/healthy-workplace/mental-health-workplace.html">Centre of Expertise on Mental Health in the Workplace</a> \u2013 TBS</li>\n</ul>\n</li>\n</ul>
<h3 id="b2">Integrated Disability Management</h3>
<ul>\n<li><a href="http://www.chrc-ccdp.gc.ca/eng/content/guide-managing-return-work">Guide for Managing the Return to Work</a>\u00a0- Canadian Human Rights Commission (CHRC)</li>\n<li><a href="https://www.canada.ca/en/employment-social-development/programs/accessible-people-disabilities.html">Office of Disability Issues</a>\u00a0- HRSDC</li>\n</ul>
<h3 id="b3">Workplace Injury or Illness</h3>
<ul>\n<li><a href="http://www.labour.gc.ca/eng/health_safety/compensation/pubs_wc/geca.shtml">Employers' Guide to the <cite>Government Employees Compensation Act</cite></a>\u00a0- HRSDC</li>\n<li><a href="http://www.awcbc.org/en/reportinganinjury.asp">Reporting an Injury - Links to Workers' Compensation Board forms</a></li>\n<li><a href="http://www.hrsdc.gc.ca/eng/labour/workers_compensation/index.shtml">Workers' Compensation</a>\u00a0- HRSDC</li>\n</ul>
<h3 id="b4">Non-Occupational Injury or Illness</h3>
<ul>\n    <li><a href="https://www.canada.ca/en/treasury-board-secretariat/topics/benefit-plans/plans/disability-insurance-plan.html">Disability  Insurance Plan</a></li>\n</ul>
<h3 id="b5">Accessibility and Workplace Accommodation</h3>
<ul>\n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=12044&amp;section=text">Accessibility Standard for Real Property</a>\u00a0- TBS</li>\n<li><a href="http://www.ccrw.org/">Canadian Council of Rehabilitation and Work</a></li>\n<li><a href="http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100099888&amp;lang=eng" target="_blank">Duty to Accommodate: Workplace Accommodation Guidelines (Word)</a>\u00a0- AAFC</li>\n<li><a href="http://www.psc-cfp.gc.ca/plcy-pltq/guides/assessment-evaluation/apwd-eph/index-eng.htm">Guide for Assessing Persons with Disabilities - How to determine and implement  assessment accommodations</a>\u00a0- Public Service Commission</li>\n<li><a href="http://www.chrc-ccdp.gc.ca/eng/content/guide-balancing-work-and-caregiving-obligations">Guide to Balancing Work and Caregiving Obligations</a>\u00a0- CHRC</li>\n<li><a href="http://www.chrc-ccdp.gc.ca/eng/content/place-all-guide-creating-inclusive-workplace">Guide to Creating an Inclusive Workplace</a>\u00a0- CHRC</li>\n<li><a href="http://www.hrsdc.gc.ca/eng/disability/arc/inclusive_meetings.shtml">Guide to Planning Inclusive Meetings</a>\u00a0- HRSDC</li>\n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=23601">Standard on Web Accessibility</a>\u00a0- TBS</li>\n</ul>
<h3 id="b6">Legislative Instruments</h3>
<ul>\n<li><a href="http://laws-lois.justice.gc.ca/eng/const/page-15.html">Canadian Charter of Rights and Freedoms</a></li>\n<li><a href="http://laws-lois.justice.gc.ca/eng/acts/h-6/"><cite>Canadian Human Rights Act</cite></a></li>\n<li><a href="http://laws-lois.justice.gc.ca/eng/acts/e-5.401/index.html"><cite>Employment Equity Act</cite></a></li>\n<li><a href="http://laws-lois.justice.gc.ca/eng/acts/G-5/index.html"><cite>Government Employees Compensation Act</cite></a></li>\n<li><a href="http://laws-lois.justice.gc.ca/eng/regulations/SOR-2005-334/"><cite>Public Service Employment Regulations</cite></a></li>\n</ul>
<h3 id="b7">Directives and Policies</h3>
<ul>\n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=15774&amp;section=text">Directive on Leave and Special Working Arrangements</a>\u00a0- TBS</li>\n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=12541">Duty to Accommodate Persons with Disabilities - Policy</a>\u00a0- TBS</li>\n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=12542&amp;section=text">Employee and Family Assistance Program - Policy</a>\u00a0- TBS</li>\n<li><a href="http://publiservice.tbs-sct.gc.ca/pol/doc-eng.aspx?id=12139&amp;section=text">Injury-on-Duty Leave - Policy</a>\u00a0- TBS</li>\n<li><a href="https://www.tbs-sct.gc.ca/hidb-bdih/plan-eng.aspx?Org=0&amp;Hi=132&amp;Pl=650">Workplace Wellness and Productivity Strategy (TBS)</a></li>\n</ul>
</section><section>
<section class="alert alert-info">
<h2>Renseignements \u00e0 l\u2019intention des employ\u00e9s</h2>
<p><a href="http://multimedia.agr.gc.ca/COVID-19/index-fra.html">Maladie \u00e0 coronavirus (COVID-19)</a></p>\n</section>
<div class="alert alert-info mrgn-tp-sm">\n<p><a href="http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=103781799&amp;lang=fra" target="_blank">Le Guide sur la sant\u00e9 mentale</a> d'Agriculture et Agroalimentaire Canada est maintenant disponible! Ce guide pratique qui peut servir \u00e0 favoriser la sant\u00e9 mentale et le mieux-\u00eatre s'adresse \u00e0 tous les employ\u00e9s d'AAC.</p></div>
<p>Agriculture et Agroalimentaire Canada (AAC) entend promouvoir la sant\u00e9 et le mieux-\u00eatre des employ\u00e9s. Le Minist\u00e8re reconna\u00eet que la sant\u00e9 et le mieux-\u00eatre d'un employ\u00e9 peuvent \u00eatre touch\u00e9s par des probl\u00e8mes personnels de tous genres qui, faute d'\u00eatre r\u00e9gl\u00e9s, peuvent avoir des cons\u00e9quences nuisibles sur le rendement au travail et dans la vie de tous les jours.</p>
<p>La gestion int\u00e9gr\u00e9e de l'incapacit\u00e9 consiste \u00e0 g\u00e9rer les absences du travail pour raison de sant\u00e9, ainsi que les risques qui causent ces absences afin de raccourcir leur dur\u00e9e ou les pr\u00e9venir, d'am\u00e9liorer la productivit\u00e9 du personnel et de r\u00e9duire les co\u00fbts. Elle vise les maladies et les blessures des employ\u00e9s, tant professionnelles que non professionnelles.</p>
<p>La gestion int\u00e9gr\u00e9e de l'incapacit\u00e9 comporte trois composantes\u00a0:</p>
<ol>\n<li>la pr\u00e9vention des maladies et des blessures des employ\u00e9s;</li>  \n<li>le soutien aux employ\u00e9s en cas de maladie ou d'incapacit\u00e9 afin qu'ils restent au travail ou y retournent d\u00e8s qu'ils peuvent le faire en toute s\u00e9curit\u00e9;</li>\n<li>la prise de mesures d'adaptation pour les employ\u00e9s ayant une incapacit\u00e9 (temporairement et de fa\u00e7on permanente)</li>\n</ol>
<nav>\n<p><b>Sur cette page\u00a0:</b></p>
<ul>\n <li>Services\n  <ul>\n   <li class="mrgn-tp-md"><a href="#a1">Services internes d'ergonomie d'Agriculture et Agroalimentaire Canada</a></li> \n   <li><a href="#a2">Programme d'accessibilit\u00e9, d'adaptations et de technologie informatique adapt\u00e9e</a></li>\n   <li><a href="#a3">Obligation de prendre des mesures d'adaptation et am\u00e9nagement en milieu de travail</a></li> \n   <li><a href="#a4">Programme d'aide aux employ\u00e9s et \u00e0 la famille</a></li> \n   <li><a href="#a5">Proc\u00e9dures pour les accidents du travail</a></li> \n   <li><a href="#a6">Services de r\u00e9solution int\u00e9gr\u00e9s</a></li> \n   <li><a href="#a7">Relations en milieu de travail</a></li> \n  </ul>\n </li>\n <li class="mrgn-tp-md">Ressources\n  <ul>\n   <li class="mrgn-tp-md"><a href="#b1">Mieux-\u00eatre</a></li> \n   <li><a href="#b2">Gestion int\u00e9gr\u00e9e de l'incapacit\u00e9</a></li> \n   <li><a href="#b3">Accident du travail ou maladie professionnelle</a></li> \n   <li><a href="#b4">Maladie ou accident non li\u00e9 au travail</a></li> \n   <li><a href="#b5">Accessibilit\u00e9 et mesures d'adaptation au travail</a></li> \n   <li><a href="#b6">Instruments l\u00e9gislatifs</a></li> \n   <li><a href="#b7">Directives et politiques</a></li>\n  </ul>\n </li>\n</ul>\n</nav>
<h2>Services</h2>
<h3 id="a1">Services internes d'ergonomie d'Agriculture et Agroalimentaire Canada</h3>
<p>Les <a href="display-afficher.do?id=1343332159322&amp;lang=fra#test-1">services internes d'ergonomie</a> incluent\u00a0:</p>
<ul>    \n<li><a href="display-afficher.do?id=1343395757527&amp;lang=fra#test-1">\u00c9valuation des mesures d'adaptation en milieu de travail\u00a0: \u00c9valuation ergonomique</a></li>\n<li><a href="display-afficher.do?id=1343394570727&amp;lang=fra#test-1">S\u00e9ances adapt\u00e9es en ergonomie</a></li>\n</ul>
<h3 id="a2">Le programme d'accessibilit\u00e9, d'adaptations et de technologie informatique adapt\u00e9e</h3>
<ul>\n<li>Le programme d'accessibilit\u00e9, d'adaptations et de technologie informatique adapt\u00e9e (Services partag\u00e9s Canada) (AATIA) fournit des \u00e9valuations des besoins en mati\u00e8re de technologie informatique adapt\u00e9e d'une personne afin de d\u00e9terminer l'appariement appropri\u00e9 entre l'employ\u00e9 et les adaptations de mat\u00e9riel ou de logiciels.</li>\n<li>Pour demander une \u00e9valuation des mesures d'adaptation au travail du programme AATIA, envoyez un courriel \u00e0 l'adresse <a href="mailto:aafc.accommodatedutyto-adaptationmesures.aac@canada.ca">aafc.accommodatedutyto-adaptationmesures.aac@canada.ca</a>.</li>\n</ul>
<h3 id="a3">Obligation de prendre des mesures d'adaptation et mise en \u0153uvre des mesures d'adaptation en milieu de travail</h3>
<p>L'obligation de prendre des mesures d'adaptation est une obligation juridique qui exige express\u00e9ment que l'employeur prenne des mesures d'adaptation pour emp\u00eacher toute discrimination et \u00e9liminer les d\u00e9savantages subis par des employ\u00e9s.</p>
n<p>Vous pouvez en apprendre davantage sur l'<a href="display-afficher.do?id=1462362082190&amp;lang=fra">obligation de prendre des mesures d'adaptation et la mise en \u0153uvre des mesures d'adaptation en milieu de travail</a>, notamment sur la formation pour aider les gestionnaires et les superviseurs \u00e0 comprendre leurs r\u00f4les et responsabilit\u00e9s.</p>
n<h3 id="a4">Programme d'aide aux employ\u00e9s et \u00e0 la famille</h3>
n<p>Le <a href="display-afficher.do?id=1287673689033&amp;lang=fra">Programme d'aide aux employ\u00e9s et \u00e0 la famille</a> offre des conseils confidentiels, des conseils d'experts, du soutien et des ressources aux personnes qui \u00e9prouvent des probl\u00e8mes personnels ou professionnels.</p>
<h3 id="a5">Proc\u00e9dures pour les accidents du travail</h3>
<p>Les employ\u00e9s qui sont victimes d'un accident du travail ou d'une maladie professionnelle ont droit \u00e0 un remplacement du salaire et \u00e0 une couverture de frais m\u00e9dicaux par l'entremise de la  <a href="http://www.cchst.ca/oshanswers/information/wcb_canada.html">commission des accidents du travail</a> (CAT) de leur province. Tout incident au travail ayant entra\u00een\u00e9 une perte de temps ou n\u00e9cessit\u00e9 l'obtention de soins m\u00e9dicaux doit \u00eatre d\u00e9clar\u00e9 dans un d\u00e9lai de trois jours. Si vous ou l'un de vos employ\u00e9s avez subi un accident du travail ou souffrez d'une maladie professionnelle, consultez les <a href="?id=1321562720661#inj-test-test-test">Proc\u00e9dures pour les accidents du travail</a>.</p>
<h3 id="a6">Services de r\u00e9solution

 <a href="?id=1321562720661">int\u00e9gr\u00e9s</h3>
<p>L'\u00e9quipe des <a href="display-afficher.do?id=1288027396522&amp;lang=fra">Services de r\u00e9solution int\u00e9gr\u00e9s</a> offre des services, tels l'accompagnement en situation de conflits, des discussions dirig\u00e9es, des s\u00e9ances de m\u00e9diation et de la formation sur le r\u00e8glement efficace des probl\u00e8mes.</p>
n<h3 id="a7">Relations en milieu de travail</h3>
<p>Le mandat des conseillers en relations de travail de l'\u00e9quipe des <a href="display-afficher.do?id=1288027875377&amp;lang=fra">Relations en milieu de travail</a> est d'aider les superviseurs et les gestionnaires avec toute question ou tout probl\u00e8me concernant les mesures d'adaptation.</p>
<h2>Ressources</h2>
<h3 id="b1">Mieux-\u00eatre</h3>
<ul>\n<li><a href="http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1744020&amp;lang=fra" target="_blank">Bo\u00eete \u00e0 outils pour la promotion d'un milieu de travail sans parfum (Word)</a></li>\n<li><a href="http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=2991794&amp;lang=fra" target="_blank">Politique de pr\u00e9vention de la violence dans le lieu de travail (Word)</a>\u00a0- AAC</li> \n<li><a href="display-afficher.do?id=1287673689033&amp;lang=fra">Programme d'aide aux employ\u00e9s et \u00e0 la famille</a></li>\n<li><a href="http://www.hc-sc.gc.ca/hl-vs/index-fra.php">Vie saine</a>\u00a0- Sant\u00e9 Canada (SC)</li>\n<li>Sant\u00e9 mentale
n<ul>\n<li class="mrgn-tp-md"><a href="http://www.conferenceboard.ca/e-library/abstract.aspx?did=1594">Ce que vous devez savoir sur la sant\u00e9 mentale\u00a0: Un outil pour les gestionnaires</a></li>\n<li><a href="https://www.canada.ca/fr/secretariat-conseil-tresor/sujets/travail-sain/sante-mentale-travail.html">Centre d'expertise pour la sant\u00e9 mentale en milieu de travail</a> - SCT</li>\n<li><a href="http://www.mentalhealthcommission.ca/Francais">Commission de la sant\u00e9 mentale du Canada</a></li>\n<li><a href="http://wfmh.com/">F\u00e9d\u00e9ration mondiale pour la sant\u00e9 mentale\u00a0: La sant\u00e9 mentale et les maladies physiques chroniques (en anglais seulement)</a></li>\n<li><a href="http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=102720303&amp;lang=fra" target="_blank">Guide du gestionnaire sur la sant\u00e9 mentale en milieu de travail (PDF)</a></li>\n<li><a href="http://www.mentalhealthworks.ca">La sant\u00e9 mentale au travail (en anglais seulement)</a></li>
<li>Championne de la sant\u00e9 mentale - <a href="display-afficher.do?id=1580246566002&amp;lang=fra">Un sur cinq (Vid\u00e9o)</a></li>
<li><a href="http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=103781799&amp;lang=fra" target="_blank">Guide sur la sant\u00e9 mentale (PDF)</a> - AAC</li>\n<li><a href="http://www.who.int/topics/mental_health/fr/">Organisation mondiale de la Sant\u00e9\u00a0- Sant\u00e9 mentale</a></li>\n<li><a href="http://www.chrc-ccdp.gc.ca/fra/content/politique-sur-les-mesures-d%E2%80%99adaptation-en-mati%C3%A8re-de-maladie-mentale-et-proc%C3%A9dures-connexes">Politique sur les mesures d'adaptation en mati\u00e8re de maladie mentale et proc\u00e9dures connexes de la Commission canadienne des droits de la personne</a></li>\n<li><a href="https://www.psmt.ca/">Prot\u00e9geons la sant\u00e9 mentale au travail\u00a0: Un guide pour la sant\u00e9 et la s\u00e9curit\u00e9 psychologiques au travail</a></li>\n</ul>\n</li>\n</ul>
<h3 id="b2">Gestion int\u00e9gr\u00e9e de l'incapacit\u00e9</h3>
<ul>\n<li><a href="http://www.tbs-sct.gc.ca/hrh/dmi-igi/index-fra.asp">Gestion de l'incapacit\u00e9 dans la fonction publique f\u00e9d\u00e9rale</a>\u00a0- SCT</li>\n<li><a href="http://www.tbs-sct.gc.ca/hrh/dmi-igi/tool-outil/intro-fra.asp">La gestion des cas d'incapacit\u00e9\u00a0: L'Outil</a>\u00a0- SCT</li>\n</ul>
<h3 id="b3">Accident du travail ou maladie professionnelle</h3>
<ul>\n<li><a href="http://www.travail.gc.ca/fra/sante_securite/indemnisation/pubs_idt/liae.shtml">Guide de l'employeur au sujet de la <cite>Loi sur l'indemnisation des agents de l'\u00c9tat</cite></a>\u00a0- RHDCC</li>\n<li><a href="http://www.rhdcc.gc.ca/fra/travail/indemnisation_travail/index.shtml">Indemnisation des accident\u00e9s du travail </a>\u00a0- RHDCC</li>\n<li><a href="http://awcbc.org/fr/?page_id=383">Rapport d'accident\u00a0- Liens vers les formulaires de la Commission des accidents du travail</a></li>\n</ul>
<h3 id="b4">Maladie ou accident non li\u00e9 au travail</h3>
<ul>\n <li><a href="https://www.canada.ca/fr/secretariat-conseil-tresor/sujets/regimes-assurance/regimes/regime-assurance-invalidite.html">R\u00e9gime d'assurance-invalidit\u00e9</a></li>\n</ul>
<h3 id="b5">Accessibilit\u00e9 et mesures d'adaptation au travail</h3>
<ul>\n<li><a href="https://www.ccrw.org/">Conseil canadien de la r\u00e9adaptation et du travail</a></li>\n<li><a href="http://www.tbs-sct.gc.ca/psm-fpfm/ve/dee/index-fra.asp">\u00c9tablir un milieu de travail accueillant pour les employ\u00e9s handicap\u00e9s</a>\u00a0- SCT</li>\n<li><a href="http://www.psc-cfp.gc.ca/plcy-pltq/guides/assessment-evaluation/apwd-eph/index-fra.htm">Guide relatif \u00e0 l'\u00e9valuation des personnes handicap\u00e9es\u00a0- Comment \u00e9tablir et mettre en place des mesures d'adaptation en mati\u00e8re d'\u00e9valuation</a>\u00a0- Commission de la fonction publique</li>\n<li><a href="http://www.chrc-ccdp.gc.ca/fra/content/guide-sur-la-conciliation-des-responsabilites-professionnelles-et-des-obligations-familiales">Guide sur la conciliation des responsabilit\u00e9s professionnelles et des obligations familiales des proches aidants</a>\u00a0- CCDP</li> \n<li><a href="http://www.tbs-sct.gc.ca/hrh/dmi-igi/tool-outil/acc-ada-fra.asp">La gestion des cas d'incapacit\u00e9\u00a0- Adaptation</a>\u00a0- SCT</li> \n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=12044&amp;section=text">Norme d'acc\u00e8s facile aux biens immobiliers</a>\u00a0- SCT</li>\n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=23601">Norme sur l'accessibilit\u00e9 des sites Web</a>\u00a0- SCT</li>\n<li><a href="http://www.tbs-sct.gc.ca/ee/dorf-fra.asp">Obligation de prendre des mesures d'adaptation\u00a0: D\u00e9marche g\u00e9n\u00e9rale \u00e0 l'intention des gestionnaires</a>\u00a0- SCT</li>\n<li><agridoc id="100255014">Obligation de prendre des mesures d'adaptation\u00a0: Lignes directrices sur les mesures d'adaptation au travail (Word)</agridoc>\u00a0- AAC</li>\n<li><a href="http://www.chrc-ccdp.gc.ca/fra/content/une-place-pour-tous-guide-pour-la-cr%C3%A9ation-dun-milieu-de-travail-inclusif">Une place pour tous\u00a0: Guide pour la cr\u00e9ation d'un milieu de travail inclusif</a>\u00a0- CCDP</li>\n</ul>
<h3 id="b6">Instruments l\u00e9gislatifs</h3>
<ul>\n<li><a href="http://laws-lois.justice.gc.ca/fra/const/page-15.html">Charte canadienne des droits et libert\u00e9s</a></li>\n<li><a href="http://laws-lois.justice.gc.ca/fra/lois/h-6/"><cite>Loi canadienne sur les droits de la personne</cite></a></li>\n<li><a href="http://laws-lois.justice.gc.ca/fra/lois/e-5.401/index.html"><cite>Loi sur l'\u00e9quit\u00e9 en mati\u00e8re d'emploi</cite></a></li>\n<li><a href="http://laws-lois.justice.gc.ca/fra/lois/G-5/index.html"><cite>Loi sur l'indemnisation des agents de l'\u00c9tat</cite></a></li>\n<li><a href="http://laws-lois.justice.gc.ca/fra/reglements/DORS-2005-334/"><cite>R\u00e8glement sur l'emploi dans la fonction publique</cite></a></li></ul>
<h3 id="b7">Directives et politiques</h3>
<ul>\n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=12139">Cong\u00e9 pour accident du travail\u00a0- Politique</a>\u00a0- SCT</li> \n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=15774&amp;section=text">Directive sur les cong\u00e9s et les modalit\u00e9s de travail sp\u00e9ciales</a>\u00a0- SCT</li>\n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=12541">Obligation de prendre des mesures d'adaptation pour les personnes handicap\u00e9es\u00a0- Politique</a>\u00a0- SCT</li> \n<li><a href="http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=12542&amp;section=text">Programme d'aide aux employ\u00e9s et \u00e0 la famille\u00a0- Politique</a>\u00a0- SCT</li>\n<li><a href="https://www.tbs-sct.gc.ca/hidb-bdih/plan-fra.aspx?Org=0&amp;Hi=132&amp;Pl=650">Strat\u00e9gie de mieux-\u00eatre au travail et de productivit\u00e9 (SCT)</a></li>\n</ul>
</section>
JOSEPH;

  echo updateInternalReference($str);
  echo "???";
  die;
// END OF DEBUG SECTION
*/

  $changed = $node->get('field_modified')->getValue();
  $changed = reset($changed);
  $changed = current($changed);
  $changed = (isset($changed) && !empty($changed)) ? strtotime($changed) : time();
  $node->changed = $changed;
  $node->setSyncing(TRUE);
  $node->save();

  //update French content
  if ($node->hasTranslation("fr")) {
    $trnode = $node->getTranslation('fr');
    $nodeValueFr = $trnode->body->value;
    $trnode->body->value = updateInternalReference($nodeValueFr);

    $trnode->changed = $changed;
    $trnode->setSyncing(TRUE);
    $trnode->save();
  }
  $count++;
}

$result = $db->getDistinctNodeIdFromNodeBody();


foreach ($result as $row) {

    echo $row->entity_id . "\n" ;
    $nodeBodys = $db->getNodeBodyByEntityId($row->entity_id);
    foreach ($nodeBodys as $nodeBody) {
        $page = $nodeBody->body_value;
       // echo "\n count = " . $count++;
        $nodeBody->body_value = updateInternalReference($page);
        // echo $nodeBody->body_value;
        $db->updateNodeBody($nodeBody);


    }

    //

}


echo "\n count = " . $count . "\n";

// $page = file_get_contents('/app/d8tools/convertTeamsiteIdToDrupal/testdata.txt');
// $page = updateInternalReference($page);
// echo $page . "\n";
//echo page;
function updateInternalReference($page){

    $matches = array();


    preg_match_all('<a href=\"(\?id=\S+.\d?)">', $page, $matches);
    $page = findAndReplace($matches, $page);


    preg_match_all('<a href=\"(display-afficher.do\?id=\S+.\d?)">', $page, $matches);
    $page = findAndReplace($matches, $page);

    preg_match_all('<a href=\"(.*?)(intranet.agr.gc.ca)(.*?)(\?id=\S+.\d?)">', $page, $matches);
    $page = findAndReplace($matches, $page);

    return $page;
}

//print_r ($matches);

//preg_match_all('<a href=\"http://intranet.agr.gc.ca\S+(\?id=\S+.\d?)">', $page, $matches);

//$page =  findAndReplace($matches, $page);
//print_r ($matches);



//echo $page;

function findAndReplace($matches, $page){
    foreach ($matches[0] as $val) {
        // echo "matched: " . $val[0] . "\n";
        // echo "part 1: " . $val[1] . "\n";
        echo $val . "\n" ;

        //skip if the link to AgriDoc
        if (stripos($val, "AgriDoc.do?") !== false) {
          echo "Agridoc ignor: " . $val . "\n" ;
          continue;
        }


        // $position = strpos($val, "?id=", 0);
        // //
        // $matchedId =  substr($val,$position+4, -1);
        // $position = strpos($matchedId, "&amp;lang", 0);
        // // echo $matchedId . "\n" ;
        // // echo $position . "\n" ;
        // $matchedAnchor = '';
        // $positionAnchor = strpos($matchedId, "#", 0);
        // if($positionAnchor>0)
        //   $matchedAnchor = substr($matchedId, $positionAnchor);
        // if($position>0)
        //   $matchedId = substr($matchedId, 0, $position);

        //if find match of pattern like "id=1580246566002", extract matchedId
        if(preg_match('/id=([0-9]+)/', $val, $matchedIDs)){

            // $matchedString = $matchedIDs[0][0];
            // $position = strpos($val, "?id=", 0);
          $matchedId = $matchedIDs[1];
          echo $matchedId . "\n" ;

          //get anchor value;
          $matchedAnchor = '';
          if(preg_match('/#(.*?)\"/', $val, $match)){
            $matchedAnchor = "#" . $match[1];
          }

          $node = findDrupalNodeIdByTeamsiteId($matchedId);
          print_r($node);

          if (!empty($node)) {
            echo "val = " . $val ."\n";
            echo                      "a data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\"" . "\n";
            $page = str_replace($val, "a data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\"", $page);
          } else {
            global $nodeId;
            echo " node_id = " . $nodeId;
            echo " dcr_id not exiting in node table = " . $matchedId ."\n";
          }
        }
    }
    return $page;
}

/*
 * search for Drupal id by using Teamsite Id after content is imported into Drupal
 */
function findDrupalNodeIdByTeamsiteId($param) {
    $db = new DBOperations();
    $node = $db->getNodeBydcrId($param);
    //TODO
    return $node;
}

function processMenuLink($menu_links){
  if (!empty($menu_links)) {
    foreach ($menu_links as $menu_link) {
      // $link = [];
      // //$link['type'] = 'menu_link';
      // $link['mlid'] = $menu_link->id->value;
      // //$link['plid'] = $menu_link->parent->value ?? '0';
      // $link['menu_name'] = $menu_link->menu_name->value;
      // $link['link_title'] = $menu_link->title->value;
      $link = $menu_link->link->uri;
      //$link['options'] = $menu_link->link->options;
      //$link['weight'] = $menu_link->weight->value;
      // print_r($link);

      if(startsWith($link ,"http://intranet.agr.gc.ca/agrisource")) {
        $position = strpos($link, "?id=", 0);
        //
        $matchedId =  substr($link,$position+4);
        $position = strpos($matchedId, "&amp;lang", 0);
        // echo $matchedId . "\n" ;
        // echo $position . "\n" ;
        if($position>0)
          $matchedId = substr($matchedId, 0, $position);
        $res = preg_replace("/[^0-9]/", "", $matchedId );

        echo $link . "\n" ;
        echo $matchedId . "\n" ;
        echo $res . "\n";

        $node = findDrupalNodeIdByTeamsiteId($matchedId);
        print_r($node);
        if (!empty($node)) {
          $menu_link->link->uri = 'entity:node/' . $node[0]->nid;
          $menu_link->save();
        }

        //handle translation
        // if (!$menu_link->hasTranslation('fr') && !empty($titleFr)) {
        //   $tr_menu_link = $menu_link->getTranslation('fr');
        //   $trlink = $tr_menu_link->link->uri;
        //   if(startsWith($trlink ,"http://intranet.agr.gc.ca/agrisource")) {
        //     $position = strpos($trlink, "?id=", 0);
        //     //
        //     $matchedId =  substr($trlink,$position+4);
        //     $position = strpos($matchedId, "&amp;lang", 0);
        //     // echo $matchedId . "\n" ;
        //     // echo $position . "\n" ;
        //     if($position>0)
        //       $matchedId = substr($matchedId, 0, $position);
        //     $res = preg_replace("/[^0-9]/", "", $matchedId );
        //
        //     echo $link . "\n" ;
        //     echo $matchedId . "\n" ;
        //     echo $res . "\n";
        //
        //     $node = findDrupalNodeIdByTeamsiteId($matchedId);
        //     print_r($node);
        //     if (!empty($node)) {
        //       $tr_menu_link->link->uri = 'entity:node/' . $node[0]->nid;
        //       $tr_menu_link->save();
        //     }
        //   }
        // }

      }
    }
  }
}


function startsWith ($string, $startString)
{
  $len = strlen($startString);
  return (substr($string, 0, $len) === $startString);
}

?>
