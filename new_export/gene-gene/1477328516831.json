{
    "dcr_id": "1477328516831",
    "lang": "en",
    "title": {
        "en": "Interacting With Lobbyists",
        "fr": "Interaction avec les lobbyistes "
    },
    "modified": "2018-12-04 00:00:00.0",
    "issued": "2016-10-26 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279035911647",
    "layout_name": "1 column",
    "created": "2018-12-04 10:29:04.0",
    "updated": "2018-12-04 10:29:04.0",
    "meta": {
        "issued": {
            "en": "2016-10-26",
            "fr": "2016-10-26"
        },
        "modified": {
            "en": "2018-12-04",
            "fr": "2018-12-04"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Interacting With Lobbyists",
            "fr": "Interaction avec les lobbyistes "
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>The purpose of the <a href=\"http://www.ocl-cal.gc.ca/eic/site/012.nsf/eng/h_00014.html\"><cite>Lobbyists' Code of Conduct</cite></a> is to assure Canadians that the lobbying of federal public servants is done ethically and according to the highest standards, so that public confidence and trust in the integrity of government decision-making is enhanced. It also ensures that we, as public servants, understand the behaviours we should expect from lobbyists.</p>\n\n<p>The Lobbyists' Code includes rules that provide greater clarity to lobbyists with respect to the use of information received from public servants, preferential access, political activities and the provision of gifts, favours or other benefits.</p>\n\n<h2>How Are You Implicated?</h2>\n\n<p>Some of the elements in the <cite>Lobbyists' Code of Conduct</cite> specifically implicate public servants:</p>\n\n   <ul>\n     <li><p><b>Rule 5: A lobbyist shall use and disclose information received from a public servant only in the manner consistent with the purpose for which it was shared. If a lobbyist obtains a government document they should not have, they shall neither use nor disclose it.</b></p>\n     <p>This rule addresses the issue of improper disclosure of information. Engagement with the private sector and other stakeholders is an essential part of Agriculture and Agri-Food Canada's (AAFC) operations. Such contact takes place every day and at many levels within our organization, and is necessary for the effective conduct of our business. We share information with stakeholders for many legitimate purposes. However, when information obtained from public servants is used improperly by third parties, the public interest may be adversely affected.</p>\n     <p>As such, prior to our interactions with lobbyists, it is important to clarify the purpose for which the information is being shared and inform them of any limitations or restrictions on how it can be shared.</p></li>\n     <li><p><b>Rule 10\u00a0- Gifts: To avoid the creation of a sense of obligation, a lobbyist shall not provide or promise a gift, favour, or other benefit to a public servant, whom they are lobbying or will lobby, which the public servant is not allowed to accept.</b></p>\n     <p>Lobbyists will be advised to check departmental Values and Ethics Codes to determine the acceptability of gifts or other benefits when interacting with public servants. The <a href=\"http://www.agr.gc.ca/eng/about-us/values-and-ethics-code-for-agriculture-and-agri-food-canada/?id=1448297869529\">Values and Ethics Code for Agriculture and Agri-Food Canada</a> is available to the public on our external website to help lobbyists comply with this rule.</p></li>\n    </ul>\n    \n<h2>Additional Information</h2>\n  \n<p>For more information regarding the application of the <cite>Lobbyists' Code of Conduct</cite>, please consult the <a href=\"http://www.ocl-cal.gc.ca/eic/site/012.nsf/eng/home\">Office of the Commissioner of Lobbying of Canada</a> website.</p>\n  \n<h2>Contact Us</h2>\n  \n<p>For questions in regards to the acceptability of gifts from stakeholders, please consult the <a href=\"http://www.agr.gc.ca/eng/about-us/values-and-ethics-code-for-agriculture-and-agri-food-canada/?id=1448297869529\">Values and Ethics Code for Agriculture and Agri-Food Canada</a> and <a href=\"display-afficher.do?id=1405693710539&amp;lang=eng\"> Questions and Answers: Conflicts of Interest</a>, or contact the Values and Ethics Policy Centre by email at <a href=\"mailto:aafc.valuesandethics-valeursetethique.aac@canada.ca\">aafc.valuesandethics-valeursetethique.aac@canada.ca</a> or by phone 1-866-894-5464 (toll-free).</p>\n\n\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Le <a href=\"http://www.ocl-cal.gc.ca/eic/site/012.nsf/fra/h_00014.html\"><cite>Code de d\u00e9ontologie des lobbyistes</cite></a> a pour objet de rassurer les Canadiens au sujet des normes d'\u00e9thique \u00e9lev\u00e9es que doivent respecter les lobbyistes de fa\u00e7on \u00e0 accro\u00eetre la confiance du public dans l'int\u00e9grit\u00e9 de la prise de d\u00e9cisions du gouvernement. Le Code fait \u00e9galement en sorte que nous, en tant que fonctionnaires, comprenions les comportements auxquels nous pouvons nous attendre de la part des lobbyistes.</p>\n\n<p>Le Code comprend maintenant des r\u00e8gles qui donnent aux lobbyistes davantage de pr\u00e9cisions concernant l'utilisation des renseignements re\u00e7us d'un fonctionnaire, l'acc\u00e8s pr\u00e9f\u00e9rentiel, les activit\u00e9s politiques et l'offre d'un cadeau, d'une faveur ou d'un autre avantage.</p>\n\n<h2>En quoi \u00eates-vous impliqu\u00e9 dans la situation?</h2>\n\n<p>Certains \u00e9l\u00e9ments du <cite>Code de d\u00e9ontologie des lobbyistes</cite> visent pr\u00e9cis\u00e9ment les fonctionnaires\u00a0:</p>\n\n   <ul>\n     <li><p><b>R\u00e8gle 5\u00a0: Un lobbyiste ne doit utiliser et divulguer des renseignements re\u00e7us d'un titulaire d'une charge publique que de fa\u00e7on conforme \u00e0 l'objectif pour lequel ils ont \u00e9t\u00e9 partag\u00e9s. Si un lobbyiste obtient un document du gouvernement qu'il ne devrait pas avoir en sa possession, il ne doit ni l'utiliser ni le divulguer.</b></p>\n     <p>Cette r\u00e8gle concerne la question de la communication irr\u00e9guli\u00e8re de renseignements. Les relations avec le secteur priv\u00e9 et les autres intervenants sont une composante fondamentale des activit\u00e9s d'Agriculture et Agroalimentaire Canada (AAC). De tels contacts ont lieu tous les jours et \u00e0 de nombreux paliers de l'organisation, et ils sont n\u00e9cessaires \u00e0 une ex\u00e9cution efficace de nos activit\u00e9s. Nous communiquons de l'information \u00e0 des intervenants \u00e0 diff\u00e9rentes fins autoris\u00e9es par la loi. Toutefois, lorsque l'information obtenue par un titulaire de charge publique est utilis\u00e9e de mani\u00e8re inappropri\u00e9e par une tierce partie, cela peut nuire \u00e0 l'int\u00e9r\u00eat public.</p>\n     <p>Par cons\u00e9quent, avant d'interagir avec des lobbyistes, il est important de pr\u00e9ciser \u00e0 quelle fin l'information est communiqu\u00e9e et de les aviser de toute contrainte ou restriction relative \u00e0 la fa\u00e7on dont elle peut \u00eatre communiqu\u00e9e.</p></li>\n     <li><p><b>R\u00e8gle 10\u00a0\u2013 Cadeaux\u00a0: Afin d'\u00e9viter la cr\u00e9ation d'un sentiment d'obligation, un lobbyiste ne doit pas offrir ou promettre un cadeau, une faveur ou un autre avantage \u00e0 un titulaire d'une charge publique, aupr\u00e8s duquel il fait ou fera du lobbying, que le titulaire d'une charge publique n'est pas autoris\u00e9 \u00e0 accepter.</b></p>\n     <p>Les lobbyistes seront invit\u00e9s \u00e0 consulter le Code de valeurs et d'\u00e9thique du Minist\u00e8re pour d\u00e9terminer l'acceptabilit\u00e9 de cadeaux ou d'autres avantages dans le cadre de leurs interactions avec des fonctionnaires. Pour aider les lobbyistes \u00e0 se conformer \u00e0 la r\u00e8gle, le <a href=\"http://www.agr.gc.ca/fra/a-propos-de-nous/code-de-valeurs-et-d-ethique-d-agriculture-et-agroalimentaire-canada/?id=1448297869529\">Code de valeurs et d'\u00e9thique d'Agriculture et Agroalimentaire Canada</a> est accessible au public sur notre site Web externe.</p></li>\n    </ul>\n    \n<h2>Renseignements suppl\u00e9mentaires</h2>\n  \n<p>Pour de plus amples renseignements concernant l'application du <cite>Code de d\u00e9ontologie des lobbyistes</cite>, veuillez consulter le site Web du <a href=\"https://lobbycanada.gc.ca/eic/site/012.nsf/fra/h_00000.html\">Commissariat au lobbying du Canada</a>.</p>\n  \n<h2>Pour nous joindre</h2>\n  \n<p>En cas de questions sur l'acceptabilit\u00e9 de cadeaux de la part d'intervenants, veuillez consulter le <a href=\"http://www.agr.gc.ca/fra/a-propos-de-nous/code-de-valeurs-et-d-ethique-d-agriculture-et-agroalimentaire-canada/?id=1448297869529\">Code de valeurs et d'\u00e9thique d'Agriculture et Agroalimentaire Canada</a> et le document <a href=\"display-afficher.do?id=1405693710539&amp;lang=fra\">Questions et r\u00e9ponses\u00a0: les conflits d'int\u00e9r\u00eats</a> ou communiquer avec le Centre de la politique sur les valeurs et l'\u00e9thique par courriel \u00e0 l'adresse <a href=\"mailto:aafc.valuesandethics-valeursetethique.aac@canada.ca\">aafc.valuesandethics-valeursetethique.aac@canada.ca</a> ou par t\u00e9l\u00e9phone au 1-866-894-5464 (sans frais).</p>\n\t\t"
    }
}