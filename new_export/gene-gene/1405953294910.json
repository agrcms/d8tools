{
    "dcr_id": "1405953294910",
    "lang": "en",
    "title": {
        "en": "Post-Employment Measures",
        "fr": "Mesures concernant l'apr\u00e8s-mandat"
    },
    "modified": "2020-08-20 00:00:00.0",
    "issued": "2014-07-22 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279035911647",
    "layout_name": "1 column",
    "created": "2020-08-20 07:56:20.0",
    "updated": "2020-08-20 07:56:20.0",
    "meta": {
        "issued": {
            "en": "2014-07-22",
            "fr": "2014-07-22"
        },
        "modified": {
            "en": "2020-08-20",
            "fr": "2020-08-20"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Post-Employment Measures",
            "fr": "Mesures concernant l'apr\u00e8s-mandat"
        },
        "subject": {
            "en": "communications;values and ethics",
            "fr": "communications;valeurs et \u00e9thique"
        },
        "description": {
            "en": "All public servants have a responsibility to minimize the possibility of real, apparent or potential conflict of interest between their most recent responsibilities within the federal public service and their subsequent employment outside the public service. The Values and Ethics Code for Agriculture and Agri-Food Canada (PDF) outlines requirements to prevent post-employment conflict of interest situations.",
            "fr": "Tous les fonctionnaires ont la responsabilit\u00e9 de r\u00e9duire au minimum la possibilit\u00e9 de se trouver dans des situations de conflit d'int\u00e9r\u00eats r\u00e9el, apparent ou potentiel entre leurs derni\u00e8res responsabilit\u00e9s dans la fonction publique f\u00e9d\u00e9rale et leur nouvel emploi \u00e0 l'ext\u00e9rieur de la fonction publique. Le Code de valeurs et d'\u00e9thique d'Agriculture et Agroalimentaire Canada (PDF) souligne les exigences relatives \u00e0 la pr\u00e9vention des situations de conflit d'int\u00e9r\u00eats apr\u00e8s la cessation des fonctions."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "offers of employment or proposed activities outside the public service",
            "fr": "offres d'emploi ou toutes les activit\u00e9s propos\u00e9es en dehors de la fonction publique"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>All public servants have a responsibility to minimize the possibility of real, apparent or potential conflict of interest between their most recent responsibilities within the federal public service and their subsequent employment outside the public service. The <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/hrb_wrd_ve_aafc_values_and_ethics_code-eng.pdf\">Values and Ethics Code for Agriculture and Agri-Food Canada (PDF)</a> outlines requirements to prevent post-employment conflict of interest situations.</p>\n\n<p>Before leaving employment with Agriculture and Agri-Food Canada (AAFC), all employees <b>should</b>:</p>\n  \n <ul>\n  <li>Disclose their intentions regarding any future outside employment or activities that may pose a risk of real, apparent or potential conflict of interest with their current responsibilities</li>\n  <li>Discuss potential conflicts with their manager or the Values and Ethics Policy Centre</li>\n</ul>\n  \n  <h2>Measures for employees in executive (EX) , EX minus 1, EX minus 2 and equivalent positions</h2>\n  \n  <p>Certain positions at AAFC involve official duties that raise post-employment concerns. These positions include all those classified as executive (EX) positions, those classified as EX minus 1, EX minus 2, and their equivalents. AAFC may designate certain other positions as ones that raise post-employment concerns, based on an assessment of the conflict of interest or post-employment risks inherent in the official duties of the positions.</p>\n  \n  <p>Before leaving employment with AAFC, employees in these designated positions <b>must</b>:</p>\n  \n  <ul>\n <li>disclose to the Deputy Minister or his or her delegate all offers of employment or proposed activities outside the public service that could place them in a real, apparent or potential conflict of interest situation</li>\n </ul>\n  \n  <h2>One-year limitation period</h2>\n  \n <p>For a period of one year upon leaving AAFC and/or the public service, employees in executive (EX), EX minus 1 and EX minus 2 positions and their equivalents <b>may not</b>, without the authorization of the Deputy Minister or his or her delegate:</p>\n  \n  <ul>\n <li>accept appointment to a board of directors of a private entity or employment with private entities with which they had significant official dealings (directly or through subordinates) 12 months immediately prior to the termination of their service</li>\n\n <li>make representations to any government organization on behalf of persons or entities outside of the public service with which they had significant official dealings (directly with or through subordinates)</li>\n\n <li>give advice to clients or an employer using information that is not publicly available concerning AAFC programs or policies\n  <ul>\n <li><b>Note</b>: \"Significant official dealings\" include, but are not limited to:\n   <ul>\n <li>working on the development of a project proposal for the organization</li>\n <li>working collaboratively with the organization</li>\n <li>being involved in providing recommendations to the Minister on projects/proposals submitted by the organization</li>\n <li>being involved in approving applications from the organization to any of AAFC's funding programs</li>\n </ul></li>\n </ul></li>\n </ul>\n\n  <h2>Waiver or reduction of limitation period</h2>\n  \n <p>You may apply to the Deputy Minister or his or her delegate to waive or reduce the limitation period as stated in the <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/hrb_wrd_ve_aafc_values_and_ethics_code-eng.pdf\">Values and Ethics Code for Agriculture and Agri-Food Canada (PDF)</a>.</p>\n \n <p>Please contact the Values and Ethics Policy Centre for help if you would like to apply for a waiver or reduction of the limitation period.</p>\n \n <h2>Contact us</h2>\n\n<p>Contact the Values and Ethics Policy Centre by email at <a href=\"mailto:aafc.valuesandethics-valeursetethique.aac@canada.ca\">aafc.valuesandethics-valeursetethique.aac@canada.ca</a> or by phone at 1-866-894-5464 (toll-free).</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Tous les fonctionnaires ont la responsabilit\u00e9 de r\u00e9duire au minimum la possibilit\u00e9 de se trouver dans des situations de conflit d'int\u00e9r\u00eats r\u00e9el, apparent ou potentiel entre leurs derni\u00e8res responsabilit\u00e9s dans la fonction publique f\u00e9d\u00e9rale et leur nouvel emploi \u00e0 l'ext\u00e9rieur de la fonction publique. Le to <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/hrb_wrd_ve_aafc_values_and_ethics_code-fra.pdf\">Code de valeurs et d'\u00e9thique d'Agriculture et Agroalimentaire Canada (PDF)</a> souligne les exigences relatives \u00e0 la pr\u00e9vention des situations de conflit d'int\u00e9r\u00eats apr\u00e8s la cessation des fonctions.</p>\n\n<p>Avant de quitter leur emploi \u00e0 Agriculture et Agroalimentaire Canada (AAC), les employ\u00e9s <b>doivent\u00a0</b>:</p>\n \n <ul>\n <li>divulguer leur intention d'entreprendre toute activit\u00e9 ou tout emploi ext\u00e9rieur susceptible de repr\u00e9senter un risque r\u00e9el, apparent ou possible de conflit d'int\u00e9r\u00eats avec leurs responsabilit\u00e9s courantes;</li>\n <li>discuter des conflits d'int\u00e9r\u00eats potentiels avec leur gestionnaire ou le Centre de la politique sur les valeurs et l'\u00e9thique.</li>\n</ul>\n \n <h2>Mesures pour les employ\u00e9s occupant des postes du groupe de la direction (EX), des postes EX moins 1, EX moins 2 et leurs \u00e9quivalents</h2>\n \n <p>Certains postes au sein d'AAC comportent des fonctions officielles susceptibles de poser des risques relativement \u00e0 l'apr\u00e8s-mandat. Il s'agit notamment des postes du groupe de la direction (EX), des postes EX moins 1, EX moins 2 et de leurs \u00e9quivalents. AAC peut d\u00e9signer d'autres postes susceptibles de poser des risques relativement \u00e0 l'apr\u00e8s-mandat suivant l'\u00e9valuation du risque de conflit d'int\u00e9r\u00eats et du risque relatif \u00e0 l'apr\u00e8s-mandat inh\u00e9rent \u00e0 ces postes.</p>\n \n <p>Avant de quitter leur emploi \u00e0 AAC, les employ\u00e9s qui occupent un poste d\u00e9sign\u00e9 <b>doivent\u00a0</b>:</p>\n \n <ul>\n <li>divulguer au sous-ministre ou \u00e0 son d\u00e9l\u00e9gataire toutes les offres d'emploi ou toutes les activit\u00e9s propos\u00e9es en dehors de la fonction publique qui pourraient les placer dans une situation de conflit d'int\u00e9r\u00eats r\u00e9el, apparent ou potentiel.</li>\n </ul>\n \n <h2>P\u00e9riode de restriction d'un an</h2>\n \n <p>Pendant une p\u00e9riode d'un an apr\u00e8s avoir quitt\u00e9 son emploi \u00e0 AAC ou \u00e0 la fonction publique, un employ\u00e9 ayant occup\u00e9 un poste du groupe de la direction (EX), un poste EX moins 1, EX moins 2 et leurs \u00e9quivalents <b>n'a pas le droit</b>, sans l'autorisation du sous-ministre ou de son d\u00e9l\u00e9gataire :</p>\n \n <ul>\n <li>d'accepter une nomination au conseil d'administration d'une entit\u00e9 priv\u00e9e avec laquelle il a eu (personnellement ou par l'entremise de ses subalternes) des rapports officiels importants au cours des 12\u00a0mois ayant pr\u00e9c\u00e9d\u00e9 la fin de son mandat, ou d'accepter un emploi au sein d'une telle entit\u00e9;</li>\n\n <li>d'intervenir pour le compte ou au nom d'une autre personne ou d'une autre entit\u00e9 aupr\u00e8s de tout organisme du gouvernement avec lequel il a eu (personnellement ou par l'entremise de ses subalternes) des rapports officiels importants;</li>\n\n <li>de donner \u00e0 son client ou \u00e0 son employeur, au moyen de renseignements qui ne sont pas accessibles au public, des conseils touchant les programmes ou les politiques d'AAC.\n  <ul>\n <li><b>Remarque\u00a0:</b> Les rapports officiels importants comprennent notamment\u00a0:\n  <ul>\n <li>travailler \u00e0 l'\u00e9laboration d'une proposition de projet pour un organisme;</li>\n <li>collaborer avec l'organisme;</li>\n <li>avoir particip\u00e9 aux recommandations donn\u00e9es au ministre sur des projets ou des propositions soumises par l'organisme;</li>\n <li>avoir particip\u00e9 \u00e0 l'approbation de demandes de l'organisme en question, pour un des programmes de financement d'AAC.</li>\n </ul></li>\n </ul></li>\n </ul>\n\n <h2>Annulation ou r\u00e9duction de la p\u00e9riode de restriction</h2>\n \n <p>Vous pouvez demander au sous-ministre ou au sous-ministre d\u00e9l\u00e9gu\u00e9 d'annuler ou de r\u00e9duire la p\u00e9riode de restriction comme le pr\u00e9voit le\n <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/hrb_wrd_ve_aafc_values_and_ethics_code-fra.pdf\">Code de valeurs et d'\u00e9thique d'Agriculture et Agroalimentaire Canada (PDF)</a>.</p>\n \n <p>Veuillez communiquer avec le Centre de la politique sur les valeurs et l'\u00e9thique pour de l'aide si vous souhaitez demander une annulation ou une r\u00e9duction de la p\u00e9riode de restriction.</p>\n \n <h2>Communiquez avec nous</h2>\n\n<p>Communiquez avec le Centre de la politique sur les valeurs et l'\u00e9thique par courriel \u00e0 <a href=\"mailto:aafc.valuesandethics-valeursetethique.aac@canada.ca\">aafc.valuesandethics-valeursetethique.aac@canada.ca</a> ou au num\u00e9ro sans frais suivant\u00a0: 1-866-894-5464.</p>\n\n\t\t"
    }
}