{
    "dcr_id": "1287433512049",
    "lang": "en",
    "title": {
        "en": "Governance Structure",
        "fr": "Structure de gouvernance"
    },
    "modified": "2020-08-13 00:00:00.0",
    "issued": "2010-11-09 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1285342898140",
    "layout_name": "1 column",
    "created": "2020-08-13 15:47:56.0",
    "updated": "2020-08-13 15:47:56.0",
    "meta": {
        "issued": {
            "en": "2010-11-09",
            "fr": "2010-11-09"
        },
        "modified": {
            "en": "2020-08-13",
            "fr": "2020-08-13"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Governance Structure",
            "fr": "Structure de gouvernance"
        },
        "subject": {
            "en": "planning;strategic management",
            "fr": "planification;gestion strat\u00e9gique"
        },
        "description": {
            "en": "Governance, as defined by the Office of the Comptroller General, is the combination of processes and structures used to inform, direct, manage and monitor the activities of an organization towards the achievement of its objectives. AAFC\u2019s governance structure is reviewed annually to ensure its effectiveness. There are now three corporate decision-making bodies comprising senior level executives.",
            "fr": "Le Bureau du contr\u00f4leur g\u00e9n\u00e9ral d\u00e9finit la gouvernance comme la combinaison de processus et de structures mis en oeuvre pour informer, diriger, g\u00e9rer et surveiller les activit\u00e9s d\u2019une organisation en vue d\u2019atteindre ses objectifs. La structure de gouvernance d\u2019AAC est examin\u00e9e annuellement pour en assurer l\u2019efficacit\u00e9. Il existe maintenant trois organismes d\u00e9cisionnels minist\u00e9riels compos\u00e9s de cadres sup\u00e9rieurs."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "decision making framework, Management Committee",
            "fr": "cadre de prise de d\u00e9cisions, Comit\u00e9 de gestion"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "organizational description",
            "fr": "description de l'organisation"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Governance, as defined by the Office of the Comptroller General, is the combination of processes and structures used to inform, direct, manage and monitor the activities of an organization towards the achievement of its objectives. Agriculture and Agri-Food Canada's (AAFC) governance structure sets  our organization's decision-making framework and helps us to achieve our  strategic outcomes, as well as strengthening the integration of our policies  and programs.</p>\n\n<p>There are four corporate executive committees chaired by the Deputy Minister, as follows:</p>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/operations_com_reference_mandat-eng.pdf\">Operations Committee (PDF)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/dmc_cgm_reference_mandat-eng.pdf\">Departmental Management Committee (PDF)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/ppmc_cgpp_reference_mandat-eng.pdf\">Policy and Program Management Committee (PDF)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/rdmc_cgrl_reference_mandat-eng.pdf\">Results and Delivery Management Committee (PDF)</a></li>\n</ul>\n\n<p>In addition, there are two Directors General (DG) level committees:</p>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/dgmc_cgdg_refence_mandat-eng.pdf\">Directors General Management Committee (PDF)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/dgppmc_cgppdg_reference_mandat-eng.pdf\">Directors General Policy and Program Management Committee (PDF)</a></li>\n</ul>\n\n<p>The subject matter of any item under consideration will determine the appropriate committee for review, direction, or decision.</p>\n\n<p>Learn more about the departmental <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/structure_gov_2018_2019-eng.pdf\">governance structure and view sample governance flow (PDF)</a>, the supporting <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/gov_committees_networks-eng.pdf\">executive committees (PDF)</a>.</p>\n\n<p>Visit the <a href=\"https://collab.agr.gc.ca/gv/execomm-comexe/SitePages/Home.aspx\">AAFC Governance Committees</a> section in Knowledge Workspace.</p>\n\n<h2>Departmental Champions</h2>\n\n<table class=\"table table-bordered info table-striped\">\n<thead>\n\t<tr class=\"bg-info\">\n\t\t<th>Champion mandates</th>\n\t\t<th>Champion(s)</th>\n\t</tr>\n</thead>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>Beyond 2020</td>\n\t\t\t<td>Fr\u00e9d\u00e9ric Seppey</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Inclusivity and Diversity\n\t\t\t\t<ul>\n\t\t\t\t\t<li>Gender and Sexual Diversity and Inclusiveness Network</li>\n\t\t\t\t\t<li>Persons with Disabilities Network</li>\n\t\t\t\t\t<li>Visible Minorities Network</li>\n\t\t\t\t\t<li>Women in Science Network</li>\n\t\t\t\t</ul>\n\t\t\t</td>\n\t\t\t<td>Christine Walker and Michel Lessard</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Indigenous and Reconciliation \n\t\t\t\t<ul>\n\t\t\t\t<li>Indigenous Network Circle</li>\n\t\t\t\t</ul></td>\n\t\t\t<td>Brian Gray</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Innovation and Digital Strategy\n\t\t\t<ul>\n\t\t\t\t<li>Idea Farm</li>\n\t\t\t</ul>\n\t\t\t</td>\n\t\t\t<td>Paul Samson</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Government of Canada Charitable Workplace Campaign and Public Service Employee Survey</td>\n\t\t\t<td>Fred Gorrell and Fr\u00e9d\u00e9ric Seppey</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Mental Wellness</td>\n\t\t\t<td>Mary Dila</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Modern Workforce\n\t\t\t<ul>\n\t\t\t\t<li>SPROUT</li>\n\t\t\t\t<li>Young Professionals Network</li>\n\t\t\t</ul>\n\t\t\t</td>\n\t\t\t<td>Tom Rosser</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Modern Workplace\n\t\t\t\t<ul>\n\t\t\t\t\t<li>Greening government</li>\n\t\t\t\t\t<li>Accessibility</li>\n\t\t\t\t</ul>\n\t\t\t</td>\n\t\t\t<td>Gilles Saindon</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Official Languages</td>\n\t\t\t<td>Kimberly Saunders</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td colspan=\"2\"><b>Regional representation</b></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>North/British Columbia/Prairies</td>\n\t\t\t<td>Mary Dila</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Ontario/Quebec</td>\n\t\t\t<td>Tom Rosser</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Atlantic</td>\n\t\t\t<td>Kristine Allen</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Le Bureau du contr\u00f4leur g\u00e9n\u00e9ral d\u00e9finit la gouvernance comme la combinaison de processus et de structures mis en \u0153uvre pour informer, diriger, g\u00e9rer et surveiller les activit\u00e9s d'une organisation en vue d'atteindre ses objectifs. Le mod\u00e8le de gouvernance d'Agriculture et Agroalimentaire Canada (AAC) d\u00e9finit le cadre de prise de d\u00e9cisions de notre organisation et nous aide \u00e0 atteindre nos r\u00e9sultats strat\u00e9giques, ainsi que le renforcement de l'int\u00e9gration de nos politiques et nos programmes.</p>\n\n<p>Il existe quatre organismes d\u00e9cisionnels minist\u00e9riels pr\u00e9sid\u00e9 par la sous-ministre, comme suit\u00a0:</p>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/operations_com_reference_mandat-fra.pdf\">Comit\u00e9 des op\u00e9rations (PDF)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/dmc_cgm_reference_mandat-fra.pdf\">Comit\u00e9 de gestion du Minist\u00e8re (PDF)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/ppmc_cgpp_reference_mandat-fra.pdf\">Comit\u00e9 de gestion des politiques et des programmes (PDF)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/rdmc_cgrl_reference_mandat-fra.pdf\">Comit\u00e9 de la gestion des r\u00e9sultats et livraison (PDF)</a></li>\n</ul>\n\n<p>En outre, il existe deux comit\u00e9s de gestion des directeurs g\u00e9n\u00e9raux (DG)\u00a0:</p>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/dgmc_cgdg_refence_mandat-fra.pdf\">Comit\u00e9 de gestion des directeurs g\u00e9n\u00e9raux (PDF)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/dgppmc_cgppdg_reference_mandat-fra.pdf\">Comit\u00e9 de gestion des politiques et des programmes des directeurs g\u00e9n\u00e9raux (PDF)</a></li>\n</ul>\n\n<p>Le sujet de tout \u00e9l\u00e9ment \u00e0 l'\u00e9tude permettra de nommer le comit\u00e9 qui sera charg\u00e9 de l'examen, de la direction ou de la prise des d\u00e9cisions.</p>\n\n<p>Pour de plus amples renseignements, visitez l'<a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/structure_gov_2018_2019-fra.pdf\">organigramme de gouvernance minist\u00e9rielle et un \u00e9chantillon de l'acheminement (PDF)</a> et les <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/gov_committees_networks-fra.pdf\">Comit\u00e9s ex\u00e9cutifs (PDF)</a>.</p>\n\n<p>Visitez la section de l'Espace de travail du savoir des <a href=\"https://collab.agr.gc.ca/gv/execomm-comexe/SitePages/Home.aspx\">comit\u00e9s de gouvernance d'AAC</a>.\n\n</p><h2></h2>\n\n<table class=\"table table-bordered info table-striped\">\n<thead>\n\t<tr class=\"bg-info\">\n\t\t<th>Mandats des champions</th>\n\t\t<th>Champion(s)</th>\n\t</tr>\n</thead>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>Au-del\u00e0 de 2020</td>\n\t\t\t<td>Fr\u00e9d\u00e9ric Seppey</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Inclusivit\u00e9 et diversit\u00e9 \n\t\t\t\t<ul>\n\t\t\t\t\t<li>R\u00e9seau de l\u2019inclusivit\u00e9 et de la diversit\u00e9 des genres et de la sexualit\u00e9</li>\n\t\t\t\t\t<li>R\u00e9seau des personnes handicap\u00e9es</li>\n\t\t\t\t\t<li>R\u00e9seau des minorit\u00e9s visibles</li>\n\t\t\t\t\t<li>R\u00e9seau des femmes en sciences</li>\n\t\t\t\t</ul>\n\t\t\t</td>\n\t\t\t<td>Christine Walker et Michel Lessard</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Autochtones et r\u00e9conciliation \n\t\t\t\t<ul>\n\t\t\t\t<li>Cercle de r\u00e9seautage des employ\u00e9s autochtones</li>\n\t\t\t\t</ul></td>\n\t\t\t<td>Brian Gray</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Innovation et strat\u00e9gie pour le num\u00e9rique \n\t\t\t<ul>\n\t\t\t\t<li>P\u00e9pini\u00e8re d\u2019id\u00e9es</li>\n\t\t\t</ul>\n\t\t\t</td>\n\t\t\t<td>Paul Samson</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Campagne de charit\u00e9 en milieu de travail du gouvernement du Canada et Sondage aupr\u00e8s des fonctionnaires f\u00e9d\u00e9raux</td>\n\t\t\t<td>Fred Gorrell et Fr\u00e9d\u00e9ric Seppey</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Sant\u00e9 mentale</td>\n\t\t\t<td>Mary Dila</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Effectif moderne\n\t\t\t<ul>\n\t\t\t\t<li>POUSSE</li>\n\t\t\t\t<li>R\u00e9seau des jeunes professionnels</li>\n\t\t\t</ul>\n\t\t\t</td>\n\t\t\t<td>Tom Rosser</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Milieu de travail moderne\n\t\t\t\t<ul>\n\t\t\t\t\t<li>\u00c9cologisation du gouvernement</li>\n\t\t\t\t\t<li>Accessibilit\u00e9</li>\n\t\t\t\t</ul>\n\t\t\t</td>\n\t\t\t<td>Gilles Saindon</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Langues officielles</td>\n\t\t\t<td>Kimberly Saunders</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td colspan=\"2\"><b>Repr\u00e9sentants des r\u00e9gions</b></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Nord/Colombie-Britannique/Prairies</td>\n\t\t\t<td>Mary Dila</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Ontario/Qu\u00e9bec</td>\n\t\t\t<td>Tom Rosser</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>Atlantique</td>\n\t\t\t<td>Kristine Allen</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\t\t"
    }
}