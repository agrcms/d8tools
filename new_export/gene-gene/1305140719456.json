{
    "dcr_id": "1305140719456",
    "lang": "en",
    "title": {
        "en": "National Radiation Safety Program",
        "fr": "Programme national de radioprotection"
    },
    "modified": "2018-12-21 00:00:00.0",
    "issued": "2011-05-24 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1374858613407",
    "layout_name": "1 column",
    "created": "2018-12-21 10:11:28.0",
    "updated": "2018-12-21 10:11:28.0",
    "meta": {
        "issued": {
            "en": "2011-05-24",
            "fr": "2011-05-24"
        },
        "modified": {
            "en": "2018-12-21",
            "fr": "2018-12-21"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "National Radiation Safety Program",
            "fr": "Programme national de radioprotection"
        },
        "subject": {
            "en": "occupational health and safety",
            "fr": "sant\u00e9 et s\u00e9curit\u00e9 au travail"
        },
        "description": {
            "en": "Employees are occasionally required to use nuclear substances. Such use in Canada is tightly regulated by the Nuclear Safety and Control Act and by the Canadian Nuclear Safety Commission (CNSC). The CNSC's mission is to control the use of nuclear energy to ensure the health and safety of citizens, protect the environment and respect Canada's international commitments with regard to the peaceful use of nuclear energy.",
            "fr": "Les employ\u00e9s doivent \u00e0 l'occasion utiliser des substances nucl\u00e9aires. Cette utilisation au Canada est rigoureusement r\u00e9glement\u00e9e par la Loi sur la s\u00fbret\u00e9 et la r\u00e9glementation nucl\u00e9aires et la Commission canadienne de s\u00fbret\u00e9 nucl\u00e9aire (CCSN). La CCSN a pour mission de contr\u00f4ler l'utilisation de l'\u00e9nergie nucl\u00e9aire pour prot\u00e9ger la sant\u00e9 des citoyens et assurer leur s\u00e9curit\u00e9, prot\u00e9ger l'environnement et respecter les engagements internationaux du Canada en ce qui concerne l'utilisation pacifique de l'\u00e9nergie nucl\u00e9aire."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Nuclear Safety and Control Act, Canadian Nuclear Safety Commission (CNSC), nuclear energy, National Radiation Safety Committee (NRSC), Internal Use Permit (IUP)",
            "fr": "Loi sur la s\u00fbret\u00e9 et la r\u00e9glementation nucl\u00e9aires, Commission canadienne de s\u00fbret\u00e9 nucl\u00e9aire (CCSN), \u00e9nergie nucl\u00e9aire, Comit\u00e9 national de radioprotection (CNR), permis d'utilisation interne (PUI)"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "resource list",
            "fr": "liste de r\u00e9f\u00e9rence"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Employees are occasionally required to use nuclear substances. Such use in Canada is tightly regulated by the <cite>Nuclear Safety and Control Act</cite> and by the Canadian Nuclear Safety Commission (CNSC). The CNSC's mission is to control the use of nuclear energy to ensure the health and safety of citizens, protect the environment and respect Canada's international commitments with regard to the peaceful use of nuclear energy.</p>\n\n<p>Agriculture and Agri-Food Canada (AAFC) has placed the responsibility for ensuring the safe use of radioactive substances in the hands of the National Radiation Safety Committee (NRSC). AAFC employees must possess an internal AAFC Internal Use Permit (IUP) to acquire, store, use and dispose of nuclear substances and radiation devices at AAFC sites. To receive a permit, you must complete the IUP Application Form, available in the National Radiation Safety Program. </p>\n\n<h2>Resources:</h2>\n\n<ul>\n  <li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_National_Radiation_Safety_Program_En.pdf\">National Radiation Safety Program (PDF)</a>\n    <ul>\n      <li>Roles and Responsibilities</li>\n      <li>Radiation Safety Training Standards</li>\n      <li>Internal Use Permit</li>\n      <li>Internal Compliance Inspections</li>\n      <li>Radiation Safety Officer Guidelines</li>\n      <li>Security and Emergency</li>\n    </ul>\n  </li>\n  <li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Decision_Tree_Emergency_En.docx\">Decision tree: Emergencies involving radioactive materials or radiation devices (Word)</a></li>\n</ul>\n\n<h3>Training</h3>\n\n<h4>Manual</h4>\n<ul>\n<li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Radiation_Safety_Training_Manual_En.pdf\">National Radiation Safety Manual (PDF)</a>\n  <ul>\n   <li>Radiation Safety Basic Concepts</li>\n   <li>Radiation Safety Procedures</li>\n   <li>Dosimetry</li>\n   <li>Instrumentation</li>\n   <li>Transportation of Dangerous Goods Class 7, 9</li>\n  </ul></li>\n   </ul>\n \n <h4>PowerPoint</h4>\n \n <ul>  \n\t<li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Radiation_Safety_Basic_Training_En.PPTX\">Radiation Safety Basic Training (PowerPoint)</a></li>\n\t<li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Radiation_Safety_Unsealed_Sources_Training_En.PPTX\">Radiation Safety Unsealed Sources Training (PowerPoint)</a></li>\n\t<li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Radiation_Safety_Sealed_Sources_Training_En.PPTX\">Radiation Safety Sealed Sources Training (PowerPoint)</a></li>\n\t<li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Radiation_Safety_TDG_Class_7_Training_En.PPTX\">Transportation of Dangerous Goods Class 7 and 9 Training (PowerPoint)</a></li>\n</ul>\n\n<h2>Contact Us</h2>\n\n<p><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/SitePages/Home.aspx\">AAFC Radiation Safety Knowledge Workspace</a></p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Les employ\u00e9s doivent \u00e0 l'occasion utiliser des substances nucl\u00e9aires. Cette utilisation au Canada est rigoureusement r\u00e9glement\u00e9e par la <cite>Loi sur la s\u00fbret\u00e9 et la r\u00e9glementation nucl\u00e9aires</cite> et la Commission canadienne de s\u00fbret\u00e9 nucl\u00e9aire (CCSN). La CCSN a pour mission de contr\u00f4ler l'utilisation de l'\u00e9nergie nucl\u00e9aire pour prot\u00e9ger la sant\u00e9 des citoyens et assurer leur s\u00e9curit\u00e9, prot\u00e9ger l'environnement et respecter les engagements internationaux du Canada en ce qui concerne l'utilisation pacifique de l'\u00e9nergie nucl\u00e9aire.</p>\n\n<p>Agriculture et Agroalimentaire Canada (AAC) a confi\u00e9 la responsabilit\u00e9 d'assurer l'utilisation s\u00e9curitaire des substances radioactives au Comit\u00e9 national de radioprotection (CNR). Les employ\u00e9s d'AAC doivent avoir un permis d'utilisation interne (PUI) d'AAC pour poss\u00e9der, entreposer, utiliser et \u00e9vacuer des substances nucl\u00e9aires et appareils \u00e0 rayonnement aux sites d'AAC. Pour recevoir un permis, vous devez remplir le formulaire de demande de permis disponible dans le programme national de radioprotection.</p>\n\n<h2>Ressources\u00a0:</h2>\n\n<ul>\n  <li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_National_Radiation_Safety_Program_Fr.pdf\">Programme national de radioprotection (PDF)</a>\n    <ul>\n      <li>R\u00f4les et responsabilit\u00e9s</li>\n      <li>Normes de formation en radioprotection</li>\n      <li>Permis d'utilisation interne</li>\n      <li>Conformit\u00e9 des inspections internes</li>\n      <li>Directives pour les responsables de la radioprotection</li>\n      <li>S\u00e9curit\u00e9 et urgence</li>\n    </ul>\n  </li>\n  <li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Decision_Tree_Emergency_Fr.docx\">Arbre de d\u00e9cision : Situations d'urgence mettant en cause des mati\u00e8res radioactives ou appareils \u00e0 rayonnement (Word)</a></li>\n</ul>\n\n<h3>Formation</h3>\n\n<h4>Manuel</h4>\n\n<ul>\n<li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Radiation_Safety_Training_Manual_Fr.pdf\">Manuel de formation en radioprotection (PDF)</a>\n  <ul>\n   <li>Concepts de base en radioprotection</li>\n   <li>Proc\u00e9dures de radioprotection</li>\n   <li>Dosim\u00e9trie</li>\n   <li>Instrumentation</li>\n   <li>Transport de marchandises dangereuses de classe 7, 9</li>\n  </ul></li>\n   </ul>\n \n <h4>PowerPoint</h4>\n \n <ul>  \n\t<li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Radiation_Safety_Basic_Training_Fr.PPTX\">Formation de base en radioprotection (PowerPoint)</a></li>\n\t<li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Radiation_Safety_Unsealed_Sources_Training_Fr.PPTX\">Formation en radioprotection sur les sources non scell\u00e9es (PowerPoint)</a></li>\n\t<li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Radiation_Safety_Sealed_Sources_Training_Fr.PPTX\">Formation en radioprotection sur les sources scell\u00e9es (PowerPoint)</a></li>\n\t<li><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/Documents/AAFC%20ProgramOriginalDocuments_ProgrammeDocumentsOriginaux/AAFC_Radiation_Safety_TDG_Class_7_Training_Fr.PPTX\">Formation en radioprotection sur le Transport de marchandises dangereuses de classe 7 et 9 (PowerPoint)</a></li>\n</ul>\n\n<h2>Communiquez avec nous</h2>\n\n<p><a href=\"https://collab.agr.gc.ca/co/aafcrs-raac/SitePages/Home.aspx\">Radioprotection \u00e0 AAC sur l'espace de travail du savoir</a></p>\n\n\t\t"
    }
}