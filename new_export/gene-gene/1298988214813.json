{
    "dcr_id": "1298988214813",
    "lang": "en",
    "title": {
        "en": "Translation and Revision Policies",
        "fr": "Politiques concernant la traduction et la r\u00e9vision"
    },
    "modified": "2017-02-10 00:00:00.0",
    "issued": "2011-03-18 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1322231938999",
    "layout_name": "1 column",
    "created": "2017-03-30 12:58:24.0",
    "updated": "2017-03-30 12:58:24.0",
    "meta": {
        "issued": {
            "en": "2011-03-18",
            "fr": "2011-03-18"
        },
        "modified": {
            "en": "2017-02-10",
            "fr": "2017-02-10"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Translation and Revision Policies",
            "fr": "Politiques concernant la traduction et la r\u00e9vision"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Translation and Revision Policies",
            "fr": "Politiques concernant la traduction et la r\u00e9vision"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "policies, translation",
            "fr": "politique, traduction"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<nav>\n<p><a href=\"#revision\">Revision Policy</a><br>\n<a href=\"#copyright\">Translation of Material Protected by Copyright</a></p>\n</nav>\n\n\n\n<h2 id=\"revision\">Revision Policy</h2>  \n\n<p>Clients submitting revision requests for documents that they have translated themselves usually do so to save time or to hone their translation skills.</p>\n\n<p>Although done with the best of intentions, this often slows down the process and incurs additional costs. Such is the case, for example, with literal translations and texts containing many mistakes. Revising these documents requires more time than it would to retranslate them.</p>\n\n<p>In order to ensure that they correspond to our mandate (which consists of providing translation and revision services as opposed to writing services), revision requests for documents translated by clients will be evaluated and processed according to the following standards:</p>\n\n<ol>\n\t<li class=\"mrgn-bttm-md\">Texts containing more than six major errors (grammar or syntax errors, omissions, false meanings, inconsistencies, gibberish) per 500 words will be deemed inadmissible for revision.</li>\n\t<li>Texts deemed inadmissible for revision will be returned to the clients, who will need to rework the original text or submit a translation request for the original text.</li>\n</ol>\n\n\n\n\n<h2 id=\"copyright\">Translation of Material Protected by Copyright</h2>\n\n<p>If the document you wish to have translated is protected by copyright (for example, a scientific article), you must obtain written permission from the author (or copyright owner) prior to sending your translation request. This permission must then be sent to Translation and Revision Services (TRS) at the same time as your translation request. <strong>We cannot process your translation request without written permission from the author</strong>.</p>\n\n<p>Please note that it is not necessary to obtain the author's permission to have a scientific abstract translated for comprehension purposes only (and not dissemination), provided the abstract is no longer than 10% of the total length of the original article.</p>\n\n<p><a href=\"display-afficher.do?id=1298922166577&amp;lang=eng#contactus\">Contact us</a> for more information.</p>\n\n\t\t",
        "fr": "\n\t\t\t  \n\n<nav>\n  <p><a href=\"#revision\">Politique de r\u00e9vision</a><br>\n    <a href=\"#copyright\">Traduction de documents prot\u00e9g\u00e9s par le droit d'auteur</a></p>\n</nav>\n\n<h2 id=\"revision\">Politique de r\u00e9vision</h2>\n\n<p>Les clients qui pr\u00e9sentent des demandes de r\u00e9vision pour des textes qu'ils ont traduits eux-m\u00eames cherchent habituellement \u00e0 gagner du temps ou \u00e0 parfaire leurs aptitudes \u00e0 la traduction.</p>\n\n<p>Bien que les clients soient motiv\u00e9s par une intention louable, cette pratique ralentit souvent le processus et occasionne des co\u00fbts additionnels. C'est le cas notamment des traductions litt\u00e9rales et des r\u00e9dactions comportant de nombreuses erreurs. Il faut plus de temps pour r\u00e9viser ces documents que pour les retraduire.</p>\n\n<p>Afin de s'assurer qu'elles correspondent bien \u00e0 notre mandat, qui consiste \u00e0 faire de la traduction et de la r\u00e9vision et non pas de la r\u00e9daction, les demandes de r\u00e9vision de textes traduits par le client seront \u00e9valu\u00e9es et trait\u00e9es en fonction des crit\u00e8res suivants\u00a0:</p>\n\n<ol>\n  <li class=\"mrgn-bttm-md\">Les textes comportant plus de six erreurs importantes (fautes de grammaire ou de syntaxe, omissions, contresens, incoh\u00e9rences, charabia) par \u00e9chantillon de 500 mots seront jug\u00e9s inadmissibles \u00e0 la r\u00e9vision;</li>\n  \n  <li>Les textes jug\u00e9s inadmissibles \u00e0 la r\u00e9vision seront retourn\u00e9s au client qui devra, selon le cas, remanier la r\u00e9daction originale ou faire une demande de traduction du texte de d\u00e9part. </li>\n</ol>\n\n<h2 id=\"copyright\">Traduction de documents prot\u00e9g\u00e9s par le droit d'auteur</h2>\n\n<p>Si le document que vous voulez faire traduire par le <abbr title=\"Service de traduction et de r\u00e9vision\">STR</abbr> est prot\u00e9g\u00e9 par le droit d'auteur (un article scientifique, par exemple), vous devez obtenir l'autorisation \u00e9crite de l'auteur (ou du d\u00e9tenteur du droit d\u2019auteur) et la joindre \u00e0 votre demande de traduction.</p>\n\n<p><b>Le <abbr title=\"Service de traduction et de r\u00e9vision\">STR</abbr> ne pourra pas traiter votre demande de traduction tant qu'il n'aura pas re\u00e7u l'autorisation \u00e9crite de l'auteur.</b></p>\n\n<p>Veuillez noter qu'il n'est pas n\u00e9cessaire d'obtenir la permission de l'auteur pour traduire, aux fins de compr\u00e9hension seulement (et non de diffusion), un r\u00e9sum\u00e9 scientifique qui n'exc\u00e8de pas 10\u00a0% de la longueur totale de l'article original.</p>\n\n<p><a href=\"display-afficher.do?id=1298922166577&amp;lang=fra#contacteznous\">Contactez-nous</a> pour obtenir plus de renseignements.</p>\n\n\t\t"
    }
}