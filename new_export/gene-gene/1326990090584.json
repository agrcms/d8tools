{
    "dcr_id": "1326990090584",
    "lang": "en",
    "title": {
        "en": "Financial coding",
        "fr": "Codage financier"
    },
    "modified": "2020-07-28 00:00:00.0",
    "issued": "2012-01-31 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1320677087463",
    "layout_name": "1 column",
    "created": "2020-07-28 14:26:41.0",
    "updated": "2020-07-28 14:26:41.0",
    "meta": {
        "issued": {
            "en": "2012-01-31",
            "fr": "2012-01-31"
        },
        "modified": {
            "en": "2020-07-28",
            "fr": "2020-07-28"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Financial coding",
            "fr": "Codage financier"
        },
        "subject": {
            "en": "finance",
            "fr": "finances"
        },
        "description": {
            "en": "Financial Coding designates the accounts and codes required to report all Agriculture and Agri-Food Canada financial transactions. Financial codes must be entered into SAP for all transactions.",
            "fr": "Le codage financier \u00e9tablit les comptes et les codes n\u00e9cessaires \u00e0 la d\u00e9claration des op\u00e9rations financi\u00e8res d'Agriculture et Agroalimentaire Canada. Il faut entrer ces codes dans SAP pour comptabiliser toutes les op\u00e9rations comptables."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "financial coding",
            "fr": "codage financier"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "resource list",
            "fr": "liste de r\u00e9f\u00e9rence"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Financial Coding designates the accounts and codes required to record all Agriculture and Agri-Food Canada financial transactions. Financial codes must be entered into the departmental financial system, SAP, for all transactions.</p>\n\n<p>A new dynamic coding resource has been added to the Financial Management Reports (FMR). It is available as a tile you can add to your SAP portal home screen, and will give you access to all current valid financial coding elements (listed below).</p>\n\n<p>To change existing coding or request new coding, use the links to the respective forms below and submit the appropriate form to the Financial Coding Mailbox, <a href=\"mailto:aafc.financialcoding-codagefinancier.aac@canada.ca\">aafc.financialcoding-codagefinancier.aac@canada.ca</a>.</p>\n\n<h2>Resources</h2>\n\n<p>To view these reports, you will need access to the Financial Management Reports on the SAP portal. For information on how to gain access, please see <a href=\"display-afficher.do?id=1452885922802&amp;lang=eng#b5\">Financial Management Reports access and training</a>.</p>\n\n<h3>Financial coding</h3>\n\n<p>The following reports can be accessed via the SAP Portal. If you do not have an account, please contact <a href=\"mailto:aafc.sap-system-access-systeme-acces-sap.aac@canada.ca\">SAP-SYSTEM-ACCESS-SYST\u00c8ME-ACC\u00c8S-SAP (AAFC/AAC).</a></p>\n\n<ul>\n <li><a href=\"https://dbciajp.agr.gc.ca:57701/irj/portal/fiori?NavMode=10#EP--593161317-Cost_Centers\">Cost Centres</a></li>\n <li><a href=\"https://dbciajp.agr.gc.ca:57701/irj/portal/fiori?NavMode=10#EP-1734348435-Fund_Centers\">Fund Centres</a></li>\n <li><a href=\"https://dbciajp.agr.gc.ca:57701/irj/portal/fiori?NavMode=10#EP--30932039-Funds\">Funds</a></li>\n <li><a href=\"https://dbciajp.agr.gc.ca:57701/irj/portal/fiori?NavMode=10#EP--1620073621-GL_Accounts\">GL Accounts</a></li>\n <li><a href=\"https://dbciajp.agr.gc.ca:57701/irj/portal/fiori?NavMode=10#EP-365370723-WBS_Balance\">WBS Balance</a></li>\n</ul>\n\n<p>For further information on navigating these reports, please review this <a href=\"http://ottpwin1.agr.gc.ca/ucontent/bbbb697dce7849ecbbb121c9bdc730ca_en-US/index.pdf\">bulletin on financial coding master data reports (PDF)</a>.</p>\n\n<h3>Departmental Results Framework</h3>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/financial_financier/paa_2017_2018-eng.xlsx\">PAA for fiscal year 2017-2018 (Excel)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/services/drf_cmr_conversion.xlsx\">Departmental Results Framework conversion for fiscal year 2018-2019 (Excel)</a></li>\n\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/services/drf_2019-20.xlsx\">Departmental Results Framework for fiscal year 2019-2020 (Excel)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/services/paa_pi_crosswalk_aap_ip.xlsx\">Program Alignment Architecture to Program Inventory Crosswalk (Excel)</a></li>\n</ul>\n\n<p>For additional information regarding FMR, see <a href=\"display-afficher.do?id=1483542043018&amp;lang=eng\">Financial Management Reports on the SAP Portal</a>.</p>\n\n<h3>Request forms</h3>\n\n<ul>\n <li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/financial_financier/fccc_form1_43-eng.xlsm\">Financial Coding Request Forms - Financial Coding (Excel)</a></li>\n <li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/financial_financier/fund_fonds1_43-eng.xlsx\">Financial Coding Request Forms - Fund (Excel)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/financial_financier/aafc_budget_addresses_form-eng.xlsx\">Financial Coding Request Forms - Budget Addresses (Excel)</a></li>\n <li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/financial_financier/coding_forms_formulaire_wbse-eng.xlsx\">Financial Coding Request Forms - Work Breakdown Structure Element (Excel)</a></li>\n</ul>\n\n<h3>Previous master data reports</h3>\n\n<ul>\n<li><a href=\"display-afficher.do?id=1462546450164&amp;lang=eng\">Fiscal Year 2017\u00a0- Effective from April\u00a01, 2016</a></li>\n<li><a href=\"display-afficher.do?id=1426779041451&amp;lang=eng\">Fiscal Year 2016\u00a0- Effective from April\u00a01, 2015</a></li>\n</ul>\n\n<h2>Contact us</h2>\n\n<p>To submit a coding request, email <a href=\"mailto:aafc.financialcoding-codagefinancier.aac@canada.ca\">aafc.financialcoding-codagefinancier.aac@canada.ca</a>.</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Le codage financier \u00e9tablit les comptes et les codes n\u00e9cessaires \u00e0 la consignation des op\u00e9rations financi\u00e8res d'Agriculture et Agroalimentaire Canada. Il faut entrer des codes financiers dans le syst\u00e8me financier du Minist\u00e8re, SAP, pour toutes les op\u00e9rations comptables.</p>\n\n<p>Une nouvelle ressource de codage dynamique a \u00e9t\u00e9 ajout\u00e9e aux Rapports de gestion financi\u00e8re (RGF). Il s\u2019agit d\u2019une vignette que vous pouvez ajouter \u00e0 votre \u00e9cran d\u2019accueil dans le portail de SAP pour avoir acc\u00e8s \u00e0 tous les codes financier valides actuels (\u00e9num\u00e9r\u00e9s ci dessous).</p>\n\n<p>Pour faire modifier les codes existants ou pour demander de nouveaux codes, utilisez les liens menant aux formulaires pertinents ci-dessous et transmettez votre formulaire \u00e0 la bo\u00eete aux lettres de l\u2019\u00e9quipe du codage financier, <a href=\"mailto:aafc.financialcoding-codagefinancier.aac@canada.ca\">aafc.financialcoding-codagefinancier.aac@canada.ca</a>.</p>\n\n<h2>Ressources</h2>\n\n<p>Pour voir ces rapports, vous devez avoir acc\u00e8s aux Rapports de gestion financi\u00e8re sur le portail de SAP. Pour des renseignements sur la fa\u00e7on d\u2019y avoir acc\u00e8s, allez \u00e0 la page <a href=\"display-afficher.do?id=1452885922802&amp;lang=fra\">Acc\u00e8s et changements aux syst\u00e8mes financiers</a>.</p>\n\n<h3>Codes financiers</h3>\n\n<p>Les rapports suivants sont accessibles via le portail SAP. Si vous n'avez pas un compte, veuillez contacter <a href=\"mailto:aafc.sap-system-access-systeme-acces-sap.aac@canada.ca\">SAP-SYSTEM-ACCESS-SYST\u00c8ME-ACC\u00c8S-SAP (AAFC/AAC)</a>.</p>\n\n<ul>\n <li><a href=\"https://dbciajp.agr.gc.ca:57701/irj/portal/fiori?NavMode=10#EP--593161317-Cost_Centers\">Centres de co\u00fbts</a></li>\n <li><a href=\"https://dbciajp.agr.gc.ca:57701/irj/portal/fiori?NavMode=10#EP-1734348435-Fund_Centers\">Centres de fonds</a></li>\n <li><a href=\"https://dbciajp.agr.gc.ca:57701/irj/portal/fiori?NavMode=10#EP--30932039-Funds\">Fonds</a></li>\n <li><a href=\"https://dbciajp.agr.gc.ca:57701/irj/portal/fiori?NavMode=10#EP--1620073621-GL_Accounts\">Comptes g\u00e9n\u00e9raux</a></li>\n <li><a href=\"https://dbciajp.agr.gc.ca:57701/irj/portal/fiori?NavMode=10#EP-365370723-WBS_Balance\">\u00c9l\u00e9ments d'OTP</a></li>\n</ul>\n\n<p>Pour obtenir plus de renseignements sur la navigation dans ces rapports, veuillez jeter un coup d\u2019\u0153il \u00e0 ce <a href=\"http://ottpwin1.agr.gc.ca/ucontent/bbbb697dce7849ecbbb121c9bdc730ca_fr-CA/index.pdf\">bulletin sur le rapport de gestion financi\u00e8re (PDF)</a>.</p>\n\n<h3>Cadre minist\u00e9riel des r\u00e9sultats</h3>\n\n<ul>\n <li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/financial_financier/paa_2017_2018-fra.xlsx\">AAP pour l\u2019exercice 2017-2018 (Excel)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/services/drf_cmr_conversion.xlsx\">Conversion du Cadre minist\u00e9riel des r\u00e9sultats pour l\u2019exercice 2018-2019 (Excel)</a></li>\n\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/services/drf_2019-20.xlsx\">Cadre minist\u00e9riel des r\u00e9sultats pour l\u2019exercice 2019-2020 (Excel)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/services/paa_pi_crosswalk_aap_ip.xlsx\">Conversion de l\u2019Architecture d\u2019alignement des programmes \u00e0 l\u2019Inventaire des programmes (Excel)</a></li>\n</ul>\n\n\n<p>Pour obtenir plus de renseignements sur les RGF, allez \u00e0 la page <a href=\"display-afficher.do?id=1483542043018&amp;lang=fra\">Rapports de gestion financi\u00e8re sur le Portail SAP</a>.</p>\n\n<h3>Formulaires de demande</h3>\n\n<ul>\n <li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/financial_financier/fccc_form1_43-fra.xlsm\">Formulaire de demande de codage financier \u2013 Codage Financier (Excel)</a></li>\n <li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/financial_financier/fund_fonds1_43-fra.xlsx\">Formulaires de demande de codage financier \u2013 Fonds (Excel)</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/financial_financier/aafc_budget_addresses_form-fra.xlsx\">Formulaires de demande de codage financier \u2013 Adresses Budg\u00e9taires (Excel)</a></li>\n <li><a href=\"https://intranet.agr.gc.ca/resources/prod/xls/cmb_dggi/financial_financier/coding_forms_formulaire_wbse-fra.xlsx\">Formulaires de demande de codage financier \u2013 \u00c9l\u00e9ment d'organigramme technique de projet (Excel)</a></li>\n</ul>\n\n<h3>Anciens rapports de donn\u00e9es de base</h3>\n\n<ul>\n<li><a href=\"display-afficher.do?id=1462546450164&amp;lang=fra\">Exercice 2017 - En date du 1<sup>er</sup> avril 2016</a></li>\n<li><a href=\"display-afficher.do?id=1426779041451&amp;lang=fra\">Exercice 2016 - En date du 1<sup>er</sup> avril 2015</a></li>\n</ul>\n\n<h2>Communiquez avec nous</h2>\n\n<p>Pour soumettre vos demandes de codes financiers envoyez un courriel \u00e0 <a href=\"mailto:aafc.financialcoding-codagefinancier.aac@canada.ca\">aafc.financialcoding-codagefinancier.aac@canada.ca</a></p>\n\t\t"
    }
}