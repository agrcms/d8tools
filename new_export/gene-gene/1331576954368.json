{
    "dcr_id": "1331576954368",
    "lang": "en",
    "title": {
        "en": "Greenhouse gas reduction",
        "fr": "R\u00e9duction des \u00e9missions de gaz \u00e0 effet de serre "
    },
    "modified": "2019-06-12 00:00:00.0",
    "issued": "2012-03-12 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1477664619841",
    "layout_name": "1 column",
    "created": "2019-06-12 13:40:21.0",
    "updated": "2019-06-12 13:40:21.0",
    "meta": {
        "issued": {
            "en": "2012-03-12",
            "fr": "2012-03-12"
        },
        "modified": {
            "en": "2019-06-12",
            "fr": "2019-06-12"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Greenhouse gas reduction",
            "fr": "R\u00e9duction des \u00e9missions de gaz \u00e0 effet de serre "
        },
        "subject": {
            "en": "communications;property management",
            "fr": "communications;gestion des biens"
        },
        "description": {
            "en": "Greenhouse gases (GHG), such as carbon dioxide and methane, are emitted through both natural and human activities. Because Canada has gone through the process of industrialization over the past 150 years, and depends on energy to operate vehicles, heat homes and operate industries, greenhouse gas emissions have increased and continue to increase. Abundant scientific data has demonstrated that the earth's climate has become warmer as a result of greenhouse gases and that the warming has been accelerating over the past two decades. This change in the climate has become a global issue of concern.",
            "fr": "L'activit\u00e9 humaine et naturelle produit des gaz \u00e0 effet de serre (GES), comme le dioxyde de carbone et le m\u00e9thane. Comme le Canada est en pleine industrialisation depuis 150 ans et consomme de l'\u00e9nergie pour faire fonctionner les v\u00e9hicules, chauffer les maisons et exploiter les industries, les \u00e9missions de gaz \u00e0 effet de serre ont augment\u00e9 et augmentent toujours. Une abondance de donn\u00e9es scientifiques d\u00e9montrent que le climat de la terre se r\u00e9chauffe en raison des gaz \u00e0 effet de serre et que ce r\u00e9chauffement s'acc\u00e9l\u00e8re depuis une vingtaine d'ann\u00e9es. Ce changement climatique est d\u00e9sormais une pr\u00e9occupation mondiale."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "greenhouse gas emissions, energy conservation, energy efficiency",
            "fr": "\u00e9missions de gaz \u00e0 effet de serre, consommation d'\u00e9nergie, efficacit\u00e9 \u00e9nerg\u00e9tique"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/images/employee_engagement/GGO/Green-Gas-Reduction_icone.jpg\" class=\"pull-left mrgn-rght-md\" alt=\"\">Agriculture and Agri-Food Canada's (AAFC) facilities and fleet (automotive and off-road) produce greenhouse gases (GHG), with 95% being from energy use in buildings. Examples of building energy uses include heating, ventilation and air-conditioning equipment, lighting, office equipment, and laboratory equipment. AAFC's fleet is comprised of approximately 1,000\u00a0cars and trucks, plus tractors, lawnmowers, and other fuel-consuming equipment.</p>\n\t\n<h2>Greening government strategy</h2>\t\n\t\n<p>The Government of Canada has committed to reduce its GHG emissions from direct and indirect sources from federal government facilities and fleets by 40% below 2005 levels by 2030 (with an aspiration to achieve this target by 2025) and 80% below 2005 levels by 2050, without using carbon offsets to reach the target:</p>\t\n\t\n<ul>\n\t<li>use renewable electricity generated on-site or purchased off-site</li>\n\t<li>promote lower-carbon work related travel, and encourage employees to use low-carbon forms of transportation to reduce emissions from commuting</li>\n\t<li>reduce emissions intensity from the goods and services purchased</li>\n</ul>\t\n\n<h3>Mobility and fleet</h3>\n\t\n<p>The government will adopt low-carbon mobility solutions, deploy supporting infrastructure in its facilities, and modernize its fleet as follows:</p>\n\t\n<ul>\n\t<li>by 2030, 80% of the government's administrative fleet will comprise of zero-emission vehicles (ZEVs) or hybrids</li>\n\t<li>departments have the opportunity to utilize telematics to collect and analyze routine energy use data in order to strategically identify which conventional fuel vehicles would be suitable for replacement with ZEVs or hybrids</li>\n</ul>\t\n\n<h3>Federal buildings</h3>\n\t\n<p>Investment will be made in capital projects with favourable financial payback and most affordable greenhouse gas reductions by:</p>\n\t\n<ul>\n\t<li>using 100% clean electricity by 2025</li>\n\t<li>monitoring energy use intensity for government-owned buildings of no less than 1,000 square metres by 2022</li>\n\t<li>ensuring all new buildings and major building retrofits are constructed to be \"net-zero carbon ready\" by 2022</li>\n\t<li>implementing energy efficiency technologies and major energy retrofits projects to reduce energy intensity and the carbon footprint of AAFC\u2019s portfolio</li>    \n</ul>\t\n\n<h2>Agriculture and Agri-Food Canada's actions</h2>\n\n<p>The department will contribute to the federal government greenhouse gas emissions reduction target from federal buildings and fleet by 40% below 2005 levels by 2030.</p>\n\n<p>AAFC continues to focus on these key elements to reduce GHGs:</p>\n\n<ul>\n\t<li>improving fleet management practices by right sizing (adjustment of vehicle size and numbers) AAFC's fleet and promoting green driving practices through education and training</li>\n\t<li>continuing to expand AAFC's infrastructure of electric vehicle supply equipment in support of plug-in electric vehicles in its fleet</li>\n\t<li>increasing energy efficiency in AAFC-owned buildings by prioritizing the highest returns on energy efficiency investments and GHG reduction</li> \n\t<li>continuing to modernize the fleet by disposing of vehicles at the end of their useful life and investing annually in alternative energy sources and new fuel efficient inventory</li>\n    <li>performing a carbon neutral strategic portfolio study to identify the roadmap to follow to achieve the 80% reduction target by 2050</li>\n\t<li>performing energy efficiency studies to identify the most appropriate measures for different sites</li>\n    <li>completing major energy retrofit projects</li>\n\t<li>continuing to purchase green electricity and use 100% clean electricity by 2025</li>\n</ul>\n\n<h2>Resources</h2>\n\n<ul>\n\t<li><a href=\"?id=1332250809743\">Green Defensive Driving Course</a> (AGR 623)</li>\n\t<li><a href=\"http://www.gcpedia.gc.ca/wiki/Green_Fleet_Management\">Green Fleet Management</a> (GCPedia)</li> \n\t<li><a href=\"http://www.gcpedia.gc.ca/wiki/Greenhouse_Gas_Emissions_and_Energy\">Greenhouse Gas Emissions and Energy</a> (GCPedia)</li> \n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/images/employee_engagement/GGO/Green-Gas-Reduction_icone.jpg\" class=\"pull-left mrgn-rght-md\" alt=\"\"> Les installations et le parc de v\u00e9hicules (automobiles et v\u00e9hicules hors route) d'Agriculture et Agroalimentaire Canada (AAC) produisent des gaz \u00e0 effet de serre (GES), 95\u00a0% du volume \u00e9tant imputable \u00e0 la consommation d'\u00e9nergie dans les b\u00e2timents, c'est-\u00e0-dire l'\u00e9quipement de chauffage, de ventilation et de conditionnement de l'air (CVCA), l'\u00e9clairage, le mat\u00e9riel de bureau et le mat\u00e9riel de laboratoire. Le parc de v\u00e9hicules d'AAC compte environ 1\u00a0000\u00a0voitures et camions, en plus des tracteurs, des tondeuses et d'autres \u00e9quipements \u00e0 essence.</p>\n\n<h2>Strat\u00e9gie d'\u00e9cologisation du gouvernement</h2>\n\n<p>Le gouvernement du Canada s'est engag\u00e9 \u00e0 r\u00e9duire ses \u00e9missions de GES, provenant de sources directes et indirectes li\u00e9es \u00e0 ses installations et \u00e0 ses parcs de v\u00e9hicules, de 40\u00a0% par rapport au niveau de 2005 d'ici 2030 (avec pour ambition d\u2019atteindre cet objectif d\u2019ici 2025), et de 80\u00a0% par rapport \u00e0 ces m\u00eames niveaux d'ici 2050, sans utiliser de cr\u00e9dits compensatoires pour le carbone pour l'atteinte de cet objectif\u00a0:</p>\n\n<ul>\n\t<li>utiliser de l'\u00e9lectricit\u00e9 renouvelable produite sur place ou achet\u00e9e d'ailleurs.</li>\n\t<li>promouvoir des d\u00e9placements li\u00e9s au travail qui entra\u00eenent moins d'\u00e9missions de carbone, et encourager les employ\u00e9s \u00e0 utiliser des moyens de transport \u00e0 faible \u00e9mission de carbone pour r\u00e9duire les \u00e9missions g\u00e9n\u00e9r\u00e9es lors des d\u00e9placements entre le domicile et le lieu de travail.</li>\n\t<li>r\u00e9duire l'intensit\u00e9 des \u00e9missions provenant des biens et des services achet\u00e9s.</li>\n</ul>\n\n<h3>Mobilit\u00e9 et parc de v\u00e9hicules</h3>\n\t\t\n<p>Le gouvernement adoptera des solutions de mobilit\u00e9 \u00e0 faibles \u00e9missions, d\u00e9ploiera l'infrastructure n\u00e9cessaire dans ses installations et modernisera son parc de v\u00e9hicules de la fa\u00e7on suivante\u00a0:</p>\n\t\n<ul>\n\t<li>d'ici 2030, 80\u00a0% du parc de v\u00e9hicules administratifs du gouvernement sera constitu\u00e9 de v\u00e9hicules hybrides ou de v\u00e9hicules \u00e0 \u00e9mission z\u00e9ro (VEZ).</li>\n\t<li>les minist\u00e8res ont la possibilit\u00e9 d'utiliser la t\u00e9l\u00e9matique pour recueillir et analyser les donn\u00e9es sur l'utilisation d'\u00e9nergie courante pour d\u00e9terminer de fa\u00e7on strat\u00e9gique les v\u00e9hicules aliment\u00e9s par des carburants traditionnels qui devraient \u00eatre remplac\u00e9s par des VEZ ou des v\u00e9hicules hybrides.</li>\n</ul>\t\n\n<h3>Immeubles f\u00e9d\u00e9raux</h3>\n\n<p>Des sommes seront investies dans les projets d'immobilisation comportant des retomb\u00e9es financi\u00e8res et les solutions les plus abordables pour la r\u00e9duction des GES\u00a0:</p>\n\t\n<ul>\n\t<li>utiliser une \u00e9lectricit\u00e9 propre d'ici 2025.</li>\n\t<li>surveiller l'intensit\u00e9 de la consommation \u00e9nerg\u00e9tique des b\u00e2timents d'au moins 1\u00a0000\u00a0m\u00e8tres carr\u00e9s appartenant au gouvernement f\u00e9d\u00e9ral d'ici 2022.</li>\n\t<li>veiller \u00e0 ce que tous les nouveaux b\u00e2timents et les projets de modernisation majeure soient r\u00e9alis\u00e9s conform\u00e9ment \u00e0 un code de construction qui pr\u00e9voit une empreinte carbone nulle, d\u2019ici 2022.</li>\n    <li>mettre en \u0153uvre des technologies \u00e9co\u00e9nerg\u00e9tiques et des projets d\u2019am\u00e9lioration de l\u2019efficacit\u00e9 \u00e9nerg\u00e9tique en vue de r\u00e9duire l\u2019intensit\u00e9 \u00e9nerg\u00e9tique et l\u2019empreinte carbone du portefeuille d\u2019AAC.</li>\n</ul>\t\n\n<h2>Mesures prises par Agriculture et Agroalimentaire Canada</h2>\n\n<p>Le Minist\u00e8re contribuera \u00e0 la cible de r\u00e9duction des \u00e9missions de gaz \u00e0 effet de serre (GES) du gouvernement f\u00e9d\u00e9ral, qui proviennent de ses b\u00e2timents et de son parc de v\u00e9hicules, de 40\u00a0% sous le niveau de 2005 d'ici 2030, avec pour ambition d\u2019atteindre cet objectif d\u2019ici 2025.</p>\n\n<p>Des sommes seront investies dans les projets d'immobilisation comportant des retomb\u00e9es financi\u00e8res et les solutions les plus abordables pour la r\u00e9duction des GES.</p> \n\n<p>AAC continue de mettre l'accent sur les \u00e9l\u00e9ments cl\u00e9s suivants pour r\u00e9duire les GES\u00a0:</p>\n\n<ul>\n\t<li>am\u00e9liorer les pratiques de gestion du parc de v\u00e9hicules d'AAC en choisissant des v\u00e9hicules de taille ad\u00e9quate (changement de la taille des v\u00e9hicules et de leur nombre) et en favorisant des pratiques de conduite \u00e9cologiques par la sensibilisation et la formation.</li>\n\t<li>continuer \u00e0 accro\u00eetre l'infrastructure d'AAC li\u00e9e \u00e0 l'\u00e9quipement d'alimentation des v\u00e9hicules \u00e9lectriques afin de favoriser l'utilisation de v\u00e9hicules \u00e9lectriques rechargeables dans son parc de v\u00e9hicules.</li>\n\t<li>accro\u00eetre l'efficacit\u00e9 \u00e9nerg\u00e9tique des b\u00e2timents appartenant \u00e0 AAC en accordant la priorit\u00e9 aux rendements les plus \u00e9lev\u00e9s sur les investissements dans l'efficacit\u00e9 \u00e9nerg\u00e9tique et \u00e0 la r\u00e9duction des GES. </li>\n\t<li>continuer de moderniser le parc de v\u00e9hicules en \u00e9liminant les v\u00e9hicules \u00e0 la fin de leur dur\u00e9e de vie utile et en investissant annuellement dans des sources d'\u00e9nergie de remplacement ainsi que de nouveaux v\u00e9hicules \u00e9co\u00e9nerg\u00e9tiques.</li>\n    <li>r\u00e9aliser une \u00e9tude sur le portefeuille carboneutre strat\u00e9gique, afin de d\u00e9finir le plan d\u2019action \u00e0 suivre pour atteindre l\u2019objectif de r\u00e9duction de 80\u00a0% d\u2019ici 2050.</li>\n\t<li>r\u00e9aliser des \u00e9tudes sur l\u2019efficacit\u00e9 \u00e9nerg\u00e9tique afin de d\u00e9terminer les mesures qui conviennent le mieux aux diff\u00e9rents sites</li>\n    <li>r\u00e9aliser d\u2019importants projets d\u2019am\u00e9lioration du rendement \u00e9co\u00e9nerg\u00e9tique.</li>\n\t<li>continuer \u00e0 acheter de l'\u00e9lectricit\u00e9 \u00ab\u00a0verte\u00a0\u00bb et utiliser uniquement de l\u2019\u00e9lectricit\u00e9 propre (100\u00a0%) d\u2019ici 2025.</li>\n</ul>\n\n<h2>Ressources</h2>\n\n<ul>\n\t<li><a href=\"?id=1332250809743\">Cours d'\u00e9coconduite pr\u00e9ventive</a> (AGR 623)</li>\n\t<li><a href=\"http://www.gcpedia.gc.ca/wiki/Gestion_des_parcs_automobiles\">Gestion \u00e9cologique des parcs de v\u00e9hicules</a> (GCp\u00e9dia)</li> \n\t<li><a href=\"http://www.gcpedia.gc.ca/wiki/%C3%89missions_de_gaz_%C3%A0_effet_de_serre_(GES)_et_%C3%A9nergie\">\u00c9missions de gaz \u00e0 effet de serre et \u00e9nergie</a> (GCp\u00e9dia)</li>\n</ul>\n\t\t"
    }
}