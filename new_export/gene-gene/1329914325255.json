{
    "dcr_id": "1329914325255",
    "lang": "en",
    "title": {
        "en": "Greening government ",
        "fr": "\u00c9cologisation du gouvernement"
    },
    "modified": "2019-06-12 00:00:00.0",
    "issued": "2012-03-07 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1477664619841",
    "layout_name": "1 column",
    "created": "2019-06-12 13:40:21.0",
    "updated": "2019-06-12 13:40:21.0",
    "meta": {
        "issued": {
            "en": "2012-03-07",
            "fr": "2012-03-07"
        },
        "modified": {
            "en": "2019-06-12",
            "fr": "2019-06-12"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Greening government ",
            "fr": "\u00c9cologisation du gouvernement"
        },
        "subject": {
            "en": "communications;planning;property management",
            "fr": "communications;planification;gestion des biens"
        },
        "description": {
            "en": "Greening Government Operations (GGO) is one of four themes within the first Federal Sustainable Development Strategy (FSDS) tabled in October 2010. The FSDS provides an integrated, whole-of-government picture of actions and results to achieve environmental sustainability.",
            "fr": "L'\u00e9cologisation des op\u00e9rations gouvernementales (EOG) est l'un des quatre piliers de la premi\u00e8re Strat\u00e9gie f\u00e9d\u00e9rale de d\u00e9velopement durable (SFDD), qui a \u00e9t\u00e9 d\u00e9pos\u00e9e au Parlement en octobre 2010. La SFDD fournit un tableau int\u00e9gr\u00e9 et pangouvernemental des mesures et des r\u00e9sultats pour l'atteinte de la durabilit\u00e9 environnementale."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Greening Government Operations (GGO), Federal Sustainable Development Strategy (FSDS)",
            "fr": "L'\u00e9cologisation des op\u00e9rations gouvernementales (EOG), Strat\u00e9gie f\u00e9d\u00e9rale de d\u00e9velopement durable (SFDD)"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "resource list",
            "fr": "liste de r\u00e9f\u00e9rence"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>The Federal Sustainable Development Strategy (FSDS) provides Canadians with Government of Canada sustainable development priorities, establishes goals and targets, and highlights government actions from 42\u00a0federal organizations over three-year cycles.</p>\n\n<p>Within the FSDS, Greening Government outlines the federal government's efforts to reduce its environmental footprint associated with its departmental practices and operations, as prescribed in successive FSDSs.</p>\n\n<p>As a large federal department, Agriculture and Agri-Food Canada (AAFC) is committed to continue contributing to Greening Government.</p>\n\n<h2>Greening government 2016-2019</h2>\n\n<p>AAFC made very good progress towards the <a href=\"http://www.fsds-sfdd.ca/index.html#/en/goals/\">third FSDS</a> relating to the three dimensions of sustainable development: social, economic and environmental. Highlights include:</p>\n\n<ul>\n\t<li>the procurement of 35% green electricity from renewable energy sources in Alberta</li>\n\t<li>1262\u00a0kg of batteries were collected and sent for recycling and diversion from landfills in 2018-19 through the Call2Recycle battery  recycling initiative.</li>\n\t<li>adherence to the requirement that 100% of all new contracts that include janitorial services will include the use of janitorial products that minimize the environmental impact</li>\n\t<li>60,276\u00a0kg of e-waste was disposed of in 2018-2019 across all AAFC locations within Canada</li>\n</ul>\n\n<h2>Greening government 2019-2022</h2>\n\n<p>2019-2022 is Canada\u2019s upcoming <a href=\"http://fsds-sfdd.ca/index.html#/en/detail/all/\">fourth FSDS</a> cycle. FSDS\u00a04 will be tabled in Parliament in June 2019. It outlines what the federal government will do to promote clean growth, ensure healthy ecosystems and build safe, secure and sustainable communities over the next 3 years.</p>  \n\n<p>With the low-carbon goal, the Government of Canada has committed to lead by example by making its operations low carbon. The Government of Canada has followed this up in December 2017 with the release of the <a href=\"https://www.canada.ca/en/government/publicservice/modernizing.html\">Greening Government Strategy</a> to transition to low carbon and climate resilient operations with a focus on four key areas:</p>\n\n<ul>\n\t<li>low-carbon, sustainable, and climate resilient real property</li> \n\t<li>low-carbon mobility and fleet</li>\n\t<li>climate resilient assets, services, and operations</li>\n\t<li>green goods and services</li>\n</ul>\n\n<p>To learn more about AAFC's efforts to reduce its environmental footprint, visit the following web pages:</p>\n\n<ul>\n\t<li><a href=\"?id=1331576954368\">Greenhouse gas emissions</a></li>\n\t<li><a href=\"?id=1464788651920\">Real property</a></li>\n\t<li><a href=\"?id=1331664486009\">Green procurement</a></li>\n</ul>\n\n<h2>Background Information</h2>\n\n<ul>\n\t<li><a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/innovation/greening-government/strategy.html\">Government of Canada\u2019s Greening Government Strategy</a></li>\n\t<li><a href=\"https://laws-lois.justice.gc.ca/eng/acts/F-8.6/FullText.html\"><cite>Federal Sustainable Development Act</cite></a></li>\n\t<li><a href=\"http://www.fsds-sfdd.ca/index.html#/en/goals/\">Federal Sustainable Development Strategy (FSDS)</a></li>\n\t<li><a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/innovation/greening-government.html\">Centre for Greening Government</a></li>\n\t<li><a href=\"http://www.agr.gc.ca/eng/about-us/planning-and-reporting/departmental-plans/2018-19-departmental-plan/?id=1520458805687\">AAFC's Departmental Sustainable Development Strategy</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>La Strat\u00e9gie f\u00e9d\u00e9rale de d\u00e9veloppement durable (SFDD) pr\u00e9sente aux Canadiens les priorit\u00e9s de d\u00e9veloppement durable du gouvernement du Canada, \u00e9tablit les buts et objectifs, et met en \u00e9vidence les mesures gouvernementales que prendront les 42\u00a0organisations f\u00e9d\u00e9rales au cours des cycles de trois\u00a0ans.</p>\n\n<p>Dans le cadre de la SFDD, la Politique sur l'\u00e9cologisation du gouvernement \u00e9nonce les efforts d\u00e9ploy\u00e9s par le gouvernement f\u00e9d\u00e9ral pour r\u00e9duire son empreinte \u00e9cologique li\u00e9e aux pratiques et aux activit\u00e9s de ses minist\u00e8res, comme le prescrivent les SFDD successives.</p>\n\n<p>En tant que grand minist\u00e8re f\u00e9d\u00e9ral, Agriculture et Agroalimentaire Canada (AAC) s'est engag\u00e9 \u00e0 continuer sa contribution \u00e0 l'\u00e9cologisation du gouvernement.</p>\n\n<h2>\u00c9cologisation du gouvernement 2016-2019</h2>\n\n<p>AAC a r\u00e9alis\u00e9 d\u2019importants progr\u00e8s en vue de la <a href=\"http://fsds-sfdd.ca/index_fr.html#/fr/intro/\">troisi\u00e8me SFDD</a>, li\u00e9e aux trois dimensions du d\u00e9veloppement durable (dimensions sociale, \u00e9conomique et environnementale). En voici les points saillants\u00a0:</p>\n\n\n<ul>\n\t<!--li>une r&eacute;duction des &eacute;missions de gaz &agrave; effet de serre (GES) de 22&nbsp;% par rapport au niveau de r&eacute;f&eacute;rence de 2005-2006 au moyen de la m&eacute;thode fixe, ou une r&eacute;duction de 35&nbsp;% au moyen de la m&eacute;thode dynamique.</li-->\n\t<li>l\u2019approvisionnement de 35\u00a0% d\u2019\u00e9lectricit\u00e9 verte provenant de sources d\u2019\u00e9nergie renouvelable en Alberta.</li>\n\t<li>en 2018-2019, 1\u00a0262\u00a0kg de piles us\u00e9es ont \u00e9t\u00e9 recueillis et envoy\u00e9s vers des sites d\u2019enfouissement dans le cadre de l\u2019initiative de recyclage des piles \u00ab\u00a0Appel\u00e0Recycler\u00a0\u00bb.</li>\n\t<li>le respect de l\u2019exigence selon laquelle 100\u00a0% de tous les nouveaux contrats qui comprennent des services d\u2019entretien m\u00e9nager devront pr\u00e9voir l\u2019utilisation de produits de nettoyage et d\u2019entretien qui limitent le plus possible les effets n\u00e9fastes pour l\u2019environnement.</li>\n\t<li>un total de 60\u00a0276\u00a0kg de d\u00e9chets \u00e9lectroniques ont \u00e9t\u00e9 \u00e9limin\u00e9s en 2018-2019 dans l\u2019ensemble des bureaux d\u2019AAC au Canada.</li>    \n</ul>\n\n<h2>\u00c9cologisation du gouvernement 2019-2022</h2>\n\n<p>Le <a href=\"http://www.fsds-sfdd.ca/index_fr.html#/fr/detail/all/\">quatri\u00e8me cycle de la SFDD</a> au Canada se d\u00e9roulera de 2019 \u00e0 2022. La SFDD 4 sera d\u00e9pos\u00e9e au Parlement en juin 2019. Elle d\u00e9crit les mesures que prendra le gouvernement f\u00e9d\u00e9ral au cours des trois prochaines ann\u00e9es pour favoriser une croissance propre, pr\u00e9server des \u00e9cosyst\u00e8mes sains et construire des collectivit\u00e9s s\u00fbres, s\u00e9curitaires et durables.</p>\n\n<p>Compte tenu de l'objectif li\u00e9 \u00e0 une faible empreinte de carbone, le gouvernement du Canada s'est engag\u00e9 \u00e0 pr\u00eacher par l'exemple en rendant ses activit\u00e9s faibles en \u00e9missions de carbone. Le gouvernement f\u00e9d\u00e9ral y a fait suite en d\u00e9cembre 2017 en mettant en \u0153uvre la <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/innovation/ecologiser-gouvernement/strategie.html\" rel=\"external\">Strat\u00e9gie d'\u00e9cologisation du gouvernement</a> pour effectuer une transition vers des activit\u00e9s faibles en \u00e9missions de carbone et r\u00e9silientes aux changements climatiques, tout en \u00e9tant ax\u00e9es sur les quatre secteurs cl\u00e9s suivants\u00a0:</p>\n\n<ul>\n\t<li>biens immobiliers \u00e0 faibles \u00e9missions de carbone, durables et r\u00e9silients au changement climatique;</li>\n\t<li>mobilit\u00e9 et parc de v\u00e9hicules \u00e0 faibles \u00e9missions de carbone;</li>\n\t<li>biens, services et activit\u00e9s r\u00e9silients au climat;</li>\n\t<li>biens et services \u00e9cologiques.</li>\n</ul>\n\n<p>Pour en savoir davantage sur les efforts que d\u00e9ploie AAC pour r\u00e9duire son empreinte \u00e9cologique, consultez les pages Web suivantes\u00a0:</p>\n\n<ul>\n\t<li><a href=\"?id=1331576954368\">\u00c9mission de gaz \u00e0 effet de serre</a></li>\n\t<li><a href=\"?id=1464788651920\">Biens immobiliers</a></li>\n\t<li><a href=\"?id=1331664486009\">Achats \u00e9cologiques</a></li>\n</ul>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n\n<ul>\n\t<li><a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/innovation/ecologiser-gouvernement/strategie.html\">Strat\u00e9gie d\u2019\u00e9cologisation du gouvernement du Canada</a></li>\n\t<li><a href=\"http://laws-lois.justice.gc.ca/fra/lois/F-8.6/TexteComplet.html\"><cite>Loi f\u00e9d\u00e9rale sur le d\u00e9veloppement durable</cite></a></li>\n\t<li><a href=\"http://www.fsds-sfdd.ca/index_fr.html#/fr/goals/\">Strat\u00e9gie f\u00e9d\u00e9rale de d\u00e9veloppement durable (SFDD)</a></li>\n\t<li><a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/innovation/ecologiser-gouvernement.html\">Centre pour un gouvernement vert</a></li>\n\t<li><a href=\"http://www.agr.gc.ca/fra/a-propos-de-nous/planification-et-rapports/plans-ministeriels/plan-ministeriel-2018-2019/?id=1520458805687\">Strat\u00e9gie de d\u00e9veloppement durable d\u2019AAC</a></li>\n</ul>\n\n\t\t"
    }
}