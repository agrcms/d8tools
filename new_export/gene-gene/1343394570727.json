{
    "dcr_id": "1343394570727",
    "lang": "en",
    "title": {
        "en": "Ergonomic Coaching",
        "fr": "S\u00e9ances adapt\u00e9es en ergonomie"
    },
    "modified": "2019-11-04 00:00:00.0",
    "issued": "2012-08-09 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1480694537376",
    "layout_name": "1 column",
    "created": "2019-11-04 09:40:43.0",
    "updated": "2019-11-04 09:40:43.0",
    "meta": {
        "issued": {
            "en": "2012-08-09",
            "fr": "2012-08-09"
        },
        "modified": {
            "en": "2019-11-04",
            "fr": "2019-11-04"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Ergonomic Coaching",
            "fr": "S\u00e9ances adapt\u00e9es en ergonomie"
        },
        "subject": {
            "en": "occupational health and safety",
            "fr": "sant\u00e9 et s\u00e9curit\u00e9 au travail"
        },
        "description": {
            "en": "An ergonomic coaching session identifies and prevents ergonomic related hazards associated with jobs, tasks, and the work environment.",
            "fr": "Une s\u00e9ance adapt\u00e9e en ergonomie permet de d\u00e9terminer et de pr\u00e9venir les risques ergonomiques li\u00e9s aux postes, aux t\u00e2ches et au milieu de travail."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "ergonomics",
            "fr": "ergonomie"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<section>\n\t\t\t  \n\n<div class=\"row\">\n\n<div class=\"col-md-4 pull-right\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Ergonomic Coaching</h2>\n</header>\n<div class=\"panel-body\">\n<ol>\n     <li>Try to self-adjust your work station with the <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3510440&amp;lang=eng\" target=\"_blank\">Workstation Set Up (PowerPoint)</a> sheet.</li>\n     <li>Complete the <a href=\"https://forms-formulaires.agr.gc.ca/pdf/A6204-E.pdf\">Ergonomic Coaching Request Form (PDF)</a> for a one-on-one ergonomic coaching session.</li>\n     <li>Send your completed form to <a href=\"mailto:aafc.ergonomics-ergonomie.aac@canada.ca\">aafc.ergonomics-ergonomie.aac<br>@canada.ca</a></li>\n     <li>You will receive a confirmation email once your request has been processed.</li>\n</ol>\n</div>\n</section>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n<p>An ergonomic coaching session identifies and prevents ergonomic related hazards associated with jobs, tasks, and the work environment. A trained and certified ergonomic coach will work one-on-one with an employee to identify and address ergonomic hazards in an effort to help prevent the development of a work musculoskeletal injury (WMSI).</p>\n\n<p>Ergonomic coaching sessions, conducted within Agriculture and Agri-Food Canada's Internal Ergonomics Services, can be initiated under one or several of the following circumstances:</p>\n\n<ul>\n     <li class=\"mrgn-bttm-md\">When an employee reports symptoms of a WMSI</li>\n     <li class=\"mrgn-bttm-md\">The identification of jobs, processes, or work activities where work-related ergonomics risk factors may cause or aggravate WMSI</li>\n     <li class=\"mrgn-bttm-md\">When a safety walk-through, scheduled inspection or survey has uncovered potential WMSI risks</li>\n     <li>Upon request of an employee through their manager/supervisor after attempting an ergonomic self-adjustment</li>\n</ul>\n\n<p>The <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3734116&amp;lang=eng\" target=\"_blank\">Ergonomic Coaching Process (PowerPoint)</a> flow chart details the steps in the ergonomic coaching process.</p>\n</div></div>\n\n\t\t</section>",
        "fr": "<section>\n\t\t\t  \n\n<div class=\"row\">\n\n<div class=\"col-md-4 pull-right\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">S\u00e9ances adapt\u00e9es en ergonomie</h2>\n</header>\n<div class=\"panel-body\">\n<ol>\n      <li>Tentez de r\u00e9gler votre poste de travail avec l\u2019aide de la fiche sur le <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3557242&amp;lang=fra\" target=\"_blank\">R\u00e9glage du poste de travail (PowerPoint)</a>.</li> \n      <li>Remplissez le <a href=\"https://forms-formulaires.agr.gc.ca/pdf/A6204-F.pdf\">Formulaire de demande - s\u00e9ance adapt\u00e9e en ergonomie (PDF)</a> afin d'obtenir une s\u00e9ance adapt\u00e9e individuelle en ergonomie.</li>\n      <li>Envoyez votre formulaire rempli \u00e0 l'adresse \u00e9lectronique <a href=\"mailto:aafc.ergonomics-ergonomie.aac@canada.ca\">aafc.ergonomics-ergonomie.aac@canada.ca</a>.</li>\n      <li>Vous recevrez un courriel de confirmation lorsque votre demande sera trait\u00e9e.</li>\n</ol>\n</div>\n</section>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n<p>Une s\u00e9ance adapt\u00e9e en ergonomie permet de d\u00e9terminer et de pr\u00e9venir les risques ergonomiques li\u00e9s aux postes, aux t\u00e2ches et au milieu de travail. Un instructeur form\u00e9 et certifi\u00e9 en ergonomie tiendra une s\u00e9ance individuelle avec un employ\u00e9 afin de d\u00e9terminer et d'aborder les risques li\u00e9s \u00e0 l'ergonomie dans le but de pr\u00e9venir les troubles musculo-squelettiques li\u00e9s au travail (TMSLT).</p>\n\n<p>Les s\u00e9ances adapt\u00e9es qui sont offertes par les Services internes d'ergonomie d'Agriculture et Agroalimentaire Canada peuvent \u00eatre initi\u00e9es dans l'une ou plusieurs des situations suivantes\u00a0:</p>\n\n<ul>\n      <li class=\"mrgn-bttm-md\">lorsqu'un employ\u00e9 pr\u00e9sente des sympt\u00f4mes de TMSLT</li>\n      <li class=\"mrgn-bttm-md\">lorsqu'on identifie des postes, des processus et des activit\u00e9s de travail o\u00f9 des risques ergonomiques li\u00e9s au travail pourraient causer ou aggraver un TMSLT</li>\n      <li class=\"mrgn-bttm-md\">lorsqu'une revue de la s\u00e9curit\u00e9, une inspection planifi\u00e9e ou un sondage r\u00e9v\u00e8le des risques de TMSLT</li>\n      <li>\u00e0 la demande d'un employ\u00e9, par l'interm\u00e9diaire de son gestionnaire ou de son superviseur, \u00e0 la suite d'une tentative d'autoajustement ergonomique</li>\n</ul>\n\n<p>Le diagramme <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3734163&amp;lang=fra\" target=\"_blank\">S\u00e9ances adapt\u00e9es en ergonomie (PowerPoint)</a> d\u00e9crit les \u00e9tapes du processus pour les s\u00e9ances adapt\u00e9es en ergonomie.</p></div></div></section>"
    }
}