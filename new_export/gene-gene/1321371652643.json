{
    "dcr_id": "1321371652643",
    "lang": "en",
    "title": {
        "en": "Ministerial Correspondence and Briefings Division",
        "fr": "Division de la correspondance minist\u00e9rielle et breffages"
    },
    "modified": "2018-06-14 00:00:00.0",
    "issued": "2011-11-16 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279030367366",
    "layout_name": "1 column",
    "created": "2018-06-14 14:57:52.0",
    "updated": "2018-06-14 14:57:52.0",
    "meta": {
        "issued": {
            "en": "2011-11-16",
            "fr": "2011-11-16"
        },
        "modified": {
            "en": "2018-06-14",
            "fr": "2017-05-15"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Ministerial Correspondence and Briefings Division",
            "fr": "Division de la correspondance minist\u00e9rielle et breffages"
        },
        "subject": {
            "en": "communications;",
            "fr": "communications"
        },
        "description": {
            "en": "The Ministerial Correspondence and Records Unit (MinCor) is part of the Deputy Minister's Office. MinCor, is responsible for managing and processing the correspondence of: the Minister of Agriculture and Agri-Food; the Minister of State (Agriculture); the Deputy Minister and Associate Deputy Minister; and any other supporting elected officials (such as parliamentary secretaries). MinCor provides the following services: routing incoming ministerial correspondence to the Department and Portfolio agencies for reply or information; monitoring and responding to correspondence trends; providing high-quality bilingual writing and editing of ministerial and deputy ministerial replies; liaising with the executive offices; and managing all ministerial correspondence records.",
            "fr": "La Section de la correspondance et des dossiers minist\u00e9riels (SCDM) rel\u00e8ve du Bureau du sous-ministre. La SCDM est responsable de la gestion et du traitement de la correspondance: du ministre de l'Agriculture et de l'Agroalimentaire; du ministre d'\u00c9tat (Agriculture); du sous-ministre et du sous-ministre d\u00e9l\u00e9gu\u00e9; de tout autre employ\u00e9 de soutien \u00e9lu (comme les secr\u00e9taires parlementaires). La SCDM offre les services suivants: acheminer la correspondance minist\u00e9rielle d'arriv\u00e9e vers le Minist\u00e8re et les organismes du portefeuille pour l'obtention d'une r\u00e9ponse ou \u00e0 titre d'information; contr\u00f4ler et analyser la correspondance dans le but de s'adapter \u00e0 l'\u00e9volution des th\u00e8mes qu'on y observe; fournir des services bilingues de qualit\u00e9 sup\u00e9rieure pour la r\u00e9daction et la r\u00e9vision des r\u00e9ponses du ministre et des sous-ministres; assurer un lien avec le Cabinet du ministre; g\u00e9rer tous les dossiers relatifs \u00e0 la correspondance minist\u00e9rielle."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Coordination Section, Tracking and Reporting Executive Correspondence System (TRECS), Editing/Writing Section",
            "fr": "Sous-section de la coordination, Syst\u00e8me de suivi et de rapports pour la correspondance de la haute direction (SSRCHD), Sous-section de la r\u00e9daction-r\u00e9vision"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "organizational description",
            "fr": "description de l'organisation"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Ministerial Correspondence and Briefings Division (MCBD) is part of the Deputy Minister's Office. For information about how to prepare and process ministerial correspondence, please visit the <a href=\"display-afficher.do?id=1288788891723&amp;lang=eng#rr\">Executive Correspondence-Resources section</a>.</p>\n\n<p>MCBD, is responsible for managing and processing the correspondence of:</p>\n\n<ul>\n  <li>the Minister of Agriculture and Agri-Food;</li>\n  <li>the Minister of State (Agriculture);</li>\n  <li>the Deputy Minister and Associate Deputy Minister; and</li>\n  <li>any other supporting elected officials (such as parliamentary secretaries).</li>\n</ul>\n\n<p>MCBD provides the following services:</p>\n<ul>\n  <li>routing incoming ministerial correspondence to the Department and Portfolio agencies for reply or information;</li>\n  <li>monitoring and responding to correspondence trends;</li>\n  <li>providing high-quality bilingual writing and editing of ministerial and deputy ministerial replies;</li>\n  <li>liaising with the executive offices; and</li>\n  <li>managing all ministerial correspondence records.</li>\n</ul>\n\n<h2>Coordination Section</h2>\n\n<p>MCBD's Coordination Section is responsible for analyzing incoming mail and routing it to the appropriate branches/agencies to prepare draft input. The coordinators also set, monitor, and enforce the deadlines for the return of this input to MCBD, and assign the draft input to the Editing/Writing Section to be finalized. The Section coordinates revisions on behalf of the executive offices, scanning the request into the Tracking and Reporting Executive Correspondence System (TRECS) and assigning the revisions to the appropriate branches/agencies or MCBD editor. The Coordination Section also:</p>\n<ul>\n  <li>responds to inquiries about the status of correspondence;</li>\n  <li>provides quote numbers for branch-initiated correspondence;</li>\n  <li>prepares correspondence reports; and</li>\n  <li>maintains manual and automated systems for registering, tracking, classifying, storing, and retrieving ministerial correspondence.</li>\n</ul>\n \n\n<h2>Editing/Writing Section</h2>\n\n<p>MCBD's Editing/Writing Section edits correspondence prepared by the branches/agencies to ensure that responses are:</p>\n<ul>\n <li>grammatically correct;</li>\n <li>complete;</li>\n <li>up to date; and</li>\n <li>appropriate in style and tone.</li>\n</ul>\n\n<p>After a reply is edited and proofread in MCBD, it is sent to the executive offices for approval.</p>\n\n<p>When a write-in campaign on a particular issue is under way, a MCBD editor will work with the responsible branches/agencies to develop a standard reply that can be used to address most of the incoming correspondence on that issue.</p>\n\n<p>This section also prepares regrets letters, end acknowledgements, and transfer letters. On an ad hoc basis, the Editing/Writing Section edits departmental documents for public distribution.</p>\n\n<h2>Public Information Request Services</h2>\n\n<p>Public Information Request  Services (PIRS) responds to queries from Canadians and people outside Canada  who are seeking information about Agriculture and Agri-Food Canada (AAFC)  policies, programs, services and activities, and the Canadian agriculture and  agri-food sector. Depending on the request, the unit will respond in person, by  telephone, email, regular mail or fax.</p>\n\n<p>PIRS will:</p>\n\n<ul>\n  <li>respond to requests from the public about AAFC policies, programs, services and       activities;</li>\n  <li>produce statistics for specific issues or initiatives as requested; and</li>\n  <li>assist in disseminating information in emergency situations.</li>\n</ul>\n\n<p>If you  receive a call or a written request from the public that is not in your area of  expertise, please refer it to the Public Information Request Services (PIRS)  and we will provide a timely response. We are committed to providing an answer to a request as soon as possible, within one to three business days.</p>\n\n<h2>Contact Us</h2>\n\n<p>See the Ministerial Correspondence and Briefings Division's <a title=\"DirectInfo\" href=\"http://directinfo.agr.gc.ca/directInfo/eng/index.php?fuseaction=agriInfo.orgUnit&amp;dn=ou=GAD-DGA,OU=CS-SM,OU=DMO-BSM,OU=AAFC-AAC,o=gc,c=ca\">structure and staff</a> in the employee directory.</p>\n\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>La Division de la correspondance minist\u00e9rielle et breffages (DCMB) rel\u00e8ve du Bureau du sous-ministre. Pour des renseignements sur la fa\u00e7on de r\u00e9diger et de traiter la correspondance minist\u00e9rielle, veuillez visiter <a href=\"display-afficher.do?id=1288788891723&amp;lang=fra#rr\">la section sur la Correspondance de la haute direction\u00a0\u2013 ressources</a>.</p>\n\n<p>La DCMB  est responsable de la gestion et du traitement de la correspondance\u00a0:</p>\n\n<ul>\n <li>du ministre de l'Agriculture et de l'Agroalimentaire;</li>\n <li>du ministre d'\u00c9tat (Agriculture);</li>\n <li>du sous-ministre et du sous-ministre d\u00e9l\u00e9gu\u00e9;</li>\n <li>de tout autre employ\u00e9 de soutien \u00e9lu (comme les secr\u00e9taires parlementaires).</li>\n</ul>\n\n<p>La DCMB  offre les services suivants\u00a0:</p>\n<ul>\n <li>acheminer la correspondance minist\u00e9rielle d'arriv\u00e9e vers le Minist\u00e8re et les organismes du portefeuille pour l'obtention d'une r\u00e9ponse ou \u00e0 titre d'information;</li>\n <li>contr\u00f4ler et analyser la correspondance dans le but de s'adapter \u00e0 l'\u00e9volution des th\u00e8mes qu'on y observe;</li>\n <li>fournir des services bilingues de qualit\u00e9 sup\u00e9rieure pour la r\u00e9daction et la r\u00e9vision des r\u00e9ponses du ministre et des sous-ministres;</li>\n <li>assurer un lien avec le Cabinet du ministre;</li>\n <li>g\u00e9rer tous les dossiers relatifs \u00e0 la correspondance minist\u00e9rielle.</li>\n</ul>\n\n<h2>Sous-section de la coordination</h2>\n\n<p>La sous-section de la coordination de la DCMB doit faire l'analyse des lettres d'arriv\u00e9e et les transmettre aux directions g\u00e9n\u00e9rales ou organismes appropri\u00e9s pour qu'une \u00e9bauche de r\u00e9ponse soit pr\u00e9par\u00e9e. Elle \u00e9tablit la date limite de retour des \u00e9bauches \u00e0 la DCMB et surveille de pr\u00e8s le processus d'acheminement. Une fois re\u00e7ues, les \u00e9bauches sont assign\u00e9es \u00e0 la sous-section de la r\u00e9daction-r\u00e9vision pour la pr\u00e9paration des versions d\u00e9finitives. Cette sous-section fait \u00e9galement la coordination des demandes de r\u00e9visions pour la haute direction. Elle num\u00e9rise les demandes et les sauvegarde dans le Syst\u00e8me de suivi et de rapports pour la correspondance de la haute direction (SSRCHD). Elle assigne ensuite ces demandes \u00e0 la direction g\u00e9n\u00e9rale ou \u00e0 l'organisme appropri\u00e9, ou \u00e0 un r\u00e9dacteur de la DCMB.</p>\n\n<p>La sous-section de la coordination assume \u00e9galement les t\u00e2ches suivantes\u00a0:</p>\n\n<ul>\n <li>r\u00e9pondre aux demandes relatives \u00e0 l'\u00e9tat d'avancement de la correspondance;</li>\n <li>assigner les num\u00e9ros de r\u00e9f\u00e9rence aux lettres \u00e9mises par les directions g\u00e9n\u00e9rales;</li>\n <li>pr\u00e9parer les rapports sur la correspondance;</li>\n <li>veiller au bon fonctionnement des syst\u00e8mes informatis\u00e9s et manuels pour l'enregistrement, le suivi, le classement, l'entreposage et la r\u00e9cup\u00e9ration de la correspondance minist\u00e9rielle.</li>\n</ul>\n\n\n\n<h2>Sous-section de la r\u00e9daction-r\u00e9vision</h2>\n\n<p>Les r\u00e9dacteurs-r\u00e9viseurs de la DCMB font la r\u00e9vision de la correspondance pr\u00e9par\u00e9e par les directions g\u00e9n\u00e9rales ou les organismes afin de veiller \u00e0 ce que les r\u00e9ponses soient\u00a0:</p>\n\n<ul>\n <li>grammaticalement correctes;</li>\n <li>compl\u00e8tes;</li>\n <li>\u00e0 jour;</li>\n <li>r\u00e9dig\u00e9es dans un style et un ton appropri\u00e9s.</li>\n</ul>\n\n<p>Une fois les r\u00e9ponses r\u00e9vis\u00e9es et relues au sein de la sous-section, elles sont envoy\u00e9es \u00e0 la haute direction pour approbation.</p>\n\n<p>Lorsque la DCMB s'aper\u00e7oit qu'une campagne \u00e9pistolaire sur une question particuli\u00e8re est en cours, un r\u00e9dacteur-r\u00e9viseur de la DCMB collabore avec les directions g\u00e9n\u00e9rales ou les organismes concern\u00e9s pour r\u00e9diger un texte type qui pourra \u00eatre utilis\u00e9 pour r\u00e9pondre \u00e0 la plupart des lettres re\u00e7ues sur cette question.</p>\n\n<h2>Service des renseignements au public</h2>\n\n<p>Le Service des renseignements  au public (SRP) r\u00e9pond aux demandes d'information des Canadiens et des gens \u00e0  l'ext\u00e9rieur du Canada concernant les politiques, les programmes, les services  et les activit\u00e9s d'Agriculture et Agroalimentaire Canada (AAC) et du secteur  canadien de l'agriculture et de l'agroalimentaire. Selon le type de demande,  une r\u00e9ponse sera donn\u00e9e en personne, par t\u00e9l\u00e9phone, par courriel, par courrier  ou par t\u00e9l\u00e9copieur.</p>\n\n<p>Le SRP\u00a0:</p>\n\n<ul>\n  <li>r\u00e9pondra aux demandes d'information du public concernant les politiques, les       programmes, les services et les activit\u00e9s d'AAC;</li>\n  <li>produira des rapports statistiques sur des questions ou des initiatives particuli\u00e8res, au besoin;</li>\n  <li>aidera \u00e0 diffuser l'information en cas de situation d'urgence.</li>\n</ul>\n\n<p>Si  vous recevez un appel ou une demande \u00e9crite du public, qui ne rel\u00e8ve pas de  votre domaine d'expertise, veuillez l'envoyer au Service des renseignements au  public (SRP), et nous nous chargerons d'y r\u00e9pondre en temps opportun. Nous nous  engageons \u00e0 fournir une r\u00e9ponse \u00e0 une demande d\u00e8s que possible, dans une \u00e0  trois jours ouvrables.</p>\n\n<h2>Contactez-nous</h2>\n\n<p>Veuillez consulter\u00a0: <a href=\"http://directinfo.agr.gc.ca/directInfo/fra/index.php?fuseaction=agriInfo.orgUnit&amp;dn=ou=GAD-DGA,OU=CS-SM,OU=DMO-BSM,OU=AAFC-AAC,o=gc,c=ca\" title=\"DirectInfo\">Structure et personnel</a> de la Division de la correspondance minist\u00e9rielle et breffages dans le r\u00e9pertoire des employ\u00e9s.</p>\n\n\t\t"
    }
}