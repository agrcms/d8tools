{
    "dcr_id": "1329768187967",
    "lang": "en",
    "title": {
        "en": "Part-time Worker",
        "fr": "Travail \u00e0 temps partiel "
    },
    "modified": "2020-08-25 00:00:00.0",
    "issued": "2012-03-08 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279035869288",
    "layout_name": "1 column",
    "created": "2020-08-25 15:01:00.0",
    "updated": "2020-08-25 15:01:00.0",
    "meta": {
        "issued": {
            "en": "2012-03-08",
            "fr": "2012-03-08"
        },
        "modified": {
            "en": "2020-08-25",
            "fr": "2020-08-25"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Part-time Worker",
            "fr": "Travail \u00e0 temps partiel "
        },
        "subject": {
            "en": "staffing",
            "fr": "dotation en personnel"
        },
        "description": {
            "en": "Part-Time Worker",
            "fr": "Travailleur \u00e0 temps partiel"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "part time",
            "fr": "temps partiel"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<!--\n<div class=\"alert alert-warning\">\n   <p>Please note that the HR service request forms will not be available on Wednesday, December&nbsp;4<sup>th</sup> from 6:00&nbsp;a.m. to 9:00&nbsp;a.m&nbsp;EST due to server updates.</p>\n</div>-->\n\n<p>If the work to be performed requires less than 1/3 of the usual work week of persons doing similar work.</p>\n\n<nav>\n<p>On this page:</p>\n<ul>\n<li><a href=\"#a\">Overview</a></li>\n<li><a href=\"#b\">Process</a></li>\n<li><a href=\"#c\">Service standard</a></li>\n<li><a href=\"#d\">Contact us</a></li>\n<li><a href=\"#e\">Additional resources</a></li>\n</ul>\n</nav>\n\n<h2 id=\"a\">Overview</h2>\n\n<p>If you want to hire someone in order to meet  specific part-time operational needs, you have two options:</p>\n\n<ul>\n<li>The <a href=\"http://laws-lois.justice.gc.ca/eng/regulations/SOR-81-33a/page-1.html\"><i>Part-Time Work Exclusion Approval Order</i></a> applies to the hiring of part-time workers, who are not employees.</li>\n\n<li>If the work to be performed requires <b>more than the 1/3 of the usual work week of persons doing similar work</b>, you can hire  a \"<b>part-time employee</b>\" using an  appointment process (see <a href=\"?id=1329492520008#c\">Staffing Actions \u2013 Consult your HR Advisor</a>). A <b>part-time employee</b> is a person employed to work less than the normal daily or weekly hours of work established for a full-time employee of the same occupational  group and level, and is an employee as defined in the <a href=\"http://laws-lois.justice.gc.ca/eng/acts/P-33.35/page-1.html\"><cite>Public Service Labour Relations and Employment Board Act</cite></a> or the <a href=\"http://laws-lois.justice.gc.ca/eng/acts/P-33.01/\"><cite>Public Service Employment Act</cite></a>.</li>\n</ul> \n\n<p>The \"part-time worker\" option can also  be used to retain scientists transitioning to retirement or to re-hire retired  scientists under the Agriculture and Agri-Food Canada (AAFC) Research Fellows  program.</p>\n\n<p>Consult your <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1548432&amp;lang=eng\" target=\"_blank\">Human Resources Advisor (Word)</a> if the  part-time worker being considered is not a Canadian citizen.</p>\n\n<h2 id=\"b\">Process</h2>\n\n<h3>Step 1: Verify/obtain security level</h3>\n\n<p>For information on the security process, consult <a href=\"?id=1598021292135\">Steps in the security process</a>.</p>\n\n<h3>Step 2: Gather information</h3>\n\n<p>Gather the following information:</p>\n<ol class=\"lst-lwr-alph\">\n     <li>Name of  employee</li>\n     <li>Date of  birth</li>\n     <li>Official  language preferences</li>\n     <li>Start and  end date (Part time workers hired under the <a href=\"http://laws-lois.justice.gc.ca/eng/regulations/SOR-81-33a/page-1.html\"><i>Part-Time Work Exclusion Approval Order</i></a> must have an end date)</li>\n     <li>Classification  Group and Level</li>\n     <li>Position  Number (if unknown, contact Classification at <a href=\"mailto:aafc.classification.aac@canada.ca\">aafc.classification.aac@canada.ca</a>)</li>\n     <li>Days and  hours of work (not to exceed one third of the usual work week of persons doing  similar work)</li>\n     <li>Statement of  duties (2 to 3 lines)</li>\n     <li>Confirmation  the individual is qualified (a brief explanation describing how the individual  is qualified to perform the duties of the position)</li>\n     <li>Name of  hiring branch</li>\n     <li>Name of sub-delegated  manager</li>\n     <li>Name of  supervisor</li>\n\t <li>R\u00e9sum\u00e9 of individual</li>\n</ol>\n\n<h3>Step 3: Submit your request</h3>\n\n<p>Click <a href=\"http://hri-irh.agr.gc.ca:8080/elsf-fdve/F027-1.aspx?lang=eng\">Submit Request</a> and fill out the online form. The staffing sub-delegated manager identified on this request form will be provided  with a copy of the request as well as the letter of offer for  approval/signature.</p>\n\n<h2 id=\"c\">Service standard</h2>\n\n<p><b>Five business days</b> - from the date all  documents are received to the date the letter is issued to the candidate.</p>\n\n<h2 id=\"d\">Contact us</h2>\n\n<p>For any questions, please contact the HR Centre:</p>\n<ul>\n  <li>Email: <a href=\"mailto:aafc.hrcentrerh.aac@canada.ca\">aafc.hrcentrerh.aac@canada.ca</a></li>\n  <li>Phone: 1-855-545-9575</li>\n  <li>Fax: 506-777-6059</li>\n</ul>\n\n<p>For advice on staffing for the executive group, contact <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=102720770&amp;lang=eng\" target=\"_blank\">Executive Services (Word)</a>.</p>\n\n<h2 id=\"e\">Additional resources</h2>\n\n<ul class=\"list-unstyled\">\n\n<li class=\"mrgn-bttm-md\"><a href=\"http://laws-lois.justice.gc.ca/eng/regulations/SOR-81-33a/page-1.html\">Part-time Work Exclusion Approval Order</a></li>\n\n<li><a href=\"http://laws-lois.justice.gc.ca/eng/regulations/SOR-81-33b/index.html\">Public Service Part-time Regulations</a></li>\n</ul>\n\n\t\t",
        "fr": "\n\t\t\t  \n\n<!--\n<div class=\"alert alert-warning\">\n   <p>Veuillez noter que les formulaires de service RH ne seront pas disponible entre 6h00 et 9h00&nbsp;HNE le mercredi&nbsp;4&nbsp;d&eacute;cembre&nbsp;2019 afin de proc&eacute;der a une mise-&agrave;-jour du serveur.</p>\n</div>-->\n\n<p>Si le travail \u00e0  accomplir n\u00e9cessite moins du tiers de la semaine normale de travail des  personnes faisant un travail semblable.</p>\n\n<nav>\n<p>Sur cette page\u00a0:</p>\n<ul><li><a href=\"#a\">Aper\u00e7u</a></li>\n<li><a href=\"#b\">Processus</a></li>\n<li><a href=\"#c\">Norme de service</a></li>\n<li><a href=\"#d\">Contactez-nous</a></li>\n<li><a href=\"#e\">Autres ressources</a></li></ul>\n</nav>\n\n<h2 id=\"a\">Aper\u00e7u</h2>\n\n<p>Si vous voulez embaucher quelqu'un afin de  r\u00e9pondre \u00e0 certains besoins op\u00e9rationnels \u00e0 temps partiel, deux options  s'offrent \u00e0 vous\u00a0:</p>\n\n<ul>\n<li>Le <a href=\"http://laws-lois.justice.gc.ca/fra/reglements/DORS-81-33a/page-1.html\">D\u00e9cret approuvant l'exclusion sur le travail \u00e0 temps  partiel</a> s'applique \u00e0 l'embauche de travailleurs \u00e0 temps  partiel, qui ne sont pas des employ\u00e9s.</li>\n\n<li>Si le travail \u00e0 effectuer exige <b>plus</b> <b>d'un tiers de la semaine normale de travail  des personnes qui font un travail similaire</b>, vous pourriez embaucher un  employ\u00e9 \u00e0 temps partiel en suivant un processus de nomination (voir <a href=\"?id=1329492520008#c\">Mesures de dotation\u00a0\u2013 consultez votre conseiller  en ressources humaines</a>). Un <b>travailleur \u00e0 temps partiel</b> est une  personne dont le nombre normal d'heures de travail par jour ou par semaine est  inf\u00e9rieur au nombre normal d'heures de travail par jour ou par semaine d'un  employ\u00e9 \u00e0 temps plein du m\u00eame groupe professionnel et du m\u00eame niveau, et qui  est un employ\u00e9 conform\u00e9ment \u00e0 la <a href=\"http://laws-lois.justice.gc.ca/fra/lois/P-33.35/page-1.html\"><cite>Loi sur  la Commission des relations de travail</cite></a> et de l'emploi dans la fonction publique ou \u00e0 la <a href=\"http://laws-lois.justice.gc.ca/fra/lois/P-33.01/\"><cite>Loi sur l'emploi dans la fonction publique</cite></a>.</li>\n</ul> \n\n<p>On peut aussi recourir \u00e0 l'option \u00ab\u00a0travailleur \u00e0 temps partiel\u00a0\u00bb  pour maintenir en poste des scientifiques qui sont en transition vers la  retraite ou r\u00e9embaucher des scientifiques retrait\u00e9s dans le cadre du Programme  des bourses de recherche d'Agriculture et Agroalimentaire Canada (AAC).</p>\n\n<p>Consultez votre <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1548432&amp;lang=fra\" target=\"_blank\">conseiller en ressources humaines (Word)</a> si le travailleur \u00e0 temps partiel n'est pas un citoyen  canadien.</p>\n\n<h2 id=\"b\">Processus</h2>\n\n<h3>\u00c9tape 1\u00a0 V\u00e9rifier ou obtenir une autorisation de s\u00e9curit\u00e9</h3>\n\n<p>Pour obtenir des renseignements sur le processus de s\u00e9curit\u00e9, consultez les <a href=\"?id=1598021292135\">\u00c9tapes du processus de s\u00e9curit\u00e9</a>.</p>\n\n<h3>\u00c9tape 2\u00a0 Collecte d'information</h3>\n\n<p>Veuillez recueillir les renseignements  suivants\u00a0:</p>\n\n<ol class=\"lst-lwr-alph\">\n  <li>Nom de l'employ\u00e9</li>\n  <li>Date de naissance</li>\n  <li>Langue officielle  pr\u00e9f\u00e9r\u00e9e</li>\n  <li>Dates de d\u00e9but et  de fin (les travailleurs \u00e0 temps partiel dans le cadre du <a href=\"http://laws-lois.justice.gc.ca/fra/reglements/DORS-81-33a/page-1.html\">D\u00e9cret approuvant l'exclusion sur le  travail \u00e0 temps partiel</a> doivent avoir une  date de fin)</li>\n  <li>Groupe et niveau de  classification </li>\n  <li>Num\u00e9ro du poste (si  vous ne le connaissez pas, contactez l'\u00e9quipe responsable de la classification  \u00e0 <a href=\"mailto:aafc.classification.aac@canada.ca\">aafc.classification.aac@canada.ca</a>)</li>\n  <li>Jours et heures de  travail (ne doivent pas d\u00e9passer un tiers de la semaine normale de travail des  personnes qui font un travail similaire)</li>\n  <li>\u00c9nonc\u00e9 des  fonctions (deux ou trois lignes)</li>\n  <li>Confirmation des  qualifications de la personne (expliquez bri\u00e8vement les qualifications de la  personne en fonction des t\u00e2ches du poste)</li>\n  <li>Nom de la direction  g\u00e9n\u00e9rale qui embauche</li>\n  <li>Nom du gestionnaire  subd\u00e9l\u00e9gu\u00e9</li>\n  <li>Nom du superviseur</li>\n  <li><i>Curriculum vit\u00e6</i></li>\n</ol>\n\n<h3>\u00c9tape 3\u00a0 Soumettre une demande</h3>\n\n<p>Cliquez sur <a href=\"http://hri-irh.agr.gc.ca:8080/elsf-fdve/F027-1.aspx?lang=fra\">Soumettre la demande</a> et remplissez le  formulaire en ligne. Le gestionnaire subd\u00e9l\u00e9gu\u00e9 en dotation dont le nom figure  sur ce formulaire de demande recevra une copie de la demande, ainsi que la  lettre d'offre pour approbation et signature.</p>\n\n<h2 id=\"c\">Norme de service</h2>\n\n<p><b>Cinq jours ouvrables</b> - \u00e0 compter de la date de  r\u00e9ception de tous les documents requis jusqu'\u00e0 ce que la lettre soit envoy\u00e9e au  candidat.</p>\n\n<h2 id=\"d\">Contactez-nous</h2>\n\n<p>Pour toute question, veuillez communiquer avec le  Centre de RH\u00a0:</p>\n\n<ul>\n  <li>Courriel\u00a0: <a href=\"mailto:aafc.hrcentrerh.aac@canada.ca\">aafc.hrcentrerh.aac@canada.ca</a></li>\n  <li>T\u00e9l\u00e9phone\u00a0: 1-855-545-9575</li>\n  <li>T\u00e9l\u00e9copieur\u00a0: 506-777-6059</li>\n</ul>\n\n<p>Pour des conseils en mati\u00e8re de dotation pour les cadres sup\u00e9rieurs, communiquez avec les <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=102720770&amp;lang=fra\" target=\"_blank\">Services aux cadres sup\u00e9rieurs (Word)</a>.</p>\n\n<h2 id=\"e\">Autres ressources</h2>\n\n<ul class=\"list-unstyled\">\n\n<li class=\"mrgn-bttm-md\"><a href=\"http://laws-lois.justice.gc.ca/fra/reglements/DORS-81-33a/page-1.html\">D\u00e9cret approuvant l'exclusion sur le  travail \u00e0 temps partiel</a></li>\n\n<li><a href=\"http://laws-lois.justice.gc.ca/fra/reglements/DORS-81-33b/index.html\"><cite>R\u00e8glement sur le travail \u00e0 temps partiel dans la Fonction publique</cite></a></li>\n</ul>\n\t\t"
    }
}