{
    "dcr_id": "1343395757527",
    "lang": "en",
    "title": {
        "en": "Workplace Accommodation Assessment: Ergonomic Assessment",
        "fr": "\u00c9valuation des mesures d'adaptation en milieu de travail : \u00c9valuation ergonomique"
    },
    "modified": "2020-02-04 00:00:00.0",
    "issued": "2012-08-09 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1480694537376",
    "layout_name": "1 column",
    "created": "2020-02-04 12:15:45.0",
    "updated": "2020-02-04 12:15:45.0",
    "meta": {
        "issued": {
            "en": "2012-08-09",
            "fr": "2012-08-09"
        },
        "modified": {
            "en": "2020-02-04",
            "fr": "2020-02-04"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Workplace Accommodation Assessment: Ergonomic Assessment",
            "fr": "\u00c9valuation des mesures d'adaptation en milieu de travail : \u00c9valuation ergonomique"
        },
        "subject": {
            "en": "occupational health and safety",
            "fr": "sant\u00e9 et s\u00e9curit\u00e9 au travail"
        },
        "description": {
            "en": "An ergonomic assessment is considered when a workplace accommodation is required to address issues, such as, serious injury, chronic pain, and special accommodations, which are beyond the scope of ergonomic coaching.",
            "fr": "On songe \u00e0 mener une \u00e9valuation ergonomique lorsqu'une mesure d'adaptation en milieu de travail est n\u00e9cessaire afin de r\u00e9gler certains probl\u00e8mes, notamment une blessure grave, une douleur chronique et des mesures d'adaptation sp\u00e9ciales, qui d\u00e9passent la port\u00e9e d\u2019une s\u00e9ance adapt\u00e9e en ergonomie."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "ergonomics, assessment",
            "fr": "ergonomie, \u00e9valuation"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>An ergonomic assessment is considered when a workplace accommodation is required to address issues, such as, serious injury, chronic pain, and special accommodations, which are beyond the scope of ergonomic coaching.  A workplace assessment is completed by a professional assessor through an external service provider who assists in identifying workplace barriers and provides recommendations on how to accommodate the employee for maximum safety and productivity on the job. This process allows for the design and adaptation of the work environment to the needs of the employee.</p>\n\n<h2>Request an Ergonomic Assessment</h2>\n\n<p>Follow these steps to request an ergonomic assessment:</p>\n\n<ol>\n<li>Review the document <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101881710&amp;lang=eng\" target=\"_blank\">Requesting a Workplace Accommodation Assessment (Word)</a>.</li>\n<li>Notify your supervisor or manager of your need for an ergonomic accommodation. You may refer to the guide on <agridoc id=\"101881703\">Discussing Possible Accommodation Requirements with your Supervisor (Word)</agridoc> to help you.</li>\n<li>If it is determined that an ergonomic assessment is required, you and your supervisor/manager will be provided with a form to complete and sign.</li>  \n<li>You and your supervisor/manager will be contacted by the external service provider once your completed request has been received.</li>\n</ol> \n\n<h2>Additional Resources</h2>\n\n<ul>\n <li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3734222&amp;lang=eng\" target=\"_blank\">Ergonomic Assessment and Follow up Process (PowerPoint)</a>\u00a0\u2013 flow chart details the steps in the ergonomic assessment and follow-up processes.</li>\n <li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101881712&amp;lang=eng\" target=\"_blank\">How to Action the AAFC Workplace Accommodation Assessment Report (Word)</a>\u00a0\u2013 provides information for supervisors/managers about how to ensure recommendations are implemented in a timely manner.</li>          \n <li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101862489&amp;lang=eng\" target=\"_blank\">Procedure for Accessing the AAFC Workplace Accommodation Fund (Word)</a>\u00a0\u2013 provides information for supervisors/managers about how to access the Workplace Accommodation Fund, if required.</li>\n</ul> \n\n<h2>Contact Us</h2>\n\n<p>For questions concerning the Ergonomic Assessment process you may contact: <a href=\"mailto:aafc.accommodatedutyto-adaptationmesures.aac@canada.ca\">aafc.accommodatedutyto-adaptationmesures.aac@canada.ca</a></p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>On songe \u00e0 mener une \u00e9valuation ergonomique lorsqu'une mesure d'adaptation en milieu de travail est n\u00e9cessaire afin de r\u00e9gler certains probl\u00e8mes, notamment une blessure grave, une douleur chronique et des mesures d'adaptation sp\u00e9ciales, qui d\u00e9passent la port\u00e9e d'une s\u00e9ance adapt\u00e9e en ergonomie. Un \u00e9valuateur professionnel effectue une \u00e9valuation du lieu de travail, par l'interm\u00e9diaire d'un fournisseur de services externes, afin de d\u00e9terminer les obstacles en milieu de travail et de fournir des recommandations sur la fa\u00e7on d'adapter le milieu de travail en fonction de l'employ\u00e9 pour optimiser sa s\u00e9curit\u00e9 et sa productivit\u00e9. Ce processus permet de concevoir et d'adapter l'environnement de travail en fonction des besoins de l'employ\u00e9.</p>\n\n<h2>Demande d'\u00e9valuation ergonomique</h2>\n\n<p>Suivez ces \u00e9tapes pour demander une \u00e9valuation ergonomique\u00a0:</p>\n\n<ol>\n<li>Consultez le document sur la marche \u00e0 suivre pour une <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=102196046&amp;lang=fra\" target=\"_blank\">Demande d'\u00e9valuation des mesures d'adaptation en milieu de travail (Word)</a>.</li>\n<li>Avisez votre superviseur ou votre gestionnaire de votre besoin de mesures d'adaptation ergonomiques. Vous pouvez consulter le guide sur la fa\u00e7on de <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=102196065&amp;lang=fra\" target=\"_blank\">Discuter d'\u00e9ventuels mesures d'adaptation avec votre superviseur (Word)</a> pour vous aider.</li>\n<li>Si l'on d\u00e9termine qu'une \u00e9valuation ergonomique est n\u00e9cessaire, vous et votre superviseur/gestionnaire recevrez un formulaire \u00e0 remplir et \u00e0 signer.</li>  \n<li>Le fournisseur de service externe communiquera avec vous et avec votre superviseur/gestionnaire lorsqu'il aura re\u00e7u votre demande d\u00fbment compl\u00e9t\u00e9e.</li>\n</ol> \n\n<h2>Ressources additionnelles</h2>\n\n<ul>\n <li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3734284&amp;lang=fra\" target=\"_blank\">\u00c9valuation ergonomique et processus de suivi (PowerPoint)</a>\u00a0\u2013 le diagramme illustre les \u00e9tapes des processus d'\u00e9valuation ergonomique et de suivi.</li>\n <li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=102196056&amp;lang=fra\" target=\"_blank\">Comment donner suite au rapport d'\u00e9valuation des mesures d'adaptation en milieu de travail (Word)</a>\u00a0\u2013 offre aux superviseurs/gestionnaires de l'information sur la fa\u00e7on de veiller \u00e0 ce que les recommandations soient appliqu\u00e9es en temps opportun.</li>          \n <li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101879386&amp;lang=fra\" target=\"_blank\">Proc\u00e9dure d'acc\u00e8s au Fonds des mesures d'adaptation en milieu de travail d'AAC (Word)</a>\u00a0\u2013 offre aux superviseurs/gestionnaires de l'information sur la fa\u00e7on d'avoir acc\u00e8s au Fonds pour des mesures d'adaptation au travail, au besoin.</li>\n</ul> \n\n<h2>Pour nous joindre</h2>\n\n<p>Si vous avez des questions li\u00e9es aux \u00e9valuations ergonomiques, communiquez avec nous \u00e0 l'adresse \u00e9lectronique suivante\u00a0: <a href=\"mailto:aafc.accommodatedutyto-adaptationmesures.aac@canada.ca\">aafc.accommodatedutyto-adaptationmesures.aac@canada.ca</a></p>\n\t\t"
    }
}