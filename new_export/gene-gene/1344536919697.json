{
    "dcr_id": "1344536919697",
    "lang": "en",
    "title": {
        "en": "Surplus status",
        "fr": "Statut d'employ\u00e9 exc\u00e9dentaire"
    },
    "modified": "2020-08-25 00:00:00.0",
    "issued": "2012-08-10 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1285347861916",
    "layout_name": "1 column",
    "created": "2020-08-25 15:00:51.0",
    "updated": "2020-08-25 15:00:51.0",
    "meta": {
        "issued": {
            "en": "2012-08-10",
            "fr": "2012-08-10"
        },
        "modified": {
            "en": "2020-08-25",
            "fr": "2020-08-25"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Surplus status",
            "fr": "Statut d'employ\u00e9 exc\u00e9dentaire"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Agriculture and Agri-Food Canada (AAFC) is committed to supporting our surplus employees as they transition to new employment opportunities by providing the tools and resources needed for their job search. On this page you will find links to the information you will need to know as a surplus employee and where to get more assistance.",
            "fr": "Agriculture et Agroalimentaire Canada (AAC) est d\u00e9termin\u00e9 \u00e0 soutenir nos employ\u00e9s exc\u00e9dentaires durant la transition vers d'autres possibilit\u00e9s d'emploi, en leur fournissant les outils et les ressources n\u00e9cessaires \u00e0 la recherche d'un emploi. Sur cette page, vous trouverez des liens vers l'information dont vous devrez disposer \u00e0 titre d'employ\u00e9s exc\u00e9dentaires et vers d'autres sources d'aide."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "staffing, workforce adjustment, WFA",
            "fr": "dotation, r\u00e9am\u00e9nagement des effectifs"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Agriculture and Agri-Food Canada (AAFC) is committed to supporting our surplus employees as they transition to new employment opportunities by providing the tools and resources needed for their job search. On this page you will find links to the information you will need to know as a surplus employee and where to get more assistance:</p>\n\n<ul>\n<li><a href=\"#a\">Considerations for Surplus Employees</a></li>\n<li><a href=\"#b\">Job Search Supports</a></li>\n<li><a href=\"#c\">Training on Career Transition Skills</a></li>\n<li><a href=\"#d\">Frequently Asked Questions</a></li>\n</ul>\n\n<h2 id=\"a\">Considerations for Surplus Employees</h2>\n\n<p>An indeterminate employee who has been affected by a workforce adjustment (WFA) situation is in surplus status for a 12-month period from the date declared surplus or until the occurrence of one of the following:</p>\n\n<ul>\n<li>date of layoff</li>\n<li>date indeterminately appointed or deployed to another indeterminate position</li>\n<li>surplus status is rescinded</li>\n<li>the employee resigns</li>\n</ul>\n\n<p>If your position has been declared surplus, you are still expected to report to work and you may be assigned your current duties until the date the position ceases to exist. Once your position is eliminated, and before the end of your surplus status, you may be assigned other duties for which you are qualified. A resource entitled <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3538936&amp;lang=eng\" target=\"_blank\">Assigning Meaningful Work (Word)</a> has been developed to provide you with further guidance on this issue.</p>\n\n<p>During the surplus status period, employees have certain responsibilities with regards to seeking continued employment which are outlined in the <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3591507&amp;lang=eng\" target=\"_blank\">Surplus Employees Responsibilities (Word)</a> reference document. AAFC recognizes the need to allow adequate time for surplus employees to secure alternate work. In view of this, managers should allow their surplus employees up to 7.5\u00a0hours a week to focus on their job search and other related activities such as training; this may vary depending on the circumstances.</p>\n\n<p>Employees on surplus status are entitled to a 12-month period in which to secure a reasonable job offer (RJO). A RJO is an offer of indeterminate employment with the department, or within the Core Public Administration and Schedule\u00a0V Federal Administration Act (FAA) Employers, and is normally at an equivalent level, but could also include lower levels. If you refuse a RJO, you will be subject to lay-off one month following the refusal, but not before six months after your surplus declaration date. As a surplus employee, it is important that you understand what is considered a\u00a0<a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3538928&amp;lang=eng\" target=\"_blank\">Reasonable Job Offer (Word)</a> within AAFC.</p>\n\n<h2 id=\"b\">Job Search Supports</h2>\n\n<p>To ensure that all affected and surplus employees will be given first consideration when job vacancies or opportunities arise at AAFC, the Departmental Priority Referral List has been established as a central resource that AAFC managers must use when undertaking staffing activities. Every affected employee will automatically be registered on the Departmental Priority Referral List.</p>\n\n<p>You should also register with <a href=\"https://www.canada.ca/en/public-service-commission/jobs/services/gc-jobs.html\">Careers in the Federal Public Service</a> to receive Public Service job alerts and postings.</p>\n\n<h2 id=\"c\">Training on Career Transition Skills</h2>\n\n<p>Additionally, the following courses are available to assist you to prepare for your job search:</p>\n\n<ul>\n<li><a href=\"display-afficher.do?id=1306503292350&amp;lang=eng\">Learning and Development</a></li>\n<li><a href=\"display-afficher.do?id=1330531231770&amp;lang=eng\">Competencies</a></li>\n</ul>  \n\n<p>To register, complete the <a href=\"http://agrisource1.agr.gc.ca/apps/Forms_Catalogue/pdf/A4623-E.pdf\">registration form\u00a0(PDF)</a> and forward it to <a href=\"mailto:aafc.learning-apprentissage.aac@canada.ca\">aafc.learning-apprentissage.aac@canada.ca</a>.</p>\n\n<h2 id=\"d\">Frequently Asked Questions</h2>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_csd_si_wfa_qa_job_placement_support-eng.docx\">Questions and answers on Agriculture and Agri-Food Canada's job placement support (Word)</a></li>\n</ul>\n\n<h2>Resources</h2>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_definitions_work_force_adjustment-eng.doc\">Glossary of terms (Word)</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Agriculture et Agroalimentaire Canada (AAC) est d\u00e9termin\u00e9 \u00e0 soutenir ses employ\u00e9s exc\u00e9dentaires durant la transition vers de  nouvelles possibilit\u00e9s d'emploi, en leur fournissant les outils et les ressources n\u00e9cessaires \u00e0 la recherche d'un emploi. Sur cette page, vous trouverez des liens vers l'information dont vous devrez disposer \u00e0 titre d'employ\u00e9s exc\u00e9dentaires et vers d'autres sources d'aide\u00a0:</p>\n\n<ul>\n<li><a href=\"#a\">Aspects que les employ\u00e9s exc\u00e9dentaires doivent consid\u00e9rer</a></li>\n<li><a href=\"#b\">Soutien \u00e0 la recherche d'un emploi</a></li>\n<li><a href=\"#c\">Formation sur la reconversion professionnelle</a>\u00a0</li>\n<li><a href=\"#d\">Foire aux questions</a></li>\n</ul>\n\n<h2 id=\"a\">Aspects que les employ\u00e9s exc\u00e9dentaires doivent consid\u00e9rer</h2>\n\n<p>Un employ\u00e9 nomm\u00e9 pour une p\u00e9riode ind\u00e9termin\u00e9e qui a \u00e9t\u00e9 touch\u00e9 par un r\u00e9am\u00e9nagement des effectifs a le statut d'employ\u00e9 exc\u00e9dentaire pendant une p\u00e9riode de 12\u00a0mois \u00e0 compter de la date \u00e0 laquelle il est d\u00e9clar\u00e9 exc\u00e9dentaire ou jusqu'\u00e0 ce que survienne l'une des situations suivantes\u00a0:</p>\n\n<ul>\n<li>sa mise en disponibilit\u00e9</li>\n<li>sa nomination ou sa mutation pour une p\u00e9riode ind\u00e9termin\u00e9e \u00e0 un autre poste</li>\n<li>l'annulation de son statut d'employ\u00e9 exc\u00e9dentaire</li>\n<li>sa d\u00e9mission</li>\n</ul>\n\n<p>Si votre poste a \u00e9t\u00e9 d\u00e9clar\u00e9 exc\u00e9dentaire, vous \u00eates encore tenu de vous pr\u00e9senter au travail et vos fonctions habituelles pourraient vous \u00eatre confi\u00e9es jusqu'\u00e0 la date o\u00f9 le poste cesse d'exister. Une fois votre poste \u00e9limin\u00e9, et avant la fin de votre statut d'exc\u00e9dentaire, on pourrait vous confier d'autres fonctions pour lesquelles vous \u00eates qualifi\u00e9. La ressource <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3544501&amp;lang=fra\" target=\"_blank\">Assignation de t\u00e2ches valorisantes (Word)</a> a \u00e9t\u00e9 mise au point pour vous guider relativement \u00e0 cette question.</p>\n\n<p>Durant la p\u00e9riode o\u00f9 ils ont le statut d'exc\u00e9dentaire, les employ\u00e9s ont certaines responsabilit\u00e9s concernant la recherche d'un emploi permanent, qui sont d\u00e9crites dans le document de r\u00e9f\u00e9rence <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3601621&amp;lang=fra\" target=\"_blank\">Responsabilit\u00e9s des employ\u00e9s exc\u00e9dentaires (Word)</a>. AAC reconna\u00eet l'importance d'accorder suffisamment de temps aux employ\u00e9s exc\u00e9dentaires \u00e0 la recherche d'un autre emploi. Les gestionnaires devraient accorder \u00e0 leurs employ\u00e9s d\u00e9clar\u00e9s exc\u00e9dentaires un maximum de 7,5 heures par semaine pour se consacrer \u00e0 la recherche d'un emploi et \u00e0 d'autres activit\u00e9s connexes comme la formation; ce temps peut varier selon les situations.</p>\n\n<p>Les employ\u00e9s d\u00e9clar\u00e9s exc\u00e9dentaires disposent d'une p\u00e9riode de 12\u00a0mois pour trouver une offre d'emploi raisonnable (OER). L'OER est une offre d'emploi pour une p\u00e9riode ind\u00e9termin\u00e9e au Minist\u00e8re, ou dans l'administration publique centrale et chez les employeurs \u00e9num\u00e9r\u00e9s \u00e0 l'annexe\u00a0V de la Loi sur les gestions des finances publiques (LGFP), habituellement \u00e0 un niveau \u00e9quivalent, sans que soient exclues les offres d'emploi \u00e0 des niveaux plus bas. Si vous refusez une OER vous serez mis en disponibilit\u00e9 avec un mois de pr\u00e9avis, mais pas avant six mois suivant la date o\u00f9 vous avez \u00e9t\u00e9 d\u00e9clar\u00e9 exc\u00e9dentaire. En tant qu'employ\u00e9 exc\u00e9dentaire, il est important que vous compreniez ce qui est consid\u00e9r\u00e9 comme une <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3539118&amp;lang=fra\" target=\"_blank\">offre d'emploi raisonnable (Word)</a> \u00e0 AAC.</p>\n\n<h2 id=\"b\">Soutien \u00e0 la recherche d'un emploi</h2>\n\n<p>Pour veiller \u00e0 ce que tous les employ\u00e9s touch\u00e9s et tous les employ\u00e9s exc\u00e9dentaires soient les premiers consid\u00e9r\u00e9s lorsque s'ouvriront des postes vacants ou d'autres possibilit\u00e9s, une ressource centrale, le R\u00e9pertoire d'employ\u00e9s prioritaires d'AAC, a \u00e9t\u00e9 cr\u00e9e, et les gestionnaires d'AAC doivent l'utiliser lorsqu'ils entreprennent des activit\u00e9s de dotation. Chaque employ\u00e9 touch\u00e9 sera syst\u00e9matiquement inscrit dans le r\u00e9pertoire d'employ\u00e9s.</p>\n\n<p>Inscrivez-vous \u00e0 <a href=\"https://www.canada.ca/fr/commission-fonction-publique/emplois/services/emplois-gc.html\">Carri\u00e8res \u00e0 la fonction publique f\u00e9d\u00e9rale</a> pour recevoir des alertes-emplois et \u00eatre  inform\u00e9s des occasions d'emploi au sein de la fonction publique.</p>\n\n<h2 id=\"c\">Formation sur la reconversion professionnelle</h2>\n\n<p>De plus, les cours suivants sont disponibles pour vous aider \u00e0 pr\u00e9parer votre recherche d'emploi\u00a0:</p>\n\n<ul>\n<li><a href=\"display-afficher.do?id=1306503292350&amp;lang=fra\">Apprentissage et perfectionnement </a></li>\n<li><a href=\"display-afficher.do?id=1330531231770&amp;lang=fra\">Comp\u00e9tences</a></li>\n</ul>  \n\n<p>Pour vous inscrire, remplissez le <a href=\"http://agrisource1.agr.gc.ca/apps/Forms_Catalogue/pdf/A4623-F.pdf\">formulaire d'inscription (PDF)</a>\u00a0et envoyez-le \u00e0 l'adresse <a href=\"mailto:aafc.learning-apprentissage.aac@canada.ca\">aafc.learning-apprentissage.aac@canada.ca</a>.</p>\n\n<h2 id=\"d\">Foire aux questions</h2>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_csd_si_wfa_qa_job_placement_support-fra.docx\">Questions et r\u00e9ponses concernant le soutien pour le placement professionnel d'Agriculture et Agroalimentaire Canada (Word)</a></li>\n</ul>\n\n<h2>Ressources</h2>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_definitions_work_force_adjustment-fra.doc\">Glossaire (Word)</a></li>\n</ul>\n\t\t"
    }
}