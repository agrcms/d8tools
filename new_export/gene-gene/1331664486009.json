{
    "dcr_id": "1331664486009",
    "lang": "en",
    "title": {
        "en": "Green procurement",
        "fr": "Achats \u00e9cologiques"
    },
    "modified": "2018-06-27 00:00:00.0",
    "issued": "2012-03-13 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1477664619841",
    "layout_name": "1 column",
    "created": "2018-06-27 10:35:32.0",
    "updated": "2018-06-27 10:35:32.0",
    "meta": {
        "issued": {
            "en": "2012-03-13",
            "fr": "2012-03-13"
        },
        "modified": {
            "en": "2018-06-27",
            "fr": "2018-06-27"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Green procurement",
            "fr": "Achats \u00e9cologiques"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Green procurement integrates environmental performance considerations into the procurement process.",
            "fr": "Les achats \u00e9cologiques int\u00e8grent les aspects de performance environnementale au processus d'achat."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "sustainable development, Greening Government Operations, GGO, procurement, purchasing, green products",
            "fr": "d\u00e9veloppement durable, \u00e9cologisation des op\u00e9rations gouvernementales, EOG, achat, produit \u00e9cologique"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/images/employee_engagement/GGO/Green-Procurement_icone.jpg\" alt=\"\" class=\"pull-left mrgn-rght-md\">Green procurement integrates environmental performance considerations into the procurement process. Goods and services are considered green when they have a lesser or reduced effect on human health and the environment than competing goods or services that serve the same purpose. They are produced using energy-efficient and sustainable manufacturing processes, using fewer hazardous and toxic materials. Goods and services may be designed with recycled materials and less packaging to reduce overall waste generation.</p>\n\n<p>The <a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=32573\" rel=\"external\">Policy on Green Procurement</a> (April 2006) applies to the procurement of goods, services, business travel, and construction, including Information Technology (IT) hardware, vehicles, furniture, office equipment, office supplies, IT services, and professional services.</p>\n\n<p>The benefits are:</p>\n\n<ul>\n\t<li>protection of the environment</li>\n\t<li>protection of human health</li>\n\t<li>support for markets of environmentally sound goods and services</li>\n\t<li>cost savings, particularly in a life cycle basis</li>\n</ul>\n\n<h2>Greening government strategy</h2>\n\n<p>The Government of Canada is committed to aid the transition to a low-carbon economy through green procurement and the adoption of clean technologies and green products and services by:</p>\n\n<ul>\n\t<li>integrating sustainability and life-cycle assessment principles in procurement policies and practices, including the government\u2019s supply chain</li>\n\t<li>working with major suppliers to encourage the disclosure of their greenhouse gas emissions and environmental performance information</li>\n\t<li>supporting departments in adopting clean technology and clean technology demonstration projects</li>\n\t<li>increasing training and support on green procurement to public service employees</li>\n</ul>\n\n<h2>Agriculture and Agri-Food Canada actions</h2>\n\n<p>AAFC's materiel management personnel responsible for procurement will work towards contributing to the government\u2019s green procurement efforts.</p> \n\n<p>AAFC continues to focus on these green procurement elements:</p>\n\n<ul>\n\t<li>paper purchases will contain a minimum of 30% certified recycled content</li> \n\t<li>100% of all new janitorial service contracts will include the use of janitorial products with low environmental impact</li>\n\t<li>environmental considerations are integrated into procurement management processes and controls</li>\n\t<li>minimize IT assets and operate these assets in an eco-friendly manner by using power saving settings, energy efficient laptops instead of desktops, and lifecycle disposal assessment</li>\n</ul>\n\n<h2>Resources</h2>\n\n<ul>\n\t<li><a href=\"http://www.gcpedia.gc.ca/wiki/Green_Procurement\" rel=\"external\">Green procurement</a></li>  \n\t<li><a href=\"?id=1323878777193\">Materiel management</a></li>\n\t<li><a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=32573\" rel=\"external\">Policy on Green Procurement</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/images/employee_engagement/GGO/Green-Procurement_icone.jpg\" alt=\"\" class=\"pull-left mrgn-rght-md\">L'approvisionnement \u00e9cologique int\u00e8gre des facteurs li\u00e9s \u00e0 la performance environnementale dans le processus d'achat. On qualifie d'\u00ab\u00a0\u00e9cologiques\u00a0\u00bb les biens et services qui sont moins nocifs pour la sant\u00e9 et l'environnement comparativement \u00e0 d'autres produits et services concurrents qui servent aux m\u00eames fins. Ils sont produits \u00e0 l'aide de proc\u00e9d\u00e9s de fabrication durables et efficaces sur le plan \u00e9nerg\u00e9tique et qui n\u00e9cessitent une plus faible quantit\u00e9 de mat\u00e9riaux dangereux et toxiques. Les biens et services peuvent \u00eatre con\u00e7us avec des mat\u00e9riaux recycl\u00e9s et utiliser moins d'emballage afin de r\u00e9duire la production globale de d\u00e9chets.</p>\n\n<p>La <a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=32573\" rel=\"external\">Politique d'achats \u00e9cologiques</a> (avril 2006) s'applique \u00e0 l'acquisition de biens, aux services, aux voyages d'affaires et \u00e0 la construction, y compris le mat\u00e9riel li\u00e9 \u00e0 la technologie de l'information (TI), les v\u00e9hicules, les meubles, le mat\u00e9riel de bureau, les fournitures de bureau, les services de TI et les services professionnels.</p>\n\n<p>Parmi les avantages, notons\u00a0:</p>\n\n<ul>\n\t<li>la protection de l'environnement;</li>\n\t<li>la protection de la sant\u00e9 humaine;</li>\n\t<li>le soutien aux march\u00e9s de biens et de services respectueux de l'environnement;</li>\n\t<li>l'\u00e9conomie de co\u00fbts, notamment pendant la dur\u00e9e du cycle de vie.</li>\n</ul>\n\n<h2>Strat\u00e9gie d'\u00e9cologisation du gouvernement</h2>\n\n<p>Le gouvernement du Canada s'est engag\u00e9 \u00e0 faciliter la transition vers une \u00e9conomie \u00e0 faibles \u00e9missions de carbone par l'interm\u00e9diaire d'achats \u00e9cologiques et de l'adoption de technologies propres et de biens et services \u00e9cologiques, notamment en\u00a0:</p>\n\n<ul>\n\t<li>int\u00e9grant les principes de durabilit\u00e9 et d'\u00e9valuation du cycle de vie aux politiques et aux pratiques en mati\u00e8re d'approvisionnement, y compris la cha\u00eene d'approvisionnement du gouvernement;</li>\n\t<li>collaborant avec les principaux fournisseurs pour les encourager \u00e0 divulguer les donn\u00e9es li\u00e9es \u00e0 leurs \u00e9missions de gaz \u00e0 effet de serre et \u00e0 leur performance environnementale;</li>\n\t<li>aidant les minist\u00e8res \u00e0 adopter des technologies propres et des projets de d\u00e9monstration de technologies propres;</li>\n\t<li>accroissant la formation et le soutien offerts aux employ\u00e9s de la fonction publique en mati\u00e8re d'achats \u00e9cologiques.</li>\n</ul>\n\n<h2>Mesures prises par Agriculture et Agroalimentaire Canada</h2>\n\n<p>Le personnel de la gestion du mat\u00e9riel d'AAC responsable de l'approvisionnement veillera \u00e0 contribuer aux efforts du gouvernement en mati\u00e8re d'achats \u00e9cologiques.</p> \n\n<p>AAC continuera de mettre l'accent sur les \u00e9l\u00e9ments suivants de la Politique d'achats \u00e9cologiques\u00a0:</p>\n\n<ul>\n\t<li>les achats de papier contiendront au minimum 30\u00a0% de fibres recycl\u00e9es certifi\u00e9es;</li>\n\t<li>la totalit\u00e9 (100\u00a0%) des nouveaux contrats de services de nettoyage et d'entretien pr\u00e9voira l'utilisation de produits de nettoyage peu polluants;</li>\n\t<li>les facteurs environnementaux sont int\u00e9gr\u00e9s aux processus et aux contr\u00f4les de gestion des achats;</li>\n\t<li>la r\u00e9duction au minimum des biens li\u00e9s \u00e0 la TI, et l'exploitation de ces biens d'une fa\u00e7on respectueuse de l'environnement \u00e0 l'aide des fonctions d'\u00e9conomie d'\u00e9nergie, de l'utilisation d'ordinateurs portables \u00e0 faible consommation d'\u00e9nergie et d'\u00e9valuations portant sur l'\u00e9limination des biens \u00e0 la fin de leur cycle de vie.</li>\n</ul>\n\n<h2>Ressources</h2>\n\n<ul>\n\t<li><a href=\"http://www.gcpedia.gc.ca/wiki/Achats_%C3%A9cologiques\" rel=\"external\">Achats \u00e9cologiques</a></li> \n\t<li><a href=\"?id=1323878777193\">Gestion du mat\u00e9riel</a></li>\n\t<li><a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=32573\" rel=\"external\">Politique d'achats \u00e9cologiques</a></li>\n</ul>\n\n\t\t"
    }
}