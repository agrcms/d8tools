<?php

use Drush\Drush;

use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database; // Rather than db_query use this instead.

class UpdateNode
{
  public $could_not_load = FALSE;
  protected $node;
  protected $trnode;
  protected $data;
  protected $preserveNid = false;
  protected $connection = NULL;

  function loadData($jsonfile)
  {
    $this->data = json_decode(file_get_contents($jsonfile));
  }

  function nid()
  {
    return $this->node->id();
  }

  function legacy_id()
  {
    //Drush::output()->writeln('debug dcrid ' . (int) $this->data->nid);
    return $this->data->dcr_id;
  }

  function loadNodeByDcrId() {
    //dcr_id
    if (is_null($this->connection)) {
      $this->connection = Database::getConnection();
    }
    $nid = $this->connection->query('SELECT nid FROM {node} where dcr_id = :dcrid',
       [':dcrid' => $this->legacy_id()])->fetchField();
     
    if (is_numeric($nid)) {
      $node = Node::load($nid);
      $oct11_2020 = strtotime('2020-10-10');
      // @TODO: REMOVE NEXT 4 LINES AFTER HARD LAUNCH OR DISABLE.
      if ($node->getChangedTime() < $oct11_2020) {
        $this->could_not_load = TRUE;
        return FALSE;
      }
    } else {
      $this->could_not_load = TRUE;
      Drush::output()->writeln('Unable to load d8 node, d8nid was not found by dcrid=' . $this->legacy_id());
    }
    if ($node) {
      $this->node = $node;
      $this->trnode = $this->node->getTranslation('fr'); //Get the translation
    }
  }

  function updateField($field, $d8_field=null)
  {
    global $nid_old_to_new;
    global $nid_new_to_old;

    if (!$d8_field) {
      $d8_field = $field;
    }

    if($this->data->language == "en") {
      $origLang = "en";
      $oppLang = "fr";
    }
    else {
      $origLang = "fr";
      $oppLang = "en";
    }

    //Implement the mapping relation between legacy news_category_type_id and field_newstype
    $categorytypeid = $this->data->news_category_type_id;
    $newstypeid = 0;
    if ($categorytypeid) {
      switch ($categorytypeid) {
        case '1308325693175':
              $newstypeid = 21;
              break;
        case '1508942999196':
              $newstypeid = 22;
              break;
        case '1508942999197':
              $newstypeid = 23;
              break;
        case '1308334182981':
              $newstypeid = 24;
              break;
        case '1308334182982':
              $newstypeid = 25;
              break;
        case '1308334182983':
              $newstypeid = 26;
              break;
        case '1508942999198':
              $newstypeid = 27;
              break;
        case '1379951494338':
              $newstypeid = 28;
              break;
        case '1591971211315':
              $newstypeid = 111;
              break;
      }  //End of category type id mapping
    }

    switch ($field) {
      case 'field_name':
        // Some field.
        // Do stuff
        // $this->node->set($field, $this->data->field_name);
        // $this->trnode->set($field, $this->data->field_name);
        break;
      case 'field_newstype':
        // Some field.
        // Do stuff
        $this->node->set($field,$newstypeid);
        $this->trnode->set($field,$newstypeid);
        break;
      case 'field_newsexpirydate':
        $timestamp = strtotime($this->data->expirydate);
        $expirydate =  \Drupal::service('date.formatter')->format($timestamp, 'custom', 'Y-m-d\T06:00:00','UTC');
        $this->node->set($field,$expirydate );
        $this->trnode->set($field,$expirydate );
        break;
      case 'field_newsreviewedby':
        $this->node->set($field,$this->data->approvalsadvisor);
        $this->trnode->set($field,$this->data->approvalsadvisor);
        break;
      case 'field_newsapprovedby':
        $this->node->set($field,$this->data->approvalsdirector);
        $this->trnode->set($field,$this->data->approvalsdirector);
        break;
      case 'field_newssubmittername':
        $this->node->set($field,$this->data->submitername);
        $this->trnode->set($field,$this->data->submitername);
        break;
      case 'field_newssubmitteremail':
        $this->node->set($field,$this->data->submiteremail);
        $this->trnode->set($field,$this->data->submiteremail);
        break;
      case 'field_newssubmitterphone':
        $this->node->set($field,$this->data->submiterphoneNumber);
        $this->trnode->set($field,$this->data->submiterphoneNumber);
        break;
      case 'field_newstranslationnum':
        $this->node->set($field,$this->data->translateReqNumber);
        $this->trnode->set($field,$this->data->translateReqNumber);
        break;
      // for updating the missing informaiton of Employment
      case 'field_opportunityapproval':
        $this->node->set($field,$this->data->staffadvisor);
        $this->trnode->set($field,$this->data->staffadvisor);
        break;
      case 'field_name':
        $this->node->set($field,$this->data->submitername);
        $this->trnode->set($field,$this->data->submitername);
        break;
      case 'field_email':
        $this->node->set($field,$this->data->submiteremail);
        $this->trnode->set($field,$this->data->submiteremail);
        break;
      case 'field_employmentrequestorname':
        $this->node->set($field,$this->data->contactname);
        $this->trnode->set($field,$this->data->contactname);
        break;
      case 'field_employmentrequestoremail':
        $this->node->set($field,$this->data->contactemail);
        $this->trnode->set($field,$this->data->contactemail);
        break;
      case 'field_employmentrequestorphone':
        $this->node->set($field,$this->data->contactphonenumber);
        $this->trnode->set($field,$this->data->contactphonenumber);
        break;
      default:
	Drush::output()->writeln('Switch default, no action taken.');
        break;
    } //End switch
  } // End updateField function.

  function save()
  {
    $this->node->set('moderation_state', 'published');
    $this->trnode->set('moderation_state', 'published');
    $this->node->save();
    $this->trnode->save();
  }

}
