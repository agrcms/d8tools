<?php

use Drush\Drush;

class News extends UpdateNode
{
  public $success = FALSE;
  //public $could_not_load = FALSE; // inherited.
  function update($jfile)
  {
    $this->loadData($jfile);
    // news_category
    if ($some_field = $this->data->news_category_type_id) {  //   if ($some_field = $this->data->change_this_field_name) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_newstype');   //   $this->updateField('field_change_this_field_name');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->node_title);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
      Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }
        
    // approvals.advisor
    if ($this->data->approvalsadvisor) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_newsreviewedby');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->dcr_id);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
     // Drush::output()->writeln( "no twitter override for " . $this->data->dcr_id);
    }

    // approvalsdirector
    if ($this->data->approvalsdirector) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_newsapprovedby');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->dcr_id);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
    //  Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }
    
    //submitername
    if ($this->data->submitername) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_newssubmittername');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
     // Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }

    //submiteremail
    if ($this->data->submiteremail) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_newssubmitteremail');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->node_title);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
    //  Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }

    //submiterphoneNumber
    if ($this->data->submiterphoneNumber) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_newssubmitterphone');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
     //  Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }

    //translateReqNumber
    if ($this->data->translateReqNumber && $this->data->translateReqNumber != 'null') {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_newstranslationnum');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
     // Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }

    //expirydate
    if ($this->data->expirydate && $this->data->expirydate != 'null') {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_newsexpirydate');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
     // Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }
    if ($this->success) { $this->save(); } // when you are ready, uncomment this.
  }
}
