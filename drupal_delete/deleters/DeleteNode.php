<?php

use Drush\Drush;
use Drupal\node\Entity\Node;
use Drupal\node\NodeStorageInterface;

global $import_override;
$import_override = TRUE;

class DeleteNode
{
  protected $node;
  protected $trnode;
  protected $data;
  protected $preserveNid = FALSE;
  public $type;
  public $too_old = FALSE;
  public $legacy_server_error = FALSE;
  public $previously_imported = FALSE;
  public $sitemap_external_only = FALSE;
  public $unable_to_import = FALSE;
  public $top_level_item = FALSE;

  /**
   * @var NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * @param \Drupal\node\NodeStorageInterface $node_storage
   */
  public function __construct(NodeStorageInterface $node_storage) {
    $this->nodeStorage = $node_storage;
  }


  function loadData($jsonfile)
  {
    $this->data = json_decode(file_get_contents($jsonfile));
    global $import_override; // See the agri_admin.module code.
    $import_override = TRUE; // See the agri_admin.module code.
    $this->loadNode();
  }


  function loadNode($nid = NULL) {
    if (!isset($nid)) {
      $nid = $this->getNidFromDcrId();
    }
    
    Drush::output()->writeln('debug: dcr_id='. $this->old_id() . ' :nid= ' . $nid);
    $node = $this->nodeStorage->load($nid);
    $this->node = $node;
    
  }


  function getNidFromDcrId($dcrid = NULL) {
    if (!isset($dcrid)) {
      $dcrid = $this->old_id();
    }
    // Retrieves a PDOStatement object
    $connection = \Drupal::service('database');
    // http://php.net/manual/en/pdo.prepare.php
    $sth = $connection->select('node', 'n')
      ->fields('n', ['nid'])
      ->condition('n.dcr_id', $dcrid, '=');

    // Execute the statement
    $data = $sth->execute();

    // Get only one result.
    $result = $data->fetch();
    if (property_exists($result, 'nid') && isset($result->nid)) {
      $value = $result->nid;
      return $value;
    }
    else {
      return NULL;
    }
  }


  function deleteNode()
  {
    $this->node->delete();
  }


  function nid()
  {
    if (!isset($this->node)) {
      return 0;
    }
    return $this->node->id();
  }

  function old_id()
  {
    if (isset($this->data->dcr_id) && is_numeric($this->data->dcr_id)) {
      return $this->data->dcr_id;
    }
    else {
      return 0;
    }
  }

  function sitemap_id()
  {
    if (isset($this->data->node_id) && is_numeric($this->data->node_id)) {
      return $this->data->node_id;
    }
    else {
      return 0;
    }
  }

  function sitemap_pid()
  {
    if (isset($this->data->parent_node_id) && is_numeric($this->data->parent_node_id)) {
      return $this->data->parent_node_id;
    }
    else {
      return 0;
    }
  }

}
