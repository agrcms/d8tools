<?php

use Drush\Drush;
use Drupal\agri_admin\AgriAdminHelper;

class News extends DeleteNode
{
  public function isTooOld() {
    if (isset($this->data->meta->modified->en)) {
      $january2018 = 1514764800;
      $issued = strtotime($this->data->meta->modified->en);
      if ($issued > 1 && $issued < $january2018) {
        $this->too_old = TRUE;
        return TRUE;
      }
    }
    return FALSE;
  }

  private function preDelete() {
  }

  function delete($jfile)
  {
    global $export_root;
    Drush::output()->writeln('debug ??: ');
    $this->loadData($jfile);

    $this->deleteNode();
    return TRUE; // Success.
  }
}
