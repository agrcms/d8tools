<?php

use Drupal\node\Entity\Node;

class MapExport
{
  public $nid;
  public $pid;
  public $vid;
  public $type;
  public $landingPageUrl;
  public $breadcrumb;
  public $language;
  public $title;
  public $uid;
  public $status;
  public $created;
  public $changed;
  public $comment;
  public $promote;
  public $sticky;
  public $tnid;
  public $translate;
  public $uuid;

  function __construct()
  {
  }

  function setValuesEn($nodeId, $pid, $title, $breadcrumb, $landingPageUrl, $type, $lang = 'en', $langOrig = 'en')
  {
    $DEBUG = FALSE;
    if ($DEBUG) {
      if ($fp = fopen('DEBUG_methods.txt', 'a')) {
        fwrite($fp, print_r($nodeId, TRUE) . "\r\n");
        fclose($fp);
      }
    }
    $this->nid = $nodeId;
    $this->type = $type;
    $this->status = true;
    $this->created = time();
    $this->changed = time();
    $this->pid = $pid;
    $langOrig = 'en'; // Get english field values.
    $this->language = $langOrig; // Default to 'en' original, should be ok.
    $this->title[$lang] = $title;
    $this->breadcrumb[$lang] = $breadcrumb;
    $this->url[$lang] = $this->path_to_legacy_node($lang);
  }
  function setValuesFr($nodeId, $pid, $title, $breadcrumb, $landingPageUrl, $type, $lang = 'fr', $langOrig = 'fr')
    $this->title[$lang] = $title;
  }

  function path_to_legacy_node($langcode) {
    if ($langcode == 'en') {
      return $path = 'https://intranet.agr.gc.ca/agrisource/eng?' . $this->nid;
    } else {
      return $path = 'https://intranet.agr.gc.ca/agrisource/fra?' . $this->nid;
    }
  }

  function to_json()
  {
    $data = array();
    $fields = array(
      'nid','pid','type','language','status','created','changed','title','breadcrumb','landingPageUrl'
//      'nid','vid','type','language','node_title','uid','status','created','changed','comment','promote','sticky',
//      'tnid','translate','uuid','date_released','news_category','location','title','title_format',
//      'body','summary','body_format','image_caption','path','image_caption_format',
//      'metatag','fid','width','height','news_image','news_thumbnail','override_image_sharing',
    );
    foreach ($fields as $fld) {
      $data[$fld] = isset($this->$fld) ? $this->$fld : null;
    }
    return json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
  }
}
