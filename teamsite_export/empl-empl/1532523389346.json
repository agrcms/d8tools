{
    "dcr_id": "1532523389346",
    "lang": "en",
    "title": {
        "en": "Senior Policy Analyst",
        "fr": "Analyste principal des politiques"
    },
    "modified": "2018-07-25 00:00:00.0",
    "issued": "2018-07-25 00:00:00.0",
    "type_name": "intra-intra/empl-empl",
    "node_id": "1279029955397",
    "layout_name": null,
    "dc_date_created": "2018-07-25",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-07-25",
            "fr": "2018-07-25"
        },
        "modified": {
            "en": "2018-07-25",
            "fr": "2018-07-25"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Senior Policy Analyst",
            "fr": "Analyste principal des politiques"
        },
        "subject": {
            "en": "staffing",
            "fr": "dotation en personnel"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "notice",
            "fr": "avis"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n<div>\n<div><strong>\u00a0</strong></div>\nEmployees of Agriculture and Agri-Food Canada (AAFC) at the EC-05 or EC-06 (and equivalents) are invited to express their interest in a 12 month assignment with possibility of extension as part of the Business Risk Management (BRM) team within the Policy, Planning and Integration (PPI) Directorate in Strategic Policy Branch (SPB).</div>\n<p>The BRM team is responsible for liaising with the provinces, other government departments, and industry by playing a challenge function, undertaking analysis and research and providing policy recommendations on the suite of BRM programs.</p>\n<p>The position of policy advisor requires strategic individuals with strong analytical, research and writing skills. The successful candidate will conduct policy analysis and provide strategic advice to senior management and the Minister. Primary responsibilities include:</p>\n<ul>\n<li>\n<p>Understanding the issues affecting the agriculture sector and providing strategic policy advice on BRM programs, and/or strategic initiatives, including around the implementation of the action plan agreed by Ministers in response to the recent Review of BRM,</p>\n</li>\n<li>\n<p>Participating in departmental working groups, the Federal/Provincial/Territorial (FPT) BRM Policy Working Group and potentially other inter-governmental or inter-departmental groups to support policy integration and the policy challenge function related to BRM and the broader agriculture and agri-food portfolio,</p>\n</li>\n<li>\n<p>Understanding analysis and research on emerging BRM and other Canadian Agricultural Partnership (CAP) issues,</p>\n</li>\n<li>\n<p>Preparing Memoranda to Cabinet, policy papers, briefing notes, and speaking points, as necessary,</p>\n</li>\n<li>\n<p>Working collaboratively with others under extremely tight deadlines and managing a sometimes challenging workload.</p>\n</li>\n</ul>\n<p><strong>\u00a0<strong>\u00a0</strong></strong></p>\n<p><strong>Education:</strong></p>\n<ul>\n<li>\n<p>Graduation with a degree from a recognized post-secondary institution with acceptable specialization in economics, business, finance, agriculture, sociology or statistics.\n</p>\n</li>\n<li>\n<p>\nCandidates must always have a degree. The courses for the specialization must be acceptable and may have been taken at a recognized post-secondary institution, but not necessarily within a degree program in the required specialization. The specialization may also be obtained through an acceptable combination of education, training and/or experience.\n</p>\n</li>\n</ul>\n<p>\n\t<strong>\u00a0</strong></p>\n<p><strong>The ideal candidate should have the following essential experience and knowledge:</strong></p>\n<ul>\n<li>\n<p>Experience participating in projects with demanding deadlines.</p>\n</li>\n<li>\n<p>Experience in conducting policy analysis, policy position papers, and/or policy-related research.</p>\n</li>\n<li>\n<p>Experience in providing briefing materials including strategic advice and/or recommendations on economic, social or environmental issues for Senior Management (ADM or higher).</p>\n</li>\n<li>\n<p>Experience in working cooperatively with partners and stakeholders including industry, other government departments, provincial departments, national or international organizations or portfolio agencies on horizontal issues.</p>\n</li>\n<li>\n<p>Knowledge of AAFC priorities, objectives, structure and processes.</p>\n</li>\n</ul>\n<p><strong>\u00a0</strong></p>\n<p><strong>Asset Qualifications: </strong></p>\n<ul>\n<li>\n<p>Experience working with BRM programs or strategic initiatives (non-BRM programs).</p>\n</li>\n<li>\n<p>Experience participating in multi-disciplinary working groups.</p>\n</li>\n<li>\n<p>\nExperience in working with Central Agencies.</p>\n</li>\n<li>\n<p>\nKnowledge of primary agriculture, including risk management.</p>\n</li>\n<li>\n<p>\nKnowledge of BRM programs.</p>\n</li>\n<li>\n<p>Knowledge of economic, or sociological or statistical principles, practices, techniques, tools and/or methods.</p>\n</li>\n<li>\n<p>Knowledge of external partners and/or stakeholders and/or fora (e.g., governmental and non-governmental organizations, provinces, other departments, etc.) pertaining to field of work.</p>\n</li>\n</ul>\n<p><strong>\u00a0</strong></p>\n<p><strong>Language Requirement:</strong> </p>\n<ul>\n<li>\n<p>Bilingual Imperative BBB/BBB, English Essential, or French Essential</p>\n</li>\n</ul>\n<p><strong>\u00a0</strong></p>\n<p><strong>Conditions of Employment:</strong>\n\t</p>\n<ul>\n<li>\n<p>Security: Secret clearance is required.</p>\n</li>\n<li>\n<p>Ability and willingness to work overtime on short notice.</p>\n</li>\n<li>\n<p>Willing and able to travel.</p>\n</li>\n</ul>\n<p>\n<strong>\u00a0</strong></p>\n<p><strong>Organizational Needs:</strong>\n\t</p>\n<ul>\n<li>\n<p>In support of achieving a diversified workforce, consideration may be given to candidates self-identifying as belonging to one of the following Employment Equity groups: Indigenous Peoples, Persons with a Disability, Visible Minorities, Women.</p>\n</li>\n</ul>\n<p><strong>\u00a0</strong></p>\n<p><strong>Applications:</strong></p>\n<p>Interested AAFC employees are encouraged to forward their resume and cover letter to Julie Levert (julie.levert@canada.ca). Candidates must clearly demonstrate IN THEIR COVER LETTER how they meet the essential qualifications and any applicable asset qualification(s). Your cover letter must also clearly identify:</p>\n<ul>\n<li>\n<p>Your tenure (e.g., term or indeterminate)</p>\n</li>\n<li>\n<p>Your substantive group and level</p>\n</li>\n<li>\n<p>Your language profile</p>\n</li>\n<li>\n<p>Your security level</p>\n</li>\n</ul>\n<p>\u00a0</p>\n<p>Should you have any questions about the position specifically, please contact Jody Proctor (jody.proctor2@canada.ca or 613-773-2494).</p>\n<p>\u00a0</p>\n",
        "fr": "\n<p>\u00a0</p>\n<p>Les employ\u00e9s d'Agriculture et Agroalimentaire Canada (AAC) qui occupent un poste EC-05 ou EC-06 (ou l\u2019\u00e9quivalent) sont invit\u00e9s \u00e0 exprimer leur int\u00e9r\u00eat pour une affectation de 12 mois avec possibilit\u00e9 de prolongation au sein de l'\u00e9quipe de Gestion des risques de l'entreprise (GRE) de la Direction de la planification et de l'int\u00e9gration des politiques (DPIP), Direction g\u00e9n\u00e9rale des politiques strat\u00e9giques (DGPS).</p>\n<p>L'\u00e9quipe de GRE est charg\u00e9e d'assurer la liaison avec les provinces, les autres minist\u00e8res et l'industrie en jouant un r\u00f4le d\u2019analyste critique, en proc\u00e9dant \u00e0 des analyses et \u00e0 des recherches, et en fournissant des recommandations strat\u00e9giques sur l'ensemble des programmes de la GRE.</p>\n<p>Le poste d\u2019analyste des politiques requiert de solides comp\u00e9tences en analyse, en recherche et en r\u00e9daction. La personne choisie pour le poste analysera des politiques et donnera des conseils strat\u00e9giques \u00e0 la haute direction et au ministre. Les principales responsabilit\u00e9s comprennent les suivantes :</p>\n<ul>\n<li>\n<p>Comprendre les enjeux qui touchent le secteur de l\u2019agriculture et fournir des conseils strat\u00e9giques sur les programmes de GRE et/ou les initiatives strat\u00e9giques, dont la mise en \u0153uvre du plan d'action convenu par les ministres en r\u00e9ponse au r\u00e9cent examen de la GRE;</p>\n</li>\n<li>\n<p>Participer aux groupes de travail minist\u00e9riels, au groupe de travail f\u00e9d\u00e9ral-provincial-territorial (FPT) sur la GRE et possiblement \u00e0 d\u2019autres groupes intergouvernementaux ou interminist\u00e9riels pour appuyer l\u2019int\u00e9gration des politiques et la fonction d\u2019examen critique des politiques li\u00e9es \u00e0 la GRE ainsi qu\u2019au portefeuille plus g\u00e9n\u00e9ral de l\u2019agriculture et de l\u2019agroalimentaire.</p>\n</li>\n<li>\n<p>Comprendre l\u2019analyse et la recherche ayant trait aux nouveaux probl\u00e8mes qui touchent la GRE et d\u2019autres partenariats agricoles canadiens (PAC);</p>\n</li>\n<li>\n<p>Pr\u00e9parer des m\u00e9moires au Cabinet, des documents d\u2019orientation, des notes d\u2019information et des notes d\u2019allocution, au besoin.</p>\n</li>\n<li>\n<p>Travailler en collaboration avec d\u2019autres en respectant des d\u00e9lais extr\u00eamement serr\u00e9s et g\u00e9rer une charge de travail avec laquelle il est parfois difficile de composer.</p>\n</li>\n</ul>\n<p>\u00a0</p>\n<p><strong>\u00c9tudes:</strong></p>\n<ul>\n<li>Dipl\u00f4me d\u2019un \u00e9tablissement d\u2019enseignement postsecondaire reconnu avec sp\u00e9cialisation acceptable en \u00e9conomie, en affaires, en finance, en agriculture, en sociologie ou en statistique.<strong> </strong></li>\n<li>Les candidats doivent obligatoirement poss\u00e9der un dipl\u00f4me. Les cours de sp\u00e9cialisation doivent \u00eatre acceptables et peuvent avoir \u00e9t\u00e9 suivis dans un \u00e9tablissement d\u2019enseignement postsecondaire reconnu, mais pas n\u00e9cessairement dans le cadre d\u2019un programme d\u2019\u00e9tudes postsecondaires dans la sp\u00e9cialit\u00e9 demand\u00e9e. Une combinaison acceptable d\u2019\u00e9tudes, de formation et/ou d\u2019exp\u00e9rience peut tenir lieu de sp\u00e9cialisation.<strong><br>\n\u00a0\u00a0</strong></li>\n</ul>\n<p>\n\t<strong>Le candidat id\u00e9al r\u00e9pond aux </strong><strong>exigences relatives \u00e0 l\u2019exp\u00e9rience et aux connaissances essentielles suivantes </strong><strong>:</strong></p>\n<ul>\n<li>\n<p>\n\tExp\u00e9rience de la participation \u00e0 des projets assortis d\u2019\u00e9ch\u00e9ances serr\u00e9es.</p>\n</li>\n<li>\n<p>\n\tExp\u00e9rience de l\u2019analyse de politiques, de la r\u00e9daction d\u2019expos\u00e9s de principes et/ou de la recherche se rapportant \u00e0 des politiques.</p>\n</li>\n<li>\n<p>Exp\u00e9rience de la production de documents d\u2019information \u00e0 la haute direction (SMA ou fonctionnaires d\u2019un niveau plus \u00e9lev\u00e9), dont des conseils et/ou des recommandations strat\u00e9giques sur des enjeux \u00e9conomiques, sociaux ou environnementaux.</p>\n</li>\n<li>\n<p>Exp\u00e9rience de la collaboration \u00e0 des dossiers horizontaux avec des partenaires et des intervenants, dont l\u2019industrie, d\u2019autres minist\u00e8res du gouvernement, des minist\u00e8res provinciaux, des organismes nationaux ou internationaux ou des organismes du portefeuille.</p>\n</li>\n<li>\nConnaissance des priorit\u00e9s, des objectifs, de la structure et des processus d\u2019AAC.</li>\n</ul>\n<p>\u00a0</p>\n<p><strong>Qualifications\u00a0constituant un atout</strong></p>\n<ul>\n<li>\n<p>\n\tExp\u00e9rience de travail avec des programmes de GRE ou des initiatives strat\u00e9giques (autres que les programmes de GRE).</p>\n</li>\n<li>\n<p>Exp\u00e9rience de la participation \u00e0 des groupes de travail multidisciplinaires.</p>\n</li>\n<li>\n<p>\n\tExp\u00e9rience de travail avec des organismes centraux.</p>\n</li>\n<li>\n<p>Connaissance de l'agriculture primaire, y compris la gestion des risques.</p>\n</li>\n<li>\n<p>\n\tConnaissance des programmes de GRE.</p>\n</li>\n<li>\n<p>Connaissance des principes, pratiques, techniques, outils et/ou m\u00e9thodes se rapportant \u00e0 l\u2019\u00e9conomie, la sociologie ou la statistique.</p>\n</li>\n<li>\n<p>\n\tConnaissance des partenaires, des intervenants ou des forums externes (p. ex. : organismes gouvernementaux ou non gouvernementaux, provinces et autres minist\u00e8res) en lien avec le domaine de travail.</p>\n</li>\n</ul>\n<p><strong>\u00a0</strong></p>\n<p><strong>Exigences linguistiques :</strong></p>\n<ul>\n<li>Poste bilingue, niveau BBB/BBB imp\u00e9ratif \u00e0 la nomination; anglais essentiel ou fran\u00e7ais essentiel.</li>\n</ul>\n<p>\u00a0</p>\n<p><strong>Conditions d\u2019emploi</strong></p>\n<ul>\n<li>\n<p>\n\tD\u00e9tenir une cote de s\u00e9curit\u00e9 de niveau \u00ab Secret \u00bb.</p>\n</li>\n<li>\n<p>\n\t\u00catre dispos\u00e9 et apte \u00e0 faire des heures suppl\u00e9mentaires \u00e0 court pr\u00e9avis.</p>\n</li>\n<li>\n<p>\n\tConsentir \u00e0 voyager et \u00eatre en mesure de le faire.</p>\n</li>\n</ul>\n<p>\u00a0</p>\n<p><strong>Besoins organisationnels </strong></p>\n<ul>\n<li>\n\tAux fins de la diversification de l\u2019effectif, la pr\u00e9f\u00e9rence pourrait \u00eatre accord\u00e9e aux candidats qui auront indiqu\u00e9 leur appartenance \u00e0 l\u2019un des groupes suivants, vis\u00e9s par l\u2019\u00e9quit\u00e9 en mati\u00e8re d\u2019emploi : Autochtones, personnes handicap\u00e9es, minorit\u00e9s visibles et femmes.</li>\n</ul>\n<p>\u00a0</p>\n<p><strong>Demandes</strong></p>\n<p>Les employ\u00e9s d\u2019AAC int\u00e9ress\u00e9s par la pr\u00e9sente possibilit\u00e9 doivent envoyer leur curriculum vitae accompagn\u00e9 d\u2019une lettre de pr\u00e9sentation \u00e0 Julie Levert (julie.levert@canada.ca). Les candidats doivent clairement d\u00e9montrer dans leur LETTRE DE PR\u00c9SENTATION comment ils satisfont aux exigences relatives aux qualifications essentielles et \u00e0 l\u2019une ou l\u2019autre des qualifications constituant un atout. Ils doivent \u00e9galement indiquer clairement dans la lettre de pr\u00e9sentation :\n</p>\n<ul>\n<li>\n<p>\n\tla dur\u00e9e de leur emploi (c.-\u00e0-d. d\u00e9termin\u00e9e ou ind\u00e9termin\u00e9e);</p>\n</li>\n<li>\n<p>leur groupe et niveau de titularisation;</p>\n</li>\n<li>\n<p>leur profil linguistique;</p>\n</li>\n<li>\n<p>leur niveau de s\u00e9curit\u00e9.</p>\n</li>\n</ul>\n<p>\u00a0</p>\n<p>Si vous avez des questions sur ce poste, veuillez communiquer avec Jody Proctor (jody.proctor2@canada.ca ou 613-773-2494).</p>\n<p>\u00a0</p>\n\n"
    },
    "breadcrumb": {
        "en": "Senior Policy Analyst",
        "fr": "Analyste principal des politiques"
    },
    "empl": {
        "type": {
            "en": "Assignment, Acting",
            "fr": "Affectation, Int\u00e9rimaire"
        },
        "classification": {
            "en": "EC-6 and equivalent",
            "fr": "EC-6 et \u00e9quivalent"
        },
        "branch": {
            "en": "Strategic Policy Branch",
            "fr": "Direction g\u00e9n\u00e9rale des politiques strat\u00e9giques"
        },
        "locations": {
            "en": "National Capitol Region",
            "fr": "R\u00e9gion de la capitale nationale"
        },
        "date_closing": {
            "en": "2018-08-08",
            "fr": "2018-08-08"
        },
        "open_to": {
            "en": "AAFC employees at the EC-05 or EC-06 group and level (and equivalents)",
            "fr": "Employ\u00e9s d'AAC du groupe et du niveau EC-05 ou EC-06 (ou l'\u00e9quivalent)"
        },
        "name": {
            "en": "Julie.levert@canada.ca",
            "fr": "Julie.levert@canada.ca"
        },
        "email": {
            "en": "Julie.levert@canada.ca",
            "fr": "Julie.levert@canada.ca"
        }
    }
}