{
    "dcr_id": "1593453407703",
    "lang": "en",
    "title": {
        "en": "Policy Analyst - Policy and Coordination ",
        "fr": "Analyste des politiques - Politiques et coordination"
    },
    "modified": "2020-06-30 00:00:00.0",
    "issued": "2020-06-30 00:00:00.0",
    "type_name": "intra-intra/empl-empl",
    "node_id": "1279029955397",
    "layout_name": null,
    "dc_date_created": "2020-06-30",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2020-06-30",
            "fr": "2020-06-30"
        },
        "modified": {
            "en": "2020-06-30",
            "fr": "2020-06-30"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Policy Analyst - Policy and Coordination ",
            "fr": "Analyste des politiques - Politiques et coordination"
        },
        "subject": {
            "en": "staffing",
            "fr": "dotation en personnel"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "notice",
            "fr": "avis"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n<p><strong>\u00a0</strong></p>\n<p><strong>Statement of Merit Criteria &amp; Conditions of Employment\u00a0</strong></p>\n<p>This position is located within the Policy, Planning and Emergency Management Division and supports the Assistant Deputy Minister in ensuring that the Branch maintains the appropriate business, human resource and operational plans to effectively carry out work undertaken in pursuit of the Branch's mandate, and represents MISB in departmental planning and reporting activities (e.g., Management, Resources and Results Structure, Report on Plans and Priorities, Departmental Performance Report).\u00a0</p>\n<p>The Policy and Coordination team provides policy analysis for the Branch, is responsible for cross-cutting policy development and advice, leads on the Department\u2019s International Strategy, and ensures that initiatives and activities are in line with the priorities of the Department and the Government more broadly.</p>\n<p>\u00a0</p>\n<p><strong>Essential Qualifications:</strong></p>\n<p><strong><br>Education </strong></p>\n<p>Graduation with a degree from a recognized post-secondary institution with acceptable specialization in economics, sociology or statistics.*</p>\n<p>*Candidates must always have a <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#deg\">degree</a>. The courses for the <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#spec\">specialization</a> must be <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#acce\">acceptable</a> and may have been taken at a <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#rpsi\">recognized post-secondary institution</a>, but not necessarily within a <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#deg\">degree</a> program in the required <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#spec\">specialization</a>. The <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#spec\">specialization</a> may also be obtained through an <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/staffing/qualification-standards/core.html#acce\">acceptable</a> combination of education, training and/or experience.</p>\n<p>\u00a0</p>\n<p><strong>Experience </strong></p>\n<ul>\n<li>\n<p>Significant experience** in conducting analysis of complex strategic issues and presenting options and recommendations to support senior management*** decision-making.</p>\n</li>\n<li>\n<p>Experience developing strategic approaches in a complex environment and providing advice on optimal processes to achieve them.</p>\n</li>\n<li>\n<p>Experience in managing conflicting priorities across various concurrent responsibilities, and producing results under tight timelines.</p>\n</li>\n</ul>\n<ul>\n<li>\n<p>Significant experience** in developing and maintaining relationships with government and/or industry partners.</p>\n</li>\n</ul>\n<p>** Significant Experience means experience that is a core part of the candidate\u2019s employment for a period of time that would provide the depth and breadth of experience normally obtained in a minimum of two (2) years.</p>\n<p>***Senior management includes Director Level and above.</p>\n<p>\u00a0</p>\n<p><strong>Knowledge</strong></p>\n<p>Knowledge of the key issues and trends facing the Canadian agriculture and agri-food sector and Agriculture and Agri-Food Canada\u2019s Priorities.</p>\n<p>\u00a0</p>\n<p><strong>Competencies </strong></p>\n<p>Strategic and Analytical Thinking</p>\n<p>Networking and Relationship Building</p>\n<p>Interactive Communication (Verbal and Written)</p>\n<p>Initiative</p>\n<p>\u00a0</p>\n<p><strong>Language Requirement </strong></p>\n<p>Bilingual Imperative: BBB/BBB</p>\n<p>\u00a0</p>\n<p><strong>Asset Qualifications:</strong></p>\n<p>Graduation with a Master\u2019s degree from a recognized post-secondary institution with specialization in a field relevant to the position.</p>\n<p>Experience working on cross \u2013sectoral issues in the sector.</p>\n<p>Experience working in multidisciplinary teams with diverse reporting relationships.</p>\n<p>\u00a0</p>\n<p><strong>Organizational Needs:</strong></p>\n<p>In support of achieving a diversified workforce, consideration may be given to candidates self-identifying as belonging to one of the following Employment Equity groups: Aboriginal peoples, Persons with a Disability; Visible Minorities, Women.</p>\n<p>\u00a0</p>\n<p><strong>Conditions of Employment: </strong></p>\n<p>Security Requirements: Secret Clearance</p>\n<p>\u00a0</p>\n<p><strong>Operational Requirements:</strong></p>\n<p>Ability to work under tight deadlines with competing demands.</p>\n<p>Ability and willingness to work overtime.</p>\n<p>\u00a0</p>\n<p><strong>Applications:</strong></p>\n<p>Interested employees are encouraged to provide:</p>\n<ul>\n<li>Resume and a cover letter outlining their interest in the position and how they meet the criteria for the position; and</li>\n<li>Resumes must clearly identify substantive group and level, tenure, current SLE profile/validity.</li>\n</ul>\n<p>Documents should be sent to Amrane Boumghar at Amrane.Boumghar@Canada.ca by July 13, 2020.</p>\n<p>\u00a0</p>\n<p><strong>Notes:</strong></p>\n<ul>\n<li>*IMPORTANT*: ECDP Participants at the substantive EC-04 group and level are not eligible for acting opportunities and therefore, would not be able to present their candidacy for this position.</li>\n<li>Deployments cannot change the tenure of an employee (i.e.: term to indeterminate) or constitute a promotion.</li>\n<li>Communication for this process will be sent via email.</li>\n<li>A pool of qualified candidates may be established and may be used to staff similar positions on an acting, assignment or deployment basis.</li>\n<li>Your cover letter may be used to assess your communication and writing skills.</li>\n<li>Candidates may be assessed through an interview.</li>\n<li>Reference checks may be sought.</li>\n<li>Preference will be given to candidates at-level and equivalent.</li>\n</ul>\n<p>\u00a0</p>\n",
        "fr": "\n<p>\u00a0</p>\n<p><b>\u00c9nonc\u00e9 des crit\u00e8res de m\u00e9rite et des conditions d'emploi</b><br>\n<br>\nCe poste est situ\u00e9 au sein de la Division des politiques, de la planification et de la gestion des urgences et aide le sous-ministre adjoint \u00e0 veiller \u00e0 ce que la Direction maintienne les plans d'affaires, de ressources humaines et op\u00e9rationnels appropri\u00e9s pour ex\u00e9cuter efficacement les travaux entrepris dans le cadre de son mandat et repr\u00e9sente la DGSIM dans les activit\u00e9s de planification et de production de rapports du Minist\u00e8re (p. ex. gestion, structure des ressources et des r\u00e9sultats, rapport sur les plans et les priorit\u00e9s, rapport minist\u00e9riel sur le rendement).<br>\n<br>\nL'\u00e9quipe des politiques et de la coordination fournit une analyse des politiques \u00e0 la Direction, est responsable de l'\u00e9laboration et des conseils intersectoriels des politiques, dirige la strat\u00e9gie internationale du Minist\u00e8re et s'assure que les initiatives et les activit\u00e9s sont conformes aux priorit\u00e9s du Minist\u00e8re et du gouvernement en g\u00e9n\u00e9ral. .<br>\n<br>\n<br><b>Qualifications essentielles:</b></p>\n<p><b><br>\u00c9ducation</b><br>\nDipl\u00f4me avec un dipl\u00f4me d'un \u00e9tablissement postsecondaire reconnu avec sp\u00e9cialisation acceptable en \u00e9conomie, sociologie ou statistique. *<br>\n* Les candidats doivent toujours avoir un dipl\u00f4me. Les cours de sp\u00e9cialisation doivent \u00eatre acceptables et peuvent avoir \u00e9t\u00e9 suivis dans un \u00e9tablissement postsecondaire reconnu, mais pas n\u00e9cessairement dans le cadre d'un programme menant \u00e0 un dipl\u00f4me dans la sp\u00e9cialisation requise. La sp\u00e9cialisation peut \u00e9galement \u00eatre obtenue gr\u00e2ce \u00e0 une combinaison acceptable d'\u00e9tudes, de formation et / ou d'exp\u00e9rience.<br>\n<br>\n<br><b>Exp\u00e9rience</b><br>\n\u2022 Exp\u00e9rience significative ** dans l'analyse de probl\u00e8mes strat\u00e9giques complexes et la pr\u00e9sentation d'options et de recommandations pour soutenir la prise de d\u00e9cision de la haute direction ***.<br>\n\u2022 Exp\u00e9rience en d\u00e9veloppement d'approches strat\u00e9giques dans un environnement complexe et en fournissant des conseils sur les processus optimaux pour les atteindre.<br>\n\u2022 Exp\u00e9rience de la gestion de priorit\u00e9s conflictuelles entre diverses responsabilit\u00e9s concurrentes et de la production de r\u00e9sultats dans des d\u00e9lais serr\u00e9s.<br>\n\u2022 Exp\u00e9rience significative ** dans le d\u00e9veloppement et le maintien de relations avec le gouvernement et / ou les partenaires de l'industrie.<br>\n** Une exp\u00e9rience significative signifie une exp\u00e9rience qui fait partie int\u00e9grante de l'emploi du candidat pendant une p\u00e9riode de temps qui fournirait la profondeur et l'\u00e9tendue de l'exp\u00e9rience normalement acquise en un minimum de deux (2) ans.<br>\n*** La haute direction comprend le niveau de directeur et au-dessus.<br>\n<br>\n<br>\n<b>Connaissance</b><br>\nConnaissance des principaux enjeux et tendances auxquels fait face le secteur canadien de l'agriculture et de l'agroalimentaire et des priorit\u00e9s d'Agriculture et Agroalimentaire Canada.<br>\n<br>\n<b>Comp\u00e9tences</b><br>\nPens\u00e9e strat\u00e9gique et analytique<br>\nR\u00e9seautage et \u00e9tablissement de relations<br>\nCommunication interactive (verbale et \u00e9crite)<br>\nInitiative<br>\n<br>\n<b>Exigence linguistique</b><br>\nImp\u00e9ratif bilingue: BBB / BBB<br>\n<br>\n<b>Qualifications constituant un atout:</b><br>\nDipl\u00f4me avec ma\u00eetrise dans un \u00e9tablissement postsecondaire reconnu avec sp\u00e9cialisation dans un domaine pertinent au poste.<br>\nExp\u00e9rience de travail sur des questions intersectorielles dans le secteur.<br>\nExp\u00e9rience de travail au sein d'\u00e9quipes multidisciplinaires avec diverses relations hi\u00e9rarchiques.<br>\n<br>\n<b>Besoins organisationnels:</b><br>\nAfin de favoriser la diversification de la main-d'\u0153uvre, les candidats peuvent s'identifier comme appartenant \u00e0 l'un des groupes d'\u00e9quit\u00e9 en mati\u00e8re d'emploi suivants: Autochtones, personnes handicap\u00e9es; Minorit\u00e9s visibles, femmes.<br>\n<br>\n<b>Conditions d'emploi:</b><br>\nExigences de s\u00e9curit\u00e9: autorisation secr\u00e8te<br>\n<br>\n<b>Exigences op\u00e9rationnelles:</b><br>\nCapacit\u00e9 \u00e0 travailler dans des d\u00e9lais serr\u00e9s avec des demandes concurrentes.<br>\nCapacit\u00e9 et volont\u00e9 de faire des heures suppl\u00e9mentaires.<br>\n<br>\n<b>Applications:</b><br>\nLes employ\u00e9s int\u00e9ress\u00e9s sont encourag\u00e9s \u00e0 fournir:<br>\n\u2022 CV et lettre de motivation d\u00e9crivant leur int\u00e9r\u00eat pour le poste et comment ils r\u00e9pondent aux crit\u00e8res du poste; et<br>\n\u2022 Les curriculum vitae doivent clairement identifier le groupe et le niveau substantiel, la dur\u00e9e du mandat, le profil / validit\u00e9 actuel du LES<br>\n\u2022 Les documents doivent \u00eatre envoy\u00e9s \u00e0 Amrane Boumghar \u00e0 Amrane.Boumghar@Canada.ca avant le 13 juillet 2020.<br>\n<br>\n<b>Remarques:</b><br>\n\u2022 * IMPORTANT *: Les participants \u00e0 l'ECDP au niveau du groupe et du niveau EC-04 ne sont pas \u00e9ligibles pour des opportunit\u00e9s d'acteur et ne seront donc pas en mesure de pr\u00e9senter leur candidature pour ce poste.<br>\n\u2022 Les mutations ne peuvent pas changer le mandat d'un employ\u00e9 (c.-\u00e0-d.: Dur\u00e9e ind\u00e9termin\u00e9e) ou constituer une promotion.<br>\n\u2022 La communication pour ce processus sera envoy\u00e9e par e-mail.<br>\n\u2022 Un bassin de candidats qualifi\u00e9s peut \u00eatre \u00e9tabli et peut \u00eatre utilis\u00e9 pour doter des postes similaires sur une base int\u00e9rimaire, d'affectation ou de d\u00e9ploiement.<br>\n\u2022 Votre lettre de motivation peut \u00eatre utilis\u00e9e pour \u00e9valuer vos comp\u00e9tences en communication et en r\u00e9daction.<br>\n\u2022 Les candidats peuvent \u00eatre \u00e9valu\u00e9s lors d'un entretien.<br>\n\u2022 Des v\u00e9rifications des r\u00e9f\u00e9rences peuvent \u00eatre demand\u00e9es.<br>\n\u2022 La pr\u00e9f\u00e9rence sera accord\u00e9e aux candidats de niveau et \u00e9quivalent.</p>\n\n"
    },
    "breadcrumb": {
        "en": "Policy Analyst - Policy and Coordination",
        "fr": "Analyste des politiques - Politiques et coordination"
    },
    "empl": {
        "type": {
            "en": "Assignment, Deployment, Acting",
            "fr": "Affectation, Mutation, Int\u00e9rimaire"
        },
        "classification": {
            "en": "EC-5 and equivalent",
            "fr": "EC-5 et \u00e9quivalent"
        },
        "branch": {
            "en": "Market and Industry Services Branch",
            "fr": "Direction g\u00e9n\u00e9rale des services \u00e0 l'industrie et aux march\u00e9s"
        },
        "locations": {
            "en": "1341 Baseline Road Ottawa, ON",
            "fr": "1341, chemin Baseline, Ottawa, ON K1A 0C5"
        },
        "date_closing": {
            "en": "2020-07-13",
            "fr": "2020-07-13"
        },
        "open_to": {
            "en": "Employees in the NCR at the EC-04,EC-05 or CO-01 substantive group and level (or equivalent)",
            "fr": "Les employ\u00e9s de la RCN du groupe fonctionnel EC-04, EC-05 ou CO-01 (ou \u00e9quivalent)"
        },
        "name": {
            "en": "Amrane Boumghar",
            "fr": "Amrane Boumghar"
        },
        "email": {
            "en": "Amrane.Boumghar@canada.ca",
            "fr": "Amrane.Boumghar@canada.ca"
        }
    }
}