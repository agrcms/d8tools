{
    "dcr_id": "1562764836984",
    "lang": "en",
    "title": {
        "en": "Twitter use for AAFC employees",
        "fr": "Utilisation de twitter pour les employ\u00e9s d\u2019AAC"
    },
    "modified": "2019-07-12 00:00:00.0",
    "issued": "2019-07-12 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1447698539491",
    "layout_name": "1 column",
    "dc_date_created": "2019-07-10",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-07-12",
            "fr": "2019-07-12"
        },
        "modified": {
            "en": "2019-07-12",
            "fr": "2019-07-12"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Twitter use for AAFC employees",
            "fr": "Utilisation de twitter pour les employ\u00e9s d\u2019AAC"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>For your convenience, this page is available in the following format:<br>\n<a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/twitter_tips-eng.pdf\">PDF (159 KB)</a></p>\n\n<div class=\"row\">\n\t<div class=\"col-md-3 pull-right  mrgn-lft-md\">\n<img src=\"https://intranet.agr.gc.ca/resources/prod/images/ccb_dgcc/the_department/presence_eng.jpg\" class=\"img-responsive pull-right  mrgn-lft-md\" alt=\"Description of this image follows\">\n\t\t\n\t\n<details>\n<summary>Description of the above image.</summary>\n<p>Example of a professional twitter account:<br>\nAggie Smith AAFC<br>\n@AggieSmithAAFC<br>\n#GoC Communications Advisor for #CdnAg. Young professional in #OttCity. My views are my own.</p>\n</details>\n<div class=\"clearfix\"></div>\n\t</div>\n\t<div class=\"mrgn-lft-md mrgn-rght-md\">\n\t\t\n<p>A collection of tips and tricks to make the most of your Twitter account</p>\n\n<h2>Key ingredients to a great twitter presence</h2>\n\n<ul>\n\t<li><b>It's accurate.</b> Say what you really do or who you are in 160 characters or less!</li>\n\t<li>It's <b>targeted to attract</b> people like you.</li>\n\t<li><b>It's humanizing.</b> Be yourself, but remember the Values and Ethics Code for the Public Sector and AAFC.</li>\n\t<li><b>It's connected.</b> Use hashtags, @s or links, but not too many.\n<p>Check out the SM Toolkit for communicators on AgriSource under <a href=\"?id=1287487121135\">Public Affairs Branch</a> &gt; <a href=\"?id=1523620215671\">Social Media</a> &gt; <a href=\"?id=1445270936396\">Social Media Resources</a></p>\n</li>\n\t<li><strong>It's recommended</strong> to add a \"Views are my own\" statement.</li>\n</ul>\n\t\t\n\t\n\n<h2>Your obligations as an AAFC employee</h2>\n\n<p>Public service employees are expected to abide by the following rules even outside of regular working hours:</p>\n\n<ul>\n\t<li>Values and Ethics Code for the Public Sector and AAFC</li>\n\t<li>AAFC Code of conduct</li>\n\t<li>Conflict of interest</li>\n\t<li>Privacy</li>\n</ul>\n\t</div>\n</div>\t\n\t\n<p>Find the details on <a href=\"?id=1287261736402\">AgriSource</a> under <a href=\"?id=1287487121135\">Public Affairs Branch</a> &gt; <a href=\"?id=1523620215671\">Social Media</a></p>\n\n<h2>Creating engaging tweets</h2>\n\n<ul>\n\t<li>Research and emulate your peers' tweets that are <b>retweeted</b> and <b>liked</b></li>\n\t<li>Use <b>Photos, videos and GIFs</b> wherever possible</li>\n\t<li>Be <b>authentic</b></li>\n\t<li>Link to online content</li>\n</ul>\n \n<h2>What should I tweet about</h2>\n\n<ul>\n\t<li>Tweet about your work\n\t\t<p>Highlight work in progress, events and engagements, or milestones/successes</p></li>\n\t<li>Retweet\n\t<ul>\n\t<li>Your team's content</li>\n\t<li>Follow your colleagues to cross-promote</li>\n\t<li>Follow AAFC senior managers, such as: <a href=\"https://twitter.com/ChrisWForbes\">@ChrisWForbes</a> <a href=\"https://twitter.com/_AnnetteGibbons\">@_AnnetteGibbons</a></li>\n</ul>\n\t</li>\n</ul>\n\n<h2>What shouldn't I tweet about</h2>\n\n<ul class=\"lst-spcd\">\n\t<li>Secret information\n\t\t<p>Details that have not yet been announced or that are related to official procurement</p></li>\n\t<li>Critical comments about the Government of Canada</li>\n\t<li>Personal bias\n\t\t<p>Don't promote one vendor, business or stakeholder over others</p></li>\n\t<li>Avoid tagging or retweeting partisan accounts\n\t\t<p>Political parties, politicians or activists</p></li>\n</ul>\n\n<h2>Example of a professional tweet</h2>\n \n<div class=\"row\">\n\t<div class=\"col-md-4\">\n<img src=\"https://intranet.agr.gc.ca/resources/prod/images/ccb_dgcc/the_department/example-exemple_v1_eng.jpg\" alt=\"Description of this image follows\">\n\n<details>\n<summary>Description of the image above.</summary>\n<p>1202 Alarm<br>\n@AndrewMDavidson<br>\nThe @AgMuseum @csa Space to Spoon exhibit is fantastic! Much content provided by @AAFC_Canada scientists working in the space domain to monitor Canada's agricultural land from orbit, including Synthetic Aperture Radar (SAR) #RADARSAT #OdySci</p>\n</details>\n</div></div>\n\n<h2>Popular #CdnAG hashtags</h2>\n\n<ul class=\"colcount-sm-2\">\n  <li>#GOC</li>\n  <li>#LeadersGC</li>\n  <li>#OpenGov</li>\n  <li>#GCStories</li>\n  <li>#DiscoverAg</li>\n  <li>#WomenInAg</li>\n  <li>#WomenInSTEM</li>\n  <li>#CETA</li>\t\n  <li>#CPTPP</li>\n  <li>#AgQC</li>\n  <li>#Harvest19</li>\n  <li>#GC</li>\n  <li>#GCDigital</li>\n  <li>#GC2020</li>\n  <li>#CdnAg</li>\n  <li>#AgScience</li>\t\n  <li>#WomenInScience</li>\n  <li>#CdnAgMission</li>\n  <li>#WestCdnAg</li>\n  <li>#Plant19</li>\t\n</ul>\n\n<p>Use our AAFC hashtag <b>#GCAggie</b> when tweeting about your work!</p>\n\n <h2>AAFC's twitter accounts</h2>\n\n<ul>\n\t<li><a href=\"https://twitter.com/AAFC_Canada\">AAFC_Canada</a></li>\n\t<li><a href=\"https://twitter.com/AAC_Canada\">AAC_Canada (in French only)</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Pour votre convenance, cette page est disponible en PDF (format de document portable)\u00a0:<br>\n<a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/the_department/twitter_tips-fra.pdf\">PDF (176 KO)</a></p>\n\n<div class=\"row\">\n\t<div class=\"col-md-3 pull-right mrgn-lft-md\">\n<img src=\"https://intranet.agr.gc.ca/resources/prod/images/ccb_dgcc/the_department/presence_fra.jpg\" class=\"img-responsive pull-right mrgn-lft-md\" alt=\"La description de l'image suit.\">\n\t\t\n\n<details>\n<summary>Description de l'image ci-dessus.</summary>\n<p>Exemple de compte twitter professionnel\u00a0:<br>\nAgromane Tremblay<br>\n@AgromaneTremblayAAC<br>\nConseiller en communications du #GC pour l'#AgCan. Jeune professionnel d'#Ottawa.<br>\nMon compte ne refl\u00e8te que mes opinions personnelles.</p>\n</details>\n<div class=\"clearfix\"></div>\n\t</div>\n\t<div class=\"mrgn-lft-md mrgn-rght-md\">\n\t\t\n<p>Un recueil de conseils et d'astuces pour tirer le maximum de votre compte Twitter</p>\n\n<h2>\u00c9l\u00e9ments cl\u00e9s pour une pr\u00e9sence g\u00e9niale sur Twitter</h2>\n\n<ul>\n\t<li><b>Elle est exacte.</b> Indiquez ce que vous faites et qui vous \u00eates r\u00e9ellement en 160 caract\u00e8res ou moins</li>\n\t<li>Elle est <b>cibl\u00e9e pour attirer</b> des personnes comme vous.</li>\n\t<li><b>Elle vous donne un visage.</b> Soyez vous-m\u00eame, mais gardez en t\u00eate le Code de valeurs et d'\u00e9thique de la fonction publique et d'AAC.</li>\n\t<li><b>Elle est connect\u00e9e.</b> Utilisez des mots-clics, des @ ou des liens; mais pas trop.\n\t<p>Jetez un coup d'oeil \u00e0 la trousse d'outils de communication des m\u00e9dias\nsociaux sur Agri-source, sous <a href=\"?id=1287487121135\">Direction g\u00e9n\u00e9rale des affaires publiques</a> &gt; <a href=\"?id=1523620215671\">m\u00e9dias sociaux</a> &gt; <a href=\"?id=1445270936396\">Ressources li\u00e9s aux m\u00e9dias sociaux</a></p>\n\t</li>\n\t<li><strong>Il est recommand\u00e9</strong> d'ajouter un \u00e9nonc\u00e9 indiquant que vos opinions refl\u00e8tent uniquement votre propre exp\u00e9rience.</li>\n</ul>\n\t\t\n\t\t\n\n<h2>Vos obligations \u00e0 titre d'employ\u00e9 d'AAC</h2>\n\n<p>Les employ\u00e9s de la fonction publique doivent adh\u00e9rer aux r\u00e8gles suivantes, m\u00eame en dehors des heures de travail\u00a0:</p>\n\n<ul>\n\t<li>Code de valeurs et d'\u00e9thique de la fonction publique</li>\n\t<li>Code de conduite au sein d'AAC</li>\n\t<li>Conflit d'int\u00e9r\u00eats</li>\n\t<li>Protection de la vie priv\u00e9e</li>\n</ul>\n\t</div>\n</div>\n\n<p> Vous trouverez de plus amples d\u00e9tails sur <a href=\"?id=1287261736402\">AgriSource</a> sous <a href=\"?id=1287487121135\">Direction g\u00e9n\u00e9rale des affaires publiques</a> &gt; <a href=\"?id=1523620215671\">m\u00e9dias sociaux</a></p>\n\n<h2>R\u00e9diger des gazouillis <b>attrayants</b></h2>\n\n<ul>\n\t<li>Cherchez les gazouillis de vos coll\u00e8gues qui ont \u00e9t\u00e9 <b>partag\u00e9s</b> et <b>aim\u00e9s</b> et utilisez-les comme mod\u00e8les.</li>\n\t<li>Utilisez <b>des photos, des vid\u00e9os et des GIF</b> lorsque c'est possible.</li>\n\t<li>Restez <b>authentique</b>.</li>\n\t<li>Publiez des liens vers du contenu en ligne.</li>\n</ul>\n\n<h2>Les sujets que je devrais aborder sur Twitter</h2>\n\n<ul>\n\t<li>Des gazouillis \u00e0 propos de votre travail\n\t\t<p>Soulignez le travail en cours, les \u00e9v\u00e9nements et les engagements, ou les jalons ou  r\u00e9ussites</p></li>\n\t<li>Partagez\n\t<ul>\n\t<li>Le contenu Twitter de votre \u00e9quipe</li>\n\t<li>Suivez vos coll\u00e8gues afin de faire la promotion crois\u00e9e.</li>\n\t<li>Suivez les hauts dirigeants d'AAC, comme\u00a0: <a href=\"https://twitter.com/ChrisWForbes\">@ChrisWForbes</a> <a href=\"https://twitter.com/_AnnetteGibbons\">@_AnnetteGibbons</a></li>\n</ul>\n\t</li>\n</ul>\n\n<h2>Les sujets que je devrais \u00e9viter sur Twitter</h2>\n\n<ul class=\"lst-spcd\">\n\t<li>Les renseignements secrets\n\t\t<p>Fournir des d\u00e9tails qui n'ont pas encore \u00e9t\u00e9 annonc\u00e9s ou qui portent sur des achats officiels.</p></li>\n\t<li>Les remarques critiques sur le Gouvernement du Canada</li>\n\t<li>Les pr\u00e9juges personnels\n\t\t<p>Ne pas faire la promotion d'un fournisseur, d'une entreprise ou d'un intervenant en particulier.</p></li>\n\t<li>L'identification ou le partage de comptes partisans\n\t\t<p>Des comptes de partis politiques, de politiciens ou de militants.</p></li>\n</ul>\n\n<h2>Exemple d'un gazouillis professionnel</h2>\n \n\n<div class=\"row\">\n\t<div class=\"col-md-4\">\n<img src=\"https://intranet.agr.gc.ca/resources/prod/images/ccb_dgcc/the_department/example-exemple_v1_fra.jpg\" alt=\"La description de cette image suit.\">\n\n<details>\n<summary>Description de l'image ci-dessus.</summary>\n<p>Sara Biovin-Chabot<br>\n@SaraBoivinC<br>\nLes travaux des chercheuses d\u2019#AAC_Qc en vedette \u00e0 la #jsciCRAAQ hier @AAC_Canada #25joursdeFemmes #Femmesensciences.</p>\n</details>\n</div></div>\n\n<h2>Mots-clics populaires de l'#AgCan</h2>\n\n<ul class=\"colcount-sm-2\">\n  <li>#GOC</li>\n  <li>#GC</li>\n  <li>#LeadersGC</li>\n  <li>#GCNumerique</li>\n  <li>#GouvOuvert</li>\n  <li>#GC2020</li>\n  <li>#NoshistoriesGC</li>\n  <li>#AgCan</li>\t\n  <li>#D\u00e9couvrezAg</li>\n  <li>#ScienceAg</li>\n  <li>#FemmesEnAgr</li>\n  <li>#femmesensciences</li>\n  <li>#FemmesEnSTIM</li>\n  <li>#MissionAgCan</li>\n  <li>#AECG</li>\n  <li>#PTPGP</li>\t\n  <li>#AgOuestCan</li>\n  <li>#AcQC</li>\n  <li>#Plant19</li>\n  <li>#Recolte19</li>\t\n</ul>\n\n<p>Utilisez le mot-clic <b>#AgromaneGC</b> lorsque vous r\u00e9digez des gazouillis sur votre travail!</p>\n\n <h2>Comptes Twitter d'AAC</h2>\n\n<ul>\n\t<li><a href=\"https://twitter.com/AAC_Canada\">AAC_Canada</a></li>\n\t<li><a href=\"https://twitter.com/AAFC_Canada\">AAFC_Canada (en anglais seulement)</a></li>\t\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Twitter use for AAFC employees",
        "fr": "Utilisation de twitter pour les employ\u00e9s d\u2019AAC"
    }
}