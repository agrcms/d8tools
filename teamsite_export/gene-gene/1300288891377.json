{
    "dcr_id": "1300288891377",
    "lang": "en",
    "title": {
        "en": "Web Publishing Services",
        "fr": "Service d'\u00e9dition Web"
    },
    "modified": "2020-09-18 00:00:00.0",
    "issued": "2011-04-05 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1313612109926",
    "layout_name": "1 column",
    "dc_date_created": "2011-03-16",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2011-04-05",
            "fr": "2011-04-05"
        },
        "modified": {
            "en": "2020-09-18",
            "fr": "2020-09-18"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Web Publishing Services",
            "fr": "Service d'\u00e9dition Web"
        },
        "subject": {
            "en": "communications;consultation",
            "fr": "communications;consultation"
        },
        "description": {
            "en": "E-Communications",
            "fr": "Communications \u00e9lectroniques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Web Communications is part of the Digital Communications unit.  It is comprised of Web Communications, Digital Communications Planning, and Digital Media.</p>\n\n<p>The Digital Communications unit accomplishes the following tasks:</p>\n\n<ul>\n<li>Web Publishing Services</li>\n<li><a href=\"?id=1324398229236\">Web Statistics and Analytics Services</a></li>\n<li><a href=\"?id=1523620215671\">Social Media</a></li>\n<li><a href=\"?id=1398685109107\">Video Production Services</a></li>\n</ul>\n\n<p>Web Communications provides oversight and added value to Agriculture and Agri-Food Canada's (AAFC) websites. Web Communications optimizes the Department's digital communications activities to ensure they effectively support AAFC's programs and priorities, are compliant to government policies and regulations, and are usable for targeted audiences.</p>\n\n<p class=\"text-center mrgn-tp-lg\"><a href=\"https://collab.agr.gc.ca/co/e-comm-e/Lists/Tracking/WebPublishingRequestForm.aspx?SPSLanguage=EN&amp;lcid=1033\" type=\"button\" class=\"btn btn-primary\">Web Publishing Request Form</a></p>\n\n<p>For web publishing requests related to COVID-19, use the <a href=\"https://forms.office.com/Pages/ResponsePage.aspx?id=sYupnVcYw0yHUZpJ410kzd1IaUNVQIVNv6E4bO5kr1BUMFBHWVNHR1c4QzA2UzM2WUJaQ0E2SUYwNy4u\">Web Publishing Request Form for COVID-19 Related Content</a>.</p>\n\n<h2>What services are provided by Web Communications</h2>\n\n<p>Web Communications provides a range of services to AAFC clients looking to create, improve or delete content on the Web, including:</p>\n\n<ul>\n <li>Optimizing placement and writing content for the Web</li>\n <li>Development of Web communications content plans/strategies to ensure:\n  <ul>\n    <li>Proper  structure and layout</li>\n    <li>Accessibility  and adherence to Government of Canada Web policies and standards and AAFC Web-related guidelines</li>\n    <li>Web content is aligned with other digital communications efforts such as social media, advertising, etc.</li>\n  </ul>\n  </li>\n <li>Publishing content to the Web</li>\n <li>Ensuring content visibility, including marketing and promotion</li>\n <li>Review or revise existing Web content</li>\n <li>Collection, measurement and analysis of data to better understand and improve Web usage</li>\n \n</ul>\n\n<h2>How do I access the services provided by Web Communications</h2>\n\n<p>Accessing the services provided by Web Communications depends on the amount and complexity of the work required. The services provided will be determined based on whether your request can be categorized as a simple or complex request.</p>\n\n<p>See our <a href=\"display-afficher.do?id=1300289092070&amp;lang=eng\">Web Publishing Steps</a> for more information on how to proceed with your request.</p>\n\n<p>See our <a href=\"display-afficher.do?id=1313410889839&amp;lang=eng\">Web Publishing Request Service Standards</a> for more information on how to determine between a simple and complex request.</p>\n\n<h2>Resources</h2>\n\n<ul>\n<li><a href=\"?id=1300289034048\">How to prepare a web request</a></li>\n<li><a href=\"display-afficher.do?id=1300289092070&amp;lang=eng\">Web Publishing Steps</a> outline the Web publishing process</li>\n<li>The Government of Canada web content style guides (version 2.1) establish the rules you must use to develop and edit content that will be published on a Government of Canada website: \n  <ul>\n    <li>Use the <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/government-communications/canada-content-style-guide.html\">style guide for English content</a> to develop and edit English content for AAFC Online, AgriSource and Canada.ca</li>\n    <li>Use the <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/communications-gouvernementales/guide-redaction-contenu-canada.html\">style guide for French content</a> to develop and edit French content for AAC en direct, AgriSource and Canada.ca</li>\n    </ul>\n    </li>\n    \n<li><a href=\"https://collab.agr.gc.ca/co/e-comm-e/Lists/Tracking/WebPublishingRequestForm.aspx?SPSLanguage=EN&amp;lcid=1033\" target=\"_blank\">Web Publishing Request Form</a> must be completed and submitted when your content is ready for publishing on AAFC Online or AgriSource</li>\n</ul>\n\n<h2>Contact us</h2>\n\n<ul>\n<li>Email: <a href=\"mailto:aafc-ecommunications.commselectroniques-aac@canada.ca\">aafc.webcommunications-communicationsweb.aac@canada.ca</a></li>\n<li>Telephone: 613-773-2666</li>\n<li>Staff Directory: <a href=\"http://directinfo.agr.gc.ca/directInfo/eng/index.php?fuseaction=agriInfo.orgUnit&amp;dn=OU=EC-CE,OU=CS-SC,OU=CCB-DGCC,OU=AAFC-AAC,O=GC,C=CA\" title=\"DirectInfo\">Digital Communications</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t   \n\n<p>Les Communications Web font partie de l\u2019unit\u00e9 des Communications num\u00e9riques.  L\u2019unit\u00e9 se compose des Communications Web, de la Planification des communications num\u00e9riques et des M\u00e9dias num\u00e9riques.</p>\n\n<p>Elle offre les services suivants\u00a0:</p>\n\n<ul>\n<li>Services d'\u00e9dition Web</li>\n<li><a href=\"?id=1324398229236\">Statistiques Web et les services d\u2019analyse</a></li>\n<li><a href=\"?id=1523620215671\">M\u00e9dias sociaux</a></li>\n<li><a href=\"?id=1398685109107\">Services de production vid\u00e9o</a></li>\n</ul>\n\n<p>L'\u00e9quipe des Communications Web veille sur la cr\u00e9ation et la mise \u00e0 jour de contenus sur les sites Web d'Agriculture et Agroalimentaire Canada (AAC). Elle optimise les activit\u00e9s de communication num\u00e9rique du Minist\u00e8re afin qu'elles soutiennent efficacement les programmes et les priorit\u00e9s d'AAC, qu'elles soient conformes aux politiques et aux r\u00e8glements gouvernementaux, et qu'elles soient conviviales pour les publics cibl\u00e9s.</p>\n \n<p class=\"text-center mrgn-tp-lg\"><a href=\"https://collab.agr.gc.ca/co/e-comm-e/Lists/Tracking/WebPublishingRequestForm_fra.aspx?SPSLanguage=FR&amp;lcid=1036\" type=\"button\" class=\"btn btn-primary\">Formulaire de demande de publication Web</a></p>\n\n<p>Pour les demandes d'\u00e9dition Web li\u00e9es \u00e0 la COVID-19, utilisez le <a href=\"https://forms.office.com/Pages/ResponsePage.aspx?id=sYupnVcYw0yHUZpJ410kzd1IaUNVQIVNv6E4bO5kr1BUMFBHWVNHR1c4QzA2UzM2WUJaQ0E2SUYwNy4u\">Formulaire de demande de publication Web pour le contenu li\u00e9 \u00e0 la COVID-19</a>.</p>\n\n<h2>Quels services sont offerts par les Communications Web</h2>\n\n<p>Les  Communications Web d'AAC offrent un \u00e9ventail de services \u00e0 leurs  clients qui veulent cr\u00e9er, am\u00e9liorer ou supprimer  du contenu Web, dont les  suivants\u00a0: </p>\n\n<ul>\n  <li>optimiser le placement et la r\u00e9daction de contenu Web;</li>\n  <li>\u00e9laborer des plans de contenu et des strat\u00e9gies pour  les communications Web afin d'assurer\u00a0:\n  <ul>\n    <li>une structure et une mise en page ad\u00e9quates;</li>\n    <li>l'accessibilit\u00e9 du contenu et le respect des normes et politiques du gouvernement du Canada li\u00e9es aux sites Web ainsi que des lignes directrices d'AAC li\u00e9es au Web;</li>\n    <li>un contenu Web harmonis\u00e9 avec d'autres v\u00e9hicules de communication num\u00e9rique tels que les m\u00e9dias sociaux, la publicit\u00e9, etc.</li>\n  </ul></li>\n  <li>publier du contenu sur le Web;</li>\n  <li>s'assurer que le contenu a une place bien en vue, ce qui comprend l'examen du travail de marketing et de promotion;</li> \n<li>r\u00e9viser du contenu Web existant;</li> \n<li>recueillir, mesurer et analyser des donn\u00e9es pour mieux comprendre et am\u00e9liorer l'utilisation du Web.</li>\n</ul>\n\n<h2>Comment pouvez-vous utiliser  les services offerts par les Communications Web</h2>\n\n<p>Les services offerts par les Communications Web d\u00e9pendent du  travail requis (quantit\u00e9 et complexit\u00e9). Il faut d\u00e9terminer si votre demande  est simple ou complexe pour savoir quels services seront fournis.</p>\n\n<p>Consultez les <a href=\"display-afficher.do?id=1300289092070&amp;lang=fra\">\u00c9tapes de la  publication sur le Web</a> pour de plus amples renseignements sur la fa\u00e7on de pr\u00e9senter votre demande.</p>\n\n<p>Consultez les <a href=\"display-afficher.do?id=1313410889839&amp;lang=fra\">Normes de  service pour les demandes de publication Web</a> pour de plus amples  renseignements sur la fa\u00e7on de d\u00e9terminer si une demande est simple ou complexe.</p>\n  \n<h2>Ressources</h2>\n\n<ul>\n<li><a href=\"?id=1300289034048\">Comment pr\u00e9parer une demande de publication sur le Web</a></li>\n<li><a href=\"display-afficher.do?id=1300289092070&amp;lang=fra\">\u00c9tapes de la publication Web</a> pr\u00e9sente le processus de publication sur le Web</li>\n<li>Les guides de r\u00e9daction Web du gouvernement du Canada (version 2.1) \u00e9tablissent les r\u00e8gles \u00e0 respecter lors de la r\u00e9daction et de la r\u00e9vision de contenu qui sera publi\u00e9 sur un site du gouvernement du Canada\u00a0:\n\n  <ul>\n    <li>Utilisez le <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/communications-gouvernementales/guide-redaction-contenu-canada.html\">guide de r\u00e9daction du contenu fran\u00e7ais</a> pour r\u00e9diger et r\u00e9viser le contenu fran\u00e7ais pour AAC en direct, AgriSource et Canada.ca</li>\n\n    <li>Utilisez le <a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/government-communications/canada-content-style-guide.html\">guide de r\u00e9daction du contenu anglais</a> pour r\u00e9diger et r\u00e9viser le contenu anglais pour AAFC Online, AgriSource et Canada.ca</li>\n\n  </ul></li>\n \n<li><a href=\"https://collab.agr.gc.ca/co/e-comm-e/Lists/Tracking/WebPublishingRequestForm_fra.aspx?SPSLanguage=FR&amp;lcid=1036\">Formulaire de demande de publication Web</a> doit \u00eatre rempli et soumis en ligne lorsque votre contenu est pr\u00eat \u00e0 \u00eatre publi\u00e9 sur AAC en direct ou sur AgriSource</li>\n</ul>\n\n<h2>Communiquez avec nous</h2>\n<ul>\n    <li>Courriel\u00a0: <a href=\"mailto:aafc.webcommunications-communicationsweb.aac@canada.ca\">aafc.webcommunications-communicationsweb.aac@canada.ca</a></li>\n    <li>T\u00e9l\u00e9phone\u00a0: 613-773-2666</li>\n    <li>R\u00e9pertoire des employ\u00e9s\u00a0: <a href=\"http://directinfo.agr.gc.ca/directInfo/fra/index.php?fuseaction=agriInfo.orgUnit&amp;dn=OU=EC-CE,OU=CS-SC,OU=CCB-DGCC,OU=AAFC-AAC,O=GC,C=CA\" title=\"DirectInfo\">Communications num\u00e9riques</a></li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Web Publishing Services",
        "fr": "Service d'\u00e9dition Web"
    }
}