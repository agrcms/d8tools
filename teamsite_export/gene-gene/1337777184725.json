{
    "dcr_id": "1337777184725",
    "lang": "en",
    "title": {
        "en": "Anonymous File Transfer Protocol (FTP) Service",
        "fr": "Service protocole de transfert de fichier (FTP) anonyme"
    },
    "modified": "2020-08-31 00:00:00.0",
    "issued": "2012-05-30 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1333380344003",
    "layout_name": "1 column",
    "dc_date_created": "2012-05-23",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2012-05-30",
            "fr": "2012-05-30"
        },
        "modified": {
            "en": "2020-08-31",
            "fr": "2020-08-31"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Anonymous File Transfer Protocol (FTP) Service",
            "fr": "Service protocole de transfert de fichier (FTP) anonyme"
        },
        "subject": {
            "en": "applications;communications",
            "fr": "applications;communications"
        },
        "description": {
            "en": "The Anonymous File Transfer Protocol (FTP) service is used to facilitate the transfer of large files that would be difficult or impossible to share through email.",
            "fr": "Le service FTP (protocole de transfert de fichier) sert \u00e0 faciliter le transfert de gros fichiers qui seraient autrement difficiles ou impossibles \u00e0 transmettre par courriel."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "service, FTP",
            "fr": "service, FTP"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<section>\n\t\t\t  \n\n<div class=\"row\">\n\n<div class=\"col-md-4 pull-right\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Instructions</h2>\n</header>\n<div class=\"panel-body\">\n<p><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3568259&amp;lang=eng\" target=\"_blank\">Quick Start Guide (Word)</a></p>\n</div>\n</section>\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Login</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n     <li>host name: ftp.agr.gc.ca</li>\n     <li>userid: anonymous</li>\n     <li>password: your_email_address</li>\n</ul>\n</div>\n</section>\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">System Requirements</h2>\n</header>\n<div class=\"panel-body\">\n<p>File Transfer Protocol (FTP) software.</p>\n<p><b>Getting an Account or FTP software</b></p>\n<ul>\n    <li>Email: <a href=\"mailto:Service@itcentreti.collaboration.gc.ca\">Service@itcentreti.<br>collaboration.gc.ca</a></li>\n    <li>Telephone: <br>1-855-545-4411 (Canada and International Toll-free)</li>\n</ul>\n</div>\n</section>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n<p>The Anonymous File Transfer Protocol (FTP) service is used to facilitate the transfer of large files that would be difficult or impossible to share through email. You require an account to use the FTP service. If you do not have an account, contact <a href=\"display-afficher.do?id=1313593700518&amp;lang=eng\">IT\u00a0Centre\u00a0TI</a>. When your FTP account is created, AgriHelp will create your personal sub-directory in both the incoming and outgoing directories. Do not share your personal userid and password with anyone; including Agriculture and Agri-Food Canada (AAFC) colleagues.</p>\n\n<p>Here are some facts about using the Anonymous FTP service:</p>\n\n<ul>\n     <li class=\"mrgn-bttm-md\">This service is not intended for the transfer of confidential or secret documents.</li>\n     <li class=\"mrgn-bttm-md\">This service should only be used for necessary work-related sharing of large files that cannot be easily transferred through email.</li>\n     <li class=\"mrgn-bttm-md\">This service is not to be advertised or communicated to the general public.</li>\n     <li class=\"mrgn-bttm-md\">You are responsible for educating your non-AAFC contacts about how to use the service properly and responsibly.</li>\n     <li>The service is monitored and subject to audit and any apparent misuse should be reported immediately to <a href=\"display-afficher.do?id=1313593700518&amp;lang=eng\">IT\u00a0Centre\u00a0TI</a>.</li>\n</ul>\n\n<p><b>On this page:</b></p>\n\n<nav>\n<ul>\n     <li><a href=\"#a\">Features</a></li>\n     <li><a href=\"#b\">Policies and guidelines</a></li>\n     <li><a href=\"#c\">Contact us</a></li>\n</ul>\n</nav>\n\n\n<h2 id=\"a\">Features</h2>\n\n<p>Here are some common uses of the Anonymous FTP service:</p>\n\n<ul>\n    <li>Canada Institute for Scientific and Technical Information (CISTI) library arrangement; CISTI delivers electronic documents to Agriculture and Agri-Food Canada (AAFC) librarians via FTP</li>\n    <li>transfer of electronic files from an external contractor to an AAFC employee</li>\n</ul>\n</div></div>\n\n<h2 id=\"b\">Policies and guidelines</h2>\n\n<ul>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/information_management/it_2011_security_policy-eng.doc\">Information Technology Security Policy (Word)</a></li>\n    <li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1630053&amp;lang=eng\" target=\"_blank\">Internet Usage Directive (Word)</a></li>\n    <li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1630069&amp;lang=eng\" target=\"_blank\">Network Usage Directive (Word)</a></li>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/information_management/electronic_messaging_directive-eng.doc\">Electronic Messaging Directive (Word)</a></li>\n</ul>\n\n\n<h2 id=\"c\">Contact us</h2>\n\n<ul>\n      <li>Telephone: 1-855-545-4411 (Canada and International Toll-free)</li>\n      <li>Email: <a href=\"mailto:Service@itcentreti.collaboration.gc.ca\">Service@itcentreti.collaboration.gc.ca</a></li>\n</ul>\n\n\t\t</section>",
        "fr": "<section>\n\t\t\t  \n\n<div class=\"row\">\n\n<div class=\"col-md-4 pull-right\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Instructions</h2>\n</header>\n<div class=\"panel-body\">\n<p><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3568275&amp;lang=fra\" target=\"_blank\">Guide de d\u00e9marrage rapide (Word)</a></p>\n</div>\n</section>\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Ouverture de session</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n     <li>Nom de l'h\u00f4te\u00a0: ftp.agr.gc.ca</li>\n     <li>ID d'utilisateur\u00a0: anonyme</li>\n     <li>Mot de passe\u00a0: votre_adresse<br>_\u00e9lectronique</li>\n</ul>\n</div>\n</section>\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Configuration requise</h2>\n</header>\n<div class=\"panel-body\">\n<p>Logiciel Protocole de transfert de fichier (FTP)</p>\n<p><b>Cr\u00e9ation d'un compte ou obtenir logiciel FTP</b></p>\n<ul>\n    <li>Courriel\u00a0: <a href=\"mailto:Service@itcentreti.collaboration.gc.ca\">Service@itcentreti.<br>collaboration.gc.ca</a></li>\n    <li>T\u00e9l\u00e9phone\u00a0: <br>1-855-545-4411 (Num\u00e9ro sans frais au Canada et \u00e0 l'\u00e9tranger)</li>\n</ul>\n</div>\n</section>\n</div>\n\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n\n<p>Le service protocole de transfert de fichier (FTP) sert \u00e0 faciliter le transfert de gros fichiers qui seraient autrement difficiles ou impossibles \u00e0 transmettre par courriel. Vous devez avoir un compte pour utiliser le service FTP. Si vous n'en avez pas, communiquez avec <a href=\"display-afficher.do?id=1313593700518&amp;lang=fra\">IT\u00a0Centre\u00a0TI</a>. Une fois votre compte FTP activ\u00e9, IT\u00a0Centre\u00a0TI cr\u00e9era votre sous-r\u00e9pertoire personnel dans les r\u00e9pertoires d'arriv\u00e9e et de sortie. Ne donnez votre ID d'utilisateur et votre mot de passe \u00e0 personne, y compris vos coll\u00e8gues d'Agriculture et Agroalimentaire Canada (AAC).</p>\n\n<p>Voici certaines pr\u00e9cisions concernant l'utilisation du Service FTP anonyme\u00a0:</p>\n\n<ul>\n    <li class=\"mrgn-bttm-md\">Ce service n'a pas pour objet le transfert de documents confidentiels ou secrets.</li>\n    <li class=\"mrgn-bttm-md\">Ce service ne doit \u00eatre utilis\u00e9 que pour l'\u00e9change de gros fichiers ayant trait au travail et difficiles \u00e0 transmettre par courriel.</li>\n    <li class=\"mrgn-bttm-md\">Ce service ne doit pas \u00eatre annonc\u00e9 ou communiqu\u00e9 au grand public.</li>\n    <li class=\"mrgn-bttm-md\">Il vous appartient d'indiquer la fa\u00e7on d'utiliser le service de mani\u00e8re responsable et juste \u00e0 vos personnes-ressources \u00e0 l'ext\u00e9rieur d'Agriculture et Agroalimentaire Canada (AAC).</li>\n    <li>Ce service est surveill\u00e9 et peut faire l'objet d'une v\u00e9rification. Tout abus apparent doit \u00eatre imm\u00e9diatement signal\u00e9 \u00e0 <a href=\"display-afficher.do?id=1313593700518&amp;lang=fra\">IT\u00a0Centre\u00a0TI</a>.</li>\n</ul>\n\n<p><b>Sur cette page\u00a0:</b></p>\n<nav>\n<ul>\n      <li><a href=\"#a\">Caract\u00e9ristiques</a></li>\n      <li><a href=\"#b\">Politiques et lignes directrices</a></li>\n      <li><a href=\"#c\">Communiquez avec nous</a></li>\n</ul>\n</nav>\n\n\n<h2 id=\"a\">Caract\u00e9ristiques</h2>\n\n<p>Voici des exemples d'utilisation courante du Service FTP anonyme\u00a0:</p>\n\n<ul>\n      <li>l'Institut canadien de l'information scientifique et technique (ICIST) arrangement de biblioth\u00e8que; l'ICIST livre des documents \u00e9lectroniques aux biblioth\u00e9caires d'AAC au moyen de FTP;</li>\n      <li>transfert de fichiers \u00e9lectroniques d'un entrepreneur de l'ext\u00e9rieur \u00e0 un employ\u00e9 d'AAC.</li>\n</ul>\n</div></div>\n\n<h2 id=\"b\">Politiques et lignes directrices</h2>\n\n<ul>\n      <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/information_management/it_2011_security_policy-fra.doc\">Politique de s\u00e9curit\u00e9 des technologies de l'information (Word)</a></li>\n      <li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1638845&amp;lang=fra\" target=\"_blank\">Directive sur l'utilisation d'Internet (Word)</a></li>\n      <li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1638806&amp;lang=fra\" target=\"_blank\">Directive sur l'utilisation du r\u00e9seau (Word)</a></li>\n      <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/information_management/electronic_messaging_directive-fra.doc\">Directive sur la messagerie \u00e9lectronique (Word)</a></li>\n</ul>\n\n\n\n<h2 id=\"c\">Communiquez avec nous</h2>\n\n<ul>\n      <li>Courriel\u00a0: <a href=\"mailto:Service@itcentreti.collaboration.gc.ca\">Service@itcentreti.collaboration.gc.ca</a></li>\n      <li>T\u00e9l\u00e9phone\u00a0: 1-855-545-4411 (Sans frais au Canada et \u00e0 l'\u00e9tranger)</li>\n</ul>\n\n\t\t</section>"
    },
    "breadcrumb": {
        "en": "Anonymous File Transfer Protocol (FTP) Service",
        "fr": "Service protocole de transfert de fichier (FTP) anonyme"
    }
}