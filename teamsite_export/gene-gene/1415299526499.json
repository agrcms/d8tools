{
    "dcr_id": "1415299526499",
    "lang": "en",
    "title": {
        "en": "Submit your expense report",
        "fr": "Soumettez votre rapport de d\u00e9penses"
    },
    "modified": "2019-04-11 00:00:00.0",
    "issued": "2014-12-04 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1484940874128",
    "layout_name": "1 column",
    "dc_date_created": "2014-11-06",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2014-12-04",
            "fr": "2014-12-04"
        },
        "modified": {
            "en": "2019-04-11",
            "fr": "2019-04-11"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Submit your expense report",
            "fr": "Soumettez votre rapport de d\u00e9penses"
        },
        "subject": {
            "en": "communications;travel",
            "fr": "communications;voyage"
        },
        "description": {
            "en": "The following page will help you in submitting your Expense Report (ER). It is the traveller's responsibility to submit a completed ER along with all necessary supporting documentation as soon as possible after the completion of the trip for reimbursement. If you received a travel advance, the ER must be finalized within 10 days from the completion of your trip.",
            "fr": "Cette page vous aidera \u00e0 produire votre rapport de d\u00e9penses. Il incombe au voyageur de soumettre un rapport de d\u00e9penses d\u00fbment rempli et assorti de toutes les pi\u00e8ces justificatives n\u00e9cessaires le plus rapidement possible apr\u00e8s son voyage afin d\u2019obtenir un remboursement. Si vous avez re\u00e7u une avance de voyage, vous devez mettre votre rapport de d\u00e9penses au point dans les 10 jours suivant la fin de votre voyage."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Shared Travel Services (STS) Portal, Expense Management Tool (EMT), Expense Report (ER)",
            "fr": "Portail des Services de voyage partag\u00e9s, Outil de gestion des d\u00e9penses (OGD), rapport de d\u00e9penses"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>The following page will help you in submitting your Expense Report (ER).</p>\n\n<p>It is the traveller's responsibility to submit a completed ER along with  all necessary supporting documentation as soon as possible after the completion  of the trip for reimbursement. If you received a travel advance, the ER must be  finalized within 10 days from the completion of your trip.</p>\n\n<h2>Using the Shared Travel Services Portal</h2>\n\n<p>Login to the <a href=\"https://isuite6.hrgworldwide.com/gcportal/en-ca/sts.aspx\">Shared  Travel Service Portal</a> and use the Expense Management Tool (EMT) to  create your ER. You will create your ER from your Travel Request (TR).</p>\n\n<p>If you used the Government of Canada Individual Designated Travel Card  (IDTC), your expense transactions are available on the Shared Travel Services (STS) Portal and can be  linked conveniently when you build your ER. Linked transactions are not  automatically paid to the Bank of Montreal (BMO)  when the ER is processed. It is the responsibility of the cardholder to make  the payment to BMO of the balance due by the statement due date.</p>\n\n<p>There are many user guides available in the STS Portal that will help you use the travel solution. In the STS Portal, click on \"Support\" and then \"Training\" to access the user guides.</p>\n\n<p><b>Please note:</b></p>\n\n<ul>\n  <li>If you have forgotten your login information, please contact <a href=\"aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca\">aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca</a> and a member of the team will assist you.</li>\n  <li>Many Shared Travel Service Portal user guides are available to help you use the STS Portal. The <b>\"Creating an Expense  Report \u2013 Travellers and Arrangers\"</b> guide will help you create and submit your ER.</li>\n  <li>Please ensure that you are requesting the  correct <a href=\"display-afficher.do?id=1415290508568&amp;lang=eng#wwnayt\">level of approval</a> when you are  submitting the completed ER for approval or recommendation. </li>\n  <li>Once the ER has been approved, it will be sent for  processing. You will receive an email once your ER has been processed.</li>\n  <li>Employees are required to retain original receipts. In the event a receipt is lost or stolen, complete the <a href=\"https://forms-formulaires.agr.gc.ca/pdf/A9387-E.pdf\">Lost or Destroyed Receipt Declaration \u2013 Government Travel (PDF)</a> (AAFC/AAC 9387).</li>\n  <li>For information on an  IDTC, see \"<a href=\"display-afficher.do?id=1415284129223&amp;lang=eng#idtc\">Government of Canada Individual Designated  Travel Card (IDTC)</a>.\"</li>\n</ul>\n\n<h2>How-to videos</h2>\n<p>These short videos produced by the Travel Helpdesk provide step-by-step instructions on how to complete certain tasks within this step of the travel process.</p>\n\n<ul>\n <li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Add%20an%20image%20to%20ER%20V2%20-%20Compressed.mp4\">Adding an image to an expense report (MP4)</a>\n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_1-eng.docx\">Adding an image to an expense report - Transcript (Word)</a></li></ul></li> \n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Changing%20an%20Amount%20in%20an%20ER%20(Compressed).mp4\">Changing an amount (MP4)</a>  \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_2-eng.docx\">Changing an amount - Transcript (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Change%20Currecy%20Expense%20Rates%20-%20compress.mp4\">Changing an exchange rate (MP4)</a> \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_3-eng.docx\">Changing an exchange rate - Transcript (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Changing%20HRG%20Coding%20-%20Compressed.mp4\">Changing transaction coding (for example #25 Transaction/Service Charge) (MP4)</a>\n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_4-eng.docx\">Changing transaction coding - Transcript (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Removing%20Estimated%20Mileage%20-%20compress.mp4\">Removing estimated mileage (MP4)</a> \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_5-eng.docx\">Removing estimated mileage - Transcript (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Searching%20for%20a%20Travel%20Document%20v2%20-%20compressed.mp4\">Searching for a travel document (MP4)</a>\n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_6-eng.docx\">Searching for a travel document - Transcript (Word)</a></li></ul></li>\n</ul>\n\n<h2>Contact us</h2>\n\n<p>Have questions? View our list of key contacts at <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100477471&amp;lang=eng\" target=\"_blank\">Agriculture and Agri-Food Canada for travel (Word)</a>.</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Cette page vous aidera \u00e0 produire votre rapport de d\u00e9penses.</p>\n\n<p>Il incombe au voyageur de soumettre un rapport de d\u00e9penses d\u00fbment rempli  et assorti de toutes les pi\u00e8ces justificatives n\u00e9cessaires le plus rapidement  possible apr\u00e8s son voyage afin d'obtenir un remboursement. Si vous avez re\u00e7u  une avance de voyage, vous devez mettre votre rapport de d\u00e9penses au point dans  les 10\u00a0jours suivant la fin de votre voyage.</p>\n\n<h2>Portail des Services de voyage partag\u00e9s</h2>\n\n<p>Ouvrez  une session dans le <a href=\"https://isuite6.hrgworldwide.com/gcportal/fr-ca/sts.aspx\">Portail des Services de voyage partag\u00e9s</a> et utilisez l'Outil de gestion des d\u00e9penses (OGD)  pour cr\u00e9er votre rapport de d\u00e9penses \u00e0 partir de votre demande de voyage.</p>\n\n<p>Si vous avez utilis\u00e9 une carte individuelle de  voyage (CIV) du gouvernement du Canada, vos transactions sont disponibles sur  le Portail des Services de voyages partag\u00e9s (SVP) et peuvent facilement \u00eatre associ\u00e9es \u00e0 votre rapport de  d\u00e9penses. Les transactions associ\u00e9es ne sont pas pay\u00e9es automatiquement \u00e0 la Banque  de Montr\u00e9al (BMO) lorsque le rapport de d\u00e9penses est  trait\u00e9. Il incombe au d\u00e9tenteur de la CIV d'effectuer le paiement \u00e0 la BMO du solde d\u00fb \u00e0 la date d'\u00e9ch\u00e9ance.</p>\n\n<p>Le Portail des SVP vous donne acc\u00e8s \u00e0 de nombreux guides de l'utilisateur qui vous aideront \u00e0 vous servir de la solution de voyage. Dans le Portail des SVP, cliquez sur \u00ab\u00a0Soutien\u00a0\u00bb et ensuite \u00ab\u00a0Formation\u00a0\u00bb pour acc\u00e9der aux guides de l'utilisateur.</p>\n\n<p><b>Remarques\u00a0:</b></p>\n<ul>\n  <li>Si vous avez oubli\u00e9 votre information pour  l'ouverture de session, communiquez le <a href=\"mailto:aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca\">aafc.travelhelpdesk-voyagebureauaide.aac@canada.ca</a>, et  un membre de l'\u00e9quipe vous aidera.</li>\n  <li>De nombreux guides de l'utilisateur pour le Portail des Services de voyage partag\u00e9s sont disponibles pour vous aider \u00e0 utiliser le Portail des SVP. Le  guide intitul\u00e9 <b>\u00ab\u00a0Cr\u00e9er un rapport  de d\u00e9penses \u2013 Voyageurs et organisateurs\u00a0\u00bb</b> vous aidera \u00e0 produire et  envoyer votre rapport.</li>\n  <li>Assurez-vous de transmettre votre rapport \u00e0 la  personne d\u00e9tenant le bon <a href=\"display-afficher.do?id=1415290508568&amp;lang=fra#qdavv\">niveau d'approbation</a> pour approbation ou recommandation.</li>\n  <li>Une fois votre rapport de d\u00e9penses approuv\u00e9, il  sera envoy\u00e9 au traitement. Vous recevrez un courriel une fois que votre rapport  aura \u00e9t\u00e9 trait\u00e9.</li>\n  <li>Vous devez conserver les originaux de vos re\u00e7us. Si un re\u00e7u est perdu ou vol\u00e9, remplissez la <a href=\"https://forms-formulaires.agr.gc.ca/pdf/A9387-F.pdf\">D\u00e9claration en cas de perte ou de destruction d\u2019un re\u00e7u \u2013 Voyage en service command\u00e9 (PDF)</a> (AAFC/AAC 9387).</li>\n  <li>Pour de plus amples renseignements sur la CIV, veuillez consulter la section \u00ab\u00a0<a href=\"display-afficher.do?id=1415284129223&amp;lang=fra#civ\">Carte  individuelle de voyage du gouvernement du Canada</a>\u00a0\u00bb.</li>\n</ul>\n\n<h2>Vid\u00e9os d\u00e9mo</h2>\n<p>Ces courtes vid\u00e9os produites par le bureau d\u2019aide de voyage fournissent des instructions \u00e9tape par \u00e9tape sur la fa\u00e7on d'effectuer certaines t\u00e2ches au cours de cette \u00e9tape du processus de voyage.</p>\n\n<ul>\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Remastered%20-%20Adding%20an%20image%20-%20compressed.mp4\">Ajouter une image \u00e0 un rapport de d\u00e9penses  (MP4)</a> \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_1-fra.docx\">Ajouter une image \u00e0 un rapport de d\u00e9penses - Transcription (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Remastered%20-%20Changing%20Coding%20to%2025)Transaction%20Service%20Charges-French%20%5Bcompressed%5D.mp4\">Changer le codage de la transaction (par exemple\u00a0: #25 Frais de transactions/services) (MP4)</a>  \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_2-fra.docx\">Changer le codage de la transaction - Transcription (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Changing%20an%20Amount%20-%20Remasted%20(Compressed).mp4\">Changer un montant dans un rapport de d\u00e9penses  (MP4)</a> \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_3-fra.docx\">Changer un montant dans un rapport de d\u00e9penses - Transcription (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Remastered%20Changing%20Exchange%20Rates%20%5BCompressed%5D.mp4\">Changer un taux d\u2019\u00e9change (MP4)</a>\n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_4-fra.docx\">Changer un taux d\u2019\u00e9change - Transcription (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/remastered%20-%20Rechercher%20un%20document%20de%20voyage%20compressed.mp4\">Rechercher un document de voyage  (MP4)</a> \n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_5-fra.docx\">Rechercher un document de voyage - Transcription (Word)</a></li></ul></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/th-vba/SiteAssets/Remasted%20-%20removing%20estimated%20mileage%20French%20(compressed).mp4\">Supprimer le kilom\u00e9trage estim\u00e9 (MP4)</a>\n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/%5Cdoc%5Ccmb_dggi%5Cservices%5C1415299526499_6-fra.docx\">Supprimer le kilom\u00e9trage estim\u00e9 - Transcription (Word)</a></li></ul></li>\n</ul>\n\n\n<h2>Contactez-nous</h2>\n\n<p>Vous avez des questions? Consultez la liste de personnes-ressources cl\u00e9s d'<a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100477477&amp;lang=fra\" target=\"_blank\">Agriculture et Agroalimentaire Canada pour les voyages (Word)</a> afin de savoir \u00e0 qui vous adresser.</p>\n\n\t\t"
    },
    "breadcrumb": {
        "en": "Submit your expense report",
        "fr": "Soumettez votre rapport de d\u00e9penses"
    }
}