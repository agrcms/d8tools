{
    "dcr_id": "1464788651920",
    "lang": "en",
    "title": {
        "en": "Real property",
        "fr": "Biens immobiliers"
    },
    "modified": "2019-06-12 00:00:00.0",
    "issued": "2016-06-02 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1478003007900",
    "layout_name": "1 column",
    "dc_date_created": "2016-06-01",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2016-06-02",
            "fr": "2016-06-02"
        },
        "modified": {
            "en": "2019-06-12",
            "fr": "2019-06-12"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Real property",
            "fr": "Biens immobiliers"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n \n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/images/employee_engagement/GGO/Green-Building_icone.jpg\" class=\"pull-left mrgn-rght-md\" alt=\"\"> Managing the Agriculture and Agri-Food Canada\u2019s (AAFC) real property environmental performance spans the entire lifecycle of a building from siting, design, construction, operation, maintenance, renovation, and demolition. The intent is to reduce waste, improve air and water quality, reduce operating costs, and enhance the comfort and health of buildings.</p>\n\n<h2>Greening government strategy</h2>\n\n<p>The Government is committed to reduce environmental impact from its real property portfolio through:</p> \n\n<h3>Water</h3>\n\n<p>The Government will reduce its water consumption and its load on municipal systems by tracking and disclosing its potable water consumption, using water efficiently in new construction and major renovations, and by designing all new Crown-owned buildings to effectively manage storm water.</p>\n\n<h3>Materials</h3>\n\t\n<p>The Government will reduce the environmental impact of building materials, using life-cycle assessment techniques to minimize embodied carbon and the use of harmful materials in construction and renovation.</p>\n\n<h3>Waste</h3>\n\n<p>The Government will take steps to reduce the environmental impact of waste by:</p>\n\t\n<ul>\n\t<li>diverting at least 75% of plastic waste from federal operations by 2030</li>\n\t<li>eliminating the unnecessary use of single-use plastics in government operations, events, and meetings</li>\n\t<li>diverting at least 75% by weight of all non-hazardous operational waste by 2030</li>\n\t<li>diverting at least 90% by weight of all construction and demolition waste and striving to achieve 100% by 2030</li>\n\t<li>minimizing environmentally harmful and hazardous chemicals and materials used and disposed of in real property operations</li>\n</ul>\t\n\n<h3>Real property operations</h3>\n\n<p>The Government will manage its real property portfolios using the principles of sustainable development to maximize their energy and resource efficiency, including the deployment of technologies and implementation of procedures to manage building operations, and take advantage of programs to improve building performance.</p>\n\n<h2>Agriculture and Agri-Food Canada actions</h2>\n\n<p>AAFC continues to update and adopt policies and practices to improve each of the above items in their real property portfolio and operations.</p>\n\n<p>AAFC continues to focus on these key elements:</p>\n\n<ul>\n\t<li>pilot composting programs, in the hopes of rolling out composting systems across all sites</li>\n\t<li>upgrade with water-conserving devices during failure, renovations or new construction on an opportunistic basis and meter water usage in new high water usage projects within building envelopes</li>\n\t<li>promote <a href=\"http://onottaagridoc06.agr.gc.ca/CyberDOCS/autopapiact.asp?AppINT=-1&amp;mode=no&amp;autopapiurl=%2FCyberDOCS%2FLibraries%2FDefault%5FLibrary%2FCommon%2Fviewdocact%2Easp%3Flib%3DAAFCAAC%26doc%3D3120239%26noframes%3Dyes&amp;SCICO=false\">green meetings (Word)</a> and <a href=\"http://www.gcpedia.gc.ca/gcwiki/images/d/d7/Guidance_plastic_waste_in_meetings_FINAL-ENG.pdf\">plastic reduction (PDF)</a></li> \n\t<li>reduce the environmental impact of building materials using life-cycle assessment techniques to minimize embodied carbon and the use of harmful materials in construction and renovation</li>\n\t<li>dispose of waste in an eco-friendly manner through the electronic and electrical equipment collection and appropriate-disposal program and waste reduction and recycling at Public Services and Procurement Canada-tenant facilities</li> \n\t<li>recycle batteries up to 5 kg at work through the <a href=\"https://www.call2recycle.ca/\" rel=\"external\">Call2Recycle</a> program</li>\n</ul>\n\n<h2>Resources</h2>\n\n<ul>\n\t<li><a href=\"http://www.gcpedia.gc.ca/wiki/Real_Property\">Real Property</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n \n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/images/employee_engagement/GGO/Green-Building_icone.jpg\" class=\"pull-left mrgn-rght-md\" alt=\"\">La gestion de la performance environnementale des biens immobiliers d'Agriculture et Agroalimentaire Canada (AAC) s'\u00e9chelonne sur l'ensemble du cycle de vie d'un b\u00e2timent, du choix de l'emplacement, de la conception et de la construction, \u00e0 l'exploitation, \u00e0 l'entretien, \u00e0 la r\u00e9novation et \u00e0 la d\u00e9molition. Le but \u00e9tant de r\u00e9duire les d\u00e9chets, d'am\u00e9liorer la qualit\u00e9 de l'eau et de l'air, de r\u00e9duire les co\u00fbts d'exploitation et d'am\u00e9liorer le confort et la sant\u00e9 des occupants des b\u00e2timents.</p>\n\n<h2>Strat\u00e9gie d'\u00e9cologisation du gouvernement</h2>\n\n<p>Le gouvernement s'est engag\u00e9 \u00e0 r\u00e9duire l'incidence environnementale de son portefeuille de biens immobiliers, notamment en ce qui concerne les \u00e9l\u00e9ments suivants\u00a0:</p>\n\n<h3>Eau</h3>\n\n<p>Le gouvernement r\u00e9duira sa consommation d'eau et la charge sur les r\u00e9seaux municipaux en assurant un suivi de sa consommation en eau potable, et en divulguant les donn\u00e9es \u00e0 cet \u00e9gard, en utilisant l'eau de fa\u00e7on efficiente durant la construction de nouveaux immeubles ou la r\u00e9alisation d'importantes r\u00e9novations, et en concevant de nouveaux immeubles appartenant \u00e0 l'\u00c9tat qui permettent une meilleure gestion des eaux pluviales.</p>\n\n<h3>Mat\u00e9riaux</h3>\n\n<p>Le gouvernement r\u00e9duira l'incidence environnementale des mat\u00e9riaux de construction en utilisant des techniques d'\u00e9valuation du cycle de vie pour r\u00e9duire au minimum les \u00e9missions de carbone ainsi que l'utilisation de mat\u00e9riaux dangereux durant les projets de construction et de r\u00e9novation.</p>\n\n<h3>D\u00e9chets</h3>\n\n<p>Le gouvernement prendra des mesures pour r\u00e9duire l'incidence environnementale des d\u00e9chets en\u00a0:</p>\n\n<ul>\n\t<li>r\u00e9acheminant au moins 75\u00a0% des d\u00e9chets de plastique des activit\u00e9s f\u00e9d\u00e9rales d\u2019ici 2030;</li>\n\t<li>\u00e9liminant l\u2019utilisation inutile de plastiques \u00e0 usage unique dans les activit\u00e9s, les \u00e9v\u00e9nements et les r\u00e9unions du gouvernement;</li>\n\t<li>r\u00e9acheminant au moins 75\u00a0%, en poids, de l'ensemble des d\u00e9chets d'exploitation non dangereux d'ici 2030;</li>\n\t<li>r\u00e9acheminant au moins 90\u00a0%, en poids, de l'ensemble des d\u00e9chets de construction et de d\u00e9molition, l'objectif \u00e9tant d'atteindre un taux de 100 % d'ici 2030;</li>\n\t<li>en r\u00e9duisant au minimum la quantit\u00e9 de produits chimiques et de mat\u00e9riaux nocifs et dangereux utilis\u00e9e lors d'activit\u00e9s immobili\u00e8res et qui doit \u00eatre \u00e9limin\u00e9e apr\u00e8s celles ci.</li>\n</ul>\n\n<h3>Activit\u00e9s immobili\u00e8res</h3>\n\n<p>Le gouvernement g\u00e9rera ses portefeuilles de biens immobiliers en suivant les principes du d\u00e9veloppement durable pour optimiser l'efficacit\u00e9 \u00e9nerg\u00e9tique et des ressources, notamment en d\u00e9ployant des technologies et en mettant en \u0153uvre des proc\u00e9dures de gestion du fonctionnement des b\u00e2timents ainsi qu'en tirant profit des programmes pour am\u00e9liorer le rendement des b\u00e2timents.</p>\n\n<h2>Mesures prises par AAC</h2>\n\n<p>AAC continue de mettre \u00e0 jour et d'adopter des politiques et des pratiques pour am\u00e9liorer chacun des \u00e9l\u00e9ments susmentionn\u00e9s dans le cadre de son portefeuille de biens immobiliers et de ses activit\u00e9s.</p>\n\n<p>AAC continue de mettre l'accent sur les \u00e9l\u00e9ments cl\u00e9s suivants\u00a0:</p>\n\n<ul>\n\t<li>mettre en place des programmes pilotes de compostage, dans l\u2019espoir de mettre en \u0153uvre des syst\u00e8mes de compostage dans tous les sites.</li>\n\t<li>effectuer une mise \u00e0 niveau en utilisant, lorsque l'occasion se pr\u00e9sente, des dispositifs de conservation de l'eau dans le cadre de pannes, de r\u00e9novations ou de nouvelles constructions, et \u00e9valuer l'utilisation de l'eau durant les projets qui en n\u00e9cessitent une grande quantit\u00e9 \u00e0 l'int\u00e9rieur de l'enveloppe du b\u00e2timent.</li>\n\t<li>favoriser des <a href=\"http://agonk1awvagrp04.agr.gc.ca/CyberDOCS/autopapiact.asp?AppINT=-1&amp;mode=no&amp;autopapiurl=%2FCyberDOCS%2FLibraries%2FDefault%5FLibrary%2FCommon%2Fviewdocact%2Easp%3Flib%3DAAFCAAC%26doc%3D3191351%26noframes%3Dyes&amp;SCICO=false\">r\u00e9unions \u00e9cologiques (Word)</a> et la <a href=\"http://www.gcpedia.gc.ca/gcwiki/images/f/f1/Guidance_plastic_waste_in_meetings_FINAL-7131118_1-FR.pdf\">r\u00e9duction du plastique (PDF)</a>.</li> \n\t<li>r\u00e9duire l'incidence environnementale des mat\u00e9riaux de construction en utilisant des techniques d'\u00e9valuation du cycle de vie pour r\u00e9duire au minimum les \u00e9missions de carbone ainsi que l'utilisation de mat\u00e9riaux dangereux durant les projets de construction et de renovation.</li>\n\t<li>\u00e9liminer les d\u00e9chets d'une fa\u00e7on qui respecte l'environnement au moyen de la collecte de l'\u00e9quipement \u00e9lectrique et \u00e9lectronique, et de l'application d'un programme d'\u00e9limination appropri\u00e9 \u00e0 cet \u00e9gard, de la r\u00e9duction des d\u00e9chets et du recyclage dans les installations o\u00f9 SPAC en est le locataire.</li>\n\t<li>recycler les piles, dont le poids n'exc\u00e8de pas 5\u00a0kg chacune, au travail dans le cadre du programme <a href=\"https://www.appelarecycler.ca/\">Appel \u00e0 Recycler</a>.</li>\n</ul>\n\n<h2>Ressources</h2>\n\n<ul>\n\t<li><a href=\"http://www.gcpedia.gc.ca/wiki/Biens_immobiliers\">Biens immobiliers</a></li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Real Property Environmental Performance",
        "fr": "Performance environnementale des biens immobiliers"
    }
}