{
    "dcr_id": "1402587733277",
    "lang": "en",
    "title": {
        "en": "Consider Persons with a Priority Entitlement",
        "fr": "Consid\u00e9rer les b\u00e9n\u00e9ficiaires de priorit\u00e9"
    },
    "modified": "2017-05-04 00:00:00.0",
    "issued": "2014-07-03 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279035869288",
    "layout_name": "1 column",
    "dc_date_created": "2014-06-12",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2014-07-03",
            "fr": "2014-07-03"
        },
        "modified": {
            "en": "2017-05-04",
            "fr": "2017-05-04"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Consider Persons with a Priority Entitlement",
            "fr": "Consid\u00e9rer les b\u00e9n\u00e9ficiaires de priorit\u00e9"
        },
        "subject": {
            "en": "staffing",
            "fr": "dotation en personnel"
        },
        "description": {
            "en": "A priority person is a public servant whose job has been affected by a change in their life or employment. These changes include: becoming disabled, being affected by a Workforce Adjustment (WFA) situation, returning from extended leave, re-locating with a spouse or common law partner, or being medically released from the Canadian Forces or Royal Canadian Mounted Police (RCMP).",
            "fr": "Un b\u00e9n\u00e9ficiaire de priorit\u00e9 est un fonctionnaire dont le travail a \u00e9t\u00e9 touch\u00e9 par un changement dans sa vie ou son emploi. Il peut s\u2019agir d\u2019un des changements suivants : une incapacit\u00e9, un r\u00e9am\u00e9nagement des effectifs, un retour apr\u00e8s un cong\u00e9 prolong\u00e9, un d\u00e9m\u00e9nagement avec un conjoint ou un conjoint de fait, ou une lib\u00e9ration des Forces canadiennes ou de la Gendarmerie royale du Canada pour raisons m\u00e9dicales."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "PSC Priority Administration, Public Service Employment Act (PSEA), Public Service Employment Regulations (PSER)",
            "fr": "Administration des priorit\u00e9s par la CFP, Politique sur la s\u00e9lection et la nomination, Guide sur l'administration des priorit\u00e9s"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>A person with a priority  entitlement is a public servant whose job has been affected by a change in  their life or employment. These changes include: becoming disabled, being  affected by a Workforce Adjustment (WFA) situation, returning from extended  leave, re-locating with a spouse or common law partner, or being medically  released from the Canadian Forces or Royal Canadian Mounted Police (RCMP).</p>\n\n<p>The Public Service Commission  (PSC) administers a system which provides an entitlement, for limited periods,  for these individuals to be appointed in priority of others in the federal  public service.</p>\n\n<p>As a manager staffing a vacant  position, you are obligated to consider persons with a priority entitlement  referred to you by the PSC.</p>\n\n<p>For more information see <a href=\"https://www.canada.ca/en/public-service-commission/services/information-priority-administration.html\">Public Service Commission  Priority Administration</a>.</p>\n\n<p>The PSC may refer persons with a  priority entitlement (or such persons may refer themselves directly) at any  time during the appointment process. Such persons are required to meet all  essential qualifications and conditions of employment; they are not required to  meet any other merit criteria established for the position. If they do meet the  essential qualifications and any conditions of employment, they must be  appointed, even if you are considering other persons. If more than one person  with a priority entitlement is referred to you, appoint the person considered a  priority under the <cite>Public Service Employment Act</cite> (PSEA) before the person who  is a priority under the <cite>Public Service Employment Regulations</cite> (PSER).</p>\n\n<p>See the <a href=\"https://www.canada.ca/en/public-service-commission/services/information-priority-administration/priority-administration-directive.html\">Public Service Commission Priority Administration  Directive</a> for additional information about the requirements for  considering persons with a priority entitlement.</p>\n\n<h2>Contact us</h2>\n\n<p>For more information, contact your <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1548432&amp;lang=eng\" target=\"_blank\">Human Resources Advisor (Word)</a>.</p>\n\n<p>For advice on staffing for the executive group, contact <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=102720770&amp;lang=eng\" target=\"_blank\">Executive Services (Word)</a>.</p>\n\n <h2>Additional  Resources</h2>\n  \n<p><a href=\"https://www.canada.ca/en/public-service-commission/services/information-priority-administration/public-service-commission-guide-priority-administration.html\">Public Service Commission Guide on Priority Administration, Section 1.2 Priority entitlement types</a></p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Un b\u00e9n\u00e9ficiaire de priorit\u00e9 est un fonctionnaire  dont le travail a \u00e9t\u00e9 touch\u00e9 par un changement dans sa vie ou son emploi. Il  peut s'agir d'un des changements suivants\u00a0: une incapacit\u00e9, un  r\u00e9am\u00e9nagement des effectifs, un retour apr\u00e8s un cong\u00e9 prolong\u00e9, un d\u00e9m\u00e9nagement  avec un conjoint ou un conjoint de fait, ou une lib\u00e9ration des Forces  canadiennes ou de la Gendarmerie royale du Canada (GRC) pour raisons m\u00e9dicales.</p>\n\n<p>La Commission de la fonction publique (CFP)  administre un syst\u00e8me qui accorde \u00e0 ces personnes le droit d'\u00eatre nomm\u00e9es en  priorit\u00e9 \u00e0 des postes de la fonction publique f\u00e9d\u00e9rale, droit qui est valide  pour des p\u00e9riodes limit\u00e9es.</p>\n\n<p>En tant que gestionnaire dotant un poste vacant,  vous \u00eates tenu de consid\u00e9rer les b\u00e9n\u00e9ficiaires de priorit\u00e9 qui vous sont propos\u00e9s  par la CFP.</p>\n\n<p>Pour des renseignements suppl\u00e9mentaires, voir <a href=\"https://www.canada.ca/fr/commission-fonction-publique/services/administration-priorites.html\" rel=\"external\">Administration des  priorit\u00e9s par la Commission de la fonction publique</a>.</p>\n\n\n<p>La CFP peut proposer des b\u00e9n\u00e9ficiaires de  priorit\u00e9 (ou ces personnes peuvent se proposer elles-m\u00eames directement) en tout  temps au cours du processus de nomination. Ces personnes doivent poss\u00e9der  toutes les qualifications essentielles et r\u00e9pondre \u00e0 toutes les conditions  d'emploi. Elles ne sont pas tenues de satisfaire aux autres crit\u00e8res de m\u00e9rite  \u00e9tablis pour le poste. Si elles poss\u00e8dent les qualifications essentielles et  r\u00e9pondent aux conditions d'emploi, elles doivent \u00eatre nomm\u00e9es, m\u00eame si vous  consid\u00e9riez d'autres personnes. Si plus d'un b\u00e9n\u00e9ficiaire de priorit\u00e9 vous est  propos\u00e9, nommez le b\u00e9n\u00e9ficiaire dont la priorit\u00e9 est accord\u00e9e en vertu de la <cite>Loi sur l'emploi dans la fonction publique</cite> (LEFP) avant celui dont la priorit\u00e9 est accord\u00e9e en vertu du <cite>R\u00e8glement sur l'emploi dans la fonction publique</cite> (REFP).</p>\n\n<p>Voir la <a href=\"https://www.canada.ca/fr/commission-fonction-publique/services/administration-priorites/directive-administration-priorites.html\" rel=\"external\">\u00a0Directive sur l'administration des priorit\u00e9s  de la Commission de la fonction publique</a> pour des renseignements  suppl\u00e9mentaires sur l'obligation de consid\u00e9rer les b\u00e9n\u00e9ficiaires de priorit\u00e9.</p>\n\n<h2>Contactez-nous</h2>\n\n<p>Pour  des renseignements suppl\u00e9mentaires, communiquez avec votre <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=1548432&amp;lang=fra\" target=\"_blank\">conseiller en ressources humaines (Word)</a>.</p>\n\n<p>Pour des conseils en mati\u00e8re de dotation pour les cadres sup\u00e9rieurs, communiquez avec les <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=102720770&amp;lang=fra\" target=\"_blank\">Services aux cadres sup\u00e9rieurs (Word)</a>.</p>\n\n\n<h2>Autres ressources</h2>\n  \n<p><a href=\"https://www.canada.ca/fr/commission-fonction-publique/services/administration-priorites/guide-administration-priorites-commission-fonction-publique.html\" rel=\"external\">Guide sur l'administration des priorit\u00e9s de la Commission de la fonction publique, Section 1.2 Types de priorit\u00e9</a></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Consider Persons with a Priority Entitlement",
        "fr": "Consid\u00e9rer les b\u00e9n\u00e9ficiaires de priorit\u00e9"
    }
}