{
    "dcr_id": "1332354068085",
    "lang": "en",
    "title": {
        "en": "Information for employees in career transition",
        "fr": "Information pour les employ\u00e9s en transition de carri\u00e8re"
    },
    "modified": "2020-10-08 00:00:00.0",
    "issued": "2012-03-23 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1332350899688",
    "layout_name": "1 column",
    "dc_date_created": "2012-03-21",
    "breadcrumb_label": "Information for Employees in career transition",
    "meta": {
        "issued": {
            "en": "2012-03-23",
            "fr": "2012-03-23"
        },
        "modified": {
            "en": "2020-10-08",
            "fr": "2020-10-08"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Information for employees in career transition",
            "fr": "Information pour les employ\u00e9s en transition de carri\u00e8re"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "On this page you will find information and resources on what you need to know and do in a situation of work force adjustment and where to get more assistance.",
            "fr": "Dans la pr\u00e9sente page, vous trouverez de l'information et des ressources sur ce que vous devez savoir et faire dans une situation de r\u00e9am\u00e9nagement des effectifs, et o\u00f9 vous pourrez obtenir une aide suppl\u00e9mentaire."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "work force adjustment, budget",
            "fr": "r\u00e9am\u00e9nagement des effectifs, budget"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "resource list",
            "fr": "liste de r\u00e9f\u00e9rence"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<div class=\"row\">\n<div class=\"col-md-4 pull-right\">\n<div class=\"well\">\n<p>Information on <a href=\"?id=1339780840393\">alternation</a> and <a href=\"?id=1344536919697\">surplus status</a> is available to assist employees and managers during transition periods.</p>\n</div>\n</div>\n\n<div class=\"col-md-8\">\n\n<p>On this page you will find information and resources on what you need to know and do in a situation of work force adjustment and where to get more assistance.</p>\n\n<p>On this page:</p>\n\n<ul>\n  <li><a href=\"#Training\">Information on Training</a></li>\n  <li><a href=\"#Resources\">Resources</a></li>\n  <li><a href=\"#Support\">Support Services</a></li>\n  <li><a href=\"#FAQ\">Frequently Asked Questions</a></li>\n</ul>\n\n</div>\n</div>\n\n<p>If there is information you need that we have not posted, please let us know at <a href=\"mailto:aafc.workforce-effectif.aac@canada.ca\">aafc.workforce-effectif.aac@canada.ca</a>.</p>\n\n<p>Executives should consult the <a href=\"http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=14218\">Directive on Career Transition for Executives</a>. For more information, email <a href=\"mailto:aafc.egs-sgd.aac@canada.ca\">aafc.egs-sgd.aac@canada.ca</a>.</p>\n\n<div class=\"well\">\n  <p>Opting employees are reminded of the  importance of making a decision among the options provided in the Workforce  Adjustment (WFA) provisions before the 120 day opting period comes to an end.  Opting employees who fail to make a selection will automatically be deemed to  have chosen the 12 month surplus priority option (Option A among the WFA  provisions).</p>\n</div>\n\n<h2 id=\"Training\">Information on Training </h2>\n\n<ul>\n<li><a href=\"display-afficher.do?id=1306503292350&amp;lang=eng\">Learning and Development</a></li>\n<li><a href=\"display-afficher.do?id=1330531231770&amp;lang=eng\">Competencies</a></li>\n</ul>  \n\n<h2 id=\"Resources\">Resources</h2>\n\n  <ul>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hrb_csd_si_reasonable_job_offer-eng.docx\">Reasonable Job Offer (Word)</a></li>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hrb_csd_so_assigning_meaninful_work-eng.docx\">Assigning Meaningful Work (Word)</a></li>    \n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/hrb_dgrh/hrb_csd_so_flowchart_workforce_adjustment-eng.pdf\">Work force adjustment flowchart (PDF)</a></li>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_definitions_work_force_adjustment-eng.doc\">Glossary of terms (Word)</a></li>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_csd_so_wfa_key_contacts-eng.docx\">Key contacts (Word)</a> can help you work through your strategies, address your questions, or refer you to other qualified services.</li>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/cmb_wr_ve_post_employment_information_for_employees_in_transition-eng.docx\">Preventing post-employment conflict of interest situations (Word)</a></li>\n  </ul>\n\n<h3>Education Allowance</h3>\n\n<p>Employees who have been affected by a work force adjustment situation and have chosen option C (Education Allowance) should take into account certain criteria for reimbursement of their studies. A <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/wfa_option_c_education_allowance_reimbursement_of_expenses_factsheet-eng.docx\">fact sheet on reimbursement of education allowance expenses (Option C) (Word)</a> was created to outline the criteria to be considered, such as the recognized learning institution, the process for claiming educational reimbursement, etc.</p>\n\n<h2 id=\"Support\">Support Services</h2>\n\n<p>A variety of support services are available through the <a href=\"?id=1287673689033\">Employee and Family Assistance Program</a>.</p>\n\n<h3>Job Search</h3>\n\n<p>Agriculture and Agri-Food Canada (AAFC) <a href=\"?id=1339780840393\">Alternation</a> facilitates job exchanges between opting employees and non-affected employees.</p>\n\n<p>All affected or surplus employees will be automatically registered on the Departmental Priority Referral List. These employees will be given first consideration when job vacancies or opportunities arise in the department.</p>\n\n<p>As part of AAFC's ongoing commitment to <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_csd_si_wfa_job_placement_support-eng.docx\">support our employees (Word)</a>, CMB has developed an outreach strategy to maximize placement opportunities for our affected employees. This outreach strategy is designed to provide additional resources for employees seeking new career opportunities.</p>\n\n<p>Register with <a href=\"https://www.canada.ca/en/public-service-commission/jobs/services/gc-jobs.html\">Careers in the Federal Public Service</a> for Public Service job alerts and postings.</p>\n\n<h2 id=\"FAQ\">Frequently Asked Questions</h2>\n\n<ul>\n  <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/faq_workforce_adjustment-eng.docx\">Work force adjustment at Agriculture and Agri-Food Canada: Questions and answers (Word)</a></li>\n  <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_csd_si_wfa_qa_job_placement_support-eng.docx\">Questions and answers on Agriculture and Agri-Food Canada's job placement support (Word)</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<div class=\"row\">\n\n<div class=\"col-md-4 pull-right\">\n<div class=\"well\">\n<p>De l'information sur l'<a href=\"?id=1339780840393&amp;lang=fra\">\u00e9change de postes</a> et sur le <a href=\"?id=1344536919697\">statut d'employ\u00e9 exc\u00e9dentaire</a> est disponible afin d'aider les employ\u00e9s et les gestionnaires pendant les p\u00e9riodes de transition.</p>\n</div>\n</div>\n\n\n<div class=\"col-md-8\">\n\n<p>La pr\u00e9sente page contient des renseignements et  des ressources sur les choses \u00e0 savoir et \u00e0 faire en cas de situation de r\u00e9am\u00e9nagement des effectifs et  sur comment obtenir de l'aide.</p>\n\n<p>Sur cette page\u00a0:</p>\n\n<ul>\n  <li><a href=\"#Formation\">Information sur la formation</a></li>\n  <li><a href=\"#Resources\">Ressources</a> </li>\n  <li><a href=\"#Services\">Services de soutien</a> </li>\n  <li><a href=\"#Foire\">Foire aux questions</a> </li>\n</ul>\n\n</div>\n</div>\n\n<p>Si vous ne trouvez pas les renseignements que vous cherchez, \u00e9crivez-nous un courriel \u00e0 l'adresse suivante\u00a0: <a href=\"mailto:aafc.workforce-effectif.aac@canada.ca\">aafc.workforce-effectif.aac@canada.ca</a>.</p>\n\n<p>Les cadres sup\u00e9rieurs devraient consulter la <a href=\"http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?section=text&amp;id=14218\">Directive sur la transition dans la carri\u00e8re des cadres sup\u00e9rieurs</a>. Pour obtenir de plus amples renseignements, \u00e9crivez \u00e0 l'adresse suivante\u00a0: <a href=\"mailto:aafc.egs-sgd.aac@canada.ca\">aafc.egs-sgd.aac@canada.ca</a>.</p>\n\n<div class=\"well\">\n  <p>Ceci est un rappel \u00e0 tous les employ\u00e9s optants l'importance de faire un choix parmi les options figurant  dans les dispositions sur le r\u00e9am\u00e9nagement des effectifs avant l'expiration de la p\u00e9riode de 120 jours donnant droit aux options. Les employ\u00e9s optants qui ne feront pas de choix seront consid\u00e9r\u00e9s automatiquement comme ayant s\u00e9lectionn\u00e9 l'option de la priorit\u00e9 d'employ\u00e9 exc\u00e9dentaire pour une p\u00e9riode de 12 mois (option A dans les dispositions sur le r\u00e9am\u00e9nagement des effectifs).</p>\n</div>\n\n<h2 id=\"Formation\">Information sur la formation</h2>\n\n<ul>\n<li><a href=\"display-afficher.do?id=1306503292350&amp;lang=fra\">Apprentissage et perfectionnement </a></li>\n<li><a href=\"display-afficher.do?id=1330531231770&amp;lang=fra\">Comp\u00e9tences</a></li>\n</ul>  \n\n<h2 id=\"Resources\">Ressources</h2>\n\n  <ul>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hrb_csd_si_reasonable_job_offer-fra.docx\">Offre d'emploi raisonnable (Word)</a></li>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hrb_csd_so_assigning_meaninful_work-fra.docx\">Assignation de t\u00e2ches valorisantes (Word)</a></li>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/hrb_dgrh/hrb_csd_so_flowchart_workforce_adjustment-fra.pdf\">Organigramme du r\u00e9am\u00e9nagement des effectifs (PDF)</a></li>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_definitions_work_force_adjustment-fra.doc\">Glossaire (Word)</a></li>\n    <li>Les <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_csd_so_wfa_key_contacts-fra.docx\">principales personnes-ressources (Word)</a> peuvent vous aider \u00e0 \u00e9laborer des strat\u00e9gies, r\u00e9pondre \u00e0 vos questions ou vous diriger vers d'autres services qualifi\u00e9s.</li>\n    <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/cmb_wr_ve_post_employment_information_for_employees_in_transition-fra.docx\">Pr\u00e9vention des situations de conflit d'int\u00e9r\u00eats relatives \u00e0 l'apr\u00e8s-mandat (Word)</a></li>\n  </ul>\n\n<h3>Indemnit\u00e9 d'\u00e9tudes</h3>\n\n<p>Les employ\u00e9s qui ont \u00e9t\u00e9 touch\u00e9s par un r\u00e9am\u00e9nagement des effectifs et qui ont choisi l'option C, soit l'indemnit\u00e9 d'\u00e9tudes, doivent prendre en consid\u00e9ration certains crit\u00e8res pour le remboursement de leurs \u00e9tudes. Le <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/wfa_option_c_education_allowance_reimbursement_of_expenses_factsheet-fra.docx\">feuillet de documentation sur le remboursement de l'indemnit\u00e9 d'\u00e9tudes (Option C) (Word)</a> a \u00e9t\u00e9 produit pour souligner les crit\u00e8res \u00e0 consid\u00e9rer, tels que l'\u00e9tablissement d'enseignement reconnu, la marche \u00e0 suivre pour demander l'indemnit\u00e9 d'\u00e9tudes, etc.</p>\n\n<h2 id=\"Services\">Services de soutien</h2>\n\n<p>Divers services de soutien sont offerts par le <a href=\"?id=1287673689033\">Programme d'aide aux employ\u00e9s et \u00e0 la famille</a>.</p>\n\n<h3>Recherche d'emplois</h3>\n\n<p>L'<a href=\"?id=1339780840393&amp;lang=fra\">\u00e9change de postes</a> \u00e0 Agriculture et Agroalimentaire Canada (AAC)  facilite ce processus entre les employ\u00e9s optants et les employ\u00e9s non touch\u00e9s.</p>\n\n<p>Tous les employ\u00e9s touch\u00e9s ou exc\u00e9dentaires seront inscrits automatiquement dans le r\u00e9pertoire d'employ\u00e9s prioritaires. Ces employ\u00e9s auront la priorit\u00e9 lorsque des postes devront \u00eatre dot\u00e9s ou que des occasions se pr\u00e9senteront au sein du Minist\u00e8re.</p>\n\n<p>Dans le cadre de l'engagement continu d'AAC visant \u00e0 <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_csd_si_wfa_job_placement_support-fra.docx\">appuyer ses employ\u00e9s (Word)</a>, la DGGI a \u00e9labor\u00e9 une strat\u00e9gie d'information afin de maximiser les possibilit\u00e9s de placement des employ\u00e9s touch\u00e9s. Cette strat\u00e9gie est con\u00e7ue de fa\u00e7on \u00e0 fournir des ressources suppl\u00e9mentaires aux employ\u00e9s qui cherchent un nouvel emploi.</p>\n\n<p>Inscrivez-vous \u00e0 <a href=\"https://www.canada.ca/fr/commission-fonction-publique/emplois/services/emplois-gc.html\">Carri\u00e8res \u00e0 la fonction publique f\u00e9d\u00e9rale</a> pour recevoir des alertes-emplois et \u00eatre inform\u00e9 des occasions d'emploi au sein de la fonction publique.</p>\n\n<h2 id=\"Foire\">Foire aux questions</h2>\n\n<ul>\n     <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/faq_workforce_adjustment-fra.docx\">R\u00e9am\u00e9nagement des effectifs \u00e0 Agriculture et Agroalimentaire Canada : Questions et r\u00e9ponses (Word)</a></li>\n     <li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/hrb_dgrh/hr_csd_si_wfa_qa_job_placement_support-fra.docx\">Questions et r\u00e9ponses concernant le soutien pour le placement professionnel d'Agriculture et Agroalimentaire Canada (Word)</a></li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Information for Employees in career transition",
        "fr": "Information pour les employ\u00e9s en transition de carri\u00e8re"
    }
}