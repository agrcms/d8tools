{
    "dcr_id": "1325704881538",
    "lang": "en",
    "title": {
        "en": "Information Security Part II: Handling of Sensitive Information",
        "fr": "S\u00e9curit\u00e9 de l'information, 2e partie : la manipulation des renseignements sensibles"
    },
    "modified": "2018-02-22 00:00:00.0",
    "issued": "2012-01-05 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1485266766613",
    "layout_name": "1 column",
    "dc_date_created": "2012-01-04",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2012-01-05",
            "fr": "2012-01-05"
        },
        "modified": {
            "en": "2018-02-22",
            "fr": "2018-02-22"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Information Security Part II: Handling of Sensitive Information",
            "fr": "S\u00e9curit\u00e9 de l'information, 2e partie : la manipulation des renseignements sensibles"
        },
        "subject": {
            "en": "security",
            "fr": "s\u00e9curit\u00e9"
        },
        "description": {
            "en": "We previously identified the different categories of sensitive information and now we will highlight some of the best practices for handling the information. Sensitive information is recorded both on paper and electronically. There are different requirements for the two types of records which are outlined here.",
            "fr": "Au cours du volet pr\u00e9c\u00e9dent, nous avons d\u00e9crit les diff\u00e9rentes cat\u00e9gories de renseignements sensibles. Nous allons maintenant mettre en valeur quelques-unes des pratiques exemplaires relatives \u00e0 la manipulation de tels renseignements. Les renseignements sensibles sont conserv\u00e9s sur support papier ou \u00e9lectronique. \u00c0 ces deux types de support s'appliquent des exigences diff\u00e9rentes qui sont \u00e9nonc\u00e9es ici."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n \n<p>We previously identified the <a href=\"?id=1325703893933\">different categories of sensitive information</a> and now we will highlight some of the best practices for handling the information. Sensitive information is recorded both on paper and electronically. There are different requirements for the two types of records which are outlined here.</p>\n\n<h2>Document Standards:</h2>\n\n<p><b>Marking</b> - the Government Security Policy requires all documents containing sensitive information which is classified or protected to be marked appropriately. Documents must be marked with the classification level- <b>Confidential</b>, <b>Secret</b>, or <b>Top Secret</b> or protection level - <b>Protected A</b>, <b>B</b>, or <b>C</b> in upper case letters on the top right hand corner. Additionally with Top Secret documents it is necessary to ensure that the pages are numbered and copies identified (for example, page 1 of xx and copy number 1 of xx).</p>\n\n<h3>Storage</h3>\n\n<ul>\n\t<li>For Confidential, Protected A and B documents storage should be in an approved security container with an approved lock in an approved <b>operations zone</b>;</li>\n\t<li>For Secret, and Protected C documents storage should be in an approved security container with an approved lock in an approved <b>security zone</b>;</li>\n\t<li>For Top Secret documents storage should be in an approved security container with an approved lock in an approved <b>high security zone</b>.</li>\n</ul>\n\n\n<h3>Disposal</h3> \n\n<p>Sensitive information can be disposed of utilizing an approved security shredder, in accordance with <a href=\"http://www.rcmp-grc.gc.ca/physec-secmat/res-lim/pubs/seg/html/home_e.htm\">Royal Canadian Mounted Police (RCMP) Guide G1-001</a> and approved records disposition authorities. Note that there is a requirement for the use of different classes of shredders depending on the level of information being destroyed. For further information, please contact Records Management Services for guidance.</p>\n\n\n<h3>Mailing Standards - Transport and Transmission of information</h3>\n\n<ul>\n\t<li>Protected A and B and Confidential - single sealed envelope in both restricted and non restricted areas. No security markings on the envelope.</li>\n\t<li>Secret - Within a restricted area, single sealed envelope; outside a restricted area double sealed envelope. Address inner envelope to addressee, security mark inner envelope, address outer envelope.</li>\n\t<li>Top Secret and Protected C - Within a restricted area, single sealed envelope; outside a restricted area double sealed envelope. Address inner envelope to addressee, security mark inner envelope, address outer envelope. Must be sent using registered mail, priority post or reliable courier.</li>\n</ul>\n\n\n\n<h2>Electronic Standards:</h2>\n\n<h3>Storage</h3>\n\n<ul>\n\t<li>Protected A - Can be stored on Agriculture and Agri-Food Canada (AAFC)'s electronic infrastructure (including AgriDOC) without encryption (except when accessible via the internet).</li>\n\t<li>Protected B - The storage of Protected B information should be on AgriDOC or other AAFC approved corporate systems (such as Peoplesoft). Can be stored on removable media (diskette, tape, CD, USB memory stick) and secured in a security approved container. Information should only be stored on the network as an exception with access control for authorized personnel. Encryption, such as Public Key Infrastructure (PKI) is recommended to minimize the risk of exposing sensitive information.</li>\n\t<li>Confidential, Secret, Top Secret and Protected C - Can only be stored on removable media and locked in a security approved container.</li>\n</ul>\n\n\n<h3>Printing</h3>\n\n<ul>\n\t<li>Protected A and B - Network or local printer;</li>\n\t<li>Confidential, Secret, Top Secret and Protected C - Local printer only.</li>\n</ul>\n\n\n<h3>Transmission</h3> \n\n<ul>\n\t<li>Protected A -\n    <ul>\t\n      <li>Voice - Land line (no wireless);</li>\t\n      <li>Facsimile - Regular fax;</li>\t\n      <li>Internet/Intranet - Does not require encryption (PKI).</li>\n    </ul>\n  </li>\n\t<li>Protected B - \n    <ul>\t\n      <li>Voice - Land line (no wireless);</li>\t\n      <li>Facsimile - Regular fax with secure process (for example phone receiver and have the individual monitor the fax transmission);</li>\t\n      <li>Intranet - Does not require encryption;</li>\t\n      <li>Internet - Must be encrypted (PKI-Entrust).</li>\n    </ul>\n  </li>\n\t<li>Confidential, Secret, Top Secret and Protected C -\n    <ul>\t\n      <li>Voice - Secure phone;</li>\t\n      <li>Facsimile - Secure fax;</li>\t\n      <li>Internet/Intranet - Can not be transmitted.</li>\n    </ul>\n  </li>\n</ul>\n\n\n<h3>Disposal</h3>\n\n<ul>\n\t<li>Hardware - Contact the National Service Desk - Agrihelp.</li>\n</ul>\n\n\n\t\t",
        "fr": "\n\t\t\t  \n \n<p>Au cours du volet pr\u00e9c\u00e9dent, nous avons d\u00e9crit les <a href=\"?id=1325703893933\">diff\u00e9rentes cat\u00e9gories de renseignements sensibles</a>. Nous allons maintenant mettre en valeur quelques-unes des pratiques exemplaires relatives \u00e0 la manipulation de tels renseignements. Les renseignements sensibles sont conserv\u00e9s sur support papier ou \u00e9lectronique. \u00c0 ces deux types de support s'appliquent des exigences diff\u00e9rentes qui sont \u00e9nonc\u00e9es ici.</p>\n\n<h2>Normes relatives aux documents</h2>\n\n<p><b>Marquage</b>\u00a0: la Politique du gouvernement sur la s\u00e9curit\u00e9 exige que tous les documents comportant des renseignements sensibles - classifi\u00e9s ou prot\u00e9g\u00e9s - soient ad\u00e9quatement marqu\u00e9s. Les documents sont obligatoirement marqu\u00e9s en majuscules au coin droit sup\u00e9rieur de fa\u00e7on \u00e0 indiquer leur niveau de classification - <b>Confidentiel</b>, <b>Secret</b> ou <b>Tr\u00e8s secret</b> - ou leur niveau de protection - <b>Prot\u00e9g\u00e9 A, B</b> ou <b>C</b>. De plus, quand il s'agit de documents class\u00e9s Tr\u00e8s secret, il faut s'assurer que les pages sont num\u00e9rot\u00e9es et que les copies sont identifi\u00e9es (par exemple, page 1 de xx et copie num\u00e9ro 1 de xx).</p>\n\n<h3>Entreposage</h3>\n\n<ul>\n\t<li>Les documents class\u00e9s Confidentiel ou Prot\u00e9g\u00e9 A ou B doivent \u00eatre entrepos\u00e9s dans un classeur de s\u00e9curit\u00e9 approuv\u00e9 avec cadenas approuv\u00e9 dans une <b>zone de travail</b> approuv\u00e9e.</li>\n\t<li>Les documents class\u00e9s Secret ou Prot\u00e9g\u00e9 C doivent \u00eatre entrepos\u00e9s dans un classeur de s\u00e9curit\u00e9 approuv\u00e9 avec cadenas approuv\u00e9 dans une <b>zone de s\u00e9curit\u00e9</b> approuv\u00e9e.</li>\n\t<li>Les documents class\u00e9s Tr\u00e8s secret doivent \u00eatre entrepos\u00e9s dans un classeur de s\u00e9curit\u00e9 approuv\u00e9 avec cadenas approuv\u00e9 dans une <b>zone de s\u00e9curit\u00e9 \u00e9lev\u00e9e</b> approuv\u00e9e.</li>\n</ul>\n\n\n<h3>Destruction</h3> \n\n<p>L'information sensible peut \u00eatre d\u00e9truite au moyen d'une d\u00e9chiqueteuse de s\u00e9curit\u00e9 approuv\u00e9e, conform\u00e9ment au <a href=\"http://www.rcmp-grc.gc.ca/physec-secmat/res-lim/pubs/seg/html/home_f.htm\">Guide G1-001 de la Gendarmerie royale du Canada (GRC)</a> et aux autorisations approuv\u00e9es pour la destruction des dossiers. Notez que l'utilisation de d\u00e9chiqueteuses de diff\u00e9rentes cat\u00e9gories est exig\u00e9e selon le niveau de classification ou de protection de l'information \u00e0 d\u00e9truire. Pour de plus amples renseignements, veuillez communiquer avec le Service de gestion des dossiers.</p>\n\n\n<h3>Normes visant le transport et la communication des renseignements par la poste</h3>\n\n<ul>\n\t<li>Confidentiel ou Prot\u00e9g\u00e9 A ou B\u00a0: \u00e0 l'int\u00e9rieur comme \u00e0 l'ext\u00e9rieur d'une zone \u00e0 acc\u00e8s r\u00e9glement\u00e9, enveloppe simple scell\u00e9e. Aucune cote de s\u00e9curit\u00e9 inscrite sur l'enveloppe.</li>\n\t<li>Secret\u00a0: dans une zone \u00e0 acc\u00e8s r\u00e9glement\u00e9, enveloppe simple scell\u00e9e; \u00e0 l'ext\u00e9rieur d'une zone \u00e0 acc\u00e8s r\u00e9glement\u00e9, sous double pli cachet\u00e9. Adresser l'enveloppe int\u00e9rieure au destinataire; cote de s\u00e9curit\u00e9 sur l'enveloppe int\u00e9rieure, adresse de l'exp\u00e9diteur sur l'enveloppe ext\u00e9rieure.</li>\n\t<li>Tr\u00e8s secret et Prot\u00e9g\u00e9 C\u00a0: dans une zone \u00e0 acc\u00e8s r\u00e9glement\u00e9, enveloppe simple scell\u00e9e; \u00e0 l'ext\u00e9rieur d'une zone \u00e0 acc\u00e8s r\u00e9glement\u00e9, sous double pli cachet\u00e9. Adresser l'enveloppe int\u00e9rieure au destinataire; cote de s\u00e9curit\u00e9 sur l'enveloppe int\u00e9rieure, adresse de l'exp\u00e9diteur sur l'enveloppe ext\u00e9rieure. Obligatoire\u00a0: envoi par courrier recommand\u00e9, par poste prioritaire ou par le biais d'un service de courrier fiable.</li>\n</ul>\n\n\n\n<h2>Normes visant les documents \u00e9lectroniques</h2>\n\n<h3>Entreposage</h3>\n\n<ul>\n\t<li>Prot\u00e9g\u00e9 A\u00a0: peuvent \u00eatre entrepos\u00e9s sur l'infrastructure \u00e9lectronique d'Agriculture et Agroalimentaire Canada (AAC) (y compris sur AgriDOC) sans chiffrement (sauf si accessibles par Internet).</li>\n\t<li>Prot\u00e9g\u00e9 B\u00a0: les renseignements class\u00e9s Prot\u00e9g\u00e9 B doivent \u00eatre stock\u00e9s dans AgriDOC ou dans un autre syst\u00e8me int\u00e9gr\u00e9 approuv\u00e9 par AAC (par exemple, PeopleSoft). Ils peuvent \u00eatre sauvegard\u00e9s sur un support amovible (disquette, bande magn\u00e9tique, CD ou cl\u00e9 USB) et plac\u00e9s dans un classeur de s\u00e9curit\u00e9 approuv\u00e9. L'information ne doit \u00eatre stock\u00e9e sur le r\u00e9seau qu'\u00e0 titre exceptionnel et son acc\u00e8s doit \u00eatre restreint au personnel autoris\u00e9. Le chiffrement, par exemple l'infrastructure \u00e0 cl\u00e9s publiques (ICP), est recommand\u00e9 pour minimiser le risque d'exposition des renseignements de nature d\u00e9licate.</li>\n\t<li>Confidentiel, Secret, Tr\u00e8s secret ou Prot\u00e9g\u00e9 C\u00a0: les renseignements sont stock\u00e9s uniquement sur des supports amovibles et verrouill\u00e9s dans un classeur de s\u00e9curit\u00e9 approuv\u00e9.</li>\n</ul>\n\n\n<h3>Impression</h3>\n\n<ul>\n\t<li>Prot\u00e9g\u00e9 A ou B\u00a0: imprimante locale ou de r\u00e9seau.</li>\n\t<li>Confidentiel, Secret, Tr\u00e8s secret ou Prot\u00e9g\u00e9 C\u00a0: imprimante locale seulement.</li>\n</ul>\n\n\n<h3>Transmission</h3>\n\n<ul>\n\t<li>Prot\u00e9g\u00e9 A\u00a0:\n  <ul>\t\n   <li>Voix - ligne terrestre (aucune transmission sans fil);</li>\t\n   <li>T\u00e9l\u00e9copie - t\u00e9l\u00e9copieur ordinaire;</li>\t\n   <li>Internet/intranet - n'a pas besoin de chiffrement (ICP).</li>\n  </ul>\n </li>\n\t<li>Prot\u00e9g\u00e9 B\u00a0:\n  <ul>\t\n   <li>Voix - ligne terrestre (aucune transmission sans fil);</li>\t\n   <li>T\u00e9l\u00e9copie - t\u00e9l\u00e9copieur ordinaire avec proc\u00e9d\u00e9 s\u00e9curitaire (par exemple, r\u00e9cepteur t\u00e9l\u00e9phonique et supervision de la transmission);</li>\t\n   <li>Intranet - n'a pas besoin de chiffrement;</li>\t\n   <li>Internet - chiffrement obligatoire (ICP Entrust).</li>\n  </ul>\n </li>\n\t<li>Confidentiel, Secret, Tr\u00e8s secret ou Prot\u00e9g\u00e9 C\u00a0:\n  <ul>\t\n   <li>Voix - t\u00e9l\u00e9phone cryptophonique;</li>\t\n   <li>T\u00e9l\u00e9copie - t\u00e9l\u00e9copieur prot\u00e9g\u00e9;</li>\t\n   <li>Internet/intranet - aucune transmission.</li>\n  </ul>\n </li>\n</ul>\n\n\n<h3>Destruction</h3>\n\n<ul>\n\t<li>Mat\u00e9riel - communiquez avec le Bureau de service national - AgriSecours.</li>\n</ul>\n\n\n\t\t"
    },
    "breadcrumb": {
        "en": "Information Security Part II: Handling of Sensitive Information",
        "fr": "S\u00e9curit\u00e9 de l'information, 2e partie : la manipulation des renseignements sensibles"
    }
}