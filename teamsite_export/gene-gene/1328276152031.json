{
    "dcr_id": "1328276152031",
    "lang": "en",
    "title": {
        "en": "Acquisition Cards ",
        "fr": "Cartes d'achat "
    },
    "modified": "2020-09-30 00:00:00.0",
    "issued": "2012-02-07 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1320677087463",
    "layout_name": "1 column",
    "dc_date_created": "2012-02-03",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2012-02-07",
            "fr": "2012-02-07"
        },
        "modified": {
            "en": "2020-09-30",
            "fr": "2020-09-30"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Acquisition Cards ",
            "fr": "Cartes d'achat "
        },
        "subject": {
            "en": "finance;property management",
            "fr": "finances;gestion des biens"
        },
        "description": {
            "en": "Acquisition cards are for buying low dollar amount, low risk goods and services for Government business. Only delegated persons authorized under the Financial Administration Act may use acquisition cards.",
            "fr": "Les cartes d'achat sont utilis\u00e9es pour l'achat de biens et services de faible valeur et \u00e0 faible risque, qui serviront \u00e0 la r\u00e9alisation des activit\u00e9s gouvernementales. Seules les personnes qui poss\u00e8dent des pouvoirs conf\u00e9r\u00e9s par la Loi sur la gestion des finances publiques peuvent les utiliser."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "acquisitions, procurement, purchasing",
            "fr": "achat"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "resource list",
            "fr": "liste de r\u00e9f\u00e9rence"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<section>\n\t\t\t  \n\n<div class=\"row\">\n\n<div class=\"col-md-4 pull-right\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Emergency numbers</h2>\n</header>\n<div class=\"panel-body\">\n<p><b>MasterCard</b></p>\n<ul>\n  <li>Account information \n    <ul>\n      <li>1-800-263-2263</li>\n      <li>1-416-283-2263</li>\n    </ul>\n  </li>\n  <li>Lost or stolen cards\n    <ul>\n      <li>1-800-361-3361 (Canada, United States)</li>\n      <li>1-416-232-8020 (call collect from other countries)</li>\n    </ul>\n  </li>\n</ul>\n</div>\n</section>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n\n<p>Acquisition cards are for buying low dollar amount, low risk goods and services for Government business. Only delegated persons authorized under the <cite>Financial Administration Act</cite> may use acquisition cards.</p>\n\n<p>Acquisition cards are to be used only for the day-to-day purchase of goods and services for maintenance, repair, and operations. They are not for large dollar transactions such as for fleet-related operating expenses, travel, acquisition of capital assets, and other complex transactions.</p>\n\n<p>In addition, only designated Procurement Officers and Integrated Service Managers within the Asset Team of the Corporate Management Branch can use acquisition cards for buying computer hardware, software and mobile devices. Please refer to <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/cmb_dggi/procurement_of_computer_software_hardware_and_mobile_devices-eng.docx\">2011-12-21 - Bulletin - Procurement of Computer Software/Hardware and Mobile Devices (Word)</a>.</p>\n\n\n<h2>Departmental References</h2>\n<h3>Policies</h3>\n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/cmb_dggi/policies_guidelines/2016-01-07_acquisition_cards_policy-eng.doc\">Policy on Acquisition cards (Word)</a></li></ul>\n\n<h3>Procedures</h3>\n<ul><li><a href=\"display-afficher.do?id=1312405863550&amp;lang=eng\">Acquisition Cards Handbook</a></li>\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/cmb_dggi/procurement_of_computer_software_hardware_and_mobile_devices-eng.docx\">2011-12-21 - Bulletin - Procurement of Computer Software/Hardware and Mobile Devices (Word)</a></li>\n\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/a_z_index/acquisition_card-eng.pdf\">Acquisition Card Application Form (PDF)</a></li>\n</ul>\n</div>\n</div>\n\n<h2>Treasury Board of Canada Secretariat</h2>\n<h3>Policies</h3>\n<ul><li><a href=\"http://publiservice.tbs-sct.gc.ca/pol/doc-eng.aspx?id=17059\">Directive on Acquisition Cards</a></li>\n\n<li><a href=\"http://publiservice.tbs-sct.gc.ca/fm-gf/tools-outils/guides/ac-ca-eng.asp\">Acquisition and Travel Cards</a></li></ul>\n\n<h2>Other References </h2>\n<ul><li><a href=\"http://gcintranet.tpsgc-pwgsc.gc.ca/rg/manuels-manuals/chap9/index-eng.html\">2016-08-03 \u2013 Receiver General Manual: Chapter 9, Government of Canada Acquisition Card Program</a></li>\n\n<li><a href=\"http://www.oag-bvg.gc.ca/internet/English/parl_oag_200705_01_e_17476.html\">2007 May Report of the Auditor General of Canada: Chapter 1, Use of Acquisition and Travel Cards</a></li></ul>\n\n<h2>Applying for an Acquisition Card</h2>\n<p>To request an acquisition card, you must use the most recent form that combines the delegation of authority with the request to issue a MasterCard. The <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/a_z_index/acquisition_card-eng.pdf\">Acquisition Card Application Form (PDF)</a> was revised in August 2011 to coincide with the changes in the departmental Policy on Acquisition Cards.</p> \n\n<p>Forward your completed application to:</p>\n\n<p>Lorraine Burnett<br>\nDirector<br>\nMateriel Management<br>\nRoom 353, Floor 5<br>\n1341 Baseline Road, Tower 3<br>\nOttawa, Ontario K1A 0C5<br>\nFax: 613-773-0966</p>\n\n<p><b>Other Commercial Credit Cards Not Allowed</b></p>\n\n<p>Under the present Government agreement for acquisition cards with Bank of Montreal (BMO), departments are not allowed the use of acquisition cards other than the BMO-MasterCard that bear the name of the Government of Canada.</p>\n<p>Please inform all staff concerned.</p>\n<p>If you have any questions, please contact your Regional Coordinator.</p>\n\n\n<h2>Contact us</h2>\n\n<h3>Contracting/Purchasing</h3>\n<p>Melissa Petit: <a href=\"melissa.petit@canada.ca\">melissa.petit@canada.ca</a></p>\n\n<h3>Card coordination and policy interpretation</h3>\n\n<p class=\"mrgn-bttm-0\"><b>Ontario/Quebec/Atlantic</b></p>\n<ul class=\"list-unstyled\">\n<li>Karen Tam: <a href=\"mailto:karen.tam@canada.ca\">karen.tam@canada.ca</a></li>\n<li>Yvonne Lee: <a href=\"mailto:yvonne.lee@canada.ca\">yvonne.lee@canada.ca</a></li>\n</ul>\n\n<p class=\"mrgn-bttm-0\"><b>National Capital Region</b></p>\n<ul class=\"list-unstyled\">\n<li>Isabelle Otis: <a href=\"mailto:isabelle.otis@canada.ca\">isabelle.otis@canada.ca</a></li>\n</ul>\n\n<p class=\"mrgn-bttm-0\"><b>Midwest/Pacific</b></p>\n<ul class=\"list-unstyled\">\n<li>Mark Danaher: <a href=\"mailto:mark.danaher@canada.ca\">mark.danaher@canada.ca</a></li>\n<li>Darryl Ward: <a href=\"mailto:darryl.ward@canada.ca\">darryl.ward@canada.ca</a></li>\n</ul>\n\n<h3>Financial Policy</h3>\n<p><a href=\"mailto:aafc.financialpolicy-politiquefinanciere.aac@canada.ca\">aafc.financialpolicy-politiquefinanciere.aac@canada.ca</a></p>\n\t\t</section>",
        "fr": "<section>\n\t\t\t  \n\n<div class=\"row\">\n\n<div class=\"col-md-4 pull-right\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Num\u00e9ros d'urgence</h2>\n</header>\n<div class=\"panel-body\">\n<p><b>MasterCard</b></p>\n<ul><li>Renseignements sur les comptes \n<ul><li>1-800-263-2263</li>\n<li>1-416-283-2263</li></ul></li>\n<li>Cartes perdues ou vol\u00e9es \n<ul><li>1-800-361-3361 (Canada, \u00c9tats-Unis)</li>\n<li>1-416-232-8020 (autres pays \u00e0 frais vir\u00e9s)</li>\n</ul></li></ul>\n</div>\n</section>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n<p>Les cartes d'achat sont utilis\u00e9es pour l'achat de biens et services de faible valeur et \u00e0 faible risque, qui serviront \u00e0 la r\u00e9alisation des activit\u00e9s gouvernementales. Seules les personnes qui poss\u00e8dent des pouvoirs conf\u00e9r\u00e9s par la <cite>Loi sur la gestion des finances publiques</cite> peuvent les utiliser.</p>\n\n<p>Les cartes d'achat servent uniquement \u00e0 l'achat quotidien des biens et services aff\u00e9rents \u00e0 l'entretien, aux r\u00e9parations et aux op\u00e9rations. Elles ne sont pas destin\u00e9es aux achats de grande valeur, comme les d\u00e9penses d'exploitation li\u00e9es au parc automobile, les voyages et l'achat d'immobilisations, et autres transactions complexes.</p>\n\n<p>De plus, seulement certains agents d'approvisionnement et gestionnaires des services int\u00e9gr\u00e9s au sein de l'\u00c9quipe de la gestion des biens de la Direction g\u00e9n\u00e9rale de la gestion int\u00e9gr\u00e9e peuvent utiliser une carte d'achat pour se procurer du mat\u00e9riel informatique, des logiciels et des appareils mobiles. Veuillez consulter le <a href=\"https://intranet.agr.gc.ca/resources/prod/doc/cmb_dggi/procurement_of_computer_software_hardware_and_mobile_devices-fra.docx\">2011-12-21 - Bulletin - Acquisition de logiciels/mat\u00e9riel informatique et d'appareils mobiles (Word)</a>.</p>\n\n\n<h2>R\u00e9f\u00e9rences minist\u00e9rielles</h2>\n\n<h3>Politiques</h3>\n<ul><li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/cmb_dggi/policies_guidelines/2016-01-07_acquisition_cards_cartes_pol-fra.doc\">Politique sur les cartes d'achat (Word)</a></li></ul>\n\n<h3>M\u00e9thodes</h3>\n<ul>\n<li><a href=\"display-afficher.do?id=1312405863550&amp;lang=fra\">Manuel de cartes d'achat</a></li>\n\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/cmb_dggi/procurement_of_computer_software_hardware_and_mobile_devices-fra.docx\">2011-12-21 - Bulletin - Acquisition de logiciels/mat\u00e9riel informatique et d'appareils mobiles (Word)</a></li>\n\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/a_z_index/carte_achat-fra.pdf\">Formulaire\u00a0: Demande d'adh\u00e9sion de carte d'achat (PDF)</a></li>\n</ul>\n</div>\n</div>\n\n<h2>Secr\u00e9tariat du Conseil du Tr\u00e9sor du Canada</h2>\n\n<h3>Politiques</h3>\n<ul>\n<li><a href=\"http://publiservice.tbs-sct.gc.ca/pol/doc-fra.aspx?id=17059\">Directive sur les cartes d'achat</a></li>\n<li><a href=\"http://publiservice.tbs-sct.gc.ca/fm-gf/tools-outils/guides/ac-ca-fra.asp\">Cartes d'achat et de voyage</a></li></ul>\n\n<h2>Autres r\u00e9f\u00e9rences</h2>\n<ul>\n\n<li><a href=\"http://gcintranet.tpsgc-pwgsc.gc.ca/rg/manuels-manuals/chap9/index-fra.html\">2016-08-03 \u2013 Manuel du receveur g\u00e9n\u00e9ral, chapitre\u00a09, Programme des cartes d'achat du gouvernement du Canada</a></li>\n\n<li><a href=\"http://www.oag-bvg.gc.ca/internet/Francais/parl_oag_200705_01_f_17476.html\">Rapport (2007) de la v\u00e9rificatrice g\u00e9n\u00e9rale 2007\u00a0mai\u00a0- Rapport de la v\u00e9rificatrice g\u00e9n\u00e9rale du Canada, Chapitre\u00a01\u00a0- L'utilisation des cartes d'achat et de voyage</a></li>\n</ul>\n\n<h2>Demande de carte d'achat</h2>\n<p>Afin de demander une carte d'achat, vous devez utiliser le formulaire le plus r\u00e9cent qui combine la d\u00e9l\u00e9gation de pouvoir de d\u00e9penser avec la demande d'\u00e9mission d'une carte MasterCard. La <a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/ccb_dgcc/a_z_index/carte_achat-fra.pdf\">formulaire de demande d'adh\u00e9sion de carte d'achat (PDF)</a> a fait l'objet d'une r\u00e9vision en Ao\u00fbt\u00a02011 pour correspondre aux modifications \u00e0 la politique minist\u00e9rielle sur les cartes d'achat.</p> \n\n<p>Faire parvenir le formulaire rempli \u00e0\u00a0:</p>\n\n<p>Lorraine Burnett<br>\nDirectrice<br>\nGestion des mesures d'urgence et du mat\u00e9riel<br>\nPi\u00e8ce 333, \u00c9tage 5<br>\n1341 chemin Baseline, Tour 3<br>\nOttawa (Ontario) K1A 0C5<br>\nT\u00e9l\u00e9copieur\u00a0: 613-773-0966</p>\n\n<p><b>Autres cartes de cr\u00e9dit commerciales non permis</b></p>\n\n<p>Sous le pr\u00e9sent programme de cartes d'achat du gouvernement du Canada avec Banque de Montr\u00e9al (BMO), les minist\u00e8res ne peuvent pas utiliser des cartes d'achat autres que la carte BMO-MasterCard qui portent le nom du gouvernement du Canada.</p>\n\n<p>Veuillez aviser le personnel concern\u00e9.</p>\n\n<p>Si vous avez des questions veuillez contacter votre coordonnateur r\u00e9gional.</p>\n\n<h2>Communiquez avec nous</h2>\n\n<h3>March\u00e9s/achats</h3>\n<p>Melissa Petit\u00a0: <a href=\"mailto:melissa.petit@canada.ca\">melissa.petit@canada.ca</a></p>\n\n<h3>Coordination des cartes et interpr\u00e9tation des politiques</h3>\n\n<p class=\"mrgn-bttm-0\"><b>Ontario/Qu\u00e9bec/Atlantic</b></p>\n<ul class=\"list-unstyled\">\n<li>Karen Tam\u00a0: <a href=\"mailto:karen.tam@canada.ca\">karen.tam@canada.ca</a></li>\n<li>Yvonne Lee\u00a0: <a href=\"mailto:yvonne.lee@canada.ca\">yvonne.lee@canada.ca</a></li>\n</ul>\n\n<p class=\"mrgn-bttm-0\"><b>R\u00e9gion de la capitale nationale</b></p>\n<ul class=\"list-unstyled\">\n<li>Isabelle Otis\u00a0: <a href=\"mailto:isabelle.otis@canada.ca\">isabelle.otis@canada.ca</a></li>\n</ul>\n\n<p class=\"mrgn-bttm-0\"><b>Centre-Ouest/Pacifique</b></p>\n<ul class=\"list-unstyled\">\n<li>Mark Danaher\u00a0: <a href=\"mailto:mark.danaher@canada.ca\">mark.danaher@canada.ca</a></li>\n<li>Darryl Ward\u00a0: <a href=\"mailto:darryl.ward@canada.ca\">darryl.ward@canada.ca</a></li>\n</ul>\n\n<h3>Politiques financi\u00e8res</h3>\n<p><a href=\"mailto:aafc.financialpolicy-politiquefinanciere.aac@canada.ca\">aafc.financialpolicy-politiquefinanciere.aac@canada.ca</a></p>\n\t\t</section>"
    },
    "breadcrumb": {
        "en": "Acquisition Cards",
        "fr": "Cartes d'achat"
    }
}