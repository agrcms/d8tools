{
    "dcr_id": "1288008167528",
    "lang": "en",
    "title": {
        "en": "Classification Grievance Procedures",
        "fr": "Proc\u00e9dure de r\u00e8glement des griefs de classification"
    },
    "modified": "2018-12-17 00:00:00.0",
    "issued": "2010-11-17 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279035844850",
    "layout_name": "1 column",
    "dc_date_created": "2010-11-17",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2010-11-17",
            "fr": "2010-11-17"
        },
        "modified": {
            "en": "2018-12-17",
            "fr": "2018-12-17"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Classification Grievance Procedures",
            "fr": "Proc\u00e9dure de r\u00e8glement des griefs de classification"
        },
        "subject": {
            "en": "classification",
            "fr": "classification"
        },
        "description": {
            "en": "The following documentation should be sent to the Corporate Programs Division to process a grievance submission.",
            "fr": "Les documents suivants doivent \u00eatre envoy\u00e9s \u00e0 la Division des services int\u00e9gr\u00e9s (Classification) au moment du d\u00e9p\u00f4t d'un grief de classification."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "grievance submission",
            "fr": "d\u00e9p\u00f4t d'un grief de classification"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>A classification grievance: </p>\n<ul>\n  <li>is a written complaint by an  employee against the <strong>classification of  the work</strong> assigned by the responsible manager,</li>\n  <li>is specifically regarding \u00a0<strong>the  position the grievor (the person grieving) occupies,</strong> and</li>\n  <li>may only be filed when the  classification of the work is <strong>described  in the job description</strong>. </li>\n</ul>\n\n<p>Classification means, \u201coccupational group, subgroup or the level of a  position.\u201d It <strong>does not include the  content of the job description, the effective date or other matters</strong>, which  can be resolved through the Labour Relations grievance process.</p>\n\n<h2>Classification  grievance procedure</h2>\n\n<p>Classification  grievances are processed in accordance with the Treasury Board of Canada  Secretariat <a href=\"http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=28698\">Directive on  Classification Grievances</a>, Appendix B: Classification Grievance  Procedure.</p>\n\n<h2>Who can submit a classification grievance?</h2>\n<p>The <a href=\"http://lois-laws.justice.gc.ca/eng/acts/P-33.3/\">Public Service  Labour Relations Act</a> determines who has the right to  grieve. This right is provided, but not limited, to those employed in the core  public administration in a managerial or confidential capacity, and it  excludes, among others:</p>\n  <ul>\n    <li>Persons employed on a casual basis;</li>\n    <li>Persons employed on a term basis for a period of less than three  months; and</li>\n    <li>Persons required to work less than one third of the normal period for  persons doing similar work.</li>\n  </ul>\n\n<h2>When can a  classification grievance be submitted?</h2>\n\n<p>When a  classification decision (for example, classification group, sup-group, level and/or  ratings) is rendered affecting a position(s), Agriculture and Agri-Food Canada (AAFC)  managers will formally notify employees occupying the position(s) in writing of  the classification decision. </p>\n<p>A classification grievance must be  presented by an employee no later than 35 calendar days after the day on which  the employee receives notification or, when the employee has not received such  notification, no later than 35 calendar days after the day on which he or she  first becomes aware of an action or circumstance affecting the classification  of the position he  or she occupies.</p>\n\n<h2>How  does an employee submit a classification grievance?</h2>\n<p>An employee presenting a classification grievance should  use the <a href=\"http://intranet.canada.ca/ppb-rpa/lm-rps/cg-gc/tbssct-340-55-eng.asp\">Individual Grievance Presentation</a> form. Details of the grievance should clearly state the  substance of the grievance, the corrective action requested and the name of the  grievor\u2019s representative, if applicable. The grievance form must be signed and  dated by the grievor. The grievance must be presented to the employee\u2019s  immediate supervisor or local officer in charge.</p>\n\n<h2>Next steps for supervisors/managers if  presented with a classification grievance</h2> \n  <ul>\n    <li>Sign the grievance form to indicate it has been received.</li>\n    <li>Return a signed copy of the form to the grievor.</li>\n    <li>Send the original grievance form to any  member of the <a href=\"http://directinfo.agr.gc.ca/directInfo/eng/index.php?fuseaction=agriInfo.orgUnit&amp;dn=ou=ODC-COC,ou=SCEE-DCEME,ou=WRD-DRMT,ou=HRB-RH,OU=CMB-DGGI,OU=AAFC-AAC,o=gc,c=ca\">Classification team</a> (see <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=eng\" target=\"_blank\">Classification Advisor (Word)</a> list) or by email to <a href=\"mailto:aafc.classification.aac@canada.ca\">aafc.classification.aac@canada.ca</a> as soon as possible.</li>\n    <li>Supervisors and managers will be guided  through the rest of the process by their Classification Advisor.</li>\n  </ul>\n\n<h2>How do Classification grievances differ  from than other types of grievances?</h2>\n<ul>\n  <li>All classification grievances are heard only once, by a Classification Grievance Committee of three members, including a Chairperson. The classification grievance will not be heard until all matters related to the content of the job description have been resolved.</li>\n  <li>Once the grievor or union representative has made a presentation at the classification grievance hearing, the grievor and/ or representative will be asked to leave the room. The Committee members will then deliberate. The Classification Grievance Committee members will subsequently submit a recommendation to the Deputy Head\u2019s Delegate for Classification Grievances. At AAFC, the Deputy Head\u2019s Delegate is the Director General of Human Resources. The Delegate\u2019s decision is considered final and binding.</li>\n  <li>The decision rendered could result in the upgrading, confirmation or downgrading of the grieved position.</li>\n</ul>\n\n<p>For more  information on classification grievances and procedural details, please refer  to the Treasury Board of Canada Secretariat <a href=\"http://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=28698\">Directive on Classification  Grievances</a>.</p>\n\n<h2>Contact us</h2>\n\n<p>Questions? We can help. Contact any member of the Classification team (see <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=eng\" target=\"_blank\">Classification Advisor (Word)</a> list) or email <a href=\"mailto:aafc.classification.aac@canada.ca\">aafc.classification.aac@canada.ca</a> or the  Corporate Classification Grievances Unit at: <a href=\"mailto:aafc.corporateclassification-classificationministerielle.aac@canada.ca\">aafc.corporateclassification-classificationministerielle.aac@canada.ca</a>.</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Un grief de classification\u00a0:</p>\n<ul>\n  <li>est une plainte r\u00e9dig\u00e9e par un employ\u00e9 qui s'oppose \u00e0 la <b>classification du poste</b> qui lui a \u00e9t\u00e9 attribu\u00e9 par le gestionnaire.</li>\n<li>cette plainte concerne express\u00e9ment le <b>poste occup\u00e9 par le plaignant (la personne qui d\u00e9pose le grief)</b>.</li>\n<li>peut \u00eatre d\u00e9pos\u00e9e seulement lorsque la classification du poste est <b>d\u00e9crite dans la description de poste</b>.</li>\n</ul>\n\n<p>La classification correspond \u00e0 un groupe ou \u00e0 un  sous-groupe professionnel ou au niveau d'un poste. Elle <strong>ne porte ni sur le contenu, ni sur la date d'entr\u00e9e en vigueur de la description  de poste, ni sur d'autres questions</strong> pouvant \u00eatre r\u00e9solues au moyen du  processus de r\u00e8glement des griefs des Relations de travail.</p>\n<h2>Proc\u00e9dure de r\u00e8glement des griefs de classification</h2><p>\n Les griefs de classification sont trait\u00e9s conform\u00e9ment \u00e0 l'annexe B, Proc\u00e9dure de r\u00e8glement des griefs de classification, de la  <a href=\"http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=28698\">Directive sur les griefs de classification</a> du Secr\u00e9tariat du Conseil du Tr\u00e9sor du Canada. </p>\n<h2>Qui peut pr\u00e9senter un grief de classification?</h2><p>\n  La <a href=\"http://lois-laws.justice.gc.ca/fra/lois/P-33.3/\">Loi sur les relations de travail dans la fonction publique</a> d\u00e9termine qui a le droit de pr\u00e9senter un grief de classification. Ce droit est notamment accord\u00e9 aux personnes qui occupent des postes de direction ou de confiance dans l'administration publique centrale, et exclut, entre autres, les personnes suivantes\u00a0:</p>\n\n  <ul>\n    <li>Personnes employ\u00e9es \u00e0 titre occasionnel;</li>\n<li>Personnes employ\u00e9es pour une dur\u00e9e d\u00e9termin\u00e9e de moins de trois mois;</li>\n<li>Personnes qui doivent travailler moins du tiers du temps normalement exig\u00e9 des employ\u00e9s ex\u00e9cutant des t\u00e2ches semblables</li>\n  </ul>\n\n<h2>Quand un grief de classification peut-il \u00eatre pr\u00e9sent\u00e9?</h2>\n\n<p>Lorsqu'une d\u00e9cision de classification (par exemple, un groupe, un sous-groupe, un niveau et/ou une cote de classification) est rendue \u00e0 l'\u00e9gard d'un ou de plusieurs postes, les  gestionnaires d'Agriculture et Agroalimentaire Canada en informent officiellement,  par \u00e9crit, les employ\u00e9s qui occupent le ou les postes vis\u00e9s par cette d\u00e9cision de classification.</p>\n<p>Un employ\u00e9 doit pr\u00e9senter un grief de classification au plus tard 35\u00a0jours civils  apr\u00e8s la date \u00e0 laquelle il a re\u00e7u un avis ou, si l'employ\u00e9 ne re\u00e7oit aucun avis, 35\u00a0jours civils apr\u00e8s le jour o\u00f9 il apprend l'existence d'une mesure  ou d'une circonstance touchant le poste qu'il  occupe.</p>\n\n<h2>Pr\u00e9sentation d'un grief de classification par un employ\u00e9</h2>\n\n<p>L'employ\u00e9 qui souhaite pr\u00e9senter un grief de classification doit le faire au moyen du formulaire <a href=\"http://intranet.canada.ca/ppb-rpa/lm-rps/cg-gc/tbssct-340-55-fra.asp\">Pr\u00e9sentation d'un grief individuel</a>. L'\u00e9nonc\u00e9 du grief doit clairement d\u00e9crire la nature du grief, la mesure corrective demand\u00e9e et le nom du repr\u00e9sentant du plaignant, s'il y a lieu. Le formulaire de grief doit \u00eatre sign\u00e9 et dat\u00e9 par le plaignant. Les employ\u00e9s doivent pr\u00e9senter leur grief par \u00e9crit \u00e0 leur surveillant imm\u00e9diat ou au chef de service local.</p>  \n  \n<h2>\u00c9tapes \u00e0 suivre par les superviseurs et les gestionnaires qui re\u00e7oivent un grief de classification</h2> \n    <ol>\n      <li>Signer le formulaire de grief afin de confirmer  qu'il a \u00e9t\u00e9 re\u00e7u. </li>\n      <li>Remettre une copie sign\u00e9e du formulaire au  plaignant. </li>\n      <li>Envoyer le formulaire de grief original \u00e0 un  membre de l'\u00e9quipe de classification (voir la liste <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=fra\" target=\"_blank\">conseiller en classification (Word)</a>) ou envoyer un  courriel \u00e0 <a href=\"mailto:aafc.classification.aac@canada.ca\">aafc.classification.aac@canada.ca</a> d\u00e8s que  possible. </li>\n      <li>Les superviseurs et les gestionnaires seront guid\u00e9s  par leur conseiller en classification au cours des autres \u00e9tapes du processus. </li>\n    </ol>\n\n<h2>En quoi les griefs de classification diff\u00e8rent-ils des autres types de grief?</h2>\n\n<ul>\n  <li>Les griefs de classification font l'objet d'une seule audience, devant un comit\u00e9 d'examen des griefs de classification compos\u00e9 de trois membres, dont un pr\u00e9sident. L'audience ne peut pas avoir lieu tant que toutes les questions relatives au contenu de la description de poste n'ont pas \u00e9t\u00e9 r\u00e9gl\u00e9es. </li>\n<li>Apr\u00e8s que le plaignant ou le repr\u00e9sentant syndical ont pr\u00e9sent\u00e9 le grief de classification \u00e0 l'audience, ils sont pri\u00e9s de sortir de la salle. Apr\u00e8s avoir d\u00e9lib\u00e9r\u00e9, les membres du Comit\u00e9 soumettent une recommandation au d\u00e9l\u00e9gu\u00e9 de l'administrateur g\u00e9n\u00e9ral responsable des griefs de classification. \u00c0 ACC, ce d\u00e9l\u00e9gu\u00e9 est le directeur g\u00e9n\u00e9ral des Ressources humaines. La d\u00e9cision du d\u00e9l\u00e9gu\u00e9 est consid\u00e9r\u00e9e comme \u00e9tant d\u00e9finitive et ex\u00e9cutoire. </li>\n<li>La d\u00e9cision qui est rendue peut se traduire par une reclassification \u00e0 la hausse, la confirmation ou la reclassification \u00e0 la baisse du poste qui fait l'objet du grief.</li>\n</ul>\n\n<p>Pour plus de renseignements sur les griefs de classification et la proc\u00e9dure, veuillez consulter la <a href=\"http://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=28698\">Directive sur les griefs de classification</a> du Secr\u00e9tariat du Conseil du Tr\u00e9sor du Canada.</p>\n\n<h2>Contactez-nous </h2>\n\n<p>Des questions? Nous pouvons vous aider. Contactez un membre de l'\u00e9quipe de classification (voir la liste <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=100084657&amp;lang=fra\" target=\"_blank\">conseiller en classification (Word)</a>) ou envoyez un courriel \u00e0 <a href=\"mailto:aafc.classification.aac@canada.ca\">aafc.classification.aac@canada.ca</a> ou \u00e0 l\u2019unit\u00e9 d'examen des griefs de classification \u00e0 l'adresse\u00a0: <a href=\"mailto:aafc.corporateclassification-classificationministerielle.aac@canada.ca\">aafc.corporateclassification-classificationministerielle.aac@canada.ca</a>.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Classification Grievance Procedures",
        "fr": "Proc\u00e9dure de r\u00e8glement des griefs de classification"
    }
}