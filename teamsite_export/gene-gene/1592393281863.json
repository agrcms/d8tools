{
    "dcr_id": "1592393281863",
    "lang": "en",
    "title": {
        "en": "Pre-retirement transition leave (AAFC fact sheet)",
        "fr": "Cong\u00e9 de transition \u00e0 la retraite (fiche d'information d'AAC)"
    },
    "modified": "2020-06-17 00:00:00.0",
    "issued": "2020-06-17 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279035583756",
    "layout_name": "1 column",
    "dc_date_created": "2020-06-17",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2020-06-17",
            "fr": "2020-06-17"
        },
        "modified": {
            "en": "2020-06-17",
            "fr": "2020-06-17"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Pre-retirement transition leave (AAFC fact sheet)",
            "fr": "Cong\u00e9 de transition \u00e0 la retraite (fiche d'information d'AAC)"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<h2>What it is and how to get it</h2>\n\n<p>Pre-retirement transition leave is a special working arrangement that allows employees to reduce their work week by up to 40% during the two years prior to their retirement. During the arrangement, an employee\u2019s pay is adjusted to reflect the shorter work week, but pension and benefits coverage, as well as premiums and contributions, continue at pre-leave levels.</p>\n\n<h2>What you need to know</h2>\n\n<p>Employees on pre-retirement transition leave maintain their employment status, and continue to fall under the provisions of the relevant collective agreement or terms and conditions of employment.</p>\n\n<p>Before approving the leave, Section 34 managers must confirm that:</p>\n\n<ul>\n\t<li>the employee meets the terms and conditions of leave, and</li>\n\t<li>the work arrangement is operationally feasible for the two-year period\u2014in other words, the quality of service or costs associated with service delivery would not be adversely affected.</li>\n</ul>\n\n<h2>Conditions of leave</h2>\n\n<p>To be eligible, employees must:</p>\n\n<ul>\n\t<li>Be appointed to the core public administration with indeterminate employment status</li>\n\t<li>Not be surplus at the start of the arrangement</li>\n\t<li>Be eligible for an unreduced pension at the start or within two years of the arrangement</li>\n\t<li>Agree not to work for the federal public service while on leave without pay</li>\n\t<li>Agree to respect the <cite><a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=25049\">Values and Ethics Code for the Public Sector</a></cite> while on leave without pay</li>\n\t<li>Agree to resign at the end of the leave arrangement</li>\n</ul>\n\t\n<p>Note that the employer\u2019s acceptance of resignation is conditional until the leave arrangement is completed.  Once the application has been signed by the employee and the Section 34 manager, any changes to the arrangement may only be made in rare and unforeseen circumstances.</p>\n\n<p>Refer to <a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=15774#appC\">Appendix C of the Directive on Leave and Special Working Arrangements</a> for more information on terms and conditions of pre-retirement transition leave.</p>\n\n<h2>Applying for pre-retirement leave: employees</h2>\n\n<ol>\n\t<li>Consult the Directive to verify that you meet all the required terms and condition, and fully understand the impacts on your pay and benefits</li>\n\t<li>After speaking with your manager, submit an <a href=\"http://publiservice.tbs-sct.gc.ca/tbsf-fsct/325-9-eng.asp\">application for pre-retirement transition leave</a></li>\n</ol>\n  \n<h2>Responsibilities for Section 34 managers</h2>\n\n<ol>\n\t<li>Verify that the employee meets all of the eligibility criteria</li>\n\t<li>Assess the impact of the special working arrangement on operations</li>\n\t<li>If leave is approved, send the signed application and a <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/demande-employee-request-eng.html#a1\">Pay Action Request</a> to the AAFC HR Trusted Source by email at <a href=\"mailto:aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca\">aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca</a> for processing by the <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/cn-cu-eng.html\">Pay Centre</a>.</li>\n</ol>\n\n<h2>Resources</h2>\n\n<ul>\n\t<li><a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-information-pay/vie-life/vie-conge-life-leave/conge-preret-holiday-eng.html\">Pre-retirement Transition Leave (PSPC)</a></li>\n</ul>\n\t\t",
        "fr": "\n\t\t\t  \n\n<h2>De quoi s'agit-il?</h2>\n\n<p>Un cong\u00e9 de transition \u00e0 la retraite est une modalit\u00e9 de travail sp\u00e9ciale qui permet aux employ\u00e9s qui sont \u00e0 moins de deux ans de la retraite de r\u00e9duire leur semaine de travail de jusqu'\u00e0 40\u00a0%. Pendant la p\u00e9riode vis\u00e9e, le salaire de l'employ\u00e9 est rajust\u00e9 pour correspondre \u00e0 la semaine de travail r\u00e9duite, mais son niveau de participation aux r\u00e9gimes de pension et d'avantages sociaux (y compris les cotisations payables) demeure inchang\u00e9.</p>\n\n<h2>Ce que vous devez savoir</h2>\n\n<p>Les employ\u00e9s en cong\u00e9 de transition \u00e0 la retraite conservent leur situation d'emploi et continuent d'\u00eatre assujettis aux dispositions de leur convention collective et des conditions d'emploi applicables.</p>\n\n<p>Avant d'approuver le cong\u00e9, le gestionnaire disposant du pouvoir d\u00e9l\u00e9gu\u00e9 en vertu de l'article 34 doit confirmer que\u00a0:</p>\n\n<ul>\n\t<li>l'employ\u00e9 satisfait aux conditions li\u00e9es au cong\u00e9,</li>\n\t<li>la modalit\u00e9 de travail est faisable sur le plan op\u00e9rationnel pour la dur\u00e9e du cong\u00e9 (autrement dit, elle n'aura pas d'incidence sur la qualit\u00e9 des services ni sur le co\u00fbt de la prestation).</li>\n</ul>\n\n<h2>Conditions li\u00e9es au cong\u00e9</h2>\n\n<p>Pour \u00eatre admissible, l'employ\u00e9 doit\u00a0:</p>\n\n<ul>\n\t<li>\u00eatre nomm\u00e9 \u00e0 l'administration publique centrale pour une p\u00e9riode ind\u00e9termin\u00e9e,</li>\n\t<li>ne pas \u00eatre qualifi\u00e9 d'exc\u00e9dentaire au moment o\u00f9 la modalit\u00e9 entre en vigueur,</li>\n\t<li>\u00eatre admissible \u00e0 une pension non r\u00e9duite \u00e0 la date d'entr\u00e9e en vigueur de la modalit\u00e9 de travail ou le deviendra au cours des deux ann\u00e9es suivantes,</li>\n\t<li>accepter de ne pas travailler pour la fonction publique f\u00e9d\u00e9rale pendant le cong\u00e9 non pay\u00e9,</li>\n\t<li>accepter de se conformer aux dispositions du <cite><a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=25049\">Code de valeurs et d'\u00e9thique du secteur public</a></cite> pendant le cong\u00e9 non pay\u00e9,</li>\n\t<li>accepter de d\u00e9missionner \u00e0 la fin de la p\u00e9riode du cong\u00e9.</li>\n</ul>\n\n<p>Il est \u00e0 noter que l'acceptation de la d\u00e9mission par l'employeur est conditionnelle jusqu'\u00e0 la fin de la p\u00e9riode vis\u00e9e par la modalit\u00e9 de travail. Une fois que la demande a \u00e9t\u00e9 sign\u00e9e par l'employ\u00e9 et le gestionnaire disposant du pouvoir d\u00e9l\u00e9gu\u00e9 en vertu de l'article 34, la modalit\u00e9 ne sera modifi\u00e9e qu'en cas de circonstances rares et impr\u00e9vues.</p>\n\n<p>Consultez l'<a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=15774#appC\">annexe C de la Directive sur les cong\u00e9s et les modalit\u00e9s de travail sp\u00e9ciales</a> pour en savoir plus sur les conditions li\u00e9es au cong\u00e9 de transition \u00e0 la retraite.</p>\n\n<h2>Demander un cong\u00e9 de transition \u00e0 la retraite</h2>\n\n<ol>\n\t<li>Consultez la Directive pour d\u00e9terminer si vous satisfaites \u00e0 toutes les conditions et vous assurer de bien comprendre l'incidence sur votre salaire et vos avantages sociaux.</li>\n\t<li>Apr\u00e8s en avoir discut\u00e9 avec votre gestionnaire, pr\u00e9sentez une <a href=\"https://publiservice.tbs-sct.gc.ca/tbsf-fsct/325-9-fra.asp\">demande de cong\u00e9 de transition \u00e0 la retraite</a></li>\n</ol>\n \n<h2>Responsabilit\u00e9s du gestionnaire disposant du pouvoir d\u00e9l\u00e9gu\u00e9 en vertu de l'article 34</h2>\n\n<ol>\n\t<li>V\u00e9rifier que l'employ\u00e9 respecte tous les crit\u00e8res d'admissibilit\u00e9.</li>\n\t<li>\u00c9valuer l'incidence de la modalit\u00e9 de travail sp\u00e9ciale sur les op\u00e9rations.</li>\n\t<li>Si le cong\u00e9 est approuv\u00e9, envoyer la demande sign\u00e9e et une <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/demande-employee-request-fra.html\">demande d'intervention de paye</a> \u00e0 la source fiable des RH d'AAC par courriel \u00e0 l'adresse <a href=\"mailto:aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca\">aafc.hrtrustedsource-sourcefiablerh.aac@canada.ca</a> \u00e0 des fins de traitement par le <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/cn-cu-fra.html\">Centre de paye</a>.</li>\n</ol>\n\n<h2>Ressources</h2>\n\n<ul>\n\t<li><a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-information-pay/vie-life/vie-conge-life-leave/conge-preret-holiday-fra.html\">Cong\u00e9 de transition \u00e0 la retraite (SPAC)</a></li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Pre-retirement transition leave (AAFC fact sheet)",
        "fr": "Cong\u00e9 de transition \u00e0 la retraite (fiche d'information d'AAC)"
    }
}