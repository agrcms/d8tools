{
    "dcr_id": "1405953647313",
    "lang": "en",
    "title": {
        "en": "Political Activities",
        "fr": "Activit\u00e9s politiques"
    },
    "modified": "2018-12-11 00:00:00.0",
    "issued": "2014-07-22 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279035911647",
    "layout_name": "1 column",
    "dc_date_created": "2014-07-21",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2014-07-22",
            "fr": "2014-07-22"
        },
        "modified": {
            "en": "2018-12-11",
            "fr": "2018-12-11"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Political Activities",
            "fr": "Activit\u00e9s politiques"
        },
        "subject": {
            "en": "communications;values and ethics",
            "fr": "communications;valeurs et \u00e9thique"
        },
        "description": {
            "en": "The Public Service Employment Act (PSEA) outlines your right to engage in any political activity at the federal, provincial, territorial or municipal level as long as you are able to perform your duties in an impartial manner.",
            "fr": "La Loi sur l'emploi dans la fonction publique (LEFP) reconna\u00eet le droit des fonctionnaires de participer \u00e0 des activit\u00e9s politiques d'ordre f\u00e9d\u00e9ral, provincial, territorial ou municipal, dans la mesure o\u00f9 ils peuvent ex\u00e9cuter leurs t\u00e2ches de fa\u00e7on impartiale."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Political Activity Self-Assessment Tool",
            "fr": "Outil d'auto\u00e9valuation sur les activit\u00e9s politiques"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>The <a href=\"http://laws-lois.justice.gc.ca/eng/acts/P-33.01/\"><cite>Public Service Employment Act</cite></a> (PSEA) outlines your right to engage in any political activity at the federal, provincial, territorial or municipal level as long as you are able to perform your duties in an impartial manner.</p>\n\n<h2>What is a political activity?</h2>\n \n<p>The PSEA defines a political activity as:</p>\n \n <ul>\n <li>any activity in support of, within or in opposition to a political party;</li>\n <li>any activity in support of or in opposition to a candidate before or during an election period; or</li>\n <li>seeking nomination as or being a candidate in an election before or during an election period.</li>\n </ul>\n \n <h2>What to do if you wish to engage in a political activity?</h2>\n \n<p>Complete the Public Service Commission (PSC) <a href=\"http://www2.psc-cfp.gc.ca/pat-oap/?lang=en\">Political Activity Self-Assessment Tool</a> if you are unsure whether a political activity you wish to engage in could impair or be perceived as impairing your ability to perform your duties in a politically impartial manner.</p>\n \n<h2>What to do if you wish to seek nomination or become a candidate?</h2>\n\n<div class=\"well\">\n\n <p>You <b>must </b>first obtain permission from the PSC if you are seeking nomination or becoming a candidate before or during an election period.</p>\n \n <p>The PSC requires <b>30 days</b> to render a decision on candidacy requests once it receives all required documentation (see the steps below).</p>\n</div>\n\n<ul class=\"list-unstyled mrgn-lft-lg\">\n <li class=\"mrgn-bttm-md\"><b>Step 1:</b> Contact the <a href=\"display-afficher.do?id=1288015767856&amp;lang=eng#contact\">Values and Ethics Policy Centre</a> (VEPC) to notify them of your intention to seek nomination or become a candidate.</li>\n \n <li class=\"mrgn-bttm-md\"><b>Step 2:</b> Complete all of the <a href=\"http://www.psc-cfp.gc.ca/plac-acpl/guides-eng.htm\">required forms</a> found on the PSC website.</li>\n \n <li class=\"mrgn-bttm-md\"><b>Step 3:</b> Contact the VEPC once you have completed the forms; the VEPC will advise you on the next steps to seek management input (note: management input varies based on the type of candidacy request).</li>\n \n <li class=\"mrgn-bttm-md\"><b>Step 4:</b> Your management's office submits the request to the VEPC.</li>\n \n <li><b>Step 5:</b> The VEPC submits the request to the PSC.</li>\n</ul>\n\n<p><strong>Please note:</strong></p>\n\n <ul>\n  <li>The PSC will only grant permission under the condition that being a candidate will not impair, or be perceived as impairing, your ability to perform your duties in an impartial manner.</li>\n\n  <li>Only <b>after</b> the PSC has granted you permission, you can go public with your candidacy and undertake related activities.</li>\n\n  <li>For provincial, territorial or federal elections, you need to request and obtain from the PSC Leave Without Pay (LWOP) for the election period. In the event that you are elected you will <b>cease to be an employee</b> of the public service on the day that you are elected.</li>\n\n  <li>For municipal elections, the PSC may grant you permission contingent that you take LWOP for the period, or any part of the period where you are seeking nomination, or are a candidate, before and during the election period. As well, the PSC may make permission conditional on you taking LWOP.</li>\n </ul>\n \n <h2>Additional Information</h2>\n \n <ul>\n <li><a href=\"http://www.psc-cfp.gc.ca/plac-acpl/index-eng.htm\">Public Service Commission's - Political Activities</a></li>\n <li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/hrb_dgrh/political_activities_and_you_activites_politiques_et_vous-eng.pdf\">Political Activities and You Pamphlet (PDF)</a></li>\n  <li><a href=\"http://www.psc-cfp.gc.ca/plac-acpl/leave-conge/index-eng.htm\">Political Candidacy Guidelines</a></li>\n </ul>\n\n<h2>Contact Us</h2>\n\n<p>For advice and guidance contact the <a href=\"display-afficher.do?id=1288015767856&amp;lang=eng#contact\">Values and Ethics Policy Centre</a> or Agriculture and Agri-Food Canada's Departmental Designated Political Activities Representatives (DPAR) by email at <a href=\"mailto:aafc.politicalactivities-activitespolitiques.aac@canada.ca\">aafc.politicalactivities-activitespolitiques.aac@canada.ca</a> or by phone at 1-866-894-5464 (toll-free).</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>La <a href=\"http://laws-lois.justice.gc.ca/fra/lois/P-33.01/\"><cite>Loi sur l'emploi dans la fonction publique</cite></a> (LEFP) reconna\u00eet le droit des fonctionnaires de participer \u00e0 des activit\u00e9s politiques d'ordre f\u00e9d\u00e9ral, provincial, territorial ou municipal, dans la mesure o\u00f9 ils peuvent ex\u00e9cuter leurs t\u00e2ches de fa\u00e7on impartiale.</p>\n\n <h2>Qu'est-ce qu'une activit\u00e9 politique?</h2>\n \n<p>La LEFP d\u00e9finit une activit\u00e9 politique de la fa\u00e7on suivante\u00a0:</p>\n \n <ul>\n <li>toute activit\u00e9 exerc\u00e9e au sein d'un parti politique, ou exerc\u00e9e pour soutenir un tel parti ou s'y opposer;</li>\n <li>toute activit\u00e9 exerc\u00e9e pour soutenir un candidat avant ou pendant la p\u00e9riode \u00e9lectorale ou pour s'y opposer;</li>\n <li>le fait d'\u00eatre candidat \u00e0 une \u00e9lection ou de tenter de le devenir, avant ou pendant la p\u00e9riode \u00e9lectorale.</li>\n </ul>\n \n <h2>Quoi faire si vous souhaitez participer \u00e0 une activit\u00e9 politique?</h2>\n \n<p>Dans le cas o\u00f9 vous n'\u00eates pas certain si une activit\u00e9 politique \u00e0 laquelle vous souhaitez participer peut porter ou semble porter atteinte \u00e0 votre capacit\u00e9 d'exercer vos fonctions de fa\u00e7on politiquement impartiale, remplissez <a href=\"http://www2.psc-cfp.gc.ca/pat-oap/?lang=fr\">l'Outil d'auto\u00e9valuation sur les activit\u00e9s politiques</a> de la Commission de la fonction publique (CFP).</p>\n \n<h2>Quoi faire si vous souhaitez pr\u00e9senter votre candidature ou devenir candidat?</h2>\n\n<div class=\"well\">\n <p>Vous <b>devez </b>d'abord obtenir l'autorisation de la CFP pour pr\u00e9senter votre candidature ou devenir candidat avant ou pendant la p\u00e9riode \u00e9lectorale.</p>\n \n <p>Apr\u00e8s r\u00e9ception des documents n\u00e9cessaires (voir les \u00e9tapes ci-dessous), <b>30\u00a0jours</b> sont n\u00e9cessaires \u00e0 la CFP pour rendre une d\u00e9cision sur les demandes de candidature.</p>\n</div>\n\n<ul class=\"list-unstyled mrgn-lft-lg\">\n <li class=\"mrgn-bttm-md\"><b>\u00c9tape\u00a01\u00a0:</b> Contactez le <a href=\"display-afficher.do?id=1288015767856&amp;lang=fra#communiquez\">Centre de la politique sur les valeurs et l'\u00e9thique</a> (CPVE) pour l'informer de votre intention de pr\u00e9senter votre candidature ou de devenir candidat.</li>\n \n <li class=\"mrgn-bttm-md\"><b>\u00c9tape\u00a02\u00a0:</b> Remplissez tous les <a href=\"http://www.psc-cfp.gc.ca/plac-acpl/guides-fra.htm\">formulaires requis</a> qui se trouvent sur le site Web de la CFP.</li>\n \n <li class=\"mrgn-bttm-md\"><b>\u00c9tape\u00a03\u00a0:</b> Communiquez avec le CPVE une fois que vous avez rempli les formulaires; le CPVE vous informera des prochaines \u00e9tapes \u00e0 suivre pour obtenir la participation de votre gestionnaire (remarque\u00a0: la participation de votre gestionnaire d\u00e9pend du type de demande relative \u00e0 la candidature).</li>\n \n <li class=\"mrgn-bttm-md\"><b>\u00c9tape\u00a04\u00a0:</b> Le bureau de votre gestionnaire pr\u00e9sente la demande au CPVE.</li>\n \n <li><b>\u00c9tape\u00a05\u00a0:</b> Le CPVE pr\u00e9sente la demande \u00e0 la CFP.<br><br></li>\n</ul>\n\n<p><strong>Veuillez prendre note des points suivants\u00a0:</strong></p>\n\n <ul>\n <li>La CFP acceptera la demande \u00e0 la condition que votre candidature ne porte pas atteinte \u00e0 votre capacit\u00e9 d'exercer vos fonctions de fa\u00e7on impartiale ou ne semble pas y porter atteinte.</li>\n\n <li>Vous ne pourrez annoncer publiquement votre intention de vous porter candidat et entreprendre les activit\u00e9s connexes qu'<b>apr\u00e8s</b> avoir obtenu la permission de la CFP.</li>\n\n <li>Pour toute candidature d'ordre provincial, territorial ou f\u00e9d\u00e9ral, vous devez demander et obtenir de la CFP un cong\u00e9 non pay\u00e9 (CNP) pour la p\u00e9riode \u00e9lectorale. Si vous \u00eates \u00e9lu, vous <b>cesserez d'\u00eatre un employ\u00e9</b> de la fonction publique le jour de votre \u00e9lection.</li>\n\n <li>Pour des \u00e9lections municipales, la CFP peut vous accorder une permission si vous prenez un CNP pour la p\u00e9riode ou toute partie de la p\u00e9riode durant laquelle vous tentez d'\u00eatre candidat ou \u00eates candidat, avant et apr\u00e8s la p\u00e9riode d'\u00e9lection. La CFP peut \u00e9galement rendre la permission conditionnelle au fait que vous preniez un CNP.</li>\n </ul>\n \n <h2>Renseignements suppl\u00e9mentaires</h2>\n \n <ul>\n <li><a href=\"http://www.psc-cfp.gc.ca/plac-acpl/index-fra.htm\">Commission de la fonction publique - Activit\u00e9s politiques</a></li>\n <li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/hrb_dgrh/political_activities_and_you_activites_politiques_et_vous-fra.pdf\">Les activit\u00e9s politiques et vous (PDF)</a></li>\n <li><a href=\"http://www.psc-cfp.gc.ca/plac-acpl/leave-conge/index-fra.htm\">Candidature \u00e0 une \u00e9lection</a></li>\n </ul>\n\n<h2>Communiquez avec nous</h2>\n\n<p>Pour obtenir des conseils ou des directives, communiquez avec le <a href=\"display-afficher.do?id=1288015767856&amp;lang=fra#communiquez\">Centre de la politique sur les valeurs et l'\u00e9thique</a> ou avec un repr\u00e9sentant d\u00e9sign\u00e9 en mati\u00e8re d'activit\u00e9s politiques (RDMAP) d'Agriculture et Agroalimentaire Canada par courriel \u00e0 <a href=\"mailto:aafc.politicalactivities-activitespolitiques.aac@canada.ca\">aafc.politicalactivities-activitespolitiques.aac@canada.ca</a> ou au num\u00e9ro sans frais suivant\u00a0: 1-866-894-5464.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Political Activities",
        "fr": "Activit\u00e9s politiques"
    }
}