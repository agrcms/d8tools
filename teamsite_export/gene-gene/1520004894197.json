{
    "dcr_id": "1520004894197",
    "lang": "en",
    "title": {
        "en": "Priority payments",
        "fr": "Paiements prioritaires"
    },
    "modified": "2019-10-17 00:00:00.0",
    "issued": "2018-03-05 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1279035583756",
    "layout_name": "1 column",
    "dc_date_created": "2018-03-02",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-03-05",
            "fr": "2018-03-05"
        },
        "modified": {
            "en": "2019-10-17",
            "fr": "2019-10-17"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Priority payments",
            "fr": "Paiements prioritaires"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>A priority payment is a temporary funding arrangement that is intended to address a shortfall in monies owed to an employee. The amount is repaid once certain conditions have been met, and has no impact on an employee\u2019s taxable earnings. Learn about <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/systeme-paye-employes-pay-system-employees/trop-payes-overpayments-eng.html#a4\">repayment options</a>.</p>\n\n<table class=\"table table-bordered\">\n<tr>\n\t<th>Eligibility</th>\n\t<td><p>Employees may request a priority payment at any time if they: </p>\n\n<ul>\n<li>did not receive a regular pay</li>\n<li>received an inaccurate or incomplete pay</li>\n</ul></td>\n</tr>\n\t\n<tr>\n\t<th>How it works</th>\n\t<td>Priority payments are calculated at up to 66% of an employee\u2019s gross earnings to approximate net pay. They are usually processed by direct deposit within 2-3 business days following a request. In certain circumstances, AAFC can issue a hard copy cheque to address more immediate needs.</td>\n</tr>\n\t\n<tr>\n\t<th>Steps to request a priority payment</th>\n\t<td><ol>\n<li>Inform your manager of your pay issue and intention to request a priority payment.</li>\n<li>Contact the AAFC Pay team by email at <a href=\"mailto:aafc.paytransfer-transfertpaye.aac@canada.ca\">aafc.paytransfer-transfertpaye.aac@canada.ca</a>.  A Pay Liaison Officer will contact you to review your pay issue and action the request.</li>\n</ol></td>\n</tr>\n\t\n<tr>\n\t<th>Recovery of priority payments</th>\n\t<td><p>Unless employees choose to pay the amounts owing right away, recoveries of these amounts will <strong>only</strong> start when:</p>\n\n<ul>\n<li>all monies owed to the employee have been paid</li>\n<li>the employee has received 3 consecutive correct pays</li>\n<li>a recovery agreement has been established</li>\n</ul>\n\n<p>Learn more about the <a href=\"http://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/systeme-paye-employes-pay-system-employees/recuperation-flexible-qa-flexible-recovery-eng.html\">Recovery of overpayments, emergency salary advances and priority payments</a>.</p></td>\n</tr>\n</table>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Un paiement prioritaire est une entente de financement temporaire qui vise \u00e0 combler une somme qui est due \u00e0 un employ\u00e9. Le montant est rembours\u00e9 lorsque certaines conditions ont \u00e9t\u00e9 respect\u00e9es, et n\u2019a aucune incidence sur le revenu imposable de l\u2019employ\u00e9. Renseignez-vous sur les <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/systeme-paye-employes-pay-system-employees/trop-payes-overpayments-fra.html#a4\">options de remboursement</a>.</p>\n\n<table class=\"table table-bordered\">\n<tr>\n\t<th>Admissibilit\u00e9</th>\n\t<td><p>L\u2019employ\u00e9 peut demander un paiement prioritaire en tout temps s\u2019il :</p>\n\n<ul>\n<li>n\u2019a pas re\u00e7u de paye normale;</li>\n<li>a re\u00e7u une paye inexacte ou incompl\u00e8te.</li>\n</ul>\n</td>\n</tr>\n\t\n<tr>\n\t<th>Fonctionnement</th>\n\t<td>Les paiements prioritaires sont calcul\u00e9s jusqu\u2019\u00e0 concurrence de 66\u00a0% de la r\u00e9mun\u00e9ration brute de l\u2019employ\u00e9 pour correspondre approximativement \u00e0 sa r\u00e9mun\u00e9ration nette. Elles sont habituellement trait\u00e9es par d\u00e9p\u00f4t direct dans les 2 \u00e0 3 jours ouvrables suivant une demande. Dans certaines circonstances, AAC peut \u00e9mettre un ch\u00e8que sur papier pour r\u00e9pondre \u00e0 des besoins plus imm\u00e9diats.</td>\n</tr>\n\t\n<tr>\n\t<th>\u00c9tapes pour demander un paiement prioritaire</th>\n\t<td><ol>\n<li>Informez votre gestionnaire de votre probl\u00e8me de paye et de votre intention de demander un paiement prioritaire.</li>\n<li>Communiquez avec l\u2019\u00e9quipe de la paye d\u2019AAC, par courriel, \u00e0 <a href=\"mailto:aafc.paytransfer-transfertpaye.aac@canada.ca\">aafc.paytransfer-transfertpaye.aac@canada.ca</a>.  Un agent de liaison de la paye communiquera avec vous pour examiner votre probl\u00e8me de paye et donner suite \u00e0 votre demande.</li>\n</ol></td>\n</tr>\n\t\n<tr>\n\t<th>Recouvrement des paiements prioritaires</th>\n\t<td><p>\u00c0 moins que les employ\u00e9s ne choisissent de payer imm\u00e9diatement les montants qui lui sont dus, le recouvrement de ces montants commencera <strong>seulement</strong> lorsque\u00a0:</p>\n\n<ul>\n<li>toutes les sommes dues \u00e0 l\u2019employ\u00e9 auront \u00e9t\u00e9 pay\u00e9es;</li>\n<li>l\u2019employ\u00e9 aura re\u00e7u 3 paiements corrects cons\u00e9cutifs;</li>\n<li>un accord de recouvrement aura \u00e9t\u00e9 conclu.</li>\n</ul>\n\n<p>D\u00e9couvrez les <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/systeme-paye-employes-pay-system-employees/recuperation-flexible-qa-flexible-recovery-fra.html\">Options flexibles de recouvrement de trop-pay\u00e9s, d\u2019avances de salaire d\u2019urgence et de paiements prioritaires</a>.</p></td>\n</tr>\n</table>\n\t\t"
    },
    "breadcrumb": {
        "en": "Priority payments",
        "fr": "Paiements prioritaires"
    }
}