{
    "dcr_id": "1328882423004",
    "lang": "en",
    "title": {
        "en": "Information Management",
        "fr": "Gestion de l'information"
    },
    "modified": "2020-09-11 00:00:00.0",
    "issued": "2012-02-14 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1333380344003",
    "layout_name": "1 column",
    "dc_date_created": "2012-02-10",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2012-02-14",
            "fr": "2012-02-14"
        },
        "modified": {
            "en": "2020-09-11",
            "fr": "2020-09-11"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Information Management",
            "fr": "Gestion de l'information"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "You rely on information every day in your work. We all do. In every area and at every level, information is the fuel that drives our programs and services, the power that supports our decisions and actions, and the route to quality client service and program delivery.",
            "fr": "Tous ont besoin d'information pour ex\u00e9cuter leur travail de tous les jours. Dans chaque secteur et \u00e0 chaque niveau d'AAC, l'information est le carburant de nos programmes et nos services, l'assise de nos d\u00e9cisions et interventions et un outil indispensable \u00e0 la prestation de services de qualit\u00e9 \u00e0 la client\u00e8le et \u00e0 l'ex\u00e9cution des programmes."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "information management, IM",
            "fr": "gestion de l'information, GI"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<section>\n\t\t\t  \n\n<div class=\"row\">\n\n<div class=\"col-md-4 pull-right\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Contact us</h2>\n</header>\n<div class=\"panel-body\">\n<p>Information Management Team<br>\n<a href=\"mailto:aafc.im-gi.aac@canada.ca\">aafc.im-gi.aac@canada.ca</a></p>\n</div>\n</section>\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Mandatory information management (IM) training</h2>\n</header>\n<div class=\"panel-body\">\n\n<p><a href=\"https://learn-apprendre.csps-efpc.gc.ca/application/en/content/fundamentals-information-management-i301\">I301 - Fundamentals of Information Management (GCcampus)</a></p>\n\n<p><b>Time required:</b> 1 hour (approximate)<br>\n<b>Location:</b> Online / at your computer<br>\n<b>Requirements:</b> Access to <a href=\"https://learn-apprendre.csps-efpc.gc.ca/\">GCcampus</a></p>\n\n<p><strong>Note:</strong> Before starting the course, check that your GCcampus Profile lists your organization as \"Department of Agriculture and Agri-Food\".</p>\n</div>\n</section>\n\n<section class=\"alert alert-info\">\n<p><strong>Important:</strong> Information about the Canada School of Public Service courses, programs and events is available <strong>only</strong> on <a href=\"https://learn-apprendre.csps-efpc.gc.ca/\">GCcampus</a>. <b>To view these products, you will need to log in with your MyAccount username and password.</b> Follow the directions on the login page to set up your MyAccount login credentials. If you need assistance, please consult the Canada School of Public Service <a href=\"http://www.csps-efpc.gc.ca/Contact_Us/index-eng.aspx\">Contact Us page</a> or send an email to <a href=\"mailto:CSPS.internet.EFPC@csps-efpc.gc.ca\">CSPS.internet.EFPC@csps-efpc.gc.ca</a>.</p>\n</section>\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Other IM-related training</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li><a href=\"display-afficher.do?id=1326124885316&amp;lang=eng\">AgriDOC (AGR-458)</a></li>\n<li><a href=\"display-afficher.do?id=1341950176975&amp;lang=eng\">TRECS (AGR-457)</a></li>\n<li>Visit the <a href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/IM%20Repositories%20-%20R%C3%A9f%C3%A9rentiels%20de%20GI.aspx\">IM\u00a0@\u00a0AAFC Knowledge Workspace</a> site for links to all available IM-related training.</li>\n</ul>\n</div>\n</section>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n<p>You rely on information every day in your work. We all do. In every area and at every level, information is the fuel that drives our programs and services, the power that supports our decisions and actions, and the route to quality client service and program delivery.</p>\n\n<p>To get the most out of information, to keep it credible and secure and to be able to share it with others we need the tools and know-how to manage it wisely, effectively and in step with departmental and government-wide information law and policy.</p>\n\n<p>Visit the <a href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/IM%20Repositories%20-%20R%C3%A9f%C3%A9rentiels%20de%20GI.aspx\">IM\u00a0@\u00a0AAFC Knowledge Workspace</a> site for more detailed support and guidance.</p>\n\n\n<p><b>On this page:</b></p>\n\n<nav>\n<ul>\n<li><a href=\"#features\">Features</a></li>\n<li><a href=\"#tips\">Tips and tricks</a></li>\n<li><a href=\"#policies\">Policies and guidelines </a></li>\n</ul>\n</nav>\n\n<h2 id=\"features\">Features</h2>\n\n<ul>\n      <li>organize information so that what you need is accessible quickly and easily;</li>\n      <li>differentiate between information that must be retained and information that must be disposed of when no longer needed;</li>\n      <li>reduce clutter by disposing of transitory or redundant information;</li>\n      <li>reduce the level of effort by minimizing duplication of work; and</li>\n      <li>make informed decisions based on up-to-date, reliable information.</li>\n</ul>\n\n<h2 id=\"tips\">Tips and tricks</h2>\n\n<ul>\n\n  <li>\n    <a href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/IRBV%20and%20Transitory%20Information.aspx\">Business Value versus Transitory Information</a></li>\n  <li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/_layouts/15/DocIdRedir.aspx?ID=AGR-8874057\">Managing e-mail (Word)</a></li>\n  <li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/_layouts/15/DocIdRedir.aspx?ID=AGR-7882957\">File Naming Conventions (PPT)</a></li>\n  <li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/Records%20Assessment%20and%20Disposition.aspx\">Records Retention and Disposition Processes</a></li>\n</ul>\n\n\n<h2 id=\"policies\">Policies and guidelines</h2>\n\n<ul>\n\n<li><a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=32603\">Policy on Service and Digital (TBS)</a></li>\n\n<li><a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=32601\">Directive on Service and Digital (TBS)</a></li>\n\n<li><a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=16557\">Guideline for Employees of the Government of Canada: Information Management (IM) Basics (TBS)</a></li>\n\n  <li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/_layouts/15/DocIdRedir.aspx?ID=AGR-8787293\">Information Management Exit Directive (Word)</a></li>\n\n  <li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/Documents/AAFC_Information_Management_Strategy_2016-2021.docx\">AAFC Information Management Strategy 2016-2021 (Word)</a></li>\n\n<li><a href=\"https://intranet.agr.gc.ca/resources/prod/doc/information_management/guide_to_handling_sensitive_information-eng.doc\">Guide to classifying and handling of sensitive information (Word)</a></li>\n\n</ul>\n\n<p>More <a href=\"display-afficher.do?id=1287431560148&amp;lang=eng\">Policies and Guidelines (Information Systems Branch)</a> </p>\n\n</div></div>\n\t\t</section>",
        "fr": "<section>\n\t\t\t  \n\n<div class=\"row\">\n\n<div class=\"col-md-4 pull-right\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Communiquez avec nous</h2>\n</header>\n<div class=\"panel-body\">\n<p>\u00c9quipe de la gestion de l'information<br>\n<a href=\"mailto:aafc.im-gi.aac@canada.ca\">aafc.im-gi.aac@canada.ca</a></p>\n</div>\n</section>\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Formation obligatoire sur la gestion de l'information</h2>\n</header>\n<div class=\"panel-body\">\n\n<p><a href=\"https://learn-apprendre.csps-efpc.gc.ca/application/fr/content/principes-fondamentaux-de-la-gestion-de-linformation-i301\">I301 - Principes fondamentaux de la gestion de l'information (GCcampus)</a></p>\n\n<p><b>Temps requis\u00a0:</b> 1 heure environ<br>\n<b>Lieu\u00a0:</b> Formation en ligne faite \u00e0 votre poste de travail<br>\n<b>Exigences\u00a0:</b> Acc\u00e8s \u00e0 <a href=\"https://learn-apprendre.csps-efpc.gc.ca/\">GCcampus</a></p>\n\n<p><b>Remarque :</b> Avant de suivre ce cours, confirmez que votre profil dans GCcampus indique le \u00ab\u00a0Minist\u00e8re de l'Agriculture et de l'Agroalimentaire\u00a0\u00bb comme \u00e9tant votre organisation.</p>\n</div>\n</section>\n\n<section class=\"alert alert-info\">\n<p><strong>Important:</strong> L'information sur les cours, les programmes et les activit\u00e9s de l'\u00c9cole de la fonction publique du Canada se trouvent <strong>seulement</strong> sur <a href=\"https://learn-apprendre.csps-efpc.gc.ca/\">GCcampus</a>. <b>Pour consulter ces produits, vous devez ouvrir une session avec votre nom d'utilisateur et votre mot de passe MonDossier.</b> Suivre les directives sur la page d'ouverture de session pour r\u00e9gler vos donn\u00e9es de connexion pour MonDossier. Si vous avez besoin d'aide, veuillez consulter la page <a href=\"http://www.csps-efpc.gc.ca/Contact_Us/index-fra.aspx\">Contactez-nous</a> de l'\u00c9cole de la fonction publique du Canada ou envoyez un courriel \u00e0 l'adresse <a href=\"mailto:CSPS.internet.EFPC@csps-efpc.gc.ca\">CSPS.internet.EFPC@csps-efpc.gc.ca</a>.</p>\n</section>\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Autres formations sur la gestion de l'information</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li><a href=\"display-afficher.do?id=1326124885316&amp;lang=fra\">AgriDOC (AGR-458)</a></li>\n<li><a href=\"display-afficher.do?id=1341950176975&amp;lang=fra\">SSRCHD (AGR-457)</a></li>\n\n<li>Visitez le site <a href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/IM%20Repositories%20-%20R%C3%A9f%C3%A9rentiels%20de%20GI.aspx\">GI\u00a0@\u00a0AAC sur l\u2019Espace de travail</a> du savoir pour obtenir des liens vers toutes les formations disponibles en mati\u00e8re de GI.</li>\n</ul>\n</div>\n</section>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n\n<p>Tous ont besoin d'information pour ex\u00e9cuter leur travail de tous les jours. Dans chaque secteur et \u00e0 chaque niveau d'Agriculture et Agroalimentaire Canada (AAC), l'information est le carburant de nos programmes et nos services, l'assise de nos d\u00e9cisions et interventions et un outil indispensable \u00e0 la prestation de services de qualit\u00e9 \u00e0 la client\u00e8le et \u00e0 l'ex\u00e9cution des programmes.</p>\n\n<p>Pour pouvoir b\u00e9n\u00e9ficier pleinement de l'information, pour assurer sa cr\u00e9dibilit\u00e9 et sa s\u00e9curit\u00e9 et pour l'\u00e9changer avec d'autres, nous devons disposer des outils et du savoir-faire qui nous permettront de la g\u00e9rer avec sagesse et efficacit\u00e9, en conformit\u00e9 avec les lois et politiques tant du Minist\u00e8re que de l'ensemble de l'administration f\u00e9d\u00e9rale.</p>\n\n<p>Visit the <a href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/IM%20Repositories%20-%20R%C3%A9f%C3%A9rentiels%20de%20GI.aspx\">GI\u00a0@\u00a0AAC Knowledge Workspace</a> site for more detailed support and guidance.</p>\n\n<b>Sur cette page\u00a0:</b>\n\n<nav>\n<ul>\n<li><a href=\"#particularites\">Particularit\u00e9s</a></li>\n<li><a href=\"#conseils\">Conseils et astuces</a></li>\n<li><a href=\"#politiques\">Politiques et lignes directrices</a></li>\n</ul>\n</nav>\n\n<h2 id=\"particularites\">Particularit\u00e9s</h2>\n\n<ul>     \n  <li>organiser l'information de sorte que l'information que vous avez besoin soit accessible rapidement et facilement;</li>     \n  <li>distinguer entre l'information qui doit \u00eatre conserv\u00e9e et celle qui doit \u00eatre \u00e9limin\u00e9e lorsqu'elle n'est plus utile;</li>     \n  <li>diminuer l'encombrement en \u00e9liminant l'information \u00e9ph\u00e9m\u00e8re ou exc\u00e9dentaire;</li>     \n  <li>r\u00e9duire le degr\u00e9 d'efforts en diminuant le chevauchement des t\u00e2ches;</li>\n  <li>prendre des d\u00e9cisions \u00e9clair\u00e9es \u00e0 partir d'information \u00e0 jour et digne de foi.</li>\n</ul>\n\n<h2 id=\"conseils\">Conseils et astuces</h2>\n\n<ul>\n\n  <li>\n    <a href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/IRBV%20and%20Transitory%20Information.aspx\">Information \u00e0 valeur op\u00e9rationnelle versus \u00e9ph\u00e9m\u00e8re</a></li>\n  <li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/_layouts/15/DocIdRedir.aspx?ID=AGR-8874057\">La gestion des courriels (Word)</a></li>\n  <li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/_layouts/15/DocIdRedir.aspx?ID=AGR-8257306\">Convention de denomination (PPT)</a></li>\n  <li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/Records%20Assessment%20and%20Disposition.aspx\">Processus d'\u00e9valuation et d'\u00e9limination des documents</a></li>\n</ul>\n\n<h2 id=\"politiques\">Politiques et lignes directrices</h2>\n\n<ul>\n\n<li><a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=32603\">Politique sur les services et le num\u00e9rique (SCT)</a></li>\n\n<li><a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=32601\">Directive sur les services et le num\u00e9rique (SCT)</a></li>\n\n<li><a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=16557\">Ligne directrice \u00e0 l'intention des employ\u00e9s f\u00e9d\u00e9raux : Rudiments de la gestion de l'information (GI)(SCT)</a></li>\n\n  <li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/_layouts/15/DocIdRedir.aspx?ID=AGR-8787364\">Directive de d\u00e9part relative \u00e0 la GI (Word)</a></li>\n\n  <li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/Documents/Strat%C3%A9gie_de_la_GI__d_AAC_-_2016-2021.docx\">Strat\u00e9gie de la GI d'AAC 2016-2021 (Word)</a></li>\n\n<li><a href=\"https://collab.agr.gc.ca/co/ima_sgi/_layouts/15/DocIdRedir.aspx?ID=AGR-3122178\">Guide de la classification et de la manutention des renseignements d\u00e9licats (Word)</a></li>\n</ul>\n\n<p>Autres <a href=\"display-afficher.do?id=1287431560148&amp;lang=fra\">Politiques et lignes directrices (Direction g\u00e9n\u00e9rale des syst\u00e8mes d'information)</a></p>\n\n</div></div>\n\t\t</section>"
    },
    "breadcrumb": {
        "en": "Information Management",
        "fr": "Gestion de l'information"
    }
}