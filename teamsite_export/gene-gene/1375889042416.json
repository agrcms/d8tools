{
    "dcr_id": "1375889042416",
    "lang": "en",
    "title": {
        "en": "Defensive Security Brief - Travel",
        "fr": "S\u00e9ance d'information sur la s\u00e9curit\u00e9 d\u00e9fensive - Voyages"
    },
    "modified": "2020-08-10 00:00:00.0",
    "issued": "2013-08-12 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1484940874128",
    "layout_name": "1 column",
    "dc_date_created": "2013-08-07",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2013-08-12",
            "fr": "2013-08-12"
        },
        "modified": {
            "en": "2020-08-10",
            "fr": "2020-08-10"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Defensive Security Brief - Travel",
            "fr": "S\u00e9ance d'information sur la s\u00e9curit\u00e9 d\u00e9fensive - Voyages"
        },
        "subject": {
            "en": "security;travel",
            "fr": "s\u00e9curit\u00e9;voyage"
        },
        "description": {
            "en": "When travelling internationally as representatives of AAFC, it is essential that employees take part in a general Defensive Security Briefing session offered by Departmental Security Services (DSS). These briefings will ensure that travellers understand the safety and security environment of their destination, the local laws and customs, entry requirements, health conditions, and other important travel issues they may encounter on their trip.",
            "fr": "Lorsque des employ\u00e9s se d\u00e9placent \u00e0 l'\u00e9tranger \u00e0 titre de repr\u00e9sentants d'AAC, ils sont tenus de suivre une s\u00e9ance d'information g\u00e9n\u00e9rale sur la s\u00e9curit\u00e9 d\u00e9fensive. Cette s\u00e9ance offerte par les Services de s\u00e9curit\u00e9 minist\u00e9riels (SSM) permet de s'assurer que les voyageurs comprennent le contexte de la s\u00fbret\u00e9 et de la s\u00e9curit\u00e9 \u00e0 destination, les lois et les coutumes locales, les exigences en mati\u00e8re d'entr\u00e9e, les conditions sanitaires et d'autres aspects importants avec lesquels ils pourraient avoir \u00e0 composer pendant leur voyage."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Travelling Abroad, Defensive Security Briefing",
            "fr": "Voyagez-vous \u00e0 l'\u00e9tranger, s\u00e9ance d'information sur la s\u00e9curit\u00e9 d\u00e9fensive"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "resource list",
            "fr": "liste de r\u00e9f\u00e9rence"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>Travelling abroad?</p>\n\n<p>When travelling internationally as representatives of Agriculture and Agri-Food Canada (AAFC), it is essential that employees take  the <a href=\"?id=1325695839878\">AGR-620 Security Awareness course</a>. This mandatory course will ensure that employees understand the safety and security environment of their destination, the local laws and customs, entry requirements, health conditions, and other important travel issues they may encounter on their trip.</p>\n\n<p>Government employees travelling on Government status must use the official passport (green passport) as per Passport Canada.</p>\n\n<p>Specific one-on-one briefings will be offered to employees travelling to countries that are considered to pose a significant espionage risk to the department or Canadian interest.</p>\n\n<p>Upon their return employees are requested to report any security anomalies or incidents they encountered while abroad. This will allow Departmental Security Services (DSS) and Central Security Agencies to better manage emerging security trends and threats.</p>\n\n<ul class=\"lst-spcd\">\n\t<li><a href=\"https://intranet.agr.gc.ca/resources/prod/ppt/cmb_dggi/services/defensive_sec_briefing_session-eng.ppt\">Defensive security brief (PPT)</a><br>\n\t\tSecurity concerns for travel and the aggressive manner in which some countries collect intelligence. Recap for Security Awareness Training (AGR-620).</li>\n\t<li><a href=\"https://travel.gc.ca/travelling/registration\">Registration of Canadians abroad</a><br>\n\t\tFree service that allows the Government of Canada to notify you in case of an emergency abroad or a personal emergency at home.</li>\n\t<li><a href=\"https://travel.gc.ca/travelling/publications/bon-voyage-but\">Essential information for Canadian travellers</a><br>\n\t\tHow to travel safely and lists of consular services available to Canadians abroad.</li>\n\t<li><a href=\"https://travel.gc.ca/voyager/publications/un-bon-depart\">A Canadian\u2019s guide to healthy travel abroad</a><br>\n\t\tEssential information on understanding travel health risks, taking preventive measures, coping with a health emergency abroad and accessing consular services in a health emergency.</li>\n\t<li><a href=\"https://travel.gc.ca/travelling/publications/her-own-way\">A woman's safe travel guide</a><br>\n\t\tPreventive, female-friendly approach to tackling the security, cultural, health, and social concerns of women travelers.</li>\n\t<li><a href=\"?id=1325703893933\">The A-B-C's of protected information</a><br>\n\t\tHow to classify, store and send protected or classified information.</li>\n\t<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/cmb_dggi/services/risk_mitigation_oversight_surveillance_attenuation_risques-eng.pdf\">International travel (Process for risk mitigation oversight) (PDF)</a><br>\n\t\tDiagram of the process for employees to mitigate risk during travel with AAFC devices.</li>\n\t<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/cmb_dggi/services/mobile_devices_international_travel.pdf\">Mobile technologies in international travel - ITSB-88 (PDF)</a><br>\n\t\tBest practices to support the development of IT Security policies for travel ranging from routine low risk business travel to high risk travel requirements.</li>\n\t<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/cmb_dggi/services/occupant_away_tent_card_absent_bureau.pdf\">Occupant away from the office - tent card (PDF)</a><br>\n\t\tPrintable tent card for your desk.</li>\n</ul>\n\n<h2>Contact us</h2>\n\n<p>Contact <a href=\"mailto:aafc.travelsecurity-securitevoyage.aac@canada.ca\">aafc.travelsecurity-securitevoyage.aac@canada.ca</a></p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Voyagez-vous \u00e0 l'\u00e9tranger?</p>\n\n<p>Lorsque des employ\u00e9s se d\u00e9placent \u00e0 l'\u00e9tranger \u00e0 titre de repr\u00e9sentants d'Agriculture et Agroalimentaire Canada (AAC), il est  obligatoire que les employ\u00e9s prennent le <a href=\"?id=1325695839878\">programme de sensibilisation \u00e0 la s\u00e9curit\u00e9  en ligne (AGR-620)</a>. Ce cours obligatoire permet de s'assurer que les employ\u00e9s comprennent le contexte de la s\u00fbret\u00e9 et de la s\u00e9curit\u00e9 \u00e0 destination, les lois et les coutumes locales, les exigences en mati\u00e8re d'entr\u00e9e, les conditions sanitaires et d'autres aspects importants avec lesquels ils pourraient avoir \u00e0 composer pendant leur voyage.</p>\n\n<p>Les fonctionnaires f\u00e9d\u00e9raux qui voyagent en service command\u00e9 doivent utiliser le passeport officiel (couverture verte) selon Passeport Canada.</p>\n\n<p>Des s\u00e9ances d'information individuelles sont offertes aux employ\u00e9s qui doivent se rendre dans des pays consid\u00e9r\u00e9s comme pr\u00e9sentant un risque \u00e9lev\u00e9 d'espionnage pour le Minist\u00e8re ou les int\u00e9r\u00eats canadiens.</p>\n\n<p>\u00c0 leur retour, les employ\u00e9s doivent signaler toute anomalie ou tout incident li\u00e9 \u00e0 la s\u00e9curit\u00e9 dont ils ont pu \u00eatre t\u00e9moins \u00e0 l'\u00e9tranger, ce qui permettra aux Services de s\u00e9curit\u00e9 minist\u00e9riels (SSM) et aux organismes centraux de s\u00e9curit\u00e9 de mieux g\u00e9rer les nouvelles tendances et menaces en mati\u00e8re de s\u00e9curit\u00e9.</p>\n\n<ul class=\"lst-spcd\">\n\t<li><a href=\"https://intranet.agr.gc.ca/resources/prod/ppt/cmb_dggi/services/defensive_sec_briefing_session-fra.ppt\">S\u00e9ance d'information sur les d\u00e9placements \u00e0 l'\u00e9tranger (PPT)</a><br>\n\t\tLes probl\u00e8mes de s\u00e9curit\u00e9 lors des voyages. R\u00e9sum\u00e9 du programme de sensibilisation \u00e0 la s\u00e9curit\u00e9 en ligne (AGR-620)</li>\n\t<li><a href=\"https://voyage.gc.ca/voyager/inscription\">Inscription des Canadiens \u00e0 l'\u00e9tranger</a><br>\n\t\tUn service gratuit qui permet au gouvernement du Canada de vous aviser en cas d\u2019urgence \u00e0 l\u2019\u00e9tranger ou \u00e0 la maison.</li>\n\t<li><a href=\"https://voyage.gc.ca/voyager/publications/bon-voyage-mais?_ga=2.60287789.1642008010.1597062046-163031211.1594230680\">Renseignements indispensables pour les voyageurs canadiens</a><br>\n\t\tLa fa\u00e7on de voyager en toute s\u00e9curit\u00e9 et \u00e9num\u00e8re certains des services consulaires offerts aux Canadiens \u00e0 l\u2019\u00e9tranger.</li>\n\t<li><a href=\"https://voyage.gc.ca/voyager/publications/un-bon-depart?_ga=2.131935759.1642008010.1597062046-163031211.1594230680\">Un guide sant\u00e9 \u00e0 l\u2019intention des voyageurs canadiens</a><br>\n\t\tL\u2019information essentielle sur les risques pour la sant\u00e9 des voyageurs, sur les mesures pr\u00e9ventives \u00e0 prendre avant, pendant et apr\u00e8s votre voyage, sur la fa\u00e7on de g\u00e9rer une urgence de sant\u00e9 \u00e0 l\u2019\u00e9tranger et sur l\u2019acc\u00e8s aux services consulaires en cas d\u2019urgence m\u00e9dicale.</li>\n\t<li><a href=\"https://voyage.gc.ca/voyager/publications/voyager-au-feminin?_ga=2.60173869.1642008010.1597062046-163031211.1594230680\">Voyager au f\u00e9minin \u2013 La s\u00e9curit\u00e9 avant tout</a><br>\n\t\tUne approche pr\u00e9ventive et adapt\u00e9e aux enjeux qui touchent les voyageuses en mati\u00e8re de s\u00e9curit\u00e9, de culture, de sant\u00e9 et de soci\u00e9t\u00e9</li>\n\t<li><a href=\"?id=1325703893933\">Les A-B-C des renseignements prot\u00e9g\u00e9s</a><br>\n\t\tComment classer, stocker et envoyer des informations prot\u00e9g\u00e9es ou classifi\u00e9es.</li>\n\t<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/cmb_dggi/services/risk_mitigation_oversight_surveillance_attenuation_risques-fra.pdf\">D\u00e9placements internationaux (processus de surveillance de l\u2019att\u00e9nuation des risques) (PDF)</a><br>\n\t\tDiagramme du processus permettant aux employ\u00e9s d\u2019att\u00e9nuer les risques lorsqu\u2019ils voyagent avec des appareils d\u2019AAC.</li>\n\t<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/cmb_dggi/services/mobile_devices_international_travel.pdf\">Technologies mobiles pour les voyages internationaux - ITSB-88 (en anglais seulement) (PDF)</a><br>\n\t\tLes meilleures pratiques pour soutenir le d\u00e9veloppement de politiques de s\u00e9curit\u00e9 informatique pour les voyages allant des voyages d\u2019affaires de routine de fiable niveau aux voyages \u00e0 haut risque.</li>\n\t<li><a href=\"https://intranet.agr.gc.ca/resources/prod/pdf/cmb_dggi/services/occupant_away_tent_card_absent_bureau.pdf\">Absent du bureau \u2013 une carte tente (PDF)</a><br>\n\t\tUne carte-tente imprimable pour votre bureau.</li>\n</ul>\n \n<h2>Communiquez avec nous</h2>\n\n<p>Courriel\u00a0: <a href=\"mailto:aafc.travelsecurity-securitevoyage.aac@canada.ca\">aafc.travelsecurity-securitevoyage.aac@canada.ca</a></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Defensive Security Brief - Travel",
        "fr": "S\u00e9ance d'information sur la s\u00e9curit\u00e9 d\u00e9fensive - Voyages"
    }
}