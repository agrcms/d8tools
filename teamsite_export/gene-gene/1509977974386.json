{
    "dcr_id": "1509977974386",
    "lang": "en",
    "title": {
        "en": "Administrative Recruitment and Development Program",
        "fr": "Programme de recrutement et de perfectionnement de professionnels en administration"
    },
    "modified": "2018-12-20 00:00:00.0",
    "issued": "2017-11-09 00:00:00.0",
    "type_name": "intra-intra/gene-gene",
    "node_id": "1474032522040",
    "layout_name": "1 column",
    "dc_date_created": "2017-11-06",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2017-11-09",
            "fr": "2017-11-09"
        },
        "modified": {
            "en": "2018-12-20",
            "fr": "2018-12-20"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Administrative Recruitment and Development Program",
            "fr": "Programme de recrutement et de perfectionnement de professionnels en administration"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n\n<p>The Administrative Recruitment and Development Program (ARDP) is designed to bring structure and consistency to the training and development of administrative professionals working at Agriculture and Agri-Food Canada. The ARDP is a development program that maximizes learning, development and networking opportunities.</p>\n\n<p>Program participants will gain knowledge and experience in typical administrative duties, such as:</p>\n\n<ul>\n<li>finance and human resources</li>\n<li>travel</li>\n<li>information and email management</li>\n<li>task prioritization and time management</li>\n</ul>\n\n<p>Upon completion, administrative professionals will be better prepared to provide consistent, effective support for executives across the Department.</p>\n\n<h2>Program description and eligibility</h2>\n\n<p>The program consists of two streams. Both streams are designed for administrative professionals whose main function is to support an executive at AAFC (that is managing calendars, coordinating meetings, overseeing budgets, managing TRECS, etc.). Interested employees are encouraged to speak with their managers as part of their learning plan and career development discussions.</p>\n\n<h3>Stream A: New and recent hires</h3>\n\n<p><b>Stream A</b> is for new administrative professionals, hired at the AS-01 level. Candidates can be AS-01s recently hired by any given branch, terms hired for a short duration, co-op students or casuals from post-secondary administrative programs.</p>\n\n<p class=\"mrgn-lft-md\"><b>Program Length:</b> 12 weeks</p>\n\n<p class=\"mrgn-lft-md\"><b>Intake:</b> Once or twice per year, as required. The ARDP Steering Committee will contact branch representatives to determine which of their new employees should participate in the upcoming cohort.</p>\n\n<h3>Stream B: Experienced professionals</h3>\n\n<p><b>Stream B</b> is for experienced administrative professionals at the AS-01 to AS-04 levels.</p>\n\n<p class=\"mrgn-lft-md\"><b>Program Length:</b> 4 days</p>\n<p class=\"mrgn-lft-md\"><b>Intake:</b> Annually. The ARDP Steering Committee will send a call letter each spring to\tbranch representatives to determine which employees should participate in the \tupcoming Stream B cohorts.</p>\n\n<h2>Resources</h2>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=103524308&amp;lang=eng\" target=\"_blank\">Administrative Recruitment and Development Program \u2013 Program Guide</a></li>\n<li><a href=\"https://collab.agr.gc.ca/co/ardp-prpaa/SitePages/Home.aspx\">Administrative Professional Community Knowledge Workspace Page</a></li>\n</ul>\n\n<h2>Contact the Administrative Recruitment and Development Program</h2>\n\n<p>Please contact us at <a href=\"mailto:aafc.ardp-prppa.aac@canada.ca\">aafc.ardp-prppa.aac@canada.ca</a> for more information.</p>\n\t\t",
        "fr": "\n\t\t\t  \n\n<p>Le Programme de recrutement et de perfectionnement de professionnels en administration (PRPPA) vise \u00e0 structurer et \u00e0 uniformiser la formation et le perfectionnement des professionnels en administration qui travaillent \u00e0 Agriculture et Agroalimentaire Canada. Le PRPPA est un programme de perfectionnement qui maximise les occasions d\u2019apprentissage, de perfectionnement et de r\u00e9seautage.</p>\n\n<p>Les participants au Programme acquerront de l\u2019exp\u00e9rience et des connaissances pertinentes pour l\u2019exercice de t\u00e2ches administratives typiques telles que\u00a0:</p>\n\n<ul>\n<li>les finances et les ressources humaines,</li>\n<li>les voyages,</li>\n<li>la gestion de l\u2019information et des courriels,</li>\n<li>la priorisation des t\u00e2ches et la gestion du temps.</li>\n</ul>\n\n<p>Une fois le programme termin\u00e9, les professionnels en administration seront pr\u00eats \u00e0 fournir un soutien constant et efficace aux cadres sup\u00e9rieurs du Minist\u00e8re.</p>\n\n<h2>Description du programme et admissibilit\u00e9</h2>\n\n<p>Le programme comporte deux volets qui sont con\u00e7us pour accueillir des professionnels en administration dont la t\u00e2che principale est d\u2019appuyer un cadre sup\u00e9rieur d\u2019AAC (g\u00e9rer les horaires, coordonner les r\u00e9unions, superviser les budgets, g\u00e9rer le SSRCHD, etc.) Les employ\u00e9s qui veulent participer au programme doivent en parler \u00e0 leur gestionnaire lorsqu\u2019ils discuteront de leur plan d\u2019apprentissage et de leur perfectionnement professionnel.</p>\n\n<h3>Volet A : Nouveaux employ\u00e9s et employ\u00e9s embauch\u00e9s r\u00e9cemment</h3>\n\n<p>Le <b>volet A</b> s\u2019adresse aux nouveaux professionnels en administration, embauch\u00e9s au niveau AS-01. Les participants peuvent \u00eatre des AS-01 r\u00e9cemment embauch\u00e9s par une direction g\u00e9n\u00e9rale ou encore des employ\u00e9s embauch\u00e9s pour une courte dur\u00e9e d\u00e9termin\u00e9e, des \u00e9tudiants coop ou des employ\u00e9s occasionnels participants \u00e0 des programmes administratifs postsecondaires.</p>\n\n<p class=\"mrgn-lft-md\"><b>Dur\u00e9e\u00a0:</b> 12 semaines</p>\n\n<p class=\"mrgn-lft-md\"><b>Admissions\u00a0:</b> Une ou deux fois par ann\u00e9e, au besoin. Le Comit\u00e9 directeur du PRPPA communiquera avec les repr\u00e9sentants des directions g\u00e9n\u00e9rales pour savoir quels nouveaux employ\u00e9s devraient faire partie de la prochaine cohorte.</p>\n\n<h3>Volet B : Professionnels d\u2019exp\u00e9rience</h3>\n\n<p>Le <b>volet B</b> s\u2019adresse aux professionnels en administration d'exp\u00e9rience qui occupent des postes class\u00e9s aux niveaux AS-01 \u00e0 AS-04.</p>\n\n<p class=\"mrgn-lft-md\"><b>Dur\u00e9e\u00a0:</b> 4 jours</p>\n<p class=\"mrgn-lft-md\"><b>Admissions\u00a0:</b> Annuelles. Au printemps, le Comit\u00e9 directeur du PRPPA enverra une lettre aux repr\u00e9sentants des directions g\u00e9n\u00e9rales pour savoir quels employ\u00e9s devraient faire partie de la prochaine cohorte du volet B.</p>\n\n<h2>Ressources</h2>\n\n<ul>\n<li><a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=103659666&amp;lang=fra\" target=\"_blank\">Programme de recrutement et de perfectionnement de professionnels en administration \u2013 Guide du Programme</a></li>\n<li><a href=\"https://collab.agr.gc.ca/co/ardp-prpaa/SitePages/Home.aspx\">Communaut\u00e9 des professionnels en administration - Espace de travail du savoir</a></li>\n</ul>\n\n<h2>Contactez le Programme de recrutement et de perfectionnement de professionnels en administration</h2>\n\n<p>Pour de plus amples renseignements, veuillez communiquer avec nous \u00e0 l\u2019adresse <a href=\"mailto:aafc.ardp-prppa.aac@canada.ca\">aafc.ardp-prppa.aac@canada.ca</a>.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Administrative Recruitment and Development Program",
        "fr": "Programme de recrutement et de perfectionnement de professionnels en administration"
    }
}