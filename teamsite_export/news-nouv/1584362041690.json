{
    "dcr_id": "1584362041690",
    "lang": "en",
    "title": {
        "en": "Update on AAFC and COVID-19 - March 15, 2020",
        "fr": "Mise \u00e0 jour d\u2019AAC sur la COVID-19 - 15 mars 2020"
    },
    "modified": "2020-03-17 00:00:00.0",
    "issued": "2020-03-16 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2020-03-16",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2020-03-16",
            "fr": "2020-03-16"
        },
        "modified": {
            "en": "2020-03-17",
            "fr": "2020-03-16"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Update on AAFC and COVID-19 - March 15, 2020",
            "fr": "Mise \u00e0 jour d\u2019AAC sur la COVID-19 - 15 mars 2020"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "As you know, the COVID-19 situation continues to evolve. Please note that, as we continue to monitor the situation closely, ensuring the health and safety of our people and our workplace is our top priority.",
            "fr": "Comme vous le savez, la situation relative \u00e0 la COVID-19 continue d'\u00e9voluer. Alors que nous continuons de suivre la situation de pr\u00e8s, notre priorit\u00e9 absolue est d'assurer la sant\u00e9 et la s\u00e9curit\u00e9 de notre personnel et de nos lieux de travail."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>As you know, the COVID-19 situation continues to evolve. Please note that, as we continue to monitor the situation closely, ensuring the health and safety of our people and our workplace is our top priority.</p>\n<p>To keep you updated on the situation, we are providing the following guidance on a number of issues.</p>\n<h2><strong>Travel</strong></h2>\n<p>The Government of Canada advised on Friday to avoid international travel. Effective immediately, to ensure the health and safety of all employees, <strong>any employee returning from international travel, including the United States, is not to return to the workplace for 14 days from their return date to Canada</strong>.</p>\n<h2><strong>Leave</strong></h2>\n<p>In terms of leave, please note the following guidance:</p>\n<ul>\n<li><strong>Employees who are required by public health officials to self-isolate</strong>: If in good health and able to work, employees will be asked to discuss with their managers the option to telework. If that is not possible, the employees will be granted \u201cother leave with pay\u201d (699 code) as per their collective agreements.<br>\n\t<br>\n\tIndividual cases may have to be examined on their own merits (i.e., if an employee willingly chose to travel to affected areas contrary to public health advice).</li>\n</ul>\n<p>\u00a0</p>\n<ul>\n<li><strong>Employees whose children cannot attend school or daycare due to a closure or because of attendance restrictions in place in relation to the COVID-19 situation</strong>:\n\t\n<ul>\n<li>Employees will attempt to make alternative care arrangements.</li>\n<li>If that is not possible, they will discuss with their managers the option to telework.</li>\n<li>If that is not possible, they will be granted \u201cother leave with pay\u201d (699 code).</li>\n</ul>\n</li>\n</ul>\n<p>The above provisions for disruption of school and daycare operations related to COVID-19 will remain available to employees and managers for the duration of the disruption in the respective jurisdictions.</p>\n<p>We believe the above takes into consideration the collective agreements, continuity of service to Canadians, as well as reflecting the imperative we all have to follow public health advice as Canada continues its containment efforts. As the situation evolves, we would like to underscore that changes in the public health advice could lead to a different approach.</p>\n<p>If you have any questions related to leave or other HR issues, please talk to your manager. In addition, you can contact the HR Centre at 1-855-545-9575 or <a href=\"mailto:aafc.hrcentrerh.aac@canada.ca\">aafc.hrcentrerh.aac@canada.ca</a>.</p>\n<h2><strong>Flexible work arrangements</strong></h2>\n<p>While AAFC buildings remain open, employees are encouraged to work from home.</p>\n<p>The following is guidance to help our workforce maximize productivity individually and collectively:</p>\n<ul>\n<li>Use mobile devices whenever possible to send and receive emails;</li>\n<li>Connect to VPN/SRA, get what you need from the corporate network, and then disconnect, which allows for others to do the same;</li>\n<li>Limit the use of video conferencing on the GC network when audio conferencing will suffice;</li>\n<li>Localize any files before leaving the office; and</li>\n<li>Download documents outside normal business hours.</li>\n</ul>\n<p>Further tips to maximize collaboration within and across teams include:</p>\n<ul>\n<li>Using public cloud services to collaborate with colleagues, for unclassified work (examples: Facetime, MS Teams, Google HangOuts, Slack, etc.); and</li>\n<li>Using the BBMe application to communicate with colleagues, for up to Protected B work.</li>\n</ul>\n<h2><strong>Mental health</strong></h2>\n<p>We\u2019d like to remind everyone of the importance of taking care of yourselves and your families. Under circumstances such as these, it is natural to experience different levels of anxiety. Please keep in mind that the <a href=\"http://intranet.agr.gc.ca/agrisource/eng/human-resources/workplace-wellness/coronavirus-disease-covid-19?id=1287673689033\">Employee and Family Assistance Program</a> (EFAP) is always available, and you can always speak to your manager.</p>\n<p>We ask that you keep your manager\u2019s contact information and the Emergency and Business Continuity Info-Line number (1-877-898-4449) handy, so you can stay up-to-date on any emerging information.</p>\n<p>Finally, please continue to stay up to date through <a href=\"http://intranet.agr.gc.ca/agrisource/eng/human-resources/workplace-wellness/coronavirus-disease-covid-19?id=1583349579374\">AgriSource</a> and <a href=\"http://multimedia.agr.gc.ca/COVID-19/index-eng.html\">AAFC online</a>, as well as the <a href=\"https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection.html?utm_source=beprepared-en&amp;utm_medium=%20chief-hr-office-email-&amp;utm_campaign=covid-1920\">Canada.ca/Coronavirus</a> and <a href=\"https://intranet.canada.ca/psc-fsc/messages/cmt-538-eng.asp?utm_campaign=covid-1920&amp;utm_source=chro&amp;utm_medium=email&amp;utm_content=intranet-canada-ca-2020-03-13-en\">GCIntranet</a> websites.</p>\n<p>We will provide you with more details as they become available.</p>\n<p>Thank you and take care of yourselves.</p>\n<p>\u00a0</p>\n<p><strong>Chris Forbes</strong><br>\nDeputy Minister</p>\n<p><strong>Annette Gibbons</strong><br>\nAssociate Deputy Minister</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Comme vous le savez, la situation relative \u00e0 la COVID-19 continue d\u2019\u00e9voluer. Alors que nous continuons de suivre la situation de pr\u00e8s, notre priorit\u00e9 absolue est d\u2019assurer la sant\u00e9 et la s\u00e9curit\u00e9 de notre personnel et de nos lieux de travail. </p>\n<p>\u00c0 titre d\u2019information, nous avons \u00e9tabli les directives suivantes.</p>\n<h2><strong>Voyages</strong></h2>\n<p>Vendredi, le gouvernement du Canada a recommand\u00e9 aux voyageurs d\u2019\u00e9viter tout voyage international. D\u00e8s maintenant, pour prot\u00e9ger la sant\u00e9 et la s\u00e9curit\u00e9 des employ\u00e9s, <strong>tout employ\u00e9 qui revient d\u2019un voyage international, y compris un voyage aux \u00c9tats-Unis, ne doit pas retourner au travail pendant 14 jours \u00e0 compter de la date de son retour au Canada</strong>.</p>\n<h2><strong>Cong\u00e9s</strong></h2>\n<p>En ce qui concerne les cong\u00e9s, veuillez prendre connaissance des indications suivantes :</p>\n<ul>\n<li><strong>Employ\u00e9s auxquels les autorit\u00e9s de sant\u00e9 publique ont demand\u00e9 de s\u02bcauto-isoler : </strong>S\u2019ils sont en bonne sant\u00e9 et en mesure de travailler, les employ\u00e9s seront invit\u00e9s \u00e0 discuter de la possibilit\u00e9 de faire du t\u00e9l\u00e9travail avec leur gestionnaire. Si ce n\u02bcest pas possible, ils se verront accorder un \u00ab autre cong\u00e9 pay\u00e9 \u00bb (code 699), conform\u00e9ment \u00e0 leur convention collective.<br>\n\t<br>\n\tIl faudra examiner certains cas selon les circonstances qui leur sont propres (p. ex., si un employ\u00e9 a choisi volontairement de se rendre dans une zone touch\u00e9e, contrairement aux conseils de sant\u00e9 publique).</li>\n</ul>\n<p>\u00a0</p>\n<ul>\n<li><strong>Employ\u00e9s dont les enfants ne peuvent aller \u00e0 l\u2019\u00e9cole en raison d\u2019une fermeture ou de restrictions en vigueur relativement \u00e0 la situation caus\u00e9e par la COVID-19 :</strong>\n\t\n<ul>\n<li>Les employ\u00e9s doivent essayer de trouver d\u02bcautres solutions pour faire garder leurs enfants.</li>\n<li>Si cela n\u02bcest pas possible, ils discuteront avec leur gestionnaire de la possibilit\u00e9 de faire du t\u00e9l\u00e9travail.</li>\n<li>Si cela n\u02bcest pas possible, ils se verront accorder un \u00ab autre cong\u00e9 pay\u00e9 \u00bb (code 699).</li>\n</ul>\n</li>\n</ul>\n<p>Les dispositions relatives aux fermetures d\u2019\u00e9coles et de garderies dues \u00e0 la COVID-19 demeureront en vigueur pour les employ\u00e9s et les gestionnaires le temps que dureront les perturbations dans les endroits respectifs.</p>\n<p>Nous croyons que ces dispositions tiennent compte des conventions collectives, de la continuit\u00e9 des services offerts aux Canadiens et de l\u2019importance de suivre les conseils de sant\u00e9 publique alors que le Canada poursuit ses efforts de lutte contre la propagation. Nous tenons \u00e0 souligner qu\u2019\u00e0 mesure que la situation \u00e9volue, une approche diff\u00e9rente sera adopt\u00e9e si les conseils de sant\u00e9 publique changent.</p>\n<p>Si vous avez des questions concernant les cong\u00e9s ou d\u2019autres sujets li\u00e9s aux RH, veuillez en parler \u00e0 votre gestionnaire. Vous pouvez \u00e9galement communiquer avec le Centre RH au 1-855-545-9575 ou au <a href=\"mailto:aafc.hrcentrerh.aac@canada.ca\">aafc.hrcentrerh.aac@canada.ca</a>.</p>\n<h2><strong>Modalit\u00e9s de travail flexibles</strong></h2>\n<p>Bien que les immeubles d\u2019AAC demeurent ouverts, les employ\u00e9s sont encourag\u00e9s \u00e0 travailler \u00e0 partir de la maison.</p>\n<p>Voici des conseils pour aider les employ\u00e9s \u00e0 optimiser leur productivit\u00e9 individuelle et collective :</p>\n<ul>\n<li>utiliser les appareils mobiles dans la mesure du possible pour envoyer et recevoir des courriels;</li>\n<li>se brancher au RVP/ADP pour obtenir ce dont on a besoin sur le r\u00e9seau minist\u00e9riel et se d\u00e9brancher, ce qui permettra \u00e0 d\u02bcautres personnes de le faire aussi;</li>\n<li>limiter l\u2019utilisation de la vid\u00e9oconf\u00e9rence sur le r\u00e9seau du GC lorsque l\u2019audioconf\u00e9rence suffit;</li>\n<li>enregistrer les fichiers sur le bureau avant de quitter le travail;</li>\n<li>t\u00e9l\u00e9charger des documents en dehors des heures normales de travail.</li>\n</ul>\n<p>Voici des conseils pour optimiser la collaboration au sein des \u00e9quipes et entre elles :</p>\n<ul>\n<li>utiliser les services infonuagiques publics pour collaborer avec ses coll\u00e8gues, dans le cas des travaux non classifi\u00e9s (exemples : Facetime, MS Teams, Google Hangouts, Slack, etc.);</li>\n<li>utiliser l\u2019application BBME pour communiquer avec ses coll\u00e8gues, dans le cas des travaux classifi\u00e9s jusqu\u2019au niveau Prot\u00e9g\u00e9 B.</li>\n</ul>\n<h2><strong>Sant\u00e9 mentale</strong></h2>\n<p>Nous rappelons \u00e0 tous l\u2019importance de prendre soin de vous et de vos familles. Dans de telles circonstances, il est naturel de ressentir un certain niveau d\u2019anxi\u00e9t\u00e9. N\u2019oubliez pas que vous pouvez toujours recourir au <a href=\"http://intranet.agr.gc.ca/agrisource/fra/ressources-humaines/programme-daide-aux-employes-et-a-la-famille?id=1287673689033\">Programme d\u2019aide aux employ\u00e9s et \u00e0 la famille</a> (PAEF) ou en parler \u00e0 votre gestionnaire.</p>\n<p>Nous vous demandons de garder les coordonn\u00e9es de votre gestionnaire et le num\u00e9ro de la Ligne-info sur les mesures d\u2019urgence et la continuit\u00e9 des activit\u00e9s (1-877-898-4449) \u00e0 port\u00e9e de la main, pour que vous puissiez vous tenir au courant des derni\u00e8res informations.</p>\n<p>Enfin, veuillez continuer de vous tenir au courant de la situation en consultant <a href=\"http://intranet.agr.gc.ca/agrisource/fra/ressources-humaines/mieux-etre-au-travail/maladie-a-coronavirus-covid-19?id=1583349579374\">AgriSource</a> et <a href=\"http://multimedia.agr.gc.ca/COVID-19/index-fra.html\">AAC en ligne</a>, ainsi que les sites Web <a target=\"_blank\" href=\"https://www.canada.ca/fr/sante-publique/services/maladies/2019-nouveau-coronavirus.html\">Canada.ca/Coronavirus</a> et <a target=\"_blank\" href=\"https://intranet.canada.ca/psc-fsc/messages/cmt-538-fra.asp?utm_campaign=covid-1920&amp;utm_source=chro&amp;utm_medium=email&amp;utm_content=intranet-canada-ca-2020-03-13-en\">GCIntranet</a>.</p>\n<p>Nous vous fournirons plus de d\u00e9tails d\u00e8s qu\u2019ils seront disponibles.</p>\n<p>Merci et prenez soin de vous.</p>\n<p>\u00a0</p>\n<p><strong>Chris Forbes</strong><br>\nSous-ministre</p>\n<p><strong>Annette Gibbons</strong><br>\nSous-ministre d\u00e9l\u00e9gu\u00e9e</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Update on AAFC and COVID-19 - March 15, 2020",
        "fr": "Mise \u00e0 jour d\u2019AAC sur la COVID-19 - 15 mars 2020"
    },
    "news": {
        "date_posted": {
            "en": "2020-03-15",
            "fr": "2020-03-15"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Chris Forbes, Deputy Minister, and Annette Gibbons, Associate Deputy Minister",
            "fr": "Chris Forbes, Sous-ministre, et Annette Gibbons, Sous-ministre d\u00e9l\u00e9gu\u00e9e"
        }
    }
}