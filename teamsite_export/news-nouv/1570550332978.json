{
    "dcr_id": "1570550332978",
    "lang": "en",
    "title": {
        "en": "NCR - Procurement Processing Within AAFC for Deliveries Prior to March 31, 2020 (NCR)",
        "fr": "RCN - Traitement des achats \u00e0 AAC pour livraisons avant le 31 mars 2020"
    },
    "modified": "2019-10-10 00:00:00.0",
    "issued": "2019-10-10 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-10-08",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-10-10",
            "fr": "2019-10-10"
        },
        "modified": {
            "en": "2019-10-10",
            "fr": "2019-10-10"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "NCR - Procurement Processing Within AAFC for Deliveries Prior to March 31, 2020 (NCR)",
            "fr": "RCN - Traitement des achats \u00e0 AAC pour livraisons avant le 31 mars 2020"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "The following information is to assist you in managing your requirements for the acquisition of goods and services targeting delivery prior to March 31, 2020 in the National Capital Region.",
            "fr": "Les renseignements suivants vous sont offerts dans le but d'appuyer votre gestion de besoins en approvisionnement des biens et services visant une livraison avant le 31 mars 2020 dans la RCN."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>The following information is to assist you in managing your requirements for the acquisition of goods and services targeting delivery prior to March 31, 2020 in the National Capital Region.</p>\n<p>In view of the lead time required by the free trade agreements, here is a summary of the lead times required for year-end procurements processed under AAFC delegation and processed by PSPC.</p>\n<h2><strong>Year-end procurement timelines</strong></h2>\n<table cellpadding=\"3\" cellspacing=\"3\" border=\"1\" align=\"center\" width=\"750\">\n<tbody>\n<tr>\n<td style=\"width: 135pt;\">\n<p><strong>Solicitation Type</strong></p>\n</td>\n<td style=\"border-color: windowtext windowtext windowtext #000000; width: 135pt;\">\n<p><strong>Tender Posting Period</strong></p>\n</td>\n<td style=\"border-color: windowtext windowtext windowtext #000000; width: 2.25in;\">\n<p><strong>Requisition Receipt Deadline</strong></p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 135pt;\">\n<p>Competitive - $25,000 &amp; greater Subject to Free Trade Agreements*</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 135pt;\">\n<p>Minimum of 40 calendar days</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 2.25in;\">\n<p>November 8, 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 135pt;\">\n<p>Competitive - $25,000 &amp; greater Not subject to Free Trade Agreements*</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 135pt;\">\n<p>Minimum of 15 -30 calendar days</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 2.25in;\">\n<p>November 29, 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 135pt;\">\n<p>Sole Source (including ACAN) - $25,000 &amp; greater</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 135pt;\">\n<p>Minimum of 15 calendar days</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 2.25in;\">\n<p>January 3, 2020</p>\n</td>\n</tr>\n</tbody>\n</table>\n<p>\u00a0</p>\n<p>To ensure goods and services are charged to your budgets for Fiscal Year 2019-2020, the goods and/or services must be received no later than March 31, 2020. Goods/services that are received after March 31, 2020 will be charged to your budgets in Fiscal Year 2020-2021. Backup documentation, such as packing slips, work order confirmations, proof of goods receipts, etc., must be submitted to your procurement service provider in order to process the goods receipt requirements in the SAP financial system.</p>\n<p>For office supply purchases, all orders must be received by the Corporate Materiel Management Centre (CMMC) no later than <strong>March 1, 2020</strong> in order to have them charged to your budgets for Fiscal Year 2019-2020.</p>\n<h2><strong>Client service expectations</strong></h2>\n<p>I would also like to bring to your attention that policies, trade agreements and processes from the department, Public Services and Procurement Canada (PSPC), and Shared Services Canada (SSC) must be adhered to. When planning any year-end requirements, sufficient lead time must be considered if contracts for goods or services are expected to be completed by March 31, 2020. This lead time will ensure the goods or services are delivered and payment is issued against the current fiscal year. <em>All year-end requirements must be received by the deadlines mentioned in the table of this Bulletin in order to proceed with processing the requirements using this fiscal year\u2019s budgets.</em></p>\n<p>For deadlines outside of AAFC delegation, refer to the following:</p>\n<p>PSPC: <a href=\"https://www.gcpedia.gc.ca/gcwiki/images/b/b9/2019-2020_Year_End_Deadline_Letter_E_Ontario.pdf\">Policies and Processes / Year End Information</a>\u00a0(PDF).</p>\n<p>SSC: Dates will be published on <a href=\"https://www.canada.ca/en/shared-services.html\">the SSC\u00a0website</a> in the coming months.</p>\n<p>Although we are anticipating an increase in work load during the last quarter of the fiscal year, members of my team will continue to make every effort possible to ensure that our clients\u2019 requests are actioned in a timely manner. We ask that our clients plan ahead and try to amalgamate their requests for routine requirements (e.g. office supplies) whenever possible.</p>\n<p>Should you need further assistance, have any questions, or just want more clarification, please do not hesitate to contact CMMC at <a href=\"mailto:aafc.cmmc-cgmc.aac@canada.ca\">aafc.cmmc-cgmc.aac@canada.ca</a>.</p>\n<p><strong>\u00a0</strong></p>\n<p><strong>Karen Durnford-McIntosh<br></strong>Director General, Real Property and Asset Management</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Les renseignements suivants vous sont offerts dans le but d\u2019appuyer votre gestion de besoins en approvisionnement des biens et services visant une livraison avant le 31 mars 2020 dans la RCN.</p>\n<p>Compte tenu des d\u00e9lais d\u2019affichage obligatoire selon les accords de libre-\u00e9change, voici un r\u00e9sum\u00e9 des dates limites pour les approvisionnements de fin d\u2019exercice trait\u00e9s selon la d\u00e9l\u00e9gation de pouvoirs d\u2019AAC et trait\u00e9s par SPAC.</p>\n<h2><strong>D\u00e9lais d\u2019approvisionnement de fin d\u2019exercice</strong></h2>\n<table cellpadding=\"3\" cellspacing=\"3\" border=\"1\" align=\"center\" width=\"750\">\n<tbody>\n<tr>\n<td style=\"width: 139.5pt;\">\n<p><strong>Type de demande de soumissions</strong></p>\n</td>\n<td style=\"border-color: windowtext windowtext windowtext #000000; width: 130.5pt;\">\n<p><strong>P\u00e9riode d\u2019affichage de l\u2019appel d\u2019offres</strong></p>\n</td>\n<td style=\"border-color: windowtext windowtext windowtext #000000; width: 2.25in;\">\n<p><strong>Date limite de r\u00e9ception de la demande </strong></p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 139.5pt;\">\n<p>Processus concurrentiel, 25 000 $ et plus, assujetti aux accords de libre-\u00e9change*</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 130.5pt;\">\n<p>Minimum de 40 jours civils</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 2.25in;\">\n<p>8 novembre 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 139.5pt;\">\n<p>Processus concurrentiel, 25 000 $ et plus, non assujetti aux accords de libre-\u00e9change*</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 130.5pt;\">\n<p>Minimum de 15 \u00e0 30 jours civils</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 2.25in;\">\n<p>29 novembre 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 139.5pt;\">\n<p>Processus de type \u00ab fournisseur unique \u00bb (incluant le PAC), 25 000 $ et plus</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 130.5pt;\">\n<p>Minimum de 15 jours civils</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 2.25in;\">\n<p>3 janvier 2020</p>\n</td>\n</tr>\n</tbody>\n</table>\n<p>\u00a0</p>\n<p>Pour faire en sorte que les frais des biens et des services soient port\u00e9s \u00e0 votre budget de l\u2019exercice financier 2019\u20112020, ils doivent \u00eatre re\u00e7us au plus tard le 31 mars 2020. Les frais des biens et des services re\u00e7us apr\u00e8s cette date seront port\u00e9s \u00e0 votre budget de l\u2019exercice financier 2020\u20112021. Les pi\u00e8ces justificatives, comme les bordereaux des marchandises, les confirmations d\u2019autorisations de travail, les preuves de r\u00e9ception des biens et autres, doivent \u00eatre pr\u00e9sent\u00e9es \u00e0 votre fournisseur de services d\u2019approvisionnement en vue de satisfaire aux exigences de r\u00e9ception des biens du syst\u00e8me financier SAP.</p>\n<p>Pour les achats de fournitures de bureau, le Centre de la gestion int\u00e9gr\u00e9e du mat\u00e9riel (CGIM) doit avoir re\u00e7u toutes les commandes au plus tard le <strong>1 mars 2020</strong> pour qu\u2019elles soient port\u00e9es \u00e0 votre budget de l\u2019exercice financier 2019\u20112020.</p>\n<h2><strong>Attentes \u00e0 l\u2019\u00e9gard du service \u00e0 la client\u00e8le</strong></h2>\n<p>J\u2019aimerais \u00e9galement souligner que les politiques, les accords et les processus commerciaux doivent \u00eatre respect\u00e9s, qu\u2019il s\u2019agisse de transactions au sein du Minist\u00e8re, Services publics et Approvisionnement Canada (SPAC), ou Services partag\u00e9s Canada (SPC). Au moment de planifier la fin de l\u2019exercice financier, il faut pr\u00e9voir suffisamment de temps si l\u2019on veut que les march\u00e9s de biens et de services soient pass\u00e9s au plus tard le 31 mars 2020 et que les biens et services soient livr\u00e9s et les paiements vers\u00e9s pendant l\u2019exercice en cours. <em>Tous les besoins de fin d\u2019exercice doivent \u00eatre re\u00e7us au plus tard aux dates limites mentionn\u00e9es dans le tableau du pr\u00e9sent bulletin afin de permettre le traitement des besoins \u00e0 partir des budgets de l\u2019exercice en cours.</em></p>\n<p>Pour les dates limites qui ne d\u00e9pendent pas d\u2019AAC, veuillez consulter :</p>\n<p>SPAC : <a href=\"https://www.gcpedia.gc.ca/gcwiki/images/5/5d/2019-2020_Year_End_Deadline_Letter_F_Ontario.pdf\">Politiques et processus/Information pour fin de l\u2019ann\u00e9e financi\u00e8re</a>\u00a0(PDF)</p>\n<p>SPC : Les dates seront publi\u00e9es sur <a href=\"https://www.canada.ca/fr/services-partages.html\">le site Web de SPC</a> au cours des prochains mois.</p>\n<p>M\u00eame si nous pr\u00e9voyons que la charge de travail augmentera au cours du dernier trimestre de l\u2019exercice, les membres de mon \u00e9quipe continueront de faire tout ce qu\u2019ils peuvent pour traiter les demandes de leurs clients en temps opportun. Nous demandons \u00e0 nos clients de planifier leurs besoins et de tenter de regrouper leurs demandes li\u00e9es aux activit\u00e9s courantes (p. ex., fournitures de bureau), dans la mesure du possible.</p>\n<p>Si vous avez des questions ou que vous souhaitez obtenir de l\u2019aide ou de plus amples renseignements, veuillez communiquer avec le CGIM \u00e0 <a href=\"mailto:aafc.cmmc-cgmc.aac@canada.ca\">aafc.cmmc-cgmc.aac@canada.ca</a>.</p>\n<p><strong>\u00a0</strong></p>\n<p><strong>Karen Durnford-McIntosh<br></strong>Directrice g\u00e9n\u00e9rale, Direction de la gestion des biens et des immobilisations</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "NCR - Procurement Processing Within AAFC for Deliveries Prior to March 31, 2020 (NCR)",
        "fr": "RCN - Traitement des achats \u00e0 AAC pour livraisons avant le 31 mars 2020"
    },
    "news": {
        "date_posted": {
            "en": "2019-10-10",
            "fr": "2019-10-10"
        },
        "to": {
            "en": "NCR only",
            "fr": "RCN seulement"
        },
        "by": {
            "en": "Corporate Management Branch",
            "fr": "Direction de la gestion des biens et des immobilisations"
        }
    }
}