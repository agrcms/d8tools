{
    "dcr_id": "1516639398762",
    "lang": "en",
    "title": {
        "en": "Reminder - Mandatory Information Management Training for All Employees - Deadline is March 31",
        "fr": "Rappel - Formation obligatoire en gestion de l'information pour tous les employ\u00e9s - La date limite est le 31 mars"
    },
    "modified": "2020-06-05 00:00:00.0",
    "issued": "2018-01-25 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2018-01-22",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2018-01-25",
            "fr": "2018-01-25"
        },
        "modified": {
            "en": "2020-06-05",
            "fr": "2020-06-05"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Reminder - Mandatory Information Management Training for All Employees - Deadline is March 31",
            "fr": "Rappel - Formation obligatoire en gestion de l'information pour tous les employ\u00e9s - La date limite est le 31 mars"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "As you know, AAFC has made the Fundamentals of Information Management (I301) training course mandatory for all employees.",
            "fr": "Comme vous le savez, AAC a rendu le cours Principes fondamentaux de la gestion de l'information (I301) obligatoire pour tous les employ\u00e9s."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>As you know, AAFC has made the <a href=\"https://learn-apprendre.csps-efpc.gc.ca/application/en/content/fundamentals-information-management-i301\">Fundamentals of Information Management (I301)</a>* training course <a href=\"http://intranet.agr.gc.ca/agrisource/eng/newswork/new-mandatory-information-management-training?id=1476714566423\">mandatory</a> for all employees. Those who haven\u2019t completed the course yet must to do so <strong>by March 31, 2018</strong>, as per the departmental Information Management (<abbr title=\"Information Management\">IM</abbr>) Strategy, which sets out the steps the department is taking to improve the way it manages different types of information. New employees will be expected to take the course as they join the department.</p>\n<p>This one-hour online course offered by the Canada School of Public Service (<abbr title=\"Canada School of Public Service\">CSPS</abbr>) presents basic information management concepts and principles, as well as tips and best practices for managing information. Employees will learn how to apply effective information management in their day-to-day work. Completing the course will help you better prepare for a number of upcoming <abbr title=\"Information Management\">IM</abbr>-related changes over the next few years that will provide great benefits to Canadians and all public servants through quicker access to data, better decision-making, and increased transparency and accountability.</p>\n<p>Reports of completion are provided to management, so make sure that:</p>\n<ul>\n<li>\n<p>your <abbr title=\"Canada School of Public Service\">CSPS</abbr> (GCcampus) account profile lists the Department of Agriculture and Agri-Food as your current department; and</p>\n</li>\n<li>\n<p>your learning tab lists the course as completed.</p>\n</li>\n</ul>\n<p>Details on registration can be found on the <a href=\"http://intranet.agr.gc.ca/agrisource/eng/branches-and-offices/information-systems-branch/services/information-management?id=1328882423004\"><abbr title=\"Information Management\">IM</abbr> page</a> on AgriSource. If you have any issues with the <abbr title=\"Canada School of Public Service\">CSPS</abbr> web site, contact the GCcampus Client Contact Centre for assistance:</p>\n<p><strong>Telephone (Toll-free):</strong> 1-866-703-9598<br>\n<strong>Telephone (NCR):</strong> 819-953-5400</p>\n<p>*Information about the Canada School of Public Service courses, programs and events is available <strong>only</strong> on <a href=\"https://learn-apprendre.csps-efpc.gc.ca/\">GCcampus</a>. <b>To view these products, you will need to log in with your MyAccount username and password.</b></p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/IMTraining.png\" class=\"center-block\"></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Comme vous le savez, AAC a rendu le cours <a href=\"https://learn-apprendre.csps-efpc.gc.ca/application/fr/content/principes-fondamentaux-de-la-gestion-de-linformation-i301\">Principes fondamentaux de la gestion de l\u2019information (I301)</a>* <a href=\"http://intranet.agr.gc.ca/agrisource/fra/nouvelleslouvrage/nouvelle-formation-obligatoire-sur-la-gestion-de-linformation?id=1476714566423\">obligatoire</a> pour tous les employ\u00e9s. Ceux et celles qui n\u2019ont pas encore suivi le cours doivent le faire <strong>d\u2019ici le 31 mars 2018</strong>, conform\u00e9ment \u00e0 la Strat\u00e9gie de gestion de l\u2019information (<abbr title=\"gestion de l\u2019information\">GI</abbr>) du Minist\u00e8re. Cette strat\u00e9gie \u00e9tablit les mesures que le Minist\u00e8re doit prendre pour am\u00e9liorer la fa\u00e7on dont il g\u00e8re les divers types de renseignements. Les nouveaux employ\u00e9s devront suivre ce cours \u00e0 leur entr\u00e9e en fonction.</p>\n<p>Ce cours en ligne d\u2019une heure offert par l\u2019\u00c9cole de la fonction publique du Canada (<abbr title=\"\u00c9cole de la fonction publique du Canada\">EFPC</abbr>) pr\u00e9sente les concepts et les principes fondamentaux qui sous-tendent la gestion de l\u2019information de m\u00eame que des conseils et des pratiques exemplaires en la mati\u00e8re. Les employ\u00e9s apprendront comment g\u00e9rer efficacement l\u2019information dans leurs t\u00e2ches quotidiennes. Le cours vous aidera \u00e0 mieux vous pr\u00e9parer aux changements qui seront apport\u00e9s \u00e0 la <abbr title=\"gestion de l\u2019information\">GI</abbr> au cours des prochaines ann\u00e9es. Ces changements auront plusieurs avantages pour les Canadiens et les fonctionnaires, dont un acc\u00e8s plus rapide aux donn\u00e9es, un meilleur processus de prise de d\u00e9cisions, une plus grande transparence et une plus grande responsabilisation.</p>\n<p>Un rapport d\u2019ach\u00e8vement est envoy\u00e9 \u00e0 la direction. Assurez-vous donc de ce qui suit :</p>\n<ul>\n<li>\n<p>Le profil de votre compte de l\u2019<abbr title=\"\u00c9cole de la fonction publique du Canada\">EFPC</abbr> (GCcampus) indique que le minist\u00e8re de l\u2019Agriculture et de l\u2019Agroalimentaire est votre minist\u00e8re\u00a0d'attache.</p>\n</li>\n<li>\n<p>L\u2019onglet Mon apprentissage indique que le cours est suivi.</p>\n</li>\n</ul>\n<p>Vous pouvez obtenir plus\u00a0renseignements sur l\u2019inscription dans la <a href=\"http://intranet.agr.gc.ca/agrisource/fra/directions-generales-et-bureaux/direction-generale-des-systemes-dinformation/services/gestion-de-linformation?id=1328882423004\">page sur la <abbr title=\"gestion de l\u2019information\">GI</abbr></a> dans AgriSource. Si vous \u00e9prouvez des difficult\u00e9s avec le site Web de l\u2019<abbr title=\"\u00c9cole de la fonction publique du Canada\">EFPC</abbr>, veuillez communiquer avec le Centre de contact avec la client\u00e8le de GCcampus pour obtenir de l\u2019aide :</p>\n<p><strong>T\u00e9l\u00e9phone (sans frais) :</strong> 1-866-703-9598<br>\n<strong>T\u00e9l\u00e9phone (RCN) :</strong> 819-953-5400</p>\n<p>*L'information sur les cours, les programmes et les activit\u00e9s de l'\u00c9cole de la fonction publique du Canada se trouvent <strong>seulement</strong> sur <a href=\"https://learn-apprendre.csps-efpc.gc.ca/\">GCcampus</a>. <b>Pour consulter ces produits, vous devez ouvrir une session avec votre nom d'utilisateur et votre mot de passe MonDossier.</b></p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/IMTraining.png\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Reminder - Mandatory Information Management Training for All Employees - Deadline is March 31",
        "fr": "Rappel - Formation obligatoire en gestion de l'information pour tous les employ\u00e9s - La date..."
    },
    "news": {
        "date_posted": {
            "en": "2018-01-23",
            "fr": "2018-01-23"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Information Systems Branch",
            "fr": "Direction g\u00e9n\u00e9rale des syst\u00e8mes d'information"
        }
    }
}