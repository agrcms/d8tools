{
    "dcr_id": "1590775337778",
    "lang": "en",
    "title": {
        "en": "Working Together: Charting the New Way Forward",
        "fr": "Ensemble, tra\u00e7ons la nouvelle voie \u00e0 suivre"
    },
    "modified": "2020-05-29 00:00:00.0",
    "issued": "2020-05-29 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2020-05-29",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2020-05-29",
            "fr": "2020-05-29"
        },
        "modified": {
            "en": "2020-05-29",
            "fr": "2020-05-29"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Working Together: Charting the New Way Forward",
            "fr": "Ensemble, tra\u00e7ons la nouvelle voie \u00e0 suivre"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "In our last few messages to you and during our town hall on May 12, we talked about the work underway in the department to accommodate a gradual easing of restrictions and a corresponding increase in access to worksites. Today, we would like to share a few more updates.",
            "fr": "Dans nos derniers messages et lors de notre s\u00e9ance de discussion ouverte du 12 mai dernier, nous avons parl\u00e9 des travaux en cours au Minist\u00e8re pour permettre un assouplissement progressif des restrictions et faciliter ainsi l\u2019acc\u00e8s aux lieux de travail. Aujourd\u2019hui, nous aimerions faire quelques mises \u00e0 jour suppl\u00e9mentaires."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>In our last few messages to you and during our town hall on May 12, we talked about the work underway in the department to accommodate a gradual easing of restrictions and a corresponding increase in access to worksites. Today, we would like to share a few more updates.</p>\n<h2><strong>Telework for most\u2026for now</strong></h2>\n<p>It\u2019s important to remember that, virtually overnight, we managed to transition most of our organization to an entirely remote, very productive workplace. We\u2019ve learned that extensive telework does not deter us from delivering on our key commitments to the sector and Canadians. While we take the time we need to plan, <strong>we expect that most of you will continue to work remotely for the time being</strong>.</p>\n<h2><strong>Equipping you to work from home</strong></h2>\n<p>A priority will be making sure you have the office equipment and technology you need to do your jobs comfortably and effectively from home. Next week, we will share important details on what equipment you can access, how it can be transported from the worksite, and how to retrieve any personal belongings. We have also started to replace our aging remote access (VPN) platform with a new system to allow all employees to optimize their remote access.</p>\n<h2><strong>Returning to our worksites</strong></h2>\n<p>It\u2019s important to acknowledge that some of us never left the workplace, requiring access to AAFC buildings to deliver critical services. In addition, our colleagues in the Science and Technology Branch (STB) have recently restarted some time-sensitive field research activities, such as co-op trials, early-generation breeding trials, agronomy trials, integrated pest management studies, and environmental sampling.</p>\n<p><strong>Going forward, the health and safety of our employees continue to be paramount. </strong>We are currently developing plans and guidelines for both the department as a whole and for each worksite, based on public health guidance, local circumstances, and the following three key factors:</p>\n<ul>\n<li><strong>Physical workplace:</strong> We have 55 worksites across the country. While every location has its differences, we need to ensure a standard set of protocols around occupational health and safety.</li>\n<li><strong>Business priorities:</strong> Considering the priorities for the department and each branch, we need to determine what functions are most effectively performed at each of the worksites, and what work can be done remotely.</li>\n<li><strong>Personal circumstances:</strong> Every employee has different personal circumstances. It will be important to recognize these realities and to reflect them in our plans.</li>\n</ul>\n<h2><strong>The very real challenges of social isolation</strong></h2>\n<p>During last week\u2019s conversation with employees, we were reminded that the lack of face-to-face social interaction and feelings of isolation continue to be a challenge for many. We encourage all of you to be mindful of this and to go the extra mile to check in with colleagues. You can also check out the <a href=\"http://multimedia.agr.gc.ca/COVID-19/goodnews-bonnesnouvelles-eng.html#_blank\">Good News Grows</a> page on our <a href=\"http://multimedia.agr.gc.ca/COVID-19/index-eng.html#_blank\">COVID-19 website</a> for new stories about how your AAFC colleagues are keeping busy.</p>\n<p>Please take care of yourselves and your loved ones.</p>\n<p>\u00a0</p>\n<p><strong>Chris Forbes<br></strong>Deputy Minister</p>\n<p><strong>Annette Gibbons<br></strong>Associate Deputy Minister</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Dans nos derniers messages et lors de notre s\u00e9ance de discussion ouverte du 12 mai dernier, nous avons parl\u00e9 des travaux en cours au Minist\u00e8re pour permettre un assouplissement progressif des restrictions et faciliter ainsi l\u2019acc\u00e8s aux lieux de travail. Aujourd\u2019hui, nous aimerions faire quelques mises \u00e0 jour suppl\u00e9mentaires.</p>\n<h2><strong>Le t\u00e9l\u00e9travail pour la plupart des employ\u00e9s... pour l\u2019instant</strong></h2>\n<p>Il est important de se rappeler que, pratiquement du jour au lendemain, nous avons r\u00e9ussi \u00e0 transformer la majeure partie de notre organisation en un lieu de travail \u00e0 distance tr\u00e8s productif. Nous avons appris que le t\u00e9l\u00e9travail g\u00e9n\u00e9ralis\u00e9 ne nous emp\u00eache pas de tenir nos principaux engagements envers le secteur et les Canadiens. Tandis que nous prenons le temps n\u00e9cessaire pour planifier les activit\u00e9s, <strong>nous pr\u00e9voyons que le travail \u00e0 distance se poursuivra pour la plupart d\u2019entre vous jusqu\u2019\u00e0 nouvel ordre</strong>.</p>\n<h2><strong>S\u2019\u00e9quiper pour travailler \u00e0 domicile</strong></h2>\n<p>Une des priorit\u00e9s sera de s\u2019assurer que vous disposez du mat\u00e9riel et de la technologie dont vous avez besoin pour faire votre travail confortablement et efficacement de la maison. La semaine prochaine, nous vous communiquerons des d\u00e9tails importants sur l\u2019\u00e9quipement auquel vous pouvez acc\u00e9der et sur la fa\u00e7on de le transporter et de r\u00e9cup\u00e9rer vos effets personnels. De plus, nous avons commenc\u00e9 \u00e0 remplacer notre ancienne plate-forme d\u2019acc\u00e8s \u00e0 distance (RPV) par un nouveau syst\u00e8me qui permettra \u00e0 tous les employ\u00e9s d\u2019optimiser leur acc\u00e8s \u00e0 distance.</p>\n<h2><strong>Retour sur nos lieux de travail</strong></h2>\n<p>Il ne faut pas oublier que certaines personnes n\u2019ont jamais cess\u00e9 de venir au bureau et qu\u2019elles doivent acc\u00e9der aux \u00e9difices d\u2019AAC pour fournir des services essentiels. En outre, nos coll\u00e8gues de la Direction g\u00e9n\u00e9rale des sciences et de la technologie (DGST) ont r\u00e9cemment repris certaines activit\u00e9s de recherche sur le terrain pour lesquelles le facteur temps est important, telles que les essais coop\u00e9ratifs, les essais de s\u00e9lection de premi\u00e8re g\u00e9n\u00e9ration, les essais agronomiques, les \u00e9tudes de lutte int\u00e9gr\u00e9e et l\u2019\u00e9chantillonnage de l\u2019environnement.</p>\n<p><strong>La sant\u00e9 et la s\u00e9curit\u00e9 de nos employ\u00e9s restent primordiales pour la suite des choses. </strong>Nous \u00e9laborons des plans et des lignes directrices pour l\u2019ensemble du Minist\u00e8re et chaque lieu de travail en fonction des directives de sant\u00e9 publique, des circonstances locales et des trois facteurs cl\u00e9s suivants :</p>\n<ul>\n<li><strong>Lieu de travail physique :</strong> Nous avons 55 lieux de travail r\u00e9partis dans tout le pays. Bien que chacun soit diff\u00e9rent, nous devons appliquer les m\u00eames protocoles en mati\u00e8re de sant\u00e9 et de s\u00e9curit\u00e9 au travail.</li>\n<li><strong>Priorit\u00e9s op\u00e9rationnelles :</strong> Compte tenu des priorit\u00e9s du Minist\u00e8re et de chaque direction g\u00e9n\u00e9rale, nous devons d\u00e9terminer quelles fonctions sont plus efficacement ex\u00e9cut\u00e9es sur chacun des lieux de travail, et quelles activit\u00e9s peuvent \u00eatre effectu\u00e9es \u00e0 distance.</li>\n<li><strong>Situation personnelle :</strong> Chaque employ\u00e9 doit composer avec une r\u00e9alit\u00e9 diff\u00e9rente. Il sera important d\u2019en tenir compte dans nos plans.</li>\n</ul>\n<h2><strong>Les d\u00e9fis tr\u00e8s r\u00e9els de l\u2019isolement social</strong></h2>\n<p>La semaine derni\u00e8re, les employ\u00e9s nous ont rappel\u00e9 que le manque d\u2019interaction sociale en personne et le sentiment d\u2019isolement en font souffrir plus d\u2019un. Il faut en \u00eatre conscients et faire un effort suppl\u00e9mentaire pour prendre des nouvelles de vos coll\u00e8gues. Vous pouvez \u00e9galement consulter la page <a href=\"http://multimedia.agr.gc.ca/COVID-19/goodnews-bonnesnouvelles-fra.html\">Les bonnes nouvelles sont dans le pr\u00e9</a> sur notre <a href=\"http://multimedia.agr.gc.ca/COVID-19/index-fra.html\">site Web consacr\u00e9 \u00e0 la COVID-19 </a>pour voir comment vos coll\u00e8gues d\u2019AAC se tiennent occup\u00e9s.</p>\n<p>Surtout, prenez soin de vous et de vos proches.</p>\n<p>\u00a0</p>\n<p><strong>Chris Forbes<br></strong>Sous\u2011ministre</p>\n<p><strong>Annette Gibbons<br></strong>Sous\u2011ministre d\u00e9l\u00e9gu\u00e9e</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Working Together: Charting the New Way Forward",
        "fr": "Ensemble, tra\u00e7ons la nouvelle voie \u00e0 suivre"
    },
    "news": {
        "date_posted": {
            "en": "2020-05-29",
            "fr": "2020-05-29"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Chris Forbes, Deputy Minister, and Annette Gibbons, Associate Deputy Minister",
            "fr": "Chris Forbes, Sous-ministre, et Annette Gibbons, Sous-ministre d\u00e9l\u00e9gu\u00e9e"
        }
    }
}