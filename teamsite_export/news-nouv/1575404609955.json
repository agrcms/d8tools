{
    "dcr_id": "1575404609955",
    "lang": "en",
    "title": {
        "en": "Collective Agreement Implementation: Highlights of New Maternity and Parental Provisions ",
        "fr": "Mise en \u0153uvre des conventions collectives : points saillants des nouvelles dispositions relatives au cong\u00e9 de maternit\u00e9 et au cong\u00e9 parental  "
    },
    "modified": "2019-12-05 00:00:00.0",
    "issued": "2019-12-05 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-12-04",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-12-05",
            "fr": "2019-12-05"
        },
        "modified": {
            "en": "2019-12-05",
            "fr": "2019-12-05"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Collective Agreement Implementation: Highlights of New Maternity and Parental Provisions ",
            "fr": "Mise en \u0153uvre des conventions collectives : points saillants des nouvelles dispositions relatives au cong\u00e9 de maternit\u00e9 et au cong\u00e9 parental  "
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "More flexible parental benefit options are available to employees whose bargaining agents have recently signed collective agreements in 2019.",
            "fr": "Des options de prestations parentales plus souples s'offrent aux employ\u00e9s dont les agents n\u00e9gociateurs ont r\u00e9cemment sign\u00e9 des ententes collectives en 2019."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>More flexible parental benefit options are available to employees whose bargaining agents have recently signed collective agreements in 2019. A general overview of the changes regarding maternity and parental provisions follows. Employees should always refer to their specific collective agreement or consult their union representative to confirm provisions that pertain to them.</p>\n<h2><strong>Currently impacted employees in AAFC</strong></h2>\n<table cellspacing=\"0\" border=\"1\">\n<tbody>\n<tr>\n<td style=\"width: 252.35pt;\">\n<p>Audit, Commerce and Purchasing (AV)<br>\n\t\t\tIncludes groups: CO, PG<br>\n\t\t\tSigning date: August 30, 2019</p>\n</td>\n<td style=\"border-color: windowtext windowtext windowtext #000000; width: 252.4pt;\">\n<p>Economics and Social Sciences (EC)</p>\n<p>Signing date: August 28, 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 252.35pt;\">\n<p>Electronics (EL)</p>\n<p>Signing date: August 23, 2019</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 252.4pt;\">\n<p>Financial Management (FI)</p>\n<p>Signing date: August 1, 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 252.35pt;\">\n<p>Architecture, Engineering and Land Survey (NR)<br>\n\t\t\tIncludes EN group</p>\n<p>Signing date: August 30, 2019</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 252.4pt;\">\n<p>Research (RE)<br>\n\t\t\tIncludes SE group</p>\n<p>Signing date: August 30, 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 252.35pt;\">\n<p>Health Services (SH)<br>\n\t\t\tIncludes VM group</p>\n<p>Signing date: August 30, 2019</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 252.4pt;\">\n<p>Applied Science and Patent Examination (SP)<br>\n\t\t\tIncludes groups: AC, AG, BI, CH, PC</p>\n<p>\u00a0</p>\n<p>Signing date: August 30, 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 252.35pt;\">\n<p>Unrepresented members of the Personnel Administration (PE) groups</p>\n<p>Signing date: August 28, 2019</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 252.4pt;\">\n<p>Excluded groups that follow the terms of these agreements.</p>\n</td>\n</tr>\n</tbody>\n</table>\n<h2><strong>Key highlights</strong></h2>\n<p>Employees who fall under groups listed above and who are eligible for maternity, parental, paternity and adoption benefits program through the <a href=\"https://www.canada.ca/en/services/benefits/ei/ei-maternity-parental.html\">Employment Insurance (EI) Plan</a> or the <a href=\"https://www.rqap.gouv.qc.ca/en/home\">Quebec Parental Insurance Plan</a> (QPIP) are entitled to the new flexibilities:</p>\n<ul>\n<li>Two options for parental leave without pay, including an option to receive reduced benefits over a longer period of time.</li>\n<li>Standard or extended parental allowance (top-up), including an amended reimbursement formula for extended parental allowance*</li>\n<li>Extra weeks of benefits when parental benefits are shared</li>\n<li>Greater mobility for employees. Employees can now return from maternity or parental leave to any organization listed in Schedules I, IV or V of the <a href=\"https://laws-lois.justice.gc.ca/eng/acts/F-11/\"><em>Financial Administration Act</em></a>.</li>\n</ul>\n<p>*Note: the QPIP does not provide for extended parental benefits. Therefore, the option to take the extended parental allowance (top-up) is only available to those employees receiving Employment Insurance (EI). Employees who fall under the QPIP may request the extended parental leave without pay provisions of their collective agreement.</p>\n<h2><strong>Effective dates of changes</strong></h2>\n<p>\nThe new leave without pay and mobility provisions are in effect from the date of signing of the collective agreement (see above). All other changes are in effect as of November 18, 2019.</p>\n<h2><strong>More information and contacts</strong></h2>\n<ul>\n<li><a href=\"https://www.gcpedia.gc.ca/gcwiki/images/3/3c/2018_Round_Collective_Agreement_and_Salary_Rates_Update_2019_tentative_schedule.xlsx\">Tentative schedule</a> (Excel) of signing and other relevant dates for your collective agreement</li>\n<li><a href=\"https://laws-lois.justice.gc.ca/eng/acts/F-11/\">Organizations</a> to which employees on leave without pay may return. See Schedules I, IV and V</li>\n<li>Consult these Questions and Answers for details on the updated <a href=\"http://intranet.agr.gc.ca/agrisource/eng/human-resources/pay-benefits-and-phoenix/implementation-of-2018-collective-agreements-quick-facts-and-faqs?id=1571920077948#a\">maternity and parental leave provisions</a></li>\n<li><a href=\"https://intranet.canada.ca/ppb-rpa/cah-pscc/cah-pscc-eng.asp\">Highlights of recently signed agreements</a>, including new rates of pay and other provisions</li>\n</ul>\n<p>Please refer questions about <strong>collective agreements</strong> to your union representative, or speak with your manager. If you are a manager and have questions about collective agreements, please contact your Labour Relations Advisor.</p>\n<p>For questions about pay, employees are encouraged to phone the <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/cn-cu-eng.html\">Client Contact Centre</a> or submit a <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/systeme-paye-employes-pay-system-employees/retroaction-phenix-phoenix-feedback-eng.html\">Phoenix feedback form</a> online. For any general human resource enquiries, contact the<strong> HR Centre </strong>at 1-855-545-9575<strong> </strong>or<strong> </strong><a href=\"mailto:aafc.hrcentrerh.aac@canada.ca\">aafc.hrcentrerh.aac@canada.ca</a>.</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Des options de prestations parentales plus souples s\u2019offrent aux employ\u00e9s dont les agents n\u00e9gociateurs ont r\u00e9cemment sign\u00e9 des ententes collectives en 2019. Vous trouverez ci-apr\u00e8s un aper\u00e7u des changements apport\u00e9s aux dispositions relatives au cong\u00e9 de maternit\u00e9 et au cong\u00e9 parental. Les employ\u00e9s doivent toujours consulter leur convention collective ou leur repr\u00e9sentant syndical pour confirmer les dispositions qui s\u2019appliquent \u00e0 eux.</p>\n<h2><strong>Employ\u00e9s d\u2019AAC actuellement touch\u00e9s </strong></h2>\n<table cellspacing=\"0\" border=\"1\">\n<tbody>\n<tr>\n<td style=\"width: 252.35pt;\">\n<p>V\u00e9rification, commerce et achat (AV)<br>\n\t\t\tComprend les groupes CO, PG<br>\n\t\t\tDate de la signature : 30 ao\u00fbt 2019</p>\n</td>\n<td style=\"border-color: windowtext windowtext windowtext #000000; width: 252.4pt;\">\n<p>\u00c9conomique et services de sciences sociales (EC)</p>\n<p>Date de la signature : 28 ao\u00fbt 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 252.35pt;\">\n<p>\u00c9lectronique (EL)</p>\n<p>Date de la signature : 23 ao\u00fbt 2019</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 252.4pt;\">\n<p>Gestion financi\u00e8re (FI)</p>\n<p>Date de la signature : 1er ao\u00fbt 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 252.35pt;\">\n<p>Architecture, g\u00e9nie et arpentage (NR)<br>\n\t\t\tComprend le groupe EN</p>\n<p>Date de la signature : 30 ao\u00fbt 2019</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 252.4pt;\">\n<p>Recherche (RE)<br>\n\t\t\tComprend le groupe SE</p>\n<p>Date de la signature : 30 ao\u00fbt 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 252.35pt;\">\n<p>Services de sant\u00e9 (SH)<br>\n\t\t\tComprend le groupe VM</p>\n<p>Date de la signature : 30 ao\u00fbt 2019</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 252.4pt;\">\n<p>Sciences appliqu\u00e9es et examen des brevets (SP)<br>\n\t\t\tComprend les groupes AC, AG, BI, CH, PC</p>\n<p>\u00a0</p>\n<p>Date de la signature : 30 ao\u00fbt 2019</p>\n</td>\n</tr>\n<tr>\n<td style=\"border-color: #000000 windowtext windowtext; width: 252.35pt;\">\n<p>Employ\u00e9s non repr\u00e9sent\u00e9s du groupe Gestion du personnel (PE)</p>\n<p>Date de la signature : 28 ao\u00fbt 2019</p>\n</td>\n<td style=\"border-color: #000000 windowtext windowtext #000000; width: 252.4pt;\">\n<p>Les groupes exclus qui respectent les conditions de ces conventions.</p>\n</td>\n</tr>\n</tbody>\n</table>\n<h2><strong>Points saillants</strong></h2>\n<p>Les employ\u00e9s qui font partie des groupes susmentionn\u00e9s et qui ont droit \u00e0 des prestations de maternit\u00e9, \u00e0 des prestations parentales, \u00e0 des prestations de paternit\u00e9 et \u00e0 des prestations d\u2019adoption dans le cadre du <a href=\"https://www.canada.ca/fr/services/prestations/ae/assurance-emploi-maternite-parentales.html\">r\u00e9gime de l'assurance-emploi (AE)</a> ou du <a href=\"https://www.rqap.gouv.qc.ca/fr\">R\u00e9gime qu\u00e9b\u00e9cois d'assurance parentale (RQAP)</a> peuvent b\u00e9n\u00e9ficier des nouvelles options :</p>\n<ul>\n<li>Deux options pour le cong\u00e9 parental non r\u00e9mun\u00e9r\u00e9, y compris l\u2019option de recevoir des prestations r\u00e9duites sur une plus longue p\u00e9riode;</li>\n<li>Des prestations parentales standards ou prolong\u00e9es (suppl\u00e9ment), y compris une formule de remboursement modifi\u00e9e pour les prestations parentales prolong\u00e9es*;</li>\n<li>Des semaines additionnelles de prestations lorsque les prestations parentales sont partag\u00e9es;</li>\n<li>Une mobilit\u00e9 accrue \u2013 les employ\u00e9s peuvent maintenant revenir d\u2019un cong\u00e9 de maternit\u00e9 ou d\u2019un cong\u00e9 parental pour n\u2019importe laquelle des organisations r\u00e9pertori\u00e9es aux annexes I, IV et V de la <a href=\"https://laws-lois.justice.gc.ca/fra/lois/F-11/\"><em>Loi sur la gestion des finances publiques</em></a>.</li>\n</ul>\n<p>*Nota : Le RQAP n\u2019offre pas de prestations parentales prolong\u00e9es. Par cons\u00e9quent, seuls les employ\u00e9s qui re\u00e7oivent des prestations d\u2019AE ont l\u2019option de prendre des prestations parentales prolong\u00e9es (suppl\u00e9ment). Toutefois, les employ\u00e9s qui b\u00e9n\u00e9ficient du RQAP, peuvent demander l\u2019application des dispositions de leur convention collective pour le cong\u00e9 parental prolong\u00e9 non r\u00e9mun\u00e9r\u00e9.</p>\n<h2><strong>Dates d\u2019entr\u00e9e en vigueur des changements </strong></h2>\n<p>Les nouvelles dispositions relatives au cong\u00e9 non r\u00e9mun\u00e9r\u00e9 et les modifications apport\u00e9es \u00e0 la liste des organisations o\u00f9 les employ\u00e9s en cong\u00e9 de maternit\u00e9 ou cong\u00e9 parental non r\u00e9mun\u00e9r\u00e9 peuvent revenir apr\u00e8s leur cong\u00e9 entrent en vigueur le jour de la signature de la convention collective (voir ci-dessus). Tous les autres changements entrent en vigueur le 18 novembre 2019.</p>\n<p><strong>Renseignements suppl\u00e9mentaires et personnes-ressources</strong></p>\n<ul>\n<li><a href=\"https://www.gcpedia.gc.ca/gcwiki/images/0/0c/Calendrier_provisoire_de_mise_en_%C5%93uvre_des_conventions_collectives_de_2018_et_de_mise_%C3%A0_jour_des_taux_de_r%C3%A9m.xlsx\">Calendrier provisoire</a> des dates de signature des conventions collectives et des autres dates pertinentes.</li>\n<li><a href=\"https://laws-lois.justice.gc.ca/fra/lois/F-11/\">Organisations</a> o\u00f9 les employ\u00e9s en cong\u00e9 non r\u00e9mun\u00e9r\u00e9 peuvent retourner (voir les annexes I, IV et V).</li>\n<li><a href=\"http://intranet.agr.gc.ca/agrisource/fra?id=1571920077948#a\">Questions et r\u00e9ponses</a> sur la mise \u00e0 jour des dispositions relatives au cong\u00e9 de maternit\u00e9 et au cong\u00e9 parental.</li>\n<li><a href=\"https://intranet.canada.ca/ppb-rpa/cah-pscc/cah-pscc-fra.asp\">Points saillants des conventions collectives r\u00e9cemment sign\u00e9es</a>, y compris les nouveaux taux de r\u00e9mun\u00e9ration et autres dispositions.</li>\n</ul>\n<p>Veuillez adresser vos questions sur les <strong>conventions collectives</strong> \u00e0 votre repr\u00e9sentant syndical ou \u00e0 votre gestionnaire. Si vous \u00eates gestionnaire et que vous avez des questions sur les conventions collectives, veuillez communiquer avec votre conseiller en relations de travail.</p>\n<p>Les employ\u00e9s qui ont des questions au sujet de la paye sont invit\u00e9s \u00e0 appeler le <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/paye-centre-pay/cn-cu-fra.html\">Centre de contact avec la client\u00e8le</a> ou \u00e0 pr\u00e9senter un <a href=\"https://www.tpsgc-pwgsc.gc.ca/remuneration-compensation/services-paye-pay-services/systeme-paye-employes-pay-system-employees/retroaction-phenix-phoenix-feedback-fra.html\">formulaire de r\u00e9troaction sur Ph\u00e9nix</a> en ligne. En ce qui concerne les demandes de renseignements g\u00e9n\u00e9rales sur les ressources humaines, veuillez communiquer le <strong>Centre des RH</strong> par t\u00e9l\u00e9phone au 1-855-545-9575<strong> </strong>ou par courriel \u00e0<strong> </strong><a href=\"mailto:aafc.hrcentrerh.aac@canada.ca\">aafc.hrcentrerh.aac@canada.ca</a>.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Collective Agreement Implementation: Highlights of New Maternity and Parental Provisions",
        "fr": "Mise en \u0153uvre des conventions collectives : points saillants des nouvelles dispositions relatives au cong\u00e9 de maternit\u00e9 et au cong\u00e9 parental"
    },
    "news": {
        "date_posted": {
            "en": "2019-12-05",
            "fr": "2019-12-05"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Corporate Management Branch",
            "fr": "Direction g\u00e9n\u00e9rale de la gestion int\u00e9gr\u00e9e"
        }
    }
}