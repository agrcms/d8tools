{
    "dcr_id": "1483719347452",
    "lang": "en",
    "title": {
        "en": "Business Value or Transitory? Proper Information Management is Important",
        "fr": "Information ayant une valeur op\u00e9rationnelle ou information transitoire? Il est important de bien g\u00e9rer l'information"
    },
    "modified": "2018-05-16 00:00:00.0",
    "issued": "2017-01-09 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": "1 column",
    "dc_date_created": "2017-01-09",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2017-01-09",
            "fr": "2017-01-09"
        },
        "modified": {
            "en": "2018-05-16",
            "fr": "2018-05-16"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Business Value or Transitory? Proper Information Management is Important",
            "fr": "Information ayant une valeur op\u00e9rationnelle ou information transitoire? Il est important de bien g\u00e9rer l'information"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Did you know that all AAFC employees are responsible for managing the information under their control and custody?",
            "fr": "Saviez-vous que les employ\u00e9s d\u2019AAC sont responsables de la gestion de l\u2019information dont ils ont la charge?"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>\n\tDid you know that all AAFC employees are responsible for managing the information under their control and custody?</p>\n<p>\n\tBusiness Value Information (BVI) documents provide evidence of AAFC\u2019s business activities and decisions. It supports the delivery of our programs and services. <abbr title=\"Business Value Information\">BVI</abbr> should be saved in a corporate repository such as Knowledge Workspace.</p>\n<p>\n\tTransitory information is information that is only required for a limited period of time in order to complete a routine action or to prepare a subsequent record. It must be deleted when no longer needed as the sheer volume of transitory information can impede AAFC\u2019s ability to find and manage <abbr title=\"Business Value Information\">BVI</abbr>.</p>\n<p>\n\tIf you have further questions about what to keep, what not to keep or what to do with <abbr title=\"Business Value Information\">BVI</abbr>\u00a0that has met its retention period and fulfilled its business need, the following sources of information are available to you and your teams:</p>\n<ul>\n<li>\n\t\tRead <a rel=\"external\" href=\"https://collab.agr.gc.ca/co/ima_sgi/Documents/Identifying_Transitory_and_Business_Value_Information.DOC\">Identifying Transitory and Business Value Information</a>\u00a0</li>\n<li>\n\t\tTake the online <b>Fundamentals of Information Management </b>training course. This course is mandatory for all AAFC employees as announced on <a href=\"http://intranet.agr.gc.ca/agrisource/eng/agriculture-magazine/newswork/new-mandatory-information-management-training?id=1476714566423\">news@work </a>on October 20, 2016. Course and registration details can be found on the <a href=\"?id=1524583364620\">Training</a> page on AgriSource.</li>\n<li>\n\t\tVisit \"<a rel=\"external\" href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/Home.aspx\">Information Management at AAFC</a>\" in Knowledge Workspace.</li>\n<li>\n\t\tContact <a href=\"mailto:IM-GI@AGR.GC.CA\">IM-GI@AGR.GC.CA</a> to request an Information Management Awareness session for your team or to ask a question related to information management.</li>\n</ul>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>\n\tSaviez-vous que les employ\u00e9s d\u2019AAC sont responsables de la gestion de l\u2019information dont ils ont la charge?</p>\n<p>\n\tL\u2019information ayant une valeur op\u00e9rationnelle (IVO) t\u00e9moigne des activit\u00e9s et des d\u00e9cisions op\u00e9rationnelles d\u2019AAC. Elle appuie la prestation de nos programmes et de nos services. L\u2019<abbr title=\"L\u2019information ayant une valeur op\u00e9rationnelle\">IVO</abbr> doit \u00eatre conserv\u00e9e dans un r\u00e9pertoire minist\u00e9riel comme l\u2019Espace de travail du savoir.</p>\n<p>\n\tL\u2019information transitoire est l\u2019information requise seulement pour une p\u00e9riode limit\u00e9e afin d\u2019accomplir une t\u00e2che courante ou de pr\u00e9parer un document. Elle doit \u00eatre supprim\u00e9e lorsqu\u2019elle n\u2019est plus utile, car l\u2019important volume d\u2019information transitoire peut\u00a0nuire \u00e0\u00a0la capacit\u00e9 d\u2019AAC de trouver et de g\u00e9rer l\u2019<abbr title=\"L\u2019information ayant une valeur op\u00e9rationnelle\">IVO</abbr>.</p>\n<p>\n\tSi vous vous posez des questions sur ce qui doit \u00eatre conserv\u00e9 ou non ou sur ce que vous devez faire de l\u2019<abbr title=\"L\u2019information ayant une valeur op\u00e9rationnelle\">IVO</abbr> qui a r\u00e9pondu aux besoins op\u00e9rationnels et dont la p\u00e9riode de conservation est expir\u00e9e, vous pouvez consulter les sources d\u2019information suivantes\u00a0:</p>\n<ul>\n<li>\n\t\tLisez le document intitul\u00e9 <a rel=\"external\" href=\"https://collab.agr.gc.ca/co/ima_sgi/Documents/Information_%C3%A9ph%C3%A9m%C3%A8re_et_renseignements_%C3%A0_valeur_op%C3%A9rationnelle.DOC\">Information \u00e9ph\u00e9m\u00e8re et renseignements \u00e0 valeur op\u00e9rationnelle</a>.</li>\n<li>\n\t\tSuivez le cours en ligne <b>Principes fondamentaux de la gestion de l'information</b>. Ce\u00a0cours est obligatoire pour tous les employ\u00e9s d\u2019AAC, comme l\u2019indiquait l\u2019annonce faite dans les <a href=\"http://intranet.agr.gc.ca/agrisource/fra/magazine-agriculture/nouvelleslouvrage/nouvelle-formation-obligatoire-sur-la-gestion-de-linformation?id=1476714566423\">nouvelles@l'ouvrage</a> le 20 octobre 2016. Vous trouverez de l\u2019information sur le cours et l\u2019inscription dans la page <a href=\"?id=1524583364620\">Formation</a> d\u2019AgriSource.</li>\n<li>\n\t\tVisitez la page\u00a0<a rel=\"external\" href=\"https://collab.agr.gc.ca/co/ima_sgi/SitePages/Home.aspx\">Gestion de l'information\u00a0\u00e0 AAC</a>\u00a0dans l\u2019Espace de travail du savoir.</li>\n<li>\nEnvoyez un courriel \u00e0 <a href=\"mailto:IM-GI@AGR.GC.CA\">IM-GI@AGR.GC.CA</a> pour demander une s\u00e9ance de sensibilisation sur la gestion de l\u2019information pour votre \u00e9quipe ou pour poser une question sur la gestion de l\u2019information.\u00a0</li>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Business Value or Transitory? Proper Information Management is Important",
        "fr": "Information ayant une valeur op\u00e9rationnelle ou information transitoire? Il est important de bien g\u00e9r"
    },
    "news": {
        "date_posted": {
            "en": "2017-01-09",
            "fr": "2017-01-09"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Information Management Program Team",
            "fr": "\u00c9quipe du Programme de gestion de l'information"
        }
    }
}