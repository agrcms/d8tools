{
    "dcr_id": "1603728586034",
    "lang": "en",
    "title": {
        "en": "Security Tips for Working Remotely",
        "fr": "Conseils de s\u00e9curit\u00e9 pour le t\u00e9l\u00e9travail"
    },
    "modified": "2020-10-29 00:00:00.0",
    "issued": "2020-10-28 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2020-10-26",
    "breadcrumb_label": "",
    "trigger_date": "2020-10-29 11:21:23.0",
    "meta": {
        "issued": {
            "en": "2020-10-28",
            "fr": "2020-10-28"
        },
        "modified": {
            "en": "2020-10-29",
            "fr": "2020-10-29"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Security Tips for Working Remotely",
            "fr": "Conseils de s\u00e9curit\u00e9 pour le t\u00e9l\u00e9travail"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Working remotely can introduce challenges when trying to balance functionality with security.",
            "fr": "Le t\u00e9l\u00e9travail peut poser des difficult\u00e9s quand nous essayons de concilier fonctionnalit\u00e9 et s\u00e9curit\u00e9."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>As most AAFC employees are now working remotely, we remind you of the importance of security at home and our responsibilities as public servants. Working remotely can introduce challenges when trying to balance functionality with security.</p>\n<ol>\n<li><strong>Home Office<strong>:</strong></strong> Be mindful of the work conversations you have by phone and computer within hearing distance of others.\u00a0</li>\n<li><strong>Protect Your AAFC Devices: </strong>Keep your mobile devices in a safe place and ensure the screens are locked when not in use. Millions of mobile devices are lost or stolen each year, Only by protecting them with a password will they be inaccessible to others if lost or stolen. Computer screens should also be locked when not in use, and screens should not be visible to people outside the home through windows or doors.\n\t</li>\n<li><strong>Passwords: </strong>Be sure to use different passwords for all of your accounts. Separate passwords for each account make your accounts less vulnerable to cyber attacks. Analysis of recent attacks shows that using the same password or username for multiple accounts makes it much easier for hackers to gain access to your accounts. Keep your list of passwords safe. Never put it on a sticky note for example, affixed to the side of your computer at home.</li>\n<li><strong>Sensitive and protected information: </strong>If you are working on sensitive or protected information, you are responsible for safeguarding it. Pay attention to security markings, which are typically found at the beginning of an email or in the top-right corner of a document. You should not have printed copies of Protected documents in your home and ensure that you always <a href=\"https://collab.agr.gc.ca/co/dss-ssm/Documents/PROCEDURES-GUIDELINES-TEMPLATES/File%20and%20Email%20Encryption%20Instructions%20-%20EN.DOCX\">encrypt your Protected B emails</a>\u00a0(Word). To confirm the security and proper handling of sensitive information, refer to <a href=\"https://collab.agr.gc.ca/co/dss-ssm/Documents/SECURITY%20AWARENESS%20TRAINING/ABC%20and%20Secret%20-%20EN.docx\">The ABCs of Protected Information</a><em>\u00a0</em>(Word). </li>\n<li><strong>Social Media: </strong>Hackers constantly watch social media sites for information they can leverage to gain access to your digital system. Rethink the amount of personal information you expose to the public.\n\t</li>\n<li><strong>Security Incident:</strong> All <a href=\"https://collab.agr.gc.ca/co/dss-ssm/Documents/POLICY-DIRECTIVE-STANDARD/Type%20of%20Security%20Incidents%20-%20ENG.docx?d=wce1e9519d28d47d497f4a4ab8416d574\">security incidents</a> (Word) must be brought to the immediate attention of Departmental Security Services (DSS). Do your duty and report <a name=\"INCIDENT\"></a><a name=\"BookmarkChannel\"></a><a name=\"ChannelIncident\">security incidents</a> to the Security Investigations section\u00a0(<a href=\"mailto:aafc.securityincident-incidentsecurite.aac@canada.ca\">aafc.securityincident-incidentsecurite.aac@canada.ca</a>), or the Office of Internal Disclosure\u00a0at <a href=\"mailto:aafc.internaldisclosure-securitedivulgationinterne.aac@canada.ca\">aafc.internaldisclosure-securitedivulgationinterne.aac@canada.ca</a>\u00a0or 1-613-773-2500.</li>\n<li><strong>Training: </strong>Ensure you take the mandatory security training:\n<ul>\n<li>AAFC's <a href=\"http://intranet.agr.gc.ca/agrisource/eng/branches-and-offices/corporate-management-branch/services/security-awareness-training?id=1325695839878\">AGR-620 Security Awareness</a></li>\n<li><a href=\"http://intranet.agr.gc.ca/agrisource/eng/human-resources/learning-and-development/training/employees/security-awareness-a230?id=1523285530838\">A230 Security Course</a> from the Canada School of Public Service</li>\n</ul>\n</li>\n</ol>\n<p>If you have any questions or concerns, please contact DSS at any time at <a href=\"mailto:aafc.securitytraining-securiteformation.aac@canada.ca\">aafc.securitytraining-securiteformation.aac@canada.ca</a>.</p>\n<p>Security: Strength through collaboration.</p>\n<p>\u00a0</p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Comme la plupart d\u2019entre nous travaillent maintenant \u00e0 distance,\u00a0nous vous rappelons\u00a0l\u2019importance de prendre des mesures de s\u00e9curit\u00e9 \u00e0 la maison et les responsabilit\u00e9s que nous avons en tant que fonctionnaires. Le t\u00e9l\u00e9travail peut poser des difficult\u00e9s quand nous essayons de concilier fonctionnalit\u00e9\u00a0et s\u00e9curit\u00e9.</p>\n<ol>\n<li><strong>Bureau \u00e0 la maison :</strong> Soyez conscients des conversations professionnelles que vous avez au t\u00e9l\u00e9phone ou en ligne qui peuvent \u00eatre entendues par les personnes dans votre environnement imm\u00e9diat.</li>\n<li><strong>Protection de vos appareils d\u2019AAC : </strong>Gardez vos appareils mobiles dans un endroit s\u00fbr lorsqu\u2019ils ne sont pas utilis\u00e9s et assurez-vous que les \u00e9crans sont verrouill\u00e9s. Chaque ann\u00e9e, des millions d\u2019appareils mobiles sont perdus ou vol\u00e9s, mais s\u2019ils sont prot\u00e9g\u00e9s par un mot de passe, ils sont inutilisables par autrui. Les \u00e9crans d\u2019ordinateur doivent aussi \u00eatre verrouill\u00e9s quand ils ne sont pas utilis\u00e9s et ne pas \u00eatre \u00e0 la vue de personnes \u00e0 l\u2019ext\u00e9rieur de votre maison (par une fen\u00eatre ou une porte).</li>\n<li><strong>Mots de passe : </strong>Assurez-vous d\u2019utiliser des mots de passe distincts pour tous vos comptes afin de les rendre moins vuln\u00e9rables. Selon une analyse de r\u00e9centes cyberattaques, le fait d\u2019utiliser le m\u00eame mot de passe ou le m\u00eame nom d\u2019utilisateur pour plusieurs comptes facilite la t\u00e2che aux pirates informatiques. Gardez votre liste de mots de passe en lieu s\u00fbr. Par exemple, ne la r\u00e9digez jamais sur un feuillet autoadh\u00e9sif que vous collez sur le c\u00f4t\u00e9 de votre ordinateur \u00e0 la maison.</li>\n<li><strong>Renseignements de nature d\u00e9licate et prot\u00e9g\u00e9s : </strong>Si vous travaillez avec de l\u2019information de nature d\u00e9licate ou prot\u00e9g\u00e9e, vous avez la responsabilit\u00e9 de la prot\u00e9ger. Pr\u00eatez attention aux cotes de s\u00e9curit\u00e9 qui se trouvent habituellement au d\u00e9but d\u2019un courriel ou dans le coin sup\u00e9rieur droit d\u2019un document. Vous ne devez pas garder de copies imprim\u00e9es de documents prot\u00e9g\u00e9s \u00e0 la maison et vous devez vous assurer de toujours <a href=\"https://collab.agr.gc.ca/co/dss-ssm/Documents/PROCEDURES-GUIDELINES-TEMPLATES/File%20and%20Email%20Encryption%20Instructions%20-%20FR.DOCX?d=w5ee9c54cf63d4b9fbf166c25d783094d%20\">chiffrer vos courriels Prot\u00e9g\u00e9 B</a> (Word). Pour v\u00e9rifier que vous traitez l\u2019information prot\u00e9g\u00e9e de mani\u00e8re s\u00e9curitaire et appropri\u00e9e, consultez <a href=\"https://collab.agr.gc.ca/co/dss-ssm/Documents/SECURITY%20AWARENESS%20TRAINING/ABC%20and%20Secret%20-%20FR.docx?d=wed63b0463e834d9f9f9497a4335037b9\">Les ABC des renseignements prot\u00e9g\u00e9s</a>\u00a0(Word).</li>\n<li><b>M\u00e9dias sociaux :</b> Les pirates informatiques surveillent constamment les sites de m\u00e9dias sociaux afin de recueillir des renseignements dont ils peuvent se servir pour acc\u00e9der \u00e0 votre syst\u00e8me num\u00e9rique. R\u00e9duisez la quantit\u00e9 de renseignements personnels que vous divulguez.</li>\n<li>Tous les <a href=\"https://collab.agr.gc.ca/co/dss-ssm/Documents/PROCEDURES-GUIDELINES-TEMPLATES/Type%20of%20Security%20Incidents%20-%20FR.docx?d=w153263015863466788e13aac1065b7a5\">incidents de s\u00e9curit\u00e9</a><span style='line-height: 107%; font-family: \"Times New Roman\",serif; font-size: 12pt; mso-fareast-font-family: \"Times New Roman\"; mso-ansi-language: FR; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;' lang=\"EN\"> </span>(Word) doivent \u00eatre imm\u00e9diatement port\u00e9s \u00e0 l\u2019attention des Services de s\u00e9curit\u00e9 minist\u00e9riels (SSM). Accomplissez votre devoir et signalez les incidents de s\u00e9curit\u00e9 \u00e0 la Section des enqu\u00eates relatives \u00e0 la s\u00e9curit\u00e9 (<a href=\"mailto:aafc.securityincident-incidentsecurite.aac@canada.ca\">aafc.securityincident-incidentsecurite.aac@canada.ca</a>) ou au Bureau de divulgation interne par courriel \u00e0 <a href=\"mailto:aafc.internaldisclosure-securitedivulgationinterne.aac@canada.ca\">aafc.internaldisclosure-securitedivulgationinterne.aac@canada.ca</a>\u00a0ou par t\u00e9l\u00e9phone au 1-613-773-2500.</li>\n<li><strong>Formation :</strong> Assurez-vous de suivre la formation obligatoire sur la s\u00e9curit\u00e9 :</li>\n</ol>\n<ul>\n<li><a href=\"http://intranet.agr.gc.ca/agrisource/fra/ressources-humaines/apprentissage-et-perfectionnement/formation/employes/sensibilisation-a-la-securite-agr-620?id=1325695839878\">Sensibilisation \u00e0 la s\u00e9curit\u00e9 (AGR-620)</a> d\u2019AAC\n</li>\n<li><a href=\"http://intranet.agr.gc.ca/agrisource/fra/ressources-humaines/apprentissage-et-perfectionnement/formation/employes/sensibilisation-a-la-securite-a230?id=1523285530838\">Sensibilisation \u00e0 la s\u00e9curit\u00e9 (A230)</a> de l\u2019\u00c9cole de la fonction publique du Canada\n</li>\n</ul>\n<p>\nSi vous avez des questions ou des pr\u00e9occupations, vous pouvez communiquer avec les SSM en tout temps \u00e0 <a href=\"mailto:aafc.securitytraining-securiteformation.aac@canada.ca\">aafc.securitytraining-securiteformation.aac@canada.ca</a>.</p>\n<p>S\u00e9curit\u00e9 : La collaboration fait notre force.</p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Security Tips for Working Remotely",
        "fr": "Conseils de s\u00e9curit\u00e9 pour le t\u00e9l\u00e9travail"
    },
    "news": {
        "date_posted": {
            "en": "2020-10-29",
            "fr": "2020-10-29"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Christine Walker, Assistant Deputy Minister and Chief Financial Officer, Corporate Management Branch",
            "fr": "Christine Walker, sous-ministre adjointe et dirigeante principale des finances, Direction g\u00e9n\u00e9rale de la gestion int\u00e9gr\u00e9e"
        }
    }
}