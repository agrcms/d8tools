{
    "dcr_id": "1574788128452",
    "lang": "en",
    "title": {
        "en": "Raising Awareness of Gender-Based Violence - November 25 to December 10",
        "fr": "Savoir reconna\u00eetre la violence sexiste - Du 25 novembre au 10 d\u00e9cembre"
    },
    "modified": "2019-11-28 00:00:00.0",
    "issued": "2019-11-28 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-11-26",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-11-28",
            "fr": "2019-11-28"
        },
        "modified": {
            "en": "2019-11-28",
            "fr": "2019-11-28"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Raising Awareness of Gender-Based Violence - November 25 to December 10",
            "fr": "Savoir reconna\u00eetre la violence sexiste - Du 25 novembre au 10 d\u00e9cembre"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "November and December are important months for commemorating and raising awareness of gender-based violence in Canada and around the world.",
            "fr": "Novembre et d\u00e9cembre sont des mois importants pour souligner le probl\u00e8me de la violence sexiste au Canada et \u00e0 l'\u00e9tranger et en prendre conscience."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>November and December are important months for commemorating and raising awareness of gender-based violence in Canada and around the world. Gender-based violence (GBV) involves the use and abuse of power and control over another person and is perpetrated against someone based on their gender identity, gender expression or perceived gender. Violence against women and girls is one form of GBV. It also has a disproportionate impact on LGBTQ2+ and gender non-conforming people.</p>\n<p>These commemorations provide an opportunity for Canadians to reflect on the phenomenon of GBV in our society. It is also an opportunity to consider those for whom violence is a daily reality, to remember those who have died as a result of GBV, and to consider concrete actions to eliminate all forms of violence.</p>\n<h2><strong>16 Days Against Gender Violence \u2013 November 25 to December 10</strong></h2>\n<p>From November 25, the International Day for the Elimination of Violence against Women, to December 10, Human Rights Day, the <a href=\"https://cfc-swc.gc.ca/commemoration/vaw-vff/index-en.html\">16 Days of Activism Against Gender-Based Violence Campaign</a> is a time to galvanize action to end GBV around the world.</p>\n<h2><strong>National Day of Remembrance and Action on Violence Against Women in Canada \u2013 December 6</strong></h2>\n<p>December 6 is the <a href=\"http://www.swc-cfc.gc.ca/commemoration/vaw-vff/remembrance-commemoration-en.html\">National Day of Remembrance and Action on Violence Against Women in Canada</a>, which was established in 1991 by the Parliament of Canada to mark the anniversary of the murders in 1989 of 14 young women at l'\u00c9cole Polytechnique de Montr\u00e9al.</p>\n<h2><strong>What can you do?</strong></h2>\n<ul>\n<li>Become an ally in the efforts to end GBV:\n\t\n<ul style=\"list-style-type: circle;\">\n<li>Listen \u2013 be open to learning from the experiences of others</li>\n<li>Believe \u2013 support survivors and those affected by violence</li>\n<li>Speak out \u2013 add your voice to call out violence</li>\n<li>Intervene \u2013 find a safe way to help when you see acts of GBV</li>\n</ul>\n</li>\n<li>Get involved, using <a href=\"http://www.swc-cfc.gc.ca/commemoration/vaw-vff/kit-trousse-en.html\">resources from Women and Gender Equality Canada</a> and from other organizations whose goal is to reduce violence targeting Indigenous women and children.</li>\n<li>Share how you are taking action to end GBV by using the hashtag #OurActionsMatter on social media.</li>\n</ul>\n<p>If you are affected by GBV and require confidential support and guidance, the Employee and Family Assistance Program (EFAP) is available to you and your family. The services are accessible 24/7 by phone at 1-844-880-9143 or online at <a href=\"https://www.workhealthlife.com/\">workhealthlife.com</a>. Local crisis support is also available across Canada. To find a distress centre near you, visit <a href=\"https://www.crisisservicescanada.ca/en/looking-for-local-resources-support/\">Crisis Services Canada\u2019s website</a>.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/16Days2019-EN.png\" class=\"center-block\"></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Novembre et d\u00e9cembre sont des mois importants pour souligner le probl\u00e8me de la violence sexiste au Canada et \u00e0 l\u2019\u00e9tranger et en prendre conscience. La violence fond\u00e9e sur le sexe (VFS) est une forme de violence caract\u00e9ris\u00e9e par un abus de pouvoir ou d\u2019autorit\u00e9 contre une personne en raison de son identit\u00e9 sexuelle (identit\u00e9 de genre), av\u00e9r\u00e9e ou pr\u00e9sum\u00e9e, et de l\u2019expression de cette identit\u00e9. La violence faite aux femmes et aux filles est une forme de VFS. De plus, elle touche de fa\u00e7on disproportionn\u00e9e les personnes allosexuelles et non conformes dans le genre.</p>\n<p>Les activit\u00e9s comm\u00e9moratives donnent aux Canadiens l\u2019occasion de r\u00e9fl\u00e9chir au ph\u00e9nom\u00e8ne de la violence sexiste dans notre soci\u00e9t\u00e9. Elles nous donnent aussi la possibilit\u00e9 de penser aux personnes pour qui la violence est une r\u00e9alit\u00e9 quotidienne, de rendre hommage \u00e0 celles qui en sont mortes et d\u2019envisager des mesures concr\u00e8tes pour \u00e9liminer toute forme de violence.</p>\n<h2><strong>Les 16 jours contre la violence fond\u00e9e sur le sexe \u2013 du 25 novembre au 10 d\u00e9cembre</strong></h2>\n<p>Les <a href=\"https://cfc-swc.gc.ca/commemoration/vaw-vff/index-fr.html\">16 journ\u00e9es d\u2019activisme contre la violence bas\u00e9e sur le genre</a> qui auront lieu du 25 novembre, Journ\u00e9e internationale pour l\u2019\u00e9limination de la violence \u00e0 l\u2019\u00e9gard des femmes, au 10 d\u00e9cembre, Journ\u00e9e des droits de l\u2019homme, encouragent les gens \u00e0 se mobiliser pour mettre un terme \u00e0 la violence sexiste \u00e0 l\u2019\u00e9chelle mondiale.</p>\n<h2><strong>Journ\u00e9e nationale de comm\u00e9moration et d\u2019action contre la violence faite aux femmes \u2013 6 d\u00e9cembre</strong></h2>\n<p>Le 6 d\u00e9cembre est la <a href=\"http://www.swc-cfc.gc.ca/commemoration/vaw-vff/remembrance-commemoration-fr.html\">Journ\u00e9e nationale de comm\u00e9moration et d\u2019action contre la violence faite aux femmes au Canada</a>. Institu\u00e9e en 1991 par le Parlement du Canada, cette journ\u00e9e souligne l\u2019anniversaire du meurtre de 14 jeunes femmes \u00e0 l\u2019\u00c9cole polytechnique de Montr\u00e9al en 1989.</p>\n<h2><strong>Que pouvez-vous faire?</strong></h2>\n<ul>\n<li>Devenez un alli\u00e9 dans nos efforts pour lutter contre la VFS :\n\t\n<ul style=\"list-style-type: circle;\">\n<li>\u00c9coutez - soyez ouvert \u00e0 apprendre des exp\u00e9riences des autres.</li>\n<li>Croyez - soutenez les survivants et les personnes touch\u00e9es par la violence.</li>\n<li>Parlez - ajoutez votre voix pour d\u00e9noncer la violence.</li>\n<li>Intervenez - trouvez un moyen s\u00fbr d\u2019aider lorsque vous voyez des actes de VFS.</li>\n</ul>\n</li>\n<li>Engagez\u2011vous en vous inspirant des <a href=\"http://www.swc-cfc.gc.ca/commemoration/vaw-vff/kit-trousse-fr.html\">ressources</a> de Femmes et \u00c9galit\u00e9 des genres Canada et d\u2019autres organisations ayant pour objectif de r\u00e9duire la violence ciblant les femmes et les enfants autochtones.</li>\n<li>Dites comment vous \u00eates pass\u00e9 \u00e0 l\u2019action dans la lutte contre la VFS en utilisant le mot\u2011clic #MesGestesComptent sur les m\u00e9dias sociaux.</li>\n</ul>\n<p>Si vous subissez de la VFS et que vous avez besoin d\u2019une aide et de conseils confidentiels, le Programme d\u2019aide aux employ\u00e9s et \u00e0 la famille (PAEF) est l\u00e0 pour vous et votre famille. Les services du PAEF sont accessibles en tout temps, par t\u00e9l\u00e9phone au 1-844-880-9143, ou en ligne \u00e0 <a href=\"https://www.travailsantevie.com/\">travailsantevie.com</a>. Des services d\u2019aide en cas de crise sont aussi offerts \u00e0 l\u2019\u00e9chelle du Canada. Pour trouver un centre de crise, visitez <a href=\"https://www.crisisservicescanada.ca/fr/ressources-locales-et-soutien/\">le site Web des Services de crises du Canada</a>.</p>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/16Days2019-FR.png\" class=\"center-block\"></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "Raising Awareness of Gender-Based Violence - November 25 to December 10",
        "fr": "Savoir reconna\u00eetre la violence sexiste - Du 25 novembre au 10 d\u00e9cembre"
    },
    "news": {
        "date_posted": {
            "en": "2019-11-26",
            "fr": "2019-11-26"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Corporate Staffing and Diversity",
            "fr": "Politique et Programmes, Dotation Minist\u00e9rielle et Diversit\u00e9"
        }
    }
}