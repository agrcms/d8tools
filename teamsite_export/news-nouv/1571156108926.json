{
    "dcr_id": "1571156108926",
    "lang": "en",
    "title": {
        "en": "From Nougat and Oreo to Pie: Updating the Operating System on Your Samsung Device ",
        "fr": "Mise \u00e0 jour du syst\u00e8me d'exploitation de votre appareil Samsung : De Nougat ou Oreo \u00e0 Pie"
    },
    "modified": "2019-10-17 00:00:00.0",
    "issued": "2019-10-17 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2019-10-15",
    "breadcrumb_label": "",
    "meta": {
        "issued": {
            "en": "2019-10-17",
            "fr": "2019-10-17"
        },
        "modified": {
            "en": "2019-10-17",
            "fr": "2019-10-17"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "From Nougat and Oreo to Pie: Updating the Operating System on Your Samsung Device ",
            "fr": "Mise \u00e0 jour du syst\u00e8me d'exploitation de votre appareil Samsung : De Nougat ou Oreo \u00e0 Pie"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "On October 16, 2019, Shared Services Canada will release the latest operating system update for Samsung S8 devices.",
            "fr": "Le 16 octobre 2019, Services partag\u00e9s Canada diffusera la plus r\u00e9cente mise \u00e0 jour du syst\u00e8me d'exploitation pour les appareils Samsung S8."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>On October 16, 2019, Shared Services Canada will release the latest operating system update for Samsung S8 devices. All S8 owners should install the new version of the operating system, called Pie, as soon as it\u2019s available.</p>\n<h2><strong>How to install the update</strong></h2>\n<p>If you receive the notification that the update is available, just follow the instructions on your phone to download it. If you don\u2019t get the notification, you\u2019ll have to initiate the update yourself. Simply go to: Settings -&gt; Software Update -&gt; Download Updates Manually.</p>\n<p>Make sure you use your phone data for this download, even if you go over your monthly data limit. The update may take some time, especially if your device has an older version of the Android operating system (i.e., version 7 and 8 called Nougat and Oreo respectively).</p>\n<p>Remember that you should regularly accept and install phone updates as prompted. Certain updates are done to enhance security, so it\u2019s important you help keep our network safe.</p>\n<p>Once you\u2019ve upgraded to Pie, you <strong>must then immediately upgrade your UEM app</strong> to the latest version:</p>\n<ul>\n<li>Open the Play Store app &gt; Type in \u201cUEM\u201d &gt; Tap on \u201cBlackberry UEM\u201d &gt; Tap update.</li>\n<li>After updating the UEM client, you may need to re-authenticate. Follow the prompts.</li>\n</ul>\n<h2><strong>After the update</strong></h2>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/samsung-update-EN.png\" class=\"pull-right\">The biggest change you\u2019ll notice is a more intuitive separation of personal (e.g., text messages, phone) and workspace (e.g., email, calendar) applications. Following the update, the Workspace app icon will no longer exist. Instead, you\u2019ll access your work applications by swiping down on your Home screen and clicking on the Workspace tab at the bottom of your screen.</p>\n<p>Other changes include:</p>\n<ul>\n<li>a different look and feel for the phone menus and applications;</li>\n<li>the move of Workspace configurations to the Settings application on the Personal tab;</li>\n<li>the addition of a Finder window to help you find and organize applications in both Personal and Workspace tabs, and</li>\n<li>a navigation adjustment when viewing recently opened applications. Instead of clicking the button in the bottom-left corner of your screen and scrolling through opened applications by swiping up/down, you will have to swipe left/right instead.</li>\n</ul>\n<p>This update will not affect (i.e., delete) your installed applications, settings or security features such as passwords, so there is no need to back up any data.</p>\n<p>If you experience any technical issues, please contact <a href=\"http://assystweb.agr.gc.ca:8080/assystnet/#serviceOfferings/1394\">IT Centre TI</a>.</p>\n<p><strong>\u00a0</strong></p>\n<p><strong>IT Centre TI</strong><br>\nTelephone: 1-855-545-4411<br>\n<a href=\"http://myitcentreti-monitcentreti/\">My IT Centre TI</a></p>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Le 16 octobre 2019, Services partag\u00e9s Canada diffusera la plus r\u00e9cente mise \u00e0 jour du syst\u00e8me d\u2019exploitation pour les appareils Samsung S8. Tous les propri\u00e9taires d\u2019appareils S8 devraient installer la nouvelle version du syst\u00e8me d\u2019exploitation, appel\u00e9e Pie, d\u00e8s qu\u2019elle sera disponible.</p>\n<h2><strong>Comment installer la mise \u00e0 jour</strong></h2>\n<p>Lorsque vous recevrez un avis indiquant que la mise \u00e0 jour est disponible, suivez les instructions sur votre t\u00e9l\u00e9phone pour la t\u00e9l\u00e9charger. Si vous ne recevez pas d\u2019avis, vous devrez lancer la mise \u00e0 jour vous\u2011m\u00eame. Suivez le chemin suivant : Param\u00e8tres -&gt; Mise \u00e0 jour logicielle -&gt; T\u00e9l\u00e9chargement manuel des M\u00c0J.</p>\n<p>Assurez-vous d\u2019utiliser les donn\u00e9es de votre t\u00e9l\u00e9phone pour ce t\u00e9l\u00e9chargement, m\u00eame si vous d\u00e9passez votre limite mensuelle de donn\u00e9es. La mise \u00e0 jour peut prendre un certain temps, surtout si votre appareil est dot\u00e9 d\u2019une ancienne version du syst\u00e8me d\u2019exploitation Android (c\u2019est-\u00e0-dire les versions 7 et 8 appel\u00e9es respectivement Nougat et Oreo).</p>\n<p>N\u2019oubliez pas que vous devez accepter et installer r\u00e9guli\u00e8rement les mises \u00e0 jour t\u00e9l\u00e9phoniques qui vous sont demand\u00e9es. Certaines mises \u00e0 jour servent \u00e0 am\u00e9liorer la s\u00e9curit\u00e9 et sont donc essentielles pour assurer la s\u00e9curit\u00e9 de notre r\u00e9seau.</p>\n<p>Une fois la mise \u00e0 niveau vers Pie effectu\u00e9e, vous devez <strong>imm\u00e9diatement mettre \u00e0 niveau votre application UEM</strong> et la remplacer par  la derni\u00e8re version:</p>\n<ul>\n<li>Ouvrez l'application Play Store&gt; Tapez \u201cUEM\u201d&gt; Tapez sur \u201cBlackberry UEM\u201d&gt; Tapez sur mettre \u00e0 jour.</li>\n<li>Apr\u00e8s la mise \u00e0 jour du client UEM, vous devrez peut-\u00eatre vous authentifier de nouveau. Suivez les instructions.</li>\n</ul>\n<h2><strong>Apr\u00e8s la mise \u00e0 jour</strong></h2>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/samsung-update-FR.png\" class=\"pull-right\" alt=\"\">Le plus grand changement que vous constaterez est une s\u00e9paration plus intuitive entre les applications personnelles (p. ex., messages textes, t\u00e9l\u00e9phone) et les applications de travail (p. ex., courriel, calendrier). Apr\u00e8s la mise \u00e0 jour, l\u2019ic\u00f4ne de l\u2019espace de travail n\u2019existera plus. Vous acc\u00e9derez plut\u00f4t \u00e0 vos applications de travail en glissant votre doigt vers le bas sur votre \u00e9cran d\u2019accueil et en cliquant sur l\u2019onglet de l\u2019espace de travail au bas de l\u2019\u00e9cran.</p>\n<p>Parmi les autres changements, mentionnons :</p>\n<ul>\n<li>une apparence diff\u00e9rente pour les menus et les applications;</li>\n<li>le d\u00e9placement des configurations de l\u2019espace de travail vers l\u2019application des param\u00e8tres dans l\u2019onglet personnel;</li>\n<li>l\u2019ajout d\u2019une fen\u00eatre de recherche pour vous aider \u00e0 trouver et \u00e0 organiser les applications dans l\u2019onglet personnel et l\u2019espace de travail;</li>\n<li>un changement de navigation lors de l\u2019affichage des applications r\u00e9cemment ouvertes : au lieu de cliquer sur le bouton dans le coin inf\u00e9rieur gauche de votre \u00e9cran et de faire d\u00e9filer les applications ouvertes en glissant le doigt vers le haut ou le bas, vous devrez glisser le doigt vers la gauche ou la droite.</li>\n</ul>\n<p>Cette mise \u00e0 jour ne supprimera pas vos applications install\u00e9es, vos param\u00e8tres ou vos fonctions de s\u00e9curit\u00e9 telles que les mots de passe; il n\u2019est donc pas n\u00e9cessaire de sauvegarder les donn\u00e9es.</p>\n<p>Si vous \u00e9prouvez des difficult\u00e9s techniques, veuillez communiquer avec le\u00a0<a href=\"http://assystweb.agr.gc.ca:8080/assystnet/#serviceOfferings/1394\">IT Centre TI</a>.</p>\n<p><strong>\u00a0</strong></p>\n<p><strong>IT Centre TI</strong><br>\nT\u00e9l\u00e9phone : 1-855-545-4411<br>\n<a href=\"http://myitcentreti-monitcentreti/\">Mon IT Centre TI</a></p>\n\t\t"
    },
    "breadcrumb": {
        "en": "From Nougat and Oreo to Pie: Updating the Operating System on Your Samsung Device",
        "fr": "Mise \u00e0 jour du syst\u00e8me d'exploitation de votre appareil Samsung : De Nougat ou Oreo \u00e0 Pie"
    },
    "news": {
        "date_posted": {
            "en": "2019-10-17",
            "fr": "2019-10-17"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "IT Centre TI",
            "fr": "IT Centre TI"
        }
    }
}