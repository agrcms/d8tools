{
    "dcr_id": "1601316035474",
    "lang": "en",
    "title": {
        "en": "Reminder - If You Connect It, Protect It: Tips for Cyber Security Awareness Month 2020 ",
        "fr": "Prot\u00e9gez ce que vous branchez : conseils dans le cadre du Mois de la sensibilisation \u00e0 la cybers\u00e9curit\u00e9 2020"
    },
    "modified": "2020-10-14 00:00:00.0",
    "issued": "2020-09-30 00:00:00.0",
    "type_name": "intra-intra/news-nouv",
    "node_id": "1279030006975",
    "layout_name": null,
    "dc_date_created": "2020-09-29",
    "breadcrumb_label": "",
    "trigger_date": "2020-10-14 17:28:39.0",
    "meta": {
        "issued": {
            "en": "2020-09-30",
            "fr": "2020-09-30"
        },
        "modified": {
            "en": "2020-10-14",
            "fr": "2020-10-14"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Reminder - If You Connect It, Protect It: Tips for Cyber Security Awareness Month 2020 ",
            "fr": "Prot\u00e9gez ce que vous branchez : conseils dans le cadre du Mois de la sensibilisation \u00e0 la cybers\u00e9curit\u00e9 2020"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "With the quick shift to remote work, you have probably found yourself in a hastily-thrown-together home office, using various tools to stay in touch with colleagues and to continue doing your work.",
            "fr": "Le passage soudain au t\u00e9l\u00e9travail vous oblige probablement \u00e0 travailler dans un bureau \u00e0 domicile improvis\u00e9, utilisant divers outils pour rester en contact avec vos coll\u00e8gues et continuer de faire votre travail."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "<br>\n<br>\n<p>With the quick shift to remote work, you have probably found yourself in a hastily-thrown-together home office, using various tools to stay in touch with colleagues and to continue doing your work. So have most AAFC employees. With this increased number of people connecting remotely and using various devices (laptop, phone) comes an increased number of targets vulnerable to cyber attacks.</p>\n<p>As October is Cyber Security Awareness Month, it\u2019s a good time for a reminder that you are key in helping us keep our network, information and data safe from such attacks.</p>\n<p>Here are some simple steps you can take to protect your device.</p>\n<ol>\n<li><strong>Access the AAFC network through government devices only</strong>, as per the <a href=\"http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101055452&amp;lang=eng\">AAFC IT Security Policy</a>\u00a0(Word). Use AAFC-provided workstations, mobile devices, and other equipment. Using personal devices to access the government network could lead to sensitive information being shared or leaked. You are the only person who should have access to the device; family members should use your personal home devices.</li>\n<li><strong>Ensure you understand and follow the Treasury Board Secretariat directive on the secure use of network and devices</strong>. With remote work, we\u2019ve seen an increased use of the network for personal reasons. Remember that government infrastructure should be used for work purposes. Personal use is limited; read more about <a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=32605\">acceptable</a> and <a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=32606\">unacceptable</a> uses of government networks and devices.</li>\n<li><strong>Practice good password etiquette and store passwords securely</strong>. Choose strong passwords using a mix of uppercase and lowercase letters, numbers and special characters. Avoid writing down account log-in information on sticky notes or cards. Use different passwords for different accounts. Account credentials hold access to sensitive information, which could lead to security breaches if stolen.</li>\n<li><strong>Accept the latest software and operating system updates to ensure that your device is secure and continues working properly</strong>. Always accept any update prompts on your devices. If you have an AAFC mobile phone, go to the Play Store to download updates on a monthly basis (Play Store &gt; My apps and games &gt; Update all); some applications don\u2019t prompt for updates automatically.</li>\n<li><strong>Recognize and avoid potential phishing attacks. </strong>Be careful when opening emails or attachments from people you don\u2019t know, and following any links to Web sites included in these emails. Delete suspicious emails immediately. If you have opened the email and clicked on the link, notify <a href=\"https://assystweb.agr.gc.ca/assystnet/#serviceOfferings/1285\">IT Centre TI</a>. Report any email extortion attempts to security at aafc.securityincident-incidentsecurite.aac@canada.ca. Watch out for <a href=\"https://www.getcybersafe.gc.ca/en/resources/7-red-flags-phishing\">phishing red flags</a>.</li>\n<li><strong>Prevent information and data loss. </strong>Avoid sharing information/data<strong> </strong>outside of our network. Frequently save important files in a corporate repository (e.g., Knowledge Workspace, AgriDOC). <strong>Remember that M365 is for transitory and unclassified content only</strong>, and that content there is regularly deleted.</li>\n</ol>\n<p>Cyber defence is a team sport. It is essential that we all work together to strengthen our departmental cyber security.</p>\n<h2>\nLearn more </h2>\n<ul>\n<li><a href=\"https://www.getcybersafe.gc.ca/en/cyber-security-awareness-month\">Cyber Security Awareness Month web page</a>\n</li>\n<li>Find Get Cyber Safe on <a href=\"https://twitter.com/GetCyberSafe\">Twitter</a>, <a href=\"https://www.facebook.com/GetCyberSafe\">Facebook</a>, <a href=\"https://www.instagram.com/getcybersafe/\">Instagram</a>, <a href=\"https://www.youtube.com/playlist?list=PLEzYzSmu1IFQXghAzLrYBmf278UnRbVyo\">YouTube</a> and <a href=\"https://www.linkedin.com/showcase/get-cyber-safe\">LinkedIn</a>.\n<ul>\n<li>\u00a0\n<ul>\n<li>\u00a0\n<ul>\n<li>\u00a0\n<ul>\n<li>\u00a0\n<ul>\n</ul>\n</li>\n</ul>\n</li>\n</ul>\n</li>\n</ul>\n</li>\n</ul>\n</li>\n</ul>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/CSAM-820x312-banner-e.jpg\" class=\"center-block\"></p>\n<ul>\n</ul>\n<p>\u00a0</p>\n<ul>\n</ul>\n\n\t\t",
        "fr": "<br>\n<br>\n<p>Le passage soudain au t\u00e9l\u00e9travail vous oblige probablement \u00e0 travailler dans un bureau \u00e0 domicile improvis\u00e9, utilisant divers outils pour rester en contact avec vos coll\u00e8gues et continuer de faire votre travail. C\u2019est aussi le cas de la plupart des employ\u00e9s d\u2019AAC. Cette augmentation du nombre de personnes qui se branchent \u00e0 distance au moyen de divers appareils (ordinateurs portables, t\u00e9l\u00e9phones) se traduit par un nombre accru de cibles vuln\u00e9rables aux cyberattaques.</p>\n<p>Le mois d\u2019octobre \u00e9tant le Mois de la sensibilisation \u00e0 la cybers\u00e9curit\u00e9, le moment est propice pour vous rappeler le r\u00f4le essentiel que vous jouez dans la protection de notre r\u00e9seau, de nos renseignements et de nos donn\u00e9es contre les attaques.</p>\n<p>Voici quelques mesures simples que vous pouvez prendre pour prot\u00e9ger vos appareils.</p>\n<ol>\n<li><strong>Acc\u00e9dez au r\u00e9seau d\u2019AAC \u00e0 partir d\u2019un appareil du gouvernement seulement</strong>, comme il est pr\u00e9vu dans la <a target=\"_blank\" href=\"http://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=101882750&amp;lang=fra\">Politique de s\u00e9curit\u00e9 des technologies de l\u2019information</a>\u00a0(Word). Utilisez un poste de travail, un appareil mobile ou autre mat\u00e9riel fourni par AAC. L\u2019utilisation d\u2019un appareil personnel pour acc\u00e9der au r\u00e9seau du gouvernement pourrait entra\u00eener la transmission ou la fuite de renseignements sensibles. Vous seul devriez avoir acc\u00e8s \u00e0 l\u2019appareil; les membres de votre famille devraient utiliser vos appareils personnels \u00e0 domicile.</li>\n<li><strong>Assurez-vous de comprendre et de suivre la directive du Secr\u00e9tariat du Conseil du Tr\u00e9sor sur l\u2019utilisation s\u00e9curitaire des r\u00e9seaux et des appareils. </strong>Depuis que les employ\u00e9s travaillent \u00e0 distance, nous avons constat\u00e9 une utilisation accrue du r\u00e9seau pour des raisons personnelles. N\u2019oubliez pas que l\u2019infrastructure du gouvernement doit \u00eatre utilis\u00e9e \u00e0 des fins professionnelles. L\u2019utilisation personnelle est limit\u00e9e; lisez la section sur l\u2019<a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=32605\">utilisation acceptable</a> et l\u2019<a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=32606\">utilisation inacceptable</a> des r\u00e9seaux et des appareils gouvernementaux.</li>\n<li><strong>Adoptez de bonnes pratiques en mati\u00e8re de mots de passe et conservez vos mots de passe en toute s\u00e9curit\u00e9.</strong> Choisissez des mots de passe complexes en utilisant une combinaison de majuscules, de minuscules, de chiffres et de caract\u00e8res sp\u00e9ciaux. \u00c9vitez d\u2019\u00e9crire vos informations de connexion sur des feuillets autoadh\u00e9sifs ou des fiches. Utilisez un mot de passe diff\u00e9rent pour chacun de vos comptes. Les identifiants de compte donnent acc\u00e8s \u00e0 des renseignements sensibles, ce qui pourrait mener \u00e0 une atteinte \u00e0 la s\u00e9curit\u00e9 en cas de vol.</li>\n<li><strong>Acceptez les derni\u00e8res mises \u00e0 jour des logiciels et du syst\u00e8me d\u2019exploitation pour vous assurer que votre appareil est prot\u00e9g\u00e9 et continue de fonctionner correctement</strong>. Acceptez toujours les mises \u00e0 jour qui vous sont propos\u00e9es sur vos appareils. Si vous avez un t\u00e9l\u00e9phone mobile d\u2019AAC, ouvrez la boutique Play Store pour t\u00e9l\u00e9charger les mises \u00e0 jour tous les mois (Play Store &gt; Mes applications et jeux &gt; Tout mettre \u00e0 jour); certaines applications ne sont pas mises \u00e0 jour automatiquement.</li>\n<li><strong>Reconnaissez et \u00e9vitez les attaques potentielles par hame\u00e7onnage. Soyez prudent lorsque vous ouvrez des courriels ou des pi\u00e8ces jointes ou cliquez sur des liens provenant de personnes que vous ne connaissez pas.</strong> Supprimez imm\u00e9diatement les courriels suspects. Si vous avez ouvert un courriel suspect et cliqu\u00e9 sur le lien, avisez le <a href=\"https://assystweb.agr.gc.ca/assystnet/#serviceOfferings/1285\">IT Centre TI</a>. Signalez toute tentative d\u2019extorsion au Service de s\u00e9curit\u00e9 \u00e0 l\u2019adresse <a href=\"mailto:aafc.securityincident-incidentsecurite.aac@canada.ca\">aafc.securityincident-incidentsecurite.aac@canada.ca</a>. Soyez attentif aux <a href=\"https://www.pensezcybersecurite.gc.ca/fr/ressources/les-7-signaux-dalarme-de-lhameconnage\">signaux d\u2019alarme de l\u2019hame\u00e7onnage</a>.</li>\n<li><strong>Pr\u00e9venez la perte de renseignements et de donn\u00e9es. </strong>\u00c9vitez de transmettre des renseignements ou des donn\u00e9es en dehors de notre r\u00e9seau. Sauvegardez fr\u00e9quemment les fichiers importants dans un d\u00e9p\u00f4t minist\u00e9riel<strong> </strong>(p. ex. l\u2019Espace de travail du savoir, AgriDOC). <strong>N\u2019oubliez pas que M365 ne doit servir \u00e0 conserver que le contenu transitoire et non classifi\u00e9,</strong> et que le contenu qui s\u2019y trouve est r\u00e9guli\u00e8rement supprim\u00e9.</li>\n</ol>\n<p>La cyberd\u00e9fense, c\u2019est un sport d\u2019\u00e9quipe. Il est essentiel que nous contribuions tous \u00e0 renforcer la cybers\u00e9curit\u00e9 du Minist\u00e8re.</p>\n<h2><strong>Pour en savoir plus </strong></h2>\n<ul>\n<li><a href=\"https://www.pensezcybersecurite.gc.ca/fr/mois-de-la-sensibilisation-la-cybersecurite\">Page Web du Mois de la sensibilisation \u00e0 la cybers\u00e9curit\u00e9</a></li>\n<li>\u00ab Pensez cybers\u00e9curit\u00e9 \u00bb sur <a href=\"https://twitter.com/cyber_securite\">Twitter</a>, <a href=\"https://www.facebook.com/Pensezcybersecurite/?fref=tag\">Facebook</a>, <a href=\"https://www.instagram.com/pensezcybersecurite/\">Instagram</a>, <a href=\"https://www.youtube.com/playlist?list=PLrl2myz22ffQgFyvP44uGFGg1LiZuy7Dg\">YouTube</a> et <a href=\"https://www.linkedin.com/showcase/pensez-cybers%C3%A9curit%C3%A9\">LinkedIn</a></li>\n</ul>\n<p><img src=\"https://intranet.agr.gc.ca/resources/prod/news/images/CSAM-820x312-banner-f.jpg\" class=\"center-block\"></p>\n<ul>\n</ul>\n\t\t"
    },
    "breadcrumb": {
        "en": "Reminder - If You Connect It, Protect It: Tips for Cyber Security Awareness Month 2020",
        "fr": "Prot\u00e9gez ce que vous branchez : conseils dans le cadre du Mois de la sensibilisation \u00e0 la cybers\u00e9curit\u00e9 2020"
    },
    "news": {
        "date_posted": {
            "en": "2020-10-01",
            "fr": "2020-10-01"
        },
        "to": {
            "en": "All staff",
            "fr": "Tous les employ\u00e9s"
        },
        "by": {
            "en": "Information Systems Branch",
            "fr": "Direction g\u00e9n\u00e9rale des syst\u00e8mes d'information"
        }
    }
}