<?php


if (!isset($argv[1])) {
  echo "usage: " . $argv[0] . '  path/to/d8tools/Intranet_Database_dumpEN_FR_tab_adjusted.csv \t' . "\n";
  //echo "assumes header as follows: DCR_ID   LANG   TITLE   MODIFIED   ISSUED   TYPE_NAME   NODE_ID   LAYOUT_NAME   CREATED   UPDATED\n";
  echo "assumes header as follows: dcr_id   lang   title   modified   issued   type_name   node_id   layout_name   created   updated\n";
  die;
}

$file = file_get_contents($argv[1]);
$csv = csv_to_array($argv[1]);
function csv_to_array($filename='', $delimiter="\t")
{
  if(!file_exists($filename) || !is_readable($filename))
    return FALSE;

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
      {
        if(!$header)
          $header = $row;
        else
          $data[] = array_combine($header, $row);
      }
      fclose($handle);
    }
    return $data;
}

$array_of_content = array();
foreach($csv as $row) {
  if ($row['layout_name'] == '<NULL>') {
    $row['layout_name'] = NULL;
  }
  if (!isset($row['lang']) && isset($row['langcode'])) {
    $row['lang'] = $row['langcode'];
    unset($row['langcode']);
  }
  if ($row['lang'] == 'eng') {
    $row['lang'] = 'en';
  }
  if ($row['lang'] == 'fra') {
    $row['lang'] = 'fr';
  }
  if (!isset($array_of_content[$row['dcr_id']])) {
    $array_of_content[$row['dcr_id']] = $row;
  } else {
    $origRow = $array_of_content[$row['dcr_id']];
    if ($origRow['lang'] == 'en' && !isset($row['title']['en']) && $row['lang'] == 'fr') {
      $titleEn = $origRow['title'];
      $origRow['title'] = array();
      $origRow['title']['en'] = $titleEn;
      $origRow['title']['fr'] = $row['title'];
      $array_of_content[$origRow['dcr_id']] = $origRow;
    }
  }
}

$objs = arrayToObject($array_of_content);
function arrayToObject($array) {
  if (!is_array($array)) {
    return $array;
  }

  $object = new stdClass();
  if (is_array($array) && count($array) > 0) {
    foreach ($array as $name=>$value) {
      $name = strtolower(trim($name));
      if (!empty($name)) {
        $object->$name = arrayToObject($value);
      }
    }
    return $object;
  }
  else {
    return FALSE;
  }
}

//echo print_r($objs, TRUE);
$month_day = date('m-d', time());

$dir = getcwd() . '/new_export_' . $month_day;
if (!is_dir($dir)) {
  mkdir($dir);
}
chdir($dir);
$keep_existing = true;
$count_json = 0;
$count_skipped = 0;
$dec31_2017 = strtotime('2017-12-31');
$oct11_2020 = strtotime('2020-10-10');
$skip = FALSE;
foreach ($array_of_content as $key => $entity) {
  $skip = FALSE;// This needs to be at the top of the forloop inside.
  if ($keep_existing) {
    if (file_exists($key.'.json')) {
      continue;
    }
  }
  $type = str_replace('intra-intra/', '', $entity['type_name']);
  chdir($dir);
  if (!is_dir($type)) {
    mkdir($type);
  }
  chdir($type);
  if ($fp = fopen($key.'.json', 'w')) {
    $count_json++;
    $entity_obj = arrayToObject($entity);
    if ($type == 'empl-empl' || $type == 'news-nouv') {
//    if (strtotime($entity_obj->modified) < $dec31_2017) {
      if (strtotime($entity_obj->modified) < $oct11_2020) {
        $count_skipped++;
        $skip = TRUE;
      }
    }
    echo "Write $key.json\n";
    if ($key == '1281033861093') {
      continue;
      // home en / important-notices fr (skip this bogus page)
    }
    fwrite($fp, json_encode($entity, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE));
    fclose($fp);
    if ($skip) {
      echo "Delete $key.json $type\n";
      unlink($key.'.json');// or die("Couldn't delete $key.json");
      $skip = FALSE;
    }
  }
  chdir($dir);
}

echo "$count_json entities processed, skipped $count_skipped entities.\n";
echo "Wrote " . ($count_json - $count_skipped) . " json files to $dir\n";
