<?php


if (!isset($argv[1])) {
  echo "usage: " . $argv[0] . '  path/to/d8tools/Intranet_Database_dumpEN_FR_tab_adjusted.csv \t' . "\n";
  //echo "assumes header as follows: DCR_ID   LANG   TYPE_NAME   NEWS_CATEGORY_TYPE_ID   CREATED\n";
  echo "assumes header as follows: dcr_id   lang   type_name    staffadvisor  submitername submiteremail contactname contactemail contactphonenumber  issued\n";
  die;
}

$file = file_get_contents($argv[1]);
$csv = csv_to_array($argv[1]);
function csv_to_array($filename='', $delimiter="\t")
{
  if(!file_exists($filename) || !is_readable($filename))
    return FALSE;

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
      {
        if(!$header)
          $header = $row;
        else
          $data[] = array_combine($header, $row);
      }
      fclose($handle);
    }
    return $data;
}

$array_of_content = array();
foreach($csv as $row) {
  if (isset($row['layout_name']) && $row['layout_name'] == '<NULL>') {
    $row['layout_name'] = NULL;
  }
  if ($row['lang'] == 'eng') {
    $row['lang'] = 'en';
  }
  if ($row['lang'] == 'fra') {
    $row['lang'] = 'fr';
  }
  if (!isset($array_of_content[$row['dcr_id']])) {
    $array_of_content[$row['dcr_id']] = $row;
  } 
}

$objs = arrayToObject($array_of_content);
function arrayToObject($array) {
  if (!is_array($array)) {
    return $array;
  }

  $object = new stdClass();
  if (is_array($array) && count($array) > 0) {
    foreach ($array as $name=>$value) {
      $name = strtolower(trim($name));
      if (!empty($name)) {
        $object->$name = arrayToObject($value);
      }
    }
    return $object;
  }
  else {
    return FALSE;
  }
}

//echo print_r($objs, TRUE);

$dir = 'update_emplopportunity_hardlaunch';
if (!is_dir($dir)) {
  mkdir($dir);
}
chdir($dir);
$keep_existing = false;
$count_json = 0;
$count_skipped = 0;
$dec31_2017 = strtotime('2017-12-31');
$oct11_2020 = strtotime('2020-10-11');
$skip = FALSE;
foreach ($array_of_content as $key => $entity) {
  $skip = FALSE;// This needs to be at the top of the forloop inside.
  if ($keep_existing) {
    if (file_exists($key.'.json')) {
      continue;
    }
  }
  $type = str_replace('intra-intra/', '', $entity['type_name']);
  if (!is_dir($type)) {
    mkdir($type);
  }
  chdir($type);
  if ($fp = fopen($key.'.json', 'w')) {
    $count_json++;
    $entity_obj = arrayToObject($entity);
    if ($type == 'empl-empl' || $type == 'news-nouv') {
      if (strtotime($entity_obj->issued) < $dec31_2017) {
      //if (strtotime($entity_obj->issued) < $oct11_2020) {
        $count_skipped++;
        $skip = TRUE;
      }
    }
    echo "Write $key.json\n";
    fwrite($fp, json_encode($entity, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE));
    fclose($fp);
    if ($skip) {
      echo "Delete $key.json $type\n";
      unlink($key.'.json');// or die("Couldn't delete $key.json");
      $skip = FALSE;
    }
  }
  chdir('../../' . $dir);
}

echo "$count_json entities processed, skipped $count_skipped entities.\n";
echo "Wrote " . ($count_json - $count_skipped) . " json files\n";
