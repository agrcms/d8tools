{
    "has_dcr_id": true,
    "node_id": "1322232022561",
    "parent_node_id": "1313613624272",
    "dcr_id": "1298990071366",
    "landingPageUrl": {
        "en": "1298990071366",
        "fr": "1298990071366"
    },
    "parent_dcr_id": "1298922166577",
    "breadcrumb": {
        "en": "Tools and Tips",
        "fr": "Outils et conseils"
    },
    "node_title": {
        "en": "null",
        "fr": "null"
    },
    "title": {
        "en": "Tools and Tips",
        "fr": "Outils et conseils"
    },
    "type": "find_people",
    "meta": {
        "issued": {
            "en": "2011-03-18",
            "fr": "2011-03-18"
        },
        "modified": {
            "en": "2019-09-05",
            "fr": "2019-09-05"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Tools and Tips",
            "fr": "Outils et conseils"
        },
        "subject": {
            "en": "communications",
            "fr": "communications"
        },
        "description": {
            "en": "Seven Tips for Quality Translation",
            "fr": "Sept conseils pour obtenir une traduction de qualit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "translation",
            "fr": "traduction"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC_AgriSource",
            "fr": "AAC_AgriSource"
        },
        "accessRights": {
            "en": "3",
            "fr": "3"
        }
    },
    "body": {
        "en": "\n\t\t\t  \n<nav>\n<p><a href=\"#a\">Seven Tips for Quality Translation</a><br><a href=\"#b\">Tracking Changes</a><br><a href=\"#c\">Reference Tools</a><br><a href=\"#d\">Questions You Asked Us...</a></p>\n</nav>\n<h2 id=\"a\">Seven Tips for Quality Translation</h2>\n<p><b>1. Plan for translation.</b><br>Avoid unnecessary panic and stress. Quality translation and revision take time. Please consult our <a href=\"display-afficher.do?id=1298990029142&amp;lang=eng\">Service Standards</a> and factor translation and revision in your project plan. <a href=\"display-afficher.do?id=1298922166577&amp;lang=eng#contactus\">Contact us</a> if you need help in establishing a production schedule for major projects.</p>\n<p><b>2. The translated text is only as good as the source document.</b><br>The source language document should be of good quality. If required, we can edit the source document prior to translation. Please consult our <a href=\"display-afficher.do?id=1298990029142&amp;lang=eng\">Service Standards</a> and factor in the time required for editing source documents when planning your project.</p>\n<p><b>3. Compare publications in their final format.</b><br>For printed publications, send us a proofreading request once the document has been laid out in its final format (for example, in PDF). Formatting requires a lot of manipulation and may introduce errors in the wording and placement of text.</p>\n<p><b>4. No change is too small or insignificant for translation or revision.</b><br>Let us know if changes are made to the document in the course of a project. We need to review every change in order to guarantee the language quality and the faithfulness of the message.</p>\n<p><b>5. Track Changes.</b><br>Please enable the Track Changes function if you are making changes to a document that has been previously translated. If you forget to track your changes, you will need to create a <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3557438&amp;lang=eng\" target=\"_blank\">comparison document (Word)</a>. Tracking changes and using compared documents allow us to focus on the changes you made, expediting the process.</p>\n<p><b>6. Seek out other options.</b><br>If you have a large document, consider providing a summary or abstract rather than translating the entire document. Consult with your Strategic Communications Advisor or Corporate Communications Advisor to determine alternative communication strategies.</p>\n<p><b>7. Give us heads-up.</b><br>Let us know ahead of time if you will need specialized services such as:</p>\n<ul>\n<li>translation to or from languages other than English and French</li>\n<li>translation of multimedia products (for example, videos)</li>\n</ul>\n\n<h2 id=\"b\">Tracking Changes</h2>\n<p>Changes are sometimes made after translation is completed. Avoid translating the changes yourself. Provide us with the changes and we will ensure that quality remains intact.</p>\n<p>You must first identify your changes using either the Track Changes function (preferred) or the  <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3557438&amp;lang=eng\" target=\"_blank\">comparison functions (Word)</a>. Once you have compared your documents, use the request form and send us the:</p>\n<ul>\n<li>compared version (comparison of new version to last version)</li>\n<li>last source version</li>\n<li>last translated version</li>\n</ul>\n\n<p>These documents are to be sent in as reference documents.</p>\n\n<p>Be sure to verify the document once the comparison process is complete to confirm that the comparison has been carried out correctly.</p>\n\n<h2 id=\"c\">Reference Tools</h2>\n\n<h3>Government of Canada</h3>\n<p><a rel=\"external\" href=\"http://www.noslangues-ourlanguages.gc.ca/index-eng.php\">Language Portal of Canada</a><br><a rel=\"external\" href=\"http://www.btb.termiumplus.gc.ca/tpv2alpha/alpha-eng.html?lang=eng&amp;srchtxt=&amp;i=1&amp;index=alt\">Termium</a><br><a rel=\"external\" href=\"http://publiservice.gc.ca/reference/ref_e.html\">Publiservice - Reference Tools</a></p>\n\n<h3>Government of Quebec</h3>\n<p><a rel=\"external\" href=\"http://www.granddictionnaire.com/\"><span lang=\"fr\">Grand dictionnaire terminologique</span> (in French only)</a></p>\n\n<h2 id=\"d\">Questions You Asked Us...</h2>\n<p><b>Can I ask for help or explanations regarding translations, revisions or linguistic issues?</b><br>For help or explanations regarding translations, revisions or linguistic issues, please <a href=\"mailto:commtranslation@agr.gc.ca\">email us</a>.</p>\n<p><b>What should I do if I have several documents, all about one project, that must be translated?</b><br><a href=\"display-afficher.do?id=1298922166577&amp;lang=eng#contactus\">Contact us</a> and one of our coordinators will discuss your needs regarding the project and work with you to ensure its success.</p>\n<p><b>Do you provide simultaneous interpretation services?</b><br>No. Please contact the <a rel=\"external\" href=\"https://www.tpsgc-pwgsc.gc.ca/bt-tb/contact-eng.html\">Translation Bureau</a> for more information on conference interpretation services.</p>\n<p><b>What can I do if I am not satisfied with the quality of the translation?</b><br>Your feedback on your level of satisfaction is important to us. <a href=\"display-afficher.do?id=1298922166577&amp;lang=eng#contactus\">Contact us</a> and let us address your concern.</p>\n<p><b>I made some changes in my document after it was translated. What should I do?</b><br>We understand that changes are sometimes made after translation is completed. While it is tempting to try to translate these changes yourself, please refrain from doing so. Follow the steps outlined in <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3557438&amp;lang=eng\" target=\"_blank\">Creating a Comparison Document (Word)</a>.</p>\n\n\t\t",
        "fr": "\n\t\t\t  \n<nav>\n<p><a href=\"#a\">Sept conseils pour obtenir une traduction de qualit\u00e9</a><br><a href=\"#b\">Suivi des modifications</a><br><a href=\"#c\">Outils de r\u00e9f\u00e9rence</a><br><a href=\"#d\">Vous nous avez demand\u00e9...</a></p>\n</nav>\n<h2 id=\"a\">Sept conseils pour obtenir une traduction de qualit\u00e9</h2>\n<p><b>1. Planifiez la traduction.</b><br>\n\u00c9vitez la panique et le stress inutile. Il faut du temps pour effectuer une traduction et une r\u00e9vision de qualit\u00e9. Veuillez consulter les  <a href=\"display-afficher.do?id=1298990029142&amp;lang=fra\">Normes de service</a> et tenir compte de la traduction et de la r\u00e9vision dans la planification de votre projet. <a href=\"display-afficher.do?id=1298922166577&amp;lang=fra#contacteznous\">Contactez-nous</a> si vous d\u00e9sirez obtenir de l'aide pour \u00e9tablir un calendrier de production pour vos projets de grande envergure.</p>\n<p><b>2. Le texte traduit ne sera pas de meilleure qualit\u00e9 que le document original.</b><br>\nLe document original doit \u00eatre de bonne qualit\u00e9. Au besoin, nous pouvons r\u00e9viser le document original avant sa traduction. Consultez nos <a href=\"display-afficher.do?id=1298990029142&amp;lang=fra\">Normes de service</a> pour d\u00e9terminer le temps n\u00e9cessaire \u00e0 la r\u00e9vision unilingue du document original et en tenir compte dans la planification de votre projet.</p>\n<p><b>3. Comparez les publications dans leur forme d\u00e9finitive.</b><br>\nDans le cas de publications imprim\u00e9es, faites-nous parvenir une demande de correction d'\u00e9preuves lorsque le document a \u00e9t\u00e9 mis en page dans son format d\u00e9finitif (par exemple, en pdf). Le formatage n\u00e9cessite de nombreuses manipulations qui peuvent entra\u00eener des erreurs dans la phras\u00e9ologie et la disposition du texte.</p>\n<p><b>4. Aucune modification n'est trop mineure ni trop insignifiante pour ne pas \u00eatre traduite ou r\u00e9vis\u00e9e.</b><br>Avisez-nous lorsque des modifications sont apport\u00e9es au document pendant un projet. Nous devons r\u00e9viser chaque modification afin de garantir la qualit\u00e9 de la langue et la fid\u00e9lit\u00e9 du message.</p>\n<p><b>5. Faites le suivi des modifications.</b><br>\nActivez  la fonction de suivi des modifications si vous modifiez un document d\u00e9j\u00e0 traduit. Si vous oubliez  de faire ce suivi, vous devrez cr\u00e9er un <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3557439&amp;lang=fra\" target=\"_blank\">document compar\u00e9 (Word)</a>\n. Le  suivi des modifications et le recours \u00e0 des documents compar\u00e9s nous permettent  de nous concentrer sur les modifications  apport\u00e9es et ainsi d'acc\u00e9l\u00e9rer le  processus.</p>\n<p><b>6. Envisagez d'autres possibilit\u00e9s.</b><br>\nSi  vous avez un gros document, envisagez d'en faire traduire un sommaire ou un  r\u00e9sum\u00e9 plut\u00f4t que tout  le document. Consultez votre conseiller strat\u00e9gique en communications ou votre conseiller en  communications internes pour \u00e9tablir d'autres strat\u00e9gies de communication.</p>\n<p><b>7. Faites-nous signe.</b><br>\nInformez-nous d'avance si vous pr\u00e9voyez avoir besoin de services sp\u00e9cialis\u00e9s comme :</p>\n<ul>\n  <li>la traduction vers une langue \u00e9trang\u00e8re (autre que le fran\u00e7ais ou l'anglais) ou la traduction d'un texte r\u00e9dig\u00e9 en langue \u00e9trang\u00e8re vers l'anglais, le fran\u00e7ais ou une autre langue \u00e9trang\u00e8re;</li>\n  <li>la traduction de produits multim\u00e9dia (par exemple, vid\u00e9os). </li>\n</ul>\n<h2 id=\"b\">Suivi des modifications</h2>\n<p>Des modifications sont parfois  apport\u00e9es une fois la traduction termin\u00e9e. \u00c9vitez de les traduire vous-m\u00eame. Envoyez-les nous plut\u00f4t; la qualit\u00e9 de votre  document sera ainsi assur\u00e9e.</p>\nVous devez d'abord indiquer les  modifications au moyen de la fonction de suivi des modifications  ou de la \n  <a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3557439&amp;lang=fra\" target=\"_blank\">fonction de comparaison (Word)</a>. Une fois les documents compar\u00e9s, utilisez le formulaire de demande et envoyez-nous :\n<ul>\n  <li>le document compar\u00e9 (comparaison de la nouvelle version par rapport  \u00e0 la derni\u00e8re version  traduite);</li>\n<li>la derni\u00e8re version en langue de d\u00e9part;</li>\n<li>la derni\u00e8re version traduite.</li>\n</ul>\n\n<p>Ces documents doivent nous \u00eatre fournis comme documents de r\u00e9f\u00e9rence.</p>\n\n<p>Assurez-vous de v\u00e9rifier le document une fois le processus de comparaison termin\u00e9, afin de confirmer que la comparaison a \u00e9t\u00e9 effectu\u00e9e correctement.</p>\n\n<h2 id=\"c\">Outils de r\u00e9f\u00e9rence</h2>\n\n<h3>Gouvernement du Canada</h3>\n<p><a rel=\"external\" href=\"http://www.noslangues-ourlanguages.gc.ca/index-fra.php\">Portail linguistique du Canada</a><br><a rel=\"external\" href=\"http://www.btb.termiumplus.gc.ca/tpv2alpha/alpha-fra.html?lang=fra&amp;srchtxt=&amp;i=1&amp;index=alt\">Termium</a><br><a rel=\"external\" href=\"http://publiservice.gc.ca/reference/ref_f.html\">Publiservice - Outils de r\u00e9f\u00e9rence</a></p>\n\n<h3>Gouvernement du Qu\u00e9bec</h3>\n<p><a rel=\"external\" href=\"http://www.granddictionnaire.com/\">Grand dictionnaire terminologique</a></p>\n\n<h2 id=\"d\">Vous nous avez demand\u00e9...</h2>\n<p><b>Puis-je demander de l'aide ou des explications au sujet de traductions, de r\u00e9visions ou de probl\u00e8mes linguistiques?</b><br>Pour obtenir de l'aide ou des explications au sujet de traductions, de r\u00e9visions ou de probl\u00e8mes linguistiques, veuillez <a href=\"mailto:commtranslation@agr.gc.ca\">nous envoyer un courriel</a>.</p>\n<p><b>Comment dois-je proc\u00e9der si je dois faire traduire plusieurs documents faisant partie d'un m\u00eame projet?</b><br>\n<a href=\"display-afficher.do?id=1298922166577&amp;lang=fra#contacteznous\">Contactez-nous</a>. L'un  de nos coordonnateurs communiquera  avec vous pour discuter  de vos besoins et collaborera avec vous afin d'assurer le bon d\u00e9roulement de votre projet.</p>\n<p><b>Offrez-vous des services d'interpr\u00e9tation simultan\u00e9e?</b><br>\nNon. Contactez le <a rel=\"external\" href=\"https://www.tpsgc-pwgsc.gc.ca/bt-tb/contact-fra.html\">Bureau de la traduction</a> pour obtenir de plus amples renseignements sur les services d'interpr\u00e9tation de conf\u00e9rences.</p>\n<p><b>Que puis-je faire si je ne suis pas satisfait de la qualit\u00e9 de la traduction?</b><br>\n<a href=\"display-afficher.do?id=1298922166577&amp;lang=fra#contacteznous\">Contactez-nous.</a>  Nous voulons savoir si vous \u00eates  satisfaits de nos services ou si vous avez des pr\u00e9occupations. Le cas \u00e9ch\u00e9ant, nous ferons les  suivis n\u00e9cessaires.</p>\n<p><b>J'ai modifi\u00e9 mon document apr\u00e8s l'avoir fait traduire.  Que dois-je faire?</b><br>\n  Nous  comprenons qu'un document  peut \u00eatre modifi\u00e9 une fois la traduction termin\u00e9e. Vous voudrez peut-\u00eatre traduire  ces modifications vous-m\u00eame, mais nous vous prions de vous en abstenir. Suivez plut\u00f4t les instructions donn\u00e9es dans  le document\n<a href=\"https://intranet.agr.gc.ca/agrisource/AgriDoc.do?id=3557439&amp;lang=fra\" target=\"_blank\">cr\u00e9ation d'un document de comparaison (Word)</a>.</p>\n\t\t"
    }
}