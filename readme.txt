Note: this readme is mostly complete.
from the AAFC network:

exporting steps:

cd d8tools; #clone it from agr-cms gitlab if you haven't already.
git pull; #make sure up to date code
cd d8tools/agrisource/xml2json;
php -f teamsite_sitemap_xml2json.php ../sitemap_teamsite_COVID5sept28.xml
 #copy and paste json output into a file (clunky ,but can improve this step later).

# if any of these export scripts fails autoload of classes (which they will on first run) do edit the require at the top as follows:
require 'C:\Users\OlstadJ\d8tools\vendor\autoload.php';
# change OlstadJ to your username
# on windows you'll also need to install php as this won't work without php (using php version 7.3.15).

php csv_to_json_export.php ~/d8tools/Intranet_Database_dumpEN_FR_tab_adjusted_Sept25-2020.csv \t
  # output is json, have to copy paste, shrink fontsize to very small to avoid line wrap issues, paste text output into a new json file called scrape_agrisource_COVID4.json , if needed use a json validator to find invalid lines and fix them, mostlikely caused by linewrap issues mitigate with shrinking font size of your window
php agrisource/process_sitemap_COVID2.php ~/d8tools/agrisource/scrape_agrisource_COVID4.json
  # due to some legacy sitemap garbage, some files json files were manually massaged so that gene-gene pages import correctly as they have dependency on these.

php update_json.php ~/d8tools/export_sitemap_10-02 sitemap # see next line massaging, 3 records, added chmod 444 read only to those.
  # I put chmod 444 on the 3 files in ~/d8tools/export/sitemap that I don't want overwritten, this seems to work.

php update_json.php ~/d8tools/export/sitemap sitemap # I only check in the non-massaged ones here (export/sitemap). chmod 444 seems to work
  # update_json.php exporting sitemap (landing_page) data from legacy web page directly
  # After this is done I use git diff to compare old with new sitemap export to avoid changing those manually massaged files by doing a diff vs the previous run (git history is the export_sitemap folder).
php update_json.php ~/d8tools/new_export_10-02/gene-gene teamsite ~/d8tools/export/sitemap
  # above step is to fix broken references in gene-gene using the sitemap data as a comparison (IMPORTANT STEP!)
php update_json.php ~/d8tools/new_export_10-02/gene-gene teamsite
  # update_json.php exporting gene-gene (page) data from legacy web page directly
php update_json.php ~/d8tools/new_export_10-02/empl-empl teamsite
php update_json.php ~/d8tools/new_export_10-02/news-nouv teamsite
php update_json.php ~/d8tools/new_export_10-02/nwinit-nwinit teamsite
php update_json.php ~/d8tools/new_export_10-02/home-accu teamsite

   # remove the old gene-gene, put the new ones in
rm teamsite_export/gene-gene/*.json
cp new_export_10-02/gene/*.json teamsite_export/gene-gene/ -pr
   # remove the old news-nouv, put the new ones in
rm teamsite_export/news-nouv/*.json
cp new_export_10-02/news-nouv/* teamsite_export/news-nouv/
   # remove the old empl-empl, put the new ones in
rm teamsite_export/empl-empl/*.json
cp new_export_10-02/empl-empl/* teamsite_export/empl-empl/
git add teamsite_export/gene-gene
git add teamsite_export/empl-empl
git add teamsite_export/news-nouv



Drupal Bug:
When you run any import or update script, you receive the error:
Drupal\Componet\Plugin\Exception\PlugninNOtFoundException: The "EntityHasField" plugin does not exist.
Valid plugin IDs for Drupal\Core\ValidationConstraintManger are: Callback...

In SqlContentEntiyStorage.php line 846:
The "EntityHasField" plugin does not exist. 

Go to /en/admin/content, add any fake node.
After that, run the script agian.

Run the script to convert the script again.

How to run updating News information:
1. Convert a csv file as Json files
php csv_to_json_exportfornewscategory.php /fullpath/update_newscategory.csv \t;

drush scr /fullpath/update.php /fullpath/update_newscategory/news-nouv News;

php csv_to_json_exportfornewsinfo.php update_newsapprovalsubmitter.csv \t;
drush scr /fullpath/update.php /fullpath/update_newsapprovalsubmitter/news-nouv news;


How to run updating Employment Opportunity information
php csv_to_json_exportforEOinfo.php  /fullpath/update_emplopportunity.csv \t;
drush scr /fullpath/update.php /fullpath/update_newsapprovalsubmitter/empl-empl empl;






# Jun post import update scripts:
drush scr ../d8tools/convertTeamsiteIdToDrupal/convert_teamsiteId_to_drupal.php;
drush scr ../d8tools/convertTeamsiteIdToDrupal/updateContentURL.php /full/path/to/d8tools/convertTeamsiteIdToDrupal/export/
# For the last command please use the full path.


# post-script instruction 
# ---------------- news's  category & expirydate  ---------------------------------
# /**  generate json files into update_newscategory/news-nouv/ folder    ***/

php csv_to_json_exportfornewscategory.php news_category_expireddate_softlaunch.csv \t

# /**********  update news category and expirydate  **********/

drush scr /d8tools/update.php /d8tools/update_newscategory/news-nouv news;

# # ----------------- news's submitter and approver ---------------------------------
/**  generate json files into update_newsapprovalsubmitter/news-nouv/ folder    ***/

php csv_to_json_exportfornewsinfo.php update_newsapprovalsubmitter_softlaunch.csv \t;

# /**********  update news approver and submitter  **********/

drush scr /d8tools/update.php /d8tools/update_newsapprovalsubmitter/news-nouv news;

# --------------- EO submitter and approver ----------------------------------
# /**  generate json files into update_emplopportunity/empl-empl/ folder   ***/

php csv_to_json_exportforEOinfo.php update_emplopportunity_softlaunch.csv  \t;

# /**********  update EO approver and submitter  **********/

drush scr  /d8tools/update.php  /d8tools/update_emplopportunity/empl-empl empl;
