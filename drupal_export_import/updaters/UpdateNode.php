<?php

use Drush\Drush;

use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database; // Rather than db_query use this instead.

class UpdateNode
{
  public $could_not_load = FALSE;
  protected $node;
  protected $trnode;
  protected $data;
  protected $preserveNid = false;
  protected $connection = NULL;

  function loadData($jsonfile)
  {
    $this->data = json_decode(file_get_contents($jsonfile));
  }

  function nid()
  {
    return $this->node->id();
  }

  function old_id()
  {
    // Drush::output()->writeln('debug d7nid ' . (int) $this->data->nid);
    //return (int) $this->data->nid;
    return (int) $this->data->dcr_id;
  }

  function loadNodeByOldId() {
    //d7_nid
    if (is_null($this->connection)) {
      $this->connection = Database::getConnection();
    }
    $nid = $this->connection->query('SELECT nid FROM {node} where dcr_id = :dcr_id',
       [':dcr_id' => $this->old_id()])->fetchField();
     
    if (is_numeric($nid)) {
      $node = Node::load($nid);
    } else {
      $this->could_not_load = TRUE;
      Drush::output()->writeln('Unable to load d8 node, d8nid was not found by dcr_id=' . $this->old_id());
    }
    if ($node) {
      $this->node = $node;
      $this->trnode = $this->node->getTranslation('fr'); //Get the translation
    }
  }

  /**
   * Import files (images) from the Drupal 7 site
   * @param int $d7fid
   */
  function importFile($d7fid, &$d8fid=null)
  {
  }

  function updateField($field, $d8_field=null)
  {
    global $nid_old_to_new;
    global $nid_new_to_old;

    if (!$d8_field) {
      $d8_field = $field;
    }

    if($this->data->language == "en") {
      $origLang = "en";
      $oppLang = "fr";
    }
    else {
      $origLang = "fr";
      $oppLang = "en";
    }

    switch ($field) {
      case 'field_override_image_sharing':
        // Twitter override field.
        $metatag = $this->data->metatag;
	$d8fid = NULL;
        if ($twitr_img_en = $metatag->en->{'twitter:image'}->value) {
          $twitr_img_fr = NULL; 
          if (!is_null($metatag->fr->{'twitter:image'})) {
            $twitr_img_fr = $metatag->fr->{'twitter:image'}->value;
          }
          $YYYYmm = substr($this->data->date_released, 0, 7);
          $this->importTwitterImage($twitr_img_en, $twitr_img_fr,'override_sharing/priority_override', $YYYYmm, $d8fid, $d8fid_fr, $height, $width);
	  $file_storage = \Drupal::entityTypeManager()->getStorage('file');
	  $twitter_image = $file_storage->load($d8fid); 
          $save_something = FALSE;
          if (!is_null($d8fid)) {
            $save_something = TRUE;
            $this->node->set($d8_field, array(
              'width' => $width,
              'height' => $height,
              'target_id' => $d8fid,
              'alt' => isset($this->data->title->en) ? $this->data->title->en : 'Image',
            ));
            // Drush::output()->writeln('node en ' . $this->data->dcr_id . ' now has an image');
          }
          if (!is_null($d8fid_fr)) {
            $save_something = TRUE;
            $this->trnode->set($d8_field, array(
              'width' => $width,
              'height' => $height,
              'target_id' => $d8fid_fr,
              'alt' => isset($this->data->title->fr) ? $this->data->title->fr : 'Image',
            ));
            // Drush::output()->writeln('node fr ' . $this->data->dcr_id . ' now has an image');
	  } else if (!is_null($d8fid)) {
            // Use the english d8fid.
            $this->trnode->set($d8_field, array(
              'width' => $width,
              'height' => $height,
              'target_id' => $d8fid,
              'alt' => isset($this->data->title->fr) ? $this->data->title->fr : 'Image',
            ));
	  }
          if ($save_something) {
            $this->save();
            if (is_null($d8fid_fr)) {
              Drush::output()->writeln( "saved node " . $this->nid() . ' fid=' . $d8fid . ' ' . pathinfo($twitr_img_en, PATHINFO_FILENAME));
              //echo  "saved node " . $this->nid() . ' fid=' . $d8fid . ' ' . pathinfo($twitr_img_en, PATHINFO_FILENAME) . "\r";
            } else {
              Drush::output()->writeln("saved node " . $this->nid() . ' fid=' . $d8fid . ' ' . pathinfo($twitr_img_en, PATHINFO_FILENAME) .  ' fid=' . $d8fid_fr . ' ' . pathinfo($twitr_img_fr, PATHINFO_FILENAME) );
              //echo "saved node " . $this->nid() . ' fid=' . $d8fid . ' ' . pathinfo($twitr_img_en, PATHINFO_FILENAME) .  ' fid=' . $d8fid_fr . ' ' . pathinfo($twitr_img_fr, PATHINFO_FILENAME) . "\r";
            }
          }
        }
        break;
      default:
        Drush::output()->writeln('Switch default, no action taken.');
        break;
    } //End switch

  } // End updateField function.


  function save()
  {
    $this->node->set('moderation_state', 'published');
    $this->trnode->set('moderation_state', 'published');
    $this->node->save();
    $this->trnode->save();
  }

}

