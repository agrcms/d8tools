<?php

use Drupal\node\Entity\Node;

class article
{
  public $data = array();
  public $nid;
  public $vid;
  public $type;
  public $language;
  public $title;
  public $uid;
  public $status;
  public $created;
  public $changed;
  public $comment;
  public $promote;
  public $sticky;
  public $tnid;
  public $translate;
  public $uuid;

  function __construct()
  {
  }

  function load($nid)
  {
    $row = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
    $DEBUG = FALSE;
    if ($DEBUG) {
      $methods = get_class_methods($row);
      if ($fp = fopen('DEBUG_methods.txt', 'a')) {
        fwrite($fp, print_r($methods, TRUE) . "\r\n");
        fclose($fp);
      }
    }
    $this->nid = $nid;
    $this->vid = $row->getLoadedRevisionId();
    $this->type = $row->getType();
    $this->uid = $row->getOwnerId();
    $this->status = $row->isPublished();
    $this->created = $row->getCreatedTime();
    $this->changed = $row->getChangedTime();
    $this->comment = $row->getRevisionLogMessage();
    $this->promote = $row->isPromoted();
    $this->sticky = $row->isSticky();
    $this->tnid = $nid;
    $this->translate = $row->isTranslatable();
    $langOrig = 'en'; // Get english field values.
    $this->language = $langOrig; // Default to 'en' original, should be ok.
    $this->uuid = $row->uuid;
    $this->date_released = $row->get('field_date_released')->value;
    $row = $row->getTranslation($langOrig);
    $this->title[$langOrig] = $row->getTitle();
    $this->location[$langOrig] = $row->get('field_location')->value;
    $this->node_title = $row->getTitle(); // The D8/D9 importer assumes importing from d7.  Can remove this in future versions.
    $this->news_category = $row->get('field_news_category')->target_id;

    $this->news_image[$langOrig]['fid'] = $row->get('field_news_image')->target_id;
    $this->news_image[$langOrig]['file_uri'] = $row->get('field_news_image')->entity->uri->value;
    $this->news_image[$langOrig]['width'] = $row->get('field_news_image')->width;
    $this->news_image[$langOrig]['height'] = $row->get('field_news_image')->height;
    $this->news_image[$langOrig]['alt'] = $row->get('field_news_image')->alt;
    $this->news_image[$langOrig]['pm_file_path'] = $this->image_target_to_prod_filepath($row->get('field_news_image')->entity->uri->value);

    $this->news_thumbnail[$langOrig]['fid'] = $row->get('field_news_thumbnail')->target_id;
    $this->news_thumbnail[$langOrig]['file_uri'] = $row->get('field_news_thumbnail')->entity->uri->value;
    $this->news_thumbnail[$langOrig]['width'] = $row->get('field_news_thumbnail')->width;
    $this->news_thumbnail[$langOrig]['height'] = $row->get('field_news_thumbnail')->height;
    $this->news_thumbnail[$langOrig]['alt'] = $row->get('field_news_thumbnail')->alt;
    $this->news_thumbnail[$langOrig]['pm_file_path'] = $this->image_target_to_prod_filepath($row->get('field_news_thumbnail')->entity->uri->value);

    $this->override_image_sharing[$langOrig]['fid'] = $row->get('field_override_image_sharing')->target_id;
    $this->override_image_sharing[$langOrig]['file_uri'] = $row->get('field_override_image_sharing')->entity->uri->value;
    $this->override_image_sharing[$langOrig]['width'] = $row->get('field_override_image_sharing')->width;
    $this->override_image_sharing[$langOrig]['height'] = $row->get('field_override_image_sharing')->height;
    $this->override_image_sharing[$langOrig]['alt'] = $row->get('field_override_image_sharing')->alt;
    $this->override_image_sharing[$langOrig]['pm_file_path'] = $this->image_target_to_prod_filepath($row->get('field_override_image_sharing')->entity->uri->value);

    $this->path[$langOrig] = $row->get('path')->value;
    $this->title_format[$langOrig] = $row->get('title')->format;
    $this->body[$langOrig] = $row->get('body')->value;
    $this->summary[$langOrig] = $row->get('body')->summary;
    $this->body_format[$langOrig] = $row->body->format;
    if ($row->data) {
      $this->metatag[$langOrig] = unserialize($row->data);
    }

    // FRENCH NOW!
    $oppLang = 'fr'; //Switch to french.
    $row = $row->getTranslation($oppLang);
    $this->news_image[$oppLang]['fid'] = $row->get('field_news_image')->target_id;
    $this->news_image[$oppLang]['file_uri'] = $row->get('field_news_image')->entity->uri->value;
    $this->news_image[$oppLang]['width'] = $row->get('field_news_image')->width;
    $this->news_image[$oppLang]['height'] = $row->get('field_news_image')->height;
    $this->news_image[$oppLang]['alt'] = $row->get('field_news_image')->alt;
    $this->news_image[$oppLang]['pm_file_path'] = $this->image_target_to_prod_filepath($row->get('field_news_image')->entity->uri->value);

    $this->news_thumbnail[$oppLang]['fid'] = $row->get('field_news_thumbnail')->target_id;
    $this->news_thumbnail[$oppLang]['file_uri'] = $row->get('field_news_thumbnail')->entity->uri->value;
    $this->news_thumbnail[$oppLang]['width'] = $row->get('field_news_thumbnail')->width;
    $this->news_thumbnail[$oppLang]['height'] = $row->get('field_news_thumbnail')->height;
    $this->news_thumbnail[$oppLang]['alt'] = $row->get('field_news_thumbnail')->alt;
    $this->news_thumbnail[$oppLang]['pm_file_path'] = $this->image_target_to_prod_filepath($row->get('field_news_thumbnail')->entity->uri->value);

    $this->override_image_sharing[$oppLang]['fid'] = $row->get('field_override_image_sharing')->target_id;
    $this->override_image_sharing[$oppLang]['file_uri'] = $row->get('field_override_image_sharing')->entity->uri->value;
    $this->override_image_sharing[$oppLang]['width'] = $row->get('field_override_image_sharing')->width;
    $this->override_image_sharing[$oppLang]['height'] = $row->get('field_override_image_sharing')->height;
    $this->override_image_sharing[$oppLang]['alt'] = $row->get('field_override_image_sharing')->alt;
    $this->override_image_sharing[$oppLang]['pm_file_path'] = $this->image_target_to_prod_filepath($row->get('field_override_image_sharing')->entity->uri->value);

    $this->title[$oppLang] = $row->getTitle();
    $this->title_format[$oppLang] = $row->get('title')->format;
    $this->body[$oppLang] = $row->get('body')->value;
    $this->path[$oppLang] = $row->get('path')->value;
    $this->location[$oppLang] = $row->get('field_location')->value;
    $this->summary[$oppLang] = $row->get('body')->summary;
    $this->body_format[$oppLang] = $row->body->format;
    $result = db_query(sprintf("SELECT * FROM {node__field_meta_tags} where entity_id=%d and revision_id=%d", $this->nid, $this->vid));
    //$result = db_query(sprintf("SELECT * FROM {node__field_meta_tags} where entity_id=%d", 2));
    foreach ($result as $row) {
      if ($row->field_meta_tags_value) {
        $this->metatag[$row->langcode] = unserialize($row->field_meta_tags_value);
      }
    }
  }

  function image_target_to_prod_filepath($publicUri) {
    // Converts this string:
    // public://media/article/2019-11/filename.jpg
    // INTO:
    // https://pm.gc.ca/sites/pm/files/media/article/2019-11/filename.jpg
    $publicString = 'public://';
    $prodString = 'https://pm.gc.ca/sites/pm/files/';
    $pmFilename = str_replace($publicString, $prodString, $publicUri);
    $pmFilename = str_replace(' ', '%20', $pmFilename);
    return $pmFilename;
  }

  function to_json()
  {
    $data = array();
    $fields = array(
      'nid','vid','type','language','node_title','uid','status','created','changed','comment','promote','sticky',
      'tnid','translate','uuid','date_released','news_category','location','title','title_format',
      'body','summary','body_format','image_caption','path','image_caption_format',
      'metatag','fid','width','height','news_image','news_thumbnail','override_image_sharing',
    );
    foreach ($fields as $fld) {
      $data[$fld] = isset($this->$fld) ? $this->$fld : null;
    }
    return json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
  }
}
