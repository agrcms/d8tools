<?php

global $keep_existing;

$keep_existing = false;

$export_dir = DRUPAL_ROOT.'/sites/pm/export';

if (!is_dir($export_dir)) {
  mkdir($export_dir, 0777, true);
}
chdir($export_dir);

export_content(/*'page',*/'article');

chdir($export_dir);
export_files();

function export_content($ctype) {
  global $keep_existing;

  $cclass = ucfirst($ctype);
  include_once "exporters/$cclass.php";

  $nodes = array();
  // from comms list of dpm records that we want to keep:
  $article_nids = '8978,8966,8957,8840,8801,8747,8735,8717,8663,8609,8615,8537,8531,8528,8498,8489,8465,8456,8444,8348,8342,8279,8240,8231,8144,8123,8108,8003,7979,7835,7826,7760,7673,7652,7610,7598,7583,7574';
  $article_nids = explode(",", $article_nids);// Make it an array.

  $content_type = 'article';
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', $content_type);
//  $march4 = 1583280000;
//  $query->condition('created', $march4, '>');
  $query->condition('nid', $article_nids, 'in');
  //$sept1_2019 = 1567296000;
  //$query->condition('created', $sept1_2019, '>');
  //$query->condition('title', 'Deputy', 'CONTAINS');
  //$query->condition('body', 'Freeland', 'CONTAINS');
  $query->sort('created');
  $entity_ids = $query->execute();

  foreach ($entity_ids as $row) {
    $nodes[] = $row;
  }


  $dir = $ctype;
  if (!is_dir($dir)) {
    mkdir($dir);
  }
  chdir($dir);

  // Need to remove all existing json files, in case something was deleted or unpublished.
  if (!$keep_existing) {
    system('rm -rf *.json');
  }

  echo "Exporting ".count($nodes)." $ctype nodes...\n";

  $leadingZeros = ''; // Initialize leadingZeros.
  $cnt = 0;
  foreach ($nodes as $nid) {
    $cnt++;
    if (($cnt % 100) == 0) {
      echo "$cnt\n";
    }
    $nidStrLength = strlen($nid);
    $maxLeadingZeros = 7;
    $leadingZeros = '';
    if ($maxLeadingZeros-$nidStrLength > 0) {
      $leadingZeros = str_repeat('0',$maxLeadingZeros-$nidStrLength);
      // DSutter suggested a simpler way to do leading zeros with sprintf ex: $str = sprintf("%07d", $nid)
      // Suggested also possibly str_pad() for leading zeros
      // Keeping as-is for now.
    }
    if ($keep_existing) {
      if (file_exists($leadingZeros.$nid.'.json')) {
        continue;
      }
    }
    if ($fp = fopen($leadingZeros.$nid.'.json', 'w')) {
      $content = new $cclass();
      $content->load($nid);
      fwrite($fp, $content->to_json());
      fclose($fp);
    }
  }
  chdir('..');
}

/**
 * To facilitate exporting of files, we will create two files:
 * files.csv contains the fid and the URL
 * files.idx contains the byte offse of fid in files.csv
 */
function export_files() {
  if (! $fp = fopen('files.csv', 'w')) {
    echo "Cannot open files.csv for writing\n";
    return;
  }
  $index = array();
  $result = db_query("SELECT * FROM {file_managed}");
  foreach ($result as $row) {
    $index[$row->fid] = ftell($fp);
    $data = array($row->fid, $row->uri);
    fputcsv($fp, $data);
  }
  fclose($fp);

  $fp = fopen('files.idx', 'w');
  foreach ($index as $fid => $pos) {
    fwrite($fp, "$fid,$pos\n");
  }
  fclose($fp);
}
