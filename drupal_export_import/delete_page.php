<?php

use Drupal\node\Entity\Node;

$nids = \Drupal::entityQuery('node')->condition('type','page')->execute();
foreach ($nids as $nid) {
  Drush::output()->writeln('processing delete page for nid ' . $nid . "\n");
  $node = Node::load($nid);
  $node->delete();
}
