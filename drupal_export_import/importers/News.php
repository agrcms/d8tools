<?php

use Drupal\agri_admin\AgriAdminHelper;

class News extends ImportNode
{
  function getExportTitle(&$title, &$titleFr) {
    if ((is_null($title) || empty($title)) && !empty($this->data->title->en)) {
      if ($this->data->title->en != 'null') {
        $title = $this->data->title->en;
        $titleFr = $this->data->title->fr;
      }
    }
    if ((is_null($title) || empty($title)) && !empty($this->data->meta->title->en)) {
      if ($this->data->meta->title->en != 'null') {
        $title = $this->data->meta->title->en;
        $titleFr = $this->data->meta->title->fr;
      }
    }

  }

  public function isTooOld() {
    if (isset($this->data->meta->modified->en)) {
      $january2018 = 1514764800;
      $issued = strtotime($this->data->meta->modified->en);
      if ($issued > 1 && $issued < $january2018) {
        $this->too_old = TRUE;
        return TRUE;
      }
    }
    return FALSE;
  }

  private function preImport() {
    if (isset($this->data->body->en)) {
      $body_text = $this->data->body->en;
      $test_for_br = substr($body_text, 0, 10);
      if (strpos($test_for_br, 'br>', 3) > 0 && strpos($test_for_br, '<br', -5) > 0) {
        $pos_last_of_sequence = strpos($test_for_br, '<br', -5) + 2;
        $pos_first_open_tag = strpos($body_text, '<', $pos_last_of_sequence);
        $body_text = substr($body_text, $pos_first_open_tag);
        //echo substr($body_text, 0, 10); // debug
      }
    }
    if (isset($this->data->body->fr)) {
      $body_text = $this->data->body->fr;
      $test_for_br = substr($body_text, 0, 10);
      if (strpos($test_for_br, 'br>', 3) > 0 && strpos($test_for_br, '<br', -5) > 0) {
        $pos_last_of_sequence = strpos($test_for_br, '<br', -5) + 2;
        $pos_first_open_tag = strpos($body_text, '<', $pos_last_of_sequence);
        $body_text = substr($body_text, $pos_first_open_tag);
        // echo substr($body_text, 0, 10); // debug
      }
    }
    $this->type = 'page'; // Default.
    $ts_type = str_replace('intra-intra/', '', $this->data->type_name);
    switch ($ts_type) {
      case 'news-nouv':
        $this->type = 'news';
        break;
      case 'gene-gene':
        $this->type = 'page';
        return FALSE; // skip these for now.
        break;
      case 'empl-empl':
        $this->type = 'empl';
        break;
      case '':
      case NULL:
        $this->type = 'empty';
        return FALSE;
      case 'list-list':
      case 'widg-widg':
      case 'rank-clas':
      case 'nwinit-nwinit':
         $this->type = 'other';
         return FALSE;
      default:
        $this->type = $ts_type;
        break;
    }
    if (isset($this->data->pageNotFound) && $this->data->pageNotFound) {
      $this->type = 'deleted';
      //echo "pageNotFound for dcr_id: " . $this->data->dcr_id . "\n";
      return FALSE;
    }
    $ts_nid = $this->data->node_id;
    $dcr_id = $this->data->dcr_id;

    if (AgriAdminHelper::dcrIdPreviouslyImported($this->data->dcr_id)) {
      echo "Previously imported dcr_id: " . $this->data->dcr_id . "\n";
      $this->previously_imported = TRUE;
      return FALSE;
    }

    if (!AgriAdminHelper::tsNidPreviouslyImported($ts_nid)) {
      echo "ts_nid : $ts_nid\n";
      echo "dcr_id : $dcr_id\n";
      echo "Sitemap dependency was not yet imported, please run the import with \n" .
      "the sitemapinit option first and make sure you have the entire sitemap \n" .
      "content initialized first before running this teamsite import.\n";
      return FALSE;
      die;
    }
  }

  function import($jfile)
  {
    global $export_root;
    $this->loadData($jfile);

    if (strlen($this->data->dcr_id) > 7 && strlen($this->data->node_id) > 7) {
      $ts_nid = $this->data->node_id;
      $dcr_id = $this->data->dcr_id;
      $this->preImport();
      if ($this->isTooOld()) {
        return FALSE;
      }
      echo "Creating a Drupal " . $this->type . " for "  . $export_root . "/" . $this->type . "/" . $jfile . "\n";
      $this->createNode($this->type);
      if ($this->unable_to_import) {
        echo "unable to import\n";
        return FALSE;
      }

      if ($this->legacy_server_error) {
        echo "unable to import\n";
        return FALSE;
      }

      $this->importField('field_meta_type');//meta_type

      $this->importField('body');
      $this->importField('field_issued');//issued
      $this->importField('field_modified');//modified
      $this->importField('field_dc_date_created'/*, 'dc_date_created'*/);
      $this->importField('field_meta_type');//meta_type
      $this->importField('field_description');//description
      $this->importField('field_subject');//subject
      $this->importField('field_keywords');//keywords
      $this->importField('field_from','field_from');// news specific fields.
      $this->importField('field_to', 'field_to');// news specific fields
      $this->importField('field_posted','field_posted');// news specific fields
//    $this->importField('layout_builder_layout', 'layout_builder_layout');
//    $this->importField('layout_selection', 'layout_selection');

      if ($this->type == 'empl') {
        $this->importField('field_type');
        $this->importField('field_classification');
        $this->importField('field_branch');
        $this->importField('field_locations');
        $this->importField('field_date_closing');
        $this->importField('field_open_to');
        $this->importField('field_name');
        $this->importField('field_email');
      }

      $this->save();

      if (0) {
        // Disabled (for now).
        AgriAdminHelper::createInternalLegacyMenuLink($this->nid(), $ts_nid, $ts_nid, $dcr_id, 'main', 'en');
      }

      //AgriAdminHelper::updateNodeLegacyIds($this->nid(), $ts_nid, $ts_pnid, $dcr_id); // this should be done by the import.php.
      $this->generateAlias();
      return TRUE; // Success.
    } else {
      echo "Might not have a node_id or a dcr_id " . $this->data->dcr_id . ".json\n";
      $this->preImport();
      return FALSE;
    }
  }
}
