<?php

use Drush\Drush;

use Drupal\node\Entity\Node;

$nids = \Drupal::entityQuery('node')->condition('type','empl')->execute();
foreach ($nids as $nid) {
  Drush::output()->writeln('processing delete news for nid ' . $nid . "\n");
  $node = Node::load($nid);
  $node->delete();
}

