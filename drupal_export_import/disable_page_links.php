<?php

use Drush\Drush;

use Drupal\agri_admin\AgriAdminHelper;

$nids = \Drupal::entityQuery('node')->condition('type','page')->execute();
foreach ($nids as $nid) {
  Drush::output()->writeln('processing disableMenuLink for main and sidebar nid ' . $nid . "\n");
  AgriAdminHelper::disableMenuLink(NULL, $nid, 'main', TRUE);
  AgriAdminHelper::disableMenuLink(NULL, $nid, 'sidebar', TRUE);
}

